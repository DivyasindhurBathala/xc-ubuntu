<?php

set_time_limit(0);
require_once '../assets/sso/db.php';
$db = new MysqliDb(array(
    'host' => '172.16.100.252',
    'username' => 'mvno',
    'password' => 'Mvno2018!',
    'db' => 'mvno',
    'port' => 3306,
    'charset' => 'utf8'));

$connection = new AMQPConnection(array('host' => '10.10.10.104', 'vhost' => '/', 'port' => 5672, 'login' => 'admin', 'password' => 'un!t3d'));
$connection->connect();
$channel = new AMQPChannel($connection);
$channel->setPrefetchCount(0);
$queue = new AMQPQueue($channel);
$company =  getCompanyQueue($argv[1]);
$companyid = $argv[1];
print_r(array('key' => $company['rabbit_event_routingkey'], 'queue' => $company['rabbit_event_queue']));
if (!$company) {
    die('Company configuration is not correct');
} else {
    $queue->setName($company['rabbit_event_queue']);
    $queue->bind('artaEventDistributor', $company['rabbit_event_routingkey']);
}
echo date('Y-m-d H:i:s')." Starting Companyid.".$companyid." queue...\n";
$queue->setFlags(AMQP_DURABLE);
$queue->declareQueue();
$queue->consume("processMessage");
$connection->disconnect();
function processMessage($envelope, $queue)
{
    global $companyid;

    $message = $envelope->getBody();
    $rkey = $envelope->getRoutingKey();
    $payload = json_decode($envelope->getBody());
    //print_r($payload);
    //Determine the message Type
    $mtype = 'unknown';
    if (strpos($rkey, 'Arta.Events.Reseller.Subscription.Provisioning.JobEndEvent') >= 0) {
        $mtype = "JobEndEvent";
    } elseif (strpos($rkey, 'Arta.Events.Reseller.Subscription.Subscription.StatusChangedEvent') >= 0) {
        $mtype = "StatusChangedEvent";
    }
    //echo "\n Message Type:" . $mtype . "\n";
    if ($mtype == "JobEndEvent") {
        if ($envelope->isRedelivery()) {
            $queue->ack($envelope->getDeliveryTag());
            $pl = $payload[0];
            echo "SN:  ".trim($pl->SubscriptionId)." has been Processed, SKIPPING ".$companyid."\n";
        } else {

            $pl = $payload[0];

            $service = getService($pl->SubscriptionId);

            if ($service) {

                if (!empty($pl->TaskName)) {
                    if($service->serviceid == $companyid){
                    $insert = true;
                    if (($pl->TaskName == 'PortIn') && ($pl->FinalJob == 'false') && ($pl->Success == 'true') && ($pl->JobName == 'Request') && ($pl->Action == 'PortInAccepted')) {
                        // Portin Accepted //
                        ChangeStatusService($service->serviceid, array('msisdn_status' => 'PortinAccepted'));
                        process_remote('send_email/portedinaccepted/'.$companyid, json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid, 'type' => 'Accepted')));
                        process_remote('send_email/PortinAccepted/'.$companyid, json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid, 'type' => 'PortinAccepted')));
                    } elseif (($pl->TaskName == 'PortIn') && ($pl->FinalJob == 'true') && ($pl->Success == 'true') && (trim($pl->JobName) == 'PortInChangeMsisdn') && ($pl->Action == 'PortedIn')) {
                        ChangeStatusService($service->serviceid, array('msisdn_status' => 'Active'));
                        ChangeStatusHosting($service->serviceid, array('status' => 'Active'));
                        process_remote('send_email/portedin/'.$companyid, json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid, 'type' => 'Done')));
                        process_remote('sync_general_pricing', json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid)));
                        process_remote('enable_service', json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid)));
                        process_remote('send_email/OrderActive/'.$companyid, json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid, 'type' => 'OrderActive')));
                         // Portin Completed //
                    } elseif (($pl->TaskName == 'PortIn') && ($pl->FinalJob == 'false') && ($pl->Success == 'true') && ($pl->JobName == 'Request') && ($pl->Action == 'PortInRejected')) {
                        // Portin Rejected //
                        process_remote('send_email/portinrejected/'.$companyid, json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid, 'type' => 'Rejected')));
                        ChangeStatusService($service->serviceid, array('msisdn_status' => 'PortinRejected'));
                        process_remote('send_email/OrderRejected/'.$companyid, json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid, 'type' => 'PortinRejected')));
                    } elseif (($pl->TaskName == 'PortIn') && ($pl->FinalJob == 'false') && ($pl->Success == 'true') && ($pl->JobName == 'PortInValidateVerificationCodeStep2') && ($pl->Action == 'PortInRejected')) {
                         // Portin Rejected due to wrong Code //
                        ChangeStatusService($service->serviceid, array('msisdn_status' => 'PortInValidateVerificationCodeRejected'));
                    } elseif (($pl->TaskName == 'PortIn') && ($pl->FinalJob == 'false') && ($pl->Success == 'true') && ($pl->JobName == 'PortInValidateVerificationCodeStep2') && ($pl->Action == 'PortInValidateVerificationCode')) {
                         // Portin Ok code //
                        ChangeStatusService($service->serviceid, array('msisdn_status' => 'PortInValidateVerificationCodeAccepted'));
                    } elseif (($pl->TaskName == 'Terminate') && ($pl->FinalJob == 'true') && ($pl->Success == 'true') && ($pl->JobName == 'Terminate')) {
                         // Terminated //
                        ChangeStatusService($service->serviceid, array('msisdn_status' => 'Terminated'));
                        ChangeStatusHosting($service->serviceid, array('status' => 'Terminated'));
                    } elseif (($pl->TaskName == 'Reactivate') && ($pl->FinalJob == 'true') && ($pl->Success == 'true')) {
                        ChangeStatusService($service->serviceid, array('msisdn_status' => 'Active'));
                        ChangeStatusHosting($service->serviceid, array('status' => 'Active'));
                        process_remote('enable_service', array('serviceid' => $service->serviceid, 'companyid' => $service->companyid, 'sn' => trim($pl->SubscriptionId)));
                    } elseif (($pl->TaskName == 'Activate') && ($pl->FinalJob == 'false') && ($pl->Success == 'true') && ($pl->JobName == 'ActivateSubscriber')) {
                        if(trim($service->msisdn_sn) == trim($service->msisdn)){
                            process_remote('enable_service', array('serviceid' => $service->serviceid, 'companyid' => $service->companyid, 'sn' => trim($pl->SubscriptionId)));
                            ChangeStatusService($service->serviceid, array('msisdn_status' => 'Active'));
                            ChangeStatusHosting($service->serviceid, array('status' => 'Active'));
                            process_remote('send_email/OrderActive/'.$companyid, json_encode(array('companyid' => $service->companyid, 'serviceid' => $service->serviceid, 'type' => 'OrderActive')));

                        }
                    } else {
                        echo "SN:  ".trim($pl->SubscriptionId)." No Action\n";
                        $insert = false;
                        $queue->ack($envelope->getDeliveryTag());
                    }
                    if ($insert) {
                        $array = (array) $pl;
                        $array['SubscriptionId'] = trim($pl->SubscriptionId);
                        $array['serviceid'] = $service->serviceid;
                        $array['companyid'] = $service->companyid;
                        echo $companyid." ".date('Y-m-d') ." ".print_r($array);

                        InsertEvent($array);
                        unset($array);
                    }
                    //
                }
                } else {
                    $queue->ack($envelope->getDeliveryTag());
                }

            } else {
                echo "Service Not found \n";
            }
            //$queue->ack($envelope->getDeliveryTag());
        }
    }
}
function getCompanyQueue($companyid)
{
    $db = MysqliDb::getInstance();
    $db->where("companyid", $companyid);
    $q = $db->get("a_configuration");
    if ($q) {
        foreach ($q as $row) {
            $res[$row['name']] = trim($row['val']);
        }
        return $res;
    } else {
        return false;
    }
}
function getService($msisdn)
{

    $db = MysqliDb::getInstance();
    $db->where("msisdn_sn", trim($msisdn), 'like');
    $q = $db->getOne("a_services_mobile");
    if ($q) {
        return (object) $q;
    } else {
        return false;
    }
}

function InsertEvent($data)
{
    $id =false;
    $db = MysqliDb::getInstance();
    $id = $db->insert('a_event_log', $data);
    return $id;
}
function ChangeStatusService($serviceid, $data)
{
    $db = MysqliDb::getInstance();

    $db->where('serviceid', $serviceid);
    if ($db->update('a_services_mobile', $data)) {
        echo date('Y-m-d H:i:s')." ".$db->count . " records were updated\n";
    } else {
        echo date('Y-m-d H:i:s')." update failed: " . $db->getLastError()."\n";
    }
}


function ChangeStatusHosting($serviceid, $data)
{
    $db = MysqliDb::getInstance();
    $db->where('id', $serviceid);
    if ($db->update('a_services', $data)) {
        echo date('Y-m-d H:i:s')." ".$db->count . " records were updated\n";
    } else {
        echo date('Y-m-d H:i:s')." update failed: " . $db->getLastError()."\n";
    }
}


function process_remote($action, $data)
{
    $ch = curl_init('https://partner.united-telecom.be/api/dev/local/'.$action);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data)));

    $result = curl_exec($ch);

    return json_decode($result);
}
