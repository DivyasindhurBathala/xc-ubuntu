<?php

$lang["Client"] = "Client"; //automaticly added 2019-07-27 10:54:25
$lang["Dashboard"] = "Dashboard"; //automaticly added 2019-07-27 10:54:25
$lang["Changepassword"] = "Changepassword"; //automaticly added 2019-07-27 10:54:25
$lang["First"] = "First"; //automaticly added 2019-07-27 10:54:25
$lang["Current Password"] = "Current Password"; //automaticly added 2019-07-27 10:54:25
$lang["New Password"] = "New Password"; //automaticly added 2019-07-27 10:54:25
$lang["Confirm Password"] = "Confirm Password"; //automaticly added 2019-07-27 10:54:25
$lang["Save"] = "Save"; //automaticly added 2019-07-27 10:54:25
$lang["Om uw wachtwoord te kunnen wijzigen moet het sterk zijn.
Een sterk wachtwoord voldoet aan de volgende eisen:<br />
- minimaal 8 karakters lang<br />
- ten minste vier soorten tekens: hoofdletters, kleine letters,<br />
cijfers en speciale tekens.<br />
Bijvoorbeeld: W@chtw00rd"] = "Om uw wachtwoord te kunnen wijzigen moet het sterk zijn.
Een sterk wachtwoord voldoet aan de volgende eisen:<br />
- minimaal 8 karakters lang<br />
- ten minste vier soorten tekens: hoofdletters, kleine letters,<br />
cijfers en speciale tekens.<br />
Bijvoorbeeld: W@chtw00rd"; //automaticly added 2019-07-27 10:54:25
$lang["Weak"] = "Weak"; //automaticly added 2019-07-27 10:54:25
$lang["Medium"] = "Medium"; //automaticly added 2019-07-27 10:54:25
$lang["Strong"] = "Strong"; //automaticly added 2019-07-27 10:54:25
$lang["Do not link my account"] = "Do not link my account"; //automaticly added 2019-07-27 10:54:25
$lang["Your Language has been changed to"] = "Your Language has been changed to"; //automaticly added 2019-07-27 10:54:29
$lang["English"] = "English"; //automaticly added 2019-07-27 10:54:29
$lang["Invoice"] = "Invoice"; //automaticly added 2019-07-27 10:54:34
$lang["Paid"] = "Paid"; //automaticly added 2019-07-27 10:54:34
$lang["Service"] = "Service"; //automaticly added 2019-07-27 10:58:25
$lang["Prijs"] = "Prijs"; //automaticly added 2019-07-27 10:58:25
$lang["Monthly"] = "Monthly"; //automaticly added 2019-07-27 10:58:25
$lang["Monthly"] = "Monthly"; //automaticly added 2019-07-27 10:58:25
$lang["Pending"] = "Pending"; //automaticly added 2019-07-27 10:58:25
$lang["Active"] = "Active"; //automaticly added 2019-07-27 10:58:25
$lang["Detail"] = "Detail"; //automaticly added 2019-07-27 10:58:39
$lang["1813499"] = "1813499"; //automaticly added 2019-07-27 10:58:39
$lang["Factsheet"] = "Factsheet"; //automaticly added 2019-07-27 10:58:39
$lang["Subscription"] = "Subscription"; //automaticly added 2019-07-27 10:58:39
$lang["VOICEMAIL LANGUAGE"] = "VOICEMAIL LANGUAGE"; //automaticly added 2019-07-27 10:58:39
$lang["Dutch"] = "Dutch"; //automaticly added 2019-07-27 10:58:39
$lang["French"] = "French"; //automaticly added 2019-07-27 10:58:39
$lang["Service Setting"] = "Service Setting"; //automaticly added 2019-07-27 10:58:39
$lang["Purchase Bundle"] = "Purchase Bundle"; //automaticly added 2019-07-27 10:58:39
$lang["Call detail Record"] = "Call detail Record"; //automaticly added 2019-07-27 10:58:39
$lang["Description"] = "Description"; //automaticly added 2019-07-27 10:58:39
$lang["Export Excel"] = "Export Excel"; //automaticly added 2019-07-27 10:58:39
$lang["Please Choose Bundle"] = "Please Choose Bundle"; //automaticly added 2019-07-27 10:58:39
$lang["Bundle Order Confirmation"] = "Bundle Order Confirmation"; //automaticly added 2019-07-27 10:58:39
$lang["Date Start"] = "Date Start"; //automaticly added 2019-07-27 10:58:39
$lang["Are you sure that you wish to order this Package"] = "Are you sure that you wish to order this Package"; //automaticly added 2019-07-27 10:58:39
$lang["Please type 'Yes i am sure'"] = "Please type 'Yes i am sure'"; //automaticly added 2019-07-27 10:58:39
$lang["Yes i am sure"] = "Yes i am sure"; //automaticly added 2019-07-27 10:58:39
$lang["1813499"] = "1813499"; //automaticly added 2019-07-27 11:03:23
