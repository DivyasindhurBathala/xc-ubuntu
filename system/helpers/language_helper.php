<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter Language Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/language_helper.html
 */
// ------------------------------------------------------------------------
if ( ! function_exists('lang'))
{
	/**
	 * Lang
	 *
	 * Fetches a language variable and optionally outputs a form label
	 *
	 * @param	string	$line		The language line
	 * @param	string	$for		The "for" value (id of the form element)
	 * @param	array	$attributes	Any additional HTML attributes
	 * @return	string
	 */
	function lang($line, $for = '', $attributes = array())
	{
                $lola = $line;
		$line = get_instance()->lang->line($line);
		//mail('mail@simson.one','sesss',print_r())
		if ($for !== '')
		{
			$line = '<label for="'.$for.'"'._stringify_attributes($attributes).'>'.$line.'</label>';
		}else{
	   	//add_trans($lola);
		}
	if(!$line){
	$line = $lola;
	}
		return $line;
	}
 function add_trans($cont)
    {
     $ci = &get_instance();
     $filename = '/home/mvno_dev/public_html/application/language/'.$ci->lang->current_lang().'/admin_lang.php';
	if( strpos(file_get_contents($filename),'$lang["'.$cont.'"]') === false) {
        $text = '$lang["'.str_replace('"','\"',$cont).'"] = "'.str_replace('"','\"',$cont).'";';
        file_put_contents($filename, $text."\n", FILE_APPEND | LOCK_EX);
	}
    }
}
