# MVNO Portal (European Portal)

# Portal Layout

![](img/layout.jpg)

1. Sidemenu (navigation menu to all functionalities)
2. Statistics (Statistics, depend on the company profile if does not opted for invoicing then the invoice statistic will not show)
3. Company Switcher( not available for MVNO who does not have multiple companies)
4. Smart search (Search all including subscriptions, invoices and clients)
5. Notifications (Notification counter where when you hover to it will give you list of today provisioning logs)
6. Languages switch: allow to change the languages of the interfaces
7. Tables where MVNO can view newly created order that need simcard assignment
8. Chat List of the online agent 

![](img/layout.png)

