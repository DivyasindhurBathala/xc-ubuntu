# Clients  

This Menu segment has 3 sub menu: Client list, Add new Client, List client notes

# Client List 

To view all clients in a datatables where you can filter, this page also allow you to view monthly client statistics, from the table you can open the client summaru by nagivate to their name, artilium id, client id or their email and there is also button at the end to nagivate away to view their information, as standard you will see 10 entries but you can also change it to view up to 100 rows at a time.

To search customer you can also use the searchbox on the top right of the table. the field you may filter are from firstname, lastname, address, email, and customer id.
- <b>ClientId</b>: is your customer reference, it can be automate increment or came from mvno crm.
- <b>ArtiliumID</b>: is an internal id for customer
- <b>Customer</b>: Fillname of the customer, 
- <b>Email</b>: the address where emailing will be used
- <b>Paymentmethod</b>: Method chosen for payments
- <b>Pending Subscriptions</b>: number of pending subscriptions/orders
- <b>Active Subscription</b>: number of active subscription/orders

MVNO can also have posibility to create new Customer directly from this page or export the list of the customer in excel/csv format by using the button on the top right of the table page.

![](img/clients_list.png)

## Client Summary Tab

Once MVNO navigate to one of their clients, the following client summary page will appear:

![](img/clients_summary.jpg)

## Client Subscriptions Tab

This page will shows list of the subscription of the client and also the Recurring billing record from the Billing server (if MVNO opted for Invoicing to be done by United)

![](img/clients_subscription_tab.jpg)

- <strong>ProductName</strong>: Package which assign to the subscription
- <strong>Date Created</strong>: date creation in the database
- <strong>Billing Status</strong>: Status of the billing
- <strong>Recurring Amount</strong>: Amount recurring for postpaid
- <strong>Identifiers</strong>:  Number identifiers (MSISDN)
- <strong>Billing Cycle</strong>: Billing cycle, this wil be most of the time monthly

The second table below will shows the billing records

- <strong>Items</strong>: Id of the billing index pricing
- <strong>Amount</strong>: Amount to be billed (ex VAT)
- <strong>Months</strong>: How many time the recurring will accours
- <strong>Description</strong>: Invoice lines will appear on the invoices
- <strong>Date Execute</strong>:  Number identifiers (MSISDN)
- <strong>Action</strong>: Possibility to stop them when they are not linked to a subcriptions forexample discounts


## Client Invoices Tab

This tabs will show the list of client invoices and creditnotes, if the record is an invoice, when you click it it will open invoices details otherwise it will download a pdf doucment of the creditnotes

![](img/clients_invoices.jpg)

### Client Invoices Details


![](img/clients_invoice_detail.jpg)

When MVNO clicks on an invoice at invoices list page it will allow MVNO:

- Back to client details (this is allow you to go back to client summary page) (top left)
- Download Invoice (top right)
- Download CDR (top right)
- Send Invoice (top right)
- Disable Reminder (top right)
- Apply payment (top right)
- View list of payments at the bottom of the page for current invoice (bottom block)



## Client Payments Tab

List of the payments that have been made for the current customer

![](img/clients_payments.jpg)

Descriptions:
- PaymentNumber: Id of the payment
- Payment date: date of the transaction
- Payment form: Type of the payment
- Amount: Amount of the payment
- Description: Description of the payment
- Remarks: extra remarks for the payments
- Assigned: Status of the payment

## Client Tickets Tab

List of the Tickets that have been made for the current customer

![](img/clients_tickets.png)

Descriptions:
- Ticketid: Id of the Ticket
- Date: date of the ticket created
- Subject: Topic of the ticket
- Department: Department responsible which is assigned
- Status: Status of the tickets ex: Open, Assigned,Closed,etc


## Client Contacts Tab

List of the contacts created for the current customer and functionality to create new contact and edit them :

![](img/clients_contacts.jpg)

Descriptions:
- Customer: Contact name
- Role: Role of the contact
- Email: Email addres of the contact
- Telephone: Telephonenumber of the contact
- Action: MVNO can edit/delete the customer here


### Create new Contact

![](img/clients_contacts_add.png)

### Edit/Delete Contact

![](img/clients_contacts_edit.jpg)

## Client Logs Tab

Once MVNO navigate to one of their clients, the following client summary page will appear:

![](img/clients_summary.jpg)

## Client Notes Tab

Once MVNO navigate to one of their clients, the following client summary page will appear:

![](img/clients_summary.jpg)


## Client Email Logs Tab

Once MVNO navigate to one of their clients, the following client summary page will appear:

![](img/clients_summary.jpg)


# Add New Client 

European portal allow you to create customer only without having to register any subscription beforehand through the portal, rather than just doing it via API.
In the pade MVNO Admin may enter all customers informations, its diffrent per country, where in some country who adopt Prepaid system may added Random string for annonymous customers (please consult your own country regulation it is maybe illegal to do so)

![](img/clients_add.png)

Fileds descriptions

- <b>Relation ID</b>: this fields maybe diasabled if you adopt auto generate customer number, otherwise you will need to fill the reference according to your own CRM.
- <b>VAT</b>: Vat Number is applies
- <b>Companyname</b>: Name of the customer company or organization
- <b>Agent/Reseller</b>: Select your agent otherwise there is always 1 default reseller created for MVNO when reseller is not applicable
- <b>Gender</b>: The Gender of your customer the choice are limited to Male, Female, Others
- <b>Intitial</b>: the initial of customer such as S.P for Shawn Mendez other wise leave it empty (optional)
- <b>Salutation</b>: This fields are free text  example: Mr., Mrs., etc.
- <b>Firstname</b>: Firstname of customer (Mandatory)
- <b>Lastname</b>: Lastname of customer (Mandatory)
- <b>Address</b>: Streetname of the customer billing information
- <b>Housenumber</b>: House or building number
- <b>Alpahbet</b>: alphabet or mailbox of the building
- <b>Postcode</b>: Zipcode
- <b>City</b>: Cityname
- <b>Country</b>: Country resident
- <b>Languages</b>: Interface languages
- <b>Contact Number</b>: Phonenumber
- <b>Email</b>: Email address (will be used for login to selfcare)
- <b>NationalNumber</b>: Idnumber
- <b>Identification Type</b>: Type of ID provided








# List Client Notes

![](img/clients_notes.png)

This Menu will gives list of notes that created per customer level:
- <b>ID</b>: Record id of the notes
- <b>Agent</b>: The admin name who create the record
- <b>Clientid</b>: Id of the client
- <b>Notes</b>: Notes 
- <b>Sticky</b>: If this is Yes then the notes will appear on customer summary page