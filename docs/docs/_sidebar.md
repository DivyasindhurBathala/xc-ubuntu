- Admin Segment

  - [Dashboard](admin.md "Admin Portal - Dashboard")
  - [Clients](clients.md "Admin Portal - Clients")
  - [Agent & Reseller](resellers.md "Admin Portal - Reseller")
  - [Billing](billing.md "Admin Portal - Billing")
  - [Services](services.md "Admin Portal - Services")
  - [Logs](logs.md "Admin Portal - Logs")

