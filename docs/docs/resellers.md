# Agent & Reseller 

Agents or Reseller is a sublevel below Admin which MVNO may use to create a collections of sub admins which can be limited on ordereing, creating customer, provisioning on it assigned customers, this means per reseller can and may only see their own customers and subscriptions of the customers.
![](img/reseller_table.png)

# Add New Agent/Reseller

When you click this button which is on the top right of the page you will be present a modal where you can enter the following informations:
![](img/reseller_add.png)
- Companyname: Companyname of the reseller
- Contactname: Contactname of reseller
- Address1: Address and building number
- Postcode: Zipcode of the resident
- City: City of resident
- Phonenumber: Contact phonenumber of reseller
- Country : Country of resident
- Comission Type: choice of Percentage or Fixed amount
- Commission Value (integer)
- Email: will be used for login credential of the reseller, the password will be sent to the reseller when you hit Submit button.
- Password: auto generate random string

# List of the Reseller (table)

This page will present you the list of your Reseller: 

![](img/reseller_table.png)


- <b>ID</b>: is your customer reference, it can be automate increment or came from mvno crm.
- <b>Agent</b>: is an internal id for customer
- <b>Contact</b>: Fillname of the customer, 
- <b>Email</b>: the address where emailing will be used
- <b>Active Services</b>: Method chosen for payments
- <b>Earnings Current Month</b>: amount that agent generated for Topups, Bundles orders (in it currency)


# Reseller Summary Page

Once you navigate to one of your reseller above you will enter their summary Page: 

![](img/reseller_summary.jpg)


1. <h3>The Reseller information</h3>

	- <b>AgentID</b>
	- <b>AgentName</b>
	- <b>Agent Address</b>
	- <b>Agent Phonenumber</b>
	- <b>Agent Email</b>
	- <b>Agent Commision information</b>

2. <h3>Menu collections of the reseller</h3>

	- <b>Update Agent</b>: Modify Reseller/agent information

	![](img/reseller_edit.png)

	- <b>Export Subscriptions</b>: Export to csv or excel format list of the active subscriptions
	- <b>Send credential</b>: Reset Reseller credential information and send him the new password
	![](img/reseller_reset.png)
	- <b>Suspend Reseller</b>: prevent agent from accessing the reseller portal
	![](img/reseller_suspend.png)
	- <b>Login as reseller</b>: Login to their portal without having to know their password (this is comes in handy when you wish to help them)
	- <b>Assign Bulk Simcard</b>: Upload Bulk simcard from flat file (csv), the format should be:  UK: <i>IMSI,ICCD;PIN1,PUK1,PIN2,PUK2,MSISDN</i> and for BE/NL: <i>MSISDN</i>
	![](img/reseller_upload_sims.png)
	- <b>Assign Product</b>:This menu is essential to be done for every reseller, MVNO needs to allow which products he/she may sells
	![](img/reseller_assign_products.png)
	- <b>Delete Agent</b>:when MVNO wish to delete agent from the system MVNO are required to assign the current active clients, and subscription to another reseller in this case MVNo may always choose Default reseller.
	![](img/reseller_delete.png)
 
3. <h3>List of the subscriptions of the current reseller</h3>

	- ClientID: Client reference ID
	- Product: Product of the subscriptions
	- Contact: Name of the customer 
	- Status: Status of provisoning of the simcard
	- Amount: Amount recorded, this does not mean the current recurring amount when its is a prepaid platform
	- Identifier: The number of the msisdn


4. <h3>List of simcard of the current reseller</h3>

	- SimcardNumber
	- Status
	- Imported by
	- Type import

5. <h3>Statistics button</h3>

When this button clicked it will slide a new page on top of it:

![](img/reseller_stats.jpg)

- Number of activated today
- Subscription activated in the last 7 days
- Subscription activated in the last 30 days
- Subscription activated in the current year

- Topup ordered today with the amount in currency
- Topup ordered today with the amount in the last 7 days
- Topup ordered today with the amount in the last 30 days
- Topup ordered today with the amount in the current year




