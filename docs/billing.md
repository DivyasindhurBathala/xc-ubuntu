# Billing  

This Menu segment has 3 sub menu: Client list, Add new Client, List client notes

# Invoice List

![](img/billing_invoices_list.png)

1. Menu top Right
    - Export Invoices: MVNO can export their invoices in to a flat file such as .csv or .xlsx
    - Show Invoice Overdude: This button will show only invoices list which are overdue

2. Invoice performance chart
    - This chart will shows All invoices, Paid, Unpaid, Credited
    - this will show only for monthly performance
3. Table of Invoices
    - Invoicenumber
    - Billing ID
    - Invoice Date
    - Invoice Duedate
    - Amount (in the currency configured for MVNO)
    - Status of Invoice (Paid/Unpaid)

4. Records informations
5. Paginations


# Proforma List

![](img/billing_proformas_list.png)

1. Table of Invoices
    - Invoicenumber
    - Customer name
    - Proforma Date
    - Proforma Duedate
    - Amount (in the currency configured for MVNO)
    - Status of Invoice (Paid/Unpaid)

# Creditnotes List

![](img/billing_cn_list.png)

1. Table of Creditnotes
    - Number
    - Customer name
    - Proforma Date
    - Proforma Duedate
    - Amount (in the currency configured for MVNO)
    - Status of Invoice (Paid/Unpaid)
    - Download Pdf or  Send to customer


# Bank Transactions 

## Bank transaction items

![](img/billing_bt_list.png)

1. On this block MVNO may see last uploaded files. this file provided by MVNO either via FTP or Upload button
2. Table of Creditnotes
    - ID
    - Date Transactions
    - Code
    - Amount
    - IBAN
    - Message
    - Name


## Bank transaction Unassign

![](img/billing_bt_unassign.png)

1. Table of Creditnotes
    - PaymentNbr / Mark refund
        - If MVNO has refund it to their customer this action is required to refund it in the portal billing side
        ![](img/billing_bt_refund.png)
    - BillingID
    - Payment Date
    - Payment Amount
    - Amount Used
    - Amount Left
    - Description
    - Customer reference ID (MVNOID)
    - Payment Type
    - Customer Name
    - Actions
        - Assign to invoice

## SEPA Bank transaction Rejections

![](img/billing_bt_rejects.png)

1. Table of Creditnotes
    - Number
    - Customer name
    - Proforma Date
    - Proforma Duedate
    - Amount (in the currency configured for MVNO)
    - Status of Invoice (Paid/Unpaid)
    - Download Pdf or  Send to customer

# Online Transactions

![](img/billing_bt_online.png)

1. Table of Creditnotes
    - Number
    - Customer name
    - Proforma Date
    - Proforma Duedate
    - Amount (in the currency configured for MVNO)
    - Status of Invoice (Paid/Unpaid)
    - Download Pdf or  Send to customer


# Invoice Reminder Logs

![](img/billing_bt_reminder.png)

1. Table of Creditnotes
    - Number
    - Customer name
    - Proforma Date
    - Proforma Duedate
    - Amount (in the currency configured for MVNO)
    - Status of Invoice (Paid/Unpaid)
    - Download Pdf or  Send to customer
