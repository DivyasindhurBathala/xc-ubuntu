<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR | E_PARSE);

require_once 'assets/sso/lib/_autoload.php';
require_once 'assets/sso/db.php';
$db = new MysqliDb(array(
    'host' => '172.16.100.252',
    'username' => 'mvno',
    'password' => 'Mvno2018!',
    'db' => 'mvno',
    'port' => 3306,
    'charset' => 'utf8'));
$auth = new SimpleSAML_Auth_Simple('mijndelta-sp');


if (!empty($_GET['login'])) {
    $auth->requireAuth(array('saml:idp' => 'urn:idp:zeelandnet2016'));
    $attributes = $auth->getAttributes();
    $a = $auth->getAuthData("saml:sp:NameID");
    if (!empty($attributes)) {
        $email = $attributes['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress'][0];
        if ($a->value) {
            $db->where("username", strtolower(trim($a->value)));
            $user = $db->getOne("a_clients_sso");
        
            if ($user) {
                $client = api(array('id' => $user['userid']));
             
              //  echo "user found ".siteURL() . 'client/auth/sso_login?key=' . $client->key;
             
                header('Location: ' . siteURL() . 'client/auth/sso_login?key=' . $client->key);
            } else {
                echo "user not found";
                $key = base64_encode(json_encode(array("account" => $a->value, "email" => $email)));
                header('Location: ' . siteURL() . 'client/auth/sso_register?key=' . $key);
            }
        } else {
            echo "Username can not be attained";
        }
    } else {
        echo "attribute was not valid";
    }
} elseif (!empty($_GET['logout'])) {
    $url = $auth->getLogoutURL();

    header('Location: ' . $url);

    print('<a href="' . htmlspecialchars($url) . '">Logout Completely</a>');
} else {
    if ($auth->isAuthenticated()) {
        $url = $auth->getLogoutURL();
        echo '<script>
window.location.href = "' . $url . '";
</script>';
        print('<a href="' . htmlspecialchars($url) . '">Logout Completely</a>');
    } else {
        header('Location: ' . siteURL() . 'client');
    }
}

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'] . '/';
    return $protocol . $domainName;
}

function api($postfields)
{
    $whmcsUrl = siteURL() . 'client/autoauth/namorambe';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    $response = curl_exec($ch);
    if (curl_error($ch)) {
        die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);

    $jsondata = json_decode($response);
    return (object) $jsondata;
}
