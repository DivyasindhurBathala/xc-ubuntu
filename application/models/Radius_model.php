<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Radius_model extends CI_Model
{

    public function insertUser($data, $realm = "united")
    {
        print_r($data);
    	$this->db =$this->load->database(strtolower($realm), true);
    	$this->db->insert('radusergroup', array('username' => $data['username'], 'priority' => 1, 'groupname' => 'standard_profile'));
    	$this->db->insert('radcheck', $data);
    	return $this->db->insert_id();


    }
    public function getInvoices($next){
             $this->db =$this->load->database('magebo', true);
          $sql = "SELECT TOP 10000 b.iCompanyNbr, a.iInvoiceNbr, a.iAddressNbr, a.iInvoiceType, CONVERT(char(10), a.dInvoiceDate,126) as dInvoiceDate, CONVERT(char(10), a.dInvoiceDueDate,126) as dInvoiceDueDate, a.mInvoiceAmount, a.tRemark, a.iInvoiceStatus, a.iPrintType, a.bBookkeepingLock, a.dLastUpdate, a.iLastUserNbr from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.iInvoiceNbr > ".$next." order by a.iInvoiceNbr,a.iAddressNbr ASC";
            $q = $this->db->query($sql);

        return $q->result_array();

    }
      

     public function updateUser($username, $data,$realm = "united")
    {
    $this->db =$this->load->database(strtolower($realm), true);
	$this->db->where('username', $username);
	$this->db->update('radcheck', $data);
    }

     public function deleteUser($username,$realm = "united")
    {
    	$this->db =$this->load->database(strtolower($realm), true);
    	$this->db->where('username', $username);
		$this->db->delete('radcheck');

    }

     public function suspendUser($username,$realm = "united")
    {
    	$this->db =$this->load->database(strtolower($realm), true);
    $this->db->where('username', $username);
	$this->db->update('radcheck', array('password' => md5(rand(1000000,900000000))));
    }

     public function UnsuspendUser($username, $password,$realm = "united")
    {
    	$this->db =$this->load->database(strtolower($realm), true);
    	 $this->db->where('username', $username);
	$this->db->update('radcheck', array('password' =>$password));
    }

    public function changePassword($username, $password,$realm = "united")
    {
    	$this->db =$this->load->database(strtolower($realm), true);
    		 $this->db->where('username', $username);
	$this->db->update('radcheck', array('password' =>$password));

    }

    public function addFixIp($username,$realm = "united")
    {
    	$this->db =$this->load->database(strtolower($realm), true);
    	$this->db->insert('radreply', array('username' => $username, 'attribute' => 'Framed-IP-Address', 'op' => '=', 'value' => $this->getLastIP()));

    }

    public function DeleteFixIP($username,$realm = "united")
    {
    	$this->db =$this->load->database(strtolower($realm), true);
    	$this->db->where('username', $username);
    	$this->db->delete('radreply');

    }


     public function getusage($username,$month=false,$realm = "united")
    {
    	if(!$month){
    		$month = date('Y-m');
    	}
    	$this->db =$this->load->database(strtolower($realm), true);
    	$q = $this->db->query("select sum(acctinputoctets) as incoming, sum(acctoutputoctets) as outgoing from radacct where acctstarttime like ? and username=?", array($month.'%', $username));


    	if($q->num_rows()>0){

    		return $q->row_array();
    	}else{

    		return array('incoming' => 0, 'outgoing' => 0);
    	}

    }

    function getLastIP($realm="united"){

    	$this->db =$this->load->database(strtolower($realm), true);
    	$q = $this->db->query("select * from radreply order by id desc limit 1");
    	$ips =explode('.',$q->row()->value);
    	return $ips[0].".".$ips[1].".".$ips[2].".".$ips[3]+1;

    }

    public function getInternetStatus($username, $realm){

        $this->db =$this->load->database(strtolower($realm), true);

        $q = $this->db->query("select username,value as password from radcheck where username=?", array($username));
        $ses =  $this->db->query("select  * from radacct where username=? order by radacctid desc LIMIT 12", array($username));
        $last_out = $this->db->query("select  * from radpostauth where username=? order by id desc LIMIT 1", array($username));

        if($q->num_rows()>0){
            $a = $q->row_array();
            if($ses->num_rows()>0){
                if(!$ses->row()->acctstoptime){
                    $a['offer'] = $ses->row()->connectinfo_start;
                     $a['status'] = 'ONLINE';
                      $a['ipaddress'] = $ses->row()->framedipaddress;
                }else{
                     $a['offer'] = "Unknown";
               
                 $a['status'] = 'OFFLINE';
                   $a['ipaddress'] = 'NA';
                }
                $a['sessions'] = $ses->result();

                if($last_out->num_rows()>0){
                     $a['last_auth'] = $last_out->row()->authdate;
                    $a['last_auth_status'] = $last_out->row()->reply;
                }else{
                   $a['last_auth'] = 'NA';
                 $a['last_auth_status'] = 'NA';  
                }
            }else{
                $a['offer'] = "Unknown";
                $a['sessions'] = false;
                $a['status'] = 'OFFLINE';
                $a['ipaddress'] = 'NA';
                $a['last_auth'] = 'NA';
                $a['last_auth_status'] = 'NA';
                $a['sessions'] = false;
            }
          
            return (object) $a;
        }else{

            return false;
        }


    }

}