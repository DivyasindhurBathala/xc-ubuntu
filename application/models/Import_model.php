<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Import_model extends CI_Model
{

    public function getClients()
    {
        $today = date('Y-m-d H:i:s');
        $this->db = $this->load->database('import', true);
        $q = $this->db->query("select id,sso as sso_id, companyid,email,password,companyname,city,firstname,lastname,address1,postcode,country,language,phonenumber,'directdebit' as paymentmethod,deltaid as mvno_id,datecreated as date_created,? as date_modified,status
 from tblclients", array($today));
        foreach ($q->result_array() as $res) {
            $client[] = array_merge($res, $this->getClientCustomfields($res['id']));
        }

        return $client;
    }

    public function getClientCustomfields($id)
    {
        $res = array('salutation' => '', 'vat' => '', 'iban' => '', 'bic' => '', 'mageboid' => '', 'vat_exempt' => '0');
        $this->db = $this->load->database('import', true);
        $q = $this->db->query("select a.*,b.fieldname from tblcustomfieldsvalues a left join tblcustomfields b on b.id=a.fieldid  where a.relid=?", array($id));
        foreach ($q->result() as $row) {
            if (str_replace(' ', '', $row->fieldname) == "VAT") {
                $res['vat'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "IBANNumber") {
                $res['iban'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "BIC") {
                $res['bic'] = $row->value;
            }

            if (str_replace(' ', '', $row->fieldname) == "iAddressNbr") {
                $res['mageboid'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "VATExempt") {
                $res['vat_exempt'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "DateOfBirth") {
                $res['date_birth'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "Salutation") {
                $res['salutation'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "Initial") {
                $res['initial'] = $row->value;
            }
        }

        return $res;
    }
    public function updateDonor($id, $mvno_id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_services_mobile', array('donor_accountnumber' => $mvno_id));
        return $this->db->affected_rows();
    }
    public function getServices($companyid)
    {
        $today = date('Y-m-d H:i:s');
        $this->db = $this->load->database('import', true);
        $q = $this->db->query("select '" . $companyid . "' as companyid,a.id,'mobile' as type ,a.domainstatus as status ,(select b.addonid from tblhostingaddons b left join tbladdons c on c.id=b.addonid where c.name like 'Delta%' and b.hostingid=a.id) as packageid  ,userid ,regdate as date_created,a.amount as recurring,a.notes from tblhosting a
ORDER BY `packageid`  ASC", array($today));

        foreach ($q->result_array() as $res) {
            $service[] = array_merge($res, $this->getServiceCustomfields($res['id']));
        }

        return $service;
    }

    public function getServiceCustomfields($id)
    {
        $res = array('services' => array('serviceid' => '', 'msisdn' => '', 'msisdn_puk1' => '', 'msisdn_puk2' => '', 'msisdn_status' => '', 'msisdn_sn' => '0', 'donor_msisdn' => '', 'donor_provider' => '', 'donor_accountnumber' => ''), 'date_contract' => '');
        $this->db = $this->load->database('import', true);
        $q = $this->db->query("select a.*,b.fieldname from tblcustomfieldsvalues a left join tblcustomfields b on b.id=a.fieldid  where a.relid=?", array($id));

        foreach ($q->result() as $row) {
            $res['services']['serviceid'] = $id;
            if (str_replace(' ', '', $row->fieldname) == "MSISDN") {
                $res['services']['msisdn'] = trim($row->value);
            }
            if (str_replace(' ', '', $row->fieldname) == "SIMNbr") {
                $res['services']['msisdn_sim'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "PUK1") {
                $res['services']['msisdn_puk1'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "PUK2") {
                $res['services']['msisdn_puk2'] = $row->value;
            }

            if (str_replace(' ', '', $row->fieldname) == "SimStatus") {
                if (empty($row->value)) {
                    $res['services']['msisdn_status'] = 'OrderWaiting';
                } else {
                    $res['services']['msisdn_status'] = $row->value;
                }
            }

            if (str_replace(' ', '', $row->fieldname) == "SN") {
                $res['services']['msisdn_sn'] = trim($row->value);
            }
            if (str_replace(' ', '', $row->fieldname) == "PortMSISDN") {
                $res['services']['donor_msisdn'] = trim($row->value);
            }

            if (str_replace(' ', '', $row->fieldname) == "PortDonorSim") {
                $res['services']['donor_sim'] = $row->value;
            }

            if (str_replace(' ', '', $row->fieldname) == "PortDonorOperator") {
                $res['services']['donor_provider'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "PortAccountNumber") {
                $res['services']['donor_accountnumber'] = $row->value;
            }

            if (str_replace(' ', '', $row->fieldname) == "PortInWishDate") {
                $res['services']['date_wish'] = $row->value;
            }
            if (str_replace(' ', '', $row->fieldname) == "Porting") {
                if ($row->value == "on") {
                    $res['services']['msisdn_type'] = 'porting';
                } else {
                    $res['services']['msisdn_type'] = 'new';
                }
            }
            if (str_replace(' ', '', $row->fieldname) == "ContractDate") {
                $res['date_contract'] = $row->value;
            }
        }

        return $res;
    }

    public function getProducts()
    {
        $today = date('Y-m-d H:i:s');
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,	a.name,	b.monthly as recurring_total, '21' as	recurring_taxrate, '' as notes from tbladdons a left join tblpricing b on a.id=b.relid and b.type='addon' where a.id in ('6', '7', '9', '10', '11', '13', '34', '35', '36', '37', '40', '43')");
        foreach ($q->result_array() as $res) {
            $res['recurring_subtotal'] = exvat4('21', $res['recurring_total']);
            $res['recurring_tax'] = vat4('21', $res['recurring_total']);
            $service[] = $res;
        }

        return $service;
    }

    public function getTarif()
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,	a.name,	b.monthly as recurring_total, 'tarif' as bundle_type, '21' as recurring_taxrate  from tbladdons a left join tblpricing b on a.id=b.relid and b.type='addon' where a.id in ('33', '25', '31', '29', '26', '30', '38', '27', '33', '32', '39', '28')");
        foreach ($q->result_array() as $res) {
            $res['recurring_subtotal'] = exvat4('21', $res['recurring_total']);
            $res['recurring_tax'] = vat4('21', $res['recurring_total']);
            $service[] = $res;
        }

        return $service;
    }

    public function getAddon()
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,	a.name,	b.monthly as recurring_total, 'option' as bundle_type, '21' as recurring_taxrate  from tbladdons a left join tblpricing b on a.id=b.relid and b.type='addon' where a.id in ('14', '15', '16', '44')");
        foreach ($q->result_array() as $res) {
            $res['recurring_subtotal'] = exvat4('21', $res['recurring_total']);
            $res['recurring_tax'] = vat4('21', $res['recurring_total']);
            $service[] = $res;
        }

        return $service;
    }

    /* if($li['product']=='Delta Mobiel 300Mb'){
    LogActivity('Delta Mobiel 300Mb.....'.print_r($li,true));
    $addonids=array(33);
    $serviceids = array($serviceid);
    }

    if($li['product']=='Delta Mobiel 1,25 GB'){
    LogActivity('Delta Mobiel 1,25 GB.....'.print_r($li,true));
    $addonids=array(25,31,29);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }
    if($li['product']=='Delta Mobiel 2,5 GB'){
    LogActivity('Delta Mobiel 2,5 GB.....'.print_r($li,true));
    $addonids=array(26,31,30);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }
    if($li['product']=='Delta Mobiel 3 GB'){
    LogActivity('Delta Mobiel 3 GB.....'.print_r($li,true));
    $addonids=array(38,31,30);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }
    if($li['product']=='Delta Mobiel 5 GB'){
    LogActivity('Delta Mobiel 5 GB.....'.print_r($li,true));
    $addonids=array(27,31,30);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }
    if($li['product']=='Delta Mobiel 300 MB - Combi'){
    LogActivity('Delta Mobiel 300 MB - Combi.....'.print_r($li,true));
    $addonids=array(33);
    $serviceids = array($serviceid);
    }
    if($li['product']=='Delta Mobiel 2,5 GB - Combi'){
    LogActivity('Delta Mobiel 2,5 GB - Combi.....'.print_r($li,true));
    $addonids=array(26,32,29);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }

    if($li['product']=='Delta Mobiel 3 GB - Combi'){
    LogActivity('Delta Mobiel 3 GB - Combi.....'.print_r($li,true));
    $addonids=array(38,32,29);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }
    if($li['product']=='Delta Mobiel 5 GB - Combi'){
    LogActivity('Delta Mobiel 5 GB - Combi.....'.print_r($li,true));
    $addonids=array(27,32,30);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }
    if($li['product']=='Delta Mobiel 6 GB - Combi'){
    LogActivity('Delta Mobiel 6 GB - Combi.....'.print_r($li,true));
    $addonids=array(39,32,30);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }
    if($li['product']=='Delta Mobiel 10 GB - Combi'){
    LogActivity('Delta Mobiel 10 GB - Combi'.print_r($li,true));
    $addonids=array(28,32,30);
    $serviceids = array($serviceid,$serviceid,$serviceid);
    }
     */

    public function getHostingId($row)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select id,domain,username,domainstatus from tblhosting where domain like ?", array('%' . trim($row[3]) . '%'));
        if ($q->num_rows() > 0) {
            $res = $q->row_array();
            $res['contractdate'] = $this->getContractDate($q->row()->id);

            return (object) $res;
        } else {
            $s = $this->db->query("select id,domain,username,domainstatus from tblhosting where username like ?", array('%' . trim($row[3]) . '%'));
            if ($s->num_rows() > 0) {
                $res = $s->row_array();
                $res['contractdate'] = $this->getContractDate($s->row()->id);

                return (object) $res;
            } else {
                return (object) array();
            }
        }
    }

    public function getContractDate($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from tblcustomfieldsvalues where relid = ? and fieldid=?", array($id, '42'));
        if ($q->num_rows() > 0) {
            return $q->row()->value;
        } else {
            return false;
        }
    }

    public function replacecontractid($data)
    {

        //$this->db = $this->load->database('local', true);
        //$this->db->replace('contractdate', $data);
        /*
        $h = "delete from tblcustomfieldsvalues where fieldid='42' and relid='" . $data['serviceid'] . "';\n";
        $h .= "insert into tblcustomfieldsvalues (fieldid, value, relid) values ('42','" . $data['contractdate'] . "','" . $data['serviceid'] . "');\n";

        return $h;
         */
        return "REPLACE INTO `contractdate` (`serviceid`, `contractdate`) VALUES ('" . $data['serviceid'] . "', '" . $data['contractdate'] . "');";
    }

    public function delcontract($id)
    {
        $this->db = $this->load->database('local', true);
        $this->db->where('serviceid', $id);
        $this->db->delete('contractdate');
        return $this->db->affected_rows();
    }
    public function DeleteClients($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $id);
        $this->db->delete('a_clients');
    }

    public function DeleteServices1($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $id);
        $this->db->delete('a_services');
    }

    public function DeleteServices2($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $id);
        $this->db->delete('a_services_mobile');
    }
    public function insert_mobile($array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_services_mobile', $array);
    }

    public function insert_service($array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_services', $array);
        return $this->db->insert_id();
    }
    public function getMageboClients($companyid, $limit, $iAddressNbr)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->select('iAddressNbr');
        $this->db->where('iCompanyNbr', $companyid);
        $this->db->where('iAddressNbr >', $iAddressNbr);
        $this->db->order_by('iAddressNbr', 'asc');
        $this->db->limit($limit);
        $q = $this->db->get('tblAddress');
        return $q->result();
    }

    public function getLASTinvoices($companyid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from magebo_invoices where iCompanyNbr=? order by iInvoiceNbr desc limit 1", array($companyid));
        if ($q->num_rows()>0) {
            return $q->row()->iInvoiceNbr;
        } else {
            return 0;
        }
    }

    public function getInvoices($companyid, $lastinvoice, $limit)
    {
        $this->db = $this->load->database('magebo', true);
        $query ="select TOP ".$limit." a.*,b.iCompanyNbr from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where b.iCompanyNbr=".$companyid." and a.iInvoiceNbr > ".$lastinvoice." order by iInvoiceNbr asc";
        $q = $this->db->query($query);
        if ($q->num_rows()>0) {
            return $q->result_array();
        } else {
            return 0;
        }
    }

    public function BatchInsert($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert_batch('magebo_invoices', $data);
    }
}
