<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Client_model extends CI_Model
{
    private $companyid = COMPANYID;
    public function getStats($id)
    {
        $invoices = $this->getInvoices();

        $invoice_amountpaid = array();
        $invoice_amountunpaid = array();
        $invoice_amountsepa = array();
        $active_service = 0;
        $pending_service = 1;
        //print_r($invoices);
        foreach ($invoices as $invoice) {
            if ($invoice['iInvoiceStatus'] == "52") {
                $invoice_amountunpaid[] = $invoice['mInvoiceAmount'];
            } elseif ($invoice['iInvoiceStatus'] == "53") {
                $invoice_amountunpaid[] = $invoice['mInvoiceAmount'];
            } elseif ($invoice['iInvoiceStatus'] == "54") {
                $invoice_amountpaid[] = $invoice['mInvoiceAmount'];
            }
        }

        foreach ($this->getHosting() as $key => $value) {
            if ($value->status == "Active") {
                $active_service++;
            } elseif ($value->status == "Pending") {
                $active_service++;
            }
        }
        $services = $this->getServices($id);
        return (object) array('invoice_amountpaid' => array_sum($invoice_amountpaid),
            'invoice_amountunpaid' => array_sum($invoice_amountunpaid),

            'active_service' => $active_service,
            'pending_service' => $pending_service,
            'services' => $services);
    }

    public function getStats_mobile($id, $mageboid)
    {
        $invoices = $this->getInvoices_mobile($mageboid);
        $invoice_amountpaid = array();
        $invoice_amountunpaid = array();
        $invoice_amountsepa = array();
        $active_service = 0;
        $pending_service = 1;
        //print_r($invoices);
        foreach ($invoices as $invoice) {
            if ($invoice['iInvoiceStatus'] == "52") {
                $invoice_amountunpaid[] = $invoice['mInvoiceAmount'];
            } elseif ($invoice['iInvoiceStatus'] == "53") {
                $invoice_amountsepa[] = $invoice['mInvoiceAmount'];
            } elseif ($invoice['iInvoiceStatus'] == "54") {
                $invoice_amountpaid[] = $invoice['mInvoiceAmount'];
            }
        }

        $services = $this->getServices($id);
        return (object) array('invoice_amountpaid' => array_sum($invoice_amountpaid),
            'invoice_amountunpaid' => array_sum($invoice_amountunpaid),
            'invoice_amountunsepa' => array_sum($invoice_amountsepa),
            'service' => $services);
    }

    public function getProforma($id)
    {
        $amount = array();
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_invoices  where userid=? and status = 'Unpaid'", array($id));
        if ($q->num_rows() > 0) {

            foreach ($q->result() as $r) {
                $amount[] = $r->total;
            }

            return (object) array('data' => $q->result(), 'total' => array_sum($amount));

        } else {
            return false;
        }
    }
    public function getPassword($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select password from a_clients  where id=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row()->password;
        } else {
            return false;
        }
    }
    public function whmcs_api($postfields)
    {
        $whmcsUrl = WHMCS_URL;
        $postfields['username'] = "admin";
        $postfields['password'] = md5("un!t3d");
        $postfields['responsetype'] = 'json';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS Prod ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        //print_r($response);
        //exit;
        $jsondata = json_decode($response);
        return (object) $jsondata;
    }
    public function getCurrentConsumtion($mageboid)
    {
        $result = array();
        $sum = array();
        if ($mageboid) {
            $result = $this->mageboGet('GetCdrsUnbilled/' . $mageboid);
        }

        //echo $mageboid;
        //exit;
        if (!empty($result)) {
            foreach ($result as $r) {
                $sum[] = $r->kost;
            }

            return array_sum($sum);
        } else {
            return "0.00";
        }
    }
    public function getService($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.name,a.amount as recurring, b.servertype from tblhosting a left join tblproducts b on b.id=a.packageid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return array();
        }
    }
    public function getServices($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services where userid=? and status IN('Active','Suspended')", array($id));

        return $q->num_rows();
    }
    public function IsAllowedInvoice($id, $mageboid)
    {

        $this->db = $this->load->database('magebo', true);

        $q = $this->db = $this->db->query('select a.* from tblInvoice a where iAddressNbr=? and iInvoiceNbr=? and iInvoiceType=?', array($mageboid, $id, 40));

        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function IsAllowedCn($id, $mageboid)
    {

        $this->db = $this->load->database('magebo', true);

        $q = $this->db = $this->db->query('select a.* from tblInvoice a where iAddressNbr=? and iInvoiceNbr=? and iInvoiceType=?', array($mageboid, $id, 41));

        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function getInvoicePdf($id)
    {
        $this->db = $this->load->database('invoices', true);
        $this->db->select('*');
        $this->db->where('iInvoiceNbr', $id);
        $q = $this->db->get('cs_invoices');
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function IsAllowedMobile($msisdn, $userid)
    {

        $mobile = $this->mageboGet('GetSim/' . trim($msisdn));
        if (in_array($mobile->iCompanyNbr, $userid)) {
            return true;
        } else {
            return false;
        }
    }
    public function getClientid($uuid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where uuid like ?", array($uuid));
        if ($q->num_rows() > 0) {
            return $q->row()->id;
        } else {
            return false;
        }
    }
    public function getInvoices()
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT a.iInvoiceNbr,a.iAddressNbr,a.dInvoiceDate,a.dInvoiceDueDate,a.iInvoiceStatus,b.cName,a.mInvoiceAmount
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE b.iCompanyNbr =?
    and a.iAddressNbr = ?
    and a.iInvoiceType=?
    ORDER BY a.iInvoiceNbr DESC", array($this->companyid, $_SESSION['client']['mageboid'], '40'));

        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }

    public function getInvoices_mobile($mageboid)
    {
        //52 = Unpaid
        //53 = Presented Sepa
        //54 = Paid
        $this->db = $this->load->database('magebo', true);
        $this->db->select('*');
        $this->db->where_in('iAddressNbr', $mageboid);
        $this->db->where('iInvoiceType', 40);
        $q = $this->db->get('tblInvoice');
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }

    public function getHosting()
    {
        //52 = Unpaid
        //53 = Presented Sepa
        //54 = Paid
        $this->db = $this->load->database('default', true);
        $this->db->select('*');
        $this->db->where('userid', $_SESSION['client']['id']);
        $q = $this->db->get('a_services');
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
/*
public function getHosting_mobile($id)
{
//52 = Unpaid
//53 = Presented Sepa
//54 = Paid
$this->db = $this->load->database('default', true);
$this->db->select('*');
$this->db->where('userid', $id);
$q = $this->db->get('a_services');
if ($q->num_rows() > 0) {

return $q->result();

} else {
return array();
}
}

public function mageboGet($query)
{

$l = explode('/', $query);

if ($l[0] == "GetSim") {
$this->db = $this->load->database('magebo', true);
$q = $this->db->query("select A.iAddressNbr, A.cName, A.cStreet,A.iCompanyNbr,Y.cTypeDescription as PaymentType,
CASE when A.iCountryIndex = 22
then (C.cCountryCode + '-' + Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix)
else (C.cCountryCode + '-' +
CASE when Cast(A.iZipCode AS varchar(10)) is null
then ''+ A.cZipSuffix
else Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix end  ) end as ZipCode,
A.cCity, P.cInvoiceReference, S.cSIMCardNbr as SIMCardNbr, S.iMSISDN as MSISDN, S.*, X.cTypeDescription as Type,Z.cTypeDescription as SimCardType, B.cInternalLanguage as Language
from tblC_SIMCard S
left join tblC_Pincode P ON P.iPincode = S.iPincode
left join tblLocation L ON L.iLocationIndex = P.iLocationIndex
left join tblAddress A ON A.iAddressNbr = L.iAddressNbr
left join tblType Y ON Y.iTypeNbr=S.iPaymentType
left join tblType X ON X.iTypeNbr=S.iMSISDNType
left join tblType Z ON Z.iTypeNbr=S.iSimCardType
left join tblLanguages B on B.iBaseVoiceMailLanguage=S.iVoiceMailLanguage
, tblCountry C
where S.iPincode = ?
AND S.bActive = 1
AND (C.iCountryIndex = A.iCountryIndex)", array(trim($l[1])));
return $q->row();

} else {

$i = getUnityUsername(COMPANYID);
//print_r($i);
if (empty($i)) {
die('Problem when finding the companyid');
}

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, MAGEBO_URL . $query);
curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout after 30 seconds
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
$result = curl_exec($ch);
if (curl_error($ch)) {
return (object) array('name' => 'TimeoutError', 'errorno' => curl_errno($ch), 'message' => curl_error($ch));
} else {
curl_close($ch);
return json_decode($result);
}
//echo $result;

}
}
public function mageboPost($postfields)
{
$companyid = $postfields['companyid'];
$action = $postfields['action'];
$type = $postfields['type'];
unset($postfields['action']);
unset($postfields['type']);
unset($postfields['companyid']);
$i = getUnityUsername($companyid, $type);

if (empty($i)) {
die('Username and Password could not be find in tblcompanydata');
}

//print_r($i);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, MAGEBO_URL . $action . '/');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 300);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
$response = curl_exec($ch);
//echo $response;
if (curl_error($ch)) {
die('Unable to connect to API Server: ' . curl_errno($ch) . ' - ' . curl_error($ch) . ' ' . $action);
}
curl_close($ch);

$jsondata = json_decode($response);
return $jsondata;
}
public function artiliumGet($query, $type, $companyid)
{

$i = getUnityUsername(trim($companyid), trim($type));
//echo $query . ' ' . $type . ' ' . $companyid;
if (empty($i)) {
die('Error: getUnityUsername for artiliumGet/' . $query);
}
//print_r($i);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, ARTA_URL . $query);
curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//curl_setopt($ch, CURLOPT_VERBOSE, true);f
curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
$result = curl_exec($ch);

if (curl_error($ch)) {
die('Unable to connect API Server: ' . curl_errno($ch) . ' - ' . curl_error($ch) . ' - ' . $query);
}
curl_close($ch);
return json_decode($result);
}
public function artiliumGetGlobal($query, $companyid)
{
$res = array();
$r = getUnityUsernames($companyid);

if (empty($r)) {
die('Companyid not found in tblcompanydata');
}

if ($companyid == 53) {
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, ARTA_URL . $query . $r[0]->m_username);
curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($r[0]->unity_username . ":" . $r[0]->unity_password)]);
$result = curl_exec($ch);
//print_r($result);
if (curl_error($ch)) {
die('Unable to connect to API Server: ' . curl_errno($ch) . ' - ' . curl_error($ch));
}
curl_close($ch);
$res = json_decode($result, true);

//print_r($res);

return $res['Porting'];
} else {
foreach ($r as $i) {
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, ARTA_URL . $query . $i->m_username);
curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
$result = curl_exec($ch);
//print_r($result);
if (curl_error($ch)) {
die('Unable to connect to API Server: ' . curl_errno($ch) . ' - ' . curl_error($ch));
}
curl_close($ch);
$res[] = json_decode($result, true);
unset($result);
}
//print_r($res);
if (count($r) > 1) {
return array_merge($res[0]['Porting'], $res[1]['Porting']);
} else {
return $res[0]['porting'];
}
}
}
public function artiliumPost($postfields)
{
$companyid = $postfields['companyid'];
$action = $postfields['action'];
$type = $postfields['type'];
unset($postfields['action']);
unset($postfields['type']);
unset($postfields['companyid']);
$i = getUnityUsername($companyid, $type);

if (empty($i)) {
die('Username and Password could not be find in tblcompanydata');
}

//print_r($i);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, ARTA_URL . $action . '/');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 300);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
$response = curl_exec($ch);
//echo $response;
if (curl_error($ch)) {
die('Unable to connect to API Server: ' . curl_errno($ch) . ' - ' . curl_error($ch));
}
curl_close($ch);

$jsondata = json_decode($response);
return $jsondata;
}
 */

    public function donotasksso($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $_POST['userid']);
        $this->db->update('a_clients', array('sso_id' => 1));
        return $this->db->affected_rows();
    }

    public function insertDeltaUsername($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->replace('a_clients_sso', $data);
    }

    public function changePassword($id, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $data['id']);
        $this->db->update('a_clients', array('password' => password_hash(trim($data['password1']), PASSWORD_DEFAULT)));
    }

    public function is_loged($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('clientid', $id);
        $q = $this->db->get('a_client_login');
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function RemoveFirstlogin($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_client_login', array('clientid' => $id));
    }

    public function getTicket($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,a.priority,a.tid,a.subject,a.userid,a.status,a.date,a.deptid,admin,a.categoryid,
    case when a.userid  is null THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,a.admin as staf,c.name as deptname,concat(d.firstname,' ', d.lastname) as assigned, e.name as categoryname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category e on e.id=a.categoryid
    left join a_helpdesk_department c on c.id=a.deptid where a.id=? and a.companyid=?
    and a.userid=?", array($id, $this->session->cid, $_SESSION['client']['id']));

        if ($q->num_rows() > 0) {
            $res = $q->row_array();
            $res['replies'] = $this->getreplies($id);
            return $res;
        } else {
            return false;
        }
    }

    public function getreplies($id)
    {
        $this->db = $this->load->database('default', true);
        $res = array();
        $q = $this->db->query("select a.*
from a_helpdesk_ticket_replies
a where a.ticketid=? order by id asc", array($id));
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                if (!empty($row['attachment'])) {
                    $t = explode('|', $row['attachment']);
                    $row['attachments'] = $t;
                    unset($t);
                }
                $res[] = $row;
            }

            return $res;
        } else {
            return array();
        }
    }
}
