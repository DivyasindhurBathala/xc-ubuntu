<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function getStatistic($cid = false)
    {
        $client = $this->getClientCount();

        if (!$cid) {
            $cid = $this->session->cid;
        }
        $this->db = $this->load->database('default', true);
        if ($cid == "2") {
            return array('client' => $client,
            'total_unpaid' => 0,
            'count_unpaid' => '0',
            'count_paid' => '0',
            'total_amount_unpaid' => "0.00",
            'total_this_year_paid' => "0.00",
            'total_this_year_unpaid' => "0.00",
            'total_thismonthpaid' => "0.00",
            'total_thismonthunpaid' => "0.00",
            'total_thismonth_amount_unpaid' => "0.00",
            'total_thismonth_amount_paid' => "0.00",
            'percentpaid' => 0,
            'percentunpaid' => 0,
            'tickets' =>  0,
            'subscriptions' => array('active' => 0, 'pending' => 0, 'new' => 0),
            'reminder1' => 0,
            'reminder2' => 0,
            'reminder3' => 0,

            );
        } else {
            $m = date('m');
            $y = date('Y');
            $percent = 0;
            //$invoices = $this->db->query("select id,total,status,datepaid,date from tblinvoices");
            $companyid = $cid;
            $company = globofix($cid);
            if ($company->mage_invoicing) {
                $unpaid = $this->getInvoiceSum($companyid, 'Unpaid');
                $paid = $this->getInvoiceSum($companyid, 'Paid');
                $paidtotal = $this->getInvoiceCount($companyid, 'Paid');
                $unpaidtotal = $this->getInvoiceCount($companyid, 'Unpaid');
            } else {
                $unpaid= 0;
                $paid =  0;
                $paidtotal= 0;
                $unpaidtotal = 0;
            }

            $paid = str_replace(',', '', $paid);
            $unpaid = str_replace(',', '', $unpaid);
            $client = $this->getClientCount();
            if ($paid > 0 && $unpaid > 0) {
                if (is_numeric($paid) && is_numeric($unpaid)) {
                    $percent = $paid / $unpaid * 100;
                }
            }
            $new_order = $this->getOrderStatus($companyid, "New");
            $pending_order = $this->getOrderStatus($companyid, "Pending");
            $active_order = $this->getOrderStatus($companyid, "Active");
            $ticket = $this->db->query("select * from a_helpdesk_tickets where status != ?", array('closed'));

            $r1 = $this->db->query("select * from a_reminder where companyid=? and type=? and date_sent like ?", array($companyid, 'Reminder1', date('Y-m-d').'%'));
            $r2 = $this->db->query("select * from a_reminder where companyid=? and type=? and date_sent like ?", array($companyid, 'Reminder1', date('Y-m-d').'%'));
            $r3 = $this->db->query("select * from a_reminder where companyid=? and type=? and date_sent like ?", array($companyid, 'Reminder1', date('Y-m-d').'%'));


            return array('client' => $client,
            'total_unpaid' => number_format($unpaid, 2),
            'count_unpaid' => '0',
            'count_paid' => '0',
            'total_amount_unpaid' => number_format($unpaid, 2),
            'total_this_year_paid' => number_format($paid, 2),
            'total_this_year_unpaid' => number_format($unpaid, 2),
            'total_thismonthpaid' => number_format($paid, 2),
            'total_thismonthunpaid' => number_format($unpaid, 2),
            'total_thismonth_amount_unpaid' => number_format($unpaid, 2),
            'total_thismonth_amount_paid' => number_format($paid, 2),
            'percentpaid' => $percent,
            'percentunpaid' => $percent,
            'tickets' => $ticket->num_rows(),
            'subscriptions' => array('active' => $active_order, 'pending' => $pending_order, 'new' => $new_order),
            'reminder1' => $r1->num_rows(),
            'reminder2' => $r2->num_rows(),
            'reminder3' => $r3->num_rows()
            );
        }
    }
    public function CreateFutureInvoiceOnActivation($serviceid, $vat, $amount)
    {
        $amt = includevat4($vat, $amount);

        $this->db = $this->load->database('default', true);
        $this->db->where('id', $serviceid);
        $this->db->update('a_services', array('future_activation' => $amt));
    }
    public function getClientBySN($sn, $companyid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select b.* from a_services_mobile a left join a_clients b on b.id=a.userid where a.msisdn_sn=? and a.companyid=?", array($sn, $companyid));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function setEventLogDone($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('JobId', $id);
        $this->db->update('a_event_log', array('status' => 1));
    }
    public function ResetCounterPod($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $id);
        $this->db->update('a_services_mobile', array('pod_counter' => 0));
    }
    public function hasPaymentNbr($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query('select * from a_sepa_items where id=? and iPaymentNbr > 0', array($id));
        if ($q->num_rows()>0) {
            return $q->row()->iPaymentNbr;
        } else {
            return false;
        }
    }
    public function insertPayment($array)
    {
        $this->db->insert('a_payments', $array);
    }
    public function UpdateSepaItems($id, $status, $amount, $msg, $ori, $inv, $paymentid = false)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        if ($paymentid) {
            $this->db->update('a_sepa_items', array('iPaymentNbr' => $paymentid, 'status' => $status, 'balance' => $amount, 'message' => $msg . "\n ------  Amount: " . $ori . " Assigned to Invoice to = " . $inv));
        } else {
            $this->db->update('a_sepa_items', array('status' => $status, 'balance' => $amount, 'message' => $msg . "\n ------  Amount: " . $ori . " Assigned to Invoice to = " . $inv));
        }
    }
    public function isPromotionUnique($data)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query('select * from a_promotion where promo_code like ? and companyid=?', array($data['promo_code'], $data['companyid']));

        if ($q->num_rows()>0) {
            return false;
        } else {
            return true;
        }
    }

    public function insertPromotion($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_promotion', $data);


        return $this->db->insert_id();
    }
    public function CreateProforma($arr)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_invoices', $arr);

        return $this->db->insert_id();
    }
    public function getPendingMsisdn($userid)
    {
        $q = $this->db->query("select c.name,a.id,a.userid,a.recurring,b.msisdn from a_services a left join a_services_mobile b on b.serviceid=a.id left join a_products c on c.id=a.packageid where a.userid=? and a.status = ? and b.msisdn_type=?", array($userid,'Pending','porting'));

        if ($q->num_rows()>0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getPlatformList($companyid)
    {
        $q = $this->db->query("select * from a_api_data where companyid=?", array($companyid));

        if ($q->num_rows()>0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function getClientsByMonth($cid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT date_created, COUNT(id) as total FROM a_clients where date_created > '2019-01-01' and companyid=? GROUP BY MONTH(date_created) order by date_created asc", array($cid));
        if ($q->num_rows()>0) {
            $html[] ="['Month', 'New Clients']";
            foreach ($q->result_array() as $row) {
                $html[] = "['".substr($row['date_created'], 0, 7)."', ".$row['total']."]";
            }

            return $html;
        } else {
            return array();
        }
        //return $q->result_array();
    }
    public function CreateModemSubscription($serviceid, $modem, $name)
    {
        $data = array('name' => 'Modem', 'terms' => '600','serviceid' => $serviceid, 'addonid' => 1, 'recurring_total' => 0, 'addon_type' => 'others');
        if ($modem == 4) {
            $data['terms'] = 0;
            $data['recurring_total'] = 0;
        } elseif ($modem == 3) {
            $data['terms'] = 600;
            $data['recurring_total'] = 4;
        } elseif ($modem == 2) {
            $data['terms'] = 5;
            $data['recurring_total'] = 19;
        }
        $this->db->insert('a_services_addons', $data);
        $this->db->insert('a_services_addons', array('name' => 'Activatie '.$name, 'terms' => '1','serviceid' => $serviceid, 'addonid' => null, 'recurring_total' => 0, 'addon_type' => 'others'));
        //$this->db->insert('a_services_addons', array('name' => 'Activatie '.$name, 'terms' => '1','serviceid' => $serviceid, 'addonid' => NULL, 'recurring_total' => 0, 'addon_type' => 'others'));
    }
    public function insertTopup($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_topups', $data);
        return $this->db->insert_id();
    }
    public function getClientmaps($cid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT id,firstname,lastname,initial,lon,lat from a_clients where lon is not null and companyid=?", array($cid));
        if ($q->num_rows()>0) {
            foreach ($q->result() as $row) {
                $html[] = "['".str_replace("'", "\'", format_name($row))."', ".$row->lat.", ".$row->lon.", ".$row->id."]";
            }

            return $html;
        } else {
            return array();
        }
    }
    public function getInvoicebyMonth($cid)
    {
        $this->db = $this->load->database('magebo', true);
        $total = $this->db->query("SELECT CONVERT(NVARCHAR(7), dInvoiceDate, 120) [Month], SUM(mInvoiceAmount) [TotalAmount]
FROM tblInvoice a
LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressNbr
WHERE b.iCompanyNbr=?
and a.dInvoiceDate >= '2019-01-01'
and a.iInvoiceType = 40
GROUP BY CONVERT(NVARCHAR(7), dInvoiceDate, 120)
ORDER BY [Month] ASC", array($cid));

        $paid = $this->db->query("SELECT CONVERT(NVARCHAR(7), dInvoiceDate, 120) [Month], SUM(mInvoiceAmount) [TotalAmount]
FROM tblInvoice a
LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressNbr
WHERE b.iCompanyNbr=?
and a.dInvoiceDate >= '2019-01-01'
and a.iInvoiceType = 40
and a.iInvoiceStatus in (53,54)
GROUP BY CONVERT(NVARCHAR(7), dInvoiceDate, 120)
ORDER BY [Month] ASC", array($cid));


        $unpaid = $this->db->query("SELECT CONVERT(NVARCHAR(7), dInvoiceDate, 120) [Month], SUM(mInvoiceAmount) [TotalAmount]
FROM tblInvoice a
LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressNbr
WHERE b.iCompanyNbr=?
and a.dInvoiceDate >= '2019-01-01'
and a.iInvoiceType = 40
and a.iInvoiceStatus = 52
GROUP BY CONVERT(NVARCHAR(7), dInvoiceDate, 120)
ORDER BY [Month] ASC", array($cid));

        $cn = $this->db->query("SELECT CONVERT(NVARCHAR(7), dInvoiceDate, 120) [Month], SUM(mInvoiceAmount) [TotalAmount]
FROM tblInvoice a
LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressNbr
WHERE b.iCompanyNbr=?
and a.dInvoiceDate >= '2019-01-01'
and a.iInvoiceType = 41
GROUP BY CONVERT(NVARCHAR(7), dInvoiceDate, 120)
ORDER BY [Month] ASC", array($cid));
        $html =array();

        $x[] ="['Month', 'ALL INVOICES', 'PAID', 'UPAID','CREDITNOTE']";
        if ($total->num_rows()>0) {
            foreach ($total->result_array() as $row) {
                //$html[] = "['".substr($row['Month'], 0, 7)."', ".round($row['TotalAmount'], 2)."]";
                $html[substr($row['Month'], 0, 7)][0] =  round($row['TotalAmount'], 2);
            }
        }

        if ($paid->num_rows()>0) {
            //$html[] ="['Month', 'Invoices']";
            foreach ($paid->result_array() as $row) {
                //$html[] = "['".substr($row['Month'], 0, 7)."', ".round($row['TotalAmount'], 2)."]";
                $html[substr($row['Month'], 0, 7)][1]  =  round($row['TotalAmount'], 2);
            }
        }


        if ($unpaid->num_rows()>0) {
            //$html[] ="['Month', 'Invoices']";
            foreach ($unpaid->result_array() as $row) {
                //$html[] = "['".substr($row['Month'], 0, 7)."', ".round($row['TotalAmount'], 2)."]";
                $html[substr($row['Month'], 0, 7)][2]  =  round($row['TotalAmount'], 2);
            }
        }


        if ($cn->num_rows()>0) {
            //$html[] ="['Month', 'Invoices']";
            foreach ($cn->result_array() as $row) {
                //$html[] = "['".substr($row['Month'], 0, 7)."', ".round($row['TotalAmount'], 2)."]";
                $html[substr($row['Month'], 0, 7)][3]  =  round($row['TotalAmount'], 2)*-1;
            }
        }
        if ($html) {
            foreach ($html as $key => $row) {
                if (empty($row[3])) {
                    $row[3] = 0;
                }
                if (empty($row[1])) {
                    $row[1] = 0;
                }
                if (empty($row[2])) {
                    $row[2] = 0;
                }
                if (empty($row[0])) {
                    $row[0] = 0;
                }
                $x[] =  "['".$key."', ".$row['0'].", ".$row['1'].",".$row['2'].",".$row['3']."]";
            }
        }

        return $x;
    }
    public function getSubscriptionActiveByMonth($cid)
    {
        $date = new DateTime(date('Y-m-d'));

        $date->modify('-6 month');
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT date_created, COUNT(id) as total FROM a_services where date_created > ? and companyid=? and status not in ('Cancelled','Terminated') GROUP BY MONTH(date_created) order by date_created asc", array($date->format('Y-m-d'),$cid));

        $r = $this->db->query("SELECT date_terminate, COUNT(id) as total FROM a_services where date_terminate > ? and companyid=? and status in ('Terminated') and date_terminate is not null GROUP BY MONTH(date_terminate) order by date_terminate asc", array($date->format('Y-m-d'),$cid));




        if ($q->num_rows()>0) {
            $x[] ="['Month', 'New Orders', 'Terminated']";

            if ($q->num_rows()>0) {
                foreach ($q->result_array() as $row) {
                    $html[substr($row['date_created'], 0, 7)][0] =  $row['total'];
                }
            }

            if ($r->num_rows()>0) {
                foreach ($r->result_array() as $row) {
                    $html[substr($row['date_terminate'], 0, 7)][1] =  $row['total'];
                }
            }
            foreach ($html as $key => $row) {
                if (empty($row[0])) {
                    $row[0] = 0;
                }
                if (empty($row[1])) {
                    $row[1] = 0;
                }

                $x[] =  "['".$key."', ".$row['0'].", ".$row['1']."]";
            }

            return $x;
        } else {
            return array();
        }
        //return $q->result_array();
    }

    public function getSubscriptionstatuses($cid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT msisdn_status, COUNT(id) as total FROM a_services_mobile where companyid=?  GROUP BY msisdn_status order by total desc", array($cid));
        if ($q->num_rows()>0) {
            $html[] ="['Status', 'Total']";
            foreach ($q->result_array() as $row) {
                $html[] = "['".$row['msisdn_status']."', ".$row['total']."]";
            }

            return $html;
        } else {
            return array();
        }
        //return $q->result_array();
    }


    public function CreateProformaItems($arr)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_invoiceitems', $arr);
    }
    public function geolocation($address)
    {
        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false&key=AIzaSyAr8E3IAwiZVz6DCo6yDCzpAVXSBZM2-50');
        $output= json_decode($geocode);
        // return $output;
        //   print_r($output);
        return array_filter(array('lat' => $output->results[0]->geometry->location->lat, 'lon' => $output->results[0]->geometry->location->lng));
    }
    public function ChangeClientDunnigProfile($id, $type)
    {
        if ($type == "reminder_1") {
            $profile = 1;
        } elseif ($type == "reminder_2") {
            $profile = 2;
        } elseif ($type == "reminder_3") {
            $profile = 3;
        } else {
            $profile = 0;
        }
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_clients', array('dunning_profile' => $profile));
    }
    public function insertReminderLog($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_reminder', $data);
    }
    public function getOrderStatus($companyid, $status)
    {
        if ($status == "Active") {
            $q = $this->db->query("select count(*) as total from a_services where status in ('Active','Suspended') and companyid=?", array($companyid));
        } else {
            $q = $this->db->query("select count(*) as total from a_services where status=? and companyid=?", array($status, $companyid));
        }


        return $q->row()->total;
    }

    public function getSN($msisdn)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where msisdn like ?", array($msisdn));
        return $q->result();
    }
    public function check_double_sim($sim)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where msisdn_sim like ?", array('%'.trim($sim).'%'));
        if ($q->num_rows() == "1") {
            return true;
        } else {
            return false;
        }
    }
    public function updateResellerChangeRequest($id, $data)
    {
        $this->db = $this->load->database('default', true);

        $this->db->where('id', $id);

        $this->db->update('a_reseller_request_changes', array('date_accepted' =>date('Y-m-d'), 'status' => 'Processing', 'completed' => $data['date_commit']));
    }
    public function InsertHandset($d)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('tblhostingaddons', array('orderid' => $d['orderid'],
            'hostingid' => $d['serviceid'],
            'addonid' => 18,
            'userid' => $d['userid'],
            'name' => $d['handset_name'],
            'recurring' => str_replace(',', '.', $d['handset_price']),
            'billingcycle' => 'Monthly',
            'regdate' => date('Y-m-d'),
            'nextduedate' => date('Y-m-d'),
            'nextinvoicedate' => date('Y-m-d'),
            'paymentmethod' => 'banktransfer'));

        return $this->db->insert_id();
    }
    public function getBundleById($cid, $bundleid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT * FROM `a_products_mobile_bundles` WHERE `companyid` = ? AND `bundleid` = ?", array($cid, $bundleid));
        if ($q->num_rows()>0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function getDepartment($id)
    {
        $q = $this->db->query("select * from a_helpdesk_department where id=?", array($id));
        return $q->row();
    }

    public function setEventDone($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_event_status', array('processed' => 1));
    }

    public function getVoips()
    {
    }
    public function insertCustomer($data)
    {
        $this->db = $this->load->database('default', true);
        if (!$this->isEmailexisit($data['email'])) {
            $data['uuid'] = guidv4();
            if (empty($data['agentid'])) {
                $data['agentid'] = getDefaultAgentid($data['companyid']);
            }
            $this->db->insert('a_clients', $data);
            if ($this->db->insert_id() > 0) {
                return array('result' => true, 'id' => $this->db->insert_id());
            } else {
                return array('result' => false, 'message' => 'error inserting customer: ' . print_r($this->db->error(), true));
            }
        } else {
            return array('result' => false, 'message' => 'email address duplicate');
        }
    }
    public function CheckDeltaID($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where mvno_id like ? and companyid=?", array($id, $this->session->cid));

        if ($q->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }
    public function terminate_service($serviceid)
    {
    }
    public function changePasswordClient($id)
    {
        $password = random_str('alphanum', 10);
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_clients', array('password' => password_hash($password, PASSWORD_DEFAULT)));
        return $password;
    }
    public function UpdateClientExtraData($id, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_clients', array('mvno_id' => $data['deltaid'], 'companyid' => $this->session->cid));
    }
    public function ApplyPromotion($promo)
    {
        $promotion = $this->getPromotion($promo['id']);


        $this->db = $this->load->database('default', true);
        $this->db->where('id', $promo['serviceid']);
        $this->db->update('a_services', array('promocode' => $promotion->promo_code));
    }
    public function getPromotion($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_promotion where id = ?", array($id));

        return $q->row();
    }
    public function CheckDeltaIDNew($id, $userid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where a_mvno like ? and id != ? and companyid=?", array($id, $userid, $this->session->cid));

        if ($q->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }
    public function getEmailLog($id, $cid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_email_log where id = ? and companyid=?", array($id, $cid));

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return true;
        }
    }
    public function InsertCustomFields($id, $data)
    {
        $this->db = $this->load->database('default', true);
        foreach ($data as $key => $val) {
            $this->db->insert('tblcustomfieldsvalues', array('relid' => $id, 'fieldid' => $key, 'value' => $val));
        }
    }
    public function checkMsisdn($msisdn)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where msisdn = ?", array($msisdn));
        if ($q->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }
    public function MoveSubscription($data)
    {
        $this->db  = $this->load->database('default', true);
        $this->db->query("update a_services set userid=? where id=? and companyid=?", array($data['userid'], $data['serviceid'], $data['companyid']));
        $this->db->query("update a_services_mobile set userid=? where id=? and companyid=?", array($data['userid'], $data['serviceid'], $data['companyid']));
    }
    public function getClientData($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where id = ?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row_array();
        } else {
            return false;
        }
    }
    public function getClientidByState($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where mvno_id like ? and companyid=?", array($id, $this->session->cid));
        if ($q->num_rows() > 0) {
            return $q->row()->id;
        } else {
            return false;
        }
    }

    public function updateAddon($data, $id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_services_addons', $data);
    }

    public function updateAddonTeum($serviceid, $bundleid, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        $this->db->where('arta_bundleid', $bundleid);
        $this->db->update('a_services_addons', $data);
    }
    public function SetiInvoiceNbrProforma($proformainvoice, $iInvoiceNbr)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('invoicenum', $proformainvoice);
        $this->db->update('a_invoices', array('iInvoiceNbr' => $iInvoiceNbr));
    }
    public function getClientidFromMageboid($id, $fieldid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('fieldid', $fieldid);
        $this->db->where('value', $id);
        $q = $this->db->get("tblcustomfieldsvalues");
        if ($q->num_rows() > 0) {
            return $q->row()->relid;
        } else {
            return "";
        }
    }

    public function InsertDeltaUsername($username, $userid)
    {
        $this->db = $this->load->database('default', true);

        if (!$this->existSSO(strtolower(trim($username)))) {
            $this->db->insert('a_clients_sso', array('username' => strtolower(trim($username)), 'userid' => $userid));
            return true;
        } else {
            return false;
        }
    }
    public function getSepaItem($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_sepa_items a where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function existSSO($username)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients_sso where username=?", array($username));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getContractDate($serviceid)
    {
        $id = getCustomfieldsidbyNameProduct('ContractDate');
        $this->db->where('fieldid', $id);
        $this->db->where('relid', $serviceid);
        $q = $this->db->get('tblcustomfieldsvalues');

        if ($q->num_rows() > 0) {
            return $q->row()->value;
        } else {
            return date('m-d-Y');
        }
    }

    public function getOrderid($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.status as orderstatus from a_services a left join a_services_mobile b on b.serviceid=a.id where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function getClientProduct($id)
    {
        $f = $this->whmcs_api(array('action' => 'GetClientsProducts', 'serviceid' => $id));
        /*$result = (array) $f->products->product[0];
        foreach ($f->products->customfields->customfield as $row) {

        $result['customfields'][$row->name] = $row->value;
        }
         */
        return $f;
    }

    public function getClientdetailMageboData($id)
    {
        $q = $this->db->query("select b.iTypeGroupNbr,b.cTypeDescription,a.cAddressData,bPreferredOrActive from tblAddressData a  left join tblType b on b.iTypeNbr=a.iTypeNbr  where a.iAddressNbr=? and b.iTypeGroupNbr in (4,5,6)", array($id));

        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function isCustomer($companyid, $mageboid)
    {
        $this->db->where('companyid', $companyid);
        $this->db->where('mageboid', $mageboid);
        $q = $this->db->get('a_clients');
        if ($q->num_rows()>0) {
            return true;
        } else {
            return false;
        }
    }
    public function UpdatePricing($companyid, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $companyid);
        $this->db->where('id', $data['id']);
        unset($data['id']);

        $this->db->update('a_pricing_specific', $data);
        return $this->db->affected_rows();
    }
    public function checkAssignedTicket($id)
    {
        $q = $this->db->query("select * from a_helpdesk_tickets where deptid=?", array($id));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function insertService($data)
    {
        $this->db->insert('recurring_services', $data);
        return $this->db->insert_id();
    }
    public function getClientDetails($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where id=? and companyid=?", array($id, $this->session->cid));
        return $q->row_array();
    }
    public function getClientSession($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->session->cid);
        $q = $this->db->get('a_clients');
        return $q->row_array();
    }
    public function getClient($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.agent from a_clients a left join a_clients_agents b on b.id=a.agentid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $c = $q->row_array();
            $d = $q->row();
            $c['stats'] = $this->getClientStats($d);
            return (object) $c;
        } else {
            return false;
        }
    }
    public function getClientRaw($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.* from a_clients a where a.id=?", array($id));
        return $q->row_array();
    }
    public function getClientCIOT($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.agent from a_clients_ciot a left join a_clients_agents b on b.id=a.agentid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $c = $q->row_array();
            $d = $q->row();
            $c['stats'] = $this->getClientStats($d);
            return (object) $c;
        } else {
            return false;
        }
    }
    public function getClientStats($client)
    {
        $stats['invoice_paid'] = "0.00";
        $stats['invoice_unpaid'] = "0.00";
        $stats['active_service'] = "0";
        $stats['open_ticket'] = "0";
        $stats['closed_ticket'] = 0;
        $this->db = $this->load->database('default', true);
        $tickets = $this->db->query('select status from a_helpdesk_tickets where userid=?', array($client->id));

        if ($tickets->num_rows() > 0) {
            foreach ($tickets->result() as $row) {
                if (in_array($row->status, array("Open", "Awaiting-Reply", "On-Hold", "In-Progress"))) {
                    $stats['open_ticket'] = $stats['open_ticket'] + 1;
                } else {
                    $stats['closed_ticket'] = $stats['closed_ticket'] + 1;
                }
            }
        }

        $services = $this->db->query('select status from a_services where userid=?', array($client->id));
        if ($services->num_rows() > 0) {
            foreach ($services->result() as $row) {
                if ($row->status == "Active") {
                    $stats['active_service'] = $stats['active_service'] + 1;
                }
            }
        }

        $a = $this->getInvoiceStats($client);

        return (object) array_merge($stats, $a);
    }
    public function getInvoiceStats($client)
    {
        $stats['invoice_unpaid'] = "0";
        $stats['invoice_paid'] = "0";
        $stats['count_unpaid'] = "0";
        $stats['count_paid'] = "0";
        $company = globofix($client->companyid);

        if ($company->mage_invoicing) {
            $this->db = $this->load->database('magebo', true);
            if ($client->mageboid > 0) {
                $invoices = $this->db->query('select iInvoiceNbr,iInvoiceStatus,mInvoiceAmount from tblInvoice where iAddressNbr=? and iInvoiceType =?', array($client->mageboid, 40));
                if ($invoices->num_rows() > 0) {
                    foreach ($invoices->result() as $i) {
                        if ($i->iInvoiceStatus == "54") {
                            $stats['invoice_paid'] = $i->mInvoiceAmount + $stats['invoice_paid'];
                            $stats['count_paid'] = $stats['count_paid']+1;
                        } else {
                            $stats['invoice_unpaid'] = $i->mInvoiceAmount + $stats['invoice_unpaid'];
                            $stats['count_unpaid'] = $stats['count_unpaid']+1;
                        }
                    }
                }
            }
        }


        return $stats;
    }
    public function getInvoiceSum($companyid, $status)
    {
        $this->db = $this->load->database('magebo', true);
        if ($status == "Unpaid") {
            $q = $this->db->query("SELECT SUM(b.mInvoiceAmount) as total
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus IN(52,53)", array($companyid));
        } else {
            $q = $this->db->query("SELECT SUM(b.mInvoiceAmount) as total
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus = 54", array($companyid));
        }
        return number_format($q->row()->total, 2);
    }
    public function AddSmsNumber($array)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_sms_notifications where userid=? and msisdn=?", array($array['userid'], $array['msisdn']));

        if ($q->num_rows() == 0) {
            $this->db->insert('a_sms_notifications', $array);
        }
    }
    public function getInvoiceCount($companyid, $status)
    {
        $this->db = $this->load->database('magebo', true);
        if ($status == "Unpaid") {
            $q = $this->db->query("SELECT *
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus IN(52,53)", array($companyid));
        } else {
            if ($companyid != 2) {
                $q = $this->db->query("SELECT *
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus = 54", array($companyid));
            } else {
                $q = $this->db->query("SELECT *
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus = 54 and b.dInvoiceDate LIKE ?", array($companyid, date('Y-m-d').'%'));
            }
        }
        return $q->num_rows();
    }
    public function getInvoicedetail($id)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("select * from tblInvoice a left join tblAddress b on a.iAddressNbr=b.iAddressNbr where a.iInvoiceNbr=?", array($id));
        $res = $q->row_array();
        $res['items'] = $this->getInvoiceDetailsMagebo($id);

        return (object) $res;
    }

    public function getInvoice($id)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("select a.*,b.iCompanyNbr from tblInvoice a left join tblAddress b on a.iAddressNbr=b.iAddressNbr where a.iInvoiceNbr=?", array($id));
        return $q->row();
    }

    public function getMvnoIdbyInvoice($id)
    {
        $id = ltrim($id, '0');
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("select a.iAddressNbr from tblInvoice a left join tblAddress b on a.iAddressNbr=b.iAddressNbr where a.iInvoiceNbr=? and a.iInvoiceType = ?", array($id, '40'));
        $client = $this->getClientByMageboid($q->row()->iAddressNbr);
        return '<a href="https://mijnmobiel.delta.nl/admin/client/detail/' . $client->id . '" alt="' . $client->id . '" title="' . $client->id . '">' . $client->mvno_id . '</a>';
    }
    public function getClientByMageboid($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where mageboid=?", array($id));

        return $q->row();
    }
    public function getClientdetailMagebo($id)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("select a.*,b.cCountry from tblAddress a left join tblCountry b on b.iCountryIndex=a.iCountryIndex where a.iAddressNbr=?", array($id));
        $res = $q->row_array();
        $res['detail'] = $this->getClientdetailMageboData($id);

        return $res;
    }
    public function getInvoiceDetailsMagebo($id)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT a.*,b.cInvoiceDetailDescription FROM tblInvoiceDetail a
            left join tblInvoiceDetailDescription b on b.iInvoiceDetailDescriptionNbr=a.iInvoiceDetailDescriptionNbr
            left join tblInvoice c on c.iInvoiceNbr=a.iInvoiceNbr
            where a.iInvoiceNbr=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }

    public function getCreditnoteDetailsMagebo($id)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT a.*,b.cInvoiceDetailDescription FROM tblInvoiceDetail a
            left join tblInvoiceDetailDescription b on b.iInvoiceDetailDescriptionNbr=a.iInvoiceDetailDescriptionNbr
            left join tblInvoice c on c.iInvoiceNbr=a.iInvoiceNbr
            where a.iInvoiceNbr=? and c.iInvoiceType=?", array($id, 41));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function updateBundlePricingid($pricingid, $addonid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $addonid);
        $this->db->update('a_services_addons', array('iGeneralPricingIndex' => $pricingid));
    }
    public function getOptions($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.GPInvoiceGroupNbr,b.base_bundle from a_services_addons  a left join a_products_mobile_bundles b on b.id=a.addonid where a.serviceid=? and b.base_bundle ='0'", array($id));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function getOrderedAddonid($serviceid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_addons where serviceid=?", array($serviceid));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $bb[] = $row->addonid;
            }

            return $bb;
        } else {
            return false;
        }
    }
    public function updateCustomFields($relid, $key, $val)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('tblcustomfieldsvalues', array('value' => $val, 'relid' => $relid, 'fieldid' => $key));
    }
    public function reject_hosting($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_services', array('status' => 'Cancelled'));
    }

    public function reject_hostingaddons($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('hostingid', $id);
        $this->db->update('a_services_mobile', array('status' => 'Cancelled'));
    }
    public function deleteCustomFields($relid, $key)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('relid', $relid);
        $this->db->where('fieldid', $key);
        $this->db->delete('tblcustomfieldsvalues');
    }
    public function UpdateClient($id, $data)
    {
        $this->db = $this->load->database('default', true);
        unset($data['id']);
        $this->db->where('id', $id);
        $this->db->update('a_clients', $data);
        return $this->db->affected_rows();
    }
    public function UpdateClientProductPrice($a)
    {
        foreach ($a as $key => $val) {
            echo $key . " " . array_sum($val) . "\n";
            $this->update_price($key, $val);
        }
    }

    public function update_price($key, $service)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $key);
        $this->db->update('a_services', array('recurring' => array_sum($service)));
        return $this->db->affected_rows();
    }

    public function updatePrice($key, $price)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $key);
        $this->db->update('a_services', array('recurring' => $price));
        return $this->db->affected_rows();
    }

    public function getHosting($msisdn)
    {
        $this->db = $this->load->database('default', true);
        $this->db->like('msisdn', $msisdn);

        $q = $this->db->get('a_services_mobile');
        if ($q->num_rows() > 0) {
            $d['id'] = $q->row()->id . " " . $q->row()->domainstatus;
            $d['addons'] = implode('=', $this->getHostingAddons($q->row()->id)) . "<br />";

            return $d;
        } else {
            return $id . " Not Found<br/>";
        }
    }

    public function getPricing($id)
    {
        $this->db = $this->load->database('default', true);
        $a = $this->db->query("select * from a_products where id =?", array($id));

        if ($a->num_rows() > 0) {
            return $a->row()->recurring_total;
        } else {
            return '0.00';
        }
    }

    public function updateCustomer($data)
    {
        $this->db = $this->load->database('default', true);
        if (!$this->isEmailexisit_client($data['id'], $data['email'])) {
            $this->db->where('id', $data['id']);
            unset($data['id']);
            //$customfields[getCustomfieldsidbyName()]
            $this->db->update('a_clients', $data);
            if (!$this->db->affected_rows()) {
                return array('result' => false, 'message' => lang('You did not change anything'));
            } else {
                return array('result' => $this->db->affected_rows(), 'message' => lang('Client ppdated successfully'));
            }
        } else {
            return array('result' => false, 'message' => lang('Email address duplicate') . ' ' . lang('Please use another email which is not used by another account'));
        }
    }
    public function updateCustomerCIOT($data)
    {
        $this->db = $this->load->database('default', true);

        $this->db->where('id', $data['id']);
        unset($data['id']);
        //$customfields[getCustomfieldsidbyName()]
        $this->db->update('a_clients_ciot', $data);
        if (!$this->db->affected_rows()) {
            return array('result' => false, 'message' => lang('You did not change anything'));
        } else {
            return array('result' => $this->db->affected_rows(), 'message' => lang('Client ppdated successfully'));
        }
    }
    public function ChangeClientPassword($client)
    {
        $this->db = $this->load->database('default', true);
        $password = random_string('alnum', 10);
        $this->db->where('id', $client->id);
        $this->db->update('a_clients', array('password' => password_hash($password, PASSWORD_DEFAULT)));

        return $password;
    }

    public function changePasswordClientV2($client)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $client->id);

        if (!empty($client->password)) {
            $this->db->update('a_clients', array('password' => password_hash($client->password, PASSWORD_DEFAULT)));
        } else {
            $client->password = 'temp' . rand(10000000, 999999999999);
            $this->db->update('a_clients', array('password' => password_hash($client->password, PASSWORD_DEFAULT)));
        }

        return $client->password;
    }

    public function GetProductType($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_products where id=? ", array($id));
        if ($q->num_rows() > 0) {
            return $q->row()->combi;
        } else {
            return "3";
        }
    }
    public function getAdmin($email)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_admin where email like ?", array($email));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAdminbyId($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_admin where id like ?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row_array();
        } else {
            return false;
        }
    }

    public function getWelcomeLetter($serviceid, $clientid)
    {
        $this->db = $this->load->database("default", true);
        $q = $this->db->query("select * from a_clients_files where userid=? and filename like ?", array($clientid, '%' . $serviceid . '%'));
        return $q->row();
    }
    public function getCn($id)
    {
        $q = $this->db->query("select a.*,b.invoicenum from creditnotes a left join invoices b on b.id=a.invoiceid where a.id=?", array($id));
        return $q->row_array();
    }

    public function getWelcomePdf($filename)
    {
        $whmcsUrl = WHMCS_URL;
        $postfields['filename'] = $filename;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'getwelcomepdf.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);

        $jsondata = json_decode($response);
        return (object) $jsondata;
    }

    public function IsAllowedInvoice($id, $companyid, $typ = false)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query('select a.* from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.iInvoiceNbr=? and b.iCompanyNbr in ?', array($id, $companyid));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function save_state($serviceid, $data)
    {
        $pp = array();
        foreach ($data as $row) {
            $pp[$row->PackageDefinitionId] = $row->Available;
        }

        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        $this->db->update('a_services_mobile', array('packages_state' => json_encode($pp)));
    }
    public function getInvoicePdf($id)
    {
        $this->db = $this->load->database('invoices', true);
        $this->db->select('*');
        $this->db->where('iInvoiceNbr', $id);
        $q = $this->db->get('cs_invoices');
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            echo "Invoice file has not been generated please wait for few minutes then try again!\n";
            exit;
        }
    }
    public function getResellerINParams($ResellerID)
    {
        $this->db = $this->load->database('default', true);
        $q =        $this->db->query("SELECT * FROM a_united_resellers WHERE ResellerId =?", array($ResellerID));

        if ($q->num_rows()>0) {
            return $q->row_array();
        } else {
            return array();
        }
    }

    public function getSpecicifCLIInfo($SN, $rp)
    {
        if ($rp['ResellerId']==67||$rp['ResellerId']==52||$rp['ResellerId']==55) {
            //United PostPaid
            $option=array('trace'=>1,'features'=>SOAP_USE_XSI_ARRAY_TYPE,'cache_wsdl' => WSDL_CACHE_NONE,'encoding'=>'UTF-8');
            $clientCLI  = new soapclient('http://217.72.233.98/API10107/DefaultAPI/ServiceCLI.asmx?WSDL', $option);
            //$user='STUnitedPostpaid';
            //$pass='$TUn1tedP0stpa1D';
            $user = $rp['resellerlogin'];
            $pass = $rp['resellerpass'];
        } else {
            $option = array('trace' => 1, 'features' => SOAP_USE_XSI_ARRAY_TYPE,'cache_wsdl' => WSDL_CACHE_NONE);
            $clientCLI = new soapclient('http://10.13.90.75/API10107/DefaultAPI/ServiceCLI.asmx?WSDL', $option);
            $user = $rp['resellerlogin'];
            $pass = $rp['resellerpass'];
        }



        try {
            if (substr(trim($SN), 0, 2) == "32") {
                $result = $clientCLI->GetSpecificCLIInfo(array('Login' => $user, 'Password' => $pass, 'SN' => $SN));
                //print_r($result);
            }
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})"."\n".$clientCLI->__getLastRequest()."\n".$clientCLI->__getLastResponse(), E_USER_ERROR);
        }

        if ($result->GetSpecificCLIInfoResult->TotalItems > 0) {
            $s2 = simplexml_load_string($result->GetSpecificCLIInfoResult->ListInfo->any);
            $i = 0;
            foreach ($s2->NewDataSet->SpecificCLIInfo as $po) {
                $res[$i]['SN'] = (string)$po->SN;
                $res[$i]['SubscriptionStatus'] = (string)$po->SubscriptionStatus;
                $res[$i]['Balance'] = (string)$po->Balance;
                $res[$i]['Prepaid'] = (string)$po->Prepaid;
                $res[$i]['CurrencyId'] = (string)$po->CurrencyId;
                $res[$i]['Expiration'] = (string)$po->Expiration;
                $res[$i]['MsisdnNr'] = (string)htmlentities($po->MsisdnNr);
                $res[$i]['MSISDNStatus'] = (string)htmlentities($po->MSISDNStatus);
                $res[$i]['IMSINr'] = (string)htmlentities($po->IMSINr);
                $res[$i]['IMSIStatus'] = (string)htmlentities($po->IMSIStatus);
                $res[$i]['SIMNr'] = (string)htmlentities($po->SIMNr);
                $res[$i]['SIMStatus'] = (string)htmlentities($po->SIMStatus);
                $res[$i]['cTypeSIM'] = (string)htmlentities($po->cTypeSIM);
                $i++;
            }
            return $res;
        }
    }


    public function getRequest($rp, $PortInFilter, $PortOutFilter, $sn)
    {
        if ($rp['ResellerId']==67) {
            //United PostPaid
            $option=array('trace'=>1,'features'=>SOAP_USE_XSI_ARRAY_TYPE,'cache_wsdl' => WSDL_CACHE_NONE,'encoding'=>'UTF-8');
            $ServiceResellerMobileNumberPort   = new soapclient('http://217.72.233.98/API1000/DefaultAPI/ServiceResellerMobileNumberPort.asmx?WSDL', $option);
            //$user='STUnitedPostpaid';
            //$pass='$TUn1tedP0stpa1D';
            $user = $rp['resellerlogin'];
            $pass = $rp['resellerpass'];
        } else {
            $option = array('trace' => 1, 'features' => SOAP_USE_XSI_ARRAY_TYPE,'cache_wsdl' => WSDL_CACHE_NONE);
            $ServiceResellerMobileNumberPort = new soapclient('http://10.13.90.75/API1000/DefaultAPI/ServiceResellerMobileNumberPort.asmx?WSDL', $option);
            $user = $rp['resellerlogin'];
            $pass = $rp['resellerpass'];
        }



        try {
            $res3=$ServiceResellerMobileNumberPort->GetRequest(array('Login' => $user,'Password' => $pass,'PortInFilter'=>$PortInFilter,'PortOutFilter'=>$PortOutFilter,'Sn'=>$sn));
        } catch (SoapFault $fault) {
            trigger_error("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})"."\n".$clientCLI->__getLastRequest()."\n".$clientCLI->__getLastResponse(), E_USER_ERROR);
        }

        $i=0;
        $res['OperationType']=(string)$res3->PortRequest->OperationType;
        $res['RequestType']=(string)$res3->PortRequest->RequestType;
        $res['DonorOperator']=(string)$res3->PortRequest->DonorOperator;
        $res['MSISDN']=(string)$res3->PortRequest->MSISDN;
        $res['DonorSIM']=(string)$res3->PortRequest->DonorSIM;
        $res['AccountNumber']=(string)$res3->PortRequest->AccountNumber;
        $res['CustomerName']=(string)$res3->PortRequest->CustomerName;
        $res['CompanyName']=(string)$res3->PortRequest->CompanyName;
        $res['AuthorisedRequestorName']=(string)$res3->PortRequest->AuthorisedRequestorName;
        $res['VATNumber']=(string)$res3->PortRequest->VATNumber;
        $res['Sn']=(string)$res3->PortRequest->Sn;
        $res['Status']=(string)$res3->PortRequest->Status;
        $res['ActionDate']=(string)$res3->PortRequest->ActionDate;
        $res['EnteredDate']=(string)$res3->PortRequest->EnteredDate;
        $res['ErrorID']=(string)$res3->PortRequest->Error->ID;
        $res['ErrorValue']=(string)$res3->PortRequest->Error->Value;
        $res['ErrorComment']=(string)$res3->PortRequest->Error->Comment;
        $res['platform']=(string)$platform;
        $res['user']=(string)$user;
        $res['pass']=(string)$pass;
        return $res;
    }

    public function getfinancialcondition($MSISDN)
    {
        $this->db = $this->load->database('magebo', true);
        $iPincode=substr($MSISDN, 2);

        if (!$iPincode) {
            return false;
        }
        $q = $this->db->query("SELECT C.cCompany Company,
        dbo.fnGetFinancialCondition(A.iAddressNbr) FCondition,
        AD4.cAddressData EmailAddress,
        AD3.cAddressData VATNumber,
        AD1.cAddressData TelephoneNumber,
        AD2.cAddressData GsmNumber,P.iPincode,P.iLocationIndex,L.iAddressNbr,A.cName,A.cStreet,A.iZipCode,A.cCity
        FROM
        tblC_Pincode P LEFT JOIN tblLocation L ON
        L.iLocationIndex=P.iLocationIndex
        LEFT JOIN tblAddress A ON A.iAddressNbr=L.iAddressNbr
        LEFT JOIN tblAddressData AD1 ON AD1.iTypeNbr=18 AND AD1.iAddressNbr=A.iAddressNbr
        LEFT JOIN tblAddressData AD2 ON AD2.iTypeNbr=20 AND AD2.iAddressNbr=A.iAddressNbr
        LEFT JOIN tblAddressData AD3 ON AD3.iTypeNbr=23 AND AD3.iAddressNbr=A.iAddressNbr
        LEFT JOIN tblAddressData AD4 ON AD4.iTypeNbr=34 AND AD4.iAddressNbr=A.iAddressNbr
        LEFT JOIN tblCompany C On C.iCompanyNbr=A.iCompanyNbr
        where iPincode= ?", array($iPincode));
        return $q->row_array();
    }
    public function getMSubscription($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query('select * from a_subscriptions_manual where userid=?', array($id));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }

    public function getNextInvoices($id)
    {
        $this->db = $this->load->database('magebo', true);
        /*      $q = $this->db->query("SELECT iGeneralPricingIndex, iAddressNbr, iInvoiceGroupNbr, rNumber, mUnitPrice, rVATPercentage, iTotalMonths as terms, iTotalMonths, dContractDate, iStartMonth, iStartYear, bEnabled, cRemark, iShowRemarkOnReport, cInvoiceDescription, bIncludePeriodDescription, dLastUpdate, iLastUserNbr
FROM tblGeneralPricing where iAddressNbr=? and iTotalMonths >0 and bEnabled=1 and iInvoiceGroupNbr != 350", array($id));
*/
        $q = $this->db->query("SELECT G.iGeneralPricingIndex, G.iAddressNbr, G.iInvoiceGroupNbr, G.rNumber, G.mUnitPrice, G.rVATPercentage, G.iTotalMonths

as terms, G.iTotalMonths, G.dContractDate, G.iStartMonth, G.iStartYear, G.bEnabled, G.cRemark, G.iShowRemarkOnReport, G.cInvoiceDescription, G.bIncludePeriodDescription, G.dLastUpdate, G.iLastUserNbr

FROM tblGeneralPricing G

left join ( select iGeneralPricingIndex, count(*) as CNT

       from tblPricedItems

       where iGeneralPricingIndex in ( select iGeneralPricingIndex from tblGeneralPricing

              where iAddressNbr = ? )

         AND iInvoiceNbr is not null

       group by iGeneralPricingIndex ) P on P.iGeneralPricingIndex = G.iGeneralPricingIndex

WHERE iAddressNbr = ?

  AND G.iTotalMonths > ISNULL(P.CNT,0)

  AND G.bEnabled = 1

  AND G.iInvoiceGroupNbr not in ( 86, 350 )

ORDER BY G.iGeneralPricingIndex", array($id, $id));

        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function IsAllowedClient($id, $companyid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query('select * from a_clients where id=? and companyid=?', array($id, $companyid));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function IsAllowedCn($id, $userid)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query('select a.* from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.iInvoiceNbr=? and b.iCompanyNbr in ? and a.iInvoiceType=?', array($id, $userid, 41));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function IsAllowedService($id, $userid)
    {
        $this->db = $this->load->database('default', true);

        $q = $this->db->query('select * from a_services where serviceid=? and userid=? and companyid=?', array($id, $userid, $this->session->cid));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function IsAllowedMobile($msisdn, $userid)
    {
        $mobile = $this->mageboGet('GetSim/' . trim($msisdn));
        if (in_array($mobile->iCompanyNbr, $userid)) {
            return true;
        } else {
            return false;
        }
    }
    public function isEmailexisit($email)
    {
        $q = $this->db->query("select * from a_clients where email like ? and companyid=?", array($email, $this->session->cid));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isEmailexisit_client($id, $email)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where email like ? and id != ? and companyid=?", array($email, $id, $this->session->cid));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function removeStolen($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $id);
        $this->db->delete('a_mobile_stolen_number');
    }
    public function getClientLogs($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_logs where userid = ? and companyid=? order by id desc limit 5", array($id, $this->session->cid));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }

    public function getStaf($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_admin where id=? and companyid=?", array($id, $this->session->cid));
        return $q->row();
    }

    public function updateAdmin($email, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->like('email', strtolower(trim($email)));
        $this->db->update('a_admin', $data);
        return array('result' => $this->db->affected_rows());
    }


    public function updateAdminbyId($id, $data)
    {
        $this->db = $this->load->database('default', true);

        $this->db->where('id', strtolower(trim($id)));
        $this->db->update('a_admin', $data);
        if (!empty($data['password'])) {
            $this->UpdateAllPassword($data);
        }
        return array('result' => $this->db->affected_rows());
    }

    public function UpdateAllPassword($data)
    {
        $this->db = $this->load->database('default', true);
        $p = array('password' => $data['password']);
        $this->db->like('email', $data['email']);
        $this->db->update('a_admin', $p);
    }


    public function updatePoduct($id, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->session->id);
        $this->db->update('a_products', $data);
    }

    public function getProduct($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_products where id=? and companyid=?", array($id, $this->session->cid));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return array();
        }
    }

    public function GetCompany($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_mvno where companyid=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return array();
        }
    }

    public function insertPoduct($data)
    {
        $this->db = $this->load->database('default', true);
        $data['companyid'] = $this->session->cid;
        $this->db->insert('a_products', $data);
        return $this->db->insert_id();
    }

    public function updateProductDetails($id, $type, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $id);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_services_' . $type, $data);
        return $this->db->affected_rows();
    }

    public function insert_option_bundle($array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_services_addons', $array);
        return $this->db->insert_id();
    }
    public function updateContractDate($id, $date)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        //$this->db->where('companyid', $this->session->cid);
        $this->db->update('a_services', array('date_contract' => $date));
        return $this->db->affected_rows();
    }
    public function getBundlebyProduct($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        // $this->db->where('companyid', $this->session->cid);
        $q = $this->db->get('a_products');

        $res = explode(',', $q->row()->notes);
        return $res;
    }
    public function getBundleQuery($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        // $this->db->where('companyid', $this->session->cid);
        $q = $this->db->get('a_products_mobile_bundles');
        return $q->row()->pricing_query;
    }
    public function getBundleOption($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        // $this->db->where('companyid', $this->session->cid);
        $q = $this->db->get('a_products_mobile_bundles');

        return $q->row();
    }
    public function updateBundleID($serviceid, $ids)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        //$this->db->where('companyid', $this->session->cid);
        $this->db->update('a_services_mobile', array('bundleids' => $ids));
    }

    public function update_sepa($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $data['userid']);
        $this->db->update('a_clients', array('iban' => $data['iban'], 'bic' => $data['bic'], 'paymentmethod' => 'directdebit'));

        //$this->db->query("update a_clients set iban=?, bic=? where id=?", array(strtoupper(trim($_POST['iban'])), $_POST['bic'], $_POST['userid']));
    }
    public function getReminder3($companyid, $date)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_reminder where companyid=? and type=? and date_sent like ? group by userid", array($companyid, 'Reminder3', $date.'%'));
        if ($q->num_rows()>0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function getReminder3Delta($companyid, $date)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_reminder where companyid=? and type=? and date_sent like ? group by userid", array($companyid, 'Reminder3', $date.'%'));
        if ($q->num_rows()>0) {
            return (object)array('count' => $q->num_rows(), 'data' => $q->result());
        } else {
            return (object)array('count' => 0);
        }
    }
    public function getPriceProduct($product, $duration = 12)
    {
        if ($duration == 1) {
            $this->db->select('monthly as price');
        } elseif ($duration == 3) {
            $this->db->select('quarterly as price');
        } elseif ($duration == 6) {
            $this->db->select('semi_annually as price');
        } elseif ($duration == 12) {
            $this->db->select('annually as price');
        } elseif ($duration == 24) {
            $this->db->select('bienially as price');
        } elseif ($duration == 36) {
            $this->db->select('triennially as price');
        }
        $this->db->where('packageid', $product);

        $q = $this->db->get('a_pricing_specific');

        return $q->row()->price;
    }

    public function getService($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,a.userid,a.packageid,c.agentid,a.future_activation,d.date_ported,a.notes,b.proforma,b.setup,b.mobile_swapcost,b.product_sub_type,b.mobile_changecost,c.companyid,a.iGeneralPricingIndex,a.type,b.PriceListSubType,b.PriceList,a.promocode,a.date_contract,a.status,a.userid,a.recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,a.date_created as regdate,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,h.promo_value,h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,a.suspend_reason,a.contract_terms,x.factsheet_name,x.url as factsheet_url,b.api_id,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
       case when a.type  = 'mobile' THEN d.msisdn_status
       when a.type = 'xdsl' then e.status
       when a.type = 'voip' then f.status
       else 'Unknown' end as orderstatus,
       case when a.type  = 'mobile' THEN 'NA'
       when a.type = 'xdsl' then e.proximus_orderid
       when a.type = 'voip' then 'NA'
       else 'Unknown' end as proximusid,
       b.gid,bb.name as product_brand,a.date_terminate,t.reseller_type,t.reseller_balance,t.agent,b.ArtaPackageId
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_products_group bb on bb.id=b.gid
    left join a_products_factsheet x on b.id=x.productid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on e.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    left join a_promotion h on h.promo_code=a.promocode
    left join a_clients_agents t on t.id=c.agentid
    where (a.status != 'New' or a.status != 'Cancelled' or a.status != 'Terminated')
    and a.id=?
    and a.companyid = ?
    group by a.id", array($id, $this->companyid));

        //    $q = $this->db->query("select a.*,b.name,a.amount as recurring, b.servertype from tblhosting a left join tblproducts b on b.id=a.packageid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $res = $q->row_array();
            $res['porting'] = $this->getifPorting($id);
            $res['details'] = $this->getServicedetail($id, $res['type']);
            $res['addons'] = $this->getSubscriptionAddons('others', $id);
            if ($res['type'] == "xdsl") {
                $res['xdsl'] =    $this->getServicexDSLProducts($res['proximusid']);
            }

            return (object) $res;
        } else {
            return array();
        }
    }
    public function getProximusOrder($serviceid)
    {
        $orderid = '%'.$serviceid.'%';
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_xdsl_callback where rawdata like ? and action=?", array($orderid, 'FindGeographicLocation'));

        if ($q->num_rows()>0) {
            if($q->row()->orderid){
                return (object) array('result' => 'success','orderid' => $q->row()->orderid);
            }else{
                return (object) array('result' => 'notsuccess','data' => json_decode($q->row()->rawdata));
            }

        } else {
            return false;
        }
    }
    public function getPackageState($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where serviceid=?", array($id));

        return $q->row()->packages_state;
    }

    public function getServices($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,a.packageid,a.date_contract,a.status,a.userid,d.date_ported,a.notes,b.setup,b.mobile_swapcost,b.mobile_changecost,c.companyid,a.iGeneralPricingIndex,a.type,b.PriceListSubType,b.PriceList,a.promocode,case when a.recurring IS NOT NULL THEN a.recurring
       else '0.00' end as recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,h.promo_value,h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,a.suspend_reason,a.contract_terms,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
       case when a.type  = 'mobile' THEN d.msisdn_sn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as sn,
       case when a.type  = 'mobile' THEN d.msisdn_status
       when a.type = 'xdsl' then e.status
       when a.type = 'voip' then f.status
       else 'Unknown' end as orderstatus,
       case when a.type  = 'mobile' THEN 'NA'
       when a.type = 'xdsl' then e.proximus_orderid
       when a.type = 'voip' then 'NA'
       else 'Unknown' end as proximusid,
       b.gid,bb.name as product_brand,a.date_terminate,t.reseller_type,t.reseller_balance,t.agent,b.product_sub_type
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_products_group bb on bb.id=b.gid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    left join a_promotion h on h.promo_code=a.promocode
    left join a_clients_agents t on t.id=c.agentid
    where (a.status != 'New' or a.status != 'Cancelled' or a.status != 'Terminated')
    and a.userid=?
    group by a.id", array($id));

        //    $q = $this->db->query("select a.*,b.name,a.amount as recurring, b.servertype from tblhosting a left join tblproducts b on b.id=a.packageid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $res = $q->result();

            return $res;
        } else {
            return array();
        }
    }

    public function getSEPARejectListInvoice($invoicenum)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT * FROM `a_sepa_items` WHERE `amount` < 0 AND `invoicenumber` = ? ORDER BY `id` DESC", array($invoicenum));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function SepaItems($array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_sepa_items', $array);
        return $this->db->insert_id();
    }
    public function GetiAddressNbr($id)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->where('iInvoiceNbr', $id);
        $q = $this->db->get('tblInvoice');

        return $q->row();
    }
    public function SepaItemsComplete($id, $data = false)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        if ($data) {
            $this->db->update('a_sepa_items', $data);
        } else {
            $this->db->update('a_sepa_items', array('status' => 'Completed'));
        }
    }
    public function getServiceCli($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.userid,a.id,a.packageid,c.agentid,d.date_ported,a.notes,b.proforma,b.setup,b.mobile_swapcost,b.mobile_changecost,c.companyid,a.iGeneralPricingIndex, a.future_activation,a.type,b.PriceListSubType,b.PriceList,a.promocode,a.date_contract,a.status,a.userid,a.recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,a.date_created as regdate,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,h.promo_value,h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,a.contract_terms,x.factsheet_name,x.url as factsheet_url,b.api_id,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
       case when a.type  = 'mobile' THEN d.msisdn_status
       when a.type = 'xdsl' then e.status
       when a.type = 'voip' then f.status
       else 'Unknown' end as orderstatus,
       b.gid,bb.name as product_brand,a.date_terminate,t.reseller_type,t.reseller_balance,t.agent,b.ArtaPackageId,b.product_sub_type
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_products_factsheet x on b.id=x.productid
    left join a_products_group bb on bb.id=b.gid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    left join a_promotion h on h.promo_code=a.promocode
    left join a_clients_agents t on t.id=c.agentid
    where (a.status != 'New' or a.status != 'Cancelled' or a.status != 'Terminated')
    and a.id=?
    group by a.id", array($id));

        //    $q = $this->db->query("select a.*,b.name,a.amount as recurring, b.servertype from tblhosting a left join tblproducts b on b.id=a.packageid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $res = $q->row_array();
            $res['porting'] = $this->getifPorting($id);
            $res['details'] = $this->getServicedetail($id, $res['type']);
            $res['addons'] = $this->getSubscriptionAddons('others', $id);
            return (object) $res;
        } else {
            return array();
        }
    }
    public function getiPricingGeneralIndex($id)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query('select * from  tblGeneralPricing where iGeneralPricingIndex=?', array($id));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getMandateId($id)
    {
        $res = array('SEPA_MANDATE' => '', 'IBAN' => '', 'BIC' => '', 'SEPA_MANDATE_SIGNATURE_DATE' => '', 'SEPA_STATUS' => '');
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT a.iAddressDataIndex, a.iTypeNbr, b.cTypeDescription, a.cAddressData FROM tblAddressData a left join tblType b on b.iTypeNbr=a.iTypeNbr where  a.bPreferredOrActive=1 and a.iAddressNbr=? and b.iTypeGroupNbr=?", array($id, 5));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                if (!empty($row)) {
                    $res[str_replace(' ', '_', $row->cTypeDescription)] = $row->cAddressData;
                }
            }
            return (object) $res;
        } else {
            return (object) array('SEPA_MANDATE' => '', 'IBAN' => '', 'BIC' => '', 'SEPA_MANDATE_SIGNATURE_DATE' => '', 'SEPA_STATUS' => '');
        }
    }

    public function getMandateIdImport($id, $companyid)
    {
        /*
        mandate_id
mandate_date
mandate_status

 [SEPA_MANDATE] => 13899T
    [IBAN] => NL45ABNA0502600497
    [BIC] => ABNANL2A
    [SEPA_MANDATE_SIGNATURE_DATE] => 2017-03-31
    [SEPA_STATUS] => SEPA REOCCURRING
    [SEPA_SCHEME] => CORE


*/
        $res = array('companyid' =>  $companyid, 'mandate_id' => '', 'mandate_date' => '', 'mandate_status' => '');
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT a.iAddressDataIndex, a.iTypeNbr, b.cTypeDescription, a.cAddressData FROM tblAddressData a left join tblType b on b.iTypeNbr=a.iTypeNbr where  a.bPreferredOrActive=1 and a.iAddressNbr=? and b.iTypeGroupNbr=?", array($id, 5));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $ro['companyid'] = $companyid;
                if (!empty($row)) {
                    if (str_replace(' ', '_', $row->cTypeDescription) == 'SEPA_MANDATE') {
                        $resi = 'mandate_id';
                        $dd[$resi] = $row->cAddressData;
                    } elseif (str_replace(' ', '_', $row->cTypeDescription) == 'SEPA_MANDATE_SIGNATURE_DATE') {
                        $resi = 'mandate_date';
                        $dd[$resi] = $row->cAddressData;
                    } elseif (str_replace(' ', '_', $row->cTypeDescription) == 'SEPA_STATUS') {
                        $resi = 'mandate_status';
                        $dd[$resi] = $row->cAddressData;
                    }
                }
            }
            return (object) $dd;
        } else {
            return false;
        }
    }

    public function checkticket($subject)
    {
        $this->db = $this->load->database('whmcs', true);
        $q = $this->db->query("select * from tbltickets where title like ? and status = ?", array($subject, 'Open'));

        if ($q->num_rows()>0) {
            return false;
        } else {
            return true;
        }
    }
    public function createWHMCSticket($var)
    {
        $url = "https://my.united-telecom.be/includes/api.php";
        $postfieldsT["username"] = "united";
        $postfieldsT["password"]=md5("un!t3d");
        $postfieldsT["action"] = "openticket";
        $postfieldsT["name"] = $var['name'];
        $postfieldsT["email"] = $var['email'];
        $postfieldsT["markdown"]="true";
        $postfieldsT["deptid"] = $var['deptid']; //Technical Support
        $postfieldsT["subject"] =$var['subject'];
        $postfieldsT["message"] = $var['message'];
        $postfieldsT["priority"] = "High";
        //$postfieldsT["serviceid"] = $serviceid;
        //print_r($postfieldsT);exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfieldsT);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $dataTT= curl_exec($ch);

        curl_close($ch);
        return json_decode($dataTT);
    }
    public function addMandate($data)
    {
        $this->db->insert('a_clients_mandates', $data);
    }

    public function deleteMandate($id)
    {
        $this->db->where('userid', $id);
        $this->db->delete('a_clients_mandates');
    }
    public function getServicedetail($serviceid, $type)
    {
        $this->db->where('serviceid', $serviceid);
        $q = $this->db->get('a_services_' . trim($type));
        return $q->row();
    }

    public function getServiceXdslProducts($orderid)
    {
        $this->db->where('orderid', $orderid);
        $q = $this->db->get('a_services_xdsl_products');
        return $q->result();
    }
    public function getCustomfields($id)
    {
        $result = array();
        $q = $this->db->query("select b.fieldname,a.value from tblcustomfieldsvalues a left join tblcustomfields b on b.id=a.fieldid and b.type= ? where a.relid=?", array('product', $id));
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                if ($row['fieldname'] == "ContractDate") {
                    $s = explode('-', $row['value']);
                    $result[$row['fieldname']] = $s[1] . '/' . $s[0] . '/' . $s[2];
                } else {
                    $result[$row['fieldname']] = $row['value'];
                }
            }
        }

        return (object) $result;
    }
    public function change_domain($id, $number)
    {
        $this->db->where('id', $id);
        //$this->db->where('companyid', $this->session->cid);
        $this->db->update('a_services_mobile', array('msisdn' => $number));
    }
    public function remove_queue_send_email($serviceid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        $this->db->where('Action', 'PortInAccepted');
        //$this->db->query("update a_event_log set accepted_nodate='0' where serviceid=? and action='PortInAccepted'", array($id));
        $this->db->update('a_event_log', array('accepted_nodate' => '1', 'status' => 1));
    }
    public function queue_send_email($serviceid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        $this->db->where('Action', 'PortInAccepted');
        //$this->db->query("update a_event_log set accepted_nodate='0' where serviceid=? and action='PortInAccepted'", array($id));
        $this->db->update('a_event_log', array('accepted_nodate' => '0', 'status' => '0'));
    }
    public function update_services_data($type, $serviceid, $data)
    {
        if ($type == "mobile") {
            if (!empty($data['msisdn_sim'])) {
                if (strlen(trim($data['msisdn_sim'])) == 13) {
                    $data['msisdn_sim'] = '893203' . trim($data['msisdn_sim']);
                }
            }
        }
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        //$this->db->where('companyid', $this->session->cid);
        $this->db->update('a_services_' . $type, $data);

        if ($this->db->affected_rows()>0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAgents($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select b.*, (select  ROUND(sum(xx.amount), 2) from a_topups xx left join a_clients vv on vv.id=xx.userid where vv.agentid=b.id) as amount  from a_clients_agents b where b.companyid = ?", array($id));
        if ($q->num_rows() >0) {
            return array('result' => 'success', 'agent' => $q->result_array());
        } else {
            return array('result' => 'error', 'no agent found');
        }
    }
    public function insertServicemobile($data)
    {
        $this->db->insert('a_services_mobile', $data);
    }

    public function getiInvoiceGroupNbr($pid)
    {
        $q = $this->db->query("select * from a_products where id=?", array($pid));
        if ($q->num_rows() > 0) {
            return $q->row()->iInvoiceGroupNbr;
        } else {
            return 369;
        }
    }

    public function getServiceBySN($sn)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.serviceid,a.userid,a.msisdn,b.* from a_services_mobile a left join a_clients b on b.id=a.userid left join a_services g on g.id=a.serviceid where a.msisdn_sn like ? and g.status = ?", array($sn, 'Active'));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function updateReminderCounter($id, $counter)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_services_mobile', array('portin_reminder' => $counter));
    }


    public function getServiceBySNDetail($sn)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.* from a_services_mobile a left join a_clients b on b.id=a.userid left join a_services g on g.id=a.serviceid where a.msisdn_sn like ?", array($sn));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function update_services($serviceid, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $serviceid);
        //$this->db->where('companyid', $this->session->cid);
        $this->db->update('a_services', $data);
        return $this->db->affected_rows();
    }

    public function set_service_change_done($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_subscription_changes', array('cron_status' => 1));
    }
    public function getifPorting($serviceid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where msisdn_type=? and serviceid=?", array('porting', $serviceid));
        if ($q->num_rows() > 0) {
            return (object) array('porting' => true, 'number' => $q->row()->donor_msisdn);
        } else {
            return (object) array('porting' => false);
        }
    }
    public function getSubscriptionAddons($type, $serviceid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_addons where addon_type=? and serviceid=?", array($type, $serviceid));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function GetAddons($id)
    {
        $res = array();
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.regdate,a.billingcycle,a.status,b.name,a.addonid from tblhostingaddons a left join tbladdons b on b.id=a.addonid where a.hostingid=?", array($id));
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $row['price'] = $this->getPrice($row['addonid'], 'addon');
                $res[] = $row;
            }
        } else {
            $res = array();
        }

        return $res;
    }
    public function getPrice($id, $type)
    {
        $res = array();
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_products where id=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row()->recurring_total;
        } else {
            return '0.00';
        }
    }

    public function ChangeStatusService($id, $status)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_services', array('status' => $status));
    }
    public function ChangeStatusOrderID($id, $status, $type)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $id);
        //$this->db->where('companyid', $this->session->cid);
        if ($type == "mobile") {
            $field = 'msisdn_status';
        } else {
            $field = 'status';
        }
        $this->db->update('a_services_' . $type, array($field => $status));
    }

    public function ChangeStatusOrder($id, $status, $type)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        if ($type == "mobile") {
            $t = 'a_services_mobile';
            $ty = "option";
        } elseif ($type == "xdsl") {
            $t = 'a_services_mobile';
            $ty = "option";
        } elseif ($type == "voip") {
            $t = 'a_services_mobile';
            $ty = "option";
        } else {
            $t = 'a_services_mobile';
            $ty = "others";
        }
        $this->db->update($t, array('status' => $status));
    }
    /*
    public function getIban($id)
    {

    $this->db = $this->load->database('local', true);
    $this->db->where('userid', $id);
    $q = $this->db->get('clients_iban');
    if ($q->num_rows() > 0) {
    return $q->result_array();
    } else {

    return array();
    }
    }
     */

    public function updateService($id, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $id);
        $this->db->update('a_services_mobile', $data);
    }

    public function getContact($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select *,companyname as role from a_clients_contacts where id=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row_array();
        } else {
            return array();
        }
    }

    public function billi_api($data)
    {
        $url = 'https://api.billi.be/v1/wholesale/' . $data['action'];
        $ch = curl_init($url);
        unset($data['action']);
        $jsonDataEncoded = json_encode($data);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'X-API-KEY: dmhKZHFIZFFwVmtZeWdGT0o3R1ZBVHhSbElvLzJOQkFOL1B6RlJFcEJGa1JtQjEwMVIzNkhWc0V5RkpCZ051Vw=='));
        $result = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: BILLI ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        slack($result, '#billi');
        return json_decode($result);
    }

    public function AssignProximusOrderid1($data, $orderid)
    {
        unset($data['companyid']);
        unset($data['appointment_date_requested']);
        $this->db = $this->load->database('default', true);
        $this->db->where('orderid', $orderid);
        $this->db->update('a_services_xdsl_products', $data);
    }

    public function AssignProximusOrderid2($data, $orderid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('proximus_orderid', $orderid);
        $this->db->update('a_services_xdsl', $data);
    }
    public function GetProximusHistory($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_proximus_order_status where serviceid=? order by id desc", array($id));
        if ($q->num_rows()>0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getProximusProduct($pid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_proximus_offer where packageid=?", array($pid));

        return $q->row();
    }
    public function proximus_api($data)
    {
        $url = 'https://mso.united-telecom.be/api.php';
        $ch = curl_init($url);
        $jsonDataEncoded = json_encode($data);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 120);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'X-API-KEY: dmhKZHFIZFFwVmtZeWdGT0o3R1ZBVHhSbElvLzJOQkFOL1B6RlJFcEJGa1JtQjEwMVIzNkhWc0V5RkpCZ051Vw=='));
        $result = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: API.php ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        //slack($result, '#billi');
        return json_decode($result);
    }
    public function billi_api_auth($data)
    {
        $url = 'https://api.billi.be/v1/auth';
        $ch = curl_init($url);
        unset($data['action']);
        $jsonDataEncoded = json_encode($data);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'X-API-KEY: aU5tMkorR2hZY2F4VSs2SEpDdHBGTjF0Ti9rMkR2ZVluSVg2cTB0aURDWXgxUFIzK2ZEdU01NkJvQ2VndUJCYQ=='));
        $result = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: BILLI ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        slack($result, '#billi');
        return json_decode($result);
    }
    public function whmcs_api($postfields)
    {
        $whmcsUrl = WHMCS_URL;
        $postfields['username'] = "admin";
        $postfields['password'] = md5("un!t3d");
        $postfields['responsetype'] = 'json';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS Prod ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        //print_r($response);
        //exit;
        $jsondata = json_decode($response);
        return (object) $jsondata;
    }
    public function united_api($postfields)
    {
        $whmcsUrl = WHMCS_UNITED;
        $postfields['username'] = "simson";
        $postfields['password'] = md5("asunlai1234");
        $postfields['responsetype'] = 'json';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS Prod ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        //print_r($response);
        //exit;
        $jsondata = json_decode($response);
        return (object) $jsondata;
    }

    public function mageboGet($query)
    {
        $l = explode('/', $query);

        if ($l[0] == "GetSim") {
            $this->db = $this->load->database('magebo', true);
            $q = $this->db->query("select A.iAddressNbr, A.cName, A.cStreet,A.iCompanyNbr,Y.cTypeDescription as PaymentType,
CASE when A.iCountryIndex = 22
then (C.cCountryCode + '-' + Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix)
else (C.cCountryCode + '-' +
CASE when Cast(A.iZipCode AS varchar(10)) is null
then ''+ A.cZipSuffix
else Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix end  ) end as ZipCode,
A.cCity, P.cInvoiceReference, S.cSIMCardNbr as SIMCardNbr, S.iMSISDN as MSISDN, S.*, X.cTypeDescription as Type,Z.cTypeDescription as SimCardType, B.cInternalLanguage as Language
from tblC_SIMCard S
left join tblC_Pincode P ON P.iPincode = S.iPincode
left join tblLocation L ON L.iLocationIndex = P.iLocationIndex
left join tblAddress A ON A.iAddressNbr = L.iAddressNbr
left join tblType Y ON Y.iTypeNbr=S.iPaymentType
left join tblType X ON X.iTypeNbr=S.iMSISDNType
left join tblType Z ON Z.iTypeNbr=S.iSimCardType
left join tblLanguages B on B.iBaseVoiceMailLanguage=S.iVoiceMailLanguage
, tblCountry C
where S.iPincode = ?
AND S.bActive = 1
AND (C.iCountryIndex = A.iCountryIndex)", array(trim($l[1])));
            return $q->row();
        } else {
            $i = getUnityUsername($this->session->cid);
            //print_r($i);
            if (empty($i)) {
                die('Problem when finding the companyid ' . $this->session->cid);
            }

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, MAGEBO_URL . $query);
            curl_setopt($ch, CURLOPT_TIMEOUT, 20); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
            $result = curl_exec($ch);
            if (curl_error($ch)) {
                return (object) array('name' => 'TimeoutError', 'errorno' => curl_errno($ch), 'message' => curl_error($ch));
            } else {
                curl_close($ch);
                return json_decode($result);
            }
            //echo $result;
        }
    }
    /*
    public function mageboPost($postfields)
    {
    $companyid = $postfields['companyid'];
    $action    = $postfields['action'];
    $type      = $postfields['type'];
    unset($postfields['action']);
    unset($postfields['type']);
    unset($postfields['companyid']);
    $i = getUnityUsername($companyid, $type);

    if (empty($i)) {
    die('Username and Password could not be find in tblcompanydata');
    }

    //print_r($i);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, MAGEBO_URL . $action . '/');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    $response = curl_exec($ch);
    //echo $response;
    if (curl_error($ch)) {
    die('Unable to connect to API Server: ' . curl_errno($ch) . ' - ' . curl_error($ch) . ' ' . $action);
    }
    curl_close($ch);

    $jsondata = json_decode($response);
    return $jsondata;
    }

    public function artiliumGet($query, $type, $companyid)
    {

    $i = getUnityUsername(trim($companyid), trim($type));
    //echo $query . ' ' . $type . ' ' . $companyid;
    if (empty($i)) {
    die('Error: getUnityUsername for artiliumGet/' . $query);
    }
    //print_r($i);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ARTA_URL . $query);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
    $result = curl_exec($ch);

    if (curl_error($ch)) {
    die('Unable to connect API Server (' . $query . '): ' . curl_errno($ch) . ' - ' . curl_error($ch) . ' - ' . $query);
    }
    curl_close($ch);

    //print_r($result);
    return json_decode($result);
    }
    public function artiliumGetGlobal($query, $companyid)
    {
    $res = array();
    $r   = getUnityUsernames($companyid);
    //print_r($r);
    //exit;

    if (empty($r)) {
    die('Companyid not found in tblcompanydata');
    }

    if ($companyid == 53) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ARTA_URL . $query . $r[0]->m_username);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($r[0]->unity_username . ":" . $r[0]->unity_password)]);
    $result = curl_exec($ch);
    //print_r($result);
    if (curl_error($ch)) {
    die('Unable to connect to API Server (' . $query . '): ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);
    $res = json_decode($result, true);

    //print_r($res);

    return $res['Porting'];

    } else {
    foreach ($r as $i) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, ARTA_URL . $query . $i->m_username);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
    $result = curl_exec($ch);
    //print_r($result);
    if (curl_error($ch)) {
    die('Unable to connect to API Server: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);
    $res[] = json_decode($result, true);
    unset($result);

    }
    //print_r($res);
    if (count($r) > 1) {
    return array_merge($res[0]['Porting'], $res[1]['Porting']);
    } else {
    return $res[0]['porting'];
    }
    }

    }
     */
    public function artiliumPost($postfields)
    {
        $companyid = $postfields['companyid'];
        $action = $postfields['action'];
        $type = $postfields['type'];
        unset($postfields['action']);
        unset($postfields['type']);
        unset($postfields['companyid']);
        $i = getUnityUsername(COMPANYID, "POST PAID");

        if (empty($i)) {
            die('Username and Password could not be find in tblcompanydata');
        }

        //print_r($i);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, ARTA_URL . $action . '/');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($i->unity_username . ":" . $i->unity_password)]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        //echo $response;
        if (curl_error($ch)) {
            die('Unable to connect to API Server: (' . $action . ')' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);

        $jsondata = json_decode($response);
        return $jsondata;
    }

    public function record_stolen($msisdn, $serviceid, $userid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_mobile_stolen_number', array('companyid' => $this->session->cid, 'userid' => $userid, 'msisdn' => $msisdn, 'serviceid' => $serviceid, 'reported_by' => $this->session->firstname . ' ' . $this->session->lastname, 'status' => 1));
    }
    public function record_unstolen($msisdn, $serviceid, $userid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        $this->db->update('a_mobile_stolen_number', array('status' => '0'));
    }
    public function AddNotes($data)
    {
        $data['created'] = date('Y-m-d H:i:s');
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_clients_notes', $data);
        return $this->db->insert_id();
    }

    public function getRole($id)
    {
        $this->db = $this->load->database('default', true);
        //$this->db->where('companyid', $this->session->cid);
        $this->db->where('id', $id);
        $q = $this->db->get('a_role');
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function getAdminRoles($name, $companyid, $rolename)
    {
        $q = $this->db->query("select companyid,perms from a_admin_roles where companyid=? and rolename=?", array($companyid, $name));
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $row['rolename'] = $rolename;
                $res[] = $row;
            }
            return $res;
        } else {
            return array();
        }
    }
    public function getRolePermission($name)
    {
        $perms = array();
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_admin_permissions where controller not in ('chat','calendar','cron','setting','super','import','masterdb') order by folder, controller, method DESC");
        $r = $this->db->query('select * from a_admin_roles where rolename=? and companyid=?', array($name, $this->session->cid));
        if ($r->num_rows() > 0) {
            foreach ($r->result() as $row) {
                $perms[] = $row->perms;
            }
        }
        foreach ($q->result_array() as $row) {
            if (in_array($row['id'], $perms)) {
                $row['checked'] = '1';
            } else {
                $row['checked'] = '0';
            }
            $result[] = $row;
        }
        return $result;
    }

    public function generateNewKey($companyid, $sso, $creator, $level=false)
    {
        $token = md5(rand(1000000000000000, 9999999999999999));
        if ($level) {
            $this->db->insert('a_tokens', array('token' => $token, 'companyid' => $companyid, 'sso' => $sso, 'creator' => $creator, 'level' => $level, 'agentid' => $_SESSION['reseller']['id']));
        } else {
            $this->db->insert('a_tokens', array('token' => $token, 'companyid' => $companyid, 'sso' => $sso, 'creator' => $creator));
        }
        return $token;
    }

    public function getKeys($cid, $resellerid=false)
    {
        $this->db->where('companyid', $cid);
        if ($resellerid) {
            $this->db->where('agentid', $resellerid);
        } else {
            $this->db->where('agentid !=', '');
        }
        $q = $this->db->get('a_tokens');
        return $q->num_rows();
    }
    public function get_service_statistic($cid)
    {
        $q = $this->db->query("select * from a_services where status = ?  and companyid=?", array('Active', $cid));
        $r = $this->db->query("select * from a_services where status = ? and companyid=?", array('Suspended', $cid));
        $s = $this->db->query("select * from a_services where status in ('New','Pending') and companyid=?", array($cid));
        $t = $this->db->query("select * from a_services where status in ('Terminated','Cancelled') and companyid=?", array($cid));
        return  array(
            'active' => $q->num_rows(),
            'suspended' => $r->num_rows(),
            'pending' => $s->num_rows(),
            'cancelled' => $t->num_rows()
        );
    }
    public function getIps()
    {
        $this->db->where('companyid', $this->session->cid);
        $q = $this->db->get('a_whitelist_ip');
        return $q->num_rows();
    }
    public function InsertAddon($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_services_addons', $data);
    }
    public function checkIP($ip)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $this->session->cid);
        $this->db->where('ip', $ip);
        $q = $this->db->get('a_whitelist_ip');
        return $q->num_rows();
    }
    public function getProductGid($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $q = $this->db->get('a_products');
        if ($q->num_rows() > 0) {
            return $q->row()->gid;
        } else {
            return false;
        }
    }
    public function getCompanyid($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('ArtaResellerId', $id);
        $q = $this->db->get('a_mvno');
        if ($q->num_rows() > 0) {
            return $q->row()->companyid;
        } else {
            return false;
        }
    }
    public function getContactIdArta($id)
    {
        $this->db = $this->load->database('default', true);

        $q = $this->db->query("select b.* from a_products a left join a_products_group b on b.id=a.gid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row()->ContactId;
        } else {
            return "7";
        }
    }

    public function getOpenEvents($companyid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_event_log where companyid=? and portalread = ? and JobName=? and date_event like ?", array($companyid, '0', 'PortInChangeMsisdn', date('Y-m-d') . '%'));
        $x = $this->db->query("select * from a_event_log where companyid=? and portalread = ? and TaskName LIKE ? and JobName Like ? and date_event like ?", array($companyid, '0', 'PortOut', 'PortOutRepatriate', date('Y-m-d') . '%'));

        $s = $this->db->query("select * from a_event_log where companyid=? and portalread = ? and TaskName LIKE ? and JobName Like ? and date_event like ?", array($companyid, '0', 'PortIn', 'Request', date('Y-m-d') . '%'));


        if ($companyid == 2) {
            $p = $this->db->query("select a.subject as JobName,a.status as TaskName, b.serviceid, a.proximusid as SubscriptionId  from a_proximus_order_status a left join a_services_xdsl b on b.proximus_orderid=a.proximusid where date like ? and a.subject is not null order by a.id desc", array(date('Y-m-d') . '%'));
            $res['count'] = $q->num_rows() + $s->num_rows()+$p->num_rows();
        } else {
            $res['count'] = $q->num_rows() + $s->num_rows();
        }



        if ($q->num_rows()) {
            foreach ($q->result_array() as $r) {
                if ($r['JobName'] == "PortInChangeMsisdn") {
                    $r['JobName'] = "Done";
                }

                $res['items'][] = $r;
            }
        }
        if ($companyid == 2) {
            if ($p->num_rows()) {
                foreach ($p->result_array() as $rr) {
                    $res['items'][] = $rr;
                }
            }
        }

        if ($x->num_rows()) {
            foreach ($x->result_array() as $r) {
                if ($r['JobName'] == "PortOut") {
                    $r['JobName'] = "Done";
                }

                $res['items'][] = $r;
            }
        }

        if ($s->num_rows() > 0) {
            foreach ($s->result_array() as $p) {
                $p['JobName'] = $p['Action'];
                $res['items'][] = $p;
            }
        }

        return (object) $res;
    }



    public function insertIP($post)
    {
        $this->db->insert('a_whitelist_ip', array('ip' => trim($post['ip']), 'creator' => $post['creator'], 'companyid' => $this->session->cid));
        if ($this->db->affected_rows()) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_key($id, $agentid=false)
    {
        $this->db->where('companyid', $this->session->cid);
        $this->db->where('id', $id);
        if ($agentid) {
            $this->db->where('agentid', $agentid);
        }
        $this->db->delete('a_tokens');
        return $this->db->affected_rows();
    }
    public function x_delete($key, $relid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('relid', $relid);
        $this->db->where('fieldid', $key);
        $this->db->delete('tblcustomfieldsvalues');
        //$this->db->query("delete from tblcustomfieldsvalues where fielid=? and relid=?", array($key, $relid));
    }
    public function getBundleList($serviceid)
    {
        $bb = array();
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_addons where serviceid=? ", array($serviceid));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $b) {
                $bb[] = $b->addonid;
            }

            return $bb;
        } else {
            return false;
        }
    }
    public function x_add($key, $relid, $value, $date)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('tblcustomfieldsvalues', array('fieldid' => $key, 'relid' => $relid, 'value' => $value, 'created_at' => $date, 'updated_at' => $date));
    }
    public function delete_ip($id)
    {
        $this->db->where('companyid', $this->session->cid);
        $this->db->where('id', $id);
        $this->db->delete('a_whitelist_ip');
        return $this->db->affected_rows();
    }

    public function getInvoices($companyid)
    {
        $this->db = $this->load->database('magebo', true);
        //id,total,status,date
        $q = $this->db->query("SELECT a.iInvoiceNbr as id,a.dInvoiceDate as date ,a.iInvoiceStatus as status,b.cName,a.mInvoiceAmount as total FROM tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr WHERE b.iCompanyNbr=? and iInvoiceType= ? ORDER BY a.iInvoiceNbr DESC", array($companyid, 40));

        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function ChangeProductAddonId($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $data['serviceid']);
        $this->db->update('a_services', array('packageid' => $data['newid'], 'recurring' => getPriceRecurring($data['newid'], 12), 'contract_terms' => $data['contract_terms']));
        return $this->db->affected_rows();
    }

    public function ChangeProduct($username)
    {
        $array = getProductsellarray($this->session->cid);
        $serviceid = $this->getServiceByUsername($username);

        if ($serviceid) {
            $this->db = $this->load->database('default', true);
            $this->db->where('hostingid', $serviceid);
            $this->db->where_in('addonid', $array);
            $this->db->update('tblhostingaddons', array('addonid' => '43'));

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }
    public function getServiceByUsername($username)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where msisdn like ?", array('%' . $username . '%'));
        if ($q->num_rows() > 0) {
            return $q->row()->id;
        } else {
            return false;
        }
    }
    public function getClientCount()
    {
        $this->db = $this->load->database('default', true);
        $this->db->select('id');
        $this->db->where('companyid', $this->session->cid);
        $q = $this->db->get("a_clients");
        return $q->num_rows();
    }

    public function getCustomfieldsId($name)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from tblcustomfields where fbsql_field_name(result)=? and type=?", array($name, 'client'));
        return $q->row()->id;
    }

    public function deleteAddon($addon)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $addon['id']);
        $this->db->delete('a_services_addons');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function AddAddons($array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert_batch('tblhostingaddons', $array);
    }

    public function insertInternetOrder($array)
    {
        $this->db = $this->load->database('default', true);

        $this->db->insert('a_services', $array);
        return $this->db->insert_id();
    }

    public function DeleteContractDate($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('fieldid', $data['fieldid']);
        $this->db->where('relid', $data['serviceid']);
        $this->db->delete('tblcustomfieldsvalues');
    }

    public function InsertContractDate($data)
    {
        $this->db = $this->load->database('default', true);

        $this->db->insert('tblcustomfieldsvalues', array('fieldid' => $data['fieldid'], 'value' => $data['date'], 'relid' => $data['serviceid']));
        return $this->db->insert_id();
    }

    public function getDonor($code)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_mobile_donor_list where operator_code like ?", array($code));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getsimcard($msisdn, $companyid)
    {
        $this->db = $this->load->database('default', true);
        $query1 = $this->db->query("SELECT * FROM a_reseller_simcard where MSISDN=? and companyid=?", array($msisdn, $companyid));
        if ($query1->num_rows() > 0) {
            return $query1->row();
        } else {
            return false;
        }
    }

    public function update_simcard_reseller($msisdn, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('MSISDN', $msisdn);
        $this->db->update('a_reseller_simcard', $data);
    }
    public function getsimcard_teum($msisdn, $companyid)
    {
        $this->db = $this->load->database('default', true);
        $query1 = $this->db->query("SELECT simcard as value, simcard as label, MSISDN as iPincode FROM a_reseller_simcard where MSISDN=? and companyid=?", array($msisdn, $companyid));
        if ($query1->num_rows() > 0) {
            return $query1->row();
        } else {
            return false;
        }
    }
    public function updateReminderLog($companyid, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->query('UPDATE a_configuration SET val=? where companyid=? and name  LIKE ?', array(date('Y-m-d H:i:s'), $companyid, 'last_reminder_run'));
        $this->db->query('UPDATE a_configuration SET val=? where companyid=? and name  LIKE ?', array(json_encode($data), $companyid, 'last_reminder_run_data'));
    }
    public function get_reseller_simcard($simcard, $companyid)
    {
        $companyrange = getCompanyRange($companyid);
        $this->db = $this->load->database('magebo', true);
        $query1 = $this->db->query("SELECT TOP 10 cSIMCardNbr as value, cSIMCardNbr as label, iPincode from tblC_SimCard where  iCompanyRangeNbr = ? and cSIMCardNbr like ? order by cSIMCardNbr DESC", array($companyrange, $simcard));
        if ($query1->num_rows() > 0) {
            return $query1->row();
        } else {
            return false;
        }
    }
    public function magebo_sp($query)
    {
        $this->db->trans_start();
        $this->db = $this->load->database('magebo', true);
        $success = $this->db->query($query);
        $out_param_query = $this->db->query('select @iNewPaymentNbr as iNewPaymentNbr;');

        $this->db->trans_complete();
        $i['out_param_row'] = $this->db->row();
        $i['out_param_val'] = $this->iNewPaymentNbr;
        return $i;
    }
    public function UpdateMvno($id, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $id);
        $this->db->update('a_mvno', $data);
        return true;
    }
    public function UpdateMvnoConfig($id, $data)
    {
        $this->db = $this->load->database('default', true);
        foreach ($data as $key => $value) {
            $this->db->trans_start();
            $this->db->where('companyid', $id);
            $this->db->where('name', $key);
            $this->db->update('a_configuration', array('val' => $value));
            $this->db->trans_complete();
        }

        return true;
    }

    public function getTemplates($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_email_templates where id=? and companyid=?", array($id, $this->companyid));
        if ($q->num_rows() > 0) {
            $r = $this->db->query("select * from a_email_templates where name like ? and companyid=?", array($q->row()->name, $this->companyid));

            return $r->result();
        } else {
            return false;
        }
    }

    public function UpdateWelcomeTemplete($companyid, $lang, $body, $name)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('language', $lang);
        $this->db->where('companyid', $companyid);
        $this->db->where('name', $name);
        $this->db->update('a_products_pdf', array('body' => $body));
        return $this->db->affected_rows();
    }
    public function sync_template($companyid, $name, $lang, $body)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('language != ', 'english');
        $this->db->where('companyid', $companyid);
        $this->db->update('a_products_pdf', array('body' => $body));
        return $this->db->affected_rows();
    }
    public function getWelcomeTemplates($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_products_pdf where  companyid=? and name=?", array($this->companyid, $id));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function ChangeOrdertatus($serviceid, $status)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $serviceid);
        $this->db->update('a_services', array('status' => $status));
    }

    public function ChangeServiceStatus($serviceid, $status)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        $this->db->update('a_services_mobile', array('msisdn_status' => $status));
    }
    public function DeleteClient($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->query("delete from a_clients where id=?", array($id));
        return $this->db->affected_rows();
    }

    public function Deleteservices($id)
    {
        $this->db = $this->load->database('default', true);

        $this->db->query("delete from a_services where userid=?", array($id));
        $this->db->query("delete from a_services_mobile where userid=?", array($id));
        return $this->db->affected_rows();
    }

    public function getWelcomeBody($gid, $lang)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT * FROM `a_products_pdf` WHERE `package_gid` = ? AND `language` LIKE ?", array($gid, $lang));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return "NOT available template for this product and language" . $gid . " " . $lang;
        }
    }

    public function getPdfBody($gid, $lang, $name)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT * FROM `a_products_pdf` WHERE `package_gid` = ? AND `language` LIKE ? and name like ?", array($gid, $lang, $name));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return "NOT available template for this product and language" . $gid . " " . $lang;
        }
    }
    public function getBrandPdfFooter($gid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT * FROM `a_products_group` WHERE `id` = ?", array($gid));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return "NOT available template for this product and language";
        }
    }

    public function updateTemplate($row)
    {
        $this->db->where('package_gid !=', $row->package_gid);
        $this->db->where('language', $row->language);
        $this->db->where('name', $row->name);
        $this->db->where('companyid', $row->companyid);
        $this->db->update('a_products_pdf', array('body' => $row->body));
        return $this->db->affected_rows();
    }

    public function getProforma($serviceid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT b.invoicenum, b.id, b.total, b.status, b.date FROM a_invoiceitems a left join a_invoices b on b.id=a.invoiceid WHERE  a.serviceid = ?", array($serviceid));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return (object) array('invoicenum' => 'NA', 'id' => 'NA', 'total' => '0.0', 'status' => 'NA', 'date' => 'NA');
        }
    }
    public function getInternetStatus($username, $realm)
    {
        if ($realm == "unitedadsl") {
            $realm = "united";
        }

        $this->db =$this->load->database(strtolower($realm), true);

        $q = $this->db->query("select username,value as password from radcheck where username=?", array($username));
        $ses =  $this->db->query("select  * from radacct where username=? order by radacctid desc LIMIT 12", array($username));
        $last_out = $this->db->query("select  * from radpostauth where username=? order by id desc LIMIT 1", array($username));

        if ($q->num_rows()>0) {
            $a = $q->row_array();
            if ($ses->num_rows()>0) {
                if (!$ses->row()->acctstoptime) {
                    $a['offer'] = $ses->row()->connectinfo_start;
                    $a['status'] = 'ONLINE';
                    $a['ipaddress'] = $ses->row()->framedipaddress;
                } else {
                    $a['offer'] = "Unknown";

                    $a['status'] = 'OFFLINE';
                    $a['ipaddress'] = 'NA';
                }
                $a['sessions'] = $ses->result();

                if ($last_out->num_rows()>0) {
                    $a['last_auth'] = $last_out->row()->authdate;
                    $a['last_auth_status'] = $last_out->row()->reply;
                } else {
                    $a['last_auth'] = 'NA';
                    $a['last_auth_status'] = 'NA';
                }
            } else {
                $a['offer'] = "Unknown";
                $a['sessions'] = false;
                $a['status'] = 'OFFLINE';
                $a['ipaddress'] = 'NA';
                $a['last_auth'] = 'NA';
                $a['last_auth_status'] = 'NA';
                $a['sessions'] = false;
            }

            return (object) $a;
        } else {
            $a['offer'] = "Unknown";
            $a['sessions'] = false;
            $a['status'] = 'OFFLINE';
            $a['ipaddress'] = 'NA';
            $a['last_auth'] = 'NA';
            $a['last_auth_status'] = 'NA';
            $a['sessions'] = false;
            return (object) $a;
        }
    }

    public function getInvoicesList($companyid)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("select a.* from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.dInvoiceDate >= '2019-03-20' and b.iCompanyNbr=? and a.iInvoiceStatus=? order by iInvoiceNbr asc", array($companyid, 54));
        return $q->result();
    }
    public function changePromotion($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $data['serviceid']);
        $this->db->where('status', 'Pending');
        $this->db->update('a_services', array('promocode' => $data['promo_code']));
        if ($this->db->affected_rows()>0) {
            return array('result' => true, 'message' => 'Promocode has been applied');
        } else {
            return array('result' => false, 'message' => 'Promocode was not updated');
        }
    }
    public function getInvoicesPayments($iInvoiceNbr, $date1)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_payments where invoiceid = ?", array($iInvoiceNbr));
        $res = array();
        if ($q->num_rows()>0) {
            foreach ($q->result() as $row) {
                $res[] =  $row->date." / ".$row->paymentmethod.";".getdays($date1, $row->date);
            }
        }
        return implode(';', $res);
    }
    public function getTeumSubscriptionId($msisdn)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('msisdn', trim($msisdn));
        $this->db->where('companyid', $this->session->cid);
        $q =$this->db->get('a_reseller_simcard');

        return $q->row()->SubscriptionId;
    }
}
