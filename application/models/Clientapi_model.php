<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Clientapi_model extends CI_Model
{
    public function validate_client_token($data)
    {
        $this->db = $this->load->database('default', true);
        $url      = $this->db->query("select * from a_clients where companyid=? and id=? and email like ?", array(
            $data->companyid,
            $data->clientid,
            $data->email
        ));
        if ($url->num_rows() > 0)
        {
            
            log_message('error', 'API call successfully for: ' . $data->clientid . '. ' . $data->email);
            $this->db->query('update a_clients set lastseen=? where id=?', array(
                time(),
                $data->clientid
            ));
            return $url->row();
        }
        else
        {
            return false;
        }
    }

     public function getClient($id)
    {

        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.agent from a_clients a left join a_clients_agents b on b.id=a.agentid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $c = $q->row_array();
            $d = $q->row();
            $c['stats'] = $this->getClientStats($d);
            return (object) $c;

        } else {
            return false;
        }

    }
    public function getClientStats($client)
    {
        $stats['invoice_paid'] = "0.00";
        $stats['invoice_unpaid'] = "0.00";
        $stats['active_service'] = "0";
        $stats['open_ticket'] = "0";
        $stats['closed_ticket'] = 0;
        $this->db = $this->load->database('default', true);
        $tickets = $this->db->query('select status from a_helpdesk_tickets where userid=?', array($client->id));

        if ($tickets->num_rows() > 0) {
            foreach ($tickets->result() as $row) {
                if (in_array($row->status, array("Open", "Awaiting-Reply", "On-Hold", "In-Progress"))) {
                    $stats['open_ticket'] = $stats['open_ticket'] + 1;
                } else {
                    $stats['closed_ticket'] = $stats['closed_ticket'] + 1;
                }

            }

        }

        $services = $this->db->query('select status from a_services where userid=?', array($client->id));
        if ($services->num_rows() > 0) {
            foreach ($services->result() as $row) {
                if ($row->status == "Active") {
                    $stats['active_service'] = $stats['active_service'] + 1;
                }

            }

        }

        $a = $this->getInvoiceStats($client);

        return (object) array_merge($stats, $a);

    }
    public function getInvoiceStats($client)
    {
        $this->db = $this->load->database('magebo', true);
        $stats['invoice_unpaid'] = "0";
        $stats['invoice_paid'] = "0";
        $stats['count_unpaid'] = "0";
        $stats['count_paid'] = "0";
        if ($client->mageboid > 0) {
            $invoices = $this->db->query('select iInvoiceNbr,iInvoiceStatus,mInvoiceAmount from tblInvoice where iAddressNbr=? and iInvoiceType =?', array($client->mageboid, 40));
            if ($invoices->num_rows() > 0) {
                foreach ($invoices->result() as $i) {

                    if ($i->iInvoiceStatus == "54") {
                        $stats['invoice_paid'] = $i->mInvoiceAmount + $stats['invoice_paid'];
                        $stats['count_paid'] = $stats['count_paid']+1;
                    } else {
                        $stats['invoice_unpaid'] = $i->mInvoiceAmount + $stats['invoice_unpaid'];
                        $stats['count_unpaid'] = $stats['count_unpaid']+1;
                    }

                }

            }

        }
        return $stats;
    }

}