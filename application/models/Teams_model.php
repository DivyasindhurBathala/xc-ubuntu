<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Teams_model extends CI_Model
{
    public function send_msg($action, $message)
    {
        //mail('mail@simson.one','order array', print_r($message, true));
        $url = 'https://outlook.office.com/webhook/ed2d3766-e025-46d2-90fa-35f5d623cf52@783b2386-6c70-4075-a6a5-b5b252ac4708/IncomingWebhook/3110741c959a476a9ef5a37bab5ea17b/4e3ad863-d37c-480d-ae5b-54b1b20cc1ed';
        $data = array(
            '@context' => 'http://schema.org/extensions',
            '@type' => 'MessageCard',
            'themeColor' => '0072C6',
            'title' => 'Action: ' . $action,
            'text' => $message,
            /*
            'potentialAction' => array(
                0 => array(
                    '@type' => 'ActionCard',
                    'name' => 'Send Feedback',
                    'inputs' => array(
                        0 => array(
                            '@type' => 'TextInput',
                            'id' => 'feedback',
                            'isMultiline' => true,
                            'title' => 'Let us know what you think about Actionable Messages',
                        ),
                    ),
                    'actions' => array(
                        0 => array(
                            '@type' => 'HttpPOST',
                            'name' => 'Send Feedback',
                            'isPrimary' => true,
                            'target' => 'http://...',
                        ),
                    ),
                ),
                1 => array(
                    '@type' => 'OpenUri',
                    'name' => 'Learn More',
                    'targets' => array(
                        0 => array(
                            'os' => 'default',
                            'uri' => 'https://docs.microsoft.com/en-us/outlook/actionable-messages',
                        ),
                    ),
                ),
            ),
            */
        );

        $data_string = json_encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        return $result;
    }

    public function insertLog($request, $result, $ip)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_apilog', array(
            'api_call' => $request['request_uri'],
            'api_env' => $request['request_ver'],
            'result' => json_encode($result),
            'request' => json_encode($request),
            'ip' => $ip,
            'companyid' => $this->session->cid));
    }

    public function GetAccessToken($client_id, $redirect_uri, $client_secret, $code)
    {
        $url = 'https://www.googleapis.com/oauth2/v4/token';

        $curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code != 200) {
            throw new Exception('Error : Failed to receieve access token');
        }
    
        return $data;
    }

    public function GetUserProfileInfo($access_token)
    {
        $url = 'https://www.googleapis.com/oauth2/v2/userinfo?fields=name,email,gender,id,picture,verified_email';
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
        $data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code != 200) {
            throw new Exception('Error : Failed to get user information');
        }
        
        return $data;
    }

    public function getClientbyEmail($data, $password)
    {
        if (empty($data['gender'])) {
            $data['gender'] = "male";
        }
        if (empty($data['picture'])) {
            $data['google_pic'] = null;
        }
        $this->db->like('email', $data['email']);
        $q = $this->db->get('a_clients');
        if ($q->num_rows()>0) {
            $this->db->query('update a_clients set google_id=?, google_pic=?,gender=? where email like ?', array($data['id'],$data['picture'],$data['gender'],$data['email']));
            return array('result' => true, 'id' => $q->row()->id, 'type' => 'existing');
        } else {
            $this->db->insert('a_clients', array('uuid' => uuid_create(), 'email' => $data['email'],'password' => password_hash($password, PASSWORD_DEFAULT), 'gender' => $data['gender'], 'firstname' => $data['name'], 'companyid' => $this->session->cid, 'google_id' => $data['id'], 'google_pic' => $data['picture']));

            return array('result' => true, 'id' => $this->db->insert_id(), 'type' => 'new');
        }
    }
}
