<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proforma_model extends CI_Model
{

    public function getInvoice($id)
    {
        $lines = array();
        $q = $this->db->query("select a.*,b.mvno_id,b.mageboid,concat(b.firstname,' ',b.lastname) as customername from a_invoices a left join a_clients b on b.id=a.userid where a.id=?", array($id));
        $l = $this->db->query("select * from a_invoiceitems where invoiceid=?", array($id));

        $invoice = $q->row_array();
        $lines = $l->result_array();
        $invoice['items'] = $lines;
        return $invoice;
    }

    public function getClient($id)
    {
        $q = $this->db->query("select * from a_clients where id=?", array($id));
        return $q->row();
    }

    public function insertInvoice($data)
    {

        $this->db->insert('invoices', $data);
        return $this->db->insert_id();
    }
    public function getMaxInvoice($id)
    {
        $q = $this->db->query("select invoicenum from a_invoices where invoicenum > 2018000000 order by invoicenum desc");
        if ($q->num_rows() > 0) {
            return $q->row()->invoicenum + 1;
        } else {
            return date('Y') . '000000';
        }
    }

    public function geta_invoiceitemsbyUserid($userid)
    {
        $q = $this->db->query("select * from a_invoiceitems where invoiceid=0 and userid=?", array($userid));
        return $q->result_array();
    }
    public function getInvoiceidbyInvoicenum($invoicenum)
    {
        $q = $this->db->query("select a.*,b.mvno_id from a_invoices a left join a_clients b on b.id=a.userid where a.invoicenum=?", array($invoicenum));
        return $q->row_array();
    }
    public function UpdateInvoiceidItems($userid, $invoiceid)
    {

        $this->db->where('userid', $userid);
        $this->db->where('invoiceid', 0);
        $this->db->update('a_invoiceitems', array('invoiceid' => $invoiceid));
    }
    public function updateInvoicePublish($invoiceid, $taxrate, $total)
    {

        $subtotal = exvat($taxrate, $total);
        $tax = $total - $subtotal;
        $this->db->where('id', $invoiceid);
        $this->db->update('invoices', array('total' => $total, 'subtotal' => $subtotal, 'tax' => $tax));
    }
    public function changeDuedateBillableitemRecurring($id, $duedate)
    {
        $this->db->where('id', $id);
        $this->db->update('billableitems', array('invoicedaterecurring' => $duedate));
    }

    public function changeDuedateBillableitem($id, $duedate)
    {
        $this->db->where('id', $id);
        $this->db->update('billableitems', array('status' => '1'));
    }
    public function PublishInvoice($id)
    {
        $array = array('status' => 'Unpaid', 'notes' => ogm($id), 'invoicenum' => getNextInvoicenum());
        $this->db->where('id', $id);
        $this->db->update('invoices', $array);
    }

    public function change_duedatehosting($id, $date)
    {
        $this->db->where('id', $id);
        $this->db->update('subscriptions', array('nextinvoicedate' => $date));
    }
    public function insertmollie($data)
    {
        $this->db->insert('mod_mollielog', $data);
    }
    public function getmolliepaymentid($id)
    {
        $q = $this->db->query("select * from mod_mollielog where paymentid =?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function updateInvoice($id, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('invoicenum', $id);
        $this->db->update('a_invoices', $data);
    }
    public function updatea_invoiceitems($id, $array)
    {
        $this->db->where('id', $id);
        $this->db->update('a_invoiceitems', $array);
    }
    public function getPayments($id)
    {
        $total = array();
        $q = $this->db->query("select * from a_payments where invoiceid =?", array($id));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $total[] = $row->amount;
            }
        }

        return array_sum($total);
    }
    public function createCn($data)
    {

        $this->db->insert('creditnotes', $data);
        return $this->db->insert_id();
    }
    public function getTransactions($id)
    {
        $q = $this->db->query("select * from a_payments where invoiceid =?", array($id));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }

        return array_sum($total);
    }

    public function deletea_invoiceitems($id)
    {
        $this->db->where('invoiceid', $id);
        $this->db->delete('a_invoiceitems');
    }

    public function getLastNumberCn()
    {
        $q = $this->db->query("select * from creditnotes order by creditnotenum DESC");
        if ($q->num_rows() > 0) {
            return $q->row()->creditnotenum + 1;
        } else {
            return date('Y') . '000000';
        }
    }
    public function ChangeInvoiceStatus($id, $status)
    {
        $data = array('status' => $status);
        if ($status == "Paid") {
            $data['datepaid'] = date('Y-m-d H:i:s');
        }
        $this->db->where('id', $id);
        $this->db->update('invoices', $data);
    }
    public function UpdateClientCredit($id, $credit)
    {
        $this->db->where('id', $id);
        $this->db->update('a_clients', array('credit' => $credit));
    }

    public function changeStatusMollie($id)
    {
        $this->db->where('id', $id);
        $this->db->update('mod_mollielog', array('status' => 'Paid'));
    }

    public function addpayment($g)
    {
        /* ex:
        Array ( [paymentmethod] => banktransfer [iban] => BE98335032296093 [transid] => 1762 [date] => 2018-02-07 [adminid] => 1 [userid] => 2017800021 [invoicenum] => 2043532654 [amount] => 200 )
         */
        $i = $g;
        $invoice = $this->getInvoiceidbyInvoicenum($g['invoicenum']);
        $client = $this->getClient($invoice['userid']);
        $payment_balance = $this->getPayments($invoice['id']);
        $balance = $invoice['total'] - $payment_balance;
        $i['amount'] = str_replace(',', '.', trim($g['amount']));
        $i['invoiceid'] = $invoice['id'];
        unset($i['invoicenum']);
        $this->db->insert('payments', $i);
        logClient(array('userid' => $g['userid'], 'description' => $_SESSION['firstname'] . ' apply payment to invoicenumber ' . $invoice['invoicenum']));
        if ($this->db->insert_id() > 0) {
            if ($i['amount'] > $invoice['total']) {
                $credit = $i['amount'] - $invoice['total'];
                $new_credit = $credit + $client['credit'];
                $this->ChangeInvoiceStatus($invoice['id'], 'Paid');
                $this->UpdateClientCredit($invoice['userid'], $new_credit);
                $result = array('result' => 'succes', 'message' => 'Payment has been added into the system');
            } elseif ($i['amount'] == $invoice['total']) {
                $result = array('result' => 'succes', 'message' => 'Payment has been added into the system');
                $this->ChangeInvoiceStatus($invoice['id'], 'Paid');
            } else {
                $result = array('result' => 'succes', 'message' => 'Payment has been added into the system however the amount was not enough to set invoice as paid');
            }
        } else {
            $result = array('result' => 'error', 'message' => 'failed to add payments');
        }
        return $result;
    }
}
