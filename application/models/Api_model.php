<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_model extends CI_Model
{
    //$companyid = $this->session->cid;
    public function ValidateKey($key, $companyid = false)
    {
        $this->db = $this->load->database('default', true);
        if ($companyid) {
            $q = $this->db->query("select * from a_tokens where token=? and companyid=? and level =?", array($key, $companyid, 'mvno'));
        } else {
            $q = $this->db->query("select * from a_tokens where token=? and level =?", array($key, 'mvno'));
        }

        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function ValidateResellerKey($key, $companyid = false)
    {
        $this->db = $this->load->database('default', true);
        if ($companyid) {
            $q = $this->db->query("select * from a_tokens where token=? and companyid=? and level =?", array($key, $companyid, 'reseller'));
        } else {
            $q = $this->db->query("select * from a_tokens where token=? and level =?", array($key, 'reseller'));
        }
        log_message('error', $this->db->last_query());
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function validate()
    {
        echo $this->session->cid;
    }

    public function update_services_data($type, $serviceid, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $serviceid);
        //$this->db->where('companyid', $this->session->cid);
        $this->db->update('a_services_' . $type, $data);

        if ($this->db->affected_rows()>0) {
            return true;
        } else {
            return false;
        }
    }
    public function validate_admin_token($data)
    {
        $this->db = $this->load->database('default', true);
        $url = $this->db->query("select * from a_admin where companyid=? and id=? and email like ?", array($data->companyid, $data->adminid, $data->email));
        if ($url->num_rows()>0) {
            return $url->row();
        } else {
            return false;
        }
    }
    public function getURL($url)
    {
        $this->db = $this->load->database('default', true);
        $url = $this->db->query("select * from a_mvno where portal_url=?", array($url));
        if ($url->num_rows()>0) {
            return $url->row();
        } else {
            return false;
        }
    }
    public function tc_table($table)
    {
        $this->db = $this->load->database('super', true);
        $this->db->truncate($table);
    }
    public function insertOrder($array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_services', $array);
        return $this->db->insert_id();
    }
    public function insertMobileData($array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_services_mobile', $array);
        return $this->db->insert_id();
    }

    public function addAddon($serviceid, $addonid)
    {
        $this->db = $this->load->database('default', true);
        $addon = $this->GetAddon($addonid);
        if ($addon) {
            $this->db->insert('a_services_addons', array(
            'companyid' => $addon->companyid,
            'name' => $addon->name_desc,
            'terms' => $addon->bundle_duration_value,
            'cycle' => $addon->bundle_duration_type,
            'serviceid' => $serviceid,
            'addonid' => $addonid,
            'recurring_total' => $addon->recurring_total,
            'addon_type' => 'option',
            'arta_bundleid' => $addon->bundleid));
        }
    }

    public function GetAddon($id)
    {
        $q = $this->db->query("select * from a_products_mobile_bundles where id=?", array($id));
        if ($q->num_rows()>0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function send_lda($iPincode, $SentMessage)
    {
        if (substr($iPincode, 0, 2) == "04") {
            $iPincode = "32" . substr($iPincode, -9);
        }
        $url = "http://89.163.235.160/bvm/index.php/api/quickSms"; # URL to WHMCS API file

        $postfieldsT["msisdn"] = $iPincode;
        $postfieldsT["text"] = $SentMessage;

        print_r($postfieldsT);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'HTTP_X_USERNAME: lda',
            'HTTP_X_PASSWORD: lda123',
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfieldsT);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $dataTT = curl_exec($ch);

        curl_close($ch);

        $res = json_decode($dataTT);

        if ($res->status == "200") {
            return array('result' => 'success');
        } else {
            return array('result' => 'error', 'message' => $res['response']);
        }
        /*
        http://89.163.235.160/bvm/index.php/api/quickSms
        Method: POST

        Header Parameters are mentioned below:
        HTTP_X_USERNAME: lda
        HTTP_X_PASSWORD: lda123
        Content-Type: application/x-www-form-urlencoded

        Body parameters
        msisdn=32477943141
        text​=Hello i am test SMS from United telecom portal
         */
    }

    public function insertClient($data)
    {
        if (!$this->isEmailexisit(strtolower(trim($data['email'])))) {
            $this->db->insert('a_clients', $data);
            $id = $this->db->insert_id();
            if ($id > 0) {
                return (object) array('result' => 'success', 'clientid' => $id);
            } else {
                return (object) array('result' => 'error', 'message' => 'client not added');
            }
        } else {
            return (object) array('result' => 'error', 'message' => 'Duplicate email');
        }
    }
    public function isEmailexisit($email)
    {
        $q = $this->db->query("select * from a_clients where email like ? and companyid=?", array($email, $this->session->cid));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function isEmailexisit_client($id, $email)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_clients where email like ? and id != ? and companyid=?", array($email, $id, $this->session->cid));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function auth($data, $cid)
    {
        $this->db = $this->load->database('default', true);
        // Change state to mageboid on productions
        $q = $this->db->query("select * from a_clients where email like  ? and companyid=?", array($data['email'], $cid));
        if ($q->num_rows() > 0) {
            if (password_verify($data['password'], $q->row()->password)) {
                return array('result' => 'success', 'clientid' => $q->row()->id);
            } else {
                return array('result' => 'error', 'message' => 'Password Incorrect');
            }
        } else {
            return array('result' => 'error','message' => 'Email could not be found');
        }
    }
    public function getSSOToken($userid)
    {
        $this->db = $this->load->database('default', true);
        // Change state to mageboid on productions
        $q = $this->db->query("select * from tblclients where state=?", array($userid));
        if ($q->num_rows() > 0) {
            $time = time();
            $token = encrypt_decrypt('encrypt', $time . '#' . $userid);

            return array('result' => 'success', 'url' => base_url() . 'client/autoauth?key=' . $token);
        } else {
            return array('result' => 'error');
        }
    }

    public function updatePricing($id, $price)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('tblhosting', array('amount' => $price));
    }
    public function getRecurringPricing($order)
    {
        $this->db = $this->load->database('default', true);
        $price = array();
        $package = $this->db->query("select * from tblhosting where id=?", array($order->productids));
        $addons = $this->db->query("select * from tblhostingaddons where hostingid=?", array($order->productids));
        $price[] = $package->amount;
        if ($addons->num_rows() > 0) {
            foreach ($addons->result() as $a) {
                if ($a->addonid != '18') {
                    $price[] = $a->recurring;
                }
            }
        }
        $this->db->query("update tblorders set amount=? where id=?", array(array_sum($price), $order->orderid));
        $this->db->query("update tblhosting set amount=?, billingcycle='Monthly' where id=?", array(array_sum($price), $order->productids));
        //mail('info@alphatel.be', 'price', array_sum($price));
        $this->UpdateAddonCycle($order->productids);
        return array_sum($price);
    }
    public function UpdateAddonPricing($addonid, $id)
    {
        $price = $this->getPrice($addonid);
        $this->db->where('id', $id);
        $this->db->update('tblhostingaddons', array('addonid' => $addonid, 'recurring' => $price, 'billingcycle' => 'Monthly'));
    }
    public function getPrice($id)
    {
        //echo $id . "\n";
        $this->db->where('id', $id);
        $q = $this->db->get('activation_pricing');
        return trim($q->row()->price);
    }
    public function UpdateAddonCycle($id)
    {
        $this->db = $this->load->database('default', true);

        $this->db->query("update tblhostingaddons set billingcycle='Monthly' where id=?", array($id));
    }

    public function UpdateAddonRecurring($order)
    {
        $this->db = $this->load->database('default', true);
        if (substr(trim($order->name), 0, 6) == "Tariff") {
            $this->db->query("update tblhostingaddons set recurring=? where id=?", array("0.00", $order->id));
        } else {
            $this->db->query("update tblhostingaddons set recurring=? where id=?", array($order->monthly, $order->id));
        }
    }
    public function get_clientinfo($id)
    {
        // Change state to mageboid on production
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from tblclients where state=?", array($id));
        return $q->row_array();
    }
    public function getHost($host)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_tokens where host=? and sso=?", array($host, 'on'));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function UpdateDomain($id, $domain)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('tblhosting', array('billingcycle' => 'Monthly'));
    }

    public function UpdateHandsetName($data)
    {
        foreach (explode(',', $data['addonid']) as $id) {
            if ($this->isHandsetOrder($id)) {
                $this->UpdateHandsetInfo($id, $data);
            }
        }
    }
    public function UpdateHandsetMonth($data)
    {
        foreach (explode(',', $data['addonid']) as $id) {
            if ($this->isHandsetOrder($id)) {
                $this->DeleteHandsetMonth($id, $data['month']);
                $this->XUpdateHandsetMonth($id, $data['month']);
            }
        }
    }

    public function isHandsetOrder($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from tblhostingaddons where id=? and addonid=?", array($id, 18));

        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function getAddonsHosting($id)
    {
        $ids = array();
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from tblhostingaddons where hostingid=?", array($id));

        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $ids[] = $row->id;
            }
            return implode(',', $ids);
        } else {
            return '';
        }
    }
    public function UpdateHandsetInfo($id, $data)
    {
        $fieldid = getCustomfieldsidbyNameAddon('NumberOfMonths', '18');
        $this->insertHandsetMonth(array('relid' => $id,
            'fieldid' => $fieldid,
            'value' => $data['handset_terms']));

        unset($data['addonid']);
        unset($data['handset_terms']);
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('tblhostingaddons', $data);
    }
    public function insertHandsetMonth($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('tblcustomfieldsvalues', $data);
    }
    public function XUpdateHandsetMonth($id, $month)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('tblcustomfieldsvalues', array('fieldid' => '1267', 'relid' => $id, 'value' => $month));
    }

    public function DeleteHandsetMonth($id, $month)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('relid', $id);
        $this->db->where('fieldid', '1267');
        $this->db->delete('tblcustomfieldsvalues');
    }
    public function checkRequired($req, $data)
    {
        $check = array();
        foreach ($data as $k => $v) {
            $check[] = $k;
        }

        $error = array();
        foreach ($req as $key) {
            if (in_array($key, $check)) {
            } else {
                $error[] = $key;
            }
        }

        if (!empty($error)) {
            return array('result' => 'error', 'message' => 'Missing Required element(s): ' . implode(', ', $error));
        } else {
            return array('result' => 'success');
        }
    }

    public function getClientInvoice($data)
    {
        $id = getiAddressNbr($data['clientid']);
        $this->db = $this->load->database('default', true);
        $this->db->select("iInvoiceNbr as id,iInvoiceNbr as invoicenum");
        $this->db->from("a_tblInvoice");
        $this->db->where("iAddressNbr", $id);
        if (!empty($data['status'])) {
            $this->db->where("iInvoiceStatus", $data['status']);
        }
        if (!empty($data['start'])) {
            $this->db->where("dInvoiceDate >=", $data['start']);
        }
        if (!empty($data['end'])) {
            $this->db->where("dInvoiceDate <=", $data['end']);
        }
        $this->db->where("iCompanyNbr", $data['companyid']);
        $this->db->order_by("iInvoiceNbr", "ASC");
        $q = $this->db->get();
        log_message('error', $this->db->last_query());
        if ($q->num_rows() > 0) {
            return array('result' => 'success', 'count' => $q->num_rows(), 'invoices' => $q->result_array());
        } else {
            return array('result' => 'success', 'count' => 0);
        }
    }

    public function getProducts($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select('id,name,product_type,recurring_total,notes as bundles');
        $this->db->where('companyid', $id);
        $this->db->where('status', 1);
        $q = $this->db->get('a_products');

        return array('result' => 'success', 'products' => $q->result_array());
    }

    public function getPromotions($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select('
        promo_code,
        promo_name,
        promo_description,
        promo_value,
        promo_duration,
        date_valid,
        date_expired');
        $this->db->where('companyid', $id);
        $this->db->where('date_expired >=', date('Y-m-d'));
        $q = $this->db->get('a_promotion');

        return array('result' => 'success', 'promotions' => $q->result_array());
    }
    public function getAgents($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select b.id,b.agent,b.phonenumber,b.email,b.status,b.contact_name,b.reseller_type,b.reseller_balance,b.comission_type,
        b.comission_value, (select  ROUND(sum(xx.amount), 2) from a_topups xx left join a_clients vv on vv.id=xx.userid where vv.agentid=b.id) as amount  from a_clients_agents b where b.companyid = ?", array($id));
        if ($q->num_rows() >0) {
            return array('result' => 'success', 'agent' => $q->result_array());
        } else {
            return array('result' => 'error', 'no agent found');
        }
    }
    public function getNotes($id){

        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select* from a_clients_notes where userid=?", array($id));
        if ($q->num_rows() >0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getStaf($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select('
        id,
        role,
        firstname,
        lastname,
        email,
        picture,
        phonenumber,
        ip as last_ip');
        $this->db->where('companyid', $id);
        $this->db->where('master !=', '1');
        $q = $this->db->get('a_admin');

        return array('result' => 'success','total' => $q->num_rows(), 'staf' => $q->result_array());
    }

    public function getAddons($cid)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select('id,name,bundle_type,bundleid as platformid, recurring_total as price,');
        $this->db->where('companyid', $cid);
        $q = $this->db->get('a_products_mobile_bundles');

        return array('result' => 'success', 'addons' => $q->result_array());
    }

    public function getClientServices($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id as serviceid,a.packageid,a.domain as identifier,a.regdate,a.amount,a.domainstatus as status from a_services a left join tblproducts b on b.id=a.packageid where a.userid=? and a.domainstatus in ('Active','Pending')", array($id));
        if ($q->num_rows() > 0) {
            return array('result' => 'success', 'count' => $q->num_rows(), 'subscriptions' => $q->result_array());
        } else {
            return array('result' => 'success', 'count' => 0);
        }
    }

    public function getClientServices_mobile($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id as serviceid,a.packageid,c.msisdn as identifier,a.date_created as regdate,a.recurring,a.status,b.name as productname
        from a_services a
        left join a_products b on b.id=a.packageid
        left join a_services_mobile c on a.id=c.serviceid
        where a.userid=? and a.status in ('Active','Pending') group by a.id", array($id));
        if ($q->num_rows() > 0) {
            return array('result' => 'success', 'count' => $q->num_rows(), 'subscriptions' => $q->result_array());
        } else {
            return array('result' => 'success', 'count' => 0);
        }
    }

    public function getService($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.name,a.amount as recurring, b.servertype from tblhosting a left join tblproducts b on b.id=a.packageid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $data = $q->row_array();
            $data['addon'] = $this->getServiceAddons($id);
        } else {
            return array();
        }
    }

    public function getServiceAddons($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,a.recurring, case when a.name is not null THEN a.name else b.name end as addonname from tblhostingaddons a left join tbladdons b on a.addonid=b.id where a.hostingid=?", array($id));

        if ($q->num_rows() > 0) {
            return array($q->result_array());
        } else {
            return array();
        }
    }

    public function getDeltaHosting($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,a.recurring,b.name,b.billingcycle from tblhostingaddons a left join tbladdons b on b.id=a.addonid  where a.hostingid = ? and b.name like 'Delta%'", array($id));
        return $q->row_array();
    }

    public function whmcs_api($postfields)
    {
        $whmcsUrl = WHMCS_URL;
        $postfields['username'] = "admin";
        $postfields['password'] = md5("un!t3d");
        $postfields['responsetype'] = 'json';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS Prod ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        //print_r($response);
        //exit;
        $jsondata = json_decode($response);
        return (object) $jsondata;
    }

    public function getBic($iban)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://localhost:8080/validate/' . $iban . '?getBIC=true');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);

        return json_decode($response);
    }
    public function getBicTry($iban)
    {
        //curl https://api.bank.codes/iban/json/0f86d5d0b3472a85e53dbd91e6e4aa08/ES4221002646800110397817/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.bank.codes/iban/json/0f86d5d0b3472a85e53dbd91e6e4aa08/' . $iban);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);

        return json_decode($response);
    }
    public function custom_api($postfields)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://probile.united-telecom.be/api.php?step=' . $postfields['step']);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);

        return json_encode($response);
    }

    public function custom_api_sepa($postfields)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://probile.united-telecom.be/includes/api/magebosepaadd.php');

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);

        return json_encode($response);
    }
    public function getClient($companyid, $id)
    {
        $q = $this->db->query("SELECT * from a_clients a where id=? and companyid=?", array($id, $companyid));

        if ($q->num_rows()>0) {
            $res = $q->row_array();
            $res['result'] = "success";
            return $res;
        } else {
            return array('result' => 'error', 'message' => 'client not found', 'id' => $id);
        }
    }
    public function GetServiceDetails($serviceid)
    {
        $postfields['username'] = "admin";
        $postfields['password'] = md5("un!t3d");
        $postfields['responsetype'] = 'json';
        $postfields["action"] = "GetClientsProducts";
        $postfields["serviceid"] = $serviceid;
        $postfields["responsetype"] = "json";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, WHMCS_URL . 'includes/api.php');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $dataO = curl_exec($ch);
        curl_close($ch);
        $r = json_decode($dataO);

        $result = $r->products->product[0];

        foreach ($result->customfields->customfield as $cf) {
            //print_r($cf);exit;
            $k = $cf->name;
            $cfff[$k] = $cf->value;
        }
        //Here we replace the customeifles with our own
        $result->customfields = $cfff;

        return $result;
    }

    public function PortIngAction1($servicedetails)
    {
        $this->db = $this->load->database('magebo', true);
        // print_r($servicedetails);
        $iPincodePortIn = $servicedetails->details->msisdn;
        $iMSISDN = ltrim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr = ltrim($servicedetails->details->msisdn_sim);
        if (!empty($iPincodePortIn)) {
            $qryfind = "update tblC_SIMCard set iPincode = " . $iPincodePortIn . " where cSIMCardNbr = '" . $cSIMCardNbr . "' AND iPincode = " . $iMSISDN;

            $this->db->query($qryfind);
        }
        //echo "\n" . $qryfind . "\n";
        //$this->db = $this->load->database('magebo', true);


        //$resultServCheck = mssql_query($qryfind, $linkSQL) or die('Query failed: ' . mssql_get_last_message());
        //echo "\n LAST MESS:" . mssql_get_last_message() . "\n";
        //print_r($resultServCheck);
        //send_msg('PortIngAction1', $qryfind . ' Result: ' . $resultServCheck);
    }

    public function PortIngAction2($servicedetails)
    {
        $this->db = $this->load->database('magebo', true);
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN = trim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr = trim($servicedetails->details->msisdn_sim);
        //$qryfind = "update tblC_SIMCard set iPincode = ".$iPincodePortIn." where cSIMCardNbr = '".$cSIMCardNbr."' AND iPincode = ".$iMSISDN;
        $qryfind = "update tblC_Pincode set cRemark = 'Ported to " . $iPincodePortIn . "', iPincodeType = 16, iPincodeInterfaceType = 2446, iPincodeRedirectionType = 2447 where iPincode = " . $iMSISDN;
        $this->db->query($qryfind);
        //echo "\n" . $qryfind . "\n";
    }

    public function PortIngAction3($servicedetails)
    {
        $this->db = $this->load->database('magebo', true);
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN = trim($servicedetails->details->msisdn_sn);
        //$cSIMCardNbr = ltrim($servicedetails->customfields['SIMNbr']);

        $qryfind = "update tblC_BundlePerPincode set iPincode = " . $iPincodePortIn . " where iPincode = " . $iMSISDN . " AND ( dBundleStopDate is null OR dBundleStopDate > getdate() )";
        // echo "\n" . $qryfind . "\n";

        $this->db->query($qryfind);
    }

    public function getRef($iPincode)
    {
        $this->db = $this->load->database('magebo', true);
        $qryfind = "select cInvoiceReference from tblC_Pincode where iPincode = " . $iPincode;
        //echo "\n" . $qryfind . "\n";

        $q = $this->db->query($qryfind);

        return $q->row()->cInvoiceReference;
    }

    public function PortIngAction4($servicedetails)
    {
        log_message('error', 'Processing '.json_encode($servicedetails));
        $iPincodePortIn = $servicedetails->details->msisdn;
        $iMSISDN = trim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr = trim($servicedetails->details->msisdn_sim);
        $serviceid = $servicedetails->id;
        if (!empty($iMSISDN) && !empty($iPincodePortIn)) {
            $P_MSISDN = $this->getRef($iMSISDN);
            $P_PORTINGPIN = $this->getRef($iPincodePortIn);

            //We need to get the 'MageboSubscription' Addon Linked to this service
            // $sa = $this->XgetServiceAddons($serviceid);

            $gpIndexes = $servicedetails->iGeneralPricingIndex;
            //get gpIndexes

            $gpIndexes = rtrim($gpIndexes, ',');

            // exit;

            $qryfind = "update tblGeneralPricing set cRemark = replace(cRemark, '" . $P_MSISDN . "', '" . trim($P_PORTINGPIN) . "'), cInvoiceDescription = replace(cInvoiceDescription, '" . $P_MSISDN . "', '" . $P_PORTINGPIN . "')  where cRemark like '%" . $P_MSISDN . "%'";

            // echo "\n" . $qryfind . "\n";
            $this->db = $this->load->database('magebo', true);
            $this->db->query($qryfind);
        }
    }
    public function PortIngAction5($iAddressNbr)
    {
        //select a.serviceid FROM a_services_mobile a left join a_services b on b.id=a.serviceid left join a_products c on c.id=b.packageid left join a_products_group d on d.id=c.gid WHERE a.companyid = 54 AND a.date_wish ='2018-12-03' and b.notes like 'Imported from KPN' and a.msisdn_status='Active' ORDER BY `a`.`id` asc

        $this->db = $this->load->database('magebo', true);
        $this->db->query("update tblGeneralPricing set cInvoiceDescription = replace(cRemark, 'Abo - ', '')
  where iAddressNbr = ?
  AND iInvoiceGroupNbr = 346
  AND cInvoiceDescription like 'Abonnement -%'", array($iAddressNbr));
    }
    public function pinmove($donor_msisdn, $msisdn_sn, $msisdn)
    {

        /*$this->db->query("update tblC_Pincode
        set cRemark = ?,
        iPincodeType = 16,
        iPincodeInterfaceType = 2446,
        iPincodeRedirectionType = 2447
        where iPincode = ?", array($donor_msisdn, $msisdn_sn));
         */
        $this->db->query("update tblC_BundlePerPincode
set iPincode =?
where iPincode = ?
  AND ( dBundleStopDate is null OR dBundleStopDate > getdate() )
 ", array($msisdn, $msisdn_sn));

        return 'success';
    }
    public function getGeneralPricingIndex($iAddressNbr, $Reference)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->query("SELECT iGeneralPricingIndex, iAddressNbr, iInvoiceGroupNbr from tblGeneralPricing where iAddressNbr=?", array($iAddressNbr));
    }
    public function XgetServiceAddons($serviceid)
    {
        $postfields['username'] = "admin";
        $postfields['password'] = md5("un!t3d");
        $postfields['responsetype'] = 'json';
        $postfields["action"] = "GetClientsAddons";
        $postfields["serviceid"] = $serviceid;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://probile.united-telecom.be/includes/api.php');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $dataO = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($dataO);

        //send_msg('Porting', print_r($dataO, true));

        if ($result->result == "success") {
            $addons = $result->addons;
            return $addons;
        } else {
            return "ERROR :" . print_r($result, true);
        }
    }

    public function XgetAddonValue($addonfieldname, $relid)
    {
        $this->db = $this->load->database('default', true);

        $qryfind = "SELECT * FROM tblcustomfieldsvalues WHERE fieldid IN
      (SELECT id FROM tblcustomfields where fieldname='" . $addonfieldname . "')  AND relid='" . $relid . "'";

        $lineSC = $this->db->query($qryfind);
        foreach ($lineSC->result() as $u) {
            if (!empty($u->value)) {
                $res = $u->value;
            }
        }
        return $res;
    }

    public function UpdateMageboTblAddress($id, $field, $value)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->where('iAddressNbr', $id);
        $this->db->update('tblAddress', array($field => $value));
        /*
        Update tblAddress set cName = 'NEW NAME' where iAddressNbr = @iAddressNbr
        Update tblAddress set cName = 'NEW NAME' where iAddressNbr = @iAddressNbr
        Update tblAddress set cStreet = 'NEW STREET' where iAddressNbr = @iAddressNbr
        Update tblAddress set cCity = 'NEW CITY' where iAddressNbr = @iAddressNbr
        Update tblAddress set iZipCode = NEW ZIPCODE where iAddressNbr = @iAddressNbr
        Update tblAddress set cZIPSuffix = 'NEW SUFFIX' where iAddressNbr = @iAddressNbr
        Update tblAddress set iCountryIndex = NEW COUNTRYINDEX where iAddressNbr = @iAddressNbr
         */
    }

    public function UpdateMageboTblAddressData($id, $fieldid, $value)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->where('iAddressNbr', $id);
        $this->db->where('iTypeNbr', $fieldid);
        $this->db->where('bPreferredOrActive', 1);
        $this->db->update('tblAddressData', array('cAddressData' => $value));
        /*
        Update tblAddressData set cAddressData = '' where iAddressNbr = @iAddressNbr AND iTypeNbr = @iTypeNbr AND bPreferredOrActive = 1
         */
    }

    public function InsertMageboTblAddressData($id, $groupnbr, $fieldname, $value)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->query("exec spInsertAddressData ?, ?, ?, ?, ?, ?", array($id, $groupnbr, $fieldname, $value, '', 1));
        /*
        Update tblAddressData set cAddressData = '' where iAddressNbr = @iAddressNbr AND iTypeNbr = @iTypeNbr AND bPreferredOrActive = 1
         */
    }
    public function updateEinvoice($id, $email)
    {
        /*$this->db = $this->load->database('magebo', true);
        $this->db->where('iAddressNbr', $id);
         */
        $this->db = $this->load->database('magebo', true);
        $this->db->query("exec spUpdateInsertInvoiceConditionEmail ?, ?", array($id, $email));
    }
    public function insertEinvoice($id, $email)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->insert('tblInvoiceConditionEmail', array('cEmailAddress' => $email));
    }

    public function isUpdateMageboTblAddressData($id, $fieldid)
    {
        $this->db->where('iAddressNbr', $id);
        $this->db->where('iTypeNbr', $fieldid);
        $q = $this->db->get('tblAddressData');
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function hasInvoiceEmail($id)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->where('iAddressNbr', $id);
        $q = $this->db->get('tblInvoiceConditionEmail');
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getCountryIndex($code)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT iCountryIndex FROM tblCountry where cCountryCode=?", array($code));
        if ($q->num_rows() > 0) {
            return $q->row()->iCountryIndex;
        } else {
            return 151;
        }
    }

    public function insertPromo($iAddressNbr, $amount, $text, $date)
    {
        $d = explode(' ', trim($date));
        $tgl = $d[0];
        $m = explode('-', trim($d[0]));
        $month = $m[1];
        $year = $m[0];
        $this->db = $this->load->database('magebo', true);
        $query = "execute spInsertGeneralPricing " . $iAddressNbr . ", 'DELTA MOBILE', 'Discount', 1, " . $amount . ", 21, 1, 6, '" . $m[1] . "/" . $m[2] . "/" . $m[0] . "', " . $month . ", " . $year . ", 1, '" . $text . "', 'NO REPORTING', '" . $text . "', 1";

        $res = $this->db->query($query);
        return $res;
    }

    public function getNumber($pin)
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("select cInvoiceReference, * from tblC_Pincode where iPincode = ?", array(trim($pin)));

        //31640372716
        //06-40 37 27 16
        if ($q->num_rows() > 0) {
            return $q->row()->cInvoiceReference;
        } else {
            $num = substr($pin, 2, 9);

            return '0' . $num[0] . '-' . $num[1] . $num[2] . ' ' . $num[3] . $num[4] . ' ' . $num[5] . $num[6] . ' ' . $num[7] . $num[8];
        }
    }

    public function InsertNote($id, $note)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('tblhosting', array('notes' => $note));
    }
}
