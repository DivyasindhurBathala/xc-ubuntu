<?php
class Cron_model extends CI_Model
{
    /**
     * @ApiDescription(section="User", description="Get information about user")
     * @ApiMethod(type="get")
     * @ApiRoute(name="/user/get/{id}")
     * @ApiReturnHeaders(sample="HTTP 200 OK")
     * @ApiReturn(type="object", sample="{'user_id':'int','external_id':'int','extra':{'type':'integer'}}")
     */
    public function getActivationEventfromSN($id)
    {
        $this->db = $this->load->database('events', true);
        $q        = $this->db->query("
        SELECT
posttime,ExtractValue(rawdata,'//Id') Id,
ExtractValue(rawdata,'//TaskName') TaskName,
ExtractValue(rawdata,'//JobName') JobName,
ExtractValue(rawdata,'//FinalJob') FinalJob,
ExtractValue(rawdata,'//Success') Success,
ExtractValue(rawdata,'//SubscriptionId') SubscriptionId,
ExtractValue(rawdata,'//OldStatus') OldStatus,
ExtractValue(rawdata,'//NewStatus') NewStatus
FROM events_raw
WHERE ( (rawdata LIKE '%JobEndEvent%<ResellerId>14</ResellerId>%') OR (rawdata LIKE '%StatusChangedEvent%<ResellerId>14</ResellerId>%'))
AND (ExtractValue(rawdata,'//TaskName')='Activate' AND ExtractValue(rawdata,'//JobName')='ActivateSubscriber' AND ExtractValue(rawdata,'//FinalJob')='false' AND ExtractValue(rawdata,'//Success')='true')
AND (ExtractValue(rawdata,'//SubscriptionId')) LIKE ?
ORDER BY SubscriptionId,posttime", array(
            '%' . $id . '%'
        ));
        if ($q->num_rows() > 0)
        {
            return trim($q->row()->posttime);
        }
        else
        {
            return date('Y-01-01');
        }
    }
    public function getLastInvoice()
    {
        $this->db = $this->load->database('default', true);
        $date     = $this->db->query("select * from a_tblInvoice order by dLastUpdate desc limit 1");
        return $date->row()->dLastUpdate;
    }
    public function is_completed($id)
    {
        $q = $this->db->query("select * from a_sepa_items where fileid=? and status != 'Completed'", array(
            $id
        ));
        if ($q->num_rows() > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public function CheckRequestId($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where("proximus_orderid", $id);
        $res = $this->db->get("a_services_xdsl");
        if ($res->num_rows() > 0)
        {
            return $res->row_array();
        }
        else
        {
            return false;
        }
    }
    public function insertOrderStatus($array)
    {
                    $headers = "From: noreply@united-telecom.be" . "\r\n" .
            "CC: mail@simson.one";

        mail('support@united-telecom.be','Order status :'.$array['proximusid'].' '.$array['subject'], "Hello\n\nThis is a notification to inform you that:\nOrderid: ".$array['proximusid']."\nStatus: ".$array['status']."\nDescription: ".$array['feedback_description']."\nDate: ".date('Y-m-d H:i:s')."\nService: https://client.united-telecom.be/admin/subscription/detail/".$array['serviceid']."\n\nRegards\nUnitedPortal",$headers);
        $this->db = $this->load->database('default', true);
        $this->db->insert("a_proximus_order_status", $array);
    }
    public function changeOrder($orderid, $array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where("proximus_orderid", $orderid);
        $this->db->update('a_services_xdsl', $array);
    }
    public function changeService($serviceid, $array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where("id", $serviceid);
        $this->db->update('a_services', $array);
    }
    public function getInvoicetoImport($date)
    {
        $this->db = $this->load->database('magebo', true);
        $q        = $this->db->query("SELECT b.iCompanyNbr, a.iInvoiceNbr, a.iAddressNbr, a.iInvoiceType, CONVERT(char(10), a.dInvoiceDate,126) as dInvoiceDate, CONVERT(char(10), a.dInvoiceDueDate,126) as dInvoiceDueDate, a.mInvoiceAmount, a.tRemark, a.iInvoiceStatus, a.iPrintType, a.bBookkeepingLock, CONVERT(char(19), a.dLastUpdate,126) as dLastUpdate, a.iLastUserNbr from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.dLastUpdate >= ? order by a.dLastUpdate ASC", array(
            $date
        ));
        if ($q->num_rows() > 0)
        {
            return $q->result_array();
        }
        else
        {
            return false;
        }
    }
    public function UpdateInvoice($invoice)
    {
        // print_r($invoice);
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from a_tblInvoice where iInvoiceNbr=?", array(
            $invoice['iInvoiceNbr']
        ));
        if ($q->num_rows() == 0)
        {
            log_message("error", " New Invoice Found:      " . $invoice['iInvoiceNbr']);
            $this->db->insert('a_tblInvoice', $invoice);
        }
        else
        {
            log_message("error", " Updated Invoice Found:      " . $invoice['iInvoiceNbr']);
            $this->db->where('iInvoiceNbr', $invoice['iInvoiceNbr']);
            $this->db->update('a_tblInvoice', $invoice);
        }
       
    }
    public function ChangeProduct($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('hostingid', $data->serviceid);
        $this->db->where_in('addonid', getProductsellarray($data->companyid));
        $this->db->update('tblhostingaddons', array(
            'addonid' => $data->new_pid
        ));
        return $this->db->affected_rows();
    }
    public function getProduct($id)
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select a.id,b.name,a.recurring from tblhostingaddons a left join tbladdons b on b.id=a.addonid where a.hostingid=?", array(
            $id
        ));
        $amount   = 0;
        foreach ($q->result() as $row)
        {
            if (substr(trim(strtolower($row->name)), 0, 5) == "delta")
            {
                $productname = $row->name;
            }
            $amount = $amount + $row->recurring;
        }
        return $productname . ';' . $amount;
    }
    public function getiAddressNbr($id)
    {
        $id       = "Not found";
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from tblcustomfieldsvalues where relid=? and fieldid=?", array(
            $id,
            '900'
        ));
        foreach ($q->result() as $row)
        {
            if (!empty($row->value))
            {
                $id = $row->value;
            }
        }
        return $id;
    }
    public function update_resellerid($id, $resellerid)
    {
        $this->db = $this->load->database('events', true);
        $this->db->where('id', $id);
        $this->db->update('events_raw', array(
            'reseller' => $resellerid
        ));
    }
    public function RC($contacttype2id)
    {
        $option    = array(
            'trace' => 1,
            'features' => SOAP_USE_XSI_ARRAY_TYPE,
            'encoding' => 'UTF-8',
            'proxy_host' => '10.10.10.115',
            'proxy_port' => '3128'
        );
        $client    = new soapclient('http://10.13.90.75/API10106/DefaultAPI/ServiceContactType2.asmx?WSDL', $option);
        $clientCLI = new soapclient('http://10.13.90.75/API10106/DefaultAPI/ServiceCLI.asmx?WSDL', $option);
        $res8      = $client->GetContactType2(array(
            'Login' => 'ResellerUnited',
            'Password' => 'ResellerUnited2207',
            'ContactType2Id' => $contacttype2id
        ));
        log_message("error", print_r($res8, true));
        $resultSN = $client->GetFilteredListOfNumbers(array(
            'Login' => 'ResellerUnited',
            'Password' => 'ResellerUnited2207',
            'ContactType2Id' => $contacttype2id,
            'PageIndex' => 0,
            'PageSize' => 1000,
            'SortBy' => 1,
            'SortOrder' => 0
        ));
        print_r($resultSN);
        //exit;
        if ($resultSN->GetFilteredListOfNumbersResult->TotalItems > 0)
        {
            $sN2 = simplexml_load_string($resultSN->GetFilteredListOfNumbersResult->ListInfo->any);
            $i   = 0;
            foreach ($sN2->NewDataSet->Numbers as $poN)
            {
                //$resSN[$i]['SN']=(string)$poN->SN;
                log_message("error", print_r($poN, true));
                //continue;
                if (($poN->NumberCategory == 1) && ($poN->NumberStatus == 1))
                {
                    $resetres = $clientCLI->ReduceUsedCreditsSecondsCLI(array(
                        'Login' => 'ResellerUnited',
                        'Password' => 'ResellerUnited2207',
                        'SN' => $poN->SN,
                        'ReduceTotalCredits' => 1,
                        'ReduceTotalCreditsPerDay' => 1,
                        'ReduceTotalCreditsPerWeek' => 1,
                        'ReduceTotalCreditsPerMonth' => 1,
                        'ReduceTotalSeconds' => 1,
                        'ReduceTotalSecondsPerDay' => 1,
                        'ReduceTotalSecondsPerWeek' => 1,
                        'ReduceTotalSecondsPerMonth' => 1
                    ));

                    log_message("error", print_r($resetres, true));
                 
                }
                $i++;
            }
        }
    }
    public function RC_Iriscall($contacttype2id)
    {
        $option    = array(
            'trace' => 1,
            'features' => SOAP_USE_XSI_ARRAY_TYPE,
            'encoding' => 'UTF-8',
            'proxy_host' => '10.10.10.115',
            'proxy_port' => '3128'
        );
        $client    = new soapclient('http://10.13.90.75/API10106/DefaultAPI/ServiceContactType2.asmx?WSDL', $option);
        $clientCLI = new soapclient('http://10.13.90.75/API10106/DefaultAPI/ServiceCLI.asmx?WSDL', $option);
        $res8      = $client->GetContactType2(array(
            'Login' => 'ResellerIriscall',
            'Password' => 'ResellerIriscall2207',
            'ContactType2Id' => $contacttype2id
        ));
       
        log_message("error", print_r($res8, true));
        $resultSN = $client->GetFilteredListOfNumbers(array(
            'Login' => 'ResellerIriscall',
            'Password' => 'ResellerIriscall2207',
            'ContactType2Id' => $contacttype2id,
            'PageIndex' => 0,
            'PageSize' => 1000,
            'SortBy' => 1,
            'SortOrder' => 0
        ));
        
        log_message("error", print_r($resultSN, true));
        if ($resultSN->GetFilteredListOfNumbersResult->TotalItems > 0)
        {
            $sN2 = simplexml_load_string($resultSN->GetFilteredListOfNumbersResult->ListInfo->any);
            $i   = 0;
            foreach ($sN2->NewDataSet->Numbers as $poN)
            {
                //$resSN[$i]['SN']=(string)$poN->SN;
               
                log_message("error", print_r($poN, true));
                if (($poN->NumberCategory == 1) && ($poN->NumberStatus == 1))
                {
                    $resetres = $clientCLI->ReduceUsedCreditsSecondsCLI(array(
                        'Login' => 'ResellerIriscall',
                        'Password' => 'ResellerIriscall2207',
                        'SN' => $poN->SN,
                        'ReduceTotalCredits' => 1,
                        'ReduceTotalCreditsPerDay' => 1,
                        'ReduceTotalCreditsPerWeek' => 1,
                        'ReduceTotalCreditsPerMonth' => 1,
                        'ReduceTotalSeconds' => 1,
                        'ReduceTotalSecondsPerDay' => 1,
                        'ReduceTotalSecondsPerWeek' => 1,
                        'ReduceTotalSecondsPerMonth' => 1
                    ));
                  
                    log_message("error", print_r($resetres, true));
                }
                $i++;
            }
        }
    }
    public function RCEyeMobile($contacttype2id)
    {
        $option    = array(
            'trace' => 1,
            'features' => SOAP_USE_XSI_ARRAY_TYPE,
            'encoding' => 'UTF-8',
            'proxy_host' => '10.10.10.115',
            'proxy_port' => '3128'
        );
        $client    = new soapclient('http://10.13.90.75/API1000/DefaultAPI/ServiceContactType2.asmx?WSDL', $option);
        $clientCLI = new soapclient('http://10.13.90.75/API1000/DefaultAPI/ServiceCLI.asmx?WSDL', $option);
        $res8      = $client->GetContactType2(array(
            'Login' => 'ResellerEyeMobile',
            'Password' => 'ResellerEyeMobile2207',
            'ContactType2Id' => $contacttype2id
        ));
       
        log_message("error", print_r($res8, true));
        $resultSN = $client->GetFilteredListOfNumbers(array(
            'Login' => 'ResellerEyeMobile',
            'Password' => 'ResellerEyeMobile2207',
            'ContactType2Id' => $contacttype2id,
            'PageIndex' => 0,
            'PageSize' => 1000,
            'SortBy' => 1,
            'SortOrder' => 0
        ));
      
        log_message("error", print_r($resultSN, true));
        if ($resultSN->GetFilteredListOfNumbersResult->TotalItems > 0)
        {
            $sN2 = simplexml_load_string($resultSN->GetFilteredListOfNumbersResult->ListInfo->any);
            $i   = 0;
            foreach ($sN2->NewDataSet->Numbers as $poN)
            {
                //$resSN[$i]['SN']=(string)$poN->SN;
               
                log_message("error", print_r($poN, true));
                if (($poN->NumberCategory == 1) && ($poN->NumberStatus == 1))
                {
                    $resetres = $clientCLI->ReduceUsedCreditsSecondsCLI(array(
                        'Login' => 'ResellerEyeMobile',
                        'Password' => 'ResellerEyeMobile2207',
                        'SN' => $poN->SN,
                        'ReduceTotalCredits' => 1,
                        'ReduceTotalCreditsPerDay' => 1,
                        'ReduceTotalCreditsPerWeek' => 1,
                        'ReduceTotalCreditsPerMonth' => 1,
                        'ReduceTotalSeconds' => 1,
                        'ReduceTotalSecondsPerDay' => 1,
                        'ReduceTotalSecondsPerWeek' => 1,
                        'ReduceTotalSecondsPerMonth' => 1
                    ));
                  
                    log_message("error", print_r($resetres, true));
                }
                $i++;
            }
        }
    }
    public function RCiReachM($contacttype2id)
    {
        $option    = array(
            'trace' => 1,
            'features' => SOAP_USE_XSI_ARRAY_TYPE,
            'encoding' => 'UTF-8',
            'proxy_host' => '10.10.10.115',
            'proxy_port' => '3128'
        );
        $client    = new soapclient('http://10.13.90.75/API1000/DefaultAPI/ServiceContactType2.asmx?WSDL', $option);
        $clientCLI = new soapclient('http://10.13.90.75/API1000/DefaultAPI/ServiceCLI.asmx?WSDL', $option);
        $res8      = $client->GetContactType2(array(
            'Login' => 'ReselleriReachM',
            'Password' => 'ReselleriReachM2207',
            'ContactType2Id' => $contacttype2id
        ));
        log_message("error", print_r($res8, true));
        $resultSN = $client->GetFilteredListOfNumbers(array(
            'Login' => 'ReselleriReachM',
            'Password' => 'ReselleriReachM2207',
            'ContactType2Id' => $contacttype2id,
            'PageIndex' => 0,
            'PageSize' => 1000,
            'SortBy' => 1,
            'SortOrder' => 0
        ));
       
        log_message("error", print_r($resultSN, true));
        if ($resultSN->GetFilteredListOfNumbersResult->TotalItems > 0)
        {
            $sN2 = simplexml_load_string($resultSN->GetFilteredListOfNumbersResult->ListInfo->any);
            $i   = 0;
            foreach ($sN2->NewDataSet->Numbers as $poN)
            {
                //$resSN[$i]['SN']=(string)$poN->SN;
           
                log_message("error", print_r($poN, true));
                if (($poN->NumberCategory == 1) && ($poN->NumberStatus == 1))
                {
                    $resetres = $clientCLI->ReduceUsedCreditsSecondsCLI(array(
                        'Login' => 'ReselleriReachM',
                        'Password' => 'ReselleriReachM2207',
                        'SN' => $poN->SN,
                        'ReduceTotalCredits' => 1,
                        'ReduceTotalCreditsPerDay' => 1,
                        'ReduceTotalCreditsPerWeek' => 1,
                        'ReduceTotalCreditsPerMonth' => 1,
                        'ReduceTotalSeconds' => 1,
                        'ReduceTotalSecondsPerDay' => 1,
                        'ReduceTotalSecondsPerWeek' => 1,
                        'ReduceTotalSecondsPerMonth' => 1
                    ));
                   
                    log_message("error", print_r($resetres, true));
                }
                $i++;
            }
        }
    }
    public function InsertSepaFiles($companyd, $data)
    {
        $this->db->insert('a_sepa_files', $data);
        return $this->db->insert_id();
    }
}
