<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Adminapi_model extends CI_Model
{
    public function validate_admin_token($data)
    {
        $this->db = $this->load->database('default', true);
        $url      = $this->db->query("select * from a_admin where companyid=? and id=? and email like ?", array(
            $data->companyid,
            $data->adminid,
            $data->email
        ));
        if ($url->num_rows() > 0) {
            if ($_SERVER['REMOTE_ADDR'] != $data->ip) {
                log_message('error', 'Token Changed IP ' . $data->ip . ' now: ' . $_SERVER['REMOTE_ADDR']);
                return false;
            }
            log_message('error', 'API call successfully for: ' . $data->adminid . '. ' . $data->email);
            $this->db->query('update a_admin set lastseen=? where id=?', array(
                time(),
                $data->adminid
            ));
            return $url->row();
        } else {
            return false;
        }
    }
    public function getInvoicePdf($id)
    {
        $this->db = $this->load->database('invoices', true);
        $this->db->select('*');
        $this->db->where('iInvoiceNbr', $id);
        $q = $this->db->get('cs_invoices');
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function getProximusOrder($id)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('requestid', $id);
        $this->db->where('proximus_orderid is NOT NULL', null, false);
        $q = $this->db->get('a_services_xdsl');
        if ($q->num_rows()>0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function getXDSLResult($d)
    {
        $this->db = $this->load->database('default', true);
        $Q = $this->db->query("select * from a_services_xdsl_callback where action=? and orderid=? order by id desc limit 1", array('GetDetailedNetworkAvailability',$d['orderid']));

        if ($Q->num_rows()>0) {
            $array =  json_decode($Q->row()->rawdata);
            if ($array->interaction->feedback->type == "DISCARDED") {
                 $result['result'] = 'error';
                 $result['message'] = $array->interaction->feedback->description;

                 echo json_encode($result);
                 exit;
            }
            if (!empty($array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath->type)) {
                $row = $array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath;
                    $ty['averageLength'] = $row->charVal[0]->value;
                    $ty['unitOfMeasure'] = $row->charVal[0]->unitOfMeasure;
                    $ty['minimumAttenuation'] = $row->charVal[1]->value;
                    $ty['maximumAttenuation'] = $row->charVal[2]->value;
                    $ty['numberOfFreePairs'] = $row->charVal[3]->value;
                $this->db->where('orderid', $array->customerOrder->customerOrderIdentifier->id);
                $this->db->update('a_services_xdsl_products', array(
                    'numberOfFreePairs' => $ty['numberOfFreePairs'],
                    'averageLength' => $ty['averageLength'],
                    'unitOfMeasure' => $ty['unitOfMeasure'],
                    'minimumAttenuation' => $ty['minimumAttenuation'],
                    'maximumAttenuation' => $ty['maximumAttenuation'],
                    'networkType' => $array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath->networkType,
                ));
            } else {
                $row = $array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath[0];
                    $ty['averageLength'] = $row->charVal[0]->value;
                    $ty['unitOfMeasure'] = $row->charVal[0]->unitOfMeasure;
                    $ty['minimumAttenuation'] = $row->charVal[1]->value;
                    $ty['maximumAttenuation'] = $row->charVal[2]->value;
                    $ty['numberOfFreePairs'] = $row->charVal[3]->value;
                $this->db->where('orderid', $array->customerOrder->customerOrderIdentifier->id);
                $this->db->update('a_services_xdsl_products', array(
                    'numberOfFreePairs' => $ty['numberOfFreePairs'],
                    'averageLength' => $ty['averageLength'],
                    'unitOfMeasure' => $ty['unitOfMeasure'],
                    'minimumAttenuation' => $ty['minimumAttenuation'],
                    'maximumAttenuation' => $ty['maximumAttenuation'],
                    'networkType' => $array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath[0]->networkType,
                ));
            }




                $tt = $this->db->query('SELECT * from a_services_xdsl_products where orderid = ? and numberOfFreePairs  > 0', array ($d['orderid']));
            if ($tt->num_rows() > 0) {
                $z = $z+60;
                $result['orderid'] = $d['orderid'];

                foreach ($tt->result_array() as $product) {
                    if (($product['product_name'] == "Carrier VDSL2 GO") && in_array($product['serviceAvailabilityStatus'], array( "OK","OKBUT" ))) {
                        $users3['GO'] = "OK";
                        $a['min_download'][] = preg_replace("/[^0-9,.]/", "", $product['measuredMaximumDownloadSpeed']);
                        $a['min_upload'][] = preg_replace("/[^0-9,.]/", "", $product['measuredMaximumUploadSpeed']);
                    }

                    if ($product['product_name'] == "Carrier VDSL2 START" && in_array($product['serviceAvailabilityStatus'], array( "OK","OKBUT" ))) {
                        $users3['START'] = "OK";
                        $a['min_download'][] = preg_replace("/[^0-9,.]/", "", $product['measuredMaximumDownloadSpeed']);
                        $a['min_upload'][] = preg_replace("/[^0-9,.]/", "", $product['measuredMaximumUploadSpeed']);
                    }
                }
                if ($users3['GO'] == "OK" || $users3['START'] == "OK" || $users3['GO'] == "OKBUT" || $users3['START'] == "OKBUT") {
                    $result['vdsl'] = "OK";
                }
                $result['result'] = 'success';
                $result['min_download'] = max($a['min_download']);
                $result['min_upload'] =  max($a['min_upload']);
                $result['products'] = $tt->result_array();
                $_SESSION['XDSL'] = $result;
            } else {
                $result['result'] = 'error';
            }
        } else {
                 $result['result'] = 'error';
        }

        return $result;
    }


    public function IsAllowedInvoice($id, $userid)
    {
        $this->db = $this->load->database('magebo', true);
        $q        = $this->db->query('select a.* from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.iInvoiceNbr=? and b.iCompanyNbr in ?', array(
            $id,
            $userid
        ));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function getOnlinePaymentTables($data, $download = false)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select(" a.id,
   a.companyid,
   a.date,
   a.invoiceid,
   a.userid,
   b.mvno_id,
   a.paymentmethod,
   a.transid,
   a.amount,CASE a.invoiceid WHEN a.invoiceid > 799999999 and a.invoiceid < 899999999 THEN 'proforma' ELSE 'invoice' END as invoice_type,
   concat(b.firstname,' ',b.lastname) as name,
   b.mageboid", false);
        $this->db->join('a_clients b', 'b.id = a.userid', 'LEFT');
        if (!empty($data['from'])) {
            $this->db->where('a.date >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a.date <=', $data['to']);
        }
        if ($data['paymentmethod'] != 'all') {
            $this->db->where('a.paymentmethod', $data['paymentmethod']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'invoicenum') {
                $this->db->like('a.invoiceid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('b.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
            }
        }
        $this->db->where('a.companyid', $this->companyid);
        $this->db->from('a_payments a');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        if (!$download) {
            $this->db->limit($data['perPage'], $data['page']);
        }
        $result = $this->db->get();
        if ($download) {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return array();
            }
        }
        log_message('error', $this->db->last_query());
        if ($result->num_rows() > 0) {
            return array(
                'total' => $this->getOnlinePaymentsTotalTables($data),
                'data' => $result->result_array()
            );
        } else {
            return array(
                'total' => 0,
                'data' => array()
            );
        }
    }
    public function getOnlinePaymentsTotalTables($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db = $this->load->database('default', true);
        $this->db->select(" a.id,
   a.companyid,
   a.date,
   a.invoiceid,
   a.userid,
   b.mvno_id,
   a.paymentmethod,
   a.transid,
   a.amount,CASE a.invoiceid WHEN a.invoiceid > 799999999 and a.invoiceid < 899999999 THEN 'proforma' ELSE 'invoice' END as invoice_type,
   concat(b.firstname,' ',b.lastname) as name,
   b.mageboid", false);
        $this->db->join('a_clients b', 'b.id = a.userid', 'LEFT');
        if (!empty($data['from'])) {
            $this->db->where('a.date >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a.date <=', $data['to']);
        }
        if (is_numeric($data['paymentmethod'])) {
            $this->db->where('a.paymentmethod', $data['paymentmethod']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'invoicenum') {
                $this->db->like('a.invoiceid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('b.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
            }
        }
        $this->db->where('a.companyid', $this->companyid);
        $this->db->from('a_payments a');
        $result = $this->db->get();
        return $result->num_rows();
    }
    public function getProformaTables($data, $download = false)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select("a_invoices.invoicenum, a_invoices.userid,a_invoices.date,a_invoices.duedate,a_invoices.total,a_invoices.id,a_invoices.status,concat(a_clients.firstname, ' ',a_clients.lastname) as customername, a_clients.mvno_id,CASE a_invoices.status WHEN 'Paid' THEN 'light' WHEN 'Unpaid' THEN 'danger' ELSE 'light' END AS color", false);
        $this->db->join('a_clients', 'a_clients.id = a_invoices.userid', 'LEFT');
        if (!empty($data['from'])) {
            $this->db->where('a_invoices.date >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a_invoices.date <=', $data['to']);
        }
        if (is_numeric($data['status'])) {
            $this->db->where('a_invoices.status', $data['status']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'invoicenum') {
                $this->db->like('a_invoices.invoicenum', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('a_clients.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(a_clients.firstname,' ', a_clients.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
            }
        }
        $this->db->where('a_invoices.companyid', $this->companyid);
        $this->db->from('a_invoices');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        if (!$download) {
            $this->db->limit($data['perPage'], $data['page']);
        }
        $result = $this->db->get();
        if ($download) {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return array();
            }
        }
        log_message('error', $this->db->last_query());
        if ($result->num_rows() > 0) {
            return array(
                'total' => $this->getProformaTotalTables($data),
                'data' => $result->result_array()
            );
        } else {
            return array(
                'total' => 0,
                'data' => array()
            );
        }
    }
    public function getProformaTotalTables($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select("a_invoices.invoicenum, a_invoices.userid,a_invoices.date,a_invoices.duedate,a_invoices.total,a_invoices.id,a_invoices.status,concat(a_clients.firstname, ' ',a_clients.lastname) as customername, a_clients.mvno_id", false);
        $this->db->join('a_clients', 'a_clients.id = a_invoices.userid', 'LEFT');
        if (!empty($data['from'])) {
            $this->db->where('a_invoices.date >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a_invoices.date <=', $data['to']);
        }
        if (is_numeric($data['status'])) {
            $this->db->where('a_invoices.status', $data['status']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'invoicenum') {
                $this->db->like('a_invoices.invoicenum', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('a_clients.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(a_clients.firstname,' ', a_clients.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
            }
        }
        $this->db->where('a_invoices.companyid', $this->companyid);
        $this->db->from('a_invoices');
        $result = $this->db->get();
        return $result->num_rows();
    }
    public function getSepaList($companyid, $status, $data, $download = false)
    {
        $this->db = $this->load->database('default', true);
        $total    = 0;
        if (in_array($status, array(
            'Pending',
            'All'
        ))) {
            $this->db->select("id,date_transaction,invoicenumber,transactioncode,amount,iban,concat(end2endid,' ',message) as tol,name_owner", false);
            if (!empty($data['from'])) {
                $this->db->where('date_transaction >=', $data['from']);
            }
            if (!empty($data['to'])) {
                $this->db->where('date_transaction <=', $data['to']);
            }
            if ($data['type'] != 'all') {
                if ($data['type'] == 'iban') {
                    $this->db->like('iban', $data['searchTerm'], 'both');
                } elseif ($data['type'] == 'name_owner') {
                    $this->db->like('name_owner', $data['searchTerm'], 'both');
                } elseif ($data['type'] == 'invoicenumber') {
                    $this->db->like('invoicenumber', $data['searchTerm'], 'both');
                } elseif ($data['type'] == 'message') {
                    $this->db->where("concat(end2endid,' ',message) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
                }
            } else {
                if ($data['searchTerm']) {
                    $this->db->where("(iban LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR name_owner LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR date_transaction LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR concat(end2endid,' ',message) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'  OR amount LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%')");
                }
            }
            if ($status != 'All') {
                $this->db->where('status', 'Pending');
                $this->db->where('amount >', '0');
            }
            $this->db->where('companyid', $companyid);
            $this->db->where_not_in('iban', array(
                'NL39INGB0005300756',
                'NL35RABO0117713678'
            ));
            $this->db->from('a_sepa_items');
            $this->db->order_by($data['orderBy'], $data['orderType']);
            if (!$download) {
                if ($page >0) {
                    $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
                } else {
                    $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
                }
            }
            $q = $this->db->get();
            if ($status != 'All') {
                $total = $this->getPendingTotalTables($companyid, $data);
            } else {
                $total = $this->getAllTotalTables($companyid, $data);
            }
        } elseif ($status == 'Rejection') {
            if ($companyid == 54) {
                $this->db->select("a.date_transaction,a.iban,a.amount,a.return_code as coding,a.iAddressNbr,a.id,a.invoicenumber,a.extra_status,b.mvno_id,b.id as userid,concat(b.firstname,' ',b.lastname) as name, (select count(*) from a_sepa_items x where x.BatchPaymentId = '' and x.status ='Completed' and x.companyid = '" . $this->db->escape_str($companyid) . "'  and x.amount < 0 and x.transactioncode = 'st' and x.iAddressNbr=a.iAddressNbr ) as counter", false);
                $this->db->join('a_clients b', 'b.mageboid = a.iAddressNbr', 'LEFT');
                if (!empty($data['from'])) {
                    $this->db->where('a.date_transaction >=', $data['from']);
                }
                if (!empty($data['to'])) {
                    $this->db->where('a.date_transaction <=', $data['to']);
                }
                if (is_numeric($data['status'])) {
                    $this->db->where('a.extra_status', $data['status']);
                }
                if ($data['type'] != 'all') {
                    if ($data['type'] == 'iInvoiceNbr') {
                        $this->db->like('a.invoicenumber', $data['searchTerm'], 'both');
                    } elseif ($data['type'] == 'mvno_id') {
                        $this->db->like('b.mvno_id', $data['searchTerm'], 'both');
                    } elseif ($data['type'] == 'customername') {
                        $this->db->where("CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
                    }
                } else {
                    if ($data['searchTerm']) {
                        $this->db->where("(a.invoicenumber LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR a.iAddressNbr LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR b.mvno_id LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'  OR a.iban LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%')");
                    }
                }
                $this->db->where('a.BatchPaymentId', '');
                $this->db->where('a.status', 'Completed');
                $this->db->where('a.companyid', $companyid);
                $this->db->where('a.amount <', '0');
                $this->db->where('a.transactioncode', 'st');
                $this->db->from('a_sepa_items a');
                $this->db->order_by($data['orderBy'], $data['orderType']);
                if (!$download) {
                    if ($page >0) {
                        $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
                    } else {
                        $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
                    }
                }
                $q     = $this->db->get();
                $total = $this->getRejectionTotalTables($companyid, $data);
            } else {
                $this->db->select("a.date_transaction,a.iban,a.amount,concat(a.return_code,' ', x.description) as coding,a.iAddressNbr,a.id,a.invoicenumber,a.extra_status,b.mvno_id,b.id as userid,concat(b.firstname,' ',b.lastname) as name, (select count(*) from a_sepa_items x where x.BatchPaymentId is NULL and x.status ='Completed' and x.companyid = '" . $this->db->escape_str($companyid) . "'  and x.amount < 0 and x.iAddressNbr=a.iAddressNbr ) as counter ", false);
                $this->db->join('a_clients b', 'b.mageboid = a.iAddressNbr', 'LEFT');
                $this->db->join('a_sepa_reject_codes x', 'x.rejectcode = a.return_code', 'LEFT');
                if (!empty($data['from'])) {
                    $this->db->where('a.date_transaction >=', $data['from']);
                }
                if (!empty($data['to'])) {
                    $this->db->where('a.date_transaction <=', $data['to']);
                }
                if (is_numeric($data['status'])) {
                    $this->db->where('a.extra_status', $data['status']);
                }
                if ($data['type'] != 'all') {
                    if ($data['type'] == 'invoicenumber') {
                        $this->db->like('a.invoicenumber', $data['searchTerm'], 'both');
                    } elseif ($data['type'] == 'mvno_id') {
                        $this->db->like('b.mvno_id', $data['searchTerm'], 'both');
                    } elseif ($data['type'] == 'customername') {
                        $this->db->where("CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
                    }
                } else {
                    if ($data['searchTerm']) {
                        $this->db->where("(a.invoicenumber LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR a.iAddressNbr LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR b.mvno_id LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'  OR a.iban LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%')");
                    }
                }
                $this->db->where('a.BatchPaymentId IS NULL', null, false);
                $this->db->where('a.status', 'Completed');
                $this->db->where('a.companyid', $companyid);
                $this->db->where('a.amount <', '0');
                $this->db->from('a_sepa_items a');
                $this->db->order_by($data['orderBy'], $data['orderType']);
                if (!$download) {
                    if ($page >0) {
                        $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
                    } else {
                        $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
                    }
                }
                $q     = $this->db->get();
                $total = $this->getRejectionTotalTables($companyid, $data);
            }
        }
        log_message('error', $status . print_r($data, true));
        log_message('error', $this->db->last_query());
        if ($q->num_rows() > 0) {
            return array(
                'total' => $total,
                'data' => $q->result_array()
            );
        } else {
            return array(
                'total' => $total,
                'data' => array()
            );
        }
    }
    public function getPendingTotalTables($companyid, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select("id,date_transaction,transactioncode,amount,iban,concat(end2endid,' ',message) as tol,name_owner", false);
        if (!empty($data['from'])) {
            $this->db->where('date_transaction >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('date_transaction <=', $data['to']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'iban') {
                $this->db->like('iban', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'name_owner') {
                $this->db->like('name_owner', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'invoicenumber') {
                $this->db->like('invoicenumber', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'message') {
                $this->db->where("concat(end2endid,' ',message) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
            }
        } else {
            if ($data['searchTerm']) {
                $this->db->where("(iban LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR name_owner LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR date_transaction LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR concat(end2endid,' ',message) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'  OR amount LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%')");
            }
        }
        $this->db->where('status', 'Pending');
        $this->db->where('companyid', $companyid);
        $this->db->where('amount >', '0');
        $this->db->where_not_in('iban', array(
            'NL39INGB0005300756',
            'NL35RABO0117713678'
        ));
        $this->db->from('a_sepa_items');
        $q = $this->db->get();
        return $q->num_rows();
    }
    public function getAllTotalTables($companyid, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select("id,date_transaction,transactioncode,amount,iban,concat(end2endid,' ',message) as tol,name_owner", false);
        if (!empty($data['from'])) {
            $this->db->where('date_transaction >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('date_transaction <=', $data['to']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'iban') {
                $this->db->like('iban', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'name_owner') {
                $this->db->like('name_owner', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'invoicenumber') {
                $this->db->like('invoicenumber', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'message') {
                $this->db->where("concat(end2endid,' ',message) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
            }
        } else {
            if ($data['searchTerm']) {
                $this->db->where("(iban LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR name_owner LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR date_transaction LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR concat(end2endid,' ',message) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'  OR amount LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%')");
            }
        }
        $this->db->where('companyid', $companyid);
        $this->db->where_not_in('iban', array(
            'NL39INGB0005300756',
            'NL35RABO0117713678'
        ));
        $this->db->from('a_sepa_items');
        $q = $this->db->get();
        return $q->num_rows();
    }
    public function getRejectionTotalTables($companyid, $data)
    {
        $this->db = $this->load->database('default', true);
        if ($companyid == 54) {
            $this->db->select("a.date_transaction,a.iban,a.amount,a.return_code as coding,a.iAddressNbr,a.id,a.invoicenumber,a.extra_status,b.mvno_id,b.id as userid,concat(b.firstname,' ',b.lastname) as name, (select count(*) from a_sepa_items x where x.BatchPaymentId = '' and x.status ='Completed' and x.companyid = '" . $this->db->escape_str($companyid) . "'  and x.amount < 0 and x.transactioncode = 'st' and x.iAddressNbr=a.iAddressNbr ) as counter", false);
            $this->db->join('a_clients b', 'b.mageboid = a.iAddressNbr', 'LEFT');
            if (!empty($data['from'])) {
                $this->db->where('a.date_transaction >=', $data['from']);
            }
            if (!empty($data['to'])) {
                $this->db->where('a.date_transaction <=', $data['to']);
            }
            if (is_numeric($data['status'])) {
                $this->db->where('a.extra_status', $data['status']);
            }
            if ($data['type'] != 'all') {
                if ($data['type'] == 'invoicenumber') {
                    $this->db->like('a.invoicenumber', $data['searchTerm'], 'both');
                } elseif ($data['type'] == 'mvno_id') {
                    $this->db->like('b.mvno_id', $data['searchTerm'], 'both');
                } elseif ($data['type'] == 'customername') {
                    $this->db->where("CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
                }
            } else {
                if ($data['searchTerm']) {
                    $this->db->where("(a.invoicenumber LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR a.iAddressNbr LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR b.mvno_id LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'  OR a.iban LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%')");
                }
            }
            $this->db->where('a.BatchPaymentId', '');
            $this->db->where('a.status', 'Completed');
            $this->db->where('a.companyid', $companyid);
            $this->db->where('a.amount <', '0');
            $this->db->where('a.transactioncode', 'st');
            $this->db->from('a_sepa_items a');
            $q = $this->db->get();
        } else {
            $this->db->select("a.date_transaction,a.iban,a.amount,concat(a.return_code,' ', x.description) as coding,a.iAddressNbr,a.id,a.invoicenumber,a.extra_status,b.mvno_id,b.id as userid,concat(b.firstname,' ',b.lastname) as name, (select count(*) from a_sepa_items x where x.BatchPaymentId is NULL and x.status ='Completed' and x.companyid = '" . $this->db->escape_str($companyid) . "'  and x.amount < 0 and x.iAddressNbr=a.iAddressNbr ) as counter ", false);
            $this->db->join('a_clients b', 'b.mageboid = a.iAddressNbr', 'LEFT');
            $this->db->join('a_sepa_reject_codes x', 'x.rejectcode = a.return_code', 'LEFT');
            if (!empty($data['from'])) {
                $this->db->where('a.date_transaction >=', $data['from']);
            }
            if (!empty($data['to'])) {
                $this->db->where('a.date_transaction <=', $data['to']);
            }
            if (is_numeric($data['status'])) {
                $this->db->where('a.extra_status', $data['status']);
            }
            if ($data['type'] != 'all') {
                if ($data['type'] == 'iInvoiceNbr') {
                    $this->db->like('a.invoicenumber', $data['searchTerm'], 'both');
                } elseif ($data['type'] == 'mvno_id') {
                    $this->db->like('b.mvno_id', $data['searchTerm'], 'both');
                } elseif ($data['type'] == 'customername') {
                    $this->db->where("CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
                }
            } else {
                if ($data['searchTerm']) {
                    $this->db->where("(a.invoicenumber LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR a.iAddressNbr LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR b.mvno_id LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%' OR CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'  OR a.iban LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%')");
                }
            }
            $this->db->where('a.BatchPaymentId IS NULL', null, false);
            $this->db->where('a.status', 'Completed');
            $this->db->where('a.companyid', $companyid);
            $this->db->where('a.amount <', '0');
            $this->db->from('a_sepa_items a');
            $q = $this->db->get();
        }
        return $q->num_rows();
    }
    public function getInvoiceTables($data, $download = false)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select("a_clients.mvno_id,a_tblInvoice.iAddressNbr,a_tblInvoice.iInvoiceNbr,a_tblInvoice.dInvoiceDate,a_tblInvoice.dInvoiceDueDate,a_tblInvoice.mInvoiceAmount,a_clients.id,CASE a_tblInvoice.iInvoiceStatus WHEN '52' THEN 'Unpaid' WHEN '53' THEN 'Sepa Presented' ELSE 'Paid' END AS invoiceStatus,CASE a_tblInvoice.iInvoiceType WHEN '40' THEN 'Invoice' WHEN '41' THEN 'Creditnote' ELSE 'Internal' END AS InvoiceType,a_tblInvoice.iInvoiceType,
         CONCAT(a_clients.firstname,' ', a_clients.lastname) AS name,
            CASE a_tblInvoice.iInvoiceStatus WHEN '52' THEN 'danger' WHEN '53' THEN 'light' ELSE 'light' END AS color
            ", false);
        $this->db->join('a_clients', 'a_clients.mageboid = a_tblInvoice.iAddressNbr', 'LEFT');
        if (!empty($data['from'])) {
            $this->db->where('a_tblInvoice.dInvoiceDate >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a_tblInvoice.dInvoiceDate <=', $data['to']);
        }
        if (is_numeric($data['status'])) {
            $this->db->where('a_tblInvoice.iInvoiceStatus', $data['status']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'iInvoiceNbr') {
                $this->db->like('a_tblInvoice.iInvoiceNbr', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('a_clients.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(a_clients.firstname,' ', a_clients.lastname) LIKE '%" . $this->db->escape_str($data['searchTerm']) . "%'", null, false);
                //$this->db->like('name', $data['searchTerm'], 'both');
            }
            // $this->db->where('a_tblInvoice.iInvoiceStatus', $data['status']);
        }
        $this->db->where('a_tblInvoice.iCompanyNbr', $this->companyid);
        $this->db->where_in('a_tblInvoice.iInvoiceType', array(
            40,
            51
        ));
        $this->db->from('a_tblInvoice');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        if (!$download) {
            if ($page >0) {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
            } else {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
            }
        }
        $result = $this->db->get();
        if ($download) {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return array();
            }
        }
        log_message('error', $this->db->last_query());
        if ($result->num_rows() > 0) {
            return array(
                'total' => $this->getInvoiceTotalTables($data),
                'data' => $result->result_array()
            );
        } else {
            return array(
                'total' => 0,
                'data' => array()
            );
        }
    }
    public function getInvoice($id)
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select a.*,concat(b.firstname,' ',b.lastname) as cName,b.id as userid,b.mvno_id from a_tblInvoice a left join a_clients b on b.mageboid=a.iAddressNbr where a.iInvoiceNbr=? and a.iInvoiceStatus = ?", array(
            $id,
            52
        ));
        if ($q->num_rows() > 0) {
            $res                   = $q->row_array();
            $res['mInvoiceAmount'] = round($res['mInvoiceAmount'], 2);
            return $res;
        } else {
            return array();
        }
    }
    public function getInvoiceTotalTables($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->select("a_tblInvoice.*, CONCAT(a_clients.firstname,' ', a_clients.lastname) AS name,a_clients.mvno_id,a_clients.id", false);
        $this->db->join('a_clients', 'a_clients.mageboid = a_tblInvoice.iAddressNbr', 'LEFT');
        if (!empty($data['from'])) {
            $this->db->where('a_tblInvoice.dInvoiceDate >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a_tblInvoice.dInvoiceDate <=', $data['to']);
        }
        if (is_numeric($data['status'])) {
            $this->db->where('a_tblInvoice.iInvoiceStatus', $data['status']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'iInvoiceNbr') {
                $this->db->like('a_tblInvoice.iInvoiceNbr', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('a_clients.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(a_clients.firstname,' ', a_clients.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
                //$this->db->like('name', $data['searchTerm'], 'both');
            }
        }
        $this->db->where('a_tblInvoice.iCompanyNbr', $this->companyid);
        $this->db->where('a_tblInvoice.iInvoiceType', $data['invoiceType']);
        $this->db->from('a_tblInvoice');
        //$this->db->order_by($data['orderBy'], $data['orderType']);
        //$this->db->limit($data['perPage'], $this->dpost['page']);
        $result = $this->db->get();
        // log_message('error',$this->db->last_query());
        return $result->num_rows();
    }

    public function getServicesTables($status, $data, $download = false)
    {
         log_message('error', 'getServicesTables'.$status);
        if ($status == "mobile") {
            return $this->getMobileTables($data, $download);
        } elseif ($status == "xdsl") {
            return $this->getxDSLTables($data, $download);
        }
    }
    public function getServicesNewTables($status, $data, $download = false)
    {
        log_message('error', 'getMobileTables');
        $this->db->select("a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,identity,d.msisdn as domain,
            d.msisdn_status as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus ", false);
        $this->db->join('a_products b', 'b.id=a.packageid', 'LEFT');
        $this->db->join('a_clients c', 'c.id=a.userid', 'LEFT');
        $this->db->join('a_services_mobile d', 'd.serviceid=a.id', 'LEFT');

        if (!empty($data['from'])) {
            $this->db->where('a.date_created >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a.date_created <=', $data['to']);
        }


             $this->db->where("a.status", "New");


        if ($data['type'] != 'all') {
            if ($data['type'] == 'mageboid') {
                $this->db->like('c.mageboid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('c.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'identity') {
                $this->db->like('a.identity', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'id') {
                $this->db->like('a.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(c.firstname,' ', c.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
            }
        }
        $this->db->where('a.companyid', $this->companyid);
        $this->db->from('a_services a');
        $this->db->group_by('a.id');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        if (!$download) {
            if ($page >0) {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
            } else {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
            }
        }
        $result = $this->db->get();
        if ($download) {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return array();
            }
        }
        log_message('error', $this->db->last_query());
        if ($result->num_rows() > 0) {
            return array(
                'total' => $this->getMobileNewtotalTables($data),
                'data' => $result->result_array()
            );
        } else {
            return array(
                'total' => 0,
                'data' => array()
            );
        }
    }
    public function getMobileNewtotalTables($data)
    {

         $this->db->select("a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,identity,d.msisdn as domain,
            d.msisdn_status as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus ", false);
         $this->db->join('a_products b', 'b.id=a.packageid', 'LEFT');
         $this->db->join('a_clients c', 'c.id=a.userid', 'LEFT');
         $this->db->join('a_services_mobile d', 'd.serviceid=a.id', 'LEFT');

        if (!empty($data['from'])) {
            $this->db->where('a.date_created >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a.date_created <=', $data['to']);
        }


              $this->db->where("a.status", "New");


        if ($data['type'] != 'all') {
            if ($data['type'] == 'mageboid') {
                $this->db->like('c.mageboid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('c.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'identity') {
                $this->db->like('a.identity', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'id') {
                $this->db->like('a.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(c.firstname,' ', c.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
            }
        }
        $this->db->where('a.companyid', $this->companyid);
        $this->db->from('a_services a');
         $result = $this->db->get();
         return $result->num_rows();
    }
    public function getMobileTables($data, $download = false)
    {
        log_message('error', 'getMobileTables');
         $this->db->select("a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,identity,d.msisdn as domain,
            d.msisdn_status as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus ", false);
         $this->db->join('a_products b', 'b.id=a.packageid', 'LEFT');
         $this->db->join('a_clients c', 'c.id=a.userid', 'LEFT');
         $this->db->join('a_services_mobile d', 'd.serviceid=a.id', 'LEFT');

        if (!empty($data['from'])) {
            $this->db->where('a.date_created >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a.date_created <=', $data['to']);
        }

        if (!empty($data['status'])) {
            if ($data['status'] != 'all') {
                $this->db->where("d.msisdn_status", $data['status']);
            }
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'mageboid') {
                $this->db->like('c.mageboid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('c.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'identity') {
                $this->db->like('a.identity', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'id') {
                $this->db->like('a.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(c.firstname,' ', c.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
            }
        }
        $this->db->where('a.companyid', $this->companyid);
        $this->db->from('a_services a');
         $this->db->group_by('a.id');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        if (!$download) {
            if ($page >0) {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
            } else {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
            }
        }
        $result = $this->db->get();
        if ($download) {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return array();
            }
        }
        log_message('error', $this->db->last_query());
        if ($result->num_rows() > 0) {
            return array(
                'total' => $this->getMobiletotalTables($data),
                'data' => $result->result_array()
            );
        } else {
            return array(
                'total' => 0,
                'data' => array()
            );
        }
    }

    public function getXdslTables($data, $download = false)
    {
         $this->db->select("a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,identity,e.circuitid as domain,e.status as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus ", false);
         $this->db->join('a_products b', 'b.id=a.packageid', 'LEFT');
         $this->db->join('a_clients c', 'c.id=a.userid', 'LEFT');
         $this->db->join('a_services_xdsl e', 'e.serviceid=a.id', 'LEFT');

        if (!empty($data['from'])) {
            $this->db->where('b.date_created >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('b.date_created <=', $data['to']);
        }
        if (!empty($data['status'])) {
            if ($data['status'] != 'all') {
                $this->db->where("e.status", $data['status']);
            }
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'mageboid') {
                $this->db->like('c.mageboid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('c.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'identity') {
                $this->db->like('a.identity', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'id') {
                $this->db->like('a.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(c.firstname,' ', c.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
            }
        }
        $this->db->where('a.companyid', $this->companyid);
        $this->db->from('a_services a');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        if (!$download) {
            if ($page >0) {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
            } else {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
            }
        }
        $result = $this->db->get();
        if ($download) {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return array();
            }
        }
        log_message('error', $this->db->last_query());
        if ($result->num_rows() > 0) {
            return array(
                'total' => $this->getXdsltotalTables($data),
                'data' => $result->result_array()
            );
        } else {
            return array(
             'total' => 0,
             'data' => array()
            );
        }
    }


    public function getMobiletotalTables($data)
    {


        $this->db->select("a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,identity,d.msisdn as domain,
            d.msisdn_status as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus ", false);
        $this->db->join('a_products b', 'b.id=a.packageid', 'LEFT');
         $this->db->join('a_clients c', 'c.id=a.userid', 'LEFT');
         $this->db->join('a_services_mobile d', 'd.serviceid=a.id', 'LEFT');

        if (!empty($data['from'])) {
            $this->db->where('a.date_created >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a.date_created <=', $data['to']);
        }
        if (!empty($data['status'])) {
            if ($data['status'] != 'all') {
                $this->db->where("d.msisdn_status", $data['status']);
            }
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'mageboid') {
                $this->db->like('c.mageboid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('c.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'identity') {
                $this->db->like('a.identity', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'id') {
                $this->db->like('a.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(c.firstname,' ', c.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
            } elseif ($data['type'] == 'phonenumber') {
                $this->db->like('c.phonenumber', $data['searchTerm'], 'both');
            }
        }
        $this->db->where('a.companyid', $this->companyid);
        $this->db->from('a_services a');
         $result = $this->db->get();

         return $result->num_rows();
    }


    public function getXdsltotalTables($data)
    {


         $this->db->select("a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, a.date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,identity,e.circuitid as domain,e.status as orderstatus, (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus ", false);
         $this->db->join('a_products b', 'b.id=a.packageid', 'LEFT');
         $this->db->join('a_clients c', 'c.id=a.userid', 'LEFT');
          $this->db->join('a_services_xdsl e', 'e.serviceid=a.id', 'LEFT');

        if (!empty($data['from'])) {
            $this->db->where('b.date_created >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('b.date_created <=', $data['to']);
        }

        if (!empty($data['status'])) {
            if ($data['status'] != 'all') {
                $this->db->where("e.status", $data['status']);
            }
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'mageboid') {
                $this->db->like('c.mageboid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('c.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'identity') {
                $this->db->like('a.identity', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'id') {
                $this->db->like('a.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(c.firstname,' ', c.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
            }
        }
        $this->db->where('a.companyid', $this->companyid);
        $this->db->from('a_services a');
         $result = $this->db->get();

         return $result->num_rows();
    }
    public function getResellerTables($data, $download = false)
    {
        $this->db->select("b.id,b.agent,b.contact_name,b.last_month_earning,comission_type,b.comission_value,b.email,b.phonenumber,concat(b.address1,' ',b.postcode,' ',b.city) as address, (select count(x.id) from a_services x left join a_clients v on v.id=x.userid where v.agentid=b.id and x.status = 'Active') as counter,(select  ROUND(sum(xx.recurring)*(b.comission_value/100),2) from a_services xx left join a_clients vv on vv.id=xx.userid where vv.agentid=b.id and xx.status = 'Active') as amount", false);
        if (is_numeric($data['id'])) {
            $this->db->where('b.id', $data['id']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'id') {
                $this->db->like('b.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'name') {
                $this->db->like('b.agent', $data['searchTerm'], 'both');
            }
        }
        $this->db->where('b.companyid', $this->companyid);
        $this->db->from('a_clients_agents b');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        if (!$download) {
            if ($page >0) {
                 $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
            } else {
                 $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
            }
        }
        $result = $this->db->get();
        if ($download) {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return array();
            }
        }
        log_message('error', $this->db->last_query());
        if ($result->num_rows() > 0) {
            return array(
                'total' => $this->getResellerTotalTables($data),
                'data' => $result->result_array()
            );
        } else {
            return array(
                'total' => 0,
                'data' => array()
            );
        }
    }

    public function getResellerTotalTables($data)
    {
         $this->db->select("b.id,b.agent,b.contact_name,b.last_month_earning,comission_type,b.comission_value,b.email,b.phonenumber,concat(b.address1,' ',b.postcode,' ',b.city) as address, (select count(x.id) from a_services x left join a_clients v on v.id=x.userid where v.agentid=b.id and x.status = 'Active') as counter,(select  ROUND(sum(xx.recurring)*(b.comission_value/100),2) from a_services xx left join a_clients vv on vv.id=xx.userid where vv.agentid=b.id and xx.status = 'Active') as amount", false);


        if (is_numeric($data['id'])) {
            $this->db->where('b.id', $data['id']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'id') {
                $this->db->like('b.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'name') {
                $this->db->like('b.agent', $data['searchTerm'], 'both');
            }
        }
        $this->db->where('b.companyid', $this->companyid);
        $this->db->from('a_clients_agents b');
        $q =  $this->db->get();
        return $q->num_rows();
    }
    public function getClientsTables($data, $download = false)
    {
        $this->db->select("b.id,b.mageboid,b.mvno_id,b.salutation,b.initial,b.firstname,b.lastname,b.companyname,b.vat,b.email,b.address1,b.postcode,b.city,b.country,b.language,b.phonenumber,b.paymentmethod,b.payment_duedays,
    b.date_birth,b.vat_rate,b.agentid,b.gender,b.refferal,b.location,(select count(*) from a_services a where a.status='Active' and a.userid=b.id) as active_service, (select count(*) from a_services a where a.status in('Pending','New') and a.userid=b.id) as pending_service", false);
        if (!empty($data['from'])) {
            $this->db->where('b.date_created >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('b.date_created <=', $data['to']);
        }
        if (is_numeric($data['postcode'])) {
            $this->db->where('b.postcode', $data['status']);
        }
        if (is_numeric($data['agentid'])) {
            $this->db->where('b.agentid', $data['status']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'mageboid') {
                $this->db->like('b.mageboid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('b.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'id') {
                $this->db->like('b.id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(b.firstname,' ', b.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
            } elseif ($data['type'] == 'phonenumber') {
                $this->db->like('b.phonenumber', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'city') {
                $this->db->like('b.city', $data['searchTerm'], 'both');
            }
        }
        $this->db->where('b.companyid', $this->companyid);
        $this->db->from('a_clients b');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        if (!$download) {
            if ($page >0) {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']) + 1);
            } else {
                $this->db->limit($data['perPage'], ($data['page'] * $data['perPage']));
            }
        }
        $result = $this->db->get();
        if ($download) {
            if ($result->num_rows() > 0) {
                return $result->result_array();
            } else {
                return array();
            }
        }
        log_message('error', $this->db->last_query());
        if ($result->num_rows() > 0) {
            return array(
                'total' => $this->getClientTotalTables($data),
                'data' => $result->result_array()
            );
        } else {
            return array(
                'total' => 0,
                'data' => array()
            );
        }
    }
    public function getClientTotalTables($data)
    {
        $this->db->select("a_clients.*", false);
        if (!empty($data['from'])) {
            $this->db->where('a_clients.date_created >=', $data['from']);
        }
        if (!empty($data['to'])) {
            $this->db->where('a_clients.date_created <=', $data['to']);
        }
        if (is_numeric($data['postcode'])) {
            $this->db->where('a_clients.postcode', $data['status']);
        }
        if (is_numeric($data['agentid'])) {
            $this->db->where('a_clients.agentid', $data['status']);
        }
        if ($data['type'] != 'all') {
            if ($data['type'] == 'mageboid') {
                $this->db->like('a_clients.mageboid', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'mvno_id') {
                $this->db->like('a_clients.mvno_id', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'customername') {
                $this->db->where("CONCAT(a_clients.firstname,' ', a_clients.lastname) LIKE '%" . $data['searchTerm'] . "%'", null, false);
            } elseif ($data['type'] == 'phonenumber') {
                $this->db->like('a_clients.phonenumber', $data['searchTerm'], 'both');
            } elseif ($data['type'] == 'city') {
                $this->db->like('a_clients.city', $data['searchTerm'], 'both');
            }
        }
        $this->db->where('a_clients.companyid', $this->companyid);
        $this->db->from('a_clients');
        $this->db->order_by($data['orderBy'], $data['orderType']);
        $result = $this->db->get();
        return $result->num_rows();
    }
    public function addClient($data)
    {
        $this->db->insert('a_clients', $data);
        return $this->db->insert_id();
    }
    public function getClient($companyid, $id)
    {
        $q = $this->db->query("SELECT a.*,b.agent as agent_name from a_clients a left join a_clients_agents b on b.id=a.agentid where a.id=? and a.companyid=?", array(
            $id,
            $companyid
        ));
        if ($q->num_rows() > 0) {
            $res           = $q->row_array();
            $res['stats']  = $this->getClientStats($q->row());
            $res['result'] = "success";
            return $res;
        } else {
            return array(
                'result' => 'error',
                'message' => 'client not found',
                'id' => $id
            );
        }
    }
    public function update_present($id)
    {
        $this->db->query('update a_admin set lastseen =? where id=?', array(
            time(),
            $id
        ));
    }
    public function isEmailexisit($email)
    {
        $q = $this->db->query("select * from a_clients where email like ? and companyid=?", array(
            $email,
            $this->session->cid
        ));
        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function getInvoices($companyid)
    {
        $id = false;
        if (!empty($this->uri->segment(5))) {
            $id = $this->uri->segment(5);
        }
        $this->db = $this->load->database('default', true);
        if ($id) {
            $q = $this->db->query("select b.id as userid,b.mvno_id,a.*,b.firstname,b.lastname,b.companyname from a_tblInvoice a left join a_clients b on b.mageboid=a.iAddressNbr where a.iCompanyNbr=? and b.id=? order by a.iInvoiceNbr desc", array(
                $companyid,
                $id
            ));
        } else {
            $q = $this->db->query("select b.id as userid,b.mvno_id,a.*,b.firstname,b.lastname,b.companyname from a_tblInvoice a left join a_clients b on b.mageboid=a.iAddressNbr where a.iCompanyNbr=? order by a.iInvoiceNbr desc", array(
                $companyid
            ));
        }
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getAgents($companyid)
    {
        $q = $this->db->query('select isdefault,id,agent from a_clients_agents where companyid=?', array(
            $companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            $this->db->insert('a_clients_agents', array(
                'companyid' => $companyid,
                'agent' => 'Default Agent',
                'contact_name' => 'Default Agent',
                'isdefault' => 1
            ));
            $id = $this->db->insert_id();
            return array(
                array(
                    'id' => $id,
                    'agent' => 'Default Agent',
                    'isdefault' => 1
                )
            );
        }
    }
    public function getEmails($companyid, $data = false)
    {
        $q = $this->db->query("SELECT a.id,a.to,a.subject,a.status,a.date,a.userid
      FROM a_email_log a
      WHERE userid=?
      ORDER BY id DESC", array(
            $data['id']
        ));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getLogs($companyid, $data = false)
    {
        $q = $this->db->query("SELECT DATE_FORMAT(a.date, '%Y-%m-%d %H:%i:%s') as date,a.id,a.description,a.user,a.ip,a.serviceid
    FROM  a_logs a
    where a.userid=?
    ORDER BY a.id DESC", array(
            $data['id']
        ));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getContacts($companyid, $data = false)
    {
        $q = $this->db->query("  SELECT id,concat(firstname,' ',lastname) as fullname,companyname as role,email,phonenumber,gsm
    FROM a_clients_contacts
    where userid=?
    ORDER BY id DESC", array(
            $data['id']
        ));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getNotes($companyid, $data = false)
    {
        $q = $this->db->query("SELECT a.created,a.admin,a.note,a.id,case when a.sticky = '1' THEN 'YES' else 'NO' end as sticky
    FROM a_clients_notes a
    where a.userid=?
    ORDER BY a.id DESC", array(
            $data['id']
        ));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getSupports($companyid, $data, $clientid = false)
    {
        $where = "";
        $terms = $data['keyword'];
        if ($data['filterBy']) {
            if ($data['keyword']) {
                if ($data['filterBy'] == 'department') {
                    $where .= " AND (c.name like '%" . $terms . "%')";
                } elseif ($data['filterBy'] == 'name') {
                    $where .= " AND (e.name like '%" . $terms . "%')";
                } elseif ($data['filterBy'] == 'tid') {
                    $where .= " AND (e.name like '%" . $terms . "%')";
                } elseif ($data['filterBy'] == 'status') {
                    $where .= " AND (a.status like '%" . $terms . "%')";
                } elseif ($data['filterBy'] == 'subject') {
                    $where .= " AND (a.subject like '%" . $terms . "%')";
                } elseif ($data['filterBy'] == 'date') {
                    $where .= " AND (a.date like '%" . $terms . "%')";
                } else {
                    $where .= " AND (a.subject like '%" . $terms . "%' OR e.name like '%" . $terms . "%' OR a.date like '%" . $terms . "%' OR c.name like '%" . $terms . "%')";
                }
            }
        } else {
            $where = " AND (a.subject like '%" . $terms . "%' OR e.name like '%" . $terms . "%' OR a.date like '%" . $terms . "%' OR c.name like '%" . $terms . "%')";
        }
        if ($clientid) {
            $where .= " AND a.userid=" . $clientid . " ";
        }
        $orderby = " order by " . $data['orderBy'] . " " . $data['orderType'];
        log_message('error', $where . " " . $orderby);
        if (!empty($this->uri->segment(5))) {
            $q = $this->db->query("SELECT a.id,a.tid as ticketid,a.subject as title,a.userid,a.status as status,a.date,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as dept,concat(d.firstname,' ', d.lastname) as assigne, e.name as category
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category e on e.id=a.categoryid
    WHERE a.companyid=?
    " . $where . "
    and a.userid=?" . $orderby, array(
                $companyid,
                $this->uri->segment(5)
            ));
        } else {
            $q = $this->db->query("SELECT a.id,a.tid as ticketid,a.subject as title,a.userid,a.status as status,a.date,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as dept,concat(d.firstname,' ', d.lastname) as assigne, e.name as category
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
     left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category e on e.id=a.categoryid
    WHERE a.companyid=?
    " . $where . "
    " . $orderby, array(
                $companyid
            ));
        }
        if ($q->num_rows() > 0) {
            return $q->result_array();
        } else {
            return array();
        }
    }
    public function getClientStats($client)
    {
        $stats['invoice_paid']   = "0.00";
        $stats['invoice_unpaid'] = "0.00";
        $stats['active_service'] = "0";
        $stats['open_ticket']    = "0";
        $stats['closed_ticket']  = 0;
        $this->db                = $this->load->database('default', true);
        $tickets                 = $this->db->query('select status from a_helpdesk_tickets where userid=?', array(
            $client->id
        ));
        if ($tickets->num_rows() > 0) {
            foreach ($tickets->result() as $row) {
                if (in_array($row->status, array(
                    "Open",
                    "Awaiting-Reply",
                    "On-Hold",
                    "In-Progress"
                ))) {
                    $stats['open_ticket'] = $stats['open_ticket'] + 1;
                } else {
                    $stats['closed_ticket'] = $stats['closed_ticket'] + 1;
                }
            }
        }
        $services = $this->db->query('select status from a_services where userid=?', array(
            $client->id
        ));
        if ($services->num_rows() > 0) {
            foreach ($services->result() as $row) {
                if ($row->status == "Active") {
                    $stats['active_service'] = $stats['active_service'] + 1;
                }
            }
        }
        $a = $this->getInvoiceStats($client);
        return (object) array_merge($stats, $a);
    }
    public function getInvoiceStats($client)
    {
        $this->db                = $this->load->database('magebo', true);
        $stats['invoice_unpaid'] = "0";
        $stats['invoice_paid']   = "0";
        $stats['count_unpaid']   = "0";
        $stats['count_paid']     = "0";
        if ($client->mageboid > 0) {
            $invoices = $this->db->query('select iInvoiceNbr,iInvoiceStatus,mInvoiceAmount from tblInvoice where iAddressNbr=? and iInvoiceType =?', array(
                $client->mageboid,
                40
            ));
            if ($invoices->num_rows() > 0) {
                foreach ($invoices->result() as $i) {
                    if ($i->iInvoiceStatus == "54") {
                        $stats['invoice_paid'] = $i->mInvoiceAmount + $stats['invoice_paid'];
                        $stats['count_paid']   = $stats['count_paid'] + 1;
                    } else {
                        $stats['invoice_unpaid'] = $i->mInvoiceAmount + $stats['invoice_unpaid'];
                        $stats['count_unpaid']   = $stats['count_unpaid'] + 1;
                    }
                }
            }
        }
        $stats['invoice_paid']   = round($stats['invoice_paid'], 2);
        $stats['invoice_unpaid'] = round($stats['invoice_unpaid'], 2);
        return $stats;
    }
    public function getclientotal($cid, $keywords, $type)
    {
        $keywords = '%' . $keywords . '%';
        if (!empty($keywords)) {
            if ($type != "all") {
                $sql = "SELECT * FROM a_clients WHERE companyid = ? AND ?  LIKE ?";
                $tt  = $this->db->query($sql, array(
                    $cid,
                    $type,
                    $keywords
                ));
            } else {
                $sql = "SELECT * FROM a_clients WHERE companyid = ? AND ( firstname LIKE ? OR lastname LIKE ?  OR companyname LIKE  ? OR email LIKE  ? OR mageboid LIKE  ?  OR mvno_id LIKE  ? OR id LIKE ? OR address1 LIKE  ? )";
                $tt  = $this->db->query($sql, array(
                    $cid,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords,
                    $keywords
                ));
            }
        } else {
            $tt = $this->db->query("SELECT * FROM a_clients WHERE companyid = ?", array(
                $cid
            ));
        }
        return $tt->num_rows();
    }
    public function getService($companyid, $id)
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select a.future_activation,d.date_ported,a.notes,b.proforma,b.setup,b.mobile_swapcost,b.mobile_changecost,c.companyid,a.iGeneralPricingIndex,a.type,b.PriceListSubType,b.PriceList,a.promocode,a.userid,a.id,a.packageid,a.date_contract,a.status,a.userid,a.recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,a.date_created as regdate,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,h.promo_value,h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,a.suspend_reason,a.contract_terms,x.factsheet_name,x.url as factsheet_url,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
       case when a.type  = 'mobile' THEN d.msisdn_status
       when a.type = 'xdsl' then e.status
       when a.type = 'voip' then f.status
       else 'Unknown' end as orderstatus,
       case when a.type  = 'mobile' THEN 'NA'
       when a.type = 'xdsl' then e.proximus_orderid
       when a.type = 'voip' then 'NA'
       else 'Unknown' end as proximusid,
       b.gid,bb.name as product_brand,a.date_terminate
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_products_group bb on bb.id=b.gid
    left join a_products_factsheet x on b.id=x.productid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on e.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    left join a_promotion h on h.promo_code=a.promocode
    where (a.status != 'New' or a.status != 'Cancelled' or a.status != 'Terminated')
    and a.id=?
    and a.companyid = ?
    group by a.id", array(
            $id,
            $companyid
        ));
        //    $q = $this->db->query("select a.*,b.name,a.amount as recurring, b.servertype from tblhosting a left join tblproducts b on b.id=a.packageid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $res                            = $q->row_array();
            $d                              = explode('-', $res['date_contract']);
            $res['date_contract']           = $d['2'] . '-' . $d['0'] . '-' . $d['1'];
            $res['first_date_cancellation'] = calculate_contractend_date($res['date_contract'], $res['contract_terms']);
            $res['porting']                 = $this->getifPorting($id);
            $res['details']                 = $this->getServicedetail($id, $res['type']);
            $res['addons']                  = $this->getSubscriptionAddons('others', $id);
            $res['planned_changes']         = $this->hasProductChange($id);
            $res['options']                 = $this->getSubscriptionAddons('option', $id);

           // log_message('error', print_r($res['options'], 1));
            foreach ($res['options'] as $r) {
                    $res['opts'][] = $r->arta_bundleid;
            }
             log_message('error', print_r($res['opts'], 1));
            if ($res['type'] == "xdsl") {
                $res['xdsl'] = $this->getServicexDSLProducts($res['proximusid']);
            }
            return (object) $res;
        } else {
            return array();
        }
    }
    public function getServicedetail($serviceid, $type)
    {
        $this->db->where('serviceid', $serviceid);
        $q = $this->db->get('a_services_' . trim($type));
        return $q->row();
    }
    public function hasProductChange($id)
    {
        $q = $this->db->query('select a.*,b.name as new_package from a_subscription_changes a left join a_products b on b.id=a.new_pid where a.serviceid=? and a.date_commit > ?', array(
            $id,
            date('Y-m-d')
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function getServiceXdslProducts($orderid)
    {
        $this->db->where('orderid', $orderid);
        $q = $this->db->get('a_services_xdsl_products');
        return $q->result();
    }
    public function getifPorting($serviceid)
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from a_services_mobile where msisdn_type=? and serviceid=?", array(
            'porting',
            $serviceid
        ));
        if ($q->num_rows() > 0) {
            return (object) array(
                'porting' => true,
                'number' => $q->row()->donor_msisdn
            );
        } else {
            return (object) array(
                'porting' => false
            );
        }
    }
    public function getSubscriptionAddons($type, $serviceid)
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from a_services_addons where addon_type=? and serviceid=?", array(
            $type,
            $serviceid
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function getservicetotal($cid, $status, $keywords, $packageid = 0)
    {
        if (!empty($keywords)) {
            $keywords = "%" . $keywords . "%";
            $where    = " AND (a.id like " . $keywords . " OR d.msisdn like " . $keywords . " OR d.msisdn_sn like " . $keywords . "  OR d.msisdn_sim like " . $keywords . "  OR e.circuitid like " . $keywords . "  OR e.proximus_orderid like " . $keywords . " ) ";
        } else {
            $where = "";
        }
        // id,domain,packagename,date,customername
        $type      = $_GET['type'];
        $packageid = $_GET['packageid'];
        $status    = $_GET['status'];
        if ($page > 1) {
            $limit = "10, " . $offset;
        } else {
            $limit = "10";
        }
        $order     = $_GET['orderby'];
        $orderType = $_GET['ordertype'];
        // id,domain,packagename,date,customername
        if ($type == "id") {
            $where = " and a.id like '%" . $keywords . "'";
        } elseif ($order == "domain") {
            $where = " and a.id like '%" . $keywords . "'";
        } elseif ($order == "packagename") {
            $where = " and b.name like '%" . $keywords . "'";
        } elseif ($order == "date") {
            $where = " and a.date_contract like '%" . $keywords . "'";
        } elseif ($order == "customername") {
            $where = " and c.firstname like '%" . $keywords . "'";
        } else {
            $where = " ";
        }
        if ($order == "id") {
            $orderby = " a.id " . $orderType;
        } elseif ($order == "domain") {
            $orderby = " a.id " . $orderType;
        } elseif ($order == "packagename") {
            $orderby = " b.name " . $orderType;
        } elseif ($order == "date") {
            $orderby = " a.date_contract " . $orderType;
        } elseif ($order == "customername") {
            $orderby = " c.firstname " . $orderType;
        } else {
            $orderby = " " . $order . " " . $orderType;
        }
        if ($status != '') {
            if ($_GET["status"] == "pendingactivation") {
                $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
            case when a.type  = 'mobile' THEN d.msisdn
            when a.type = 'xdsl' then e.circuitid
            when a.type = 'voip' then f.cli
            else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
            when a.type = 'xdsl' then e.status
            when a.type = 'voip' then f.status
            else 'Unknown' end as orderstatus
         from a_services a
         left join a_products b on b.id=a.packageid
         left join a_clients c on c.id=a.userid
         left join a_services_mobile d on d.serviceid=a.id
         left join a_services_xdsl e on e.serviceid=a.id
         left join a_services_voip f on f.serviceid=a.id
         where a.status = 'Pending'
         and a.companyid = ?
           " . $where . "
         group by a.id order by " . $orderby . " limit " . $limit;
                $q   = $this->db->query($sql, array(
                    $cid
                ));
            } elseif ($_GET["status"] == "cancelled") {
                $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid
            case when a.type  = 'mobile' THEN d.msisdn
            when a.type = 'xdsl' then e.circuitid
            when a.type = 'voip' then f.cli
            else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
            when a.type = 'xdsl' then e.status
            when a.type = 'voip' then f.status
            else 'Unknown' end as orderstatus
         from a_services a
         left join a_products b on b.id=a.packageid
         left join a_clients c on c.id=a.userid
         left join a_services_mobile d on d.serviceid=a.id
         left join a_services_xdsl e on e.serviceid=a.id
         left join a_services_voip f on f.serviceid=a.id
         where a.status in ('Cancelled','Terminated')
         and a.companyid = ?
         " . $where . "
        group by a.id  order by " . $orderby . "  limit " . $limit;
                $q   = $this->db->query($sql, array(
                    $cid
                ));
            } elseif ($_GET["status"] == "needactions") {
                $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
         case when a.type  = 'mobile' THEN d.msisdn
         when a.type = 'xdsl' then e.circuitid
         when a.type = 'voip' then f.cli
         else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
         when a.type = 'xdsl' then e.status
         when a.type = 'voip' then f.status
         else 'Unknown' end as orderstatus
      from a_services a
      left join a_products b on b.id=a.packageid
      left join a_clients c on c.id=a.userid
      left join a_services_mobile d on d.serviceid=a.id
      left join a_services_xdsl e on e.serviceid=a.id
      left join a_services_voip f on f.serviceid=a.id
      where a.status in ('Cancelled','Terminated')
      and a.companyid = ?
       " . $where . "
      group by a.id order by " . $orderby . "  limit " . $limit;
                $q   = $this->db->query($sql, array(
                    $cid
                ));
            } else {
                $sql = "select a.id,a.status as orderstatus,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
            case when a.type  = 'mobile' THEN d.msisdn
            when a.type = 'xdsl' then e.circuitid
            when a.type = 'voip' then f.cli
            else 'Unknown' end as domain,
            case when a.type  = 'mobile' THEN d.msisdn_status
            when a.type = 'xdsl' then e.status
            when a.type = 'voip' then f.status
            else 'Unknown' end as status
         from a_services a
         left join a_products b on b.id=a.packageid
         left join a_clients c on c.id=a.userid
         left join a_services_mobile d on d.serviceid=a.id
         left join a_services_xdsl e on e.serviceid=a.id
         left join a_services_voip f on f.serviceid=a.id
         where a.status = ?
         and a.companyid = ?
           " . $where . "
       group by a.id   order by " . $orderby . "  limit " . $limit;
                $q   = $this->db->query($sql, array(
                    $status,
                    $cid
                ));
            }
        } else {
            $sql = "select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
         case when a.type  = 'mobile' THEN d.msisdn
         when a.type = 'xdsl' then e.circuitid
         when a.type = 'voip' then f.cli
         else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
         when a.type = 'xdsl' then e.status
         when a.type = 'voip' then f.status
         else 'Unknown' end as orderstatus
      from a_services a
      left join a_products b on b.id=a.packageid
      left join a_clients c on c.id=a.userid
      left join a_services_mobile d on d.serviceid=a.id
      left join a_services_xdsl e on e.serviceid=a.id
      left join a_services_voip f on f.serviceid=a.id
      where a.status in ('Pending','Active')
      and a.companyid = ?
        " . $where . "
      group by a.id  order by " . $orderby . " limit " . $limit;
            $q   = $this->db->query($sql, array(
                $cid
            ));
        }
        return $q->num_rows();
    }
    public function getServices($id)
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select d.date_ported,a.notes,b.setup,b.mobile_swapcost,b.mobile_changecost,c.companyid,a.iGeneralPricingIndex,a.type,b.PriceListSubType,b.PriceList,a.promocode,a.userid,a.id,a.packageid,a.date_contract,a.status,a.userid,case when a.recurring is not null THEN round(a.recurring)
       else '0.00' end as recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,h.promo_value,h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,a.suspend_reason,a.contract_terms,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
       case when a.type  = 'mobile' THEN d.msisdn_sn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as sn,
       case when a.type  = 'mobile' THEN d.msisdn_status
       when a.type = 'xdsl' then e.status
       when a.type = 'voip' then f.status
       else 'Unknown' end as orderstatus,
       case when a.type  = 'mobile' THEN 'NA'
       when a.type = 'xdsl' then e.proximus_orderid
       when a.type = 'voip' then 'NA'
       else 'Unknown' end as proximusid,
       b.gid,bb.name as product_brand,a.date_terminate,
        (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_products_group bb on bb.id=b.gid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    left join a_promotion h on h.promo_code=a.promocode
    where (a.status != 'New' or a.status != 'Cancelled' or a.status != 'Terminated')
    and a.userid=?
    group by a.id", array(
            $id
        ));
        //    $q = $this->db->query("select a.*,b.name,a.amount as recurring, b.servertype from tblhosting a left join tblproducts b on b.id=a.packageid where a.id=?", array($id));
        if ($q->num_rows() > 0) {
            $res = $q->result_array();
            return $res;
        } else {
            return array();
        }
    }
    public function getMSubscription($id)
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query('select * from a_subscriptions_manual where userid=?', array(
            $id
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function getNextInvoices($id)
    {
        $this->db = $this->load->database('magebo', true);
        $q        = $this->db->query("SELECT iGeneralPricingIndex, iAddressNbr, iInvoiceGroupNbr, rNumber, mUnitPrice, rVATPercentage, iTotalMonths as terms, iTotalMonths, dContractDate, iStartMonth, iStartYear, bEnabled, cRemark, iShowRemarkOnReport, cInvoiceDescription, bIncludePeriodDescription, dLastUpdate, iLastUserNbr
FROM tblGeneralPricing where iAddressNbr=? and iTotalMonths >0 and bEnabled=1 and iInvoiceGroupNbr != 350", array(
            $id
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function getMandateId($id)
    {
        $res      = array(
            'SEPA_MANDATE' => '',
            'IBAN' => '',
            'BIC' => '',
            'SEPA_MANDATE_SIGNATURE_DATE' => '',
            'SEPA_STATUS' => ''
        );
        $this->db = $this->load->database('magebo', true);
        $q        = $this->db->query("SELECT a.iAddressDataIndex, a.iTypeNbr, b.cTypeDescription, a.cAddressData FROM tblAddressData a left join tblType b on b.iTypeNbr=a.iTypeNbr where  a.bPreferredOrActive=1 and a.iAddressNbr=? and b.iTypeGroupNbr=?", array(
            $id,
            5
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                if (!empty($row)) {
                    $res[str_replace(' ', '_', $row->cTypeDescription)] = $row->cAddressData;
                }
            }
            return (object) $res;
        } else {
            return (object) array(
                'SEPA_MANDATE' => '',
                'IBAN' => '',
                'BIC' => '',
                'SEPA_MANDATE_SIGNATURE_DATE' => '',
                'SEPA_STATUS' => ''
            );
        }
    }
    public function addNote($data)
    {
        $this->db->insert('a_clients_notes', $data);
        return array(
            'result' => $this->db->insert_id()
        );
    }
    public function getBic($iban)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://localhost:8080/validate/' . $iban . '?getBIC=true');
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        return json_decode($response);
    }
    public function update_sepa($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $data['userid']);
        $this->db->update('a_clients', array(
            'iban' => $data['iban'],
            'bic' => $data['bic'],
            'paymentmethod' => 'directdebit'
        ));
        //$this->db->query("update a_clients set iban=?, bic=? where id=?", array(strtoupper(trim($_POST['iban'])), $_POST['bic'], $_POST['userid']));
    }
    public function getBicTry($iban)
    {
        //curl https://api.bank.codes/iban/json/0f86d5d0b3472a85e53dbd91e6e4aa08/ES4221002646800110397817/
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.bank.codes/iban/json/0f86d5d0b3472a85e53dbd91e6e4aa08/' . $iban);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        return json_decode($response);
    }
    public function UpdateClient($companyid, $data)
    {
        unset($data['admin']);
        $data['date_modified'] = date('Y-m-d H:i:s');
        $this->db->where('companyid', $companyid);
        $this->db->where('id', $data['id']);
        $this->db->update('a_clients', $data);
        if ($this->db->affected_rows() > 0) {
            return array(
                'result' => true,
                'message' => 'Client information has been updated'
            );
        } else {
            return array(
                'result' => true,
                'message' => 'Client information was not updated'
            );
        }
    }
    public function getInvoicebyMonth($cid)
    {
        $this->db = $this->load->database('magebo', true);
        $total    = $this->db->query("SELECT CONVERT(NVARCHAR(7), dInvoiceDate, 120) [Month], SUM(mInvoiceAmount) [TotalAmount]
FROM tblInvoice a
LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressNbr
WHERE b.iCompanyNbr=?
and a.dInvoiceDate >= '2019-01-01'
and a.iInvoiceType = 40
GROUP BY CONVERT(NVARCHAR(7), dInvoiceDate, 120)
ORDER BY [Month] ASC", array(
            $cid
        ));
        $paid     = $this->db->query("SELECT CONVERT(NVARCHAR(7), dInvoiceDate, 120) [Month], SUM(mInvoiceAmount) [TotalAmount]
FROM tblInvoice a
LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressNbr
WHERE b.iCompanyNbr=?
and a.dInvoiceDate >= '2019-01-01'
and a.iInvoiceType = 40
and a.iInvoiceStatus in (53,54)
GROUP BY CONVERT(NVARCHAR(7), dInvoiceDate, 120)
ORDER BY [Month] ASC", array(
            $cid
        ));
        $unpaid   = $this->db->query("SELECT CONVERT(NVARCHAR(7), dInvoiceDate, 120) [Month], SUM(mInvoiceAmount) [TotalAmount]
FROM tblInvoice a
LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressNbr
WHERE b.iCompanyNbr=?
and a.dInvoiceDate >= '2019-01-01'
and a.iInvoiceType = 40
and a.iInvoiceStatus = 52
GROUP BY CONVERT(NVARCHAR(7), dInvoiceDate, 120)
ORDER BY [Month] ASC", array(
            $cid
        ));
        $cn       = $this->db->query("SELECT CONVERT(NVARCHAR(7), dInvoiceDate, 120) [Month], SUM(mInvoiceAmount) [TotalAmount]
FROM tblInvoice a
LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressNbr
WHERE b.iCompanyNbr=?
and a.dInvoiceDate >= '2019-01-01'
and a.iInvoiceType = 41
GROUP BY CONVERT(NVARCHAR(7), dInvoiceDate, 120)
ORDER BY [Month] ASC", array(
            $cid
        ));
        // $x['label'] =array('PAID', 'UPAID','CREDITNOTE');
        if ($total->num_rows() > 0) {
            foreach ($total->result_array() as $row) {
                //$html[] = "['".substr($row['Month'], 0, 7)."', ".round($row['TotalAmount'], 2)."]";
                //$html[substr($row['Month'], 0, 7)][0] =  round($row['TotalAmount'], 2);
                $html['Label'][] = substr($row['Month'], 0, 7);
            }
        }
        if ($paid->num_rows() > 0) {
            //$html[] ="['Month', 'Invoices']";
            foreach ($paid->result_array() as $row) {
                ///round($row['TotalAmount'], 2)
                //$html[] = "['".substr($row['Month'], 0, 7)."', ".round($row['TotalAmount'], 2)."]";
                $html['Paid'][] = round($row['TotalAmount'], 2);
            }
        }
        if ($unpaid->num_rows() > 0) {
            //$html[] ="['Month', 'Invoices']";
            foreach ($unpaid->result_array() as $row) {
                //$html[] = "['".substr($row['Month'], 0, 7)."', ".round($row['TotalAmount'], 2)."]";
                //$html[substr($row['Month'], 0, 7)][2]  =  round($row['TotalAmount'], 2);
                $html['Unpaid'][] = round($row['TotalAmount'], 2);
            }
        }
        if ($cn->num_rows() > 0) {
            //$html[] ="['Month', 'Invoices']";
            foreach ($cn->result_array() as $row) {
                //$html[] = "['".substr($row['Month'], 0, 7)."', ".round($row['TotalAmount'], 2)."]";
                // $html[substr($row['Month'], 0, 7)][3]  =  round($row['TotalAmount'], 2)*-1;
                $html['Creditnote'][] = round($row['TotalAmount'], 2) * -1;
            }
        }
        return $html;
    }
    public function getSubscriptionActiveByMonth($cid)
    {
        $date = new DateTime(date('Y-m-d'));
        $date->modify('-6 month');
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("SELECT date_created, COUNT(id) as total FROM a_services where date_created > ? and companyid=? and status not in ('Cancelled','Terminated') GROUP BY MONTH(date_created) order by date_created asc", array(
            $date->format('Y-m-d'),
            $cid
        ));
        $r        = $this->db->query("SELECT date_terminate, COUNT(id) as total FROM a_services where date_terminate > ? and companyid=? and status in ('Terminated') and date_terminate is not null GROUP BY MONTH(date_terminate) order by date_terminate asc", array(
            $date->format('Y-m-d'),
            $cid
        ));
        if ($q->num_rows() > 0) {
            $x[] = "['Month', 'New Orders', 'Terminated']";
            if ($q->num_rows() > 0) {
                foreach ($q->result_array() as $row) {
                    $html['label'][] = substr($row['date_created'], 0, 7);
                    $html['New'][]   = $row['total'];
                }
            }
            if ($r->num_rows() > 0) {
                foreach ($r->result_array() as $row) {
                    $html['Terminated'][] = $row['total'];
                }
            }
            return $html;
        } else {
            return array();
        }
        //return $q->result_array();
    }
    public function getClientsByMonth($cid)
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("SELECT date_created, COUNT(id) as total FROM a_clients where date_created > '2019-01-01' and companyid=? GROUP BY MONTH(date_created) order by date_created asc", array(
            $cid
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $html['label'][]     = substr($row['date_created'], 0, 7);
                $html['NewClient'][] = $row['total'];
            }
            return $html;
        } else {
            return array();
        }
        //return $q->result_array();
    }
    public function getStatistic($cid = false)
    {
        $client = $this->getClientCount();
        if (!$cid) {
            $cid = $this->session->cid;
        }
        $this->db = $this->load->database('default', true);
        if ($cid == "2") {
            return array(
                'client' => $client,
                'total_unpaid' => 0,
                'count_unpaid' => '0',
                'count_paid' => '0',
                'total_amount_unpaid' => "0.00",
                'total_this_year_paid' => "0.00",
                'total_this_year_unpaid' => "0.00",
                'total_thismonthpaid' => "0.00",
                'total_thismonthunpaid' => "0.00",
                'total_thismonth_amount_unpaid' => "0.00",
                'total_thismonth_amount_paid' => "0.00",
                'percentpaid' => 0,
                'percentunpaid' => 0,
                'tickets' => 0,
                'subscriptions' => array(
                    'active' => 0,
                    'pending' => 0,
                    'new' => 0
                )
            );
        } else {
            $m           = date('m');
            $y           = date('Y');
            $percent     = 0;
            //$invoices = $this->db->query("select id,total,status,datepaid,date from tblinvoices");
            $companyid   = $cid;


            $company = globofix($cid);
            if ($company->mage_invoicing) {
                $unpaid = $this->getInvoiceSum($companyid, 'Unpaid');
                $paid = $this->getInvoiceSum($companyid, 'Paid');
                $paidtotal = $this->getInvoiceCount($companyid, 'Paid');
                $unpaidtotal = $this->getInvoiceCount($companyid, 'Unpaid');
            } else {
                $unpaid= 0;
                $paid =  0;
                $paidtotal= 0;
                $unpaidtotal = 0;
            }
            $paid        = str_replace(',', '', $paid);
            $unpaid      = str_replace(',', '', $unpaid);
            if ($paid > 0 && $unpaid > 0) {
                if (is_numeric($paid) && is_numeric($unpaid)) {
                    $percent = $paid / $unpaid * 100;
                }
            }
            $new_order     = $this->getOrderStatus($companyid, "New");
            $pending_order = $this->getOrderStatus($companyid, "Pending");
            $active_order  = $this->getOrderStatus($companyid, "Active");
            $ticket        = $this->db->query("select * from a_helpdesk_tickets where status != ?", array(
                'closed'
            ));
            $agents        = $this->db->query("select * from a_clients_agents where status = ? and companyid =?", array(
                'Active',
                $cid
            ));

            if($company->mobile_platform == "ARTA"){
                $simcard_stock = get_freesimcard_arta($cid);
            }else{
                $simcard_stock = get_freesimcard_teum($cid);
            }
            return array(
                'client' => $client,
                'total_unpaid' => number_format($unpaid, 2),
                'count_unpaid' => '0',
                'count_paid' => '0',
                'total_amount_unpaid' => round($unpaid, 2),
                'total_this_year_paid' => round($paid, 2),
                'total_this_year_unpaid' => round($unpaid, 2),
                'total_thismonthpaid' => round($paid, 2),
                'total_thismonthunpaid' => round($unpaid, 2),
                'total_thismonth_amount_unpaid' => round($unpaid, 2),
                'total_thismonth_amount_paid' => round($paid, 2),
                'percentpaid' => $percent,
                'percentunpaid' => $percent,
                'tickets' => $ticket->num_rows(),
                'agents' => $agents->num_rows(),
                'simcard_stock' => $simcard_stock,
                'subscriptions' => array(
                    'active' => $active_order,
                    'pending' => $pending_order,
                    'new' => $new_order
                )
            );
        }
    }
    public function getOrderStatus($companyid, $status)
    {

        $this->db = $this->load->database('default', true);
        if($status == "Active"){
            $q        = $this->db->query("select id from a_services where status in ('Active','Suspended') and companyid=?", array(
                $companyid
            ));
        }else{
            $q        = $this->db->query("select id from a_services where status=? and companyid=?", array(
                $status,
                $companyid
            ));
        }

        return $q->num_rows();
    }
    public function getClientCount()
    {
        $this->db = $this->load->database('default', true);
        $this->db->select('id');
        $this->db->where('companyid', $this->session->cid);
        $q = $this->db->get("a_clients");
        return $q->num_rows();
    }
    public function getInvoiceSum($companyid, $status)
    {
        $this->db = $this->load->database('magebo', true);
        if ($status == "Unpaid") {
            $q = $this->db->query("SELECT SUM(b.mInvoiceAmount) as total
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus IN(52,53)", array(
                $companyid
            ));
        } else {
            $q = $this->db->query("SELECT SUM(b.mInvoiceAmount) as total
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus = 54", array(
                $companyid
            ));
        }
        return round($q->row()->total, 2);
    }
    public function AddSmsNumber($array)
    {
        $q = $this->db->query("select * from a_sms_notifications where userid=? and msisdn=?", array(
            $array['userid'],
            $array['msisdn']
        ));
        if ($q->num_rows() == 0) {
            $this->db->insert('a_sms_notifications', $array);
        }
    }
    public function getInvoiceCount($companyid, $status)
    {
        $this->db = $this->load->database('magebo', true);
        if ($status == "Unpaid") {
            $q = $this->db->query("SELECT *
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus IN(52,53)", array(
                $companyid
            ));
        } else {
            if ($companyid != 2) {
                $q = $this->db->query("SELECT *
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus = 54", array(
                    $companyid
                ));
            } else {
                $q = $this->db->query("SELECT *
    FROM tblInvoice b
    LEFT JOIN tblAddress a on a.iAddressNbr=b.iAddressNbr
    WHERE a.iCompanyNbr=? and b.iInvoiceStatus = 54 and b.dInvoiceDate LIKE ?", array(
                    $companyid,
                    date('Y-m-d') . '%'
                ));
            }
        }
        return $q->num_rows();
    }
    public function getPaymentUnAassign($companyid)
    {
        $this->db = $this->load->database('magebo', true);
        $q        = $this->db->query("select P.iAddressNbr, P.iPaymentNbr, T.cTypeDescription as Type, CONVERT(char(10), P.dPaymentDate,126) as PaymentDate, P.mPaymentAmount, A.UsedAmount,
 P.mPaymentAmount - A.UsedAmount as AmountLeft, P.cPaymentFormDescription, ''
from tblPayment P
 left join ( select iPaymentNbr, sum(mAssignedAmount) as UsedAmount
    from tblPaymentAssignment
    where iPaymentNbr in ( select iPaymentNbr from tblPayment
        where iAddressNbr in ( select iAddressNbr from tblAddress
            where iCompanyNbr = ? ))
    group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr
  left join tblType T ON T.iTypeNbr = P.iPaymentForm
where iAddressNbr in ( select iAddressNbr from tblAddress
    where iCompanyNbr = ? )
  AND P.mPaymentAmount <> A.UsedAmount
UNION
--> Not yet used payments
select P.iAddressNbr, P.iPaymentNbr, T.cTypeDescription as Type, CONVERT(char(10), P.dPaymentDate,126) as PaymentDate, P.mPaymentAmount, cast(0 as money) as UsedAmount,
 P.mPaymentAmount as AmountLeft, P.cPaymentFormDescription, 'x'
from tblPayment P
  left join tblType T ON T.iTypeNbr = P.iPaymentForm
where iAddressNbr in ( select iAddressNbr from tblAddress
    where iCompanyNbr = ? )
  AND P.iPaymentNbr not in ( select iPaymentNbr from tblPaymentAssignment
    where iPaymentNbr in ( select iPaymentNbr from tblPayment
        where iAddressNbr in ( select iAddressNbr from tblAddress
            where iCompanyNbr = ? )))
  AND P.cPaymentRemark not like 'Payed with paymentnumber:%'
order by P.iPaymentNbr ASC", array(
            $this->companyid,
            $this->companyid,
            $this->companyid,
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $c                     = getClientDetailidbyMagebo($row['iAddressNbr']);
                $row['mPaymentAmount'] = '€' . number_format($row['mPaymentAmount'], 2);
                $row['CustomerName']   = $c->firstname . ' ' . $c->lastname;
                $row['CustomerId']     = $c->mvno_id;
                $row['id']             = $c->id;
                $row['AmountLeft']     = '€' . number_format($row['AmountLeft'], 2);
                $row['UsedAmount']     = '€' . number_format($row['UsedAmount'], 2);
                $result[]              = $row;
            }
            return $result;
        } else {
            return array();
        }
    }
}
