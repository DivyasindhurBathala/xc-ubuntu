<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Helpdesk_model extends CI_Model
{
    public function getHelpdeskStats()
    {
        $myassign = 0;
        $high = 0;
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_helpdesk_tickets where companyid =?", array($this->session->cid));
        $status = array('Open' => array(), 'In-Progress' => array(), 'On-Hold' => array(), 'Answered' => array(), 'Awaiting-Reply' => array(), 'Resolved' => array(), 'Closed' => array());
        $result = array('Open' => "", 'In-Progress' => "", 'On-Hold' => "", 'Answered' => "", 'Awaiting-Reply' => "", 'Resolved' => "", 'Closed' => "");
        foreach ($q->result_array() as $row) {
            $status[$row['status']][] = $row['id'];
            if(in_array($row['status'], array('Open','In-Progress', 'Awaiting-Reply','On-Hold'))) {
                if($row['assigne'] == $this->session->id) {
                    $myassign = $myassign+1;
                }
                if($row['priority'] <= 2) {

                    $high = $high+1;
                }


            }


        }
        $result = array(
            'Open' => count($status['Open']),
            'In-Progress' => count($status['In-Progress']),
            'On-Hold' => count($status['On-Hold']),
            'Answered' => count($status['Answered']),
            'Awaiting-Reply' => count($status['Awaiting-Reply']),
            'Resolved' => count($status['Resolved']),
            'Closed' => count($status['Closed']));
        $result['myassign'] = $myassign;
        $result['high'] = $high;
        return $result;
    }

    public function getTicket($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query(
            "select a.id,a.priority,a.tid,a.subject,a.userid,a.status,a.date,a.deptid,admin,a.categoryid,
    case when a.userid  is null THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,a.admin as staf,c.name as deptname,concat(d.firstname,' ', d.lastname) as assigned, e.name as categoryname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category e on e.id=a.categoryid
    left join a_helpdesk_department c on c.id=a.deptid where a.id=? and a.companyid=?", array($id, $this->session->cid)
        );
        $res = $q->row_array();
        $res['replies'] = $this->getreplies($id);
        return $res;
    }

    public function getreplies($id)
    {
        $this->db = $this->load->database('default', true);
        $res = array();
        $q = $this->db->query(
            "select a.*
from a_helpdesk_ticket_replies
a where a.ticketid=? order by id asc", array($id)
        );
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                if (!empty($row['attachment'])) {
                    $t = explode('|', $row['attachment']);
                    $row['attachments'] = $t;
                    unset($t);
                }
                $res[] = $row;
            }

            return $res;

        } else {
            return array();
        }

    }

    public function getLastTicketsbyUsers($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,a.date,a.subject,a.message,a.adminid, case when a.userid  is NULL THEN a.name else concat(c.firstname,' ', c.lastname) end as fullname,b.name as deptname from helpdesk_tickets a left join helpdesk_department b on b.id=a.deptid left join clients c on c.id=a.userid where a.status != 'Closed' and a.status != 'Answered' and a.userid=? order by a.id desc limit 6", array($id));
        if ($q->num_rows() > 0) {

            return $q->result();

        } else {
            return array();
        }
    }
    public function insert_attachment($id, $data)
    {
        $filename = array();
        $this->db = $this->load->database('default', true);
        foreach ($data as $row) {
            $filename[] = $row['file_name'];
        }
        $this->db->where('id', $id);
        $this->db->update('tblticketreplies', array('attachment' => implode('|', $filename)));
        return implode('|', $filename) . ' ' . $id;
    }

    public function insert_ticket($data)
    {
        $data['companyid'] = $this->session->cid;

        $replies['message'] = $data['message'];

        $replies['attachments'] = $data['attachments'];
        unset($data['message']);
        unset($data['attachments']);
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_helpdesk_tickets', $data);
        $replies['ticketid'] = $this->db->insert_id();
        if ($replies['ticketid']) {
            $replies['name'] = $this->session->firstname . ' ' . $this->session->lastname;
            $replies['admin_id'] = $this->session->id;
            $this->db->insert('a_helpdesk_ticket_replies', $replies);
            return $replies['ticketid'];
        } else {

            return false;
        }

    }

    public function getAttachment($id)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_helpdesk_attachments where id=?", array($id));
        if ($q->num_rows() > 0) {
            return $q->row()->attachment();
        } else {

            return array();
        }
    }
    public function UpdateTicket($id, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_helpdesk_tickets', $data);

    }

    public function update_reply_attachment($id, $attachments)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);

        $this->db->update('a_helpdesk_ticket_replies', array('attachments' => $attachments));
    }

    public function InsertReply($data)
    {

        $this->db = $this->load->database('default', true);
        $this->db->insert('a_helpdesk_ticket_replies', $data);
        return $this->db->insert_id();

    }

    public function openTicketUnited($data){
        $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https:/my.united-telecom.be/includes/api.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt(
        $ch, CURLOPT_POSTFIELDS,
        http_build_query(
            array(
            'action' => 'OpenTicket',
            'username' => 'admin',
            'password' => 'un!t3d',
            'deptid' => '1',
            'subject' => $data['companyname'].' Simcard Delivery',
            'message' => 'Hello, MVNO wish you to deliver simcard to customer, Please open https://my.iriscall.be/admin/subscription/\n Order ID: '.$data['serviceid'].' \nMageboId:'.$data['mageboid'],
            'clientid' => '1004813',
            'priority' => 'High',
            'markdown' => true,
            'responsetype' => 'json',
            )
        )
    );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);
    }
}
