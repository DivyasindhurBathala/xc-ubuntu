<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Config for Rabbit MQ Library
 */
$config['rabbitmq'] = array(
    'host' => '10.10.10.104', // <- Your Host       (default: localhost)
    'port' => 5672, // <- Your Port       (default: 5672)
    'user' => 'admin', // <- Your User       (default: guest)
    'pass' => 'un!t3d', // <- Your Password   (default: guest)
    'vhost' => '/', // <- Your Vhost      (default: /)
    'allowed_methods' => null, // <- Allowed methods (default: null)
    'non_blocking' => false, // <- Your Host       (default: false)
    'timeout' => 0, // <- Timeout         (default: 0)
);
