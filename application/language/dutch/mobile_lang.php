<?php
$lang["Mobile site"] = "Mobile site";
$lang["Login"] = "Login";
$lang["Password"] = "Password";
$lang["Dashboard"] = "Dashboard";
$lang["Total This Month Usage"] = "Total This Month Usage";
$lang["Amount unPaid Invoices"] = "Amount unPaid Invoices";
$lang["Logout"] = "Logout";
$lang["My Account"] = "My Account";
$lang["Home"] = "Home";
$lang["Services"] = "Services";
$lang["Setting"] = "Setting";
