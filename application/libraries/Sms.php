<?php
/**
 * Artilium Library for Soap Interfacses
 */
class Sms
{
    private $CI;
    private $username;
    private $password;
    private $companyid;
    public function __construct($vars)
    {
        libxml_disable_entity_loader(false);
        $this->CI =& get_instance();
        $this->username = $vars['username'];
        $this->password = $vars['password'];
        $this->companyid = $vars['companyid'];
        $this->userid = $vars['userid'];
        $this->CI->load->helper('globalvars');
    }
    public function send_message_bulksms($post_body)
    {
       
        $ch = curl_init();
        $headers = array(
          'Content-Type:application/json',
          'Authorization:Basic '. base64_encode("$this->username:$this->password")
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, 'https://api.bulksms.com/v1/messages');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_body));
      // Allow cUrl functions 20 seconds to execute
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
      // Wait 10 seconds while trying to connect
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
       
        $output = curl_exec($ch);
        $result = json_decode($output, true);
        //$curl_info = curl_getinfo($ch);
        //$output['http_status'] = $curl_info[ 'http_code' ];
        curl_close($ch);
        if($result[0]['type'] == "SENT"){
            logAdmin(array(
                                'companyid' => $this->companyid,
                                'serviceid' => NULL,
                                'userid' => $this->userid,
                                'user' => 'System',
                                'ip' => '127.0.0.1',
                                'description' => 'Sms sent to:'.$post_body[0]['to'].' message:'.$post_body[0]['body']
                            ));
        }
        return $result[0]['type'];
    }
}
