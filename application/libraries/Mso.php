
<?php
/**
 * Artilium Library for Soap Interfacses
 */
class Artilium
{
    private $preordering;
    private $productordering;
    private $serviceordering;
    private $appointment;
    private $url;
    private $CI;

    // private $ServiceUsageMonitoring;
    public function __construct($vars)
    {
        libxml_disable_entity_loader(false);
        $this->CI = &get_instance();
        $this->preordering = "/productordering/cws/preordering/1.0";
        $this->productordering = "/productordering/cws/ordering/1.0";
        $this->serviceordering = "/serviceordering/cws/numberporting/1.0";
        $this->appointment = " /productordering/cws/appointment/1.0";
        if (!empty($vars['env'])) {
            if ($vars['env'] == "uat") {

                $this->url = "https://olo-uat.proximus.be";

            } else {
                $this->url = "https://olo.proximus.be";
            }

        } else {
            $this->url = "https://olo.proximus.be";
        }

    }

    public function check_address()
    {

        $local_cert = "client_certificate.pem";
        $ws = ' https://wsdl.web.service.url'; //wsdl file

//define the soap server location
        $vLocation = "https//location.of.web.service";

//define the soap client object
        $client = new SoapClient($ws, array(
            "location" => $vLocation,
            "trace" => 1, "exceptions" => 1, //optional parameters for debugging
            "local_cert" => $local_cert, //parameter for certificate authentication
        ));

        return $client;
    }

    public function provide($d){


        $xml['customerOrder']['customerOrderIdentifier'] = array('id' => $d->customerOrderIdentifier, 'idContext' => array('value' => OLO_ID)); 
        $xml['customerOrder']['customerOrderItem']['customerOrderItemIdentifier'] = array('id' => $d->orderid, 'idContext' => array('value' => 'OLO')); 
        $xml['customerOrder']['customerOrderItem']['product'] = array('id' => $d->product_id, 'name' =>  $d->product_name, 'type' => 'RPO'); 
        $xml['customerOrder']['customerOrderItem']['product'] = array('childProduct' => array('id' => $d->product_id, 'name' =>  $d->product_name, 'type' => 'RPO')); 
    }

}