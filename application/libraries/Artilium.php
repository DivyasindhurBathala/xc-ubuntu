<?php
/**
 * Artilium Library for Soap Interfacses
 */
class Artilium
{
    private $CI;
    private $option;
    private $url;
    private $final;
    private $service;
    private $ServicePackages;
    private $ServiceMobile;
    private $ServiceCLI;
    private $ServicePorting;
    private $ServiceBundle;
    private $ServiceCDR;
    private $ServiceNumberPort;
    private $ServiceGeneral;
    private $ServiceReload;
    private $ServiceLogging;
    private $ServicePriceManagement;
    private $ServiceEventDistributor;
    private $companyid;
    private $log;
    // private $ServiceUsageMonitoring;
    public function __construct($vars)
    {
        libxml_disable_entity_loader(false);
        $this->CI = &get_instance();
        if (empty($vars['companyid'])) {
            $this->companyid = get_companyidby_url(base_url());
        } else {
            $this->companyid = $vars['companyid'];
        }

        $this->db = $this->CI->load->database('default', true);

        $params = $this->db->query('select * from a_api_data where companyid=?', array(
            $this->companyid,
        ));
        $this->params = $params->row();
        $this->service = $this->params->arta_url . 'API' . $this->params->arta_version . '/DefaultAPI';
        $this->service_bundle = $this->params->arta_url . 'API' . $this->params->arta_version . '/Bundle';
        $this->option = array(
            'trace' => 1,
            'features' => SOAP_USE_XSI_ARRAY_TYPE,
            'cache_wsdl' => WSDL_CACHE_NONE,
            'encoding' => 'UTF-8',
        );
        //if (!empty($this->params->arta_proxy_url)) {
        $this->option['proxy_host'] = $this->params->arta_proxy_url;
        $this->option['proxy_port'] = $this->params->arta_proxy_port;
        //  }
        $this->ServicePackages = new soapclient($this->service . '/ServicePackages.asmx?WSDL', $this->option);
        $this->ServiceMobile = new soapclient($this->service . '/ServiceMobile.asmx?WSDL', $this->option);
        $this->ServiceCLI = new soapclient($this->service . '/ServiceCLI.asmx?WSDL', $this->option);
        if (in_array($this->companyid, array(
            '53',
            '54',
            '56',
            '33',
        ))) {
            $this->ServicePorting = new soapclient($this->service . '/ServicePorting.asmx?WSDL', $this->option);
        } else {
            $this->ServicePorting = new soapclient($this->service . '/ServiceResellerMobileNumberPort.asmx?WSDL', $this->option);
            $this->ServiceBaseTables = new soapclient($this->service . '/ServiceBaseTables.asmx?WSDL', $this->option);
        }
        $this->ServiceUsageMonitoring = new soapclient($this->params->arta_url . 'API' . $this->params->arta_version . '/SUM/ServiceUsageMonitoring.asmx?WSDL', $this->option);
        $this->ServiceBundle = new soapclient($this->service_bundle . '/ServiceBundle.asmx?WSDL', $this->option);
        $this->ServiceCDR = new soapclient($this->service . '/ServiceCDR.asmx?WSDL', $this->option);
        $this->ServiceNumberPort = new soapclient($this->service . '/ServiceNumberPort.asmx?WSDL', $this->option);
        $this->ServiceGeneral = new soapclient($this->service . '/ServiceGeneral.asmx?WSDL', $this->option);
        $this->ServiceReload = new soapclient($this->service . '/ServiceReload.asmx?WSDL', $this->option);
        $this->ServiceLogging = new soapclient($this->service . '/ServiceLogging.asmx?WSDL', $this->option);
        $this->ServiceContactType2 = new soapclient($this->service . '/ServiceContactType2.asmx?WSDL', $this->option);
        $this->ServicePriceManagement = new soapclient($this->service . '/ServicePriceManagement.asmx?WSDL', $this->option);
        $this->ServiceEventDistributor = new soapclient($this->service . '/ServiceEventDistributor.asmx?WSDL', $this->option);
        //$this->ServiceUsageMonitoring = new soapclient($this->service . '/ServiceUsageMonitoring.asmx?WSDL', $this->option);
        //$this->ServiceResellerMobileNumberPort
        $this->final = (object) array(
            'result' => '',
            'data' => '',
            'message' => '',
            $this->params,
        );
        $this->log = false;
    }
    public function portinondemand_confirm()
    {
    }
    public function get_sim($simcardnumber)
    {
        $this->db = $this->CI->load->database('magebo', true);
        $q = $this->db->query("select A.iAddressNbr, A.cName, A.cStreet,A.iCompanyNbr,Y.cTypeDescription as PaymentType,
CASE when A.iCountryIndex = 22
then (C.cCountryCode + '-' + Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix)
else (C.cCountryCode + '-' +
CASE when Cast(A.iZipCode AS varchar(10)) is null
then ''+ A.cZipSuffix
else Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix end  ) end as ZipCode,
A.cCity, P.cInvoiceReference, S.cSIMCardNbr as SIMCardNbr, S.iMSISDN as MSISDN, S.*, X.cTypeDescription as Type,Z.cTypeDescription as SimCardType, B.cInternalLanguage as Language
from tblC_SIMCard S
left join tblC_Pincode P ON P.iPincode = S.iPincode
left join tblLocation L ON L.iLocationIndex = P.iLocationIndex
left join tblAddress A ON A.iAddressNbr = L.iAddressNbr
left join tblType Y ON Y.iTypeNbr=S.iPaymentType
left join tblType X ON X.iTypeNbr=S.iMSISDNType
left join tblType Z ON Z.iTypeNbr=S.iSimCardType
left join tblLanguages B on B.iBaseVoiceMailLanguage=S.iVoiceMailLanguage
, tblCountry C
where S.cSIMCardNbr = ?
AND S.bActive = 1
AND (C.iCountryIndex = A.iCountryIndex)", array(
            trim($simcardnumber),
        ));
        return $q->row();
    }
    public function GetSim($simcardnr)
    {
        $res['SN'] = "";
        $result = $this->ServiceMobile->GetSIM(array(
              'Login' => $this->params->m_username,
              'Password' => $this->params->m_password,
              'SIMNr' => $simcardnr,
          ));
        $xml = simplexml_load_string($result->GetSIMResult->ItemInfo->any);
        return (string)$xml->NewDataSet->IMSI->IMSINr;
    }

    public function GetSimInfo($simcardnr)
    {
        $res['SN'] = "";
        $result = $this->ServiceMobile->GetSIM(array(
               'Login' => $this->params->m_username,
               'Password' => $this->params->m_password,
               'SIMNr' => $simcardnr,
           ));
        $xml = simplexml_load_string($result->GetSIMResult->ItemInfo->any);
        return $xml->NewDataSet;
    }
    public function GetSn($simcardnr)
    {
        try {
            $res['SN'] = "";
            $result = $this->ServiceMobile->GetSIM(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SIMNr' => $simcardnr,
            ));

            if ($result->GetSIMResult->Result == "0") {
                $this->final->result = 'success';
                $xml = simplexml_load_string($result->GetSIMResult->ItemInfo->any);
                if (count($xml->NewDataSet->MSISDN) == "2") {
                    foreach ($xml->NewDataSet->MSISDN as $rw) {
                        if ($rw->Status == "6") {
                            $sn = $rw;
                        }
                    }
                } elseif (count($xml->NewDataSet->MSISDN) == "1") {
                    $sn = $xml->NewDataSet->MSISDN;
                }
                $sn = (array) $sn;
                if ($xml->NewDataSet->SIM) {
                    $q = (array) $xml->NewDataSet->SIM;
                    $sn['PUK1'] = $q['PUK1'];
                    $sn['PUK2'] = $q['PUK2'];
                }

                if ($xml->NewDataSet->IMSI) {
                    $L = (string) $xml->NewDataSet->IMSI->IMSINr;
                    $sn['IMSI'] = $L;
                }
                //$sn['SIM'] = $xml->NewDataSet;
                //$this->final->SN = $sn
                $this->final->data = (object) $sn;
            // $this->final->message = (object) $xml->NewDataSet;
            } else {
                $this->final->result = 'error';
                $this->final->message = $result;
                $this->final->platform =  array('request' => $this->ServiceMobile->__getLastRequest(),
                'response' => $this->ServiceMobile->__getLastResponse());
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetListPackageOptionsForSn($SN)
    {
        if (!empty($this->CI->session->language)) {
            $this->CI->config->set_item('language', $this->CI->session->language);
        } else {
            $this->CI->config->set_item('language', 'dutch');
        }
        $this->CI->lang->load('admin');
        $this->CI->load->helper('language');
        $show = array(
            'International Numbers',
            '4G',
            'Data',
            'Premium Numbers',
            'Roaming',
           /* 'Premium Gaming', */
        );
        try {
            $result = $this->ServicePackages->GetListPackageOptionsForSN(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
                'ActiveAndNonActive' => 1,
            ));

            if ($result->GetListPackageOptionsForSNResult->Result == "0") {
                $array = simplexml_load_string($result->GetListPackageOptionsForSNResult->ListInfo->any);
                // mail("mail@simson.one","packages",print_r($array, true));
                if (count($array->NewDataSet->PackageOptions) > 1 && count($array->NewDataSet->PackageOptions) != 0) {
                    foreach ($array->NewDataSet->PackageOptions as $r) {
                        $b = (array) $r;
                        if (in_array($r->CallModeDescription, $show)) {
                            $b['CallModeDescription'] = (string)$r->CallModeDescription;
                            $res[] = (object) $b;
                        } else {
                            if ($r->Customizable == 1) {
                                if (($r->TrafficDescription == "Data") && ($r->CallModeDescription == "Roaming Originating")) {
                                    $b['CallModeDescription'] = (string)$r->TrafficDescription." ".$r->CallModeDescription;
                                    $res[] = (object) $b;
                                } elseif ($r->TrafficDescription == "Short Messaging Service" && $r->CallModeDescription == "Roaming Originating") {
                                    $b['CallModeDescription'] = (string)$r->TrafficDescription." ".$r->CallModeDescription;
                                    $res[] = (object) $b;
                                } elseif ($r->TrafficDescription == "Voice phone calls" && $r->CallModeDescription == "Roaming Originating") {
                                    $b['CallModeDescription'] = (string)$r->TrafficDescription." ".$r->CallModeDescription;
                                    $res[] = (object) $b;
                                }
                            }
                        }
                        unset($b);
                    }
                } else {
                    $res = $array->NewDataSet->PackageOptions;
                }
                return $res;
            } else {
                $this->final->result = 'error';
                $this->final->message = 'No result';
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function EventSubscribe($mvnoabbr)
    {
        $subscriptions = array(

        array("Arta.Events.Reseller.Subscription.Credit.TopUpEvent","MVNO".$mvnoabbr."CreditTopUpEvent"),
        array("Arta.Events.Reseller.Subscription.Credit.CreditThresholdEvent","MVNO".$mvnoabbr."CreditThresholdEvent"),
        array("Arta.Events.Reseller.Subscription.Credit.NoCreditLeftEvent","MVNO".$mvnoabbr."NoCreditLeftEvent"),
        array("Arta.Events.Reseller.Subscription.Subscription.StatusChangedEvent","MVNO".$mvnoabbr."StatusChangedEvent"),
        array("Arta.Events.Reseller.Subscription.Package.ServiceChangedEvent","MVNO".$mvnoabbr."PackageServiceChangedEvent"),
        array("Arta.Events.Reseller.Subscription.Provisioning.JobEndEvent","MVNO".$mvnoabbr."JobEndEvent"),
        array("Arta.Events.Reseller.Subscription.Bundle.OnStartBundleUsageSetEvent","MVNO".$mvnoabbr."OnStartBundleUsageSetEvent"),
        array("Arta.Events.Reseller.Subscription.Bundle.OnExpiryBundleUsageSetEvent","MVNO".$mvnoabbr."OnExpiryBundleUsageSetEvent"),
        array("Arta.Events.Reseller.Subscription.Bundle.OnNoCreditsLeftUsageSetEvent","MVNO".$mvnoabbr."OnNoCreditsLeftUsageSetEvent"),
        array("Arta.Events.Reseller.Subscription.Bundle.BundleThresholdEvent","MVNO".$mvnoabbr."BundleThresholdEvent"),
        //SUM Stuff
        array("Arta.Events.Reseller.Subscription.Subscription.Sum.SumStartEvent","MVNO".$mvnoabbr."SumStartEvent"),
        array("Arta.Events.Reseller.Subscription.Subscription.Sum.SumExpiryEvent","MVNO".$mvnoabbr."SumExpiryEvent"),
        array("Arta.Events.Reseller.Subscription.Subscription.Sum.SumThresholdEvent","MVNO".$mvnoabbr."SumThresholdEvent"),
        array("Arta.Events.Reseller.Subscription.Subscription.Sum.SumAssignmentEvent","MVNO".$mvnoabbr."SumAssignmentEvent"),
        array("Arta.Events.Reseller.Subscription.Subscription.Sum.SumActionSetChangedEvent","MVNO".$mvnoabbr."SumActionSetChangedEvent"),
        array("Arta.Events.Reseller.Subscription.Subscription.Sum.SumStatusUpdateEvent","MVNO".$mvnoabbr."SumStatusUpdateEvent"),
        //SMS Event
        array("Arta.Events.Reseller.Subscription.Subscription.SmsEvent","MVNO".$mvnoabbr."SmsEvent"),

        );
        $uri="http://10.10.10.104/";
        foreach ($subscriptions as $sub) {
            $result[$sub[0]] = $this->ServiceEventDistributor->Subscribe(array(
          'Login' => $this->params->m_username,
          'Password' => $this->params->m_password,
          'Name'=>$sub[1],
          'Uri'=>$uri,
          'Accept'=>'application/xml',
          'MessageTypes'=>$sub[0]));
            log_message("error", "REQUEST:".$this->ServiceEventDistributor->__getLastRequest());
            log_message("error", "RESPONSE:".$this->ServiceEventDistributor->__getLastResponse());
        }

        return $result;
    }
    public function GetListPackageOptionsForSnAdvance($SN, $limit = false, $api=false)
    {
        if (empty($this->CI->session->language)) {
            $lg = "dutch";
        } else {
            $lg = $this->CI->session->language;
        }
        $this->CI->load->helper('language');
        $this->CI->config->set_item('language', $lg);
        $this->CI->lang->load('admin');

        $show = array(
            'International Numbers',
            '4G',
            'Data',
            'Premium Numbers',
            'Roaming',
          /*  'Premium Gaming', */
        );
        try {
            $result = $this->ServicePackages->GetListPackageOptionsForSN(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
                'ActiveAndNonActive' => 1,
            ));
            if ($result->GetListPackageOptionsForSNResult->Result == "0") {
                $array = simplexml_load_string($result->GetListPackageOptionsForSNResult->ListInfo->any);
                if (count($array->NewDataSet->PackageOptions) > 1 && count($array->NewDataSet->PackageOptions) != 0) {
                    foreach ($array->NewDataSet->PackageOptions as $r) {
                        $b = (array) $r;
                        if ($api) {
                            if ($r->Customizable == 1) {
                                $tt['Id'] = $b['PackageDefinitionId'];
                                $tt['Status'] = $b['Available'];
                                $tt['TrafficDescription'] = $b['TrafficDescription'];
                                $tt['CallModeDescription'] = $b['CallModeDescription'];
                                $res[] =  (object) $tt;
                            }
                            continue;
                        }
                        if (in_array($r->CallModeDescription, $show)) {
                            if (empty($r->CallModeDescription)) {
                                $r->CallModeDescription = "Unknown";
                            }
                            $b['CallModeDescription'] = (string)$r->CallModeDescription;
                            $res[] = (object) $b;
                        } else {
                            if ($limit) {
                                if ($r->Customizable == 1) {
                                    if (in_array(trim($b['TrafficDescription'] . " " . $b['CallModeDescription']), array('DATA R0', 'VOICE ROAMING O','SMS OR'))) {
                                        if (!empty($r->TrafficDescription)) {
                                            $b['CallModeDescription'] = (string)$b['TrafficDescription'] . " " . $b['CallModeDescription'];
                                        }
                                        $res[] = (object) $b;
                                    }
                                }
                            } else {
                                if ($r->Customizable == 1) {
                                    $b['CallModeDescription'] = (string)$b['CallModeDescription'];
                                    if (!empty($r->TrafficDescription)) {
                                        $b['CallModeDescription'] = (string)$b['TrafficDescription'] . " " . $b['CallModeDescription'];
                                    }
                                    $res[] = (object) $b;
                                }
                            }
                        }
                        unset($b);
                    }
                } else {
                    $res = $array->NewDataSet->PackageOptions;
                }
                return $res;
            } else {
                $this->final->result = 'error';
                $this->final->message = 'No result';
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetCDRList($SN)
    {
        include_once FCPATH . 'third_party/xmlutil.php';
        try {
            $result = $this->ServiceCDR->GetCDRList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
                'From' => date('Y-m-01\TH:i:s'),
                'Till' => date('Y-m-d\T23:i:s'),
                'SN' => $SN,
                'PageIndex' => 0,
                'PageSize' => 2500,
                'SortBy' => 0,
                'SortOrder' => 0,
            ));
            return $result;
            if ($result->GetCDRListResult->Result == "0") {
                $array = new SimpleXMLElement($result->GetCDRListResult->ListInfo->any);
                $this->final->result = 'success';
                $this->final->data = $array;
            } else {
                $this->final->result = 'error';
                $this->final->message = 'No result';
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetParametersCLI($SN)
    {
        try {
            $result = $this->ServiceCLI->GetParametersCLI(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
            ));

            //log_message('error', $this->ServiceCLI->__getLastRequest());
            return $result;
        } catch (Exception $e) {
            $this->final->result = 'error1';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function SwapSim($data)
    {
        $sim = $this->ServiceMobile->GetMSISDNList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $data['SN'],
        ));

        if ($sim->GetMSISDNListResult->TotalItems == 1) {
            $object = new SimpleXMLElement($sim->GetMSISDNListResult->ListInfo->any);
            $MSISDNId = (int) $object->NewDataSet->MSISDN->MSISDNId;
        } else {
            $object = new SimpleXMLElement($sim->GetMSISDNListResult->ListInfo->any);

            foreach ($object->NewDataSet->MSISDN as $row) {
                if ($row->Status == '6') {
                    $MSISDNId = (int) $row->MSISDNId;
                }
            }
        }

        $result = $this->ServiceCLI->SwapSim(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'NewSimNr' => $data['NewSimNr'],
            'MsisdnId' => $MSISDNId,
        ));
        if ($this->log) {
            mail('mail@simson.one', 'Swap Result', print_r($data, true) . print_r($result, true));
        }
        //
        if ($result->SwapSimResult === 0) {
            return (object) array(
                'result' => 'success',
            );
        } else {
            log_message('error', 'Error SwapSim :'.print_r($result, true));

            return (object) array(
                'result' => 'error',
                'message' => getErrorSwap('SwapSim', $result->SwapSimResult)

            );
        }
    }
    public function AcceptPortOut($porting)
    {
        $auth = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
        );
        $data = array_merge($porting, $auth);
        $result = $this->ServicePorting->AcceptPortOut($data);

        if ($result->AcceptPortOutResult == "0") {
            return (object) array(
                'result' => 'success',
            );
        } else {
            if ($result->AcceptPortOutResult == "-2") {
                $result = "No Pins are found with this PortingId";
            } elseif ($result->AcceptPortOutResult == "-3") {
                $result = "PortingId exists, but the status of request is not valid for accept";
            } elseif ($result->AcceptPortOutResult == "-4") {
                $result = "Pin can not be null";
            } elseif ($result->AcceptPortOutResult == "-5") {
                $result = "No PortOut in progress for this SN";
            } elseif ($result->AcceptPortOutResult == "-6") {
                $result = "Parameter 20519 (Port Out lead CLI/PIN) not found";
            } elseif ($result->AcceptPortOutResult == "-90") {
                $result = "Authentication failed";
            } elseif ($result->AcceptPortOutResult == "-91") {
                $result = "Field overflow (too many characters, date out of range";
            } elseif ($result->AcceptPortOutResult == "-100") {
                $result = "Unspecified error";
            }
            return (object) array(
                'result' => 'error',
                'message' => $result,
            );
        }
    }
    public function RejectPortOut($PortingId, $data)
    {
        /*array(
        'RejectId' => '',
        'SN' => '',
        'FirstPossibleDate' => ''
        )
         */
        $result = $this->ServicePorting->RejectPortOut(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'PortingId' => $PortingId,
            'FirstPossibleDate' => $data['FirstPossibleDate'],
            'RejectItems' => array('Sn' => $data['Sn'], 'RejectId' => $data['RejectId']),
        ));
        if ($this->log) {
            mail('mail@simson.one', 'Reject Portout', print_r(array('data' => array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'PortingId' => $PortingId,
                'RejectPorting' => array('Sn' => $data['Sn'], 'RejectId' => $data['RejectId']),
            ),
                'request' => $this->ServicePorting->__getLastRequest(),
                'response' => $this->ServicePorting->__getLastResponse()), true));
        }
        if ($result->RejectPortOutResult == "0") {
            return (object) array(
                'result' => 'success',
            );
        } else {
            if ($result->RejectPortOutResult == "-2") {
                $result = "No Pins are found with this PortingId";
            } elseif ($result->RejectPortOutResult == "-3") {
                $result = "PortingId exists, but the status of request is not valid for accept";
            } elseif ($result->RejectPortOutResult == "-4") {
                $result = "Pin can not be null";
            } elseif ($result->RejectPortOutResult == "-5") {
                $result = "No PortOut in progress for this SN";
            } elseif ($result->RejectPortOutResult == "-6") {
                $result = "Parameter 20519 (Port Out lead CLI/PIN) not found";
            } elseif ($result->RejectPortOutResult == "-90") {
                $result = "Authentication failed";
            } elseif ($result->RejectPortOutResult == "-91") {
                $result = "Field overflow (too many characters, date out of range";
            } elseif ($result->RejectPortOutResult == "-100") {
                $result = "Unspecified error";
            } else {
                $result = "Unspecified error";
            }
            return (object) array(
                'result' => 'error',
                'message' => $result,
            );
        }
    }
    public function GetSpecificCliInfo($SN)
    {
        try {
            $result = $this->ServiceCLI->GetSpecificCliInfo(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
            ));
            if ($result->GetSpecificCLIInfoResult->TotalItems == 1) {
                $r = simplexml_load_string($result->GetSpecificCLIInfoResult->ListInfo->any);
                $object = (array) $r->NewDataSet->SpecificCLIInfo;
                return (object) $object;
            } else {
                $r = simplexml_load_string($result->GetSpecificCLIInfoResult->ListInfo->any);
                $this->final = (object) $r->NewDataSet->SpecificCLIInfo[0];
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetBundleList()
    {
        try {
            $result = $this->ServiceBundle->GetBundleList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
            ));
            $this->final = simplexml_load_string($result->GetBundleListResult->ListInfo->any);
        } catch (Exception $e) {
            $this->final->result = 'error1';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetBundleAssignList($SN)
    {
        try {
            $result = $this->ServiceBundle->GetBundleAssignListV2(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
            ));
            $res = json_decode(json_encode(simplexml_load_string($result->GetBundleAssignListV2Result->ListInfo->any)));
            return $res;
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = array(
                $res,
                $this->ServiceBundle->__getLastRequest(),
            );
        }
        return $this->final;
    }
    public function GetBundleAssignList1($SN)
    {
        try {
            $result = $this->ServiceBundle->GetBundleAssignListV2(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
            ));
            $res = json_decode(json_encode(simplexml_load_string($result->GetBundleAssignListV2Result->ListInfo->any)));
            // mail('mail@simson.one','result', print_r($res, 1));
            if ($result->GetBundleAssignListV2Result->TotalItems == "0") {
                $r = array();
            } elseif ($result->GetBundleAssignListV2Result->TotalItems == "1") {
                $b = $res->NewDataSet->BundleAssign;
                $b->Percentage = "0";
                $h = explode('T', $b->ValidFrom);
                $b->szBundle = getMobilebundleName($b->BundleId, $this->companyid);
                $b->ValidFrom = date('Y-m-01');
                $l = $this->GetBundleUsageList($SN, $b->BundleAssignId, $b->BundleId);
                // echo "here1";

                $s = (array) $l;
                if (!empty($s)) {
                    if (!is_array($l->NewDataSet->BundleUsage)) {
                        //$b->detail = $l;
                        //$b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                        //$b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                        $p = $l->NewDataSet->BundleUsage->UsedValue / $l->NewDataSet->BundleUsage->AssignedValue;
                        $b->Percentage = round($p * 100, 2);
                        if (is_nan($b->Percentage)) {
                            $b->Percentage = '0';
                        }
                        $b->Defenition = $this->GetBundleDefinition($l->NewDataSet->BundleUsage->BundleDefinitionId);
                        if ($b->Defenition == 3) {
                            $b->AssignedValue = human_filesize(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                            $b->UsedValue = human_filesize(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                        } elseif ($b->Defenition == 1) {
                            if ($l->NewDataSet->BundleUsage->AssignedValue == 120000) {
                                $b->AssignedValue = 'onbeperkt';
                            } else {
                                $b->AssignedValue = second2hms(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                            }
                            $b->UsedValue = second2hms(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                        } else {
                            $b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                            $b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                        }
                        $b->icon = ratingtype($b->Defenition);
                    } else {
                        $usedvalues = $this->getUsedValue($l->NewDataSet->BundleUsage);
                        if ($usedvalues->used > 0) {
                            $p = $usedvalues->used / $usedvalues->assigned;
                            $b->Percentage = round($p * 100, 2);
                        }
                        if (is_nan($b->Percentage)) {
                            $b->Percentage = '0';
                        }
                        $b->Defenition = $this->GetBundleDefinition($this->getDefenitionid($l->NewDataSet->BundleUsage));
                        if ($b->Defenition == 3) {
                            $b->AssignedValue = human_filesize(round($usedvalues->assigned, 2));
                            $b->UsedValue = human_filesize(round($usedvalues->used, 2));
                        } elseif ($b->Defenition == 1) {
                            if ($usedvalues->assigned == 120000) {
                                $b->AssignedValue = 'onbeperkt';
                            } else {
                                $b->AssignedValue = second2hms(round($usedvalues->assigned, 2));
                            }
                            $b->UsedValue = second2hms(round($usedvalues->used, 2));
                        } else {
                            $b->AssignedValue = round($usedvalues->assigned, 2);
                            $b->UsedValue = round($usedvalues->used, 2);
                        }
                        $b->icon = ratingtype($b->Defenition);
                    }
                    // $b->ValidFrom = $l->NewDataSet->BundleUsage->ValidFrom;
                    if (!empty($b->ValidUntil)) {
                        $tos = explode('T', $b->ValidUntil);
                        $b->ValidUntil = $tos[0];
                        unset($tos);
                        if ($b->ValidUntil >= date('Y-m-d')) {
                            $r[] = $b;
                        }
                    } else {
                        $r[] = $b;
                    }
                } else {
                    if (!empty($b->ValidUntil)) {
                        $tos = explode('T', $b->ValidUntil);
                        $b->ValidUntil = $tos[0];
                        unset($tos);
                    }
                    if ($b->ValidUntil >= date('Y-m-d')) {
                        $r[] = $b;
                    }
                }
            } else {
                //$f =else $res->NewDataSet->BundleAssign;
                $r = array();
                if ($res) {
                    if (is_array($res->NewDataSet->BundleAssign)) {
                        foreach ($res->NewDataSet->BundleAssign as $b) {
                            $b->Percentage = "0";
                            $h = explode('T', $b->ValidFrom);
                            $b->szBundle = getMobilebundleName($b->BundleId, $this->companyid);
                            if (count($h) == 2) {
                                $b->ValidFrom = $h[0];
                            } else {
                                $b->ValidFrom = date('Y-m-01');
                            }

                            $l = $this->GetBundleUsageList($SN, $b->BundleAssignId, $b->BundleId);
                            $s = (array) $l;
                            if (!empty($s)) {
                                if (!is_array($l->NewDataSet->BundleUsage)) {
                                    //$b->detail = $l;
                                    //$b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                                    //$b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                                    $p = $l->NewDataSet->BundleUsage->UsedValue / $l->NewDataSet->BundleUsage->AssignedValue;
                                    $b->Percentage = round($p * 100, 2);
                                    if (is_nan($b->Percentage)) {
                                        $b->Percentage = '0';
                                    }
                                    $b->Defenition = $this->GetBundleDefinition($l->NewDataSet->BundleUsage->BundleDefinitionId);
                                    if ($b->Defenition == 3) {
                                        $b->AssignedValue = human_filesize(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                                        //$b->UsedValue = human_filesize(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                                        if (isset($l->NewDataSet->BundleUsage->ReservedValue)) {
                                            $b->UsedValue = human_filesize(round(($l->NewDataSet->BundleUsage->UsedValue - $l->NewDataSet->BundleUsage->ReservedValue), 2));
                                        } else {
                                            $b->UsedValue = human_filesize(round($usedvalues->used, 2));
                                        }
                                    } elseif ($b->Defenition == 1) {
                                        if ($l->NewDataSet->BundleUsage->AssignedValue == 120000) {
                                            $b->AssignedValue = 'onbeperkt';
                                        } else {
                                            $b->AssignedValue = second2hms(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                                        }
                                        $b->UsedValue = second2hms(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                                    } else {
                                        $b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                                        $b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                                    }
                                    $b->icon = ratingtype($b->Defenition);
                                } else {
                                    $usedvalues = $this->getUsedValue($l->NewDataSet->BundleUsage);
                                    if ($usedvalues->used > 0) {
                                        $p = $usedvalues->used / $usedvalues->assigned;
                                        $b->Percentage = round($p * 100, 2);
                                    }
                                    if (is_nan($b->Percentage)) {
                                        $b->Percentage = '0';
                                    }
                                    $b->Defenition = $this->GetBundleDefinition($this->getDefenitionid($l->NewDataSet->BundleUsage));
                                    if ($b->Defenition == 3) {
                                        $b->AssignedValue = human_filesize(round($usedvalues->assigned, 2));
                                        if (isset($b->ReservedValue)) {
                                            $b->UsedValue = human_filesize(round(($usedvalues->used-$b->ReservedValue), 2));
                                        } else {
                                            $b->UsedValue = human_filesize(round($usedvalues->used, 2));
                                        }
                                    } elseif ($b->Defenition == 1) {
                                        if ($usedvalues->assigned == 120000) {
                                            $b->AssignedValue = 'onbeperkt';
                                        } else {
                                            $b->AssignedValue = second2hms(round($usedvalues->assigned, 2));
                                        }
                                        $b->UsedValue = second2hms(round($usedvalues->used, 2));
                                    } else {
                                        $b->AssignedValue = round($usedvalues->assigned, 2);
                                        $b->UsedValue = round($usedvalues->used, 2);
                                    }
                                    $b->icon = ratingtype($b->Defenition);
                                }
                                if (!empty($b->ValidUntil)) {
                                    $tos = explode('T', $b->ValidUntil);
                                    $b->ValidUntil = $tos[0];
                                    unset($tos);
                                    if ($b->ValidUntil >= date('Y-m-d')) {
                                        if ($b->ValidFrom <= date('Y-m-d')) {
                                            if ($b->ValidFrom != date('Y-m-d')) {
                                                $b->ValidFrom = date('Y-m-01');
                                            }
                                            $r[] = $b;
                                        }
                                    }
                                } else {
                                    if ($b->ValidFrom <= date('Y-m-d')) {
                                        if ($b->ValidFrom != date('Y-m-d')) {
                                            $b->ValidFrom = date('Y-m-01');
                                        }
                                        $r[] = $b;
                                    }
                                }
                            } else {
                                if (!empty($b->ValidUntil)) {
                                    $tos = explode('T', $b->ValidUntil);
                                    $b->ValidUntil = $tos[0];
                                    unset($tos);
                                } else {
                                    $b->ValidUntil = '2099-12-31';
                                }
                                if ($b->ValidUntil >= date('Y-m-d')) {
                                    if ($b->ValidFrom <= date('Y-m-d')) {
                                        if ($b->ValidFrom != date('Y-m-d')) {
                                            $b->ValidFrom = date('Y-m-01');
                                            $r[] = $b;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if ($bundles->ValidFrom <= date('Y-m-d')) {
                            $r[] = $bundles->BundleAssign;
                        }
                    }
                }
            }
            $this->final = $r;
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = array(
                $res,
                $this->ServiceBundle->__getLastRequest(),
            );
        }
        return $this->final;
    }

    public function GetBundleAssignList2($SN)
    {
        try {
            $result = $this->ServiceBundle->GetBundleAssignListV2(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
            ));
            $res = json_decode(json_encode(simplexml_load_string($result->GetBundleAssignListV2Result->ListInfo->any)));
            if ($result->GetBundleAssignListV2Result->TotalItems == "0") {
                $r = array();
            } elseif ($result->GetBundleAssignListV2Result->TotalItems == "1") {
                $b = $res->NewDataSet->BundleAssign;
                $b->Percentage = "0";
                $h = explode('T', $b->ValidFrom);
                $b->szBundle = getMobilebundleName($b->BundleId, $this->companyid);
                $b->ValidFrom = date('Y-m-01');
                $l = $this->GetBundleUsageList($SN, $b->BundleAssignId, $b->BundleId);
                // echo "here1";
                $s = (array) $l;
                if (!empty($s)) {
                    if (!is_array($l->NewDataSet->BundleUsage)) {
                        //$b->detail = $l;
                        //$b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                        //$b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                        $p = $l->NewDataSet->BundleUsage->UsedValue / $l->NewDataSet->BundleUsage->AssignedValue;
                        $b->Percentage = round($p * 100, 2);
                        if (is_nan($b->Percentage)) {
                            $b->Percentage = '0';
                        }
                        $b->Defenition = $this->GetBundleDefinition($l->NewDataSet->BundleUsage->BundleDefinitionId);
                        if ($b->Defenition == 3) {
                            $b->AssignedValue = human_filesize(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                            $b->UsedValue = human_filesize(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                        } elseif ($b->Defenition == 1) {
                            if ($l->NewDataSet->BundleUsage->AssignedValue == 120000) {
                                $b->AssignedValue = 'onbeperkt';
                            } else {
                                $b->AssignedValue = second2hms(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                            }
                            $b->UsedValue = second2hms(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                        } else {
                            $b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                            $b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                        }
                        $b->icon = ratingtypev2($b->Defenition);
                    } else {
                        $usedvalues = $this->getUsedValue($l->NewDataSet->BundleUsage);
                        if ($usedvalues->used > 0) {
                            $p = $usedvalues->used / $usedvalues->assigned;
                            $b->Percentage = round($p * 100, 2);
                        }
                        if (is_nan($b->Percentage)) {
                            $b->Percentage = '0';
                        }
                        $b->Defenition = $this->GetBundleDefinition($this->getDefenitionid($l->NewDataSet->BundleUsage));
                        if ($b->Defenition == 3) {
                            $b->AssignedValue = human_filesize(round($usedvalues->assigned, 2));
                            $b->UsedValue = human_filesize(round($usedvalues->used, 2));
                        } elseif ($b->Defenition == 1) {
                            if ($usedvalues->assigned == 120000) {
                                $b->AssignedValue = 'onbeperkt';
                            } else {
                                $b->AssignedValue = second2hms(round($usedvalues->assigned, 2));
                            }
                            $b->UsedValue = second2hms(round($usedvalues->used, 2));
                        } else {
                            $b->AssignedValue = round($usedvalues->assigned, 2);
                            $b->UsedValue = round($usedvalues->used, 2);
                        }
                        $b->icon = ratingtypev2($b->Defenition);
                    }
                    // $b->ValidFrom = $l->NewDataSet->BundleUsage->ValidFrom;
                    if (!empty($b->ValidUntil)) {
                        $tos = explode('T', $b->ValidUntil);
                        $b->ValidUntil = $tos[0];
                        unset($tos);
                        if ($b->ValidUntil >= date('Y-m-d')) {
                            $r[] = $b;
                        }
                    } else {
                        $r[] = $b;
                    }
                } else {
                    if (!empty($b->ValidUntil)) {
                        $tos = explode('T', $b->ValidUntil);
                        $b->ValidUntil = $tos[0];
                        unset($tos);
                    }
                    if ($b->ValidUntil >= date('Y-m-d')) {
                        $r[] = $b;
                    }
                }
            } else {
                //$f =else $res->NewDataSet->BundleAssign;
                $r = array();
                if ($res) {
                    if (is_array($res->NewDataSet->BundleAssign)) {
                        foreach ($res->NewDataSet->BundleAssign as $b) {
                            $b->Percentage = "0";
                            $h = explode('T', $b->ValidFrom);
                            $b->szBundle = getMobilebundleName($b->BundleId, $this->companyid);
                            if (count($h) == 2) {
                                $b->ValidFrom = $h[0];
                            } else {
                                $b->ValidFrom = date('Y-m-01');
                            }

                            $l = $this->GetBundleUsageList($SN, $b->BundleAssignId, $b->BundleId);
                            $s = (array) $l;
                            if (!empty($s)) {
                                if (!is_array($l->NewDataSet->BundleUsage)) {
                                    //$b->detail = $l;
                                    //$b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                                    //$b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                                    $p = $l->NewDataSet->BundleUsage->UsedValue / $l->NewDataSet->BundleUsage->AssignedValue;
                                    $b->Percentage = round($p * 100, 2);
                                    if (is_nan($b->Percentage)) {
                                        $b->Percentage = '0';
                                    }
                                    $b->Defenition = $this->GetBundleDefinition($l->NewDataSet->BundleUsage->BundleDefinitionId);
                                    if ($b->Defenition == 3) {
                                        $b->AssignedValue = human_filesize(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                                        $b->UsedValue = human_filesize(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                                    } elseif ($b->Defenition == 1) {
                                        if ($l->NewDataSet->BundleUsage->AssignedValue == 120000) {
                                            $b->AssignedValue = 'onbeperkt';
                                        } else {
                                            $b->AssignedValue = second2hms(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                                        }
                                        $b->UsedValue = second2hms(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                                    } else {
                                        $b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                                        $b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                                    }
                                    $b->icon = ratingtypev2($b->Defenition);
                                } else {
                                    $usedvalues = $this->getUsedValue($l->NewDataSet->BundleUsage);
                                    if ($usedvalues->used > 0) {
                                        $p = $usedvalues->used / $usedvalues->assigned;
                                        $b->Percentage = round($p * 100, 2);
                                    }
                                    if (is_nan($b->Percentage)) {
                                        $b->Percentage = '0';
                                    }
                                    $b->Defenition = $this->GetBundleDefinition($this->getDefenitionid($l->NewDataSet->BundleUsage));
                                    if ($b->Defenition == 3) {
                                        $b->AssignedValue = human_filesize(round($usedvalues->assigned, 2));
                                        $b->UsedValue = human_filesize(round($usedvalues->used, 2));
                                    } elseif ($b->Defenition == 1) {
                                        if ($usedvalues->assigned == 120000) {
                                            $b->AssignedValue = 'onbeperkt';
                                        } else {
                                            $b->AssignedValue = second2hms(round($usedvalues->assigned, 2));
                                        }
                                        $b->UsedValue = second2hms(round($usedvalues->used, 2));
                                    } else {
                                        $b->AssignedValue = round($usedvalues->assigned, 2);
                                        $b->UsedValue = round($usedvalues->used, 2);
                                    }
                                    $b->icon = ratingtypev2($b->Defenition);
                                }
                                if (!empty($b->ValidUntil)) {
                                    $tos = explode('T', $b->ValidUntil);
                                    $b->ValidUntil = $tos[0];
                                    unset($tos);
                                    if ($b->ValidUntil >= date('Y-m-d')) {
                                        if ($b->ValidFrom <= date('Y-m-d')) {
                                            if ($b->ValidFrom != date('Y-m-d')) {
                                                $b->ValidFrom = date('Y-m-01');
                                            }
                                            $r[] = $b;
                                        }
                                    }
                                } else {
                                    if ($b->ValidFrom <= date('Y-m-d')) {
                                        if ($b->ValidFrom != date('Y-m-d')) {
                                            $b->ValidFrom = date('Y-m-01');
                                        }
                                        $r[] = $b;
                                    }
                                }
                            } else {
                                if (!empty($b->ValidUntil)) {
                                    $tos = explode('T', $b->ValidUntil);
                                    $b->ValidUntil = $tos[0];
                                    unset($tos);
                                } else {
                                    $b->ValidUntil = '2099-12-31';
                                }
                                if ($b->ValidUntil >= date('Y-m-d')) {
                                    if ($b->ValidFrom <= date('Y-m-d')) {
                                        if ($b->ValidFrom != date('Y-m-d')) {
                                            $b->ValidFrom = date('Y-m-01');
                                            $r[] = $b;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if ($bundles->ValidFrom <= date('Y-m-d')) {
                            $r[] = $bundles->BundleAssign;
                        }
                    }
                }
            }
            $this->final = $r;
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = array(
                $res,
                $this->ServiceBundle->__getLastRequest(),
            );
        }
        return $this->final;
    }
    public function GetBundleDefinition($Id)
    {
        try {
            $result = $this->ServiceBundle->GetBundleDefinition(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'BundleDefinitionId' => $Id,
            ));
            // mail('lainard@gmail.com', 'bundle', print_r($this->ServiceBundle->__getLastRequest() . " " . $this->ServiceBundle->__getLastResponse(), true));
            if ($result->GetBundleDefinitionResult->Result == "0") {
                if (!empty($result->GetBundleDefinitionResult->ItemInfo->Benefit->RatingUnit)) {
                    $this->final = $result->GetBundleDefinitionResult->ItemInfo->Benefit->RatingUnit;
                } else {
                    $this->final = 0;
                }
            } else {
                $this->final = 100;
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function getDefenitionid($array)
    {
        $defenition = array();
        $overaldef = array();
        if (is_array($array)) {
            foreach ($array as $p) {
                if (!empty($p->BundleDefinitionId)) {
                    $defenition[] = $p->BundleDefinitionId;
                }
            }
            $d = array_unique($defenition);
            return $d[0];
        }
    }
    public function getUsedValue($array)
    {
        // print_r($array);
        $used = array();
        $assigned = array();
        if (is_array($array)) {
            foreach ($array as $p) {
                if (empty($p->OverallBundleUsageId)) {
                    $date1 = explode('T', $p->ValidUntil);
                    if ($date1[0] >= date('Y-m-d')) {
                        $used[] = $p->UsedValue;
                        $assigned[] = $p->AssignedValue;
                    }
                    unset($date1);
                }
                // $date2 = explode('T', $p->UsedValue);
            }
            // print_r(array_sum($assigned));
            return (object) array(
                'used' => array_sum($used),
                'assigned' => array_sum($assigned),
            );
        } else {
            return (object) array(
                'used' => 0,
                'assigned' => 0,
            );
        }
    }
    public function GetBundleUsageList($SN, $id)
    {
        try {
            $result = $this->ServiceBundle->GetBundleUsageList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
                'BundleAssignId' => $id,
                'PageIndex' => 0,
                'PageSize' => 100,
                'SortBy' => 0,
                'OrderBy' => 0,
                'SortOrder' => 0,
                'ValidOn' => date('Y-m-d') . 'T' . date('H:i:s'),
            ));
            $res = json_decode(json_encode(simplexml_load_string($result->GetBundleUsageListResult->ListInfo->any)));
            $this->final = $res;
            //mail('lainard@gmail.com', 'bundle', print_r($this->ServiceBundle->__getLastRequest() . " " . $this->ServiceBundle->__getLastResponse(), true));
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }

        // mail('mail@simson.one','Usage List', print_r(simplexml_load_string($result->GetBundleUsageListResult->ListInfo->any), 1));


        return $this->final;
    }
    public function getCLI($SN)
    {
        try {
            $result = $this->ServiceCLI->GetCLI(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
            ));
            $res = json_decode(json_encode(simplexml_load_string($result->GetCLIResult->ItemInfo->any)));
            $this->final = $res->NewDataSet->CLI;
            //mail('lainard@gmail.com', 'bundle', print_r($this->ServiceBundle->__getLastRequest() . " " . $this->ServiceBundle->__getLastResponse(), true));
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetOperatorList()
    {
        try {
            $result = $this->ServiceBaseTables->GetOperatorList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
            ));
            return json_decode(json_encode(simplexml_load_string($result->GetOperatorListResult->ListInfo->any)));
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetBundleUsage($id)
    {
        try {
            $result = $this->ServiceBundle->GetBundleUsage(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'BundleUsageId' => $id,
            ));
            return json_decode(json_encode(simplexml_load_string($result->GetBundleUsageResult->ItemInfo->any)));
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }

    public function GetBundleUsageList1($SN, $id)
    {
        try {
            $result = $this->ServiceBundle->GetBundleUsageList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
                'BundleAssignId' => $id,
                'PageIndex' => 0,
                'PageSize' => 100,
                'SortBy' => 0,
                'OrderBy' => 0,
                'SortOrder' => 0,
                'ValidOn' => date('Y-m-d') . 'T' . date('H:i:s'),
            ));
            $res = json_decode(json_encode(simplexml_load_string($result->GetBundleUsageListResult->ListInfo->any)));
            $final = array(
                'BundleUsageId' => '',
                'BundleAssignId' => '',
                'BundleDefinitionId' => '',
                'SN' => $SN,
                'ValidFrom' => '',
                'ValidUntil' => '',
                'AssignedValue' => '',
                'UsedValue' => '',
                'ReservedValue' => '',
                'BundleDefinitionId' => array(),
            );
            if (is_array($res->NewDataSet->BundleUsage)) {
                foreach ($res->NewDataSet->BundleUsage as $row) {
                    $final['BundleAssignId'] = $row->BundleAssignId;
                    $final['ValidFrom'] = $row->ValidFrom;
                    $final['ValidUntil'] = $row->ValidUntil;
                    $final['AssignedValue'] = $final['AssignedValue'] + (int) $row->AssignedValue;
                    $final['UsedValue'] = $final['UsedValue'] + $row->UsedValue;
                    if (isset($row->BundleDefinitionId)) {
                        $final['BundleDefinitionId'] = 100;
                    }
                }
                $this->final = $final;
            } else {
                $this->final = $res;
            }
            //mail('lainard@gmail.com', 'bundle', print_r($this->ServiceBundle->__getLastRequest() . " " . $this->ServiceBundle->__getLastResponse(), true));
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetPendingPorts()
    {
        try {
            $result = $this->ServicePorting->GetPendingPortIns(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'ContactId' => '-2',
                'MaxResultSet' => 5000,
                'MaxResultSetSpecified' => true,
                'PortOutFilter' => 'NONE'
            ));

            return $result;
            exit;
            if ($result->GetNumberPortListResult->Result == "0") {
                $this->final->result = 'success';
                $this->final->data = $result->GetNumberPortListResult->ListInfo;
            } else {
                $this->final->result = 'error';
                $this->final->message = $result->GetNumberPortListResult->Result;
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function CancelPortIn($SN)
    {
        $s = 'error';
        $result = $this->ServicePorting->CancelPortIn(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'Sn' => $SN,
        ));
        switch ($result->CancelPortInResult) {
            case 0:
                $rmessage = 'Success';
                $s = 'success';
                break;
            case -1:
                $rmessage = 'SN does not exist';
                break;
            case -2:
                $rmessage = 'SN is required :' . $SN;
                break;
            case -3:
                $rmessage = 'No port-in request found for this SN';
                break;
            case -4:
                $rmessage = 'Port-in request status invalid';
                break;
            case -5:
                $rmessage = 'Could not delete task and job for this Port-in request';
                break;
            case -6:
                $rmessage = 'Handling of previous jobs failed because of invalid job count';
                break;
            case -7:
                $rmessage = 'The request cannot be canceled. The wish date should at least be 4 days in the future.';
                break;
            case -8:
                $rmessage = 'The PortIn cannot be cancelled for this number because the current status is Pending.';
                break;
            case -90:
                $rmessage = 'Authentication failed';
                break;
            default:
                $rmessage = 'Unknown Error...';
                break;
        }
        return (object) array(
            'result' => $s,
            'message' => $rmessage,
            'request' => $this->ServicePorting->__getLastRequest(),
            'response' => $this->ServicePorting->__getLastResponse(),
        );
    }
    public function GetNumberPort($companyid)
    {
        try {
            $result = $this->ServiceNumberPort->GetNumberPortList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'OperatorId' => $companyid,
                'PageSize' => 1,
                'PageIndex' => 0,
                'SortBy' => 0,
                'Prefix' => '%',
                'ShowBestMatch' => false,
                'SortOrder' => 1,
            ));
            echo "REQUEST:\n" . $this->ServiceNumberPort->__getLastRequest() . "\n";
            echo "Response:\n" . $this->ServiceNumberPort->__getLastResponse() . "\n";
            return $result;
            exit;
            if ($result->GetNumberPortListResult->Result == "0") {
                $this->final->result = 'success';
                $this->final->data = $result->GetNumberPortListResult->ListInfo;
            } else {
                $this->final->result = 'error';
                $this->final->message = $result->GetNumberPortListResult->Result;
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function AddNumberPort($port)
    {
        try {
            $result = $this->ServiceNumberPort->AddNumberPort(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'Prefix' => $port['Prefix'],
                'OperatorId' => $port['OperatorId'],
                'ValidFrom' => $port['ValidFrom'],
                'Area' => '',
                'RoutingInfo' => '',
            ));
            return $result;
            exit;
            if ($result->AddNumberPortResult->Result == "0") {
                $this->final->result = 'success';
                $this->final->data = $result->AddNumberPortResult->NewItemId;
            } else {
                $this->final->result = 'error';
                $this->final->message = $result->AddNumberPortResult->Result;
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function DelNumberPort($portid)
    {
        try {
            $result = $this->ServiceNumberPort->DeleteNumberPort(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'NumberPortId' => $portid,
            ));
            return $result;
            exit;
            if ($result->DeleteNumberPortResult->Result == "0") {
                $this->final->result = 'success';
            } else {
                $this->final->result = 'error';
                $this->final->message = $result;
            }
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function GetPendingPortOuts()
    {
        if (!in_array($this->companyid, array(53, 54, 55, 33,56))) {
            $result = $this->ServicePorting->GetPendingPorts(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'PortInFilter' => 'NONE',
                'PortOutFilter' => 'PENDINGANDREJECTED',
            ));
        } else {
            $result = $this->ServicePorting->GetPendingPortOuts(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'ContactId' => -2,
            ));
        }
        // mail('mail@simson.one','portinpending',print_r($result, true));
        //return array('request'=> '', 'response'=> $this->ServicePorting->__getLastResponse());
        return $result;
    }

    public function GetContactType2List($id)
    {
        $result = $this->ServiceContactType2->GetContactType2List(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'ParentId' => $id,
        ));

        $res = json_decode(json_encode(simplexml_load_string($result->GetContactType2ListResult->ListInfo->any)));
        return $res->NewDataSet->ContactType2;
    }
    public function GetContactType2($ContactType2Id)
    {
        $result = $this->ServiceContactType2->GetContactType2(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'ContactType2Id' => $ContactType2Id,
        ));

        $res = json_decode(json_encode(simplexml_load_string($result->GetContactType2Result->ItemInfo->any)));
        return $res->NewDataSet->ContactType2;
    }
    public function GetFilteredListOfNumbers($contactid)
    {
        $result = $this->ServiceContactType2->GetFilteredListOfNumbers(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'ContactType2Id' => $contactid,
            'PageIndex' => 0,
            'PageSize' => 1000,
            'SortBy' => 0,
            'SortOrder' => 0,
        ));

        $res = json_decode(json_encode(simplexml_load_string($result->GetFilteredListOfNumbersResult->ListInfo->any)));
        return $res;
        // return $result;
    }
    public function GetPendingPortIns($admin = false)
    {
        if (empty($this->CI->session->language)) {
            $lg = "dutch";
        } else {
            $lg = $this->CI->session->language;
        }
        $this->CI->load->helper('language');
        $this->CI->config->set_item('language', $lg);
        $this->CI->lang->load('admin');
        $cnt = 0;
        $res = array();
        if (!in_array($this->companyid, array(53, 54, 56, 33))) {
            //error_reporting(1);
            ///print_r($this->params);
            $result = $this->ServicePorting->GetPendingPorts(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'PortInFilter' => 'PENDINGANDREJECTED',
                'PortOutFilter' => 'NONE',
            ));

            // print_r($result);
            if (is_array($result->GetPendingPortsResult->PortRequest)) {
                foreach ($result->GetPendingPortsResult->PortRequest as $pe) {
                    $pentry["OperationType"] = "PORT_IN";
                    $pentry["CompanyName"] = "";
                    $pentry["AuthorisedRequestrName"] = "";
                    $pentry["VATNumber"] = "";
                    $pentry["SN"] = $pe->Sn;
                    $pentry["Country"] = "BE";
                    $pentry['RequestType'] = $pe->RequestType;
                    $pentry["DonorOperator"] = $pe->DonorOperator;
                    $pentry["DonorSIM"] = $pe->DonorSIM;
                    $pentry["MSISDN"] = $pe->MSISDN;
                    $pentry["EnteredDate"] = $pe->EnteredDate;
                    $pentry["ActionDate"] = $pe->ActionDate;
                    $pentry["ErrorValue"] = $pe->Error->Value;
                    $pentry["ErrorComment"] = $pe->Error->Comment;
                    if ($pe->Status == "REJECTED") {
                        $pentry["Status"] = "Port in Rejected";
                        $pentry["Remark"] = $pe->Error->Comment;
                    } else {
                        $pentry["Status"] = $pe->Status;
                        $pentry["ErrorValue"] = '';
                    }

                    $res[] = $pentry;
                }
            }
            return $res;
        } else {
            if (in_array($this->companyid, array(53,56))) {
                if (file_exists('/home/mvno/public_html/application/queue/'.$this->companyid.'_portin.date')) {
                    log_message('error', 'File found');
                    $now = time();
                    $lastUpdate = file_get_contents('/home/mvno/public_html/application/queue/'.$this->companyid.'_portin.date');
                    $diff = $now-$lastUpdate;
                    log_message('error', 'last Update:'.$lastUpdate);
                    if ($diff >= 1200) {
                        $res=$this->getFucthPortinpending();

                        $this->db->query("delete from a_portin_management where companyid=?", array($this->companyid));
                        $this->db->insert_batch('a_portin_management', $res);
                        file_put_contents('/home/mvno/public_html/application/queue/'.$this->companyid.'_portin.date', time());
                        log_message('error', 'get New one because its older than 15 min Update:'.$lastUpdate);
                    } else {
                        $h = $this->db->where('companyid', $this->companyid);
                        $f  = $this->db->get('a_portin_management');

                        $res = $f->result_array();
                        log_message('error', 'Skiping because its new:'.$lastUpdate);
                    }
                } else {
                    $res=$this->getFucthPortinpending();


                    log_message('error', 'No file found Inserting new one');
                    file_put_contents('/home/mvno/public_html/application/queue/'.$this->companyid.'_portin.date', time());
                    $this->db->query("delete from a_portin_management where companyid=?", array($this->companyid));
                    $this->db->insert_batch('a_portin_management', $res);
                }

                //mail('mail@simson.one','GetPendingPortIns', print_r($res, true));
            }




            return $res;
        }
    }
    /*
    SELECT 0, 'Port in Initiated', 0 as Seq
    UNION SELECT 30, 'Port in Wait for verificationcode', 1
    UNION SELECT 31, 'Port in Validate verificationcode', 2
    UNION SELECT 2, 'Port in Ready to request', 10
    UNION SELECT 3, 'Port in Canceled', 90
    UNION SELECT 4, 'Port in Requested', 30
    UNION SELECT 5, 'Port in Pending', 40
    UNION SELECT 32, 'Port in Cancel requested', 50
    UNION SELECT 6, 'Port in Ready to cancel', 55
    UNION SELECT 7, 'Port in Exec rejected', 60
    UNION SELECT 8, 'Port in Rejected', 80
    UNION SELECT 24, 'Port in Exec pending', 45
    UNION SELECT 25, 'Port in Accepted', 70
    UNION SELECT 26, 'Port in Ready', 75
    UNION SELECT 27, 'Port in Failed', 95
     */

    public function getFucthPortinpending()
    {
        $result = $this->ServicePorting->GetPendingPortIns(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'PortInFilter' => 'PENDINGANDREJECTED',
                'PortOutFilter' => 'PENDINGANDREJECTED',
                'ContactId' => -2,
                'MaxResultSet' => 5000,
                'MaxResultSetSpecified' => true
            ));
        file_put_contents('/tmp/log.porting', print_r($result, true));
        if ($result->GetPendingPortInsResult->TotalItems > 1) {
            foreach ($result->GetPendingPortInsResult->ListInfo as $pe) {
                $pentry["OperationType"] = "PORT_IN";
                $pentry["CompanyName"] = "";
                $pentry["AuthorisedRequestrName"] = "";
                $pentry["VATNumber"] = "";
                $pentry["SN"] = trim($pe->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn);

                if ($pe->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity) {
                    foreach ($pe->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity as $pp) {
                        if ($pp->ParameterId == 20500) {
                            $pentry["DonorOperator"] = $pp->ParameterValue;
                        }
                        if ($pp->ParameterId == 20501) {
                            $pentry["DonorSIM"] = $pp->ParameterValue;
                        }
                        if ($pp->ParameterId == 20508) {
                            $pentry["MSISDN"] = $pp->ParameterValue;
                        }
                        if ($pp->ParameterId == 20502) {
                            $pentry["RequestType"] = $pp->ParameterValue;
                        }
                        //20526
                        if ($pp->ParameterId == 20506) {
                            $pentry["AccountNumber"] = $pp->ParameterValue;
                        }
                        if ($pp->ParameterId == 20516) {
                            $pentry["EnteredDate"] = $pp->ParameterValue;
                        }

                        if ($pp->ParameterId == 20517) {
                            $pentry["ActionDate_old"] = $pp->ParameterValue;
                        }
                        if ($pp->ParameterId == 20513) {
                            $pentry["ErrorID"] = $pp->ParameterValue;
                        }
                        if ($pp->ParameterId == 20514) {
                            $pentry["ErrorValue"] = $pp->ParameterValue;
                            $pentry["ErrorComment"] = $pp->ParameterValue;
                        }
                        if ($pp->ParameterId == 20515) {
                            switch ($pp->ParameterValue) {
                                case '0':
                                    $pentry["Status"] = 'Port in Not yet complete';
                                    break;
                                case '2':
                                    $pentry["Status"] = 'Port in Ready to request';
                                    break;
                                case '4':
                                    $pentry["Status"] = 'Port in Requested';
                                    break;
                                case '5':
                                    $pentry["Status"] = 'Port in Pending';
                                    break;
                                case '24':
                                    $pentry["Status"] = 'Port in Exec pending';
                                    break;
                                case '6':
                                    $pentry["Status"] = 'Port in Ready to cancel';
                                    break;
                                case '7':
                                    $pentry["Status"] = 'Port in Exec rejected';
                                    break;
                                case '25':
                                    $pentry["Status"] = 'Port in Accepted';
                                    break;
                                case '26':
                                    $pentry["Status"] = 'Port in Ready';
                                    break;
                                case '8':
                                    $pentry["Status"] = 'Port in Rejected';
                                    break;
                                case '3':
                                    $pentry["Status"] = 'Port in Canceled';
                                    break;
                                case '27':
                                    $pentry["Status"] = 'Port in Failed';
                                    break;
                                case '21':
                                    $pentry["Status"] = 'Ported in';
                                    break;
                                case '30':
                                    $pentry["Status"] = 'Port in Wait for verificationcode';
                                    break;
                                case '31':
                                    $pentry["Status"] = 'Port in Validate verificationcode';
                                    break;
                                case '32':
                                    $pentry["Status"] = 'Port in Cancel requested';
                                    break;
                                default:
                                    $pentry["Status="] = '';
                            }

                            //$pentry["Status= $pp->ParameterValue;
                        }

                        if ($pp->ParameterId == 20526) {
                            $pentry["ActionDate"] = $pp->ParameterValue;
                            /*if (!empty($pp->ParameterValue)) {

                                $pentry["Status"] = 'Port in Accepted';

                            }
                            */
                        }
                    }
                }


                $pentry["Country"] = "NL";
                $pentry["companyid"] = $this->companyid;
                $res[] = $pentry;
            }
        }

        return $res;
    }
    public function SaveVerificationCode($sn, $code)
    {
        $result = $this->ServicePorting->SaveVerificationCode(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'Sn' => $sn,
            'Code' => $code,
        ));

        switch ($result->SaveVerificationCodeResult) {
            case "0":
                $res = "success";
                break;
            case "-1":
                $res = "Sn-field is incorrect";
                break;
            case "-2":
                $res = "Sn-field is missing";
                break;
            case "-3":
                $res = "No PortIn in progress for Sn: '{0}' to save verification code";
                break;
            case "-4":
                $res = "PortIn for Sn is in wrong state: '{0}', the current status is: '{1}'";
                break;
            case "-5":
                $res = "PortIn for Sn is in a corrupt state";
                break;
            default:
                $res = "Unknown Error";
        }

        if ($result->SaveVerificationCodeResult == "0") {
            return (object) array(
                'result' => 'success',
                'message' => $res,
                'response' => $this->ServicePorting->__getLastResponse(),
                'request' => $this->ServicePorting->__getLastRequest(),
            );
        } else {
            return (object) array(
                'result' => 'error',
                'message' => $res,
                'response' => $this->ServicePorting->__getLastResponse(),
                'request' => $this->ServicePorting->__getLastRequest(),
            );
        }
    }
    public function GetAvailableProvisioningSims($simnumber)
    {
        $result = $this->ServiceCLI->GetAvailableProvisioningSims(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SimNumber' => $simnumber,
        ));
        return array(
            'response' => $this->ServiceCLI->__getLastResponse(),
            'request' => $this->ServiceCLI->__getLastRequest(),
        );
    }
    public function GetActivationCPSList()
    {
        $result = $this->ServiceGeneral->GetActivationCPSList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
        ));
        return array(
            'response' => $this->ServiceGeneral->__getLastResponse(),
            'request' => $this->ServiceGeneral->__getLastRequest(),
            'array' => $this->soap2array($this->ServiceGeneral->__getLastResponse()),
        );
    }
    public function GetSimList()
    {
        $this->ServiceMobile->GetSimList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SIMStatus' => '5',
        ));
        $res = $this->soap2array($this->ServiceMobile->__getLastResponse());
        return $res['soapBody']['GetSIMListResponse']['GetSIMListResult']['ListInfo']['diffgrdiffgram']['NewDataSet']['SIM'];
    }

    public function GetSimListbyContact($id)
    {
        $this->ServiceMobile->GetSimList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'ContactType2Id' => $id,
        ));
        $res = $this->soap2array($this->ServiceMobile->__getLastResponse());
        return $res['soapBody']['GetSIMListResponse']['GetSIMListResult']['ListInfo']['diffgrdiffgram']['NewDataSet']['SIM'];
    }
    public function GetRequestPorting($sn)
    {
        $result = $this->ServicePorting->GetPendingPortIns(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'ContactId' => '-2',
            'MaxResultSet' => 1000,
        ));
        return array(
            'response' => $this->ServicePorting->__getLastResponse(),
            'request' => $this->ServicePorting->__getLastRequest(),
            'array' => $this->soap2array($this->ServicePorting->__getLastResponse()),
        );
    }
    public function soap2array($response)
    {
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", '$1$2$3', $response);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        return json_decode($json, true);
    }
    public function artilium_getmsisdnfromsimnr($SIMNr)
    {
        $resultIMSIList = $this->ServiceMobile->GetIMSIList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SIMNr' => $SIMNr,
        ));
        if ($resultIMSIList->GetIMSIListResult->TotalItems > 0) {
            $sN2 = simplexml_load_string($resultIMSIList->GetIMSIListResult->ListInfo->any);
            //print_r($sN2);exit;
            foreach ($sN2->NewDataSet->IMSI as $poN) {
                $IMSINr = (string) $poN->IMSINr;
            }
            //echo "\nIMSINr:".$IMSINr;exit;
            $resultIMSIMSISDNList = $this->ServiceMobile->GetIMSIMSISDNList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'IMSINr' => $IMSINr,
            ));
            //print_r($resultIMSIMSISDNList);exit;
            if ($resultIMSIMSISDNList->GetIMSIMSISDNListResult->TotalItems > 0) {
                $sN3 = simplexml_load_string($resultIMSIMSISDNList->GetIMSIMSISDNListResult->ListInfo->any);
                foreach ($sN3->NewDataSet->IMSIMSISDN as $poN2) {
                    $MSISDNId = (string) $poN2->MSISDNId;
                }
                //echo "\nMSISDNId:".$MSISDNId;exit;
                $resultGetMSISDN = $this->ServiceMobile->GetMSISDN(array(
                    'Login' => $this->params->m_username,
                    'Password' => $this->params->m_password,
                    'MSISDNId' => $MSISDNId,
                ));
                //print_r($resultGetMSISDN);exit;
                if ($resultGetMSISDN->GetMSISDNResult->Result == 0) {
                    $SNxml = simplexml_load_string($resultGetMSISDN->GetMSISDNResult->ItemInfo->any);
                    foreach ($SNxml->NewDataSet->MSISDN as $poN3) {
                        $MSISDNNr = (string) $poN3->MSISDNNr;
                    }
                    //print_r($SNxml);exit;
                    //echo "\nSN:".$SN;exit;
                    return $MSISDNNr;
                } else {
                    //Error from GetMSISDN
                    return "";
                }
            } else {
                //Error from GetIMSIMSISDNList
                return "";
            }
            return $IMSINr;
        } else {
            //Error from GetIMSIList
            return "";
        }
    }


    public function UpdateContact($client, $newroot)
    {
        // log_message('error', "Contactid :".$rootid." ".$newroot." ".$client->ContactType2Id);
        if ($client->ContactType2Id <= 0) {
            $data = array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'ContactType2Id' => $client->ContactType2Id,
                'ParentId' => getContactTypeid($client->agentid),
                'FirstName' => $client->firstname,
                'LastName' => $client->lastname,
                'Name' => trim($client->firstname).' '.trim($client->lastname),
                'Street' => $client->address1. ' '.$client->housenumber,
                'City' => $client->city,
                'Email' => $client->email,
                'Phone' => $client->phonenumber,
                'ZIPCode' => $client->postcode);
            $res = $this->ServiceContactType2->AddContactType2($data);
            log_message('error', 'Update Contact'.print_r($res, true));
        } else {
            $data = array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'ContactType2Id' => $client->ContactType2Id,
                'FirstName' => $client->firstname,
                'LastName' => $client->lastname,
                'Name' => trim($client->firstname).' '.trim($client->lastname),
                'Street' => $client->address1.' '.$client->housenumber,
                'City' => $client->city,
                'Email' => $client->email,
                'ParentId' => getContactTypeid($client->agentid),
                'Phone' => $client->phonenumber,
                'ZIPCode' => $client->postcode);


            //  $data['ParentId'] = getContactTypeid($client->agentid);

            log_message('error', 'Update Contact1'.print_r($data, true));
            $res = $this->ServiceContactType2->UpdateContactType2($data);
            log_message('error', 'Update Contact2'.print_r($res, true));
        }
    }

    public function UpdateCLI($sn, $status)
    {
        if ($status == 1) {
            $resupd = $this->ServiceCLI->UpdateCLI(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => trim($sn),
                'Status' => 1,
                'ActivationCPS' => 1,
            ));
        } else {
            $resupd = $this->ServiceCLI->UpdateCLI(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => trim($sn),
                'Status' => $status,
            ));
        }
        if ($resupd->UpdateCLIResult == "0") {
            return (object) array(
                'result' => 'success',
            );
        } else {
            return (object) array(
                'result' => 'error',
            );
        }
    }

    public function UpdateCLITest()
    {
        $resupd = $this->ServiceCLI->UpdateCLI(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => "31648922713",
                'ContactName' => "Pablo Maximiliano Campos",

            ));

        return array('object' => $resupd, 'request'=>  $this->ServiceCLI->__getLastRequest(), 'response' => $this->ServiceCLI->__getLastResponse());
    }

    public function ChangeCountryGroup($sn, $countrygroup)
    {
        $resupd = $this->ServiceCLI->UpdateCLI(array(
               'Login' => $this->params->m_username,
               'Password' => $this->params->m_password,
               'SN' => trim($sn),
               'CountryGroup' => $countrygroup
           ));

        if ($resupd->UpdateCLIResult == "0") {
            return (object) array(
                'result' => 'success',
            );
        } else {
            return (object) array(
                'result' => 'error',
                'message' => $resupd
            );
        }
    }
    public function SwitchVoiceMail($msisdn, $sn, $status)
    {
        if ($status == "1") {
            $resupd = $this->ServiceCLI->UpdateCLI(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $sn,
                'DestinationsAcceptedRejected' => $status,
                'DestinationsAccepted' => '00316240' . substr(trim($msisdn), 2),
            ));
        } else {
            $resupd = $this->ServiceCLI->UpdateCLI(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $sn,
                'DestinationsAcceptedRejected' => $status,
                'DestinationsAccepted' => '',
            ));
        }
        if ($resupd->UpdateCLIResult == "0") {
            return (object) array(
                'result' => 'success',
            );
        } else {
            return (object) array(
                'result' => 'error',
            );
        }
    }
    public function TerminateCLI($sn, $date)
    {
        $resupd = $this->ServiceCLI->UpdateCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'DateRangeUntil' => $date,
        ));
        if ($resupd->UpdateCLIResult == "0") {
            return (object) array(
                'result' => 'success',
            );
        } else {
            return (object) array(
                'result' => 'error',
            );
        }
    }

    public function CancelTerminateCLI($sn)
    {
        $resupd = $this->ServiceCLI->UpdateCLI(array(
          'Login' => $this->params->m_username,
          'Password' => $this->params->m_password,
          'SN' => $sn,
          'DateRangeUntil' => '2037-12-31T00:00:00'
        ));

        if ($this->log) {
            mail("mail@simson.one", "eror", "SOAP FAULT: Bundle Assignment \n" .  $this->ServiceCLI->__getLastRequest() .  $this->ServiceCLI->__getLastResponse() . "\n||\nRESPONSE\n" . $fault);
        }
        if ($resupd->UpdateCLIResult == "0") {
            return (object) array(
            'result' => 'success',
            );
        } else {
            return (object) array(
            'result' => 'error',
            );
        }
    }
    public function UpdateLanguage($sn, $msisdn_languageid)
    {
        $resupd = $this->ServiceCLI->UpdateCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'LanguageId' => $msisdn_languageid,
        ));
        if ($resupd->UpdateCLIResult == "0") {
            return (object) array(
                'result' => 'success',
            );
        } else {
            return (object) array(
                'result' => 'error',
            );
        }
    }
    public function PortingNewSIM($ContactName, $m)
    {
        try {
            // mail('mail@simson.one',"porting error :".$ContactName, print_r($m, true));
            $InitialAmount = 0;

            //With porting, the initial reload needs to be on the original MSISDN
            //$MSISDN = $this->artilium_getmsisdnfromsimnr($m->msisdn_sim);
            if ($m->msisdn_type == "porting") {
                if (empty($m->donor_customertype)) {
                    $m->donor_customertype = "0";
                }
                //Successful Update SIM
                //So now we do the PortInRequest:
                //logActivity("now we try porting in ");

                if ($m->ptype_belgium != "NA") {
                    if (strlen($m->donor_sim) > 18) {
                        $m->donor_sim = substr($m->donor_sim, -13);
                    }
                    $reqarray = array(
                        'Login' => $this->params->m_username,
                        'Password' => $this->params->m_password,
                        'PortRequest' => array('Sn' => $m->msisdn_sn,
                            'OperationType' => 'PORT_IN',
                            'RequestType' => $m->ptype_belgium,
                            'DonorOperator' => $m->donor_provider,
                            'MSISDN' => $m->donor_msisdn,
                            'DonorSIM' => $m->donor_sim,
                            'CustomerName' => $ContactName,
                            'ActionDate' => '0001-01-01T00:00:00',
                            'EnteredDate' => '0001-01-01T00:00:00',
                            'Status' => 'IDLE',
                            'Error' => array('ID' => 0, 'Value' => '', 'Comment' => ''),
                        ));

                    if ($m->ptype_belgium != "PREPAY") {
                        $reqarray['PortRequest']['AccountNumber'] = $m->donor_accountnumber;
                        $reqarray['PortRequest']['VATNumber'] = getCustomerVat($m->userid);
                        $reqarray['PortRequest']['AuthorisedRequestorName'] = $ContactName;
                        $reqarray['PortRequest']['CompanyName'] = getCustomerCompanyName($m->userid);
                    } else {
                        $reqarray['PortRequest']['AccountNumber'] = '';
                        $reqarray['PortRequest']['VATNumber'] = '';
                        $reqarray['PortRequest']['AuthorisedRequestorName'] = '';
                        $reqarray['PortRequest']['CompanyName'] = '';
                    }
                    log_message('error', print_r($reqarray, true));
                    $resportin = $this->ServicePorting->PortInRequest($reqarray);

                    // mail("mail@simson.one", "Result " . $m->donor_provider . " " . $m->donor_sim, "SOAP Response: Porting Assignment \n" . $this->ServicePorting->__getLastRequest() . $this->ServicePorting->__getLastResponse() . "\n||\nRAW REQUEST\n" . print_r($reqarray, true));
                    log_message('error', 'Portin Request.'.$this->ServicePorting->__getLastResponse());
                    return (object) array(
                        'result' => 'success',
                        'id' => $resportin->OperationResponse->Result,
                    );
                } else {
                    if ($m->date_wish < date('Y-m-d')) {
                        $date = date('Y-m-d', strtotime(date('Y-m-d'). ' + 33 days'));
                        $this->db->query("update a_services_mobile set date_wish=? where serviceid=?", array($date, $m->serviceid));
                    } else {
                        $date =  $m->date_wish;
                    }
                    $reqarray = array(
                        'Login' => $this->params->m_username,
                        'Password' => $this->params->m_password,
                        'Sn' => $m->msisdn_sn,
                        'PortingType' => $m->donor_type,
                        'AccountNumber' => $m->donor_accountnumber,
                        'MsisdnNumber' => $m->donor_msisdn,
                       /* 'PortInWishDate' => $date,*/
                        'CustomerType' => $m->donor_customertype,
                    );
                    log_message('error', print_r($reqarray, true));
                    if (empty($m->donor_accountnumber)) {
                        unset($reqarray['AccountNumber']);
                        if ($m->donor_customertype == 1) {
                            $reqarray['CustomerType'] = "0";
                        }

                        unset($reqarray['AccountNumber']);
                        $reqarray['CustomerType'] = "0";
                    } else {
                        $reqarray['CustomerType'] = "1";
                        $reqarray['PortingType'] = "1";
                    }

                    $resportin = $this->ServicePorting->PortInRequest($reqarray);
                    log_message('error', $this->ServicePorting->__getLastRequest());
                    log_message('error', $this->ServicePorting->__getLastResponse());
                    $result_portin = $resportin->PortInRequestResult;
                    // mail("mail@simson.one", "Porting Result", "SOAP FAULT: Porting Assignment \n".$this->ServicePorting->__getLastRequest() ."\n" .$this->ServicePorting->__getLastResponse() . "\n||\nRAW REQUEST\n".print_r($reqarray, true));

                    insertAPILog(array('api_call' => 'PortInRequest', 'api_env' => $this->params->arta_version, 'request' => $this->ServicePorting->__getLastRequest(), 'result' => $this->ServicePorting->__getLastResponse(), 'ip' => '127.0.0.1', 'companyid' => $this->companyid));

                    return (object) array(
                        'result' => 'success',
                        'id' => $resportin->PortInRequestResult,
                        'request' => $this->ServicePorting->__getLastRequest(),
                        'response' => $this->ServicePorting->__getLastResponse(),
                    );
                }
            } else {
                return (object) array(
                    'result' => 'error',
                    'message' => 'The service needs to be set as porting before requesting porting',
                );
            }
        } catch (SoapFault $fault) {
            if ($this->log) {
                mail("lainard@gmail.com", "eror", "SOAP FAULT: Bundle Assignment \n" . $resultB->__getLastRequest() . $resultB->__getLastResponse() . "\n||\nRESPONSE\n" . $fault);
            }
        }
    }

    public function MoveCLI($sn, $ContactType2Id, $copybundle = 0)
    {
        $res =   $this->ServiceCLI->MoveCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'ContactType2Id' => $ContactType2Id,
            'CopyCurrentBundles' => $copybundle,
            'Logging' => array(
                'AccountType' => 0,
                'Account' => '',
            ),
        ));

        return $res;
    }
    public function GetMasterProfiles()
    {
        //$this->ServiceBaseTables = new soapclient($this->service . '/ServiceBaseTables.asmx?WSDL', $this->option);
        $data = array( 'Login' => $this->params->m_username,
            'Password' => $this->params->m_password);

        //return $this->ServiceBaseTables->GetMasterProfiles($data);
    }
    public function CreateContact($client, $rootid)
    {
        $data = array( 'Login' => $this->params->m_username,
            'Password' => $this->params->m_password);

        if (!empty($client['companyname'])) {
            $data['Name'] = $client['companyname'];
        } else {
            $data['Name'] = $client['firstname'].' '.$client['lastname'];
        }

        if ($rootid != "-1") {
            $data['ParentId'] = $rootid;
        }

        $data['FirstName'] = $client['firstname'];
        $data['LastName'] = $client['lastname'];
        $data['Street'] = $client['address1'];
        $data['City'] = $client['city'];
        $data['Phone'] = $client['phonenumber'];
        $data['Email'] = $client['email'];
        $data['ZIPCode'] = $client['postcode'];
        $res = $this->ServiceContactType2->AddContactType2(array_filter($data));
        if ($res->AddContactType2Result->Result == 0) {
            return $res->AddContactType2Result->NewItemId;
        } else {
            return false;
        }

        //mail('mail@simson.one',"contactid",print_r($res, true));
    }
    public function DeleteContactType2($data)
    {
        $data['Login'] = $this->params->m_username;
        $data['Password'] = $this->params->m_password;


        return $this->ServiceContactType2->DeleteContactType2(array_filter($data));
    }
    public function ActiveNewSIM($ContactName, $m)
    {
        $gid = getGroupid($m->serviceid);
        $InitialAmount = getProductReloadAmount($gid);
        $ContactType2Id = $m->msisdn_contactid;
        //With porting, the initial reload needs to be on the original MSISDN
        $MSISDN = $this->artilium_getmsisdnfromsimnr($m->msisdn_sim);

        //____________MOVE CLI_____________
        $resmove = $this->ServiceCLI->MoveCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $m->msisdn_sn,
            'ContactType2Id' => $ContactType2Id,
            'CopyCurrentBundles' => 0,
            'Logging' => array(
                'AccountType' => 0,
                'Account' => '',
            ),
        ));

        log_message('error', "Move CLI: ".print_r($this->ServiceCLI->__getLastResponse(), true), print_r($resmove, true));

        if (($resmove->MoveCLIResult->Result == "0") || ($resmove->MoveCLIResult->Result == "-10")) {
            //Add MMS / GPRS (wait 3 seconds first)
            //sleep(10);
            $this->AddMMSGPRS($m);

            //Success, so we now try to do the update
            //____________UPDATE CLI_____________
            if (substr(trim($m->msisdn), 0, 2) == "32") {
                if ($m->msisdn_type != "porting") {
                    $resupd = $this->ServiceCLI->UpdateCLI(array(
                        'Login' => $this->params->m_username,
                        'Password' => $this->params->m_password,
                        'SN' => $m->msisdn_sn,
                        'Status' => 1,
                        'ActivationCPS' => 1,
                        'LanguageId' => $m->msisdn_languageid,
                        'CustomerName' =>  substr($ContactName, 0, 12)
                    ));
                    $this->PartialFullBar($m, '0');
                } else {
                    $resupd = $this->ServiceCLI->UpdateCLI(array(
                        'Login' => $this->params->m_username,
                        'Password' => $this->params->m_password,
                        'SN' => $m->msisdn_sn,
                        /*
                        'Status' => 1,
                        'ActivationCPS' => 1,
                        */
                        'LanguageId' => $m->msisdn_languageid,
                        'CustomerName' =>  substr($ContactName, 0, 12)
                    ));
                }
            } else {
                $dd = array(
                    'Login' => $this->params->m_username,
                    'Password' => $this->params->m_password,
                    'SN' => $m->msisdn_sn,
                    'Status' => 1,
                    'ActivationCPS' => 1,
                    'LanguageId' => $m->msisdn_languageid,
                    'CustomerName' => substr($ContactName, 0, 12)
                );
                if ($m->companyid==33) {
                    $dd["CountryGroup"] = "42";
                }
                $resupd = $this->ServiceCLI->UpdateCLI($dd);
            }
            log_message('error', "UpdateCLI ".print_r($this->ServiceCLI->__getLastResponse(), true));
            log_message('error', print_r($resupd, true));
            if ($resupd->UpdateCLIResult == "0") {
                //Successful Update
                //Here we add the initial amount
                //logActivity('Going to start (Initial) Reload...');
                $resultAddCredits = $this->ServiceReload->Reload(array(
                    'Login' => $this->params->m_username,
                    'Password' => $this->params->m_password,
                    'Sn' => $m->msisdn_sn,
                    'ReloadAmount' => $InitialAmount,
                    'ReloadType' => 1,
                    'ReloadSubType' => 104,
                    'Description' => 'MVNO Portal initial add',
                    'PaymentType' => 0,
                    'OrderID' => '1st :' . $MSISDN,
                    'Status' => 0,
                ));

                $ressim = $this->ServiceMobile->UpdateSIM(array(
                    'Login' => $this->params->m_username,
                    'Password' => $this->params->m_password,
                    'SIMNr' => $m->msisdn_sim,
                    'Status' => 5,
                ));

                log_message('error', "UpdateSIM ".print_r($ressim, true));
                if ($ressim->UpdateSIMResult == "0") {
                    if ($m->msisdn_type == "porting") {
                        $resportin = $this->PortingNewSIM($ContactName, $m);
                        if (substr(trim($m->msisdn), 0, 2) == "32") {
                            $this->PartialFullBar($m, '0');
                        }
                        if ($resportin->id == "0") {
                            if (in_array($m->companyid, array('53'))) {
                                $this->createSUMassignment($m->msisdn_sn);
                            }
                            return (object) array(
                                'result' => 'success',
                            );
                        } else {
                            switch ($resportin->id) {
                                case '-2':
                                    $errtext = "SN is required";
                                    break;
                                case '-3':
                                    $errtext = "One or more mandatory parameters are missing.";
                                    break;
                                case '-4':
                                    $errtext = "SOne or more mandatory parameters are missing.";
                                    break;
                                case '-5':
                                    $errtext = "One or more mandatory parameters are missing.";
                                    break;
                                case '-6':
                                    $errtext = "CustomerType invalid";
                                    break;
                                case '-7':
                                    $errtext = "PortInWishDate invalid (system setting 10000 MNP Execution Time determines how many working days should be in between the request date and the wish date).";
                                    break;
                                case '-8':
                                    $errtext = "PortingType invalid";
                                    break;
                                case '-9':
                                    $errtext = "Network status of CLI/PIN not valid";
                                    break;
                                case '-10':
                                    $errtext = "MsisdnNumber already exists in reseller database";
                                    break;
                                case '-11':
                                    $errtext = "Port-out in progress for CLI/PIN";
                                    break;
                                case '-12':
                                    $errtext = "Port-in in progress for CLI/PIN or cancellation in progress";
                                    break;
                                case '-13':
                                    $errtext = "DonorOperator Invalid";
                                    break;
                                case '-14':
                                    $errtext = "MsisdnNumber Invalid";
                                    break;
                                case '-15':
                                    $errtext = "SimCardNumber Donor operator is Invalid";
                                    break;
                                case '-16':
                                    $errtext = "AccountNumber Invalid";
                                    break;
                                case '-90':
                                    $errtext = "Authentication failed";
                                    break;
                                case '-91':
                                    $errtext = "Field overflow (too many characters, date out of range, ...)";
                                    break;
                                case '-100':
                                    $errtext = "Unspecified Error";
                                    break;
                                default:
                                    $errtext = "Unknown Error...";
                                    break;
                            }
                            if ($resportin->id == '-12') {
                                return (object) array(
                                    'result' => 'success',
                                );
                            } else {
                                return (object) array(
                                    "result" => "error",
                                    "message" => "PORT IN ERROR :  " . $errtext . " " . print_r($resportin, true),
                                );
                            }
                        }
                    } else {
                        if (in_array($m->companyid, array('53'))) {
                            $this->createSUMassignment($m->msisdn_sn);
                        }
                        return (object) array(
                            "result" => "success",
                        );
                    }
                } else {
                    return (object) array(
                        "result" => "error",
                        "message" => "SIM UPDATE ERROR",
                    );
                }
            } else {
                return (object) array(
                    "result" => "error",
                    "message" => "CLI UPDATE ERROR",
                );
            }
        } else {
            $this->db->query("update a_services_mobile set arta_contactid_status=? where id=?", array("0", $m->id));
            $headers = "From: noreply@united-telecom.be" . "\r\n" .
                          "CC: thierry.van.eylen@united-telecom.be";
            mail('simson.parlindungan@united-telecom.be', ' MoveCLI', print_r(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $m->msisdn_sn,
                'ContactType2Id' => $ContactType2Id,
                'CopyCurrentBundles' => 0,
                'Logging' => array(
                    'AccountType' => 0,
                    'Account' => '',
                ),
               ), true).print_r($resmove, true), $headers);
            createWHMCSticket(array('companyid' => $this->companyid, 'mobile' => $m, 'result' => $resmove));
            log_message('error', 'Move CLI Error '.$m->msisdn_sn.' '.print_r($resmove, true));
            return (object) array(
                "result" => "error",
                "message" => "MOVE ERROR" . print_r($resmove, true),
            );
        }
    }
    public function limit_text($text, $limit)
    {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }

    public function GetSnMoxx($simcardnr)
    {
        $this->ServiceMobile->GetSIM(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SIMNr' => $simcardnr,
        ));

        return array('simcard' => $simcardnr, 'response' => $this->ServiceMobile->__getLastResponse(), 'request' => $this->ServiceMobile->__getLastRequest());
    }

    public function ActiveNewSIMMoxx($ContactName, $sn, $simnr)
    {
        $InitialAmount = 125;

        //With porting, the initial reload needs to be on the original MSISDN
        $MSISDN = $sn;
        //____________MOVE CLI_____________
        $resmove = $this->ServiceCLI->MoveCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'ContactType2Id' => 40,
            'CopyCurrentBundles' => 0,
            'Logging' => array(
                'AccountType' => 0,
                'Account' => '',
            ),
        ));
        if (($resmove->MoveCLIResult->Result == "0") || ($resmove->MoveCLIResult->Result == "-10")) {
            //Add MMS / GPRS (wait 3 seconds first)
            //sleep(10);
            $this->AddMMSGPRSMoxx($sn);
            //logActivity("Going to start PartialFullBar...");Fac
            if (substr(trim($sn), 0, 2) == "32") {
                $this->PartialFullBarMoxx($sn, 2);
            }
            // $this->PartialFullBar($m);
            //Success, so we now try to do the update
            //____________UPDATE CLI_____________
            if (substr(trim($sn), 0, 2) == "32") {
                $resupd = $this->ServiceCLI->UpdateCLI(array(
                    'Login' => $this->params->m_username,
                    'Password' => $this->params->m_password,
                    'SN' => $sn,
                    'Status' => 1,
                    'LanguageId' => 1,
                    'CustomerName' => $ContactName,
                ));
            } else {
                $resupd = $this->ServiceCLI->UpdateCLI(array(
                    'Login' => $this->params->m_username,
                    'Password' => $this->params->m_password,
                    'SN' => $sn,
                    'Status' => 1,
                    'ActivationCPS' => 1,
                    'LanguageId' => 1,
                    'CustomerName' => $ContactName,
                ));
            }
            if ($resupd->UpdateCLIResult == "0") {
                //Successful Update
                //Here we add the initial amount
                //logActivity('Going to start (Initial) Reload...');
                $resultAddCredits = $this->ServiceReload->Reload(array(
                    'Login' => $this->params->m_username,
                    'Password' => $this->params->m_password,
                    'Sn' => $sn,
                    'ReloadAmount' => $InitialAmount,
                    'ReloadType' => 1,
                    'ReloadSubType' => 104,
                    'Description' => 'MVNO Portal initial add',
                    'PaymentType' => 0,
                    'OrderID' => '1st :' . $MSISDN,
                    'Status' => 0,
                ));

                $ressim = $this->ServiceMobile->UpdateSIM(array(
                    'Login' => $this->params->m_username,
                    'Password' => $this->params->m_password,
                    'SIMNr' => $simnr,
                    'Status' => 5,
                ));
                if ($ressim->UpdateSIMResult == "0") {
                    return (object) array(
                        "result" => "success",
                    );
                } else {
                    return (object) array(
                        "result" => "error",
                        "message" => "SIM UPDATE ERROR",
                    );
                }
            } else {
                return (object) array(
                    "result" => "error",
                    "message" => "CLI UPDATE ERROR",
                );
            }
        } else {
            return (object) array(
                "result" => "error",
                "message" => "MOVE ERROR" . print_r($resmove->MoveCLIResult),
            );
        }
    }

    public function reloadCredit($m, $desc = false, $amount = false)
    {
        if (!$desc) {
            $gid = getGroupid($m->details->serviceid);
            $Amount = getProductReloadAmount($gid);
            $desc = 'MVNO Portal initial add';
        }



        $resultAddCredits = $this->ServiceReload->Reload(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'Sn' => $m->details->msisdn_sn,
            'ReloadAmount' => $Amount,
            'ReloadType' => 1,
            'ReloadSubType' => 104,
            'Description' => $desc,
            'PaymentType' => 0,
            'OrderID' => date('YmdHis') . $m->details->msisdn,
            'Status' => 0,
        ));
        log_message('error', $this->ServiceReload->__getLastRequest() . $this->ServiceReload->__getLastResponse());
        return array($resultAddCredits, $InitialAmount);
    }

    public function AddMMSGPRSMoxx($sn)
    {
        $result = $this->ServicePackages->GetListPackageOptionsForSN(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'ActiveAndNonActive' => true,
        ));
        if ($result->GetListPackageOptionsForSNResult->TotalItems > 0) {
            $s2 = simplexml_load_string($result->GetListPackageOptionsForSNResult->ListInfo->any);
            $i = 0;
            foreach ($s2->NewDataSet->PackageOptions as $po) {
                if ($po->CallModeDescription == "MMS") {
                    $this->UpdatePackageOptionsForSN($sn, $po->PackageDefinitionId, 1);
                }
                if ($po->CallModeDescription == "WEB") {
                    $this->UpdatePackageOptionsForSN($sn, $po->PackageDefinitionId, 1);
                }
                if ($po->CallModeDescription == "4G") {
                    $this->UpdatePackageOptionsForSN($sn, $po->PackageDefinitionId, 1);
                }
                if ($po->CallModeDescription == "Roaming") {
                    $PDID_Roaming = $po->PackageDefinitionId;
                    $this->UpdatePackageOptionsForSN($sn, $po->PackageDefinitionId, 1);
                }
                if ($po->CallModeDescription == "International Numbers") {
                    $this->UpdatePackageOptionsForSN($sn, $po->PackageDefinitionId, 1);
                }
                if (($po->CallModeDescription == "Premium gaming") || ($po->CallModeDescription == "Premium Gaming")) {
                    $this->UpdatePackageOptionsForSN($sn, $po->PackageDefinitionId, 1);
                }
                if ($po->CallModeDescription == "Premium Numbers") {
                    $PDID_BarPremiumVoice = $po->PackageDefinitionId;
                    $this->UpdatePackageOptionsForSN($sn, $po->PackageDefinitionId, 1);
                }
            }
        }
        return true;
    }

    public function AddMMSGPRS($m)
    {
        $result = $this->ServicePackages->GetListPackageOptionsForSN(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $m->msisdn_sn,
            'ActiveAndNonActive' => true,
        ));


        if ($result->GetListPackageOptionsForSNResult->TotalItems > 0) {
            $s2 = simplexml_load_string($result->GetListPackageOptionsForSNResult->ListInfo->any);
            $i = 0;
            foreach ($s2->NewDataSet->PackageOptions as $po) {
                if ($po->CallModeDescription == "MMS") {
                    $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->MMS);
                }
                if ($po->CallModeDescription == "WEB") {
                    $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->GPRS);
                }
                if ($po->CallModeDescription == "4G") {
                    $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->LTE);
                }
                if ($po->CallModeDescription == "Roaming") {
                    $PDID_Roaming = $po->PackageDefinitionId;
                    $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->Roaming);
                }
                if ($po->CallModeDescription == "International Numbers") {
                    $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->BarInternationalCalls);
                }
                if (($po->CallModeDescription == "Premium gaming") || ($po->CallModeDescription == "Premium Gaming")) {
                    $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->BarPremium);
                }
                if ($po->CallModeDescription == "Premium Numbers") {
                    $PDID_BarPremiumVoice = $po->PackageDefinitionId;
                    $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->BarPremiumVoice);
                }
            }
        }
        return true;
    }
    public function CReloadCredit($amount, $sn, $voucher, $orderremark)
    {
        $resultAddCredits = $this->ServiceReload->Reload(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'Sn' => $sn,
            'ReloadAmount' => $amount,
            'ReloadType' => 1,
            'ReloadSubType' => 104,
            'Description' => $voucher,
            'PaymentType' => 0,
            'OrderID' => $orderremark,
            'Status' => 0,
        ));
        log_message('error', $this->ServiceReload->__getLastRequest() . $this->ServiceReload->__getLastResponse());
        return $resultAddCredits;
    }

    public function AddCustomerProduct($data)
    {
        $AddCustomerProduct = $this->ServicePriceManagement->AddCustomerProduct(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'ContactType2Id' => $data['ContactType2Id'],
            'SN' => $data['SN'],
            'ProductId' => -1,
            'PackageId' => $data['PackageId'],
            'Quantity' => 0,
            'ValidFrom' => date('Y-m-d').'T'.date('H:i:s'),
            'Reference' => "Add Package Via European Portal V1",
        ));
        log_message('error', 'AddCustomerProduct'.print_r($AddCustomerProduct, 1));
        return $AddCustomerProduct;
    }

    public function GetCustomerProductList()
    {
        $ss = $this->ServicePriceManagement->GetCustomerProductList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
        ));
        $array = simplexml_load_string($ss->GetCustomerProductListResult->ListInfo->any);

        log_message('error', print_r($array, 1));
        return $array;
    }

    public function DeleteCustomerProduct()
    {
    }


    public function GetPackageList()
    {
        $BAS = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password);
        $resultB = $this->ServicePackages->GetPackageList($BAS);
        $array = simplexml_load_string($resultB->GetPackageListResult->ListInfo->any);
        if (count($array->NewDataSet->Package) == 1) {
            return array((array)$array->NewDataSet->Package);
        } elseif (count($array->NewDataSet->Package) > 1) {
            foreach ($array->NewDataSet->Package as $row) {
                $arr[] = (array)$row;
            }
            return $arr;
        } else {
            return false;
        }
    }
    /*SN Serial number of CLI/PIN string Yes
    CheckSelfCare True = Package should have Selfcare availability enabled boolean Yes False = Ignore Selfcare availability option
    CheckSalesRep True = Package should have Dealer availability enabled boolean Yes False = Ignore Dealer availability option
    PackageId

    array('SN' => , 'CheckSelfCare' => true, 'CheckSalesRep' => true, 'PackageId' => );
    */
    public function ChangePackageForSN($data)
    {
        $BAS = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password
            );
        $resultB = $this->ServicePackages->ChangePackageForSN(array_merge($BAS, $data));
        log_message('error', $this->ServicePackages->__getLastRequest() . $this->ServicePackages->__getLastResponse());
        return $resultB;
    }
    public function ChangePackageForContactType($data)
    {
        $BAS = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password
            );
        $resultB = $this->ServicePackages->ChangePackageForContactType(array_merge($BAS, $data));
        log_message('error', print_r(array_merge($BAS, $data), true));
        log_message('error', $this->ServicePackages->__getLastRequest() . $this->ServicePackages->__getLastResponse());

        return $resultB;
    }

    public function AddBundleAssign($sn, $bundleid, $ValidFrom, $ValidUntil, $recurring = false)
    {
        try {
            //$option = array('trace' => 1, 'features' => SOAP_USE_XSI_ARRAY_TYPE);
            $BAS = array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'BundleId' => getBundleId($bundleid),
                'ContactTypeId' => -1,
                'AvailableSubcustomer' => 0,
                'SharedUsage' => 0,
                'SN' => trim($sn),
                'OperatorId' => -1,
                'PackageId' => -1,
                'ProductId' => -1,
                'CallModeId' => -1,
                'TrafficId' => -1,
                'ValidFrom' => $ValidFrom

            );

            if (!$recurring) {
                $BAS['ValidUntil'] = $ValidUntil;
            }
            $resultB = $this->ServiceBundle->AddBundleAssign($BAS);
            if ($resultB->AddBundleAssignResult->Result == "0") {
                return (object) array(
                    'result' => 'success',
                    'id' => $resultB->AddBundleAssignResult->NewItemId,
                );
            } else {
                if ($this->log) {
                    mail("lainard@gmail.com", "Error On Bundleassign", "SOAP FAULT: Bundle Assignment \n" . $resultB->__getLastRequest() . $resultB->__getLastResponse() . "\n||\nRESPONSE\n");
                }
                return (object) array(
                    'result' => 'error',
                    'message' => $this->ServiceBundle->__getLastResponse(),
                );
            }
        } catch (SoapFault $fault) {
            /// mail("lainard@gmail.com", "eror", "SOAP FAULT: Bundle Assignment \n" . $resultB->__getLastRequest() . $resultB->__getLastResponse() . "\n||\nRESPONSE\n" . $fault);
        }
    }
    public function BlockOriginating($sn, $pack, $status)
    {
        foreach ($pack as $row) {
            if (strpos($row->CallModeDescription, 'Originating')) {
                $this->UpdatePackageOptionsForSN($sn, $row->PackageDefinitionId, $status);
            } elseif (strpos($row->CallModeDescription, 'International')) {
                $this->UpdatePackageOptionsForSN($sn, $row->PackageDefinitionId, $status);
            }
        }
    }

    public function UnBlockOriginating($sn, $pack, $status)
    {
        foreach ($pack as $key => $value) {
            $this->UpdatePackageOptionsForSN($sn, $key, 1);
        }
    }
    public function UpdateServices($sn, $pack, $status)
    {
        $res = array();
        if ($pack) {
            foreach ($pack as $row) {
                if ($row->Customizable == 1) {
                    $change = true;
                } else {
                    $change = false;
                }
                if ($change) {
                    $res[] = $this->UpdatePackageOptionsForSN($sn, $row->PackageDefinitionId, $status);
                }
                unset($change);
            }
            return (object) array(
                'result' => 'success',
                'message' => $res,
            );
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Not possible',
            );
        }
    }

    public function UpdateServicesDone($sn, $pack, $status, $service)
    {
        $res = array();
        if ($pack) {
            foreach ($pack as $row) {
                if ($row->Customizable == 1) {
                    if (trim($row->CallModeDescription) == "Data Originating") {
                        if ($service->GPRS == 1) {
                            $change = true;
                        } else {
                            $change = false;
                        }
                    } elseif (in_array($row->CallModeDescription, array("Data Roaming Originating", "Roaming", "Voice phone calls Roaming Originating"))) {
                        if ($service->Roaming == 1) {
                            $change = true;
                        } else {
                            $change = false;
                        }
                    } elseif (trim($row->CallModeDescription) == "International Numbers") {
                        if ($service->BarInternationalCalls == 1) {
                            $change = false;
                        } else {
                            $change = true;
                        }
                    } elseif (trim($row->CallModeDescription) == "Premium Numbers") {
                        if ($service->BarPremiumVoice == 1) {
                            $change = false;
                        } else {
                            $change = true;
                        }
                    } elseif (trim($row->CallModeDescription) == "Premium Gaming") {
                        if ($service->BarPremium == 1) {
                            $change = false;
                        } else {
                            $change = true;
                        }
                    } elseif (trim($row->CallModeDescription) == "4G") {
                        if ($service->LTE == 1) {
                            $change = true;
                        } else {
                            $change = false;
                        }
                    } else {
                        $change = true;
                    }
                } else {
                    $change = false;
                }
                if ($change) {
                    $res[] = $this->UpdatePackageOptionsForSN($sn, $row->PackageDefinitionId, $status);
                }
                unset($change);
            }
            return (object) array(
                'result' => 'success',
                'message' => $res,
            );
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Not possible',
            );
        }
    }
    public function GetBalance($msisdn)
    {
        try {
            $resultPACKupd = $this->ServiceCLI->GetBalance(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'Msisdn' => $msisdn,
            ));
            if ($resultPACKupd->GetBalanceResult->Result == 0) {
                return (object) array(
                    'result' => 'success',
                    'credit' => $resultPACKupd->CreditCents,
                );
            } else {
                return (object) array(
                    'result' => 'error',
                    'details' => $resultPACKupd,
                    'SN' => $msisdn,
                );
            }
        } catch (Exception $e) {
            return (object) array(
                'result' => 'error',
            );
        }
    }
    public function UpdatePackageOptionsForSN($SN, $PackageDefinitionId, $Available)
    {
        try {
            $resultPACKupd = $this->ServicePackages->UpdatePackageOptionsForSN(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $SN,
                'PackageOptions' => array(
                    'PackageDefinitionId' => $PackageDefinitionId,
                    'Available' => $Available,
                ),
            ));

            //log_message('error',print_r($resultPACKupd, true));
            if ($this->log) {
                mail('mail@simson.one', 'result', print_r(array('request' => $this->ServicePackages->__getLastRequest(),
                    'response' => $this->ServicePackages->__getLastResponse()), true));
            }

            if ($resultPACKupd->UpdatePackageOptionsForSNResult->Result == 0) {
                return (object) array(
                    'result' => 'success',
                    'data' => $PackageDefinitionId . ' ' . $Available,
                );
            } else {
                return (object) array(
                    'result' => 'error',
                    'details' => $resultPACKupd,
                    'SN' => $SN,
                );
            }
        } catch (Exception $e) {
            return (object) array(
                'result' => 'error',
            );
        }
    }
    public function PartialFullBar($m, $value)
    {
        //0 = No bar;
        //1 = Partial Bar
        //2 = Full Bar
        //0 = No bar;
        if (substr($m->msisdn, 0, 2) == "32") {
            $result = $this->ServiceCLI->UpdateParametersCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $m->msisdn_sn,
            'ParameterList' => array(
                'Parameters' => array(
                    'Parameter' => array(
                        'ParameterId' => 20541,
                        'ParameterValue' => $value,
                        'ParameterDeleted' => false,
                    ),
                ),
            ),
            ));
            log_message('error', 'PartialFullBar', print_r($result, true));
            if ($result->UpdateParameterResult->Result == "0") {
                return $result;
            } else {
                return "IN Update Parameters Error";
            }
        }
    }

    public function PartialFullBarMoxx($sn, $value)
    {
        //0 = No bar;
        //1 = Partial Bar
        //2 = Full Bar
        //0 = No bar;

        $result = $this->ServiceCLI->UpdateParametersCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'ParameterList' => array(
                'Parameters' => array(
                    'Parameter' => array(
                        'ParameterId' => 20541,
                        'ParameterValue' => $value,
                        'ParameterDeleted' => false,
                    ),
                ),
            ),
        ));

        if ($result->UpdateParameterResult->Result == "0") {
            return true;
        } else {
            return "IN Update Parameters Error";
        }
    }
    public function convert2array($result)
    {
        $xml = simplexml_load_string($result);
        // Grabs the posts
        $posts = $xml->children('SOAP-ENV', true)->Body->children('ns1', true)->Posts->Post;
        // Take the posts and generate some markup
        return $posts;
    }
    public function GetFullCDRListWithDetails($sn, $month = false)
    {
        $from = date('Y-m-01\T00:00:00+02:00');
        $Till = date('Y-m-d\T23:i:s');

        if ($month) {
            $a_date = $month."-01";


            $from = $month.'-01T00:00:00+02:00';
            $Till =  date("Y-m-t", strtotime($a_date))."T23:59:59";
        }
        $data = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'From' => $from,
            'Till' => $Till,
            'PageIndex' => 0,
            'PageSize' => 7000,
            'SpecialFilterOptions' => array(5,6),
            'SortBy' => 0,
            'SortOrder' => 0,
        );


        $ss = array();
        // $data['SpecialFilterOptions'] = array(5,6);
        $res = $this->ServiceCDR->GetFullCDRListWithDetails($data);
        if ($res->GetFullCDRListWithDetailsResult->Result == "0") {
            if ($res->GetFullCDRListWithDetailsResult->TotalItems == 0) {
                return array();
            }
            $array = simplexml_load_string($res->GetFullCDRListWithDetailsResult->ListInfo->any);
            //return $array->NewDataSet;

            /*   if(!empty($array->NewDataSet->CDRs->TrafficTypeId)){
                  $row = $array->NewDataSet->CDRs;
                  $s['TrafficType'] = (string)$row->TrafficTypeId;
                  $s['CallMode'] = getTypeCall((string)$row->TypeCallId);
                  $s['Datetime'] = (string)$row->DateTime;
                  $s['Origin'] = (string)$row->OriginalClip;
                  $s['Destination'] = (string)$row->FinalDestination;
                  $s['PinCode'] = (string)$row->PinCode;
                  $s['VLR'] = (string)$row->DidSuffix;
                   $s['Price'] = number_format((string)$row->CurNumUnitsUsed);
                    $s['Volume'] = (string)$row->DurationConnection;


                     $s['VolumeUnit'] = RatingUnitName($row->RatingUnitID);

                     if($row->BundleId > 0){
                      $s['ChargedOnBundle'] = true;
                     }else{
                      $s['ChargedOnBundle'] = false;
                     }

                       $s['ResultCode'] = (string)$row->Cause;
                        $s['CDRId'] = (string)$row->CDRId;
                        $ss[] =$s;



               }else{
   */


            foreach ($array->NewDataSet->CDRs as $row) {
                if ($row->TrafficTypeId != 2) {
                    $s['TrafficType'] = (string)$row->TrafficTypeId;
                    $s['CallMode'] = getTypeCall((string)$row->TypeCallId);

                    if (in_array($s['CallMode'], array("O"))) {
                        $s['Datetime'] = (string)$row->DateTime;
                        $s['Origin'] = (string)$row->OriginalClip;
                        $s['Destination'] = (string)$row->FinalDestination;
                        $s['PinCode'] = (string)$row->PinCode;
                        $s['VLR'] = (string)$row->DidSuffix;
                        $s['Price'] = number_format((string)$row->CurNumUnitsUsed);
                        $s['Volume'] = (string)$row->DurationConnection;


                        $s['VolumeUnit'] = RatingUnitName($row->RatingUnitID);

                        if ($row->BundleId > 0) {
                            $s['ChargedOnBundle'] = true;
                        } else {
                            $s['ChargedOnBundle'] = false;
                        }
                        $s['ResultCode'] = (string)$row->Cause;
                        $s['CDRId'] = (string)$row->CDRId;
                        $ss[] =$s;
                    }
                }
            }


            // }

            return $ss;
        } else {
            return array();
        }
    }
    /*
    public function GetFullCDRListWithDetails($sn, $month=false){

         $from = date('Y-m-01\T00:00:00+02:00');
          $Till = date('Y-m-d\T23:i:s');

        if($month){

              $a_date = $month."-01";


               $from = $month.'-01T00:00:00+02:00';
               $Till =  date("Y-m-t", strtotime($a_date))."T23:59:59";

        }

        $params = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'From' => $from,
            'Till' => $Till,
            'PageIndex' => 0,
            'PageSize' => 7000,
            'SpecialFilterOptions' => array(5,6),
            'SortBy' => 0,
            'SortOrder' => 0,
        );
        $result = $this->ServiceCDR->GetFullCDRListWithDetails($params);
        //return $result;
        $s = json_decode(json_encode(simplexml_load_string($result->GetFullCDRListWithDetailsResult->ListInfo->any)));

        return $s->NewDataSet->CDRs;



    }

    */
    public function get_cdr($mobile, $month = false)
    {
        $total = 0;
        $bytes = array();
        //echo $msisdn;
        $cdr = array();
        $cdrin = array();
        //print_r($this->data['mobile']);
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
        //$result = $this->artilium->GetCDRList($sn);
        $sms = array();
        $voice = array();
        $bytes = array();
        /*$req = array('action' => 'GetCDRList',
        'type' => $this->data['mobile']->PaymentType,
        'From' => date('Y-m-01\TH:i:s'),
        'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
        'SN' => $sn,
        'PageIndex' => 0,
        'PageSize' => 2500,
        'SortBy' => 0,
        'SortOrder' => 0,
        'companyid' => $this->data['setting']->companyid);
        $result = $this->Admin_model->artiliumPost($req);
         */
        $from = date('Y-m-01\T00:00:00+02:00');
        $Till = date('Y-m-d\T23:i:s');

        if ($month) {
            $a_date = $month."-01";


            $from = $month.'-01T00:00:00+02:00';
            $Till =  date("Y-m-t", strtotime($a_date))."T23:59:59";
        }

        $params = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $mobile->details->msisdn_sn,
            'From' => $from,
            'Till' => $Till,
            'PageIndex' => 0,
            'PageSize' => 7000,
            'SortBy' => 0,
            'SortOrder' => 0,
        );
        $result = $this->ServiceCDR->GetCDRList($params);
        //return $result;
        $s = json_decode(json_encode(simplexml_load_string($result->GetCDRListResult->ListInfo->any)));
        //return $s;
        //$e = $this->soap2array($this->ServiceCDR->__getLastResponse());
        //return $e->soapBody->GetCDRListResponse->GetCDRListResult->ListInfo->diffgrdiffgram->NewDataSet->CDRs;
        //return $e; GetCDRListResult":{"Result":0,"ListInfo":{"schema"
        //exit;
        if (empty($s)) {
            return array(
                'cdr_count' => 0,
                'cdr' => array(),
                'data' => 0,
                'voice' => 0,
                'sms' => 0,
            );
        }

        if ($result->GetCDRListResult->Result == "0") {
            $count = $result->GetCDRListResult->TotalItems;
            if ($count == 0) {
                return array(
                    'cdr_count' => 0,
                    'cdr' => array(),
                    'data' => 0,
                    'voice' => 0,
                    'sms' => 0,
                    'total' => 0,
                );
            }

            foreach ($s->NewDataSet->CDRs as $row) {
                if (!empty($row->Begintime)) {
                    if (empty($row->DestinationCountry)) {
                        $country = "";
                    } else {
                        $country = $row->DestinationCountry;
                    }
                    if ($row->TypeCallId == "4") {
                        $country = $row->RoamingCountry . "(R)";
                    }

                    if ($row->TypeCallId == "5") {
                        $country = $row->RoamingCountry . "(RT)";
                    }
                    if ($row->TypeCallId == "6") {
                        $country = $row->RoamingCountry . "(RF)";
                    }
                    if ($row->Cause != "1000") {
                        $total = $total + includevat('21', $row->CurNumUnitsUsed);
                        //  if ($row->TypeCallId != "5") {
                        if ($row->MaskDestination != "72436") {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                                if (showIncomingCdr($mobile->packageid)) {
                                    // mail("mail@simson.one","found",print_r($row, true));
                                    if ($row->TypeCallId == "5") {
                                        $cdr[] = array(
                                        'Begintime' => format_cdr_time($row->Begintime),
                                        'DestinationCountry' => $country,
                                        'MaskDestination' => $row->MaskDestination,
                                        'DurationConnection' => str_replace('internet.arta', 'internet', $row->Destination),
                                        'TrafficTypeId' => $row->TrafficTypeId,
                                        'TypeCallId' => $row->TypeCallId,
                                        'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed),
                                        'all' => (array) $row,
                                    );
                                    } else {
                                        $cdr[] = array(
                                            'Begintime' => format_cdr_time($row->Begintime),
                                            'DestinationCountry' => $country,
                                            'MaskDestination' => $row->MaskDestination,
                                            'DurationConnection' => str_replace('internet.arta', 'internet', $row->DurationConnection),
                                            'TrafficTypeId' => $row->TrafficTypeId,
                                            'TypeCallId' => $row->TypeCallId,
                                            'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed),
                                            'all' => (array) $row,
                                        );
                                    }
                                }
                            } else {
                                if ($row->TypeCallId == "5") {
                                    $cdr[] = array(
                                        'Begintime' => format_cdr_time($row->Begintime),
                                        'DestinationCountry' => $country,
                                        'MaskDestination' => str_replace('internet.arta', 'internet', $row->Destination),
                                        'DurationConnection' => $row->DurationConnection,
                                        'Megabit' => convertToReadableSize($row->DurationConnection),
                                        'TrafficTypeId' => $row->TrafficTypeId,
                                        'TypeCallId' => $row->TypeCallId,
                                        'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed),
                                        'all' => (array) $row,
                                    );
                                } else {
                                    $cdr[] = array(
                                        'Begintime' => format_cdr_time($row->Begintime),
                                        'DestinationCountry' => $country,
                                        'MaskDestination' => str_replace('internet.arta', 'internet', $row->MaskDestination),
                                        'DurationConnection' => $row->DurationConnection,
                                        'Megabit' => convertToReadableSize($row->DurationConnection),
                                        'TrafficTypeId' => $row->TrafficTypeId,
                                        'TypeCallId' => $row->TypeCallId,
                                        'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed),
                                        'all' => (array) $row,
                                    );
                                }
                            }
                            /*
                            $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $row->DestinationCountry, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed));
                         */
                        }
                        //  }
                        // if ($row->TypeCallId != 5) {
                        if (!in_array($row->TrafficTypeId, array(
                                2,
                                3,
                            ))) {
                            if (strlen($row->MaskDestination) > 9) {
                                if ($row->TrafficTypeId) {
                                    if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {
                                    } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {
                                    } else {
                                        if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
                                            $voice[] = $row->DurationConnection;
                                        } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
                                            $sms[] = $row->DurationConnection;
                                        }
                                    }
                                }
                            }
                        } elseif ($row->TrafficTypeId == 2) {
                            $bytes[] = $row->DurationConnection;
                        }
                        // }
                    }
                }
            }
        }
        //print_r($cdr);
        //exit;
        if (!$sms) {
            $smsi = '0';
        } else {
            $smsi = array_sum($sms);
        }

        return array(
            'cdr_count' => $count,
            'cdr' => $cdr,
            'cdrin' => $cdrin,
            'data' => array_sum($bytes),
            'voice' => array_sum($voice),
            'sms' => $smsi,
            'total' => $total,
        );
        //return array('cdr' => $cdr, 'data' => array_sum($bytes));
    }
    public function GetInstanceList($sn)
    {
        $res = $this->ServiceUsageMonitoring->GetInstanceList(array('Login' => $this->params->m_username, 'Password' => $this->params->m_password, 'SN' => $sn));
        if ($sn  =="31648924895") {
            mail('mail@simson.one', 'sum GetInstanceList Request', print_r($this->ServiceUsageMonitoring->__getLastRequest(), true));
            mail('mail@simson.one', 'sum GetInstanceList Response', print_r($this->ServiceUsageMonitoring->__getLastResponse(), true));
        }
        return $res;
    }
    public function GetInstanceLists($sn)
    {
        if (!empty($data['SumAssignmentId'])) {
            unset($data['SumAssignmentId']);
        }

        //return $data;
        $res = $this->ServiceUsageMonitoring->GetInstanceList(array('Login' => $this->params->m_username, 'Password' => $this->params->m_password, 'SN' => $sn));
        //return $res;
        if ($res->GetInstanceListResult->ResultCode == "0") {
            if (!empty($res->GetInstanceListResult->Data->SumPlanId)) {
                if ($res->GetInstanceListResult->Data->SumUsageData->Ignore) {
                    $ignore = 1;
                } else {
                    $ignore = "0";
                }
                $result[] = array('SumAssignmentID' => $res->GetInstanceListResult->Data->SumUsageData->SumAssignmentId, 'SumplanCode' => $res->GetInstanceListResult->Data->SumPlanCode, 'StartDate' => $res->GetInstanceListResult->Data->SumUsageData->Validity->BeginDate, 'EndDate' => $res->GetInstanceListResult->Data->SumUsageData->Validity->EndDate, 'Usage' => $res->GetInstanceListResult->Data->SumUsageData->Used, 'Ignored' => $ignore);
            } else {
                foreach ($res->GetInstanceListResult->Data as $plan) {
                    if ($plan->SumUsageData->Ignore) {
                        $ignore = 1;
                    } else {
                        $ignore = "0";
                    }

                    $result[] = array('SumAssignmentID' => $plan->SumUsageData->SumAssignmentId, 'SumplanCode' => $plan->SumPlanCode, 'StartDate' => $plan->SumUsageData->Validity->BeginDate, 'EndDate' => $plan->SumUsageData->Validity->EndDate, 'Usage' => $plan->SumUsageData->Used, 'Ignored' => $ignore);
                }
            }
        } else {
        }
        return $result;
    }
    public function UpdateBundleAssignV2($data)
    {
        $data['Login'] = $this->params->m_username;
        $data['Password'] = $this->params->m_password;
        $result = $this->ServiceBundle->UpdateBundleAssignV2($data);
        return $result;
    }
    public function ServiceLogging($method, $data)
    {
        $data['Login'] = $this->params->m_username;
        $data['Password'] = $this->params->m_password;
        try {
            $Result = $this->ServiceLogging->$method($data);

            // log_message("error", "REQUEST:".$this->ServiceLogging->__getLastRequest());
            // log_message("error",  "RESPONSE:".$this->ServiceLogging->__getLastResponse());

            return $Result->GetLoggingBySnResult->ListInfo;
            if ($Result->ServiceLoggingResult->Result == 0) {
                $res = json_decode(json_encode(simplexml_load_string($result->ServiceLoggingResult->ListInfo)));
                return (object) array(
                    'result' => 'success',
                    'data' => $res,
                );
            } else {
                return (object) array(
                    'result' => 'error',
                );
            }
            $this->final = $res;
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function getOutofBundleusagePostpaid($numbers)
    {
        $bal[] = 0;

        foreach ($numbers as $number) {
            $res =  $this->GetInstanceList($number);
            //print_r($res);

            //mail('mail@simson.one',"used",print_r($res, true));
            if ($res->GetInstanceListResult->ResultCode == "0") {
                if (!is_array($res->GetInstanceListResult->Data)) {
                    $bal[] = $res->GetInstanceListResult->Data->SumUsageData->Used;
                } else {
                    //echo "it is a row\n";
                    foreach ($res->GetInstanceListResult->Data as $row) {
                        //print_r($row);
                        if (in_array($this->companyid, array(53,56))) {
                            if ($row->SumPlanCode=="SUM_Credit") {
                                $bal[] = $row->SumUsageData->Used;
                            }
                        } elseif (in_array($this->companyid, array(54))) {
                            if ($row->SumPlanCode=="SUM_Credit_Hard") {
                                $bal[] = $row->SumUsageData->Used;
                            }
                        } else {
                            $bal[] = $row->SumUsageData->Used;
                        }
                        # code...
                    }
                }
                # code...
            }

            if (count($bal) > 1) {
                return includevat(21, array_sum($bal));
            } elseif (count($bal) == 1) {
                return includevat(21, $bal);
            } else {
                return "0.00";
            }
        }
    }
    public function GetSUMAssignmentList1($sn)
    {
        try {
            $Result = $this->ServiceUsageMonitoring->GetSUMAssignmentList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $sn,
            ));

            return $Result;
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }

    public function UpdateAssignmentValidity($AssignmentId, $EndDate)
    {
        $data['Login'] = $this->params->m_username;
        $data['Password'] = $this->params->m_password;
        $data['AssignmentId'] = $AssignmentId;
        $data['EndDate'] = $EndDate;
        try {
            $Result = $this->ServiceLogging->UpdateAssignmentValidity($data);
            return $Result;
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
    }
    public function SetPlanState($data)
    {
        $data['Login'] = $this->params->m_username;
        $data['Password'] = $this->params->m_password;
        $state = $this->ServiceUsageMonitoring->SetPlanState($data);

        return $state;
    }
    public function GetCompanySumPLan()
    {
        $Plan = $this->ServiceUsageMonitoring->GetPlanList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password));

        return $Plan;
    }
    public function GetSUMPlanActionSets($planid)
    {
        $Result = $this->ServiceUsageMonitoring->GetSUMPlanActionSets(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SumPlanId' => $planid,
            ));

        return $Result;
    }
    public function GetSUMAssignmentList($sn)
    {
        try {
            $Plan = $this->ServiceUsageMonitoring->GetPlanList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password));

            //$this->ServiceUsageMonitoring = new soapclient($this->service . '/SUM/ServiceUsageMonitoring.asmx?WSDL', $this->option);
            $Result = $this->ServiceUsageMonitoring->GetSUMAssignmentList(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'SN' => $sn,
            ));

            if ($sn  =="31648924895") {
                mail('mail@simson.one', 'sum GetSUMAssignmentList Request', print_r($this->ServiceUsageMonitoring->__getLastRequest(), true));
                mail('mail@simson.one', 'sum GetSUMAssignmentList Response', print_r($this->ServiceUsageMonitoring->__getLastResponse(), true));
            }
            $Ignore  = 0;
            $instances = $this->GetInstanceLists($sn);
            //mail('mail@simson.one','instance',print_r($instances, true));
            /*  $Result = $this->ServiceUsageMonitoring->GetSUMAssignmentList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn
            ));

             */
            // print_r($Plan);
            $res = array();
            // mail('mail@simson.one')
            foreach ($Plan->GetPlanListResult->Data as $pl) {
                if (!empty($Result->GetSumAssignmentListResult->Data)) {
                    if (is_array($Result->GetSumAssignmentListResult->Data)) {
                        //mail('mail@simson.one','sum',print_r($Result->GetSumAssignmentListResult->Data, true));
                        foreach ($Result->GetSumAssignmentListResult->Data as $row) {
                            if ($row->SumPlanId == $pl->SumPlanId) {
                                if (empty($row->SumAssignmentData->Validity->EndDate)) {
                                    foreach ($instances as $instance) {
                                        if ($instance['SumAssignmentID'] == $row->SumAssignmentData->SumAssignmentId) {
                                            $Ignored = $instance['Ignored'];
                                        }
                                    }

                                    if ($pl->IsDisabled) {
                                        $disabled = 1;
                                    } else {
                                        $disabled = 0;
                                    }

                                    $res[] = array('PlanId' => $pl->SumPlanId, 'SumAssignmentId' => $row->SumAssignmentData->SumAssignmentId, 'SumSetId' => $row->SumAssignmentData->SumActionSetId, 'Name' => $pl->Code, 'ValidFrom' => $row->SumAssignmentData->Validity->BeginDate, 'ValidUntil' => $row->SumAssignmentData->Validity->EndDate, 'ActionSet' => $this->ServiceUsageMonitoring->GetSUMPlanActionSets(array(
                                    'Login' => $this->params->m_username,
                                    'Password' => $this->params->m_password,
                                    'SumPlanId' => $pl->SumPlanId,
                                    )),'IsDisabled' => $disabled,
                                    'Ignored' => $Ignored);
                                } else {
                                    $dt = explode('T', $row->SumAssignmentData->Validity->EndDate);
                                    foreach ($instances as $instance) {
                                        if ($instance['SumAssignmentID'] == $row->SumAssignmentData->SumAssignmentId) {
                                            $Ignored = $instance['Ignored'];
                                        }
                                    }
                                    if ($dt[0]  > date('Y-m-d')) {
                                        if ($pl->IsDisabled) {
                                            $disabled = 1;
                                        } else {
                                            $disabled = 0;
                                        }
                                        $res[] = array('PlanId' => $pl->SumPlanId, 'SumAssignmentId' => $row->SumAssignmentData->SumAssignmentId, 'SumSetId' => $row->SumAssignmentData->SumActionSetId, 'Name' => $pl->Code, 'ValidFrom' => $row->SumAssignmentData->Validity->BeginDate, 'ValidUntil' => $row->SumAssignmentData->Validity->EndDate, 'ActionSet' => $this->ServiceUsageMonitoring->GetSUMPlanActionSets(array(
                                        'Login' => $this->params->m_username,
                                        'Password' => $this->params->m_password,
                                        'SumPlanId' => $pl->SumPlanId,
                                        )),'IsDisabled' => $disabled,
                                        'Ignored' => $Ignored);
                                    }
                                    unset($dt);
                                }
                            }
                        }
                    }
                }
            }
            //$res['instance'] =$instance;
            return $res;
        } catch (Exception $e) {
            $this->final->result = 'error';
            $this->final->message = $e->getMessage();
        }
        return $this->final;
    }
    public function CreateAssignment($data)
    {
        $Result = $this->ServiceUsageMonitoring->UpdateActionSet(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $data['SN'],
            'AssignmentId' => $data['AssignmentId'],
            'ActionSetId' => $data['ActionSetId'],
        ));

        return $Result;
    }

    public function createSUMassignment($SN)
    {
        //print_r($params);exit;

        $DateRangeData = new stdClass();
        $SumPlanId = 2;
        $SumActionSetId = 2;
        $DateRangeData->BeginDate = date('Y-m-d');
        $DateRangeData->EndDate = "2099-01-01";

        try {
            $resupd = $this->ServiceUsageMonitoring->CreateAssignment(array(
                'Login' => $this->params->m_username,
                'Password' => $this->params->m_password,
                'Sn' => $SN,
                'SumPlanId' => $SumPlanId,
                'SumActionSetId' => $SumActionSetId,
                'ApplyToSubContacts' => false,
                'Validity' => $DateRangeData));

            if ($resupd->CreateAssignmentResult->ResultCode == "0") {
                $ccp = 0;
            } else {
                $errorstr = "FAILED SUM assign : " . print_r($resupd, true);
                $ccp = -1;

                //  mail('mail@simson.one', 'Sum add Failed SN:'.$SN, print_r($resupd, true));
            }
            if ($ccp == "0") {
                $successful = true;
            } else {
                $successful = false;
            }
            return $resupd;
            //$successful=true;

            if ($successful) {
                $result = "success";
            } else {
                $result = $ccp;
            }
        } catch (SoapFault $fault) {
            return "error"-"SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})";
        }

        return $result;
    }

    public function mvno_PartialFullBar($INparams, $SN, $BlockType, $vars)
    {
        /*
        [platform] => POSTPAID
        [reselleruser] => STUnitedPostpaid
        [resellerpass] => $TUn1tedP0stpa1D
         */
        $user = $INparams['reselleruser'];
        $pass = $INparams['resellerpass'];
        switch ($BlockType) {
            case 'FullBar':
                $ParameterValue = 2;
                break;
            case 'PartialBar':
                $ParameterValue = 1;
                break;
            default:
                $ParameterValue = 0;
                break;
        }
        $result = $this->ServicePackages->UpdateParametersCLI(array(
            'Login' => $user,
            'Password' => $pass,
            'SN' => $SN,
            'ParameterList' => array(
                'Parameters' => array(
                    'Parameter' => array(
                        'ParameterId' => 20541,
                        'ParameterValue' => $ParameterValue,
                        'ParameterDeleted' => false,
                    ),
                ),
            ),
        ));
        if ($result->UpdateParameterResult->Result == "0") {
            //Successful Update
            return true;
        } else {
            return "IN Update Parameters Error";
        }
    }

    public function UpdateBundleUsage($data)
    {
        $data["Login"] = $this->params->m_username;
        $data["Password"] = $this->params->m_password;
        $result = $this->ServiceBundle->UpdateBundleUsage($data);

        return $result;
    }


    public function super_api($data)
    {
    }
}
