<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{

    public $foot;
    public $lang;
    public $font;
    public $footer_image;
    function __construct()
    {
        parent::__construct();
        $CI = &get_instance();

        $CI->load->helper('language');
        $CI->load->library('session');

        $this->setting = globo();
        $this->creditnote = false;
        $this->extrabox = false;
        $this->footer_image = false;
    }
    public function PrintChapter($num, $title, $file, $mode = false)
    {
        // add a new page
        $this->AddPage();
        // disable existing columns
        $this->resetColumns();
        // print chapter title
        $this->ChapterTitle($num, $title);
        // set columns
        $this->setEqualColumns(3, 57);
        // print chapter body
        $this->ChapterBody($file, $mode);
    }

    /**
     * Set chapter title
     * @param $num (int) chapter number
     * @param $title (string) chapter title
     * @public
     */
    public function ChapterTitle($num, $title)
    {
        $this->SetFont('helvetica', '', 14);
        $this->SetFillColor(200, 220, 255);
        $this->Cell(180, 6, 'Chapter '.$num.' : '.$title, 0, 1, '', 1);
        $this->Ln(4);
    }
    public function Header()
    {
        if ($this->font) {
            $this->SetFont($this->font, '', 7);
        } else {
            $this->SetFont('helvetica', '', 7);
        }
       
        $this->setY(1);
        $this->Cell(0, 10, 'Blz. ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
    }
    public function setFoot($html)
    {
        
        $s =    explode(":", $html);
        if ($s[0] == "image") {
            $this->foot = $s[1];
            $this->footer_image = true;
        } else {
            $this->foot = $html;
        }
    }
    public function setFooterfont($font)
    {
        $this->font = $font;
    }
    public function Footer()
    {
        $CI = &get_instance();
        //$CI->config->set_item('language', $this->lang);
        //$CI->lang->load('admin');
        if ($this->footer_image) {
            $this->Image(FCPATH . 'assets/img/'.$this->foot, 0, 248, 210);
        } else {
            $this->setY(-20);
            $this->writeHTML("<hr>", true, false, false, false, '');
            $this->SetFont($this->font, '', 7);
           
            $this->writeHTML($this->foot, true, false, false, false, '');
        }
    }
}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */
