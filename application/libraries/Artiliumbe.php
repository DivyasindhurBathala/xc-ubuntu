<?php
/**
 * Artilium Library for Soap Interfacses
 */
class Artiliumbe
{
   private $CI;
   private $option;
   private $url;
   private $final;
   private $service;
   private $ServicePackages;
   private $ServiceMobile;
   private $ServiceCLI;
   private $ServicePorting;
   private $ServiceBundle;
   private $ServiceCDR;
   private $ServiceNumberPort;
   private $ServiceGeneral;
   private $ServiceReload;
   private $ServiceLogging;
   private $companyid;
   // private $ServiceUsageMonitoring;
   public function __construct($vars)
   {
      libxml_disable_entity_loader(false);
      $this->CI =& get_instance();
      if (empty($vars['companyid'])) {
         $this->companyid = get_companyidby_url(base_url());
      } else {
         $this->companyid = $vars['companyid'];
      }
      $this->CI->lang->load('client');
      $this->db                   = $this->CI->load->database('default', true);
      
      $params                     = $this->db->query('select * from a_api_data where companyid=?', array(
         $this->companyid
      ));
      $this->params               = $params->row();
      $this->service              = $this->params->arta_url . 'API' . $this->params->arta_version . '/DefaultAPI';
      $this->service_bundle       = $this->params->arta_url . 'API' . $this->params->arta_version . '/Bundle';
      $this->option               = array(
         'trace' => 1,
         'features' => SOAP_USE_XSI_ARRAY_TYPE,
         'cache_wsdl' => WSDL_CACHE_NONE,
         'encoding' => 'UTF-8'
      );
      //if (!empty($this->params->arta_proxy_url)) {
      $this->option['proxy_host'] = $this->params->arta_proxy_url;
      $this->option['proxy_port'] = $this->params->arta_proxy_port;
      //  }
      $this->ServicePackages      = new soapclient($this->service . '/ServicePackages.asmx?WSDL', $this->option);
      $this->ServiceMobile        = new soapclient($this->service . '/ServiceMobile.asmx?WSDL', $this->option);
      $this->ServiceCLI           = new soapclient($this->service . '/ServiceCLI.asmx?WSDL', $this->option);
      $this->ServicePorting = new soapclient($this->service . '/ServiceResellerMobileNumberPort.asmx?WSDL', $this->option);
      $this->ServiceUsageMonitoring = new soapclient($this->params->arta_url . 'API' . $this->params->arta_version . '/SUM/ServiceUsageMonitoring.asmx?WSDL', $this->option);
      $this->ServiceBundle          = new soapclient($this->service_bundle . '/ServiceBundle.asmx?WSDL', $this->option);
      $this->ServiceCDR             = new soapclient($this->service . '/ServiceCDR.asmx?WSDL', $this->option);
      $this->ServiceNumberPort      = new soapclient($this->service . '/ServiceNumberPort.asmx?WSDL', $this->option);
      $this->ServiceGeneral         = new soapclient($this->service . '/ServiceGeneral.asmx?WSDL', $this->option);
      $this->ServiceReload          = new soapclient($this->service . '/ServiceReload.asmx?WSDL', $this->option);
      $this->ServiceLogging         = new soapclient($this->service . '/ServiceLogging.asmx?WSDL', $this->option);
      //$this->ServiceUsageMonitoring = new soapclient($this->service . '/ServiceUsageMonitoring.asmx?WSDL', $this->option);
      //$this->ServiceResellerMobileNumberPort
      $this->final                  = (object) array(
         'result' => '',
         'data' => '',
         'message' => '',
         $this->params
      );
   }
   public function get_sim($simcardnumber)
   {
      $this->db = $this->CI->load->database('magebo', true);
      $q        = $this->db->query("select A.iAddressNbr, A.cName, A.cStreet,A.iCompanyNbr,Y.cTypeDescription as PaymentType,
CASE when A.iCountryIndex = 22
then (C.cCountryCode + '-' + Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix)
else (C.cCountryCode + '-' +
CASE when Cast(A.iZipCode AS varchar(10)) is null
then ''+ A.cZipSuffix
else Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix end  ) end as ZipCode,
A.cCity, P.cInvoiceReference, S.cSIMCardNbr as SIMCardNbr, S.iMSISDN as MSISDN, S.*, X.cTypeDescription as Type,Z.cTypeDescription as SimCardType, B.cInternalLanguage as Language
from tblC_SIMCard S
left join tblC_Pincode P ON P.iPincode = S.iPincode
left join tblLocation L ON L.iLocationIndex = P.iLocationIndex
left join tblAddress A ON A.iAddressNbr = L.iAddressNbr
left join tblType Y ON Y.iTypeNbr=S.iPaymentType
left join tblType X ON X.iTypeNbr=S.iMSISDNType
left join tblType Z ON Z.iTypeNbr=S.iSimCardType
left join tblLanguages B on B.iBaseVoiceMailLanguage=S.iVoiceMailLanguage
, tblCountry C
where S.cSIMCardNbr = ?
AND S.bActive = 1
AND (C.iCountryIndex = A.iCountryIndex)", array(
         trim($simcardnumber)
      ));
      return $q->row();
   }
   public function GetSn($simcardnr)
   {

   
      try {
         $res['SN'] = "";
         $result    = $this->ServiceMobile->GetSIM(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SIMNr' => $simcardnr
         ));
       
         if ($result->GetSIMResult->Result == "0") {
            $this->final->result = 'success';
            $xml                 = simplexml_load_string($result->GetSIMResult->ItemInfo->any);
            if (count($xml->NewDataSet->MSISDN) == "2") {
               foreach ($xml->NewDataSet->MSISDN as $rw) {
                  if ($rw->Status == "6") {
                     $sn = $rw;
                  }
               }
            } else if (count($xml->NewDataSet->MSISDN) == "1") {
               $sn = $xml->NewDataSet->MSISDN;
            }
            $sn = (array) $sn;
            if ($xml->NewDataSet->SIM) {
               $q          = (array) $xml->NewDataSet->SIM;
               $sn['PUK1'] = $q['PUK1'];
               $sn['PUK2'] = $q['PUK2'];
            }
            //$sn['SIM'] = $xml->NewDataSet;
            //$this->final->SN = $sn
            $this->final->data = (object) $sn;
         } else {
            $this->final->result  = 'error';
            $this->final->message = $result;
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
        $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetListPackageOptionsForSn($SN)
   {
      if (!empty($this->CI->session->language)) {
         $this->CI->config->set_item('language', $this->CI->session->language);
      } else {
         $this->CI->config->set_item('language', 'dutch');
      }
      $show = array(
         'International Numbers',
         '4G',
         'Data',
         'Premium Numbers',
         'Roaming',
         'Premium Gaming'
      );
      try {
         $result = $this->ServicePackages->GetListPackageOptionsForSN(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN,
            'ActiveAndNonActive' => 1
         ));
         if ($result->GetListPackageOptionsForSNResult->Result == "0") {
            $array = simplexml_load_string($result->GetListPackageOptionsForSNResult->ListInfo->any);
            if (count($array->NewDataSet->PackageOptions) > 1 && count($array->NewDataSet->PackageOptions) != 0) {
               foreach ($array->NewDataSet->PackageOptions as $r) {
                  $b = (array) $r;
                  if (in_array($r->CallModeDescription, $show)) {
                     $b['CallModeDescription'] = lang($r->CallModeDescription);
                     $res[]                    = (object) $b;
                  }
                  unset($b);
               }
            } else {
               $res = $array->NewDataSet->PackageOptions;
            }
            return $res;
         } else {
            $this->final->result  = 'error';
            $this->final->message = 'No result';
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetListPackageOptionsForSnAdvance($SN)
   {
      if (!empty($this->CI->session->language)) {
         $this->CI->config->set_item('language', $this->CI->session->language);
      } else {
         $this->CI->config->set_item('language', 'dutch');
      }
      $show = array(
         'International Numbers',
         '4G',
         'Data',
         'Premium Numbers',
         'Roaming',
         'Premium Gaming'
      );
      try {
         $result = $this->ServicePackages->GetListPackageOptionsForSN(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN,
            'ActiveAndNonActive' => 1
         ));
         if ($result->GetListPackageOptionsForSNResult->Result == "0") {
            $array = simplexml_load_string($result->GetListPackageOptionsForSNResult->ListInfo->any);
            if (count($array->NewDataSet->PackageOptions) > 1 && count($array->NewDataSet->PackageOptions) != 0) {
               foreach ($array->NewDataSet->PackageOptions as $r) {
                  $b = (array) $r;
                  if (in_array($r->CallModeDescription, $show)) {
                     $b['CallModeDescription'] = lang($r->CallModeDescription);
                     $res[]                    = (object) $b;
                  } else {
                     if ($r->Customizable == 1) {
                        $b['CallModeDescription'] = $b['CallModeDescription'];
                        if (!empty($r->TrafficDescription)) {
                           $b['CallModeDescription'] = $b['TrafficDescription'] . " " . $b['CallModeDescription'];
                        }
                        $res[] = (object) $b;
                     }
                  }
                  unset($b);
               }
            } else {
               $res = $array->NewDataSet->PackageOptions;
            }
            return $res;
         } else {
            $this->final->result  = 'error';
            $this->final->message = 'No result';
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetCDRList($SN)
   {
      include_once FCPATH . 'third_party/xmlutil.php';
      try {
         $result = $this->ServiceCDR->GetCDRList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN,
            'From' => date('Y-m-01\TH:i:s'),
            'Till' => date('Y-m-d\T23:i:s'),
            'SN' => $SN,
            'PageIndex' => 0,
            'PageSize' => 2500,
            'SortBy' => 0,
            'SortOrder' => 0
         ));
         return $result;
         if ($result->GetCDRListResult->Result == "0") {
            $array               = new SimpleXMLElement($result->GetCDRListResult->ListInfo->any);
            $this->final->result = 'success';
            $this->final->data   = $array;
         } else {
            $this->final->result  = 'error';
            $this->final->message = 'No result';
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetParametersCLI($SN)
   {
      try {
         $result = $this->ServiceCLI->GetParametersCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN
         ));
         return $result;
      }
      catch (Exception $e) {
         $this->final->result  = 'error1';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function SwapSim($data)
   {
      $sim = $this->ServiceMobile->GetMSISDNList(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SN' => $data['SN']
      ));

      
      if ($sim->GetMSISDNListResult->TotalItems == 1) {
         $object   = new SimpleXMLElement($sim->GetMSISDNListResult->ListInfo->any);
         $MSISDNId = (int) $object->NewDataSet->MSISDN->MSISDNId;
      } else {
         $object = new SimpleXMLElement($sim->GetMSISDNListResult->ListInfo->any);

        
         foreach ($object->NewDataSet->MSISDN as $row) {
            if ($row->Status == '6') {
               $MSISDNId = (int) $row->MSISDNId;
            }
         }
      }

     
      $result = $this->ServiceCLI->SwapSim(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'NewSimNr' => $data['NewSimNr'],
         'MsisdnId' => $MSISDNId
      ));
      mail('mail@simson.one','Swap Result', print_r($data, true). print_r($result, true));
      if ($result->SwapSimResult === 0) {
         return (object) array(
            'result' => 'success'
         );
      } else {
         return (object) array(
            'result' => 'error',
            'message' => getErrorSwap($result->SwapSimResult)
        
         );
      }
   }
   public function AcceptPortOut($PortingId)
   {
      $result = $this->ServicePorting->AcceptPortOut(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'PortingId' => $PortingId
      ));
      if ($result->ServicePortingResult->Result == "0") {
         echo (object) array(
            'result' => 'success'
         );
      } else {
         if ($result->ServicePortingResult->Result == "-2") {
            $result = "No Pins are found with this PortingId";
         } elseif ($result->ServicePortingResult->Result == "-3") {
            $result = "PortingId exists, but the status of request is not valid for accept";
         } elseif ($result->ServicePortingResult->Result == "-4") {
            $result = "Pin can not be null";
         } elseif ($result->ServicePortingResult->Result == "-5") {
            $result = "No PortOut in progress for this SN";
         } elseif ($result->ServicePortingResult->Result == "-6") {
            $result = "Parameter 20519 (Port Out lead CLI/PIN) not found";
         } elseif ($result->ServicePortingResult->Result == "-90") {
            $result = "Authentication failed";
         } elseif ($result->ServicePortingResult->Result == "-91") {
            $result = "Field overflow (too many characters, date out of range";
         } elseif ($result->ServicePortingResult->Result == "-100") {
            $result = "Unspecified error";
         }
         echo (object) array(
            'result' => 'error',
            'message' => $result
         );
      }
   }
   public function RejectPortOut($SN)
   {
      $result = $this->ServicePorting->RejectPortOut(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'PortingId' => $data['PortingId'],
         'RejectPorting' => array(
            'RejectId' => '',
            'SN' => ''
         )
      ));
      if ($result->ServicePortingResult->Result == "0") {
         echo (object) array(
            'result' => 'success'
         );
      } else {
         if ($result->ServicePortingResult->Result == "-2") {
            $result = "No Pins are found with this PortingId";
         } elseif ($result->ServicePortingResult->Result == "-3") {
            $result = "PortingId exists, but the status of request is not valid for accept";
         } elseif ($result->ServicePortingResult->Result == "-4") {
            $result = "Pin can not be null";
         } elseif ($result->ServicePortingResult->Result == "-5") {
            $result = "No PortOut in progress for this SN";
         } elseif ($result->ServicePortingResult->Result == "-6") {
            $result = "Parameter 20519 (Port Out lead CLI/PIN) not found";
         } elseif ($result->ServicePortingResult->Result == "-90") {
            $result = "Authentication failed";
         } elseif ($result->ServicePortingResult->Result == "-91") {
            $result = "Field overflow (too many characters, date out of range";
         } elseif ($result->ServicePortingResult->Result == "-100") {
            $result = "Unspecified error";
         }
         echo (object) array(
            'result' => 'error',
            'message' => $result
         );
      }
   }
   public function GetSpecificCliInfo($SN)
   {
      try {
         $result = $this->ServiceCLI->GetSpecificCliInfo(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN
         ));
         if ($result->GetSpecificCLIInfoResult->TotalItems == 1) {
            $r      = simplexml_load_string($result->GetSpecificCLIInfoResult->ListInfo->any);
            $object = (array) $r->NewDataSet->SpecificCLIInfo;
            return (object) $object;
         } else {
            $r           = simplexml_load_string($result->GetSpecificCLIInfoResult->ListInfo->any);
            $this->final = (object) $r->NewDataSet->SpecificCLIInfo[0];
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetBundleList()
   {
      try {
         $result      = $this->ServiceBundle->GetBundleList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password
         ));
         $this->final = simplexml_load_string($result->GetBundleListResult->ListInfo->any);
      }
      catch (Exception $e) {
         $this->final->result  = 'error1';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetBundleAssignList($SN)
   {
      try {
         $result = $this->ServiceBundle->GetBundleAssignListV2(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN
         ));
         $res    = json_decode(json_encode(simplexml_load_string($result->GetBundleAssignListV2Result->ListInfo->any)));
         return $res;
      
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = array(
            $res,
            $this->ServiceBundle->__getLastRequest()
         );
      }
      return $this->final;
   }
   public function GetBundleAssignList1($SN)
   {
      try {
         $result = $this->ServiceBundle->GetBundleAssignListV2(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN
         ));
         $res    = json_decode(json_encode(simplexml_load_string($result->GetBundleAssignListV2Result->ListInfo->any)));
         if ($result->GetBundleAssignListV2Result->TotalItems == "0") {
            $r = array();
         } elseif ($result->GetBundleAssignListV2Result->TotalItems == "1") {
            $b             = $res->NewDataSet->BundleAssign;
            $b->Percentage = "0";
            $h             = explode('T', $b->ValidFrom);
            $b->szBundle   = getMobilebundleName($b->BundleId, $this->companyid);
            $b->ValidFrom  = date('Y-m-01');
            $l             = $this->GetBundleUsageList($SN, $b->BundleAssignId, $b->BundleId);
            // echo "here1";
            $s             = (array) $l;
            if (!empty($s)) {
               if (!is_array($l->NewDataSet->BundleUsage)) {
                  //$b->detail = $l;
                  //$b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                  //$b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                  $p             = $l->NewDataSet->BundleUsage->UsedValue / $l->NewDataSet->BundleUsage->AssignedValue;
                  $b->Percentage = round($p * 100, 2);
                  if (is_nan($b->Percentage)) {
                     $b->Percentage = '0';
                  }
                  $b->Defenition = $this->GetBundleDefinition($l->NewDataSet->BundleUsage->BundleDefinitionId);
                  if ($b->Defenition == 3) {
                     $b->AssignedValue = human_filesize(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                     $b->UsedValue     = human_filesize(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                  } elseif ($b->Defenition == 1) {
                     if ($l->NewDataSet->BundleUsage->AssignedValue == 120000) {
                        $b->AssignedValue = 'onbeperkt';
                     } else {
                        $b->AssignedValue = second2hms(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                     }
                     $b->UsedValue = second2hms(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                  } else {
                     $b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                     $b->UsedValue     = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                  }
                  $b->icon = ratingtype($b->Defenition);
               } else {
                  $usedvalues = $this->getUsedValue($l->NewDataSet->BundleUsage);
                  if ($usedvalues->used > 0) {
                     $p             = $usedvalues->used / $usedvalues->assigned;
                     $b->Percentage = round($p * 100, 2);
                  }
                  if (is_nan($b->Percentage)) {
                     $b->Percentage = '0';
                  }
                  $b->Defenition = $this->GetBundleDefinition($this->getDefenitionid($l->NewDataSet->BundleUsage));
                  if ($b->Defenition == 3) {
                     $b->AssignedValue = human_filesize(round($usedvalues->assigned, 2));
                     $b->UsedValue     = human_filesize(round($usedvalues->used, 2));
                  } elseif ($b->Defenition == 1) {
                     if ($usedvalues->assigned == 120000) {
                        $b->AssignedValue = 'onbeperkt';
                     } else {
                        $b->AssignedValue = second2hms(round($usedvalues->assigned, 2));
                     }
                     $b->UsedValue = second2hms(round($usedvalues->used, 2));
                  } else {
                     $b->AssignedValue = round($usedvalues->assigned, 2);
                     $b->UsedValue     = round($usedvalues->used, 2);
                  }
                  $b->icon = ratingtype($b->Defenition);
               }
               $b->ValidFrom = $l->NewDataSet->BundleUsage->ValidFrom;
               if (!empty($b->ValidUntil)) {
                  $tos           = explode('T', $b->ValidUntil);
                  $b->ValidUntil = $tos[0];
                  unset($tos);
                  if ($b->ValidUntil > date('Y-m-d')) {
                     $r[] = $b;
                  }
               } else {
                  $r[] = $b;
               }
            } else {
               if (!empty($b->ValidUntil)) {
                  $tos           = explode('T', $b->ValidUntil);
                  $b->ValidUntil = $tos[0];
                  unset($tos);
               }
               if ($b->ValidUntil > date('Y-m-d')) {
                  $r[] = $b;
               }
            }
         } else {
            //$f =else $res->NewDataSet->BundleAssign;
            $r = array();
            if ($res) {
               if (is_array($res->NewDataSet->BundleAssign)) {
                  foreach ($res->NewDataSet->BundleAssign as $b) {
                     $b->Percentage = "0";
                     $h             = explode('T', $b->ValidFrom);
                     $b->szBundle   = getMobilebundleName($b->BundleId, $this->companyid);
                     $b->ValidFrom  = date('Y-m-01');
                     $l             = $this->GetBundleUsageList($SN, $b->BundleAssignId, $b->BundleId);
                     $s             = (array) $l;
                     if (!empty($s)) {
                        if (!is_array($l->NewDataSet->BundleUsage)) {
                           //$b->detail = $l;
                           //$b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                           //$b->UsedValue = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                           $p             = $l->NewDataSet->BundleUsage->UsedValue / $l->NewDataSet->BundleUsage->AssignedValue;
                           $b->Percentage = round($p * 100, 2);
                           if (is_nan($b->Percentage)) {
                              $b->Percentage = '0';
                           }
                           $b->Defenition = $this->GetBundleDefinition($l->NewDataSet->BundleUsage->BundleDefinitionId);
                           if ($b->Defenition == 3) {
                              $b->AssignedValue = human_filesize(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                              $b->UsedValue     = human_filesize(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                           } elseif ($b->Defenition == 1) {
                              if ($l->NewDataSet->BundleUsage->AssignedValue == 120000) {
                                 $b->AssignedValue = 'onbeperkt';
                              } else {
                                 $b->AssignedValue = second2hms(round($l->NewDataSet->BundleUsage->AssignedValue, 2));
                              }
                              $b->UsedValue = second2hms(round($l->NewDataSet->BundleUsage->UsedValue, 2));
                           } else {
                              $b->AssignedValue = round($l->NewDataSet->BundleUsage->AssignedValue, 2);
                              $b->UsedValue     = round($l->NewDataSet->BundleUsage->UsedValue, 2);
                           }
                           $b->icon = ratingtype($b->Defenition);
                        } else {
                           $usedvalues = $this->getUsedValue($l->NewDataSet->BundleUsage);
                           if ($usedvalues->used > 0) {
                              $p             = $usedvalues->used / $usedvalues->assigned;
                              $b->Percentage = round($p * 100, 2);
                           }
                           if (is_nan($b->Percentage)) {
                              $b->Percentage = '0';
                           }
                           $b->Defenition = $this->GetBundleDefinition($this->getDefenitionid($l->NewDataSet->BundleUsage));
                           if ($b->Defenition == 3) {
                              $b->AssignedValue = human_filesize(round($usedvalues->assigned, 2));
                              $b->UsedValue     = human_filesize(round($usedvalues->used, 2));
                           } elseif ($b->Defenition == 1) {
                              if ($usedvalues->assigned == 120000) {
                                 $b->AssignedValue = 'onbeperkt';
                              } else {
                                 $b->AssignedValue = second2hms(round($usedvalues->assigned, 2));
                              }
                              $b->UsedValue = second2hms(round($usedvalues->used, 2));
                           } else {
                              $b->AssignedValue = round($usedvalues->assigned, 2);
                              $b->UsedValue     = round($usedvalues->used, 2);
                           }
                           $b->icon = ratingtype($b->Defenition);
                        }
                        if (!empty($b->ValidUntil)) {
                           $tos           = explode('T', $b->ValidUntil);
                           $b->ValidUntil = $tos[0];
                           unset($tos);
                           if ($b->ValidUntil > date('Y-m-d')) {
                              $r[] = $b;
                           }
                        } else {
                           $r[] = $b;
                        }
                     } else {
                        if (!empty($b->ValidUntil)) {
                           $tos           = explode('T', $b->ValidUntil);
                           $b->ValidUntil = $tos[0];
                           unset($tos);
                        } else {
                           $b->ValidUntil = '2099-12-31';
                        }
                        if ($b->ValidUntil > date('Y-m-d')) {
                           $r[] = $b;
                        }
                     }
                  }
               } else {
                  $r[] = $bundles->BundleAssign;
               }
            }
         }
         $this->final = $r;
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = array(
            $res,
            $this->ServiceBundle->__getLastRequest()
         );
      }
      return $this->final;
   }
   public function GetBundleDefinition($Id)
   {
      try {
         $result = $this->ServiceBundle->GetBundleDefinition(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'BundleDefinitionId' => $Id
         ));
         // mail('lainard@gmail.com', 'bundle', print_r($this->ServiceBundle->__getLastRequest() . " " . $this->ServiceBundle->__getLastResponse(), true));
         if ($result->GetBundleDefinitionResult->Result == "0") {
            if (!empty($result->GetBundleDefinitionResult->ItemInfo->Benefit->RatingUnit)) {
               $this->final = $result->GetBundleDefinitionResult->ItemInfo->Benefit->RatingUnit;
            } else {
               $this->final = 0;
            }
         } else {
            $this->final = 100;
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function getDefenitionid($array)
   {
      $defenition = array();
      $overaldef  = array();
      if (is_array($array)) {
         foreach ($array as $p) {
            if (!empty($p->BundleDefinitionId)) {
               $defenition[] = $p->BundleDefinitionId;
            }
         }
         $d = array_unique($defenition);
         return $d[0];
      }
   }
   public function getUsedValue($array)
   {
      // print_r($array);
      $used     = array();
      $assigned = array();
      if (is_array($array)) {
         foreach ($array as $p) {
            if (empty($p->OverallBundleUsageId)) {
               $date1 = explode('T', $p->ValidUntil);
               if ($date1[0] >= date('Y-m-d')) {
                  $used[]     = $p->UsedValue;
                  $assigned[] = $p->AssignedValue;
               }
               unset($date1);
            }
            // $date2 = explode('T', $p->UsedValue);
         }
         // print_r(array_sum($assigned));
         return (object) array(
            'used' => array_sum($used),
            'assigned' => array_sum($assigned)
         );
      } else {
         return (object) array(
            'used' => 0,
            'assigned' => 0
         );
      }
   }
   public function GetBundleUsageList($SN, $id)
   {
      try {
         $result      = $this->ServiceBundle->GetBundleUsageList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN,
            'BundleAssignId' => $id,
            'PageIndex' => 0,
            'PageSize' => 100,
            'SortBy' => 0,
            'OrderBy' => 0,
            'SortOrder' => 0,
            'ValidOn' => date('Y-m-d') . 'T' . date('H:i:s')
         ));
         $res         = json_decode(json_encode(simplexml_load_string($result->GetBundleUsageListResult->ListInfo->any)));
         $this->final = $res;
         //mail('lainard@gmail.com', 'bundle', print_r($this->ServiceBundle->__getLastRequest() . " " . $this->ServiceBundle->__getLastResponse(), true));
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function getCLI($SN)
   {
      try {
         $result      = $this->ServiceCLI->GetCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN
         ));
         $res         = json_decode(json_encode(simplexml_load_string($result->GetCLIResult->ItemInfo->any)));
         $this->final = $res->NewDataSet->CLI;
         //mail('lainard@gmail.com', 'bundle', print_r($this->ServiceBundle->__getLastRequest() . " " . $this->ServiceBundle->__getLastResponse(), true));
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }

   public function GetBundleUsage($id){

      try {
         $result = $this->ServiceBundle->GetBundleUsage(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'BundleUsageId' => $id
         ));
        return json_decode(json_encode(simplexml_load_string($result->GetBundleUsageResult->ItemInfo->any)));
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetBundleUsageList1($SN, $id)
   {
      try {
         $result = $this->ServiceBundle->GetBundleUsageList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN,
            'BundleAssignId' => $id,
            'PageIndex' => 0,
            'PageSize' => 100,
            'SortBy' => 0,
            'OrderBy' => 0,
            'SortOrder' => 0,
            'ValidOn' => date('Y-m-d') . 'T' . date('H:i:s')
         ));
         $res    = json_decode(json_encode(simplexml_load_string($result->GetBundleUsageListResult->ListInfo->any)));
         $final  = array(
            'BundleUsageId' => '',
            'BundleAssignId' => '',
            'BundleDefinitionId' => '',
            'SN' => $SN,
            'ValidFrom' => '',
            'ValidUntil' => '',
            'AssignedValue' => '',
            'UsedValue' => '',
            'ReservedValue' => '',
            'BundleDefinitionId' => array()
         );
         if (is_array($res->NewDataSet->BundleUsage)) {
            foreach ($res->NewDataSet->BundleUsage as $row) {
               $final['BundleAssignId'] = $row->BundleAssignId;
               $final['ValidFrom']      = $row->ValidFrom;
               $final['ValidUntil']     = $row->ValidUntil;
               $final['AssignedValue']  = $final['AssignedValue'] + (int) $row->AssignedValue;
               $final['UsedValue']      = $final['UsedValue'] + $row->UsedValue;
               if (isset($row->BundleDefinitionId)) {
                  $final['BundleDefinitionId'] = 100;
               }
            }
            $this->final = $final;
         } else {
            $this->final = $res;
         }
         //mail('lainard@gmail.com', 'bundle', print_r($this->ServiceBundle->__getLastRequest() . " " . $this->ServiceBundle->__getLastResponse(), true));
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetPendingPorts()
   {
      try {
         $result = $this->ServicePorting->GetPendingPortIns(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'ContactId' => '-2',
            'MaxResultSet' => 1000
         ));
         return $result;
         exit;
         if ($result->GetNumberPortListResult->Result == "0") {
            $this->final->result = 'success';
            $this->final->data   = $result->GetNumberPortListResult->ListInfo;
         } else {
            $this->final->result  = 'error';
            $this->final->message = $result->GetNumberPortListResult->Result;
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function CancelPortIn($SN)
   {
      $s      = 'error';
      $result = $this->ServicePorting->CancelPortIn(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'Sn' => $SN
      ));
      switch ($result->CancelPortInResult) {
         case 0:
            $rmessage = 'Success';
            $s        = 'success';
            break;
         case -1:
            $rmessage = 'SN does not exist';
            break;
         case -2:
            $rmessage = 'SN is required :' . $SN;
            break;
         case -3:
            $rmessage = 'No port-in request found for this SN';
            break;
         case -4:
            $rmessage = 'Port-in request status invalid';
            break;
         case -5:
            $rmessage = 'Could not delete task and job for this Port-in request';
            break;
         case -6:
            $rmessage = 'Handling of previous jobs failed because of invalid job count';
            break;
         case -7:
            $rmessage = 'The request cannot be canceled. The wish date should at least be 4 days in the future.';
            break;
         case -8:
            $rmessage = 'The PortIn cannot be cancelled for this number because the current status is Pending.';
            break;
         case -90:
            $rmessage = 'Authentication failed';
            break;
         default:
            $rmessage = 'Unknown Error...';
            break;
      }
      return (object) array(
         'result' => $s,
         'message' => $rmessage
      );
   }
   public function GetNumberPort($companyid)
   {
      try {
         $result = $this->ServiceNumberPort->GetNumberPortList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'OperatorId' => $companyid,
            'PageSize' => 1,
            'PageIndex' => 0,
            'SortBy' => 0,
            'Prefix' => '%',
            'ShowBestMatch' => false,
            'SortOrder' => 1
         ));
         echo "REQUEST:\n" . $this->ServiceNumberPort->__getLastRequest() . "\n";
         echo "Response:\n" . $this->ServiceNumberPort->__getLastResponse() . "\n";
         return $result;
         exit;
         if ($result->GetNumberPortListResult->Result == "0") {
            $this->final->result = 'success';
            $this->final->data   = $result->GetNumberPortListResult->ListInfo;
         } else {
            $this->final->result  = 'error';
            $this->final->message = $result->GetNumberPortListResult->Result;
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function AddNumberPort($port)
   {
      try {
         $result = $this->ServiceNumberPort->AddNumberPort(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'Prefix' => $port['Prefix'],
            'OperatorId' => $port['OperatorId'],
            'ValidFrom' => $port['ValidFrom'],
            'Area' => '',
            'RoutingInfo' => ''
         ));
         return $result;
         exit;
         if ($result->AddNumberPortResult->Result == "0") {
            $this->final->result = 'success';
            $this->final->data   = $result->AddNumberPortResult->NewItemId;
         } else {
            $this->final->result  = 'error';
            $this->final->message = $result->AddNumberPortResult->Result;
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function DelNumberPort($portid)
   {
      try {
         $result = $this->ServiceNumberPort->DeleteNumberPort(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'NumberPortId' => $portid
         ));
         return $result;
         exit;
         if ($result->DeleteNumberPortResult->Result == "0") {
            $this->final->result = 'success';
         } else {
            $this->final->result  = 'error';
            $this->final->message = $result;
         }
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetPendingPortOuts()
   {
      $result = $this->ServicePorting->GetPendingPortOuts(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'ContactId' => -2
      ));
      return $result;
   }
   public function GetPendingPortIns()
   {
      $res    = array();
      $result = $this->ServicePorting->GetPendingPortIns(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'PortInFilter' => 'PENDINGANDREJECTED',
         'PortOutFilter' => 'PENDINGANDREJECTED',
         'ContactId' => -2
      ));
      if ($result->GetPendingPortInsResult->TotalItems > 1) {
         foreach ($result->GetPendingPortInsResult->ListInfo as $pe) {
            $pentry["OperationType"]          = "PORT_IN";
            $pentry["CompanyName"]            = "";
            $pentry["AuthorisedRequestrName"] = "";
            $pentry["VATNumber"]              = "";
            $pentry["SN"]                     = trim($pe->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn);
            foreach ($pe->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity as $pp) {
               if ($pp->ParameterId == 20500) {
                  $pentry["DonorOperator"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20501) {
                  $pentry["DonorSIM"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20508) {
                  $pentry["MSISDN"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20502) {
                  $pentry["RequestType"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20506) {
                  $pentry["AccountNumber"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20516) {
                  $pentry["EnteredDate"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20517) {
                  $pentry["ActionDate"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20513) {
                  $pentry["Error.ID"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20514) {
                  $pentry["ErrorValue"]   = $pp->ParameterValue;
                  $pentry["ErrorComment"] = $pp->ParameterValue;
               }
               if ($pp->ParameterId == 20515) {
                  switch ($pp->ParameterValue) {
                     case '0':
                        $pentry["Status"] = 'Port in Not yet complete';
                        break;
                     case '2':
                        $pentry["Status"] = 'Port in Ready to request';
                        break;
                     case '4':
                        $pentry["Status"] = 'Port in Requested';
                        break;
                     case '5':
                        $pentry["Status"] = 'Port in Pending';
                        break;
                     case '24':
                        $pentry["Status"] = 'Port in Exec pending';
                        break;
                     case '6':
                        $pentry["Status"] = 'Port in Ready to cancel';
                        break;
                     case '7':
                        $pentry["Status"] = 'Port in Exec rejected';
                        break;
                     case '25':
                        $pentry["Status"] = 'Port in Accepted';
                        break;
                     case '26':
                        $pentry["Status"] = 'Port in Ready';
                        break;
                     case '8':
                        $pentry["Status"] = 'Port in Rejected';
                        break;
                     case '3':
                        $pentry["Status"] = 'Port in Canceled';
                        break;
                     case '27':
                        $pentry["Status"] = 'Port in Failed';
                        break;
                     case '21':
                        $pentry["Status"] = 'Ported in';
                        break;
                     default:
                        $pentry["Status="] == '';
                  }
                  //$pentry["Status= $pp->ParameterValue;
               }
            }
            $res[] = $pentry;
         }
      }
      return $res;
   }
   public function SaveVerificationCode($sn, $code)
   {
      $result = $this->ServicePorting->GetAvailableProvisioningSims(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'Sn' => $sn,
         'code' => $code
      ));
      return array(
         'response' => $this->ServicePorting->__getLastResponse(),
         'request' => $this->ServicePorting->__getLastRequest()
      );
   }
   public function GetAvailableProvisioningSims($simnumber)
   {
      $result = $this->ServiceCLI->GetAvailableProvisioningSims(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SimNumber' => $simnumber
      ));
      return array(
         'response' => $this->ServiceCLI->__getLastResponse(),
         'request' => $this->ServiceCLI->__getLastRequest()
      );
   }
   public function GetActivationCPSList()
   {
      $result = $this->ServiceGeneral->GetActivationCPSList(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password
      ));
      return array(
         'response' => $this->ServiceGeneral->__getLastResponse(),
         'request' => $this->ServiceGeneral->__getLastRequest(),
         'array' => $this->soap2array($this->ServiceGeneral->__getLastResponse())
      );
   }
   public function GetSimList()
   {
      $this->ServiceMobile->GetSimList(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SIMStatus' => '5'
      ));
      $res = $this->soap2array($this->ServiceMobile->__getLastResponse());
      return $res['soapBody']['GetSIMListResponse']['GetSIMListResult']['ListInfo']['diffgrdiffgram']['NewDataSet']['SIM'];
   }
   public function GetRequestPorting($sn)
   {
      $result = $this->ServicePorting->GetPendingPortIns(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'ContactId' => '-2',
         'MaxResultSet' => 1000
      ));
      return array(
         'response' => $this->ServicePorting->__getLastResponse(),
         'request' => $this->ServicePorting->__getLastRequest(),
         'array' => $this->soap2array($this->ServicePorting->__getLastResponse())
      );
   }
   public function soap2array($response)
   {
      $xml  = preg_replace("/(<\/?)(\w+):([^>]*>)/", '$1$2$3', $response);
      $xml  = simplexml_load_string($xml);
      $json = json_encode($xml);
      return json_decode($json, true);
   }
   public function artilium_getmsisdnfromsimnr($SIMNr)
   {
      $resultIMSIList = $this->ServiceMobile->GetIMSIList(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SIMNr' => $SIMNr
      ));
      if ($resultIMSIList->GetIMSIListResult->TotalItems > 0) {
         $sN2 = simplexml_load_string($resultIMSIList->GetIMSIListResult->ListInfo->any);
         //print_r($sN2);exit;
         foreach ($sN2->NewDataSet->IMSI as $poN) {
            $IMSINr = (string) $poN->IMSINr;
         }
         //echo "\nIMSINr:".$IMSINr;exit;
         $resultIMSIMSISDNList = $this->ServiceMobile->GetIMSIMSISDNList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'IMSINr' => $IMSINr
         ));
         //print_r($resultIMSIMSISDNList);exit;
         if ($resultIMSIMSISDNList->GetIMSIMSISDNListResult->TotalItems > 0) {
            $sN3 = simplexml_load_string($resultIMSIMSISDNList->GetIMSIMSISDNListResult->ListInfo->any);
            foreach ($sN3->NewDataSet->IMSIMSISDN as $poN2) {
               $MSISDNId = (string) $poN2->MSISDNId;
            }
            //echo "\nMSISDNId:".$MSISDNId;exit;
            $resultGetMSISDN = $this->ServiceMobile->GetMSISDN(array(
               'Login' => $this->params->m_username,
               'Password' => $this->params->m_password,
               'MSISDNId' => $MSISDNId
            ));
            //print_r($resultGetMSISDN);exit;
            if ($resultGetMSISDN->GetMSISDNResult->Result == 0) {
               $SNxml = simplexml_load_string($resultGetMSISDN->GetMSISDNResult->ItemInfo->any);
               foreach ($SNxml->NewDataSet->MSISDN as $poN3) {
                  $MSISDNNr = (string) $poN3->MSISDNNr;
               }
               //print_r($SNxml);exit;
               //echo "\nSN:".$SN;exit;
               return $MSISDNNr;
            } else {
               //Error from GetMSISDN
               return "";
            }
         } else {
            //Error from GetIMSIMSISDNList
            return "";
         }
         return $IMSINr;
      } else {
         //Error from GetIMSIList
         return "";
      }
   }
   /*function ActiveNewSIM($ContactName, $m) {
   $InitialAmount = 0;
   $ContactType2Id = 7;
   $MSISDN = $this->artilium_getmsisdnfromsimnr($m->msisdn_sim);
   //____________MOVE CLI_____________
   $resmove = $this->ServiceCLI->MoveCLI(array('Login' => $this->params->m_username, 'Password' => $this->params->m_password, 'SN' => $m->msisdn_sn, 'ContactType2Id' => $ContactType2Id, 'CopyCurrentBundles' => 0, 'Logging' => array('AccountType' => 0, 'Account' => '')));
   
   if (($resmove->MoveCLIResult->Result == "0") || ($resmove->MoveCLIResult->Result == "-10")) {
   $this->AddMMSGPRS($m);
   $this->PartialFullBar($m);
   
   //Success, so we now try to do the update
   //____________UPDATE CLI_____________
   $resupd = $this->ServiceCLI->UpdateCLI(array('Login' => $this->params->m_username, 'Password' => $this->params->m_password, 'SN' => $m->msisdn_sn, 'Status' => 1, 'LanguageId' => $m->msisdn_languageid, 'CustomerName' => $ContactName, 'ActivationCPS' => 1, 'MaxTotalCredits' => 0));
   
   if ($resupd->UpdateCLIResult == "0") {
   //Successful Update
   
   //Here we add the initial amount
   if ($InitialAmount > 0) {
   $resultAddCredits = $this->ServiceReload->Reload(array('Login' => $this->params->m_username, 'Password' => $this->params->m_password,
   'Msisdn' => $MSISDN, 'ReloadAmount' => $InitialAmount,
   'ReloadType' => 1,
   'ReloadSubType' => 104,
   'Description' => 'MVNO Portal initial add',
   'PaymentType' => 0,
   'OrderID' => '1st :' . $MSISDN,
   'Status' => 0,
   ));
   
   }
   
   //Now we change the SIM status to active
   //Some of them are on warehouse
   //____________UPDATE SIM_____________
   $ressim = $this->ServiceMobile->UpdateSIM(array('Login' => $this->params->m_username, 'Password' => $this->params->m_password, 'SIMNr' => $m->msisdn_sim, 'Status' => 5));
   if ($ressim->UpdateSIMResult == "0") {
   return (object) array("result" => "success");
   } else {
   return (object) array("result" => "error", "message" => "SIM UPDATE ERROR");
   }
   } else {
   return (object) array("result" => "error", "message" => "CLI UPDATE ERROR");
   }
   } else {
   return (object) array("result" => "error", "message" => "MOVE ERROR");
   }
   }
   */
   public function UpdateCLI($sn, $status)
   {
      if ($status == 1) {
         $resupd = $this->ServiceCLI->UpdateCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'Status' => $status,
            'ActivationCPS' => 1
         ));
      } else {
         $resupd = $this->ServiceCLI->UpdateCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'Status' => $status
         ));
      }
      if ($resupd->UpdateCLIResult == "0") {
         return (object) array(
            'result' => 'success'
         );
      } else {
         return (object) array(
            'result' => 'error'
         );
      }
   }
   public function SwitchVoiceMail($msisdn, $sn, $status)
   {
      if ($status == 1) {
         $resupd = $this->ServiceCLI->UpdateCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'DestinationsAcceptedRejected' => $status,
            'DestinationsAccepted' => '00316240' . substr(trim($msisdn), 2)
         ));
      } else {
         $resupd = $this->ServiceCLI->UpdateCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn,
            'DestinationsAcceptedRejected' => $status,
            'DestinationsAccepted' => ''
         ));
      }
      if ($resupd->UpdateCLIResult == "0") {
         return (object) array(
            'result' => 'success'
         );
      } else {
         return (object) array(
            'result' => 'error'
         );
      }
   }
   public function TerminateCLI($sn, $date)
   {
      $resupd = $this->ServiceCLI->UpdateCLI(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SN' => $sn,
         'DateRangeUntil' => $date
      ));
      if ($resupd->UpdateCLIResult == "0") {
         return (object) array(
            'result' => 'success'
         );
      } else {
         return (object) array(
            'result' => 'error'
         );
      }
   }
   public function UpdateLanguage($sn, $msisdn_languageid)
   {
      $resupd = $this->ServiceCLI->UpdateCLI(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SN' => $sn,
         'LanguageId' => $msisdn_languageid
      ));
      if ($resupd->UpdateCLIResult == "0") {
         return (object) array(
            'result' => 'success'
         );
      } else {
         return (object) array(
            'result' => 'error'
         );
      }
   }
   public function PortingNewSIM($ContactName, $m)
   {
      try {
      mail('mail@simson.one',"porting error :".$ContactName, print_r($m, true));
      $InitialAmount  = 0;
      $ContactType2Id = 7;
      //With porting, the initial reload needs to be on the original MSISDN
      //$MSISDN = $this->artilium_getmsisdnfromsimnr($m->msisdn_sim);
      if ($m->msisdn_type == "porting") {
         if (empty($m->donor_customertype)) {
            $m->donor_customertype = "0";
         }
         
         //Successful Update SIM
         //So now we do the PortInRequest:
         //logActivity("now we try porting in ");
         $reqarray  = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $m->msisdn_sn,
            'OperationType' => 'PORT_IN',
            'RequestType' => $m->ptype_belgium,
            'DonorOperator' => $m->donor_provider,
            'MSISDN' => $m->donor_msisdn,
            'DonorSIM' => $m->donor_sim,
            'AccountNumber' => '',
            'CustomerName' => $ContactName,
            'ActionDate' => $m->date_wish.'T00:00:00',
            'EnteredDate' => date('Y-m-d').'T'.date('H:i:s'),
            'Status' => '',
         );

         if($m->ptype_belgium != "PREPAY"){

            $reqarray['AccountNumber'] =  $m->donor_accountnumber;
            $reqarray['VATNumber'] = getCustomerVat($m->userid);
            $reqarray['AuthorisedRequestorName'] =  $ContactName;
            $reqarray['CompanyName'] =  getCustomerCompanyName($m->userid);
         }
         $ressim=$this->ServiceMobile->UpdateSIM(array('Login' => $this->params->m_username,'Password' => $this->params->m_password,'SIMNr'=>$m->msisdn_sim,'Status'=>5));

         
         $resportin = $this->ServicePorting->PortInRequest($reqarray);
         mail('mail@simson.one',"porting error :".$ContactName, print_r($this->ServicePorting->__getLastResponse(), true));
         /* insert_query_log(array(
         'funct' => 'ActiveNewSIM->PortInRequest',
         'description' => json_encode(array('Login' => $this->params->m_username, 'Password' => $this->params->m_password,
         'Sn' => $m->msisdn_sn,
         'DonorOperator' => $m->donor_provider,
         'SimCardNumber' => $m->donor_sim,
         'PortingType' => $m->donor_type,
         'AccountNumber' => $m->donor_accountnumber,
         'MsisdnNumber' => $m->donor_msisdn,
         'PortInWishDate' => $m->date_wish,
         'CustomerType' => $m->donor_customertype,
         ))));
         */
         return (object) array(
            'result' => 'success',
            'id' => $resportin->PortInRequestResult
         );
      } else {
         return (object) array(
            'result' => 'error',
            'message' => 'The service needs to be set as porting before requesting porting'
         );
      }
   }catch (SoapFault $fault) {
      // mail("lainard@gmail.com", "eror", "SOAP FAULT: Bundle Assignment \n" . $resultB->__getLastRequest() . $resultB->__getLastResponse() . "\n||\nRESPONSE\n" . $fault);
   }
   }
   public function ActiveNewSIM($ContactName, $m)
   {
      $InitialAmount  = 0;
      $ContactType2Id = $m->msisdn_contactid;
      //With porting, the initial reload needs to be on the original MSISDN
      $MSISDN         = $this->artilium_getmsisdnfromsimnr($m->msisdn_sim);
      //____________MOVE CLI_____________
      $resmove        = $this->ServiceCLI->MoveCLI(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SN' => $m->msisdn_sn,
         'ContactType2Id' => $ContactType2Id,
         'CopyCurrentBundles' => 0,
         'Logging' => array(
            'AccountType' => 0,
            'Account' => ''
         )
      ));
      if (($resmove->MoveCLIResult->Result == "0") || ($resmove->MoveCLIResult->Result == "-10")) {
         //Add MMS / GPRS (wait 3 seconds first)
         //sleep(10);
         $this->AddMMSGPRS($m);
         //logActivity("Going to start PartialFullBar...");
         $this->PartialFullBar($m);
         //Success, so we now try to do the update
         //____________UPDATE CLI_____________
         $resupd = $this->ServiceCLI->UpdateCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $m->msisdn_sn,
            'Status' => 1,
            'LanguageId' => $m->msisdn_languageid,
            'CustomerName' => $ContactName
         ));
         if ($resupd->UpdateCLIResult == "0") {
            //Successful Update
            //Here we add the initial amount
            //logActivity('Going to start (Initial) Reload...');
            $resultAddCredits = $this->ServiceReload->Reload(array(
               'Login' => $this->params->m_username,
               'Password' => $this->params->m_password,
               'Sn' => $m->msisdn_sn,
               'ReloadAmount' => $InitialAmount,
               'ReloadType' => 1,
               'ReloadSubType' => 104,
               'Description' => 'MVNO Portal initial add',
               'PaymentType' => 0,
               'OrderID' => '1st :' . $MSISDN,
               'Status' => 0
            ));
            //logActivity("Add Initial Amount : REQUEST \n" . $this->ServiceReload->__getLastRequest() . "\n||\nRESPONSE\n" . $this->ServiceReload->__getLastResponse());
            //Now we change the SIM status to active
            //Some of them are on warehouse
            //____________UPDATE SIM_____________
            //logActivity('Going to start UpdateSIM...');
            $ressim           = $this->ServiceMobile->UpdateSIM(array(
               'Login' => $this->params->m_username,
               'Password' => $this->params->m_password,
               'SIMNr' => $m->msisdn_sim,
               'Status' => 5
            ));
            //print_r($ressim);exit;
            //logActivity("Activation STEP 3 UPDATE SIM: REQUEST \n" . $this->ServiceMobile->__getLastRequest() . "\n||\nRESPONSE\n" . $this->ServiceMobile->__getLastResponse());
            if ($ressim->UpdateSIMResult == "0") {
               if ($m->msisdn_type == "porting") {
                  //Successful Update SIM
                  //So now we do the PortInRequest:
                  //logActivity("now we try porting in ");
                  /*
                  $reqarray = array('Login' => $this->params->m_username, 'Password' => $this->params->m_password,
                  'Sn' => $m->msisdn_sn,
                  'DonorOperator' => $m->donor_provider,
                  'SimCardNumber' => $m->donor_sim,
                  'PortingType' => $m->donor_type,
                  'AccountNumber' => $m->donor_accountnumber,
                  'MsisdnNumber' => $m->donor_msisdn,
                  'PortInWishDate' => $m->date_wish,
                  'CustomerType' => $m->donor_customertype,
                  );
                  */
                  $resportin = $this->PortingNewSIM($ContactName, $m);
                  //print_r($this->ServicePorting->__getLastRequest());
                  //print_r($this->ServicePorting->__getLastResponse());
                  //exit;
                  mail('simson.parlindungan@united-telecom.be', 'Porting request : ', $m->msisdn . " \n" . print_r($resportin, true));
                  /*insert_query_log(array(
                  'funct' => 'ActiveNewSIM->PortInRequest',
                  'description' => $resportin));
                  */
                  if ($resportin->id == "0") {
                     if($m->companyid == 53){
                        $this->createSUMassignment($m->msisdn_sn);
                     }
                
                     return (object) array(
                        'result' => 'success'
                     );
                  } else {
                     switch ($resportin->id) {
                        case '-2':
                           $errtext = "SN is required";
                           break;
                        case '-3':
                           $errtext = "One or more mandatory parameters are missing.";
                           break;
                        case '-4':
                           $errtext = "SOne or more mandatory parameters are missing.";
                           break;
                        case '-5':
                           $errtext = "One or more mandatory parameters are missing.";
                           break;
                        case '-6':
                           $errtext = "CustomerType invalid";
                           break;
                        case '-7':
                           $errtext = "PortInWishDate invalid (system setting 10000 MNP Execution Time determines how many working days should be in between the request date and the wish date).";
                           break;
                        case '-8':
                           $errtext = "PortingType invalid";
                           break;
                        case '-9':
                           $errtext = "Network status of CLI/PIN not valid";
                           break;
                        case '-10':
                           $errtext = "MsisdnNumber already exists in reseller database";
                           break;
                        case '-11':
                           $errtext = "Port-out in progress for CLI/PIN";
                           break;
                        case '-12':
                           $errtext = "Port-in in progress for CLI/PIN or cancellation in progress";
                           break;
                        case '-13':
                           $errtext = "DonorOperator Invalid";
                           break;
                        case '-14':
                           $errtext = "MsisdnNumber Invalid";
                           break;
                        case '-15':
                           $errtext = "SimCardNumber Donor operator is Invalid";
                           break;
                        case '-16':
                           $errtext = "AccountNumber Invalid";
                           break;
                        case '-90':
                           $errtext = "Authentication failed";
                           break;
                        case '-91':
                           $errtext = "Field overflow (too many characters, date out of range, ...)";
                           break;
                        case '-100':
                           $errtext = "Unspecified Error";
                           break;
                        default:
                           $errtext = "Unknown Error...";
                           break;
                     }
                     if ($resportin->id == '-12') {
                        return (object) array(
                           'result' => 'success'
                        );
                     } else {
                        return (object) array(
                           "result" => "error",
                           "message" => "PORT IN ERROR :  " . $errtext
                        );
                     }
                  }
               } else {
                  return (object) array(
                     "result" => "success"
                  );
               }
            } else {
               return (object) array(
                  "result" => "error",
                  "message" => "SIM UPDATE ERROR"
               );
            }
         } else {
            return (object) array(
               "result" => "error",
               "message" => "CLI UPDATE ERROR"
            );
         }
      } else {
         return (object) array(
            "result" => "error",
            "message" => "MOVE ERROR" . print_r($resmove->MoveCLIResult)
         );
      }
   }
   public function AddMMSGPRS($m)
   {
      $result = $this->ServicePackages->GetListPackageOptionsForSN(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SN' => $m->msisdn_sn,
         'ActiveAndNonActive' => true
      ));
      if ($result->GetListPackageOptionsForSNResult->TotalItems > 0) {
         $s2 = simplexml_load_string($result->GetListPackageOptionsForSNResult->ListInfo->any);
         $i  = 0;
         foreach ($s2->NewDataSet->PackageOptions as $po) {
            if ($po->CallModeDescription == "MMS") {
               $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->MMS);
            }
            if ($po->CallModeDescription == "WEB") {
               $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->GPRS);
            }
            if ($po->CallModeDescription == "4G") {
               $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->LTE);
            }
            if ($po->CallModeDescription == "Roaming") {
               $PDID_Roaming = $po->PackageDefinitionId;
               $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->Roaming);
            }
            if ($po->CallModeDescription == "International Numbers") {
               $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->BarInternationalCalls);
            }
            if (($po->CallModeDescription == "Premium gaming") || ($po->CallModeDescription == "Premium Gaming")) {
               $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->BarPremium);
            }
            if ($po->CallModeDescription == "Premium Numbers") {
               $PDID_BarPremiumVoice = $po->PackageDefinitionId;
               $this->UpdatePackageOptionsForSN($m->msisdn_sn, $po->PackageDefinitionId, $m->BarPremiumVoice);
            }
         }
      }
      return true;
   }
   public function AddBundleAssign($sn, $bundleid, $ValidFrom, $ValidUntil)
   {
      try {
         //$option = array('trace' => 1, 'features' => SOAP_USE_XSI_ARRAY_TYPE);
         $BAS     = array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'BundleId' => getBundleId($bundleid),
            'ContactTypeId' => -1,
            'AvailableSubcustomer' => 0,
            'SharedUsage' => 0,
            'SN' => trim($sn),
            'OperatorId' => -1,
            'PackageId' => -1,
            'ProductId' => -1,
            'CallModeId' => -1,
            'TrafficId' => -1,
            'ValidFrom' => $ValidFrom,
            'ValidUntil' => $ValidUntil
         );
         $resultB = $this->ServiceBundle->AddBundleAssign($BAS);
         if ($resultB->AddBundleAssignResult->Result == "0") {
            return (object) array(
               'result' => 'success',
               'id' => $resultB->AddBundleAssignResult->NewItemId
            );
         } else {
            //mail("lainard@gmail.com", "Error On Bundleassign", "SOAP FAULT: Bundle Assignment \n" . $resultB->__getLastRequest() . $resultB->__getLastResponse() . "\n||\nRESPONSE\n");
            return (object) array(
               'result' => 'error'
            );
         }
      }
      catch (SoapFault $fault) {
         /// mail("lainard@gmail.com", "eror", "SOAP FAULT: Bundle Assignment \n" . $resultB->__getLastRequest() . $resultB->__getLastResponse() . "\n||\nRESPONSE\n" . $fault);
      }
   }
   public function UpdateServices($sn, $pack, $status)
   {
      $res = array();
      if ($pack) {
         foreach ($pack as $row) {
            if ($row->Customizable == 1) {
               $change = true;
            } else {
               $change = false;
            }
            if ($change) {
               $res[] = $this->UpdatePackageOptionsForSN($sn, $row->PackageDefinitionId, $status);
            }
            unset($change);
         }
         return (object) array(
            'result' => 'success',
            'message' => $res
         );
      } else {
         return (object) array(
            'result' => 'error',
            'message' => 'Not possible'
         );
      }
   }

   public function UpdateServicesDone($sn, $pack, $status, $service)
   {
      $res = array();
      if ($pack) {
         foreach ($pack as $row) {

            if ($row->Customizable == 1) {
               if(trim($row->CallModeDescription) == "Data Originating"){
                  if($service->GPRS == 1){
                     $change = true;
                  }else{
                     $change = false;
                  }

               }elseif(in_array($row->CallModeDescription, array("Data Roaming Originating","Roaming","Voice phone calls Roaming Originating"))){
    
                  if($service->Roaming == 1){
                     $change = true;
                  }else{
                     $change = false;
                  }
               }elseif(trim($row->CallModeDescription) == "International Numbers"){
                  if($service->BarInternationalCalls == 1){
                     $change = false;
                  }else{
                     $change = true;
                  }
               }elseif(trim($row->CallModeDescription) == "Premium Numbers"){

                  if($service->BarPremiumVoice == 1){
                     $change = false;
                  }else{
                     $change = true;
                  }
    
               }elseif(trim($row->CallModeDescription) == "Premium Gaming"){
                  if($service->BarPremium == 1){
                     $change = false;
                  }else{
                     $change = true;
                  }

               }elseif(trim($row->CallModeDescription) == "4G"){
                  if($service->LTE == 1){
                     $change = true;
               }else{
                  $change = false;
               }
            } else {
               $change = true;
            }
            } else {
               $change = false;
            }
            if ($change) {
               $res[] = $this->UpdatePackageOptionsForSN($sn, $row->PackageDefinitionId, $status);
            }
            unset($change);
         }
         return (object) array(
            'result' => 'success',
            'message' => $res
         );
      } else {
         return (object) array(
            'result' => 'error',
            'message' => 'Not possible'
         );
      }
   }
   public function UpdatePackageOptionsForSN($SN, $PackageDefinitionId, $Available)
   {
      try {
         $resultPACKupd = $this->ServicePackages->UpdatePackageOptionsForSN(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $SN,
            'PackageOptions' => array(
               'PackageDefinitionId' => $PackageDefinitionId,
               'Available' => $Available
            )
         ));
         if ($resultPACKupd->UpdatePackageOptionsForSNResult->Result == 0) {
            return (object) array(
               'result' => 'success',
               'data' => $PackageDefinitionId . ' ' . $Available
            );
         } else {
            return (object) array(
               'result' => 'error'
            );
         }
      }
      catch (Exception $e) {
         return (object) array(
            'result' => 'error'
         );
      }
   }
   public function PartialFullBar($m)
   {
      //0 = No bar;
      //1 = Partial Bar
      //2 = Full Bar
      //0 = No bar;
      if ($m->bar == "2") {
         $result = $this->ServiceCLI->UpdateParametersCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $m->msisdn_sn,
            'ParameterList' => array(
               'Parameters' => array(
                  'Parameter' => array(
                     'ParameterId' => 20541,
                     'ParameterValue' => '2',
                     'ParameterDeleted' => false
                  )
               )
            )
         ));
      } elseif ($m->bar == "1") {
         $result = $this->ServiceCLI->UpdateParametersCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $m->msisdn_sn,
            'ParameterList' => array(
               'Parameters' => array(
                  'Parameter' => array(
                     'ParameterId' => 20541,
                     'ParameterValue' => '1',
                     'ParameterDeleted' => false
                  )
               )
            )
         ));
      } else {
         //We check for partail
         $result = $this->ServiceCLI->UpdateParametersCLI(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $m->msisdn_sn,
            'ParameterList' => array(
               'Parameters' => array(
                  'Parameter' => array(
                     'ParameterId' => 20541,
                     'ParameterValue' => '0',
                     'ParameterDeleted' => false
                  )
               )
            )
         ));
      }
      if ($result->UpdateParameterResult->Result == "0") {
         return true;
      } else {
         return "IN Update Parameters Error";
      }
   }
   public function convert2array($result)
   {
      $xml   = simplexml_load_string($result);
      // Grabs the posts
      $posts = $xml->children('SOAP-ENV', true)->Body->children('ns1', true)->Posts->Post;
      // Take the posts and generate some markup
      return $posts;
   }
   public function get_cdr($mobile)
   {
      $bytes  = array();
      //echo $msisdn;
      $cdr    = array();
      //print_r($this->data['mobile']);
      //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
      //$result = $this->artilium->GetCDRList($sn);
      $sms    = array();
      $voice  = array();
      $bytes  = array();
      /*$req = array('action' => 'GetCDRList',
      'type' => $this->data['mobile']->PaymentType,
      'From' => date('Y-m-01\TH:i:s'),
      'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
      'SN' => $sn,
      'PageIndex' => 0,
      'PageSize' => 2500,
      'SortBy' => 0,
      'SortOrder' => 0,
      'companyid' => $this->data['setting']->companyid);
      $result = $this->Admin_model->artiliumPost($req);
      */
      $params = array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'type' => 'POST PAID',
         'SN' => $mobile->details->msisdn_sn,
         'From' => date('Y-m-01\T00:00:00+02:00'),
         'Till' => date('Y-m-d\T23:i:s'),
         'PageIndex' => 0,
         'PageSize' => 2500,
         'SortBy' => 0,
         'SortOrder' => 0
      );
      $result = $this->ServiceCDR->GetCDRList($params);
      //return $result;
      $s      = json_decode(json_encode(simplexml_load_string($result->GetCDRListResult->ListInfo->any)));
      //return $s;
      //$e = $this->soap2array($this->ServiceCDR->__getLastResponse());
      //return $e->soapBody->GetCDRListResponse->GetCDRListResult->ListInfo->diffgrdiffgram->NewDataSet->CDRs;
      //return $e; GetCDRListResult":{"Result":0,"ListInfo":{"schema"
      //exit;
      if (empty($s)) {
         return array(
            'cdr_count' => 0,
            'cdr' => array(),
            'data' => 0,
            'voice' => 0,
            'sms' => 0
         );
      }
      if ($result->GetCDRListResult->Result == "0") {
         $count = $result->GetCDRListResult->TotalItems;
         foreach ($s->NewDataSet->CDRs as $row) {
            if (empty($row->DestinationCountry)) {
               $country = "";
            } else {
               $country = $row->DestinationCountry;
            }
            if ($row->Cause != "1000") {
               if ($row->TypeCallId != "5") {
                  if ($row->MaskDestination != "72436") {
                     if ($row->TrafficTypeId == "2") {
                        $cdr[] = array(
                           'Begintime' => format_cdr_time($row->Begintime),
                           'DestinationCountry' => $country,
                           'MaskDestination' => $row->MaskDestination,
                           'DurationConnection' => round($row->DurationConnection / 1048576, 2) . 'MB',
                           'TrafficTypeId' => $row->TrafficTypeId,
                           'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed)
                        );
                     } else {
                        if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                        } else {
                           if ($row->TypeCallId != "3") {
                              $cdr[] = array(
                                 'Begintime' => format_cdr_time($row->Begintime),
                                 'DestinationCountry' => $country,
                                 'MaskDestination' => $row->MaskDestination,
                                 'DurationConnection' => $row->DurationConnection,
                                 'TrafficTypeId' => $row->TrafficTypeId,
                                 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed))
                              );
                           }
                        }
                        /*
                        $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $row->DestinationCountry, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed));
                        */
                     }
                  }
               }
               if ($row->TypeCallId != 5) {
                  if (!in_array($row->TrafficTypeId, array(
                     2,
                     3
                  ))) {
                     if (strlen($row->MaskDestination) > 9) {
                        if ($row->TrafficTypeId) {
                           if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {
                           } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {
                           } else {
                              if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
                                 $voice[] = $row->DurationConnection;
                              } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
                                 $sms[] = $row->DurationConnection;
                              }
                           }
                        }
                     }
                  } elseif ($row->TrafficTypeId == 2) {
                     $bytes[] = $row->DurationConnection;
                  }
               }
            }
         }
      }
      //print_r($cdr);
      //exit;
      if (!$sms) {
         $smsi = '0';
      } else {
         $smsi = array_sum($sms);
      }
      return array(
         'cdr_count' => $count,
         'cdr' => $cdr,
         'data' => array_sum($bytes),
         'voice' => array_sum($voice),
         'sms' => $smsi
      );
      //return array('cdr' => $cdr, 'data' => array_sum($bytes));
   }
   public function UpdateBundleAssignV2($data)
   {
      $data['Login']    = $this->params->m_username;
      $data['Password'] = $this->params->m_password;
      $result           = $this->ServiceBundle->UpdateBundleAssignV2($data);
      return $result;
   }
   public function ServiceLogging($method, $data)
   {
      $data['Login']    = $this->params->m_username;
      $data['Password'] = $this->params->m_password;
      try {
         $Result = $this->ServiceLogging->$method($data);
         return $Result->GetLoggingBySnResult->ListInfo;
         if ($Result->ServiceLoggingResult->Result == 0) {
            $res = json_decode(json_encode(simplexml_load_string($result->ServiceLoggingResult->ListInfo)));
            return (object) array(
               'result' => 'success',
               'data' => $res
            );
         } else {
            return (object) array(
               'result' => 'error'
            );
         }
         $this->final = $res;
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetSUMAssignmentList1($sn)
   {
      try {
         $Result = $this->ServiceUsageMonitoring->GetSUMAssignmentList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn
         ));

         return $Result;

      } catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   public function GetSUMAssignmentList($sn)
   {
      try {
        $Plan =  $this->ServiceUsageMonitoring->GetPlanList(array(
          'Login' => $this->params->m_username,
          'Password' => $this->params->m_password));
          
         //$this->ServiceUsageMonitoring = new soapclient($this->service . '/SUM/ServiceUsageMonitoring.asmx?WSDL', $this->option);
         $Result = $this->ServiceUsageMonitoring->GetSUMAssignmentList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn
         ));

       /*  $Result = $this->ServiceUsageMonitoring->GetSUMAssignmentList(array(
            'Login' => $this->params->m_username,
            'Password' => $this->params->m_password,
            'SN' => $sn
         ));

*/
        // print_r($Plan);
         $res = array();
         foreach($Plan->GetPlanListResult->Data as $pl){
         if(!empty($Result->GetSumAssignmentListResult->Data)){
         if(is_array($Result->GetSumAssignmentListResult->Data)){
         foreach($Result->GetSumAssignmentListResult->Data as $row){

          if($row->SumPlanId == $pl->SumPlanId){

            $res[] = array('PlanId'=> $pl->SumPlanId,'SumAssignmentId' => $row->SumAssignmentData->SumAssignmentId, 'SumSetId' => $row->SumAssignmentData->SumActionSetId,   'Name' => $pl->Code, 'ValidFrom' => $row->SumAssignmentData->Validity->BeginDate, 'ActionSet' => $this->ServiceUsageMonitoring->GetSUMPlanActionSets(array(
               'Login' => $this->params->m_username,
               'Password' => $this->params->m_password,
               'SumPlanId' => $pl->SumPlanId
            )) );

          }

           
          }
         }
         }
         }
         return $res;
         
      }
      catch (Exception $e) {
         $this->final->result  = 'error';
         $this->final->message = $e->getMessage();
      }
      return $this->final;
   }
   function CreateAssignment($data){

      $Result = $this->ServiceUsageMonitoring->UpdateActionSet(array(
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'SN' => $data['SN'],
         'AssignmentId' => $data['AssignmentId'],
         'ActionSetId' => $data['ActionSetId']
      ));

      return $Result;


     


   }

   function createSUMassignment($SN) {
      //print_r($params);exit;
     
      $DateRangeData = new stdClass();
      $SumPlanId = 2;
	   $SumActionSetId = 2;
      $DateRangeData->BeginDate = "2018-07-01";
      $DateRangeData->EndDate = "2099-01-01";
   
      try {
         $resupd =  $this->ServiceUsageMonitoring->CreateAssignment(array( 
         'Login' => $this->params->m_username,
         'Password' => $this->params->m_password,
         'Sn' => $SN,
         'SumPlanId' => $SumPlanId,
         'SumActionSetId' => $SumActionSetId,
         'ApplyToSubContacts' => false,
         'Validity' => $DateRangeData));
   
         if ($resupd->CreateAssignmentResult->ResultCode == "0") {
          
            $ccp = 0;
         } else {
            $errorstr = "FAILED SUM assign : " . print_r($resupd, true);
            $ccp = -1;
         }
         if ($ccp == "0") {
            $successful = true;
         } else {
            $successful = false;
           
         }
         return $resupd;
         //$successful=true;
      
         if ($successful) {
            $result = "success";
         } else {
            $result = $ccp;
         }
      } catch (SoapFault $fault) {
         return "error"-"SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})";
         
      }
   
      
      return $result;
   
   }

   function mvno_PartialFullBar($INparams, $SN, $BlockType, $vars)
   {
      /*
      [platform] => POSTPAID
      [reselleruser] => STUnitedPostpaid
      [resellerpass] => $TUn1tedP0stpa1D
      */
      $user = $INparams['reselleruser'];
      $pass = $INparams['resellerpass'];
      switch ($BlockType) {
         case 'FullBar':
            $ParameterValue = 2;
            break;
         case 'PartialBar':
            $ParameterValue = 1;
            break;
         default:
            $ParameterValue = 0;
            break;
      }
      $result = $this->ServicePackages->UpdateParametersCLI(array(
         'Login' => $user,
         'Password' => $pass,
         'SN' => $SN,
         'ParameterList' => array(
            'Parameters' => array(
               'Parameter' => array(
                  'ParameterId' => 20541,
                  'ParameterValue' => $ParameterValue,
                  'ParameterDeleted' => false
               )
            )
         )
      ));
      if ($result->UpdateParameterResult->Result == "0") {
         //Successful Update
         return true;
      } else {
         return "IN Update Parameters Error";
      }
   }
}
