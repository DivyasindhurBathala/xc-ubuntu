<?php
/**
 * Artilium Library for Soap Interfacses
 */
class Pareteum
{
    private $CI;
    private $option;
    private $url;
    private $final;
    private $service;
    private $ServicePackages;
    private $ServiceMobile;
    private $ServiceCLI;
    private $ServicePorting;
    private $ServiceBundle;
    private $ServiceCDR;
    private $ServiceNumberPort;
    private $ServiceGeneral;
    private $ServiceReload;
    private $ServiceLogging;
    private $companyid;
    private $params;
    private $log;
    // private $ServiceUsageMonitoring;
    public function __construct($vars)
    {
        libxml_disable_entity_loader(false);
        $this->CI =& get_instance();
        $this->db     = $this->CI->load->database('default', true);
        if (!empty($vars['api_id'])) {
            $q            = $this->CI->db->query('select * from a_api_data where id=?', array(
            $vars['api_id']
            ));
        } else {
            if (empty($vars['companyid'])) {
                $this->companyid = get_companyidby_url(base_url());
            } else {
                $this->companyid = $vars['companyid'];
            }

            $q            = $this->CI->db->query('select * from a_api_data where companyid=?', array(
            $this->companyid
            ));
        }

        if ($q->num_rows() <= 0) {
            die('Api error');
        }
        $this->params = $q->row();
        $this->params->apiKey = base64_encode($q->row()->m_username.':'.$q->row()->m_password);
        log_message('error', 'Companyid:' . $this->companyid);
        log_message('error', 'Companyid:' . print_r($this->params, true));
    }
    public function accountInformation($params)
    {
        /*{
        "resultCode": "0",
        "resultType": "SUCCESS",
        "messages": "Success getting account information",
        "card-network-status": "ACTIVATED",
        "remaining-credit": "23.4863",
        "currency": "GBP",
        "resources": [
        {
        "resourceName": "VOICE_BUNDLE_IRN",
        "resourceValue": "6000",
        "resourceUnit": "SECOND",
        "resourceExpirationDate": "2019/06/02 00:00:00 UTC",
        "resourceCurrency": {},
        "resourceLastActivityDate": "2019/05/02 11:17:36 UTC"
        },
        {
        "resourceName": "MAIN_CREDIT",
        "resourceValue": "23.4863",
        "resourceUnit": "CENT",
        "resourceCurrency": "GBP",
        "resourceLastActivityDate": "2019/05/10 11:40:55 UTC"
        },
        Pareteum REST APIs (UK )
        Page 8 of 17
        January 2019
        {
        "resourceName": "SMS_BUNDLE",
        "resourceValue": "15000",
        "resourceUnit": "SMS",
        "resourceExpirationDate": "2019/06/02 00:00:00 UTC",
        "resourceCurrency": {},
        "resourceLastActivityDate": "2019/05/02 11:13:05 UTC"
        },
        {
        "resourceName": "VOICE_BUNDLE",
        "resourceValue": "899760",
        "resourceUnit": "SECOND",
        "resourceExpirationDate": "2019/06/02 00:00:00 UTC",
        "resourceCurrency": {},
        "resourceLastActivityDate": "2019/05/09 12:19:40 UTC"
        },
        {
        "resourceName": "DATA_BUNDLE",
        "resourceValue": "32147906",
        "resourceUnit": "KB",
        "resourceExpirationDate": "2019/06/02 00:00:00 UTC",
        "resourceCurrency": {},
        "resourceLastActivityDate": "2019/05/13 11:01:56 UTC"
        },
        {
        "resourceName": "DATA_BUNDLE_ROAM",
        "resourceValue": "33554432",
        "resourceUnit": "KB",
        "resourceExpirationDate": "2019/06/02 00:00:00 UTC",
        "resourceCurrency": {},
        "resourceLastActivityDate": "2019/05/02 11:13:05 UTC"
        },
        {
        "resourceName": "VOICE_BUNDLE_ROAM",
        "resourceValue": "900000",
        "resourceUnit": "SECOND",
        "resourceExpirationDate": "2019/06/02 00:00:00 UTC",
        "resourceCurrency": {},
        "resourceLastActivityDate": "2019/05/02 11:13:05 UTC"
        },
        {
        "resourceName": "SMS_BUNDLE_ROAM",
        "resourceValue": "15000",
        "resourceUnit": "SMS",
        "resourceExpirationDate": "2019/06/02 00:00:00 UTC",
        "resourceCurrency": {},
        "resourceLastActivityDate": "2019/05/02 11:13:05 UTC"
        Pareteum REST APIs (UK )
        Page 9 of 17
        January 2019
        }
        ]
        }
        */

        $res =  $this->__curlGet('superapi', 'accountInformation/' . $params['msisdn'], $params);
        return $res;

        //GET
    }


    public function AddCustomers($params)
    {
        //POST
        /*
        {
        "customerId": "1065000026",
        "orderCode": "6651569366989012994",
        "resultType": "Ok",
        "resultCode": "0",
        "messages": [
        {}
        ]
        }
        */
        return $this->__curlPostCore('customers', $params);
    }
    public function CreateAccount($params)
    {
        //POST
        /*
        {
        "AccountId": "1065000000000000016",
        "orderCode": "6651642579001540611",
        "resultType": "Ok",
        "resultCode": "0",
        "messages": [
        {}
        ]
        }
        */
        return $this->__curlPostCore('accounts', $params);
    }
    public function usage($params)
    {
        /*
        {
        "usages": [
        {
        "from": "447410640162",
        "to": "",
        "date": "2018-12-20T07:29:29.000Z",
        "duration": "52",
        "amount": 0,
        "serviceType": "DATA"
        },
        {
        "from": "447410640162",
        "to": "447410640163",
        "date": "2018-12-20T08:01:32.000Z",
        "duration": "60",
        Pareteum REST APIs (UK )
        Page 10 of 17
        January 2019
        "amount": 0,
        "serviceType": "VOICE_MO"
        },
        {
        "from": "447410640162",
        "to": "447410640163",
        "date": "2018-12-20T08:01:30.000Z",
        "duration": "1",
        "amount": 0,
        "serviceType": "SMS_MO"
        }
        ],
        "statusCode": 0,
        "message": "Success"
        }
        */
        return $this->__curlGet('superapi', 'usage', $params);
    }
    public function topup($params)
    {
        //POST
        /*
        {
        resultCode = '0',
        resultType = 'SUCCESS',
        messages = 'Top-Up added successfully'
        }*/
        return $this->__curlPostSuperApi('topup', $params);
    }

    public function swap_portin_msisdn($subscriptionid, $params){

        //subscriptions/1065075652/msisdn?mvno=902420&from=447419765614&to=447421966450
              //  $data = "subscriptions/".$subscriptionid.""

            return $this->__curlPatchSuperApi("subscriptions/".$subscriptionid."/msisdn", false, $params);


    }
    public function swap($params)
    {
        //POST
        /*
      {
        "msisdn": "34603045155",
        "newIccId": "1000200030004000206",
        "oldIccId": "1000200030004000526",
        "externalReference": "1",
        "channel": "TEUM_US",
        "comments": "2.4.0"
        }
        */
        return  $this->__curlPostCore('sims/swap', $params);
        // return $this->__curlPostSuperApi('topup', $params);
    }


    public function acceptPortOut($params)
    {
        //PATCH
        /*
        {
          "centralNodePortabilityId": "34698900154",
          "externalReference": "1",
          "channel": "TEUM_US",
          "comments": "2.4.0"
        }
        */
        return  $this->__curlPatchCore('/msisdns/acceptPortOut', $params);
        // return $this->__curlPostSuperApi('topup', $params);
    }

    public function rejectPortOut($params)
    {
        //PATCH
        /*
        {
          "centralNodePortabilityId": "34698900154",
          "rejectionCause": "RECH_ICCID",
          "externalReference": "1",
          "channel": "TEUM_US",
          "comments": "2.4.0"
        }
        */
        return  $this->__curlPatchCore('/msisdns/rejectPortOut', $params);
        // return $this->__curlPostSuperApi('topup', $params);
    }


    public function GetSubscription($params)
    {
        /*
        {
        "CustomerOrderId": "1050000075",
        "Subscription": {
        "CustomerId": "1065000026",
        "SubscriptionId": "1050000046",
        "AccountId": "1065000000000000016",
        "StartDate": "2019-01-28T21:36:19.6937347+01:00",
        "EndDate": {},
        "Status": "Active",
        "StatusReason": {},
        "Promotions": {},
        "Services": [
        {
        "ServiceId": "1050000019",
        "Category": "CORESERVERCHARGING",
        "Status": "active",
        "ExternalId": {},
        "StartDate": "2019-01-28T21:36:17.9456558+01:00",
        "EndDate": {},
        "ServiceCharacteristics": [
        null
        ]
        }
        ],
        "Products": [
        {
        "ProductId": "1003000061",
        "ProductOfferingId": "1019000090",
        "SubscriptionProductAssnId": "1050000000000000070",
        "ProductChargePurchaseId": "1006000026",
        Pareteum REST APIs (UK )
        Page 15 of 17
        January 2019
        "ProductCharacteristics": [
        null
        ],
        "StartDate": "2019-01-28T21:36:17.9716587+01:00",
        "EndDate": {},
        "RecurringAmount": {
        "Amount": "0",
        "Currency": "GBP"
        },
        "NonRecurringAmount": {
        "Amount": "0",
        "Currency": "GBP"
        },
        "UnbilledBalanceAmount": {
        "Amount": "0",
        "Currency": "GBP"
        }
        }
        ],
        "DeliveryAddress": {
        "Address": "unknown",
        "HouseExtension": {},
        "HouseNo": "unknown",
        "City": "unknown",
        "ZipCode": "unknown",
        "State": "unknown",
        "CountryId": "76",
        "ExternalAddressId": {}
        }
        },
        "orderCode": "6651649266259066881",
        "resultType": "Ok",
        "resultCode": "0",
        "messages": [
        "Order submitted"
        ]
        }
        */
        //POST
        return $this->__curlGetCore('subscriptions', $params);
    }


    public function GetSubscriptionDetail($subscriptionid)
    {
        /*
        {
        "CustomerOrderId": "1050000075",
        "Subscription": {
        "CustomerId": "1065000026",
        "SubscriptionId": "1050000046",
        "AccountId": "1065000000000000016",
        "StartDate": "2019-01-28T21:36:19.6937347+01:00",
        "EndDate": {},
        "Status": "Active",
        "StatusReason": {},
        "Promotions": {},
        "Services": [
        {
        "ServiceId": "1050000019",
        "Category": "CORESERVERCHARGING",
        "Status": "active",
        "ExternalId": {},
        "StartDate": "2019-01-28T21:36:17.9456558+01:00",
        "EndDate": {},
        "ServiceCharacteristics": [
        null
        ]
        }
        ],
        "Products": [
        {
        "ProductId": "1003000061",
        "ProductOfferingId": "1019000090",
        "SubscriptionProductAssnId": "1050000000000000070",
        "ProductChargePurchaseId": "1006000026",
        Pareteum REST APIs (UK )
        Page 15 of 17
        January 2019
        "ProductCharacteristics": [
        null
        ],
        "StartDate": "2019-01-28T21:36:17.9716587+01:00",
        "EndDate": {},
        "RecurringAmount": {
        "Amount": "0",
        "Currency": "GBP"
        },
        "NonRecurringAmount": {
        "Amount": "0",
        "Currency": "GBP"
        },
        "UnbilledBalanceAmount": {
        "Amount": "0",
        "Currency": "GBP"
        }
        }
        ],
        "DeliveryAddress": {
        "Address": "unknown",
        "HouseExtension": {},
        "HouseNo": "unknown",
        "City": "unknown",
        "ZipCode": "unknown",
        "State": "unknown",
        "CountryId": "76",
        "ExternalAddressId": {}
        }
        },
        "orderCode": "6651649266259066881",
        "resultType": "Ok",
        "resultCode": "0",
        "messages": [
        "Order submitted"
        ]
        }
        */
        //POST
        return $this->__curlGetCore('subscriptions/'.$subscriptionid);
    }


    public function AddSubscription($params)
    {
        /*
        {
        "CustomerOrderId": "1050000075",
        "Subscription": {
        "CustomerId": "1065000026",
        "SubscriptionId": "1050000046",
        "AccountId": "1065000000000000016",
        "StartDate": "2019-01-28T21:36:19.6937347+01:00",
        "EndDate": {},
        "Status": "Active",
        "StatusReason": {},
        "Promotions": {},
        "Services": [
        {
        "ServiceId": "1050000019",
        "Category": "CORESERVERCHARGING",
        "Status": "active",
        "ExternalId": {},
        "StartDate": "2019-01-28T21:36:17.9456558+01:00",
        "EndDate": {},
        "ServiceCharacteristics": [
        null
        ]
        }
        ],
        "Products": [
        {
        "ProductId": "1003000061",
        "ProductOfferingId": "1019000090",
        "SubscriptionProductAssnId": "1050000000000000070",
        "ProductChargePurchaseId": "1006000026",
        Pareteum REST APIs (UK )
        Page 15 of 17
        January 2019
        "ProductCharacteristics": [
        null
        ],
        "StartDate": "2019-01-28T21:36:17.9716587+01:00",
        "EndDate": {},
        "RecurringAmount": {
        "Amount": "0",
        "Currency": "GBP"
        },
        "NonRecurringAmount": {
        "Amount": "0",
        "Currency": "GBP"
        },
        "UnbilledBalanceAmount": {
        "Amount": "0",
        "Currency": "GBP"
        }
        }
        ],
        "DeliveryAddress": {
        "Address": "unknown",
        "HouseExtension": {},
        "HouseNo": "unknown",
        "City": "unknown",
        "ZipCode": "unknown",
        "State": "unknown",
        "CountryId": "76",
        "ExternalAddressId": {}
        }
        },
        "orderCode": "6651649266259066881",
        "resultType": "Ok",
        "resultCode": "0",
        "messages": [
        "Order submitted"
        ]
        }
        */
        //POST
        return $this->__curlPostCore('subscriptions', $params);
    }
    public function DeleteSubscription($id)
    {

        //DELETE
        return $this->__curlDeleteCore('subscriptions/'. $id);
    }
    public function UnFreeze($params)
    {
        unset($params['msisdn']);
        return $this->__curlPatchCore('subscriptions/unfreeze', $params);
    }

    public function Freeze($params)
    {
        /*
        {
        "CustomerOrderId": "1050000075",
        "Subscription": {
        "CustomerId": "1065000026",
        "SubscriptionId": "1050000046",
        "AccountId": "1065000000000000016",
        "StartDate": "2019-01-28T21:36:19.6937347+01:00",
        "EndDate": {},
        "Status": "Active",
        "StatusReason": {},
        "Promotions": {},
        "Services": [
        {
        "ServiceId": "1050000019",
        "Category": "CORESERVERCHARGING",
        "Status": "active",
        "ExternalId": {},
        "StartDate": "2019-01-28T21:36:17.9456558+01:00",
        "EndDate": {},
        "ServiceCharacteristics": [
        null
        ]
        }
        ],
        "Products": [
        {
        "ProductId": "1003000061",
        "ProductOfferingId": "1019000090",
        "SubscriptionProductAssnId": "1050000000000000070",
        "ProductChargePurchaseId": "1006000026",
        Pareteum REST APIs (UK )
        Page 15 of 17
        January 2019
        "ProductCharacteristics": [
        null
        ],
        "StartDate": "2019-01-28T21:36:17.9716587+01:00",
        "EndDate": {},
        "RecurringAmount": {
        "Amount": "0",
        "Currency": "GBP"
        },
        "NonRecurringAmount": {
        "Amount": "0",
        "Currency": "GBP"
        },
        "UnbilledBalanceAmount": {
        "Amount": "0",
        "Currency": "GBP"
        }
        }
        ],
        "DeliveryAddress": {
        "Address": "unknown",
        "HouseExtension": {},
        "HouseNo": "unknown",
        "City": "unknown",
        "ZipCode": "unknown",
        "State": "unknown",
        "CountryId": "76",
        "ExternalAddressId": {}
        }
        },
        "orderCode": "6651649266259066881",
        "resultType": "Ok",
        "resultCode": "0",
        "messages": [
        "Order submitted"
        ]
        }
        */
        //POST
        unset($params['msisdn']);
        return $this->__curlPatchCore('subscriptions/freeze', $params);
    }

    public function Freeze_subscription($params)
    {
        unset($params['msisdn']);
        return $this->__curlPatchCore('subscriptions/freeze', $params);
    }
    public function subscriptions_offerings($params)
    {
        //POST
        /*{
        "PurchaseOrder": {
        "CustomerOrderId": "1060000062",
        "CustomerId": "1065000026",
        "ExternalReference": {},
        "Comments": {},
        "Category": {},
        "CreationDate": "2019-01-28T21:48:16.3556442+01:00",
        "CustomerOrderStatus": "Completed",
        "RequestedCompletionDate": {},
        "ExpectedCompletionDate": {},
        "CompletionDate": "2019-01-28T21:48:21.2089244+01:00",
        "OrderItems": {
        "CustomerOrderItem": {
        "CustomerOrderId": "0",
        "CustomerOrderItemId": "0",
        "ExternalOrderId": {},
        "OrderItemAction": "Add",
        "CustomerOrderItemStatus": "Completed",
        "BillingAccountId": "1065000000000000016",
        "OrderedProductOffering": {
        "OrderedProductOffering": {
        "ProductOfferingId": "1019000091",
        "ChargingOptionId": "1006000029",
        "OfferingChildren": {},
        "OrderedProductCharacteristics": {
        "ProductCharacteristic": {
        "Name": "MSISDN",
        "Value": "447419765843"
        }
        },
        "SubscriptionProductAssnId": "0",
        "Penalties": {}
        }
        },
        "ResourceOrders": {},
        "SubscriptionId": "1050000046",
        "ParentSubscriptionId": "0",
        "ServiceAddress": {
        "Address": "unknown",
        "HouseExtension": {},
        "HouseNo": "unknown",
        "City": "unknown",
        "ZipCode": "unknown",
        "State": "unknown",
        "CountryId": "76",
        "ExternalAddressId": {}
        },
        "Appointment": "1753-01-01T00:00:00",
        "Reason": {}
        }
        }
        },
        "orderCode": "6651652362937040900",
        Pareteum REST APIs (UK )
        Page 17 of 17 January 2019
        "resultType": "Ok",
        "resultCode": "0",
        "messages": [
        "Order submitted"

        ]
        }
        */
        return $this->__curlPostCore('subscriptions/offerings', $params);
    }
    public function UpdateCustomer($params)
    {
        return $this->__curlPatchCore('customers', $params);
    }
    /*
    public function usage(){


    }

    public function usage(){


    }

    public function usage(){


    }

    public function usage(){


    }
    */

    private function __curlDeleteCore($method)
    {
        $url  = $this->params->arta_url . $this->params->arta_version . '/' . $method . '?MVNO=' . $this->params->teum_mvnoid;
        //  return array('test'=> $url);
        //Specify the username and password using the CURLOPT_USERPWD option.
        $curl = curl_init();
        curl_setopt_array($curl, array(
             CURLOPT_URL => $url,
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_TIMEOUT => 120,
             CURLOPT_CUSTOMREQUEST => "DELETE",
             CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic ".$this->params->apiKey,
                 "cache-control: no-cache"
             )
         ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        //Check for errors.
        if ($err) {
            //If an error occured, throw an Exception.
            throw new Exception(curl_error($curl));
        }
        //Print out the response.
        return json_decode($response);
    }
    private function __curlPostCore($method, $payload, $t = false)
    {
        //print_r(json_encode($payload));
        /*if ($t)
        {
            foreach ($t as $key => $val)
            {
                $p .= '&' . $key . '=' . $val;
            }
        }
        */

        $url  = $this->params->arta_url  . $this->params->arta_version . '/' . $method . '?MVNO=' . $this->params->teum_mvnoid;

        log_message('error', 'Pareteum URL: '.$url);
        //file_put_contents('/tmp/url', $url);
        log_message('error', 'Payload: '.json_encode($payload));
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_POST => true,
            CURLOPT_VERBOSE => false,
            CURLOPT_POSTFIELDS => json_encode($payload),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic ".$this->params->apiKey,
                "cache-control: no-cache"
            )
        ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        //Check for errors.
        if ($err) {
            print_r($err);
            //If an error occured, throw an Exception.
            throw new Exception($err);
        }
        //Print out the response.

        return json_decode($response);
    }

    private function __curlGetCore($method, $t = false)
    {
        //print_r(json_encode($payload));
        if ($t) {
            foreach ($t as $key => $val) {
                $p .= '&' . $key . '=' . $val;
            }
        } else {
            $p = "";
        }


        $url  = $this->params->arta_url  . $this->params->arta_version . '/' . $method . '?MVNO=' . $this->params->teum_mvnoid.$p;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_VERBOSE => false,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic ".$this->params->apiKey,
                "cache-control: no-cache"
            )
        ));
        $response = curl_exec($curl);


        $err      = curl_error($curl);
        curl_close($curl);
        //Check for errors.
        if ($err) {
            //If an error occured, throw an Exception.
            throw new Exception(curl_error($curl));
        }
        //Print out the response.
        return json_decode($response);
    }

    private function __curlPatchCore($method, $payload, $params = false)
    {
        if ($params) {
            foreach ($params as $key => $val) {
                $p .= '&' . $key . '=' . $val;
            }
        }else{
            $p = '';
        }



        $url  = $this->params->arta_url . $this->params->arta_version . '/' . $method . '?MVNO=' . $this->params->teum_mvnoid.$p;
        //  return array('test'=> $url);
        //Specify the username and password using the CURLOPT_USERPWD option.
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_CUSTOMREQUEST => "PATCH",
            CURLOPT_POSTFIELDS => json_encode($payload),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic ".$this->params->apiKey,
                "cache-control: no-cache"
            )
        ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        //Check for errors.
        if ($err) {
            //If an error occured, throw an Exception.
            throw new Exception(curl_error($curl));
        }
        //Print out the response.
        return json_decode($response);
    }

    private function __curlPostSuperApi($method, $payload, $params = false)
    {
        if ($params) {
            foreach ($params as $key => $val) {
                $p .= '&' . $key . '=' . $val;
            }
        }else{
            $p = '';
        }

        $url  = trim('https://wholesale.api.pareteum.cloud/v1/superapi/' . $method . '?mvno=' . $this->params->teum_mvnoid.$p);
        //Specify the username and password using the CURLOPT_USERPWD option.
        log_message('error', $url);
        $curl = curl_init();
        curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 120,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_POST => true,
              CURLOPT_POSTFIELDS => json_encode($payload),
              CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic ".$this->params->apiKey,
                "cache-control: no-cache"
              ),
            ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        //Check for errors.

        //Print out the response.
        return json_decode($response);
    }

    private function __curlPatchSuperApi($method, $payload=false, $params = false)
    {
        if ($params) {
            foreach ($params as $key => $val) {
                $p .= '&' . $key . '=' . $val;
            }
        }else{
            $p = '';
        }
        $url  = trim('https://wholesale.api.pareteum.cloud/v1/superapi/' . $method . '?mvno=' . $this->params->teum_mvnoid.$p);
        //Specify the username and password using the CURLOPT_USERPWD option.
        log_message('error', $url);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_CUSTOMREQUEST => "PATCH",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Authorization: Basic ".$this->params->apiKey,
                "cache-control: no-cache"
            )
        ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        //Check for errors.

        //Print out the response.
        return json_decode($response);
    }
    private function __curlGet($type, $method, $params = false)
    {
        $p = '';
        if ($params) {
            foreach ($params as $key => $val) {
                $p .= '&' . $key . '=' . $val;
            }
        }
        if ($type == "superapi") {
            if (!empty($params)) {
                $url = trim('https://wholesale.api.pareteum.cloud/v1/superapi/' . $method . '?mvno=' . $this->params->teum_mvnoid.$p);
            } else {
                $url = trim('https://wholesale.api.pareteum.cloud/v1/superapi/' . $method . '?mvno=' . $this->params->teum_mvnoid);
            }
        } else {
            $url = $this->params->arta_url . $type . '/' . $method . '?mvno=' . $this->params->teum_mvnoid . $p;
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_VERBOSE => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 120,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_USERPWD => $this->params->m_username.':'.$this->params->m_password
            /*CURLOPT_HTTPHEADER => array(
            "Authorization: Basic MTY0NDAwMDIyMDoxMjM0NTY=",
            "Postman-Token: 02395b19-ed7f-4b01-8892-2900c389d03f",
            "cache-control: no-cache"
            )
            */
        ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return array(
                'result' => 'error',
                'message' => "cURL Error #:" . $err
            );
        } else {
            return json_decode($response);
        }
    }
}
