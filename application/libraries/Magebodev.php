<?php
/**
 * Artilium Library for Soap Interfacses
 */
class Magebodev
{
    private $CI;
    private $vars;
    private $setting;
    public function __construct($vars)
    {
        $this->CI =& get_instance();
        $this->db  = $this->CI->load->database('magebo', true);
        $this->db1 = $this->CI->load->database('default', true);
        $this->CI->load->helper('globalvars');
        $this->CI->load->model('Admin_model');
        $this->companyid       = $vars['companyid'];
        $this->error           = array(
            'result' => 'error'
        );
        $this->success         = array(
            'result' => 'success'
        );
        $this->setting = getCompanySetting($vars['companyid']);
    }
    public function set_response($data, $type = false)
    {
        if ($type) {
            return (object) array_merge($this->success, $data);
        } else {
            return (object) array_merge($this->error, array(
                'message' => $data
            ));
        }
    }
    public function addSimBlocking($simcard)
    {
        $this->db->query("UPDATE tblC_SimCard SET bBarPartial=0,bBarFull=1,bActive=0 WHERE cSIMCardNbr=?", array(
            $simcard
        ));
        $this->db->query("EXEC spInsertSIMCardLog '$simcard',602,NULL,NULL,NULL,NULL,'FullyBlocked',NULL");
        return (object) array(
            'result' => 'success'
        );
    }
    public function getMandateId($id){
        $res = array('SEPA_MANDATE' => '', 'IBAN' => '', 'BIC' => '', 'SEPA_MANDATE_SIGNATURE_DATE'=> '', 'SEPA_STATUS' => '');
     $q  = $this->db->query("SELECT a.iAddressDataIndex, a.iTypeNbr, b.cTypeDescription, a.cAddressData FROM tblAddressData a left join tblType b on b.iTypeNbr=a.iTypeNbr where  a.bPreferredOrActive=1 and a.iAddressNbr=? and b.iTypeGroupNbr=?", array($id, 5));
        if($q->num_rows()>0){
          foreach($q->result() as $row){
            if(!empty($row)){
              $res[str_replace(' ', '_', $row->cTypeDescription)] = $row->cAddressData;
            }

          }
          return (object)$res;
        }else{

          return (object) array('SEPA_MANDATE' => '', 'IBAN' => '', 'BIC' => '', 'SEPA_MANDATE_SIGNATURE_DATE'=> '', 'SEPA_STATUS' => '');
        }
      }
    public function GetFinancial($msisdn)
    {
        $q = $this->db->query("SELECT C.cCompany Company,dbo.fnGetFinancialCondition(A.iAddressNbr) FCondition,AD4.cAddressData EmailAddress,AD3.cAddressData VATNumber,AD1.cAddressData TelephoneNumber,AD2.cAddressData GsmNumber,P.iPincode,P.iLocationIndex,L.iAddressNbr,A.cName,A.cStreet,A.iZipCode,A.cCity\
                FROM tblC_Pincode P LEFT JOIN tblLocation L ON L.iLocationIndex=P.iLocationIndex LEFT JOIN tblAddress A ON A.iAddressNbr=L.iAddressNbr \
                LEFT JOIN tblAddressData AD1 ON AD1.iTypeNbr=18 AND AD1.iAddressNbr=A.iAddressNbr LEFT JOIN tblAddressData AD2 ON AD2.iTypeNbr=20 AND AD2.iAddressNbr=A.iAddressNbr \
                LEFT JOIN tblAddressData AD3 ON AD3.iTypeNbr=23 AND AD3.iAddressNbr=A.iAddressNbr LEFT JOIN tblAddressData AD4 ON AD4.iTypeNbr=34 AND AD4.iAddressNbr=A.iAddressNbr LEFT JOIN tblCompany C On C.iCompanyNbr=A.iCompanyNbr where iPincode=?", array(
            $msisdn
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function getClientId($id)
    {
        $q = $this->db->query("select * from tblAddress where iAddressNbr=? and iCompanyNbr=?", array(
            $id,
            $this->companyid
        ));
        $s = $this->db->query("select b.iTypeGroupNbr,b.cTypeDescription,a.cAddressData,bPreferredOrActive from tblAddressData a  left join tblType b on b.iTypeNbr=a.iTypeNbr  where a.iAddressNbr=? and b.iTypeGroupNbr in (4,5,6)", array(
            $id
        ));
        if ($s->num_rows() > 0) {
            foreach ($s->result() as $row) {
                if ($row->bPreferredOrActive == "1") {
                    $data[str_replace(' ', '', $row->cTypeDescription)] = $row->cAddressData;
                }
            }
        } else {
            $data = array();
        }
        if ($q->num_rows() > 0) {
            return $this->set_response(array_merge($q->row_array(), $data), true);
        } else {
            return $this->set_response('No client found for id ' . $id, false);
        }
    }
    public function hasInvoice($mageboid, $vat){
        if($vat){
            return true;
        }else{
            $q = $this->db->query("select * from tblInvoice where iAddressNbr=?", array(
                $mageboid
            ));
            if ($q->num_rows() > 0) {
                return "yes";
            }else{

                return "no";
            }

        }


    }
    public function GetPricingIndexPrice($index){

        $q = $this->db->query("select * from tblGeneralPricing where iGeneralPricingIndex=?",array($index));

        return $q->row()->mUnitPrice;
    }

    public function updatePricingItem($index, $price){
        if($index){
            $this->db->where('iGeneralPricingIndex', $index);
            $this->db->update('tblPricedItems', array('mUnitPrice' => $price));
            return "success\n";
        }else{

            return "error\n";
        }


    }
    public function getInvoices($iAddressNbr)
    {
        $q = $this->db->query("select * from tblInvoice where iAddressNbr=?", array(
            $iAddressNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        }else{

            return false;
        }
    }
    public function getInvoice($iInvoiceNbr)
    {
        $q = $this->db->query("select * from tblInvoice where iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        }else{
            return false;
        }
    }
    public function getCompanyName($companyid)
    {
        $q = $this->db->query("SELECT cCompany from tblCompany where iCompanyNbr=?", array(
            $companyid
        ));
        insert_query_log(array(
            'funct' => 'getCompanyName',
            'description' => "SELECT cCompany from tblCompany where iCompanyNbr=" . $companyid
        ));
        return $q->row()->cCompany;
    }
    public function getFlag($type)
    {
        $q = $this->db->query("EXEC spGetFlag '" . $type . "'");
        return $q->row()->bFlagStatus;
    }
    public function updateFlag($type, $status)
    {
        $this->db->query("EXEC spUpdateFlag '" . $type . "', '" . $status . "'");
    }
    public function GetLastInvoice($companyname, $type)
    {
        $q      = $this->db->query("EXEC spGetLastInvoiceDate NULL, '" . $companyname . "', '".$type."'");
        $result = explode(' ', $q->row()->dLastInvoiceDate);
        return $result[0];
    }
    public function GetNewAddressNbr($companyname)
    {
        $q = $this->db->query("EXEC spGetNewAddressNbr '" . $companyname . "'");
        insert_query_log(array(
            'funct' => 'getCompanyName',
            'description' => "EXEC spGetNewAddressNbr '" . $companyname . "'"
        ));
        return $q->row()->iNewAddressNbr;
    }
    public function getNewInvoiceNbr($companyname)
    {
        $q = $this->db->query("EXEC spGetNewInvoiceNbr '" . $companyname . "'");
        return $q->row()->iNewInvoiceNbr;
    }
    public function getCountryIndex($code)
    {
        $q = $this->db->query("select * from tblCountry where cCountryCode=?", array(
            $code
        ));
        return $q->row()->iCountryIndex;
    }
    public function AddClient($cd)
    {
        try {
            $duedays = 14;
            $iCompanyNbr = $cd['companyid'];
            // return (object) array('result' => 'success', 'iAddressNbr' => $iCompanyNbr);
            $cCompany    = $this->getCompanyName($iCompanyNbr);
            $iAddressNbr = $this->GetNewAddressNbr($cCompany);
            switch ($cd['language']) {
                case "dutch":
                    $cInternalLanguage = "Dutch";
                    break;
                case "english":
                    $cInternalLanguage = "English";
                    break;
                case "french":
                    $cInternalLanguage = "French";
                    break;
                case "german":
                    $cInternalLanguage = "German";
                    break;
                default:
                    $cInternalLanguage = "Dutch";
            }
            if (!empty($cd['companyname']) && !empty($cd['vat'])) {

                $cName = utf8_decode(html_entity_decode(str_replace("'", "''", ucfirst($cd['companyname']))));
            } else {
                $ccc  = (object) $cd;
                $cName = utf8_decode(html_entity_decode(format_name_address($ccc)));
                //$cName = utf8_decode(html_entity_decode($));
                unset($ccc);
            }
            $cStreet   = utf8_decode(html_entity_decode(str_replace("'", "''", $cd['address1'])));
            $countries = getCountries();
            foreach ($countries as $key => $c) {
                if ($key == $cd['country']) {
                    $cCountry = strtoupper($c);
                }
            }
            if(!empty($cd['payment_duedays'])){
                if($duedays >1){
                    $duedays = $cd['payment_duedays'];
                }

            }
            $iZipCode   = explode(' ', $cd['postcode']);
            $cZipSuffix = "";
            $cCity      = utf8_decode(html_entity_decode(str_replace("'", "''", $cd['city'])));
            $cRemark    = "";
            $bCustomer  = 1;
            $bAgent     = 0;
            $bCarrier   = 0;
            $bIncasso   = 0;
            $bInvoice   = 0;
            $bLocation  = 1;
            $bSupplier  = 0;
            $bType      = 0;
            $cCompany   = $cCompany;
            $zc         = explode(' ', $cd['postcode']);
            if (count($zc) == 2) {
                if (is_numeric($zc[0])) {
                    $iZipCode   = $zc[0];
                    $cZipSuffix = $zc[1];
                } else {
                    $iZipCode   = "null";
                    $cZipSuffix = $cd['postcode'];
                }
            } else {
                if (is_numeric($cd['postcode'])) {
                    $iZipCode   = $cd['postcode'];
                    $cZipSuffix = "";
                } else {
                    $iZipCode   = "null";
                    $cZipSuffix = $cd['postcode'];
                }
            }
            if ($iZipCode == "null") {
                $this->db->query("EXEC spInsertAddress $iAddressNbr,'$cName','$cStreet','$cCountry',null,'$cZipSuffix','$cCity','$cRemark',$bCustomer,$bAgent,$bCarrier,$bIncasso,$bInvoice,$bLocation,$bSupplier,$bType, '$cCompany','$cInternalLanguage'");
            } else {
                $this->db->query("EXEC spInsertAddress $iAddressNbr,'$cName','$cStreet','$cCountry',$iZipCode,'$cZipSuffix','$cCity','$cRemark',$bCustomer,$bAgent,$bCarrier,$bIncasso,$bInvoice,$bLocation,$bSupplier,$bType, '$cCompany','$cInternalLanguage'");
            }
            insert_query_log(array(
                'funct' => 'AddClient',
                'description' => "EXEC spInsertAddress $iAddressNbr,'$cName','$cStreet','$cCountry',$iZipCode,'$cZipSuffix','$cCity','$cRemark',$bCustomer,$bAgent,$bCarrier,$bIncasso,$bInvoice,$bLocation,$bSupplier,$bType, '$cCompany','$cInternalLanguage'"
            ));
            if (!empty($cd['mvno_id']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 6, 'MVNO CUSTOMER LOOKUP', '" . $cd['mvno_id'] . "', '',1");
            }
            if (!empty($cd['vat']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'VAT NUMBER', '" . $cd['vat'] . "', '',1");
            }
            if (strlen($cd['invoice_email']) == "yes") {
                $this->db->query("EXEC spUpdateInsertInvoiceConditionEmail " . $iAddressNbr . ", '" . $cd['email'] . "'");
                $this->db->query("EXEC spUpdateInvoiceCondition " . $iAddressNbr . ", ".$duedays.", 'NORMAL', 0, 'DIGITAL ( WHMCS )', 1, 0, 0, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, 'LOCAL', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            }else{
                $this->db->query("EXEC spUpdateInvoiceCondition " . $iAddressNbr . ", ".$duedays.", 'NORMAL', 0, 'PAPER ( WHMCS )', 1, 0, 0, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, 'LOCAL', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            }
            if (strlen($cd['nationalnr']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 6, 'IDENTITY CARD NUMBER', '" . $cd['nationalnr'] . "', '',1");
            }
            if (strlen($cd['companyname']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'COMPANY NAME', ?, '',1", array(
                    utf8_decode(html_entity_decode($cd['companyname']))
                ));
            }
            if (strlen($cd['phonenumber']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 4, 'TELEPHONENUMBER', '" . $cd['phonenumber'] . "', '',1");
            }
            if (strlen($cd['iban']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'IBAN', '" . $cd['iban'] . "', '',1");
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'BIC', '" . $cd['bic'] . "', '',1");
            }
            if (!empty($cd['date_birth']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'BIRTHDATE', '" . $cd['date_birth'] . "', '',1");
            }
            return (object) array(
                'result' => 'success',
                'iAddressNbr' => $iAddressNbr
            );
        }
        catch (Exception $e) {
            return (object) array(
                'result' => 'error',
                'message' => $e->getMessage()
            );
        }
    }
    public function update_name($mageboid, $name)
    {
        $this->db->where('iAddressNbr', $mageboid);
        $this->db->update('tblAddress', array(
            'cName' => $name
        ));
        return $this->db->affected_rows();
    }
    public function getUnpaidInvoices($date){

        $q = $this->db->query("SELECT a.*
        FROM GDC_ERP.dbo.tblInvoice a left join GDC_ERP.dbo.tblAddress b on b.iAddressNbr =a.iAddressNbr
        where b.iCompanyNbr=?
        and a.dInvoiceDate >= ?
        and a.iInvoiceStatus != 54
        and a.iInvoiceType = 40 order by a.iInvoiceNbr asc", array($this->companyid, $date));

        if($q->num_rows()>0){

            return $q->result();
        }else{
            return false;
        }

    }
    public function updateClient($clientid, $data)
    {
        if ($clientid > 0) {
            $this->db->where('iAddressNbr', $clientid);

                $cc = (object) $data;
                $udata['cName'] =  format_name_address($cc);
                    unset($cc);

            if ($data['country'] == "NL") {
                $customerType = 49;
                $pcod         = explode(' ', $data['postcode']);
            } else {
                $eu_countries = array(
                    'AT',
                    'BE',
                    'HR',
                    'BG',
                    'CY',
                    'CZ',
                    'DK',
                    'EE',
                    'FI',
                    'FR',
                    'DE',
                    'GR',
                    'HU',
                    'IE',
                    'IT',
                    'LV',
                    'LT',
                    'LU',
                    'MT',
                    'NL',
                    'PL',
                    'PT',
                    'RO',
                    'SK',
                    'SI',
                    'ES',
                    'SE',
                    'GB'
                );
                if (in_array($data['country'], $eu_countries)) {
                    $customerType = 50;
                } else {
                    $customerType = 51;
                }
                $pcod = array(
                    $data['postcode'],
                    ''
                );
            }
            $udata['cStreet']       = $data['address1'];
            $udata['cCity']         = $data['city'];
            $udata['iZipCode']      = $pcod[0];
            $udata['cZIPSuffix']    = $pcod[1];
            $udata['iCountryIndex'] = $this->getCountryIndex($data['country']);
            if(!empty($data['payment_duedays'])){
                if($data['payment_duedays']>1){
                    $duedays = $data['payment_duedays'];
                }else{
                    $duedays = 14;
                }

            }else{
                $duedays = 14;
            }

            $this->db->update('tblAddress', $udata);
            $this->updateInvoiceZone($clientid, $customerType);
            //Update address data
            if ($data['invoice_email'] == "yes") {
                $this->UpdateInsertInvoiceConditionEmail($clientid, $data['invoice_email']);
                $this->db->query("EXEC spUpdateInvoiceCondition " . $clientid . ", ".$duedays.", 'NORMAL', 0, 'DIGITAL ( WHMCS )', 1, 0, 0, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, 'LOCAL', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            }else{
                $this->db->query("EXEC spUpdateInvoiceCondition " . $clientid . ", ".$duedays.", 'NORMAL', 0, 'PAPER ( WHMCS )', 1, 0, 0, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, 'LOCAL', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            }

            if (!empty($data['vat']) > 0) {
                $this->db->query("EXEC spInsertAddressData " . $clientid . ", 5, 'VAT NUMBER', '" . $data['vat'] . "', '',1");
            }
            if(!empty($data['companyname'])){
                $res[] = $this->updateAddressdata($clientid, 947, $data['companyname']);
            }
            $res[] = $this->updateAddressdata($clientid, 18, $data['phonenumber']);
            $res[] = $this->updateAddressdata($clientid, 34, $data['email']);
            // $this->updateAddressdata($clientid, 936, $data['email']);
            $res[] = $this->updateAddressdata($clientid, 936, strtoupper($data['gender']));
            $res[] = $this->updateAddressdata($clientid, 896, $data['nationalnr']);
            $res[] = $this->updateAddressdata($clientid, 494, $data['date_birth']);
            //$res[] = $this->updateAddressdata($clientid, 2098, $data['iban']);
            //$res[] = $this->updateAddressdata($clientid, 2099, $data['bic']);
            $res[] = $this->updateAddressdata($clientid, 736, trim($data['mvno_id']));
            return (object) array(
                'result' => 'success',
                $res
            );
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Empty ' . $clientid
            );
        }
    }
    public function addSubscription($data)
    {
    }

    public function updateMvnoId($data)
    {
        $this->updateAddressdata($data['id'], 736, trim($data['mvno_id']));

    }

    public function getInvoiceCdr($table, $iInvoiceNbr){
       // $this->db->select('iPincode, iCallerNbr, iDestinationNbr, convert(dCallDate, getdate(), 120) as dCallDate, iRealDuration, iInvoiceDuration, cDialedNumber, mSale, mInvoiceSale, iCallPriceNbr, iInvoiceNbr, dExportDate, dMonthExportDate');
       // $this->db->where('iInvoiceNber', $iInvoiceNbr);
       $query = "select iPincode, iCallerNbr, iDestinationNbr,dCallDate, iRealDuration, iInvoiceDuration, cDialedNumber, mSale, mInvoiceSale, iCallPriceNbr, iInvoiceNbr, dExportDate, dMonthExportDate from $table where iInvoiceNbr=$iInvoiceNbr";

       $s  = "select P.iPincode,P.cInvoiceReference, convert(varchar,C.dCallDate,120) as dCallDate, C.iInvoiceDuration, C.cDialedNumber, T.cCallTranslation,
       C.mInvoiceSale as mInvoiceSale,CP.mPrice as mPrice, C.iInvoiceNbr, IGD.cInvoiceGroupDescription,A.iAddressNbr,T.iDestinationNbr,IG.iInvoiceGroupNbr
       ,A.iCompanyNbr
from ".$table." C
left join tblC_Pincode P on P.iPincode = C.iPincode
left join tblLocation L on L.iLocationIndex = P.iLocationIndex
left join tblAddress A on A.iAddressNbr = L.iAddressNbr
left join tblC_CallPriceList CP on C.iCallPriceNbr = CP.iCallPriceNbr
left join tblInvoiceGroup IG on CP.iInvoiceGroupNbr = IG.iInvoiceGroupNbr
left join tblInvoiceGroupDescription IGD on IG.iInvoiceGroupNbr = IGD.iInvoiceGroupNbr and IGD.iLanguageIndex = A.iLanguageIndex
left join tblC_Destination D ON ( D.iDestinationNbr = C.iDestinationNbr )
left join dbo.tblC_CallTranslation T ON ( T.iDestinationNbr = D.iDestinationNbr ) and T.iLanguageIndex = A.iLanguageIndex
where T.iCallerNbr = C.iCallerNbr
and C.iInvoiceNbr = ".$iInvoiceNbr."
order by C.iPincode, C.dCallDate";
       // echo $s;
       $q = $this->db->query($s);
        if($q->num_rows()>0){
            return $q->result();
        }else{

            return false;
        }


    }

    public function getInvoiceCdrSplit($table, $iInvoiceNbr){
        // $this->db->select('iPincode, iCallerNbr, iDestinationNbr, convert(dCallDate, getdate(), 120) as dCallDate, iRealDuration, iInvoiceDuration, cDialedNumber, mSale, mInvoiceSale, iCallPriceNbr, iInvoiceNbr, dExportDate, dMonthExportDate');
        // $this->db->where('iInvoiceNber', $iInvoiceNbr);
        $query = "select iPincode, iCallerNbr, iDestinationNbr,dCallDate, iRealDuration, iInvoiceDuration, cDialedNumber, mSale, mInvoiceSale, iCallPriceNbr, iInvoiceNbr, dExportDate, dMonthExportDate from $table where iInvoiceNbr=$iInvoiceNbr";
        $res =array();
        $s  = "select P.iPincode,P.cInvoiceReference, convert(varchar,C.dCallDate,120) as dCallDate, C.iInvoiceDuration, C.cDialedNumber, T.cCallTranslation,
        C.mInvoiceSale as mInvoiceSale,CP.mPrice as mPrice, C.iInvoiceNbr, IGD.cInvoiceGroupDescription,A.iAddressNbr,T.iDestinationNbr,IG.iInvoiceGroupNbr
        ,A.iCompanyNbr
 from ".$table." C
 left join tblC_Pincode P on P.iPincode = C.iPincode
 left join tblLocation L on L.iLocationIndex = P.iLocationIndex
 left join tblAddress A on A.iAddressNbr = L.iAddressNbr
 left join tblC_CallPriceList CP on C.iCallPriceNbr = CP.iCallPriceNbr
 left join tblInvoiceGroup IG on CP.iInvoiceGroupNbr = IG.iInvoiceGroupNbr
 left join tblInvoiceGroupDescription IGD on IG.iInvoiceGroupNbr = IGD.iInvoiceGroupNbr and IGD.iLanguageIndex = A.iLanguageIndex
 left join tblC_Destination D ON ( D.iDestinationNbr = C.iDestinationNbr )
 left join dbo.tblC_CallTranslation T ON ( T.iDestinationNbr = D.iDestinationNbr ) and T.iLanguageIndex = A.iLanguageIndex
 where T.iCallerNbr = C.iCallerNbr
 and C.iInvoiceNbr = ".$iInvoiceNbr."
 order by C.iPincode, C.dCallDate";
        // echo $s;
        $q = $this->db->query($s);
         if($q->num_rows()>0){
            foreach($q->result() as $row){
$res[$row->iPincode][] = $row;
            }
            return $res;
         }else{

             return false;
         }


     }

    public function getDataCdr($iInvoiceNbr){
        $res = array();
       $cdr =  $this->db->query("exec sp_DataUsageTMNL ".$iInvoiceNbr);
       if($cdr->num_rows()>0){

       foreach($cdr->result() as $row){


        $res[$row->iPincode][] = $row;


       }
        return $res;
       }else{

        return false;
       }

    }
    public function UpdatePricing($data){
        $this->db->where('iGeneralPricingIndex', $data->iGeneralPricingIndex);
        $this->db->where('iAddressNbr', $data->iAddressNbr);
        $this->db->update('tblGeneralPricing', array('mUnitPrice' => $data->mUnitPrice));
        return $this->db->affected_rows();
    }
    public function getInvoiceJobNbr($iInvoiceNbr){

        $q = $this->db->query("SELECT iInvoicedCallsIndex, iInvoiceJobNbr, iInvoiceNbr, iNbrOfRecords, iInvoicePrintCounter, iCallsPrintCounter, iNbrOfInvoicePages, iNbrOfMarketingPages, iNbrOfCallPages, dLastUpdate, iLastUserNbr
        FROM tblC_InvoicedCalls where iInvoiceNbr=?", array($iInvoiceNbr));
            if($q->num_rows()>0){
                return $q->row()->tblC_InvoicedCallFileReference;
        }else{
            return false;
        }

    }

    public function getInvoiceCdrFile($iInvoiceNbr){

        $q = $this->db->query("SELECT iInvoicedCallFileReference, iInvoiceNbr, cCallFileName, dLastUpdate, iLastUserNbr
        FROM tblC_InvoicedCallFileReference where iInvoiceNbr=?", array($iInvoiceNbr));
 if($q->num_rows()>0){
    $filename = $q->row()->cCallFileName;

    return $this->getInvoiceCdr($filename, $iInvoiceNbr);


}else{
    return false;
}

    }


    public function getInvoiceCdrFileSplit($iInvoiceNbr){
        $res = array();
        $q = $this->db->query("SELECT iInvoicedCallFileReference, iInvoiceNbr, cCallFileName, dLastUpdate, iLastUserNbr
        FROM tblC_InvoicedCallFileReference where iInvoiceNbr=?", array($iInvoiceNbr));
 if($q->num_rows()>0){
    $filename = $q->row()->cCallFileName;

  return $this->getInvoiceCdrSplit($filename, $iInvoiceNbr);
}else{
    return false;
}

    }


    public function updateAddressdata($iAddressNbr, $iTypeNbr, $Value)
    {
        if ($iAddressNbr > 0) {
            $this->db->where('iAddressNbr', $iAddressNbr);
            $this->db->where('iTypeNbr', $iTypeNbr);
            $this->db->where('bPreferredOrActive', 1);
            $this->db->update('tblAddressData', array(
                'cAddressData' => $Value
            ));
            if ($this->db->affected_rows() > 0) {
                return (object) array(
                    'result' => 'success'
                );
            } else {
                if ($iTypeNbr == 736) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 6, 'MVNO CUSTOMER LOOKUP', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 23) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'VAT NUMBER', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 896) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 6, 'IDENTITY CARD NUMBER', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 947) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'COMPANY NAME', '" . utf8_decode(html_entity_decode($Value)) . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 18) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 4, 'TELEPHONENUMBER', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 2098) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'IBAN', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 2099) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'BIC', '" . $Value . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                } elseif ($iTypeNbr == 494) {
                    $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 7, 'BIRTHDATE', '" . date("d/m/Y", strtotime($Value)) . "', '',1");
                    return (object) array(
                        'result' => 'success'
                    );
                }
                return (object) array(
                    'result' => 'error'
                );
            }
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Empty ' . $iAddressNbr
            );
        }
    }
    public function getInvoiceDetail($iInvoiceNbr){

        $q = $this->db->query("select a.iInvoiceGroupNbr,b.cInvoiceDetailDescription from tblInvoiceDetail a left join tblInvoiceDetailDescription b on b.iInvoiceDetailDescriptionNbr=a.iInvoiceDetailDescriptionNbr where a.iInvoiceNbr=?", array($iInvoiceNbr));
        return $q->result();

    }
    public function updateInvoiceZone($iAddressNbr, $customerType)
    {
        if ($iAddressNbr > 0) {
            $this->db->query("Update tblInvoiceCondition set iCustomerOrigin = ? where iAddressNbr = ?", array(
                $customerType,
                $iAddressNbr
            ));
            if ($this->db->affected_rows() > 0) {
                return (object) array(
                    'result' => 'success'
                );
            } else {
                return (object) array(
                    'result' => 'error'
                );
            }
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Empty ' . $iAddressNbr
            );
        }
    }
    public function UpdateInsertInvoiceConditionEmail($iAddressNbr, $invoice_email)
    {
        if ($iAddressNbr > 0) {
            $this->db->query("exec spUpdateInsertInvoiceConditionEmail " . $iAddressNbr . ", '" . $invoice_email . "'");
            if ($this->db->affected_rows() > 0) {
                return (object) array(
                    'result' => 'success'
                );
            } else {
                return (object) array(
                    'result' => 'error'
                );
            }
        } else {
            return (object) array(
                'result' => 'error',
                'message' => 'Empty ' . $iAddressNbr
            );
        }
    }
    public function ProcessPricing($mobile, $iAddressNbr, $addonid, $createMagebo, $recurring = false, $contract = false)
    {
        $addon  = getMageboProductAddons($addonid);

            $domain = trim($mobile->details->msisdn);

            if(!$contract){

               $contract = $mobile->date_contract;
            }
            if(!$recurring){

                $prorata = "0";
            }
        //send this via api because php7.X does not support it
        if ($createMagebo) {

        if(substr($domain, 0, 2) == "32"){
                $this->AddBundlePricing(array(
                    'number' => substr(trim($mobile->details->msisdn), 2),
                    'date_contract' => $contract
                ), $addonid);
        }else{
            $this->AddBundlePricing(array(
                'number' => $mobile->details->msisdn,
                'date_contract' => $contract
            ), $addonid);
        }

        }


        if(substr($domain, 0, 2) == "32"){

           $iPincode = substr($domain, 2);
           $number  = "0" . substr($iPincode, 0, 3) .'/'. substr($iPincode, 3, 2) . "." . substr($iPincode, 5, 2) . "." . substr($iPincode, 7, 2);
        }else{
            $number = "0" . $domain[2] . "-" . $domain[3] . $domain[4] . " " . $domain[5] . "" . $domain[6] . " " . $domain[7] . "" . $domain[8] . " " . $domain[9] . $domain[10];

        }


        $prorata = "1";
       if(!$recurring){
           $recurring = 1;
       }
        if ($addon->bundle_type == "option") {
            $this->x_magebosubscription_addPricing($iAddressNbr, $addon->GPInvoiceGroupNbr, 1, $addon->recurring_subtotal, $recurring, $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), date('m/d/Y'), date('m'), date('Y'), 1, 0, $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), $addon->GPcRemark . ' ' . $number . ' - ' . date('d/m/Y'), $prorata);
        } elseif ($addon->bundle_type == "others") {
        } else {
            /* $this->x_magebosubscription_addPricing($iAddressNbr,
            $addon->GPInvoiceGroupNbr,
            1,
            $addon->recurring_subtotal,
            600,
            $addon->GPcRemark . ' ' . $number . ' ' . date('d/m/Y'),
            date('m/d/Y'),
            date('m'),
            date('Y'),
            1,
            0,
            $addon->GPcRemark . ' ' . $number . ' ' . date('d/m/Y'),
            $addon->GPcRemark . ' ' . $number . ' ' . date('d/m/Y'),
            0);
            */
        }
    }


    public function ProcessPricingExtraBundle($mobile, $iAddressNbr, $addonid, $createMagebo, $price_incl_vat, $contract_start, $recurring)
    {
        $addon  = getAddonInformation($addonid);
        if(substr($domain, 0, 2) == "32"){
            $domain = trim($mobile->details->msisdn);
        }else{
            $domain = trim($mobile->details->msisdn);
        }

        //send this via api because php7.X does not support it
        if ($createMagebo) {

        if(substr($domain, 0, 2) == "32"){
                $this->AddBundlePricing(array(
                    'number' => substr(trim($mobile->details->msisdn), 2),
                    'date_contract' => $contract_start[1].'-'.$contract_start[2].'-'.$contract_start[0]
                ), $addonid);
        }else{
            $this->AddBundlePricing(array(
                'number' => $mobile->details->msisdn,
                'date_contract' => $contract_start[1].'-'.$contract_start[2].'-'.$contract_start[0]
            ), $addonid);
        }

        }


        if(substr($domain, 0, 2) == "32"){

           $iPincode = substr($domain, 2);
           $number  = "0" . substr($iPincode, 0, 3) .'/'. substr($iPincode, 3, 2) . "." . substr($iPincode, 5, 2) . "." . substr($iPincode, 7, 2);
        }else{
            $number = "0" . $domain[2] . "-" . $domain[3] . $domain[4] . " " . $domain[5] . "" . $domain[6] . " " . $domain[7] . "" . $domain[8] . " " . $domain[9] . $domain[10];

        }



            $this->x_magebosubscription_addPricing($iAddressNbr, $addon->GPInvoiceGroupNbr, 1, exvat4('21', $price_incl_vat), $recurring, $addon->GPcRemark . ' ' . $number, $contract_start[1].'/'.$contract_start[2].'/'.$contract_start[0], $contract_start[1], $contract_start[0], 1, 0, $addon->GPcRemark . ' ' . $number, $addon->GPcRemark . ' ' . $number, 1);

    }


    public function addExtraPricing($client, $option, $contract){
       $id =  $this->x_magebosubscription_addPricing($client->mageboid,
        369,
        1,
        exvat4($client->vat_rate, $option->recurring_total),
        $option->terms,
        $option->name,
        $contract,
        substr($contract, 0, 2),
        substr($contract, -4),
        1,
        0,
        $option->name,
        $option->name,
        0);

        return $id;
    }
    public function addSetupFee($mobile){
        $setupfee = getPricing($mobile->packageid, 'setup');
        if($setupfee > 0){


        }
    }
    public function getClientInvoices($mageboid)
    {
    }
    public function ChangeSimLanguage($msisdn, $lang)
    {
        //$lang (1 - English, 2 - French, 3 - Dutch )
        if ($lang == "3") {
            // Dutch
            $langid = 592;
        } elseif ($lang == "2") {
            // French
            $langid = 593;
        } else {
            // English
            $langid = 594;
        }
        $this->db->where('iPincode', trim($msisdn));
        $this->db->update('tblC_SIMCard', array(
            'iVoiceMailLanguage' => $langid
        ));
        return $this->db->affected_rows();
    }
    public function getInvoiceId($id)
    {
    }
    public function TerminateRequest($service, $date)
    {
        $query = "execute spInsertSIMCardLog '".$service->details->msisdn_sim."', 600, NULL, NULL, NULL, NULL, '', '".$date."'";
        $this->db->query($query); //-- Termination request
    }
    public function TerminateComplete($service, $date)
    {
        sleep(2);
        $date1 = $date . ' ' . date('H-i-s');
        $date2 = str_replace('-', '', $date);
        $this->db->query("execute spInsertSIMCardLog '".$service->details->msisdn_sim."', 613, NULL, NULL, NULL, NULL, '', '$date1'"); //-- Terminated")
        $this->db->query("update tblC_Pincode set dStopDate = ? where iPincode = ?", array(
            $date2,
            $service->details->msisdn
        ));
        if ($this->db->affected_rows() > 0) {
            return array(
                'result' => 'success'
            );
        } else {
            return array(
                'result' => 'error'
            );
        }
    }
    public function addPricingSubscription($mobile, $noDiscount=false)
    {

        if($mobile->iGeneralPricingIndex){
            mail('mail@simson.one','double pricing detected, request cancelled', print_r($mobile, true));

            return 'error';
        }
        $mp                  = array();
        if(substr(trim($mobile->details->msisdn), 0, 2) == "32"){
            $q                   = $this->db->query("select * from tblC_Pincode where iPincode LIKE ?", array(
                substr(trim($mobile->details->msisdn), 2)
            ));
        }else{
            $q                   = $this->db->query("select * from tblC_Pincode where iPincode LIKE ?", array(
                trim($mobile->details->msisdn_sn)
            ));
        }

        $lineIGD             = $q->row_array();
        $pincoderef          = $lineIGD['cInvoiceReference'];
        $sstartday           = substr($mobile->date_contract, 0, 2);
        $product             = getMageboProduct($mobile->packageid);

        $InvoiceGroup        = $product->iInvoiceGroupNbr;
        $d                   = explode('-', $mobile->date_contract);
        $datequery           = "SELECT SD.InvoiceStartDate,
		                            CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)) ContractDate,
		                            STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) CtrDate
		                            ,LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) Lday,
		                            DATEDIFF(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )),
		                            STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) )diff,
		                            DAY(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ))) DaysInMonth,
		                            MONTH(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartMonth,
		                            YEAR(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartYear
		                            FROM
		                            (SELECT '" . $d[2] . '-' . $d[0] . '-' . $d[1] . "' InvoiceStartDate )SD ";
        $resultnew           = $this->db1->query($datequery);
        $row                 = $resultnew->row_array();
        //logActivity($datequery);
        $ContractDate        = $row['ContractDate'];
        $iStartMonth         = $row['StartMonth'];
        $iStartYear          = $row['StartYear'];
        $client              = $this->CI->Admin_model->getClient($mobile->userid);
        $GPcRemark           = $product->GPcRemark;
        $InvoiceDescription  = $product->iInvoiceDescription;
        $cRemark             = sprintf("$GPcRemark", $pincoderef);
        $cInvoiceDescription = sprintf("$InvoiceDescription", $pincoderef);
        $cInvoiceReference   = $pincoderef;
        //Now we create the monthly subscription
        $bEnabled            = 1;
        $bussiness            = $this->getInvoiceBussiness($InvoiceGroup);
        $cBusiness            = $bussiness['cBusiness'];
        $cInvoiceGroup        = $bussiness['cInvoiceGroup'];
        $iAddressNbr = $client->mageboid;
        //h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,
        $client      = getClientDetailidbyMagebo($iAddressNbr);
        if ($client->vat_exempt == 1) {
            $vat = 0;
        } else {
            $vat = round($client->vat_rate, 0);
        }

        if($client->language){
            $this->CI->config->set_item('language', $client->language);
        }else{
            $this->CI->config->set_item('language', 'dutch');
        }
        $this->CI->lang->load('admin');
        $PricingAmount       = exvat4($vat, $mobile->recurring);

        if($mobile->addons){
            foreach($mobile->addons as $addn){
                if($addn->recurring_total >0){
                    $others  = "Hardware - ".$pincoderef;
                    if(!$noDiscount){
                     $this->x_magebosubscription_addPricing($client->mageboid, $InvoiceGroup, 1, exvat($vat, $addn->recurring_total), $addn->terms, $addn->name, $ContractDate, $iStartMonth, $iStartYear, $bEnabled, 0, $others, $addn->name, 0);
                    }
                     unset($others);
                }

            }
        }
        //    $mp=magebosubscription_addPricing($serviceid,$iAddressNbr,$InvoiceGroup,1,$PricingAmount,600,$cInvoiceReference,$ContractDate,$iStartMonth,$iStartYear,$bEnabled,0,$cRemark);
        //If the Subscription Start is the first day of the
        //month then there is no need for prorata
        if ($d[1] == "01") {
            //logActivity('No Prorata - first of te month');
            $mp[] = $this->x_magebosubscription_addPricing($client->mageboid, $InvoiceGroup, 1, $PricingAmount, 600, $cInvoiceReference, $ContractDate, $iStartMonth, $iStartYear, $bEnabled, 0, trim($cRemark), trim($cInvoiceDescription), 1);
        } else {

            //Here we create the pricing / priced items for this
            //logActivity('!Prorata !!!');
            $prorata = round($PricingAmount * (($row['diff'] + 1) / $row['DaysInMonth']), 4);
            //logActivity('Prorata Amount : ' . $prorata);
            //First the ProRata piece
            if($this->setting->Prorata != "1"){
                $prorata = "0";
            }
            $mp[]    = $this->x_magebosubscription_addPricing($client->mageboid, $InvoiceGroup, 1, $PricingAmount, 600, $cInvoiceReference, $ContractDate, $iStartMonth, $iStartYear, $bEnabled, 1, trim($cRemark), trim($cInvoiceDescription), $prorata);
        }

        if($mobile->setup >0){
            if(trim($mobile->notes) != 'Imported from KPN'){
            $setupRemark          = "Activation - " . $pincoderef;
            $setupRemakInvoice    = "Activation - " . $pincoderef;
            $setup = exvat4($vat, $mobile->setup);
if(!$noDiscount){
    $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Activation',1,$setup,$vat,1,1,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$setupRemark','NO REPORTING','$setupRemakInvoice',0");

}

            }

        }

        if (!empty($mobile->promocode)) {
            $promoIndex  =false;
            $iTotalMonths         = $mobile->promo_duration;

            $mUnitPrice           = exvat4($client->vat_rate, $mobile->promo_value) * -1;
            $cRemarkx             = $mobile->promo_name;
            $cInvoiceDescriptionx = lang($mobile->promo_name) . " " . $pincoderef;
            if(!$noDiscount){
                $promoIndex           = $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1");

            }
            // $mp[]                 = $promoIndex;
            if($promoIndex){
                if($mobile->companyid == "53"){

                    $this->db->query("update tblGeneralPricing set cInvoiceDescription = '".$cInvoiceDescriptionx."', cRemark = '".$cInvoiceDescriptionx."'
                    where iGeneralPricingIndex = ?", array($promoIndex));
                }
            }


            //log the query
            insert_query_log(array(
                'funct' => 'x_magebosubscription_addPricing->Discount',
                'description' => "exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1"
            ));


        }
        if(!empty($mp)){
            if (!empty($mp)) {
                $this->update_generalPricing($mobile->id, implode(',', array_filter($mp)));
            }
            return  implode(',', array_filter($mp));
        }else{
            return "success";
        }

    }

    public function checkInvoiceNexmonth($IndexId, $startdate){

     $this->db->query("delete from tblPricedItems
where iGeneralPricingIndex = ?
AND iInvoiceNbr is null
AND cast(case when iMonth < 10 then '0'+cast(iMonth as varchar) else cast(iMonth as varchar) end + '/01/' + cast(iYear as varchar) as datetime)
>= ?", array($IndexId, $startdate));


    }
    public function addNextMonthInvoiceItems($client, $data){
        $product             = getMageboProduct($mobile->packageid);

        $InvoiceGroup        = $product->iInvoiceGroupNbr;

        $bussiness            = $this->getInvoiceBussiness($InvoiceGroup);
        $cBusiness            = $bussiness['cBusiness'];
        $ContractDate        = $row['ContractDate'];
        $iStartMonth         = $row['StartMonth'];
        $iStartYear          = $row['StartYear'];
        $setupRemark          = "Activation - " . $pincoderef;
        $setupRemakInvoice    = "Activation - " . $pincoderef;
        $setup = exvat4($vat, $mobile->setup);
        $this->addPricingRemote("exec spInsertgeneralPricing $client->mageboid,'$cBusiness','Activation',1,$setup,$vat,1,1,'$ContractDate',$iStartMonth,$iStartYear,1,'$setupRemark','NO REPORTING','$setupRemakInvoice',0");


    }
    public function update_generalPricing($id, $index){

        $this->db1->where('id', $id);
        $this->db1->update("a_services", array('iGeneralPricingIndex' =>$index));

    }
    public function getInvoiceBussiness($InvoiceGroup)
    {
        $s             = $this->db->query("select * from tblInvoiceGroup where iInvoiceGroupNbr= ?", array(
            $InvoiceGroup
        ));
        $lineIG        = $s->row_array();
        $cInvoiceGroup = $lineIG['cInvoiceGroup'];
        $resultIB      = $this->db->query("select * from tblType where iTypeNbr= ?", array(
            $lineIG['iBusinessNbr']
        ));
        $lineIB        = $resultIB->row_array();
        $cBusiness     = $lineIB['cTypeDescription'];
        return array(
            'cInvoiceGroup' => $cInvoiceGroup,
            'cBusiness' => $cBusiness
        );
    }
    public function x_magebosubscription_addPricing($iAddressNbr, $InvoiceGroup, $rNumber, $mUnitPrice, $iTotalMonths, $cInvoiceReference, $ContractDate, $iStartMonth, $iStartYear, $bEnabled, $createpriceditems, $cRemark, $cInvoiceDescription, $prorata)
    {
        //For now, we use $VAT=21
        //Get invoice Group Description
        $this->db->query("select * from tblInvoiceGroupDescription where iInvoiceGroupNbr= ? AND  iLanguageIndex=(SELECT iLanguageIndex FROM tblAddress where iAddressNbr=?)", array(
            $InvoiceGroup,
            $iAddressNbr
        ));
        $s             = $this->db->query("select * from tblInvoiceGroup where iInvoiceGroupNbr= ?", array(
            $InvoiceGroup
        ));
        $lineIG        = $s->row_array();
        $resultIB      = $this->db->query("select * from tblType where iTypeNbr= ?", array(
            $lineIG['iBusinessNbr']
        ));
        $lineIB        = $resultIB->row_array();
        // $iAddressNbr=$row['uniqueId'];
        //$cBusiness="DELTA MOBILE";
        $cBusiness     = $lineIB['cTypeDescription'];
        $cInvoiceGroup = $lineIG['cInvoiceGroup'];
        $client        = getClientDetailidbyMagebo($iAddressNbr);
        if ($client->vat_exempt == 1) {
            $vat = 0;
        } else {
            $vat = round($client->vat_rate, 0);
        }
        $gpIndex = $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','$cInvoiceGroup',$rNumber,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemark','NO REPORTING','$cInvoiceDescription',$prorata");
        //logActivity("General Pricing Index : " . $gpIndex);
        if ($gpIndex) {
            if ($createpriceditems == 1) {
                //1.PricedItem
                $QueryInsertActivationPriced = "exec spInsertPricedItems " . $gpIndex . ", " . $iStartMonth . ", " . $iStartYear . ", 1, " . $prorata . ", " . $vat . ", 1";
                insert_query_log(array(
                    'funct' => 'x_magebosubscription_addPricing',
                    'description' => $QueryInsertActivationPriced
                ));
                $this->db->query($QueryInsertActivationPriced);
                /*
                Do not create Invoice for it
                $QueryInsertActivationForInvoicing = "exec spInsertPricedItemsInvoice " . $iAddressNbr . ", " . $gpIndex . ", " . $iStartYear . ", " . $iStartMonth;
                insert_query_log(array(
                'funct' => 'x_magebosubscription_addPricing',
                'description' => $QueryInsertActivationForInvoicing));
                $this->db->query($QueryInsertActivationForInvoicing);
                */
            }
        } else {
            die("Failed to get GPIndex on exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','$cInvoiceGroup',$rNumber,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemark','NO REPORTING','$cInvoiceDescription',$prorata");
        }
        return trim($gpIndex);
    }
    public function insertPromo($mobile)
    {
        $q                   = $this->db->query("select * from tblC_Pincode where iPincode= ?", array(
            $mobile->details->msisdn_sn
        ));
        $lineIGD             = $q->row_array();
        $pincoderef          = $lineIGD['cInvoiceReference'];
        $sstartday           = substr($mobile->date_contract, 0, 2);
        $product             = getMageboProduct($mobile->packageid);
        $PricingAmount       = $product->recurring_subtotal;
        $InvoiceGroup        = $product->iInvoiceGroupNbr;
        $d                   = explode('-', $mobile->date_contract);
        $datequery           = "SELECT SD.InvoiceStartDate,
		                            CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)) ContractDate,
		                            STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) CtrDate
		                            ,LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) Lday,
		                            DATEDIFF(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )),
		                            STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) )diff,
		                            DAY(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ))) DaysInMonth,
		                            MONTH(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartMonth,
		                            YEAR(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartYear
		                            FROM
		                            (SELECT '" . $d[2] . '-' . $d[0] . '-' . $d[1] . "' InvoiceStartDate )SD ";
        $resultnew           = $this->db1->query($datequery);
        $row                 = $resultnew->row_array();
        //logActivity($datequery);
        $ContractDate        = $row['ContractDate'];
        $iStartMonth         = $row['StartMonth'];
        $iStartYear          = $row['StartYear'];
        $client              = $this->CI->Admin_model->getClient($mobile->userid);
        $GPcRemark           = $product->GPcRemark;
        $InvoiceDescription  = $product->iInvoiceDescription;
        $cRemark             = sprintf("$GPcRemark", $pincoderef);
        $cInvoiceDescription = sprintf("$InvoiceDescription", $pincoderef);
        $cInvoiceReference   = $pincoderef;
        $bEnabled            = 1;
        if (!empty($mobile->promocode)) {
            $iAddressNbr = $client->mageboid;
            //h.promo_name,h.promo_description,h.promo_duration,h.id as promo_id,
            $client      = getClientDetailidbyMagebo($iAddressNbr);
            if ($client->vat_exempt == 1) {
                $vat = 0;
            } else {
                $vat = round($client->vat_rate, 0);
            }
            $bussiness            = $this->getInvoiceBussiness($InvoiceGroup);
            $iTotalMonths         = $mobile->promo_duration;
            $cBusiness            = $bussiness['cBusiness'];
            $cInvoiceGroup        = $bussiness['cInvoiceGroup'];
            $mUnitPrice           = exvat4($client->vat_rate, $mobile->promo_value) * -1;
            $cRemarkx             = $mobile->promo_name;
            $cInvoiceDescriptionx = $mobile->promo_name . " " . $pincoderef;
            $promoIndex           = $this->addPricingRemote("exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1");
            echo "exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1";
            if (!empty($promoIndex)) {
                $this->db->query("update tblGeneralPricing set cInvoiceDescription = 'Introductiekorting €5, 6mnd: '+substring(cInvoiceDescription, charindex('06-',cInvoiceDescription), 14), cRemark = 'Introductiekorting €5, 6mnd: '+substring(cInvoiceDescription, charindex('06-',cInvoiceDescription), 14)
  where iGeneralPricingIndex = ?", array(
                    $promoIndex
                ));
                echo $promoIndex;
                //$this->db1->query("update a_services set iGeneralPricingIndex=? where id=?", array(implode(',', array_filter($mp)), $mobile->id));
            }
            //log the query
            insert_query_log(array(
                'funct' => 'x_magebosubscription_addPricing->Discount',
                'description' => "exec spInsertgeneralPricing $iAddressNbr,'$cBusiness','Discount Next Month',1,$mUnitPrice,$vat,1,$iTotalMonths,'$ContractDate',$iStartMonth,$iStartYear,$bEnabled,'$cRemarkx','NO REPORTING','$cInvoiceDescriptionx',1"
            ));
        }
    }
    public function AddBundlePricing($params, $addonid)
    {
        //$bundle = $this->db->query('select * from a_products_mobile_bundles where id=?', array($addonid));
        $bundle          = getMageboProductAddons($addonid);
        print_r($bundle);
        $bundlestartdate = $params['date_contract'];
        $iPincode        = $params['number'];
        //logActivity("Starting Magebo Bundle Creation for iPincode:" . $iPincode . " with bundle Date: " . $bundlestartdate);
        //****************CALL BUNDLE*****************************//
        $date_start      = explode('-', $bundlestartdate);
        $time            = date('H:i:s');
        $date_start1     = $date_start[2] . '-' . $date_start[0] . '-' . $date_start[1] . ' ' . $time;
        $date            = date_create($date_start[2] . '-' . $date_start[0] . '-' . $date_start[1]);
        date_add($date, date_interval_create_from_date_string("30 days"));
        $future_date       = date_format($date, "Y-m-d") . ' ' . $time;
        $unix_future       = strtotime($future_date);
        $final_unix_future = $unix_future - 1;
        $stopdate          = gmdate("m-d-Y H:i:s", $final_unix_future + (60 * 60));
        //Now we add the bundles
        $BPC1              = str_replace("|", "'", $bundle->pricing_query);
        if ($bundle->bundle_type == "option") {
            $qBPC1 = sprintf("$BPC1", $iPincode, $bundlestartdate . ' ' . $time, $stopdate);
        }elseif ($bundle->bundle_type == "others") {

        } else {
            $qBPC1 = sprintf("$BPC1", $iPincode, $bundlestartdate . ' ' . $time);
        }
        insert_query_log(array(
            'funct' => 'x_magebosubscription_addPricing',
            'description' => $qBPC1
        ));
        if(substr($iPincode, 0, 2) == 31){
             $this->db->query($qBPC1);
             $qresult = false;

        }else{

           $bundleIndex = $this->addBundleRemote($qBPC1);

           $counter = $this->getMaxPincodeCounter($iPincode);

           if(!empty($bundle->FCA)){
             //  exec spInsertPincodeFreeCallsAssignment @iPincode,@counter,'SMS_Pack_ATAN','FREE SMS',10000,1,'NO TRANSFER',@contractDate,@StartDate,NULL,1,'SMS BUNDLE ATAN - ID :@iPincodeBundleIndex',0
                $qT1 = $bundle->FCA;
                $qT1 = str_replace("|", "'", $qT1);
                $qT1 = sprintf("$qT1", $iPincode, $counter+1, $bundlestartdate.' '.$time,  $date_start1, $bundleIndex );

               //mail('mail@simson.one','ResultFreeAssignment0', print_r($qT1, true));
               //mail('mail@simson.one','ResultFreeAssignment', print_r($counter, true)." ".print_r($qT1, true)."\n".$iPincode."\n".$qBPC1);
               $insertBundleFreeIndex = $this->addBundleFreecallRemote($qT1);

               //mail('mail@simson.one','ResultFreeAssignment1', print_r($insertBundleFreeIndex, true));
               if(!empty($bundle->FCL)){

                $qT2 = $bundle->FCL;
                $qT2 = sprintf("$qT2", $bundleIndex, $insertBundleFreeIndex);
                //mail('mail@simson.one','ResultFreeAssignment2', print_r($qT2, true));
                $this->db->query($qT2);
                  // $qT2 = sprintf("$bundle->FCL", $bundleIndex['PincodeBundleIndex'],  $ResultFreeAssignment[]);

               }
           }


        }





        return array(
            'result' => 'success'
        );
    }

    public function getMaxPincodeCounter($iPincode){
       $q = $this->db->query("Select Max(iPriority) AS LatestPriority From tblC_PincodeFreeCallsAssignment where iPincode = ?", array($iPincode));

        if($q->num_rows()){

            return $q->row()->LatestPriority;

        }else{

            return false;
        }
    }
    public function AddPortinSIMToMagebo($mobile)
    {
        //print_r($params);exit;
        $client            = $this->CI->Admin_model->getClient($mobile->userid);
        //Initialize data fields IMPORTANT !!!
        $oldpaymeth        = "";
        $oldnumpersonal    = "";
        $oldgsmnbr         = "";
        $oldsimnbr         = "";
        $oldclientnbr      = "";
        $oldclientname     = "";
        $oldvatnumber      = "";
        $oldresp           = "";
        $oldoperator       = "";
        $gprs              = "";
        $invoice_condition = "";
        $keepold           = "";
        $strucomm          = "";
        $prepost           = "";
        $dateactivation    = "";
        $tempiAdNMag       = "";
        $SimToUse          = "";
        $iAddressNbr       = "";
        $cCompany          = $client->initial . ' ' . $client->firstname . ' ' . $client->lastname;
        /******************************************************************************************************************************************/
        /***********************************************Insert Pincode & SIMCard*******************************************************************/
        /******************************************************************************************************************************************/
        if ($mobile->details->msisdn_type == "porting") {
            $keepold = "yes";
        } else {
            $keepold = "no";
        }
        //echo "keep OLD".$keepold;exit;
        //This is the Magebo Address Number
        $iAdN = $client->mageboid;
        if ($keepold == 'no') {
            $PORTtype = "";
        }
        $ptype = $mobile->details->donor_type;
        if ($ptype == "0") {
            $PORTtype = "PrePaid";
        } else {
            $PORTtype = "PostPaid Simple";
        }
        switch ($client->language) {
            case "dutch":
                $cInternalLanguage = "Dutch";
                break;
            case "english":
                $cInternalLanguage = "English";
                break;
            case "french":
                $cInternalLanguage = "French";
                break;
            case "german":
                $cInternalLanguage = "German";
                break;
            default:
                $cInternalLanguage = "Dutch";
        }
        //if($rowOD['fieldid']==1013){$iAddressNbr=$rowOD['value'];}
        //if($rowOD['fieldid']==4){$oldnumpersonal=$res->attributevalue;}
        $oldsimnbr     = $mobile->details->donor_sim;
        $oldclientnbr  = $mobile->details->donor_accountnumber;
        $oldgsmnbr     = "0" . $mobile->details->donor_msisdn;
        $oldclientname = "";
        $oldvatnumber  = "";
        $oldresp       = "";
        $oldoperator   = $mobile->details->donor_provider;
        //1.Get the SIMCard from the order
        $SimToUse      = $mobile->details->msisdn_sim;
        $nextFreeSim   = $SimToUse;
        $dContractDate = $mobile->date_contract;
        //2.Check if it is a porting OR new number --> insert the pincode
        //Then it is a porting
        $iPincode      = $mobile->details->donor_msisdn;
        $ip            = $this->InsertPincode($iPincode, $client->mageboid, $dContractDate, $mobile->details->msisdn_sim);
        if ($ip) {
            /******************************************************************************************************************************************/
            /*******************************Insert PincodeCallPriceList & assign the SIMCard to the pincode********************************************/
            /******************************************************************************************************************************************/
            //$resultIC = select_query("magebo_products","",array("ResellerProductName"=>$params['customfields']['priceplan']));
            // $dataIC = mysql_fetch_array($resultIC);
            // $invoice_condition = $dataIC['iCallPriceListNbr'];
            // $PriceListTarif = $dataIC['iCallPriceListSubType'];
            $invoice_condition = $mobile->PriceList;
            $PriceListTarif    = $mobile->PriceListSubType;
            //print_r($dataIC);exit;
            //Insert PincodeCallPriceList $iAddressNbr, $invoice_condition, $iPincode, $PriceListTarif
            $this->InsertPricelist($client->mageboid, $invoice_condition, $iPincode, $PriceListTarif, $client->vat_exempt);
            //Assign SIMCard to the Pincode
            // $this->AssignSIMCard($iPincode, $mobile->details->msisdn_sim);
            /******************************************************************************************************************************************/
            /***************************************Insert SIMCard options of the pincode**************************************************************/
            /******************************************************************************************************************************************/
            $SIMCardType           = $mobile->details->ptype;
            //If Porting we have to do an updatesimCard
            //Then it is a porting
            $cSIMCardNbr           = $mobile->details->msisdn_sim;
            $cMSISDNType           = "PORTED";
            $cPaymentType          = $SIMCardType;
            $cCallSubscriptionType = "NO CALL SUBSCRIPTION";
            if ($gprs == "gprs") {
                $cGPRSSubscriptionType = "WEB.BE GPRS SUBSCRIPTION";
            } else {
                $cGPRSSubscriptionType = "WEB.BE GPRS SUBSCRIPTION";
            }
            $cVoiceMailLanguage     = strtoupper($cInternalLanguage);
            $bRoaming               = 1;
            $bBarIncomingCalls      = 0;
            $bBarInternationalCalls = 0;
            $bBarPremium            = 0;
            $bBarPremiumVoice       = 0;
            $bCLIR                  = 0;
            $bBarPartial            = 0;
            $bBarFull               = 0;
            $bIntelligentRoaming    = 0;
            $cRemark                = $cCompany;
            $mInitialPrepaidAmount  = 0;
            $iCarrier               = -1;
            $cTypeOfPorting         = $PORTtype;
            $cDonorMSISDN           = $oldgsmnbr;
            $cDonorSimCardNbr       = $oldsimnbr;
            $cVATNbr                = $oldvatnumber;
            $cAuthorisedRequestor   = $oldresp;
            $cDonorAccountNbr       = $oldclientnbr;
            $cDonorCustomerName     = $oldclientname;
            //$iDonorCarrier = 60008;
            //Determine the Donor Carrier Based on the the custom field PortDonorOperator
            if($SIMCardType == "PRE PAID"){
                $bPseudoPostpaid        = 1;
            }else{
                $bPseudoPostpaid        = 0;
            }

            $iDonorCarrier          = 60269;
            $mInitialCostAmount     = 0;
            $iValidityDays          = 365;
            $cBlackBerryType        = "NONE";
            $bMMS                   = 1;
            $queryPORT              = "EXEC spUpdateSimCard '$cSIMCardNbr', '$cMSISDNType','SINGLE', '$cPaymentType', '$cCallSubscriptionType', '$cGPRSSubscriptionType', '$cVoiceMailLanguage', $bRoaming, $bBarIncomingCalls, $bBarInternationalCalls, $bBarPremium, $bBarPremiumVoice, $bCLIR, $bBarPartial, $bBarFull, $bIntelligentRoaming, $bPseudoPostpaid, '$cRemark', $mInitialPrepaidAmount, $iCarrier, '$cTypeOfPorting', '$cDonorMSISDN', '$cDonorSimCardNbr', '$cVATNbr', '$cAuthorisedRequestor', '$cDonorAccountNbr', '$cDonorCustomerName', $iDonorCarrier, $mInitialCostAmount, $iValidityDays, '$cBlackBerryType', $bMMS";
            insert_query_log(array(
                'funct' => 'AddSIMToMagebo->spUpdateSimCard',
                'description' => $queryPORT
            ));
            $resultPORT = $this->db->query($queryPORT);
            return (object) array(
                "result" => "success"
            );
        } else {
            return (object) array(
                "result" => "error",
                "message" => "Adding Sim to Magebo"
            );
        }
    }
    public function AddSIMToMagebo($mobile)
    {
        //print_r($params);exit;
        $client            = $this->CI->Admin_model->getClient($mobile->userid);
        //Initialize data fields IMPORTANT !!!
        $oldpaymeth        = "";
        $oldnumpersonal    = "";
        $oldgsmnbr         = "";
        $oldsimnbr         = "";
        $oldclientnbr      = "";
        $oldclientname     = "";
        $oldvatnumber      = "";
        $oldresp           = "";
        $oldoperator       = "";
        $gprs              = "";
        $invoice_condition = "";
        $keepold           = "";
        $strucomm          = "";
        $prepost           = "";
        $dateactivation    = "";
        $tempiAdNMag       = "";
        $SimToUse          = "";
        $iAddressNbr       = "";
        $cCompany          = $client->initial . ' ' . $client->firstname . ' ' . $client->lastname;
        /******************************************************************************************************************************************/
        /***********************************************Insert Pincode & SIMCard*******************************************************************/
        /******************************************************************************************************************************************/
        if ($mobile->details->msisdn_type == "porting") {
            $keepold = "yes";
        } else {
            $keepold = "no";
        }
        //echo "keep OLD".$keepold;exit;
        //This is the Magebo Address Number
        $iAdN = $client->mageboid;
        if ($keepold == 'no') {
            $PORTtype = "";
        }
        $ptype = $mobile->details->donor_type;
        if ($ptype == "0") {
            $PORTtype = "PrePaid";
        } else {
            $PORTtype = "PostPaid Simple";
        }
        switch ($client->language) {
            case "dutch":
                $cInternalLanguage = "Dutch";
                break;
            case "english":
                $cInternalLanguage = "English";
                break;
            case "french":
                $cInternalLanguage = "French";
                break;
            case "german":
                $cInternalLanguage = "German";
                break;
            default:
                $cInternalLanguage = "Dutch";
        }
        //if($rowOD['fieldid']==1013){$iAddressNbr=$rowOD['value'];}
        //if($rowOD['fieldid']==4){$oldnumpersonal=$res->attributevalue;}
        $oldsimnbr     = $mobile->details->donor_sim;
        $oldclientnbr  = $mobile->details->donor_accountnumber;
        $oldgsmnbr     = "0" . $mobile->details->donor_msisdn;
        $oldclientname = "";
        $oldvatnumber  = "";
        $oldresp       = "";
        $oldoperator   = $mobile->details->donor_provider;
        //1.Get the SIMCard from the order
        $SimToUse      = $mobile->details->msisdn_sim;
        $nextFreeSim   = $SimToUse;
        $dContractDate = $mobile->date_contract;
        if(substr(trim($mobile->details->msisdn), 0, 2) == "32"){
            $iPincode      = substr($mobile->details->msisdn, 2);
        }else{
            $iPincode      = $mobile->details->msisdn_sn;
        }

        //2.Check if it is a porting OR new number --> insert the pincode
        $ip            = $this->InsertPincode($iPincode, $client->mageboid, $dContractDate, $mobile->details->msisdn_sim);
        if ($ip) {
            /******************************************************************************************************************************************/
            /*******************************Insert PincodeCallPriceList & assign the SIMCard to the pincode********************************************/
            /******************************************************************************************************************************************/
            //$resultIC = select_query("magebo_products","",array("ResellerProductName"=>$params['customfields']['priceplan']));
            // $dataIC = mysql_fetch_array($resultIC);
            // $invoice_condition = $dataIC['iCallPriceListNbr'];
            // $PriceListTarif = $dataIC['iCallPriceListSubType'];
            $invoice_condition = $mobile->PriceList;
            $PriceListTarif    = $mobile->PriceListSubType;
            //print_r($dataIC);exit;
            //Insert PincodeCallPriceList $iAddressNbr, $invoice_condition, $iPincode, $PriceListTarif
            $this->InsertPricelist($client->mageboid, $invoice_condition, $iPincode, $PriceListTarif, $client->vat_exempt);
            //Assign SIMCard to the Pincode
            $this->AssignSIMCard($iPincode, $mobile->details->msisdn_sim);
            /******************************************************************************************************************************************/
            /***************************************Insert SIMCard options of the pincode**************************************************************/
            /******************************************************************************************************************************************/
            $SIMCardType           = $mobile->details->ptype;
            $cSIMCardNbr           = $mobile->details->msisdn_sim;
            if(substr(trim($mobile->details->msisdn), 0, 2) == "32"){

                if($mobile->details->msisdn_type == "porting"){
                    $cMSISDNType           = "PORTED";
                }else{
                    $cMSISDNType           = "NEW";
                }
            }else{
                $cMSISDNType           = "NEW";
            }

            $cPaymentType          = $SIMCardType;
            $cCallSubscriptionType = "NO CALL SUBSCRIPTION";
            if ($gprs == "gprs") {
                $cGPRSSubscriptionType = "WEB.BE GPRS SUBSCRIPTION";
            } else {
                $cGPRSSubscriptionType = "WEB.BE GPRS SUBSCRIPTION";
            }
            $cVoiceMailLanguage     = strtoupper($cInternalLanguage);
            $bRoaming               = 1;
            $bBarIncomingCalls      = 0;
            $bBarInternationalCalls = 0;
            $bBarPremium            = 0;
            $bBarPremiumVoice       = 0;
            $bCLIR                  = 0;
            $bBarPartial            = 0;
            $bBarFull               = 0;
            $bIntelligentRoaming    = 0;
            $bPseudoPostpaid        = 0;
            $cRemark                = $cCompany;
            $mInitialPrepaidAmount  = 0;
            $iCarrier               = -1;
            $cTypeOfPorting         = '';
            $cDonorMSISDN           = $mobile->details->donor_msisdn;
            $cDonorSimCardNbr       = '';
            $cVATNbr                = '';
            $cAuthorisedRequestor   = '';
            $cDonorAccountNbr       = '';
            $cDonorCustomerName     = '';
            $iDonorCarrier          = -1;
            $cBlackBerryType        = "NONE";
            $bMMS                   = 1;
            $queryNOPORT            = "EXEC spUpdateSimCard '$cSIMCardNbr', '$cMSISDNType','SINGLE', '$cPaymentType', '$cCallSubscriptionType', '$cGPRSSubscriptionType', '$cVoiceMailLanguage', $bRoaming, $bBarIncomingCalls, $bBarInternationalCalls, $bBarPremium, $bBarPremiumVoice, $bCLIR, $bBarPartial, $bBarFull, $bIntelligentRoaming, $bPseudoPostpaid, '$cRemark', NULL, NULL, '$cTypeOfPorting', '$cDonorMSISDN', '$cDonorSimCardNbr', '$cVATNbr', '$cAuthorisedRequestor', '$cDonorAccountNbr', '$cDonorCustomerName', $iDonorCarrier, NULL, NULL, '$cBlackBerryType', $bMMS";
            $resultNOPORT           = $this->db->query($queryNOPORT);
            insert_query_log(array(
                'funct' => 'AddSIMToMagebo->spUpdateSimCard',
                'description' => $queryNOPORT
            ));
            return (object) array(
                "result" => "success"
            );
        } else {
            return (object) array(
                "result" => "error",
                "message" => "Adding Sim to Magebo"
            );
        }
    }

    public function hasPricingList($iAddressNbr, $iPincode){

        $cInvoiceDescription=   $this->getRef($iPincode);
        if(!$cInvoiceDescription){
            return false;
        }else{
            $q = $this->db->query("select * from tblGeneralPricing where cRemark like ? and iAddressNbr=?", array('%'.$cInvoiceDescription.'%', $iAddressNbr));
            if($q->num_rows()>0){

                return true;
            }else{

                return false;
            }
        }


    }


    public function InsertPricelist($iAddressNbr, $invoice_condition, $iPincode, $PriceListTarif, $vat, $counter=1)
    {
        //First We have to get the description of the pricelist
        $resultDESC  = $this->db->query("SELECT cTypeDescription FROM tblType WHERE iTypeNbr = ?", array(
            $invoice_condition
        ));
        $lineDESC    = $resultDESC->row_array();
        $PPDESC      = $lineDESC['cTypeDescription'];
        //First We have to get the description of the subtype
        $DESCquery2  = "SELECT cTypeDescription FROM tblType WHERE iTypeNbr=" . $PriceListTarif;
        $resultDESC2 = $this->db->query("SELECT cTypeDescription FROM tblType WHERE iTypeNbr=?", array(
            $PriceListTarif
        ));
        $lineDESC2   = $resultDESC2->row_array();
        $PPDESC2     = $lineDESC2['cTypeDescription'];
        if ($vat == "1") {
            $this->db->query("execute spUpdateInvoiceCondition " . $iAddressNbr . ", 14, 'NORMAL', 0, 'PAPER', 3, 10, 10, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, 'EUROPE', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0");
            insert_query_log(array(
                'funct' => 'InsertPricelist',
                'description' => "execute spUpdateInvoiceCondition " . $iAddressNbr . ", 14, 'NORMAL', 0, 'PAPER', 3, 10, 10, 1, 'FULL DETAIL', NULL, '', 'NO REPORTING', 1, 0, 'EUROPE', 0, NULL, 0, 'FIX COST', 10, 1, 'NO EXCHANGE', 0"
            ));
            $rVATPercentage = 0;
        } else {
            $rVATPercentage = 21;
        }
        $this->db->query("EXEC spInsertPincodeCallPriceList " . $iPincode . ", $counter, '" . $PPDESC . "', '" . $PPDESC2 . "', 'DEFAULT TARIF', " . $rVATPercentage . ", NULL");
        insert_query_log(array(
            'funct' => 'InsertPricelist',
            'description' => "EXEC spInsertPincodeCallPriceList " . $iPincode . ", $counter, '" . $PPDESC . "', '" . $PPDESC2 . "', 'DEFAULT TARIF', " . $rVATPercentage . ", NULL"
        ));
    }
    /****************************************************************************************************************************/
    /******************************Funtion to assign simcard to the pincode******************************************************/
    /****************************************************************************************************************************/
    public function AssignSIMCard($iPincode, $nextFreeSim)
    {
        $this->db->query("EXEC spAssignSIMtoPIN " . $iPincode . ", '" . $nextFreeSim . "', 0, 0");
        insert_query_log(array(
            'funct' => 'AssignSIMCard',
            'description' => "EXEC spAssignSIMtoPIN " . $iPincode . ", '" . $nextFreeSim . "', 0, 0"
        ));
    }
    public function addSimcardLog($simcard, $id)
    {
        $date = date('Ymd') . ' ' . date('H:i:s');
        $this->db->query("exec spInsertSIMCardLog '$simcard', $id,NULL,NULL,NULL,NULL,NULL,'$date'");
        insert_query_log(array(
            'funct' => 'AssignSIMCard',
            'description' => "exec spInsertSIMCardLog '$simcard', $id,NULL,NULL,NULL,NULL,NULL,'$date'"
        ));
    }
    public function InsertPincode($iPincode, $iAdN, $dContractDate, $nextFreeSim)
    {

        $linePinExistCheck = $this->getPincodeRemote(trim($iPincode));
        $PinCheck          = $linePinExistCheck['iPincode'];
        $SimCheck          = $linePinExistCheck['SimCardNbr'];
        $dStopDate         = $linePinExistCheck['dStopDate'];

        if ($iPincode == $PinCheck) {
            //Pincode already exists in magebo --> we have to do an internal porting
            //$message = "Internal Porting\n" . "PinCode:" . $iPincode;
            //logActivity($message);
            //WHMCSTicket(1002014, $serviceid, $message, 13, "Delta - InternalPorting ?");

           // mail('mail@simson.one','iAddressNbr: ' .$iAdN. ' Internal Portin Detected for Number: 0'.$iPincode,'Please check in Magebo as Internal Porting required');

            return false;
        }elseif ($iPincode == "31657608920") {
            return false;

        } else {
            //just insert the pincode
            //$cInvoiceReference="0".substr($iPincode,0,3)."/".substr($iPincode,3,2).".".substr($iPincode,5,2).".".substr($iPincode,7,2);
            //31648924972 - 06-48 92 49 72

            if(substr($iPincode, 0, 2) == "31"){
                $cInvoiceReference = "0" . substr($iPincode, 2, 1) . "-" . substr($iPincode, 3, 2) . " " . substr($iPincode, 5, 2) . " " . substr($iPincode, 7, 2) . " " . substr($iPincode, 9, 2);

            }else{

                $cInvoiceReference = "0" . substr($iPincode, 0, 3) .'/'. substr($iPincode, 3, 2) . "." . substr($iPincode, 5, 2) . "." . substr($iPincode, 7, 2);
            }

            if(substr(trim($iPincode), 0, 2) == 31){
                //Tmobile Nl
                $carrier =  60269;
            }else{
                //Base Belgium
               $carrier =  60007;
            }

            if(!$this->isPinCreated($iPincode)){
                $queryIPC          = "EXEC spInsertPincode $iPincode,$iAdN,$iAdN,'$cInvoiceReference','', 'UNITED CALLING MOBILE', $carrier, NULL, 'CLI', 'GSM', 'NONE', '$dContractDate', NULL, '', NULL";
                insert_query_log(array(
                    'funct' => 'InsertPincode',
                    'description' => $queryIPC
                ));
                $resultIPC = $this->db->query($queryIPC);
            }

            return true;
        }
    }
    public function isPinCreated($pin){

        $q = $this->db->query("select * from tblC_Pincode where iPincode = ?", array($pin));
        if($q->num_rows()>0){
            return true;
        }else{

            return false;
        }
    }
    public function updateBundleStopDate($pincode, $date){
        if(substr(trim($pincode), 0, 2) == 32){
            $pincode = substr($pincode, 2);
        }
        $this->db->where('iPincode', $pincode);
        $this->db->update('tblC_BundlePerPincode', array('dBundleStopDate' => $date, 'bEnabled' => '0', 'bInSubscription' => '0'));
    }
    public function getPremier()
    {
        $q = $this->db->query('select iPincode from BlockPremierTelecom');
        return $q->result();
    }
    public function getSim($pincode)
    {
        $q = $this->db->query("select A.iAddressNbr, A.cName, A.cStreet,A.iCompanyNbr,Y.cTypeDescription as PaymentType,
    CASE when A.iCountryIndex = 22
    then (C.cCountryCode + '-' + Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix)
    else (C.cCountryCode + '-' +
    CASE when Cast(A.iZipCode AS varchar(10)) is null
    then ''+ A.cZipSuffix
    else Cast(A.iZipCode AS varchar(10)) + ' ' + A.cZipSuffix end  ) end as ZipCode,
    A.cCity, P.cInvoiceReference, S.cSIMCardNbr as SIMCardNbr, S.iMSISDN as MSISDN, S.*, X.cTypeDescription as Type,Z.cTypeDescription as SimCardType, B.cInternalLanguage as Language
    from tblC_SIMCard S
    left join tblC_Pincode P ON P.iPincode = S.iPincode
    left join tblLocation L ON L.iLocationIndex = P.iLocationIndex
    left join tblAddress A ON A.iAddressNbr = L.iAddressNbr
    left join tblType Y ON Y.iTypeNbr=S.iPaymentType
    left join tblType X ON X.iTypeNbr=S.iMSISDNType
    left join tblType Z ON Z.iTypeNbr=S.iSimCardType
    left join tblLanguages B on B.iBaseVoiceMailLanguage=S.iVoiceMailLanguage
    , tblCountry C
    where S.iPincode = ?
    AND S.bActive = 1
    AND (C.iCountryIndex = A.iCountryIndex)", array(
            trim($pincode)
        ));
        return $q->row();
    }
    public function CreateWelcomeLetter($serviceid)
    {

        $mobile         = $this->CI->Admin_model->getService($serviceid);
        $client         = $this->CI->Admin_model->getClient($mobile->userid);
        $product        = $this->CI->Admin_model->getProduct($mobile->packageid);
        if($client->language){
            $this->CI->config->set_item('language', $client->language);
        }else{
            $this->CI->config->set_item('language', 'dutch');
        }
        $this->CI->lang->load('admin');
        $iAddressNbr    = $client->mageboid;
        $cName          = $client->lastname . ", " . $client->firstname;
        $iPincode       = $mobile->details->msisdn;
        $cStreet        = $client->address1;
        $iZipCode       = $client->postcode;
        $cCity          = $client->city;
        //Determine Language based on the customer's language in whmcs
        $whmcslang      = $client->language;
        $iLanguageIndex = 1; //Start by setting as languageindex to 1 dutch by default
        switch ($whmcslang) {
            case "english":
                $iLanguageIndex = 2;
                break;
            case "french":
                $iLanguageIndex = 2;
                break;
            case "dutch":
                $iLanguageIndex = 1;
                break;
            case "german":
                $iLanguageIndex = 4;
                break;
            default:
                $iLanguageIndex = 1;
        }
        //echo "WHMCSLANG:".$whmcslang."| LanguageIndex:".$iLanguageIndex;exit;
        //$iLanguageIndex=1;
        $cInvoiceReference = $mobile->details->msisdn;
        $cPUK1             = $mobile->details->msisdn_puk1;
        $cPUK2             = $mobile->details->msisdn_puk2;
        $iMSISDNType       = 589; //589 - New //590 - Porintg //591 Swap
        $iPaymentType      = 603; //Prepaid -- postpaid is 604
        $Klantnr           = lang("Cust No : ");
        $GSMnr             = lang("GSMnr : ");
        $PUK1              = "PUK1 : ";
        $PUK2              = "PUK2 : ";
        $Datum             = "DATE : ";
        $title             = lang("Mobile Welcome Letter") . "(" . $iPincode . ")";
        $fname             = $client->id . "_" . $serviceid . "_mobilewelcome.pdf";
        /*Properties to connect to the jasperserver*/
        $format            = "PDF"; // Could be HTML, RTF, etc (but remember to update the Content-Type header above)
        $report            = $product->welcome_template;
        ///reports/UnitedReports/MobileWelcomeLetter/Mobile_WelcomeLetter_1
        $repfile           = DOC_PATH . 'documents/' . $client->companyid . '/attachments/' . $fname;

        $frep              = fopen($repfile, "wb");
        $sp                = new SoapClient($product->jasper_server, array(
            "login" => "jasperadmin",
            "password" => "un!t3d",
            "trace" => 1,
            "exceptions" => 0
        ));
        $request           = "<request operationName=\"runReport\" locale=\"da\">
      <argument name=\"RUN_OUTPUT_FORMAT\">$format</argument>
      <resourceDescriptor name=\"\" wsType=\"\"
      uriString=\"$report\"
      isNew=\"false\">
      <label>null</label>

      	<parameter name='iAddressNbr' isListItem='false'>$iAddressNbr</parameter>
       	<parameter name='cName' isListItem='false'>$cName</parameter>
       	<parameter name='iPincode' isListItem='false'>$iPincode</parameter>
       	<parameter name='cStreet' isListItem='false'>$client->address1</parameter>
       	<parameter name='iZipCode' isListItem='false'>$client->postcode</parameter>
       	<parameter name='cCity' isListItem='false'>$client->city</parameter>
       	<parameter name='iLanguageIndex' isListItem='false'>$iLanguageIndex</parameter>
       	<parameter name='cInvoiceReference' isListItem='false'>$cInvoiceReference</parameter>
     	<parameter name='cPUK1' isListItem='false'>$cPUK1</parameter>
      	<parameter name='cPUK2' isListItem='false'>$cPUK2</parameter>
      	<parameter name='iMSISDNType' isListItem='false'>$iMSISDNType</parameter>
      	<parameter name='iPaymentype' isListItem='false'>$iPaymentType</parameter>
      	<parameter name='Klantnr' isListItem='false'>$Klantnr</parameter>
      	<parameter name='GSMnr' isListItem='false'>$GSMnr</parameter>
      	<parameter name='PUK1' isListItem='false'>$PUK1</parameter>
      	<parameter name='PUK2' isListItem='false'>$PUK2</parameter>
      	<parameter name='Datum' isListItem='false'>$Datum</parameter>
      </resourceDescriptor>
	</request>";
        // logActivity("Welcome Letter Jasper XML Request: " . $request);
        $sp->runReport($request);
        $filecontents = $sp->__getLastResponse();
        // logActivity($filecontents);
        //echo $filecontents;exit;
        fwrite($frep, $filecontents);
        fclose($frep);
        $query = "INSERT INTO `a_clients_files`
    (`id`,
    `userid`,
    `title`,
    `filename`,
    `adminonly`,
    `dateadded`)
    VALUES
    (
    NULL,
    '$client->id',
    '$title',
    '$fname',
    0,
    NOW()
    )";
        $this->db1->query($query);
        return $repfile;
    }
    public function addPricingRemote($query)
    {
        //this is temporary solution because issue with php7.2 version
        $whmcsUrl            = 'https://probile.united-telecom.be/api.php?step=addPricing';
        $postfields['query'] = $query;
        $ch                  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $jsondata = json_decode($response);
        insert_query_log(array(
            'funct' => 'addPricingRemote Result: ' . $jsondata->id,
            'description' => $query
        ));
        return $jsondata->id;
    }
    public function addBundleFreecallRemote($query){
         //this is temporary solution because issue with php7.2 version
         $whmcsUrl            = 'https://probile.united-telecom.be/api.php?step=addbundlefreecall';
         $postfields['query'] = $query;
         $ch                  = curl_init();
         curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_TIMEOUT, 30);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
         $response = curl_exec($ch);
         if (curl_error($ch)) {
             die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
         }
         curl_close($ch);
         $jsondata = json_decode($response);
         insert_query_log(array(
             'funct' => 'addPricingRemote Result: ' . $jsondata->id,
             'description' => $query
         ));
         return $jsondata->id;
    }
    public function addBundleRemote($query)
    {
        //this is temporary solution because issue with php7.2 version
        $whmcsUrl            = 'https://probile.united-telecom.be/api.php?step=addbundle';
        $postfields['query'] = $query;
        $ch                  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $jsondata = json_decode($response);
        insert_query_log(array(
            'funct' => 'addPricingRemote Result: ' . $jsondata->id,
            'description' => $query
        ));
        return $jsondata->id;
    }

    public function getPincodeRemote($query)
    {
        //this is temporary solution because issue with php7.2 version
        $whmcsUrl            = 'https://probile.united-telecom.be/api.php?step=getPincode';
        $postfields['query'] = $query;
        $ch                  = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $jsondata = json_decode($response);

        return (array)$jsondata;
    }
    public function PortIngAction1($servicedetails)
    {
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN        = ltrim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr    = ltrim($servicedetails->details->msisdn_sim);
        $qryfind        = "update tblC_SIMCard set iPincode = " . $iPincodePortIn . " where cSIMCardNbr = '" . $cSIMCardNbr . "' AND iPincode = " . $iMSISDN;
        insert_query_log(array(
            'funct' => 'PortIngAction1: ',
            'description' => $qryfind
        ));
        $this->db->query($qryfind);
    }
    public function PortIngAction2($servicedetails)
    {
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN        = trim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr    = trim($servicedetails->details->msisdn_sim);
        $qryfind        = "update tblC_Pincode set cRemark = 'Ported to " . $iPincodePortIn . "', iPincodeType = 16, iPincodeInterfaceType = 2446, iPincodeRedirectionType = 2447 where iPincode = " . $iMSISDN;
        $this->db->query($qryfind);
        insert_query_log(array(
            'funct' => 'PortIngAction2: ',
            'description' => $qryfind
        ));
    }
    public function PortIngAction3($servicedetails)
    {
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN        = trim($servicedetails->details->msisdn_sn);
        $qryfind        = "update tblC_BundlePerPincode set iPincode = " . $iPincodePortIn . " where iPincode = " . $iMSISDN . " AND ( dBundleStopDate is null OR dBundleStopDate > getdate() )";
        $this->db->query($qryfind);
        insert_query_log(array(
            'funct' => 'PortIngAction3: ',
            'description' => $qryfind
        ));
    }

    public function InsertBundlePincode($iPincode, $BPC1, $date){
        $d = explode('-', $date);
        $bundlestartdate = $d[1].'-'.$d[2].'-'.$d[0];
        $BPC1              = str_replace("|", "'", $BPC1);
        $qBPC1 = sprintf("$BPC1", $iPincode, $bundlestartdate . ' 00:00:00');
        $this->db->query($qBPC1);

    }
    public function getRef($iPincode)
    {
        $qryfind = "select cInvoiceReference from tblC_Pincode where iPincode = " . $iPincode;
        $q       = $this->db->query($qryfind);
        if($q->num_rows()>0){
            return $q->row()->cInvoiceReference;
        }else{
            return false;
        }

    }
    public function PortIngAction4($servicedetails)
    {
        $iPincodePortIn = $servicedetails->details->donor_msisdn;
        $iMSISDN        = trim($servicedetails->details->msisdn_sn);
        $cSIMCardNbr    = trim($servicedetails->details->msisdn_sim);
        $serviceid      = $servicedetails->id;
        $P_MSISDN       = $this->getRef($iMSISDN);
        $P_PORTINGPIN   = $this->getRef($iPincodePortIn);
        $gpIndexes      = $servicedetails->iGeneralPricingIndex;
        $gpIndexes      = rtrim($gpIndexes, ',');
        $qryfind        = "update tblGeneralPricing set cRemark = replace(cRemark, '" . $P_MSISDN . "', '" . $P_PORTINGPIN . "'), cInvoiceDescription = replace(cInvoiceDescription, '" . $P_MSISDN . "', '" . $P_PORTINGPIN . "')  where iGeneralPricingIndex in (" . $gpIndexes . ")";
        $this->db->query($qryfind);
        insert_query_log(array(
            'funct' => 'PortIngAction4: ',
            'description' => $qryfind
        ));
    }
    public function PortIngAction5($iAddressNbr)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->query("update tblGeneralPricing set cInvoiceDescription = replace(cRemark, 'Abo - ', '')
  where iAddressNbr = ?
  AND iInvoiceGroupNbr = 346
  AND cInvoiceDescription like 'Abonnement -%'", array(
            $iAddressNbr
        ));
    }
    public function MovePin($donor_msisdn, $msisdn_sn, $msisdn)
    {
        $this->db->query("update tblC_Pincode
            set cRemark = ?,
            iPincodeType = 16,
            iPincodeInterfaceType = 2446,
            iPincodeRedirectionType = 2447
            where iPincode = ?", array(
            $donor_msisdn,
            $msisdn_sn
        ));
        $this->db->query("update tblC_BundlePerPincode
set iPincode =?
where iPincode = ?
  AND ( dBundleStopDate is null OR dBundleStopDate > getdate() )
 ", array(
            $msisdn,
            $msisdn_sn
        ));
        return 'success';
    }
    public function getGeneralPricingIndex($iAddressNbr, $Reference)
    {
        $this->db = $this->load->database('magebo', true);
        $this->db->query("SELECT iGeneralPricingIndex, iAddressNbr, iInvoiceGroupNbr from tblGeneralPricing where iAddressNbr=?", array(
            $iAddressNbr
        ));
    }
    public function getPaymentList($iAddressNbr)
    {
        $q = $this->db->query("SELECT * FROM tblPayment a LEFT JOIN tblAddress b on a.iAddressNbr=b.iAddressNbr WHERE a.iAddressNbr=? and  b.iCompanyNbr=?", array(
            $iAddressNbr,
            $this->companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function getPaymentListInvoice($iInvoiceNbr)
    {
        $q = $this->db->query("SELECT iPaymentAssignmentIndex, iAddressNbr, iPaymentNbr, iInvoiceNbr, mAssignedAmount, cAssignedRemark, dLastUpdate, iLastUserNbr
        FROM GDC_ERP.dbo.tblPaymentAssignment WHERE iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return array();
        }
    }
    public function update_Mandate_Date($iAddressNbr, $date){

       $this->db->where('iAddressNbr', $iAddressNbr);
       $this->db->where('iTypeNbr', 2101);
       $this->db->update('tblAddressData', array('cAddressData' => $date));

    }
    public function insertIban($iAddressNbr, $cd){

        $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'IBAN', '" . $cd['iban'] . "', '',1");
        $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'BIC', '" . $cd['bic'] . "', '',1");
    }
    public function addMageboSEPA($iban, $bic, $iAddressNbr, $mandateid=FALSE, $date=FALSE)
    {

        if (!empty($iAddressNbr) && !empty($bic) && !empty($iban)) {
            //echo "OKI";exit;
            //First we check to see if there is already a SEPA Mandate for this customer
            //IF so, then we return an error.
            $qryAA = $this->db->query("SELECT * FROM tblAddressData where iTypeNbr=2100 AND iAddressNbr=?", array(
                $iAddressNbr
            ));
            if ($qryAA->num_rows() > 0) {
                if (strlen($qryAA->row()->cAddressData) > 0) {
                    //A Mandate already exists
                    //So we stop
                    return "Error - Mandate " . $qryAA->row()->cAddressData . "already exists for customer:" . $iAddressNbr;
                }
            }
            if($mandateid){
                $SepaMandate              = $mandateid;
            }else{
                $SepaMandate              = "SEPA" . str_pad($iAddressNbr, 10, "0", STR_PAD_LEFT);
            }

            //Use current Date as signature date
            if($date){
                $SepaMandateSignatureDate = $date;
            }else{
                $SepaMandateSignatureDate = gmdate('Y-m-d', time());
            }

            $SepaStatus               = "SEPA FIRST";
            $SepaScheme               = "CORE";
            //IBAN
            /*
            $queryAD3 = "EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'IBAN', '" . $iban . "', '',1";
            $resultAD3 = mssql_query($queryAD3, $linkSQL) or die('Query failed: ' . $queryAD3);
            logActivity("SEPA ADD:" . $queryAD3);

            //BIC
            $queryAD4 = "EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'BIC', '" . $bic . "', '',1";
            $resultAD4 = mssql_query($queryAD4, $linkSQL) or die('Query failed: ' . $queryAD4);
            logActivity("SEPA ADD:" . $queryAD4);
            */
            //SEPA MANDATE
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA MANDATE', '" . $SepaMandate . "', '',1");
            //$resultAD5 = mssql_query($queryAD5, $linkSQL) or die('Query failed: ' . $queryAD5);
            //logActivity("SEPA ADD:" . $queryAD5);
            //SEPA MANDATE
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA MANDATE SIGNATURE DATE', '" . $SepaMandateSignatureDate . "', '',1");
            //$resultAD6 = mssql_query($queryAD6, $linkSQL) or die('Query failed: ' . $queryAD6);
            //logActivity("SEPA ADD:" . $queryAD6);
            //SEPA STATUS
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA STATUS', '" . $SepaStatus . "', '',1");
            //$resultAD7 = mssql_query($queryAD7, $linkSQL) or die('Query failed: ' . $queryAD7);
            //logActivity("SEPA ADD:" . $queryAD7);
            //SEPA SCHEME
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA SCHEME', '" . $SepaScheme . "', '',1");
            //$resultAD8 = mssql_query($queryAD8, $linkSQL) or die('Query failed: ' . $queryAD8);
            //logActivity("SEPA ADD:" . $queryAD8);
            return (object) array('SEPA_MANDATE' => $SepaMandate,
            'SEPA_MANDATE_SIGNATURE_DATE' => $SepaMandateSignatureDate,
            'SEPA_STATUS' => $SepaStatus);

        }
    }
   public function CheckSubscriptionPricing($InvoiceGroupNbr, $iGeneralPricingIndex){


    $q = $this->db->query("SELECT * FROM GDC_ERP.dbo.tblGeneralPricing where iInvoiceGroupNbr = ? and iGeneralPricingIndex = ?", array($InvoiceGroupNbr, $iGeneralPricingIndex));
    if($q->num_rows()>0){
        return true;
    }else{
        return false;
    }


    }
    public function UpgradeDowngrade($iGeneralPricingIndex, $date){
     $q = $this->db->query("select CASE when day('".$date."') = 1 THEN datediff(m, max(cast(case when iMonth < 10 then '0'+cast(iMonth as varchar) else cast(iMonth as varchar) end+'/' +'01'+'/' + cast(iYear as varchar) as datetime)), '".$date."')-1 ELSE datediff(m, max(cast(case when iMonth < 10 then '0'+cast(iMonth as varchar) else cast(iMonth as varchar) end+'/' +'01'+'/' + cast(iYear as varchar) as datetime)), '".$date."') END as Counter1 from tblPricedItems  where iGeneralPricingIndex = ? ", array($iGeneralPricingIndex));
    $c = $q->row_array();
    $counter1 = $c['Counter1'];
    $r = $this->db->query(" select count(*) as Counter2 from tblPricedItems where iGeneralPricingIndex = ? ", array($iGeneralPricingIndex));
    $c1 = $r->row_array();
    $counter2 = $c1['Counter2'];
    echo "Counter:\n".$counter1." ".$counter2;

    $remaining_month = $counter1+$counter2;
    $this->db->query("update tblGeneralPricing set iTotalMonths = ? where iGeneralPricingIndex =?", array($remaining_month, $iGeneralPricingIndex));
    $this->checkInvoiceNexmonth($iGeneralPricingIndex, $date);

    return $remaining_month;

    }

    public function migrateCallPricing($iGeneralPricingIndex, $date){

        $this->db->query("update tblC_PincodeCallPriceList set dStopDate = ? where iPincode in ( select iPincode from tblC_Pincode where cInvoiceReference in ( select right(cRemark,14) from tblGeneralPricing where iGeneralPricingIndex = ? )) AND dStopDate is null", array($date, $iGeneralPricingIndex));



    }

    public function getLatestPriority($pincode){

    $q =   $this->db->query("select max(iPriority)+ 1 as LatestPriority From tblC_PincodeCallPriceList where iPincode = ?", array($pincode));
    $res =  $q->row_array();

    return $res['LatestPriority'];

//


    }

    public function amendMageboSEPA($iban, $bic, $iAddressNbr)
    {
        //IBAN itypeNbr=2098
        $q                     = $this->db->query("SELECT * FROM tblAddressData where iTypeNbr=2143 AND iAddressNbr=?", array(
            $iAddressNbr
        ));
        //logActivity('SEPA Amendment Search:'.$qry2143);
        //$result2143= mssql_query($qry2143,$linkSQL);
        //$Row2143=mssql_fetch_assoc($result2143);
        $iAddressDataIndex2143 = $q->row()->iAddressDataIndex;
        if ($iAddressDataIndex2143 > 0) {
            //Amendment already exists
            //So we update
            $this->db->query("UPDATE tblAddressData SET cAddressData='" . $iban . "|" . $bic . "',bPreferredOrActive=1 WHERE iAddressDataIndex=?", array(
                $iAddressDataIndex2143
            ));
        } else {
            //There is no amendment
            //So we add one
            $this->db->query("EXEC spInsertAddressData " . $iAddressNbr . ", 5, 'SEPA AMENDMENT', '" . $iban . "|" . $bic . "', '',1");
        }
    }
    function getBankDetails($iBankFileIndex)
    {
        $q = $this->db->query("SELECT a.*,b.*,d.cTypeDescription
    FROM tblBankFileDetail a
    LEFT JOIN  tblInvoice b ON b.iInvoiceNbr=a.iInvoiceNbr
    LEFT JOIN tblBankFile c ON c.iBankFileIndex=a.iBankFileIndex
    LEFT JOIN tblType d on d.iTypeNbr=c.iBankFileType
    WHERE a.iBankFileIndex=?", array(
            $iBankFileIndex
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }
    function checkPayment($iInvoiceNbr)
    {
        // $invqry  = $this->db->query("SELECT iAddressNbr FROM tblPayment WHERE iAddressNbr=? AND dPaymentDate=? AND mPaymentAmount=? AND iPaymentForm=?", array());
    }
    /*
    function assignPayment(){

    $QueryInsert0Payment = "execute spInsertPayment " . $iAddressNbr2Invoice . ", '" . $PaymentDate . "', $Amount, '$paymentform', '$SeqNumberPaperStat', '$remark', " . $InvoiceNbr;
    echo "\n" . $QueryInsert0Payment;
    echo "\nPayment insert query : " . $QueryInsert0Payment . "\n";
    $resultInsert0Payment = mssql_query($QueryInsert0Payment, $linkmssql);
    $lineresultInsert0Payment = mssql_fetch_array($resultInsert0Payment);
    $PaymentNbr = $lineresultInsert0Payment['iNewPaymentNbr'];
    echo "PaymentNbr --> " . $PaymentNbr . "\n";

    //Assign Payment
    $QueryAssignPayment = "execute spAssignInvoicePayment " . $iAddressNbr2Invoice . ", " . $PaymentNbr . ", " . $InvoiceNbr . ", '$remark'";
    echo "Assignment query : " . $QueryAssignPayment . "\n";
    $resultAssignPayment = mssql_query($QueryAssignPayment, $linkmssql);


    }
    */
    public function apply_payment($postfields)
    {
        $ch = curl_init();
        if ($postfields['step'] == "1") {
            curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=1');
        } elseif ($postfields["step"] == "2") {
            curl_setopt($ch, CURLOPT_URL, 'https://delta.united-telecom.nl:8443/localapi.php?step=2');
        }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $s = json_decode($response);
        mail("mail@simson.one","payments", print_r($s, true));
        return $s;
    }
    public function deleteClient($iAddressNbr){
        if($iAddressNbr){
            $this->db->query("delete from tblInvoiceCondition where iAddressNbr = ?", array($iAddressNbr));
            $this->db->query("delete from tblAddressData where iAddressNbr= ?", array($iAddressNbr));
            $this->db->query("delete from tblAgent where iAddressNbr= ?", array($iAddressNbr));
            $this->db->query("delete from tblAddress where iAddressNbr= ?", array($iAddressNbr));
        }
    }
    public function deletePaymentAssignment($iPaymentNbr)
    {
        $this->db->query("DELETE FROM tblPaymentAssignment WHERE iPaymentNbr = ?", array(
            $iPaymentNbr
        ));
        return $this->db->affected_rows();
    }
    public function updateInvoiceStatus($InvoiceNbr)
    {
        $this->db->query("UPDATE tblBankFileDetail SET bRefused = 1 WHERE iInvoiceNbr = ?", array(
            $InvoiceNbr
        ));
        return $this->db->affected_rows();
    }
    public function getPaymentInformation($data)
    {
        if (strlen(trim($data->message)) == 12) {
            $iInvoiceNbr = substr(trim($data->message), 1);
            $iInvoiceNbr = substr($iInvoiceNbr, 0, -2);
        } elseif (strlen(trim($data->message)) == 9) {
            $iInvoiceNbr = $data->message;
        } else {
            $iInvoiceNbr = trim($data->message);
        }
        $q = $this->db->query("SELECT a.*,b.*,d.cTypeDescription
FROM GDC_ERP.dbo.tblPaymentAssignment a
LEFT JOIN tblPayment b ON b.iPaymentNbr=a.iPaymentNbr
LEFT JOIN tblBankFileDetail ff ON ff.iInvoiceNbr=a.iInvoiceNbr
LEFT JOIN tblBankFile c ON c.iBankFileIndex=ff.iBankFileIndex
    LEFT JOIN tblType d on d.iTypeNbr=c.iBankFileType

WHERE a.iInvoiceNbr=?
AND b.cPaymentRemark = ?", array(
            $iInvoiceNbr,
            $data->fileid
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function getSinglePaymentInformation($data)
    {
        if (strlen(trim($data->message)) == 12) {
            $iInvoiceNbr = substr(trim($data->message), 1);
            $iInvoiceNbr = substr($iInvoiceNbr, 0, -2);
        } elseif (strlen(trim($data->message)) == 9) {
            $iInvoiceNbr = $data->message;
        } else {
            $iInvoiceNbr = trim($data->message);
        }
        $q = $this->db->query("SELECT a.*,b.*,d.cTypeDescription
FROM GDC_ERP.dbo.tblPaymentAssignment a
LEFT JOIN tblPayment b ON b.iPaymentNbr=a.iPaymentNbr
LEFT JOIN tblBankFileDetail ff ON ff.iInvoiceNbr=a.iInvoiceNbr
LEFT JOIN tblBankFile c ON c.iBankFileIndex=ff.iBankFileIndex
    LEFT JOIN tblType d on d.iTypeNbr=c.iBankFileType
WHERE a.iInvoiceNbr=?", array(
            $iInvoiceNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }


    public function updateSepaStatus($iAddressNbr)
    {
        $this->db->query("Update tblAddressData set cAddressData = 'SEPA FIRST' where iAddressNbr = ? and iTypeNbr = 2102", array(
            $iAddressNbr
        ));
        return $this->db->affected_rows();
    }
    public function assignPaymentPayment($NewPayment, $OldPayment)
    {
        $queryInsertPayment2 = "execute spAssignPaymentPayment " . $NewPayment . ", " . $OldPayment;
        $this->db->query($queryInsertPayment2);
    }
    public function getPincodeRef($number)
    {
        $q = $this->db->query("select * from tblC_Pincode where iPincode= ?", array(
            $number
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->cInvoiceReference;
        } else {
            //06-48 92 49 92
            $n = substr($number, 2);
            return "0" . $n[0] . "-" . $n[1] . $n[2] . " " . $n[3] . $n[4] . " " . $n[5] . $n[6] . " " . $n[7] . $n[8];
        }
    }
    public function getiAddress($id)
    {
        $s = $this->db->query("select  a.dCreationDate as date_created,a.iAddressNbr as mageboid,  a.iCompanyNbr as companyid,a.cName as lastname, a.cStreet as address1, a.iZipCode as postcode, a.cCity as city, b.cCountryCode as country, c.cInternalLanguage as language from tblAddress a left join tblCountry b on b.iCountryIndex=a.iCountryIndex left join tblLanguages c on c.iLanguageIndex=a.iLanguageIndex where a.iAddressNbr=?", array(
            $id
        ));
        $f = $s->row_array();
        $a = $this->getAddressData($id);
        if ($a) {
            $res = array_merge($f, $a);
            if (!empty($res['vat'])) {
                $f['companyname'] = $f['lastname'];
            }
        } else {
            $res = $f;
        }
        $res['mvno_id']  = $f['mageboid'];
        $res['language'] = strtolower($res['language']);
        $res['uuid']     = guidv4();
        $res['password'] = password_hash(random_str('alphanum', 12), PASSWORD_DEFAULT);
        if (!isset($res['email'])) {
            $res['email']         = 'client_' . $f['mageboid'] . '@united-telecom.be';
            $res['invoice_email'] = "no";
        } else {
            $res['invoice_email'] = "yes";
        }

        if(empty($res['postcode'])){
            $res['postcode'] = "0000";
        }
        print_r($res);
        return $res;
    }
    public function getAddressData($iAddressNbr)
    {
        $a = array();
        $q = $this->db->query("select b.cTypeDescription, a.cAddressData from tblAddressData a left join tblType b on b.iTypeNbr=a.iTypeNbr where a.iAddressNbr=? and bPreferredOrActive=1", array(
            $iAddressNbr
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $r) {
                if (trim($r->cTypeDescription) == "TELEPHONENUMBER") {
                    $a['phonenumber'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "VAT NUMBER") {
                    $a['vat'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "E-MAIL") {
                    $a['email'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "BIRTHDATE") {
                    $a['date_birth'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "IDENTITY CARD NUMBER") {
                    $a['nationalnr'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "IBAN") {
                    $a['iban'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "BIC") {
                    $a['bic'] = $r->cAddressData;
                }
                if (trim($r->cTypeDescription) == "GENDER") {
                    $a['gender'] = $r->cAddressData;
                }
            }
        }
        return $a;
    }
    public function getCompanyPrintType($companyid)
    {
        $q = $this->db->query("select cCompany, iDefaultInvoicePrintType from tblCompany where iCompanyNbr=?", array(
            $companyid
        ));
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }
    public function getCompanyPrintLayout($iInvoicePrintNbr)
    {
        $q = $this->db->query("select cPrintTypeDescription from tblInvoicePrintType where iInvoicePrintType =?", array(
            $iInvoicePrintNbr
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->cPrintTypeDescription;
        } else {
            return false;
        }
    }
    public function createCn($data)
    {

        /*
        invoicenum
        companyid
        payment_duedays
        date
        duedate
        amount
        vat_rate
        message
        mageboid
        */
        $this->updateFlag('479', 0);
        if($this->companyid != $data['companyid']){
            return array(
                'result' => 'error',
                'message' => 'Access Denied'
            );

        }
        $company = $this->getCompanyPrintType($data['companyid']);
        if (!$company) {
            return array(
                'result' => 'error',
                'message' => 'Company Not Found'
            );
        }
        $check_flag = $this->getFlag('479');
        if ($check_flag) {

            return array(
                'result' => 'error',
                'message' => 'Billing Temporary Blocked for maintenance, Please try again later'
            );
        }
        $this->updateFlag('479', 1);
        $iInvoicePrintNbr = $company->iDefaultInvoicePrintType; //TypeNumber
        $CompanyName      = $company->cCompany; //companyname
        $layout           = $this->getCompanyPrintLayout($iInvoicePrintNbr); //get Layout Name
        $iTermOfPayment   = $data['payment_duedays'];
        $iInvoiceNbr      = $this->getNewInvoiceNbr($CompanyName);
        $lastDate         = $this->GetLastInvoice($CompanyName, 41);
        if ($lastDate > $data['date']) {
            $this->updateFlag('479', 0);
            return array(
                'result' => 'error',
                'message' => 'Your Invoice date can not be lower than ' . $lastDate.' you send '.$data['date']
            );
        }
        $invoice_date    = convert_invoice_date($data['date']);
        $invoice_duedate = convert_invoice_date($data['duedate']);
        /* $date = new DateTime();
        $date->add(new DateInterval('P10D'));
        echo $date->format('Y-m-d') . "\n";
        */
        // Insert the Invoice
        $amount = $data['amount']*-1;
        $this->db->query("EXEC spInsertInvoice " . $iInvoiceNbr . ", " . $data['mageboid'] . ", 'CREDITNOTE', '" . $invoice_date . "', '" . $invoice_duedate . "', " . $amount . ", '', 'OPEN', '" . $layout . "'");
        //Insert the Invoice in the table for PDF printing
        $this->db->query("EXEC spInsertInvoiceEmailPDF " . $data['mageboid'] . ", " . $iInvoiceNbr . ",'" . $layout . "'");
        //Insert InvoiceDetail line :
        $this->db->query("EXEC spInsertInvoiceDetail " . $iInvoiceNbr . ", 348, '" . $data['message'] . "', 1, " . exvat4($data['vat_rate'], $data['amount']) . ", " . $data['vat_rate'] . ", NULL, 1");
        $this->updateFlag('479', 0);
        if ($this->getNewInvoiceNbr($CompanyName) > $iInvoiceNbr) {
            $Invoice = $this->getInvoice($data['invoicenum']);

            if($Invoice->iInvoiceStatus == 54){
                $this->apply_payment(array(
                    'cPaymentForm' => 'INTERN',
                    'mPaymentAmount' =>  $amount,
                    'cPaymentFormDescription' => "Credit Nota - " . $iInvoiceNbr,
                    'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                    'iInvoiceNbr' => $iInvoiceNbr,
                    'step' => 1,
                    'iAddressNbr' => $data['mageboid'],
                    'cPaymentRemark' => 'Creditnote'
                ));
              /*
                $this->apply_payment(array(
                    'cPaymentForm' => $paymentform,
                    'mPaymentAmount' => $data['amount']*-1,
                    'cPaymentFormDescription' => "Credit Nota - " . $iInvoiceNbr,
                    'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                    'iInvoiceNbr' => $iInvoiceNbr,
                    'step' => 1,
                    'iAddressNbr' => $data['mageboid'],
                    'cPaymentRemark' => 'Creditnote'
                ));
            */

            }else{
               // if (empty($_POST['iAddressNbr']) && empty($_POST['dPaymentDate']) && empty($_POST['mPaymentAmount']) && empty($_POST['cPaymentForm']) && empty($_POST['cPaymentFormDescription']) && empty($_POST['cPaymentRemark']) && empty($_POST['iInvoiceNbr'])) {


                $this->apply_payment(array(
                    'cPaymentForm' => 'INTERN',
                    'mPaymentAmount' =>  $amount,
                    'cPaymentFormDescription' => "Credit Nota - " . $iInvoiceNbr,
                    'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                    'iInvoiceNbr' => $iInvoiceNbr,
                    'step' => 1,
                    'iAddressNbr' => $data['mageboid'],
                    'cPaymentRemark' => 'Creditnote'
                ));
                $this->apply_payment(array(
                    'cPaymentForm' => 'INTERN',
                    'mPaymentAmount' => $data['amount'],
                    'cPaymentFormDescription' => "Credit Nota - " . $data['invoicenum'],
                    'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                    'iInvoiceNbr' => $data['invoicenum'],
                    'step' => 1,
                    'iAddressNbr' => $data['mageboid'],
                    'cPaymentRemark' => 'Creditnote'
                ));
            }
            //applay payment to Creditnote
           /*
            $this->apply_payment(array(
                'cPaymentForm' => $paymentform,
                'mPaymentAmount' => $data['amount']*-1,
                'cPaymentFormDescription' => "Credit Nota - " . $iInvoiceNbr,
                'dPaymentDate' => date("m/d/Y", strtotime($data['date'])),
                'iInvoiceNbr' => $iInvoiceNbr,
                'step' => 1,
                'iAddressNbr' => $data['mageboid'],
                'cPaymentRemark' => 'Creditnote'
            ));
*/

            return array(
                'result' => 'success',
                'iInvoiceNbr' => $iInvoiceNbr
            );
        } else {

            return array(
                'result' => 'error',
                'message' => 'Invoice was not created'
            );
        }
    }
}
