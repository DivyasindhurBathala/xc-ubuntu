<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';

class Spdf extends TCPDF
{
    public $invoicenumber;
    public $logo;
    public $foot;
    public $lang;
    public $font;
    public $footer_image;
    public function __construct()
    {
        parent::__construct();
    }
    public function PrintChapter($num, $title, $file, $mode = false)
    {
        // add a new page

        // disable existing columns
        $this->resetColumns();
        // print chapter title
        $this->ChapterTitle($num, $title);
        // set columns
        $this->setEqualColumns(2, 104);
        // print chapter body
        $this->ChapterBody($file, $mode);
    }
    public function Header()
    {
        if ($this->font) {
            $this->SetFont('helvetica', '', 7);
        } else {
            $this->SetFont('helvetica', '', 7);
        }

        // $this->setY(1);
        // $this->Image('/home/mvno/public_html/assets/img/trendcall.png', 0, 10, 10);
        if ($this->invoicenumber) {
            $this->Cell(0, 10, 'Invoice: ' . $this->invoicenumber, 0, false, 'L', 0, '', 0, false, 'T', 'M');
        }
    }
    /**
     * Set chapter title
     * @param $num (int) chapter number
     * @param $title (string) chapter title
     * @public
     */
    public function setInvoicenumber($id)
    {

        $this->invoicenumber = $id;
    }

    public function setLogo($logo)
    {

        $this->logo = $logo;
    }
    public function setFoot($html)
    {

        $s = explode(":", $html);
        if ($s[0] == "image") {
            $this->foot = $s[1];
            $this->footer_image = true;
        } else {
            $this->foot = $html;
        }
    }
    public function setFooterfont($font)
    {
        $this->font = 'helvetica';
    }
    public function ChapterTitle($num, $title)
    {
        //$this->SetFont('helvetica', '', 14);
        $this->SetFillColor(200, 220, 255);
        $this->Cell(180, 6, 'Page ' . $num . ' : ' . $title, 0, 1, '', 1);
        $this->Ln(4);
    }

    /**
     * Print chapter body
     * @param $file (string) name of the file containing the chapter body
     * @param $mode (boolean) if true the chapter body is in HTML, otherwise in simple text.
     * @public
     */
    public function ChapterBody($content, $mode = false)
    {
        $this->selectColumn();
        // get esternal file content

        // set font
        // $this->SetFont('helvetica', '', 9);
        $this->SetTextColor(50, 50, 50);
        // print content
        if ($mode) {
            // ------ HTML MODE ------
            $this->writeHTML($content, true, false, true, false, 'J');
        } else {
            // ------ TEXT MODE ------
            $this->Write(0, $content, '', 0, 'J', true, 0, false, true, 0);
        }
        $this->Ln();
    }

    public function Footer()
    {
        $CI = &get_instance();
        //$CI->config->set_item('language', $this->lang);
        //$CI->lang->load('admin');
        if ($this->footer_image) {
            $this->Image(FCPATH . 'assets/img/' . $this->foot, 0, 248, 210);
        } else {
            $this->setY(-20);
            $this->writeHTML("<hr>", true, false, false, false, '');
            $this->SetFont($this->font, '', 7);

            $this->writeHTML($this->foot, true, false, false, false, '');
        }
    }
}
