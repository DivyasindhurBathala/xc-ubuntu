<?php
/**
 * Email Library for Soap Interfacses
 */
class Umail
{
    private $CI;
    private $vars;
    private $setting;
    private $mvno_setting;
    private $companyid;
    private $email;
    private $session;
    private $iconfig;
    public function __construct($vars)
    {
        $this->CI =& get_instance();
        $this->db = $this->CI->load->database('default', true);
        $this->CI->load->helper('globalvars');
        $this->companyid    = $vars['companyid'];
        $this->session = $this->CI->session;
        $this->error        = array(
            'result' => 'error'
        );
        $this->success      = array(
            'result' => 'success'
        );
        $this->setting      = getCompanySetting($vars['companyid']);
        $this->mvno_setting = globofix($vars['companyid']);

        if ($this->mvno_setting->smtp_type == 'smtp') {
            $this->iconfig = array(
            'protocol'  => 'smtp',
            'smtp_host' => $this->mvno_setting->smtp_host,
            'smtp_port' => $this->mvno_setting->smtp_port,
            'smtp_user' => $this->mvno_setting->smtp_user,
            'smtp_pass' => $this->encryption->decrypt($this->mvno_setting->smtp_pass),
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'starttls'  => true,
            'wordwrap'  => true,
            );
        } else {
            $this->iconfig['protocol'] = 'sendmail';
            $this->iconfig['mailpath'] = '/usr/sbin/sendmail';
            $this->iconfig['mailtype'] = 'html';
            $this->iconfig['charset']  = 'utf-8';
            $this->iconfig['wordwrap'] = true;
        }
        $this->email = $this->CI->email;
        $this->email->clear();
        $this->email->initialize($this->iconfig);
    }
    public function set_response($data, $type = false)
    {
        if ($type) {
            return (object) array_merge($this->success, $data);
        } else {
            return (object) array_merge($this->error, array(
                'message' => $data
            ));
        }
    }

    public function admin_reset($admin, $payload)
    {
        $this->data['name'] = $admin->firstname . ' ' . $admin->lastname;
        $this->data['code'] = $code;

        $this->data['language'] = "dutch";

        $body = getMailContent('adminresetpassword', $admin->language, $admin->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', $admin->firstname . ' ' . $admin->lastname, $body);
        $body = str_replace('{$code}', $payload['code'], $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mvno_setting->smtp_sender, $this->mvno_setting->smtp_name);
        $this->email->to(trim(strtolower($payload['email'])));
        $this->email->subject(getSubject('adminresetpassword', $admin->language, $admin->companyid));
        $this->email->message($body);
        if (!isTemplateActive($admin->companyid, 'adminresetpassword')) {
            log_message('error', 'Template adminresetpassword is disabled sending aborted');
            redirect('admin/auth/entercode');
            exit;
        }
        if (!$this->email->send()) {
            $this->session->set_flashdata('success', lang('Error while sending via SMTP'));
            redirect('admin/auth/entercode');
            exit;
        } else {
            logAdmin(array('companyid' => $this->session->cid, 'userid' => 0, 'user' => $admin->firstname . ' ' . $admin->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Requesting password request'));

            $this->session->set_flashdata('success', lang('a mail with token has been sent to your email to continue'));
            redirect('admin/auth/entercode');
        }
    }

    public function resetwithcode($admin)
    {
        $this->data['name']     = $admin->firstname . ' ' . $admin->lastname;
        $this->data['language'] = "dutch";

        //$this->data['main_content'] = 'email/' . $this->data['language'] . '/adminpasswordchanged.php';
        //$body = $this->load->view('email/content', $this->data, true);

        $body = getMailContent('adminpasswordchanged', $admin->language, $admin->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', $admin->firstname . ' ' . $admin->lastname, $body);
        //$body = str_replace('{$code}', $code, $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mvno_setting->smtp_sender, $this->mvno_setting->smtp_name);
        $this->email->to($admin->email);
        $this->email->subject(getSubject('adminpasswordchanged', $admin->language, $admin->companyid));
        $this->email->message($body);
        if (!isTemplateActive($admin->companyid, 'adminpasswordchanged')) {
            log_message('error', 'Template adminpasswordchanged is disabled sending aborted');
            redirect('admin');
            exit;
        }

        if ($this->email->send()) {
            $this->session->set_flashdata('success', lang('Your password has been changed successfully'));
            redirect('admin');
            exit;
        } else {
            $this->session->set_flashdata('success', lang('there was error sending your information'));
            redirect('admin');
            exit;
        }
    }

    public function reseller_send_password($client, $password)
    {
        //$this->data['language'] = "dutch";
        //$this->data['info']     = (object) array('email' => $client->email, 'password' => $password, 'name' => $client->firstname . ' ' . $client->lastname);
        $body = getMailContent('reseller_signup_email', $client->language, $client->companyid);

        $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$password}', $password, $body);
        $body = str_replace('{$email}', trim(strtolower($client->email)), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);

        $this->email->set_newline("\r\n");
        //$this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->from($this->mvno_setting->smtp_sender, $this->mvno_setting->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('reseller_signup_email', $client->language, $client->companyid));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'reseller_signup_email')) {
            log_message('error', 'Template reseller_signup_email is disabled sending aborted');
            $this->session->set_flashdata('success', lang('Reseller has been created but credential was not sent due to reseller_signup_email id disabled'));
        } else {
            if ($this->email->send()) {
                $this->session->set_flashdata('success', lang('Password has been sent!'));
                echo json_encode(array('result' => true));
                exit;
            } else {
                echo json_encode(array('result' => false, 'message' => $this->email->print_debugger()));
                exit;
            }
        }
    }

    public function client_add($client, $password)
    {
        $body = getMailContent('signup_email', $client->language, $client->companyid);
        $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$password}', $password, $body);
        $body = str_replace('{$email}', trim(strtolower($client->email)), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mvno_setting->smtp_sender, $this->mvno_setting->smtp_name);
        $this->email->to(strtolower(trim($client->email)));
        
        $this->email->subject(getSubject('signup_email', $client->language, $client->companyid));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'signup_email')) {
            $this->session->set_flashdata('success', lang('Customer number ') . $client->id . lang(' has been successfully created'));
            log_message('error', 'Template signup_email is disabled sending aborted');
            echo json_encode(array('result' => true, 'id' => $client->id));
            if (!empty($_SESSION['registration'])) {
                $this->session->unset_userdata('registration');
            }

            exit;
        }
        if ($this->email->send()) {
            send_growl(array(
            'message' => $this->session->firstname . ' ' . $this->session->lastname.' has just added new client with id: '.$client->id,
            'companyid' => $client->companyid));

            logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('signup_email', $client->language, $client->companyid), 'message' => $body));
        } else {
            logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('signup_email', $client->language, $client->companyid), 'message' => $body, 'status' => 'error', 'error_message' => $this->email->print_debugger()));
        }

        logAdmin(array('companyid' => $this->session->cid, 'userid' => $client->id, 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Adding customer ' . $client->id));
        $this->session->set_flashdata('success', lang('Customer number ') . $client->id . lang(' has been successfully created'));

        echo json_encode(array('result' => true, 'id' => $client->id));
        if (!empty($_SESSION['registration'])) {
            $this->session->unset_userdata('registration');
        }
        exit;
    }

    public function send_email($client, $payload)
    {
        $body = $payload["message"];
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mvno_setting->smtp_sender, $this->mvno_setting->smtp_name);
        $this->email->to(strtolower(trim($payload["to"])));
        log_message('error', 'Sending email:'.print_r($payload, true));
        if (isset($payload['cc'])) {
            $this->email->bcc(strtolower(trim($this->session->email)));
        }
        $this->email->subject($payload["subject"]);
        $this->email->message($body);

        if ($this->email->send()) {
            logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => strtolower(trim($payload["to"])), 'subject' => $payload["subject"], 'message' => $body));
            $this->session->set_flashdata('success', 'Email has been sent');
            redirect('admin/client/detail/' . $payload['userid']);
        } else {
            logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => strtolower(trim($payload["to"])), 'subject' => $payload["subject"], 'message' => $body, 'status' => 'error', 'error_message' => $this->email->print_debugger()));

            $this->session->set_flashdata('error', 'Email was not sent: ' . $this->email->print_debugger());
            redirect('admin/client/detail/' . $payload['userid']);
        }
    }

    public function client_send_password($client, $password)
    {
        $body = getMailContent('signup_email', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$password}', $password, $body);
        $body = str_replace('{$email}', trim(strtolower($client->email)), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $subject = getSubject('signup_email', $client->language, $client->companyid);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mvno_setting->smtp_sender, $this->mvno_setting->smtp_name);
        $this->email->to($client->email);
        $this->email->subject($subject);
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'signup_email')) {
            $this->session->set_flashdata('success', 'Password has not been sent!');
            log_message('error', 'Template signup_email is disabled sending aborted');
        } else {
            if ($this->email->send()) {
                logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('signup_email', $client->language, $client->companyid), 'message' => $body));

                $this->session->set_flashdata('success', 'Password has been sent!');
                echo json_encode(array('result' => true));
            } else {
                logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('signup_email', $client->language, $client->companyid), 'message' => $body, 'status' => 'not_sent', 'error_message' => $this->email->print_debugger()));

                echo json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
            }
        }
    }

    public function simcard_blocking($client, $mobile)
    {
        $body = getMailContent('simcard_blocking', $client->language, $client->companyid);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$msisdn}', '0' . substr($mobile->msisdn, 2), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->msisdn_puk2, $body);
        $subject = getSubject('simcard_blocking', $client->language, $client->companyid);
        $this->email->set_newline("\r\n");
        $this->email->from($this->mvno_setting->smtp_sender, $this->mvno_setting->smtp_name);
        $this->email->to($client->email);
        $this->email->subject($subject);
        //$this->email->subject(lang("Your New Password"));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'simcard_blocking')) {
            log_message('error', 'Template simcard_blocking is disabled sending aborted');
            echo json_encode(array(
                'result' => true,
                'message' => 'Tempate simcard_blocking is disabled'
            ));
            return array(
                'result' => 'success'
            );
        }
        if ($this->email->send()) {
            logEmailOut(array(
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'to' => $client->email,
                'subject' => $subject,
                'message' => $body
            ));
            return array(
                'result' => 'success'
            );
        } else {
            logEmailOut(array(
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'to' => $client->email,
                'subject' => $subject,
                'message' => $body,
                'status' => 'error',
                'error_message' => $this->email->print_debugger()
            ));
            return array(
                'result' => 'error',
                'message' => $this->email->print_debugger()
            );
        }
    }

    public function simcard_unblocking($client, $mobile)
    {
        $body = getMailContent('simcard_unblocking', $client->language, $client->companyid);
      
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
      
        $body  = str_replace('{$msisdn}', '0' . substr($mobile->msisdn, 2), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->msisdn_puk2, $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $subject = getSubject('simcard_unblocking', $client->language, $client->companyid);
        $this->email->subject($subject);
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'simcard_unblocking')) {
            log_message('error', 'Template simcard_unblocking is disabled sending aborted');
            echo json_encode(array(
                'result' => true,
                'message' => 'Tempate simcard_unblocking is disabled'
            ));
            return array(
                'result' => 'success'
            );
        }
        if ($this->email->send()) {
            logEmailOut(array(
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'to' => $client->email,
                'subject' => $subject,
                'message' => $body
            ));
            return array(
                'result' => 'success'
            );
        } else {
            logEmailOut(array(
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'to' => $client->email,
                'subject' => $subject,
                'message' => $body,
                'status' => 'error',
                'error_message' => $this->email->print_debugger()
            ));
            return array(
                'result' => 'error',
                'message' => $this->email->print_debugger()
            );
        }
    }
}
