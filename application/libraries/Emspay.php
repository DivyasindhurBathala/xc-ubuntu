<?php
/**
 * Artilium Library for Soap Interfacses
 */
class Emspay
{
   private $CI;
   private $option;
   private $url;
   private $final;
	private $companyid;
	private $dateTime;
   // private $ServiceUsageMonitoring;
   public function __construct($vars)
   {
      libxml_disable_entity_loader(false);
      $this->CI =& get_instance();
      if (empty($vars['companyid'])) {
         $this->companyid = get_companyidby_url(base_url());
      } else {
         $this->companyid = $vars['companyid'];
      }
      $this->db                   = $this->CI->load->database('default', true);
      $params                     = $this->db->query('select * from a_payment_gateway where companyid=? and name =?', array(
			$this->companyid,
			'emspay'
      ));
      $this->params               = json_decode($params->row()->options);

      $this->option               = array(
         'trace' => 1,
         'features' => SOAP_USE_XSI_ARRAY_TYPE,
         'cache_wsdl' => WSDL_CACHE_NONE,
         'encoding' => 'UTF-8'
      );
		$this->dateTime = date("Y:m:d-H:i:s");
      $this->final                  = (object) array(
         'result' => '',
         'data' => '',
         'message' => '',
         $this->params
      );
   }


	public function getDateTime() {
		return $this->dateTime;
	}
	public function createHash($chargetotal, $currency) {

		$stringToHash =$this->params->store_name . $this->getDateTime() . $chargetotal . $currency . $this->params->shared_key;
		$ascii = bin2hex($stringToHash);
		return hash('sha256',$ascii);

	}
}