<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function process_mq($envelope, $queue)
{

    //print_r($queue);
    $ci = &get_instance();

    $ci->db  = $ci->load->database('default', true);
    $message = $envelope->getBody();
    echo "\n" . date('Y-m-d H:i:s') . " => GOING TO PROCESS MESSAGE:\n";
    $ci->load->model('Admin_model');
    $rkey    = $envelope->getRoutingKey();
    $payload = json_decode($envelope->getBody());

    $mtype = 'unknown';
    if (strpos($rkey, 'Arta.Events.Reseller.Subscription.Provisioning.JobEndEvent') >= 0) {
        $mtype = "JobEndEvent";
    } elseif (strpos($rkey, 'Arta.Events.Reseller.Subscription.Subscription.StatusChangedEvent') >= 0) {
        $mtype = "StatusChangedEvent";
    }

    echo "\nMessage Type: " . $mtype . "\n";

    if ($envelope->isRedelivery()) {
        $queue->ack($envelope->getDeliveryTag());
        echo ("\t(this message has already been delivered)");
    } else {

        foreach ($payload as $pl) {
            $service = $ci->db->query("select * from a_services_mobile where msisdn_sn =?", array(trim($pl->SubscriptionId)));
            $cid = $ci->Admin_model->getCompanyid($pl->ResellerId);
            if($cid >0) {
                $companyid = $cid;

            }
            if ($service->num_rows() > 0) {
                echo "\nFound Serviceid: " . $service->row()->serviceid . " and CustomerID: " . $service->row()->userid . "\n";

                $client = getCompanydataByUser($service->row()->userid);
                if ("JobEndEvent" == $mtype) {
                    if (!empty($pl->TaskName)) {

                        /* TRY TO DETERMINE THE TYPE OF JOB END EVENT*/




                    }

                } elseif ("StatusChangedEvent" == $mtype) {
                    /*if (($pl->OldStatus == 'Preactive') && ($pl->NewStatus == 'Active')) {

                    if (!getServiceStatus($serviceid)) {
                    echo "\nNEW NUMBER ACTIVATION !!\n";
                    $resultaddsim = addCustomerAndSimToMagebo($serviceid);
                    echo "\n" . "Result Add Sim / Customer to Magebo :: " . $resultaddsim . "\n";
                    if ($resultaddsim != "success") {
                    echo "\n" . "Add customer Add Sim Failed" . $resultaddsim;
                    exit;
                    }
                    $addons = getServiceAddons($serviceid);
                    print_r($addons);
                    foreach ($addons->addon as $a) {
                    print_r($a);
                    activateAddon($a->id);
                    }
                    addLogActivation($serviceid, 'SimcardInsert');
                    addLogActivationNewServer($serviceid, 'SimcardInsert');
                    //exit;
                    $queue->ack($envelope->getDeliveryTag());
                    }

                    }
                     */
                    $queue->ack($envelope->getDeliveryTag());

                }
                unset($service);
                unset($client);
            } else {

                echo "\n Service not found";
            }

        }
    }
}
function isPorting($serviceid){
    $ci = &get_instance();
    $ci->db = $ci->load->database('default', true);
    $q = $ci->db->query("select * from a_event_status  where serviceid=? and status=?", array($serviceid, 'PortingDone'));
if($q->num_rows()>0){
    return true;
}else{
    return false;
}

}
function addSimcardLoggid($companyid, $simcard, $id)
{
    $ci = &get_instance();
    $ci->load->library('magebo', array('companyid' => $companyid));
    $ci->magebo->addSimcardLog(trim($simcard), $id);

}

function ActivateServiceNow($companyid, $sn)
{
    $ci = &get_instance();
    $ci->load->library('artilium', array('companyid' => $companyid));
    $pack = $ci->artilium->GetListPackageOptionsForSnAdvance($sn);
    $ci->artilium->UpdateCLI($sn, 1);
    $ci->artilium->UpdateServices($sn, $pack->NewDataSet->PackageOptions, '1');
}
function getSettingInfo($cid)
{
    $res = array();
    $ci = &get_instance();

    $ci->db = $ci->load->database('default', true);
    $q = $ci->db->query("select * from a_configuration where companyid=?", array($cid));
    foreach ($q->result_array() as $row) {

        if ($row['name'] == "smtp_pass") {

            $row['val'] = encrypt_decrypt('decrypt', trim($row['val']));
        }
        $res[$row['name']] = trim($row['val']);
    }
    return (object) $res;
}
function activatePorting($service)
{
    $ci = &get_instance();
    $ci->load->library('artilium', array('companyid' => $service->companyid));
    $ci->load->library('magebo', array('companyid' => $service->companyid));
    $ci->load->model('Admin_model');
    $ci->load->model('Api_model');
    $setting = getSettingInfo($service->companyid);
    $msisdn = $ci->artilium->getSn($service->msisdn_sim);

    //$pack   = $ci->artilium->GetListPackageOptionsForSnAdvance(trim($msisdn->data->SN));
    //$kk     = $ci->artilium->UpdateServicesDone($msisdn->data->SN, $pack, '1', $service);
    $ci->artilium->UpdateCLI($msisdn->data->SN, 1);

    //$ci->db = $ci->load->database('default', true);
    //$ci->db->query("update a_services set status=? where id=?", array('Active',$service->serviceid));

    if ($setting->create_magebo_bundle) {
        $create = 1;
    } //$this->data['setting']->create_magebo_bundle
    else
    {
        $create = 0;
    }
    $order = $ci->Admin_model->getServiceCli($service->serviceid);
    $ci->magebo->addPricingSubscription($order);

    $client = $ci->Admin_model->getClient($order->userid);
    $addsim = $ci->magebo->AddPortinSIMToMagebo($order);

    if($addsim->result == "success") {

        $ci->Api_model->PortIngAction1($order);
        $ci->Api_model->PortIngAction2($order);
        $ci->Api_model->PortIngAction3($order);
        $ci->Api_model->PortIngAction4($order);
        $ci->Api_model->PortIngAction5($order->companyid);
        //
        //$ci->Api_model->pinmove($order->details->donor_msisdn, $order->details->msisdn_sn, $order->details->msisdn);
        $headers = "From: noreply@united-telecom.be" . "\r\n" .
            "BCC: mail@simson.one";
        mail('simson.parlindungan@united-telecom.be', 'Please check Porting activation in Magebo ', print_r($order, true), $headers);
    }


}


function ActivatePortingDone($serviceid)
{

    $ci = &get_instance();
    $ci->load->model('Admin_model');
    $order = $ci->Admin_model->getServiceCli($serviceid);
    $ci->load->library('artilium', array('companyid' => $order->companyid));
    $ci->load->library('magebo', array('companyid' => $order->companyid));


    $addsim = $ci->magebo->AddPortinSIMToMagebo($order);

    if($addsim->result == "success") {
        $ci->load->model('Api_model');
        $ci->Api_model->PortIngAction1($order);
        $ci->Api_model->PortIngAction2($order);
        $ci->Api_model->PortIngAction3($order);
        $ci->Api_model->PortIngAction4($order);
        $ci->Api_model->PortIngAction5($order->companyid);
        //
        //$ci->Api_model->pinmove($order->details->donor_msisdn, $order->details->msisdn_sn, $order->details->msisdn);
        $headers = "From: noreply@united-telecom.be" . "\r\n" .
            "BCC: mail@simson.one";
        mail('simson.parlindungan@united-telecom.be', 'Please check Porting activation in Magebo ', print_r($order, true), $headers);
    }
}
function ChangeServiceStatus($serviceid, $status)
{
    $ci     = &get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('serviceid', $serviceid);
    $ci->db->update('a_services_mobile', array('msisdn_status' => $status));

}

function ChangeOrdertatus($serviceid, $status)
{
    $ci     = &get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('id', $serviceid);
    $ci->db->update('a_services', array('status' => $status));

}


function getCompanydataByUser($id)
{
    $ci     = &get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.*,b.portal_url,b.email_notification from a_clients a left join a_mvno b on b.companyid=a.companyid where a.id=?", array($id));
    return $q->row();
}
function getMySN($id){
    $ci     = &get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services_mobile where serviceid =? ", array($id));
    return trim($q->row()->msisdn_sn);

}






