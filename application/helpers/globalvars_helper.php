<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function getMasterConfig()
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_config_master");

    foreach ($q->result_array() as $row) {
        $res[$row['name']] = trim($row['val']);
    }
    return (object) $res;
}
function add_days($a, $b)
{
    return $a+$b;
}
function getContactInvoiceEmail($id)
{
    $email = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients_contacts where userid=? and invoiceemails='1'", array(
    $id
    ));

    if ($q->num_rows()) {
        foreach ($q->result() as $row) {
            $email[] = $row->email;
        }
    }

    return $email;
}
function getTeumApiId($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products where id=?", array(
    $id
    ));

    if ($q->num_rows()>0) {
        return $q->row()->api_id;
    }

    return false;
}
function get_dates_of_thisweek()
{
    $date = date('m/d/Y');
    // parse about any English textual datetime description into a Unix timestamp
    $ts = strtotime($date);
    // find the year (ISO-8601 year number) and the current week
    $year = date('o', $ts);
    $week = date('W', $ts);
    // print week for the current date
    for ($i = 1; $i <= 7; $i++) {
        // timestamp from ISO week date format
        $ts = strtotime($year.'W'.$week.$i);
        $dates[]  = date("Y-m-d", $ts);
    }

    return $dates;
}

function get_dates_of_thismonth()
{
    foreach (range(1, 30) as $days) {
        $dates[] = date('Y-m-d', strtotime("-".$days." days"));
    }
    return $dates;
}

function globo()
{
    $res = array();
    $ci =& get_instance();
    if (empty($ci->session->userdata('cid'))) {
        $cid = 53;
    } else {
        $cid = $ci->session->userdata('cid');
    }
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_configuration where companyid=?", array(
    $cid
    ));
    foreach ($q->result_array() as $row) {
        if ($row['name'] == "smtp_pass") {
            $row['val'] = encrypt_decrypt('decrypt', trim($row['val']));
        }
        $res[$row['name']] = trim($row['val']);
    }
    return (object) $res;
}
function get_pppcounter($userid)
{
    $ci =& get_instance();

    $q = $ci->db->query("select * from a_services_xdsl where userid=?", array($userid));

    if ($q->num_rows()>0) {
        return str_pad($q->num_rows(), 2, '0', STR_PAD_LEFT);
    } else {
        return "01";
    }
}
function getTypeCall($id)
{
    //1 = O, 2 = T, 3 = F, 4 = RO, 5 = RT, 6 = RF
    if ($id == 1) {
        return "O";
    } elseif ($id == 2) {
        return "T";
    } elseif ($id == 3) {
        return "F";
    } elseif ($id == 4) {
        return "RO";
    } elseif ($id == 5) {
        return "RT";
    } elseif ($id == 5) {
        return "RF";
    } else {
        return "NA";
    }
}

function getResellerBalance($agentid)
{
    $ci =& get_instance();
    $q = $ci->db->query("select * from a_clients_agents where id=?", array($agentid));

    if ($q->num_rows()>0) {
        return $q->row()->reseller_balance;
    } else {
        return "0.00";
    }
}
function getResellerid($mageboid)
{
    $ci =& get_instance();
    $q = $ci->db->query("select agentid from a_clients where mageboid=?", array($mageboid));

    if ($q->num_rows()>0) {
        return $q->row()->agentid;
    } else {
        return false;
    }
}
function send_chat($data)
{
    $ch = curl_init('https://socket.united-telecom.be/sendchat');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "to=".$data['to']."&from=".$data['from']."&message=".$data['message']."&sent=".$data['sent']);

    // Set HTTP Header for POST request
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'X-API-KEY: d82c8d1619ad8176d665453cfb2e55f0bc5y7d'));

    // Submit the POST request
    $result = curl_exec($ch);

    // Close cURL session handle
    curl_close($ch);

    return json_decode($result);
}
function send_growl($data)
{
    $ch = curl_init('https://socket.united-telecom.be/send');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "channel=".trim($data['companyid'])."&message=".urlencode(date('Y-m-d H:i:s').' '.$data['message']));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/x-www-form-urlencoded',
    'X-API-KEY: d82c8d1619ad8176d665453cfb2e55f0bc5y7d'));

    $result = curl_exec($ch);
    curl_close($ch);
    return json_decode($result);
}
function sendlog($subject, $data)
{
    mail('mail@simson.one', $subject, print_r($data, true));
}
function get_enabled_events()
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query('select * from a_configuration where name=? and val=?', array(
    'event_enable',
    '1'
    ));
    foreach ($q->result() as $row) {
        $res[] = $row->companyid;
    }
    return $res;
}
function replace_body_sms($body, $data)
{
    foreach ($data as $key => $b) {
        $body = str_replace('{$' . $key . '}', trim($b), $body);
    }
    return $body;
}
function getaddons($serviceid, $msisdn)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("SELECT * FROM `a_services_addons` WHERE serviceid=?", array(
    $serviceid
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result_array() as $row) {
            $result[] =  array(
                    "ProductOfferingId" => (int) $row['arta_bundleid'],
                    "OrderedProductCharacteristics" => array(
                        array(
                            "Name" => "MSISDN",
                            "Value" => $msisdn
                        )
                    )
                     );
        }

        return $result;
    } else {
        $service = $ci->db->query("select * from a_services where id=?", array($serviceid));
        $base = $ci->db->query("select * from a_products_mobile_bundles where companyid = ? and base_bundle =1", array(
            $service->row()->companyid
            ));

        $ci->db->insert('a_services_addons', array('serviceid' => $serviceid,
            'terms' => $base->row()->bundle_duration_value,
            'cycle' => $base->row()->bundle_duration_type,
            'recurring_total'=> $base->row()->recurring_total,
            'addon_type' => 'option',
            'arta_bundleid' => $base->row()->bundleid,
            'name' =>$base->row()->name ));
        $result[] =  array(
                "ProductOfferingId" => (int)  $base->row()->bundleid,
                "OrderedProductCharacteristics" => array(
                    array(
                        "Name" => "MSISDN",
                        "Value" => $msisdn
                    )
                )
                 );
        return $result;
    }
}
function getaddons_teum($serviceid, $msisdn)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("SELECT * FROM `a_services_addons` WHERE serviceid=?", array(
    $serviceid
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result_array() as $row) {
            $result[] =  array(
                    "ProductOfferingId" => (int) $row['arta_bundleid'],
                    "OrderedProductCharacteristics" => array(
                        array(
                            "Name" => "MSISDN",
                            "Value" => $msisdn
                        )
                    )
                     );
        }

        return $result;
    } else {
        $service = $ci->db->query("select * from a_services where id=?", array($serviceid));
        $base = $ci->db->query("select * from a_products_mobile_bundles where companyid = ? and base_bundle =1", array(
            $service->row()->companyid
            ));

        $ci->db->insert('a_services_addons', array('serviceid' => $serviceid,
            'terms' => $base->row()->bundle_duration_value,
            'cycle' => $base->row()->bundle_duration_type,
            'recurring_total'=> $base->row()->recurring_total,
            'addon_type' => 'option',
            'arta_bundleid' => $base->row()->bundleid,
            'name' =>$base->row()->name ));
        $result[] =  array(
                "ProductOfferingId" => (int)  $base->row()->bundleid,
                "OrderedProductCharacteristics" => array(
                    array(
                        "Name" => "MSISDN",
                        "Value" => $msisdn
                    )
                )
                 );
        return $result;
    }
}


function getaddons_teum_base($serviceid, $msisdn)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);

    $service = $ci->db->query("select * from a_services where id=?", array($serviceid));
    $base = $ci->db->query("select * from a_products_mobile_bundles where companyid = ? and base_bundle =1", array(
            $service->row()->companyid
            ));
    /*
    $ci->db->insert('a_services_addons', array('serviceid' => $serviceid,
        'terms' => $base->row()->bundle_duration_value,
        'cycle' => $base->row()->bundle_duration_type,
        'recurring_total'=> $base->row()->recurring_total,
        'addon_type' => 'option',
        'arta_bundleid' => $base->row()->bundleid,
        'name' =>$base->row()->name ));
        */
    $result[] =  array(
                "ProductOfferingId" => (int)  $base->row()->bundleid,
                "OrderedProductCharacteristics" => array(
                    array(
                        "Name" => "MSISDN",
                        "Value" => $msisdn
                    )
                )
                 );
    return $result;
}

function getBaseBundle($cid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_reseller_simcard where simcard like ?", array(
    $sim . '%'
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function simcardExist($sim)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_reseller_simcard where simcard like ?", array(
    trim($sim) . '%'
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}

function validatesimcard($sim, $agentid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('simcard', $sim);
    $ci->db->where('resellerid', $agentid);
    $q      = $ci->db->get("a_reseller_simcard");
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function hasNote($msisdn)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_portin_notes where msisdn like ?", array(
    $msisdn
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function getSmsNumbers($id)
{
    $sms = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_sms_notifications where userid=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $sms[] = $row->msisdn;
        }
    }
    return $sms;
}
function getdays($date1, $date2)
{
    $datediff = strtotime($date2) - strtotime($date1);
    return round($datediff / (60 * 60 * 24));
}
function getInvoiceEmail($id)
{
    $email = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where id=?", array(
    $id
    ));
    $x      = $ci->db->query("select * from a_clients_contacts where userid=? and invoiceemails=?", array(
    $id,
    1
    ));
    if ($q->num_rows() > 0) {
        $email[] = trim($q->row()->email);
        if ($x->num_rows() > 0) {
            $email[] = trim($q->row()->email);
        }
        return $email;
    } else {
        return false;
    }
}
function getProductAgents($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_reseller_products where agentid=?", array(
    trim($id)
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $p) {
            $res[] = $p->productid;
        }

        return $res;
    } else {
        return array();
    }
}
function getProductforAgents($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select id,setup,recurring_total,name from a_products where companyid=? order by name asc", array(
    trim($companyid)
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $s            = (array) $row;

            $res[]        = (object) $s;
        }
        return $res;
    } else {
        return false;
    }
}
function getSepaMandateIdTrendcall($mageboid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where mageboid=?", array(
    trim($mageboid)
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->mvno_id;
    } else {
        return 'SEPA' . $mageboid;
    }
}
function getUnpaidInvoices($iAddressNbr)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('magebo', true);
    $q      = $ci->db->query("select * from tblInvoice where iAddressNbr=? and iInvoiceStatus = 52", array(
    trim($iAddressNbr)
    ));
    if ($q->num_rows() > 0) {
        return $q->result();
    } else {
        return false;
    }
}
function skipreminder($iInvoiceNbr)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_reminder_exclude where iInvoiceNbr=?", array(
    trim($iInvoiceNbr)
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function calculate_contractend_date($date, $term)
{
    if ($term < 1) {
        $term = 1;
    }
    $kk               = explode('-', $date);
    $next_month       = "Eind contractdatum: " . date('d-m-Y', strtotime("+1 months", strtotime(date('Y-m-d'))));
    $next_month_count = date('Y-m-d', strtotime("+1 months", strtotime(date('Y-m-d'))));
    if (date('Y-m-d', strtotime("+" . $term . " months", strtotime(convert_contract($date)))) < date('Y-m-d')) {
        return $next_month;
    } elseif (date('Y-m-d', strtotime("+" . $term . " months", strtotime(convert_contract($date)))) < $next_month_count) {
        return $next_month;
    }
    return "Eind contractdatum: " . date('d-m-Y', strtotime("+" . $term . " months", strtotime(convert_contract($date))));
}

function calculate_contractend_date_formated($date, $term)
{
    if ($term < 1) {
        $term = 1;
    }
    $kk               = explode('-', $date);
    $next_month       = date('Y-m-d', strtotime("+1 months", strtotime(date('Y-m-d'))));
    $next_month_count = date('Y-m-d', strtotime("+1 months", strtotime(date('Y-m-d'))));
    if (date('Y-m-d', strtotime("+" . $term . " months", strtotime(convert_contract($date)))) < date('Y-m-d')) {
        return $next_month;
    } elseif (date('Y-m-d', strtotime("+" . $term . " months", strtotime(convert_contract($date)))) < $next_month_count) {
        return $next_month;
    }
    return date('Y-m-d', strtotime("+" . $term . " months", strtotime(convert_contract($date))));
}
function reminderbutton($iInvoiceNbr)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_reminder_exclude where iInvoiceNbr=?", array(
    trim($iInvoiceNbr)
    ));
    if ($q->num_rows() > 0) {
        return false;
    } else {
        return true;
    }
}
function getSetupFee($pid, $vat, $userid)
{
    $ci =& get_instance();
    $ci->db  = $ci->load->database('default', true);
    $q       = $ci->db->query("select * from a_products where id=?", array(
    trim($pid)
    ));
    //log_message('error', $ci->db->last_query());
    $service = $ci->db->query("select * from a_services where userid=? and status IN ('Pending','Active')", array(
    $userid
    ));
    //log_message('error', $ci->db->last_query());
    if ($q->num_rows() > 0) {
        if ($service->num_rows() > 0) {
            return (object) array(
            'subtotal' => exvat4($vat, 7),
            'total' => 7,
            'tax' => vat4($vat, 7)
            );
        } else {
            return (object) array(
            'subtotal' => exvat4($vat, $q->row()->setup),
            'total' => $q->row()->setup,
            'tax' => vat4($vat, $q->row()->setup)
            );
        }
    } else {
        return false;
    }
}
function mod11($nhs_no)
{
    $checkDigit             = (int) substr($nhs_no, -1);
    $numbers                = substr($nhs_no, 0, -1);
    $numbers_array          = str_split($numbers);
    $base                   = 2;
    $numbers_array_reversed = array_reverse($numbers_array);
    foreach ($numbers_array_reversed as $number) {
        $multiplier[$number . 'x' . $base] = (float) $number * $base;
        $base++;
    }
    $sum       = (float) array_sum($multiplier);
    $modulus   = 11;
    $divider   = (float) ($sum / $modulus);
    $remainder = (int) (($divider - (int) $divider) * $modulus);
    $results   = (int) ($modulus - $remainder);
    return ($results == $checkDigit ? true : false);
}
function getNewInvoicenum($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_invoices where companyid=? order by invoicenum desc limit 1", array(
    trim($companyid)
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->invoicenum + 1;
    } else {
        return 8000000000;
    }
}
function companredate($data, $date)
{
    if (date('m-d-Y', strtotime(trim($date))) == str_replace(' 00:00:00', '', $data->date_contract)) {
        return true;
    } else {
        return false;
    }
}
function getSubscriptionbyMsisdn($msisdn)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select c.mvno_id,c.mageboid,b.msisdn, a.* from a_services a left join a_services_mobile b on b.serviceid=a.id left join a_clients c on c.id=a.userid where b.msisdn=?", array(
    trim($msisdn)
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function browser_lang()
{
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    if ($lang == "fr") {
        return 'french';
    } elseif ($lang == "nl") {
        return 'dutch';
    } elseif ($lang == "jo") {
        return 'japanese';
    } else {
        return 'english';
    }
}
function setVoiceMailLanguageByClientLang($lang)
{
    if ($lang == "dutch") {
        return 3;
    } elseif ($lang == "french") {
        return 2;
    } else {
        return 1;
    }
}
function getSubject($name, $language, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_email_templates where name=? and language= ? and companyid=? ", array(
    trim($name),
    trim($language),
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->subject;
    } else {
        return '';
    }
}
function getSubjectCustom($name, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_email_templates where name=?  and companyid=? ", array(
    trim($name),
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->subject;
    } else {
        return '';
    }
}
function getIAddressByMvno_id($id, $cid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where mvno_id like  ? and companyid=? ", array(
    trim($id),
    $cid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->mageboid;
    } else {
        return '';
    }
}
function getIdMvno_id($id, $cid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where mvno_id like  ? and companyid=? ", array(
    trim($id),
    $cid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return 0;
    }
}
function getProductidbyName($name)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products where name like  ? and companyid=? ", array(
    trim($name),
    2
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return 0;
    }
}
function getcountrycode($code)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_countries where code=?", array(
    trim($code)
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->dial_code;
    } else {
        return '32';
    }
}
function format_number($number)
{
    if (is_numeric($number)) {
        if (substr($number, 0, 2) == "00") {
            $number = substr($number, 2);
        } elseif (substr($number, 0, 1) == "0") {
            $number = "32" . substr($number, 1);
        }
        return $number;
    } else {
        return $number;
    }
}
function getMobilebundleName($id, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select name,name_desc from a_products_mobile_bundles where bundleid= ? and companyid=?", array(
    $id,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->name_desc;
    } else {
        return 'Unknown';
    }
}
function getOptionName($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select name,name_desc from a_products_mobile_bundles where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->name_desc;
    } else {
        return 'Unknown';
    }
}
function getAttachmentEmail($name, $language, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_email_templates where name = ? and language = ? and companyid=?", array(
    $name,
    $language,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        $att = explode('|', $q->row()->attachments);
        return $att;
    } else {
        return false;
    }
}
function getMailContent($name, $language, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $s      = $ci->db->query("select * from a_email_templates where companyid=? and language =? and name in ('footer','header')", array(
    $companyid,
    $language
    ));
    foreach ($s->result() as $row) {
        if ($row->name == "footer") {
            $footer = $row->body;
        }
        if ($row->name == "header") {
            $header = $row->body;
        }
    }
    $q = $ci->db->query("select body from a_email_templates where name = ? and language = ? and companyid=?", array(
    $name,
    $language,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        $html = $header;
        $html .= $q->row()->body;
        $html .= $footer;
        return $html;
    } else {
        return false;
    }
}


function getMailContentx($content, $language, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $s      = $ci->db->query("select * from a_email_templates where companyid=? and language =? and name in ('footer','header')", array(
    $companyid,
    $language
    ));
    foreach ($s->result() as $row) {
        if ($row->name == "footer") {
            $footer = $row->body;
        }
        if ($row->name == "header") {
            $header = $row->body;
        }
    }


    $html = $header;
    $html .= $content;
    $html .= $footer;
    return $html;
}


function getMailContentCustom($name, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $s      = $ci->db->query("select * from a_email_templates where companyid=? and name in ('footer','header')", array(
    $companyid
    ));
    foreach ($s->result() as $row) {
        if ($row->name == "footer") {
            $footer = $row->body;
        }
        if ($row->name == "header") {
            $header = $row->body;
        }
    }
    $q = $ci->db->query("select body from a_email_templates where name = ?  and companyid=?", array(
    $name,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        $html = $header;
        $html .= $q->row()->body;
        $html .= $footer;
        return $html;
    } else {
        return false;
    }
}
function isWeekend($date)
{
    return (date('N', strtotime($date)) >= 6);
}
function guidv4()
{
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }
    $data    = openssl_random_pseudo_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}
function getFuturedate($date, $type, $value)
{
    if ($type == "day") {
        $d = date('Y-m-d', strtotime('+' . $value . ' days', strtotime($date)));
    } elseif ($type == "month") {
        $d = date('Y-m-d', strtotime('+1 month', strtotime($date)));
    } elseif ($type == "week") {
        $d = date('Y-m-d', strtotime('+1 week', strtotime($date)));
    } elseif ($type == "year") {
        $d = date('Y-m-d', strtotime('+365 days', strtotime($date)));
    } else {
        $d = date('Y-m-d', strtotime('+30 days', strtotime($date)));
    }
    return $d . 'T' . date('H:i:s');
}
function getTicketCategories($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select id,name from a_helpdesk_category where companyid=?", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->result();
    } else {
        return array();
    }
}
function is_this_combi($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products  where id=? and product_type= ?", array(
    $id,
    'mobile'
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->combi;
    } else {
        return "0";
    }
}
function getVoiceLang($id)
{
    if ($id == 1) {
        return 'English';
    } elseif ($id == 2) {
        return 'French';
    } else {
        return 'Dutch';
    }
}
function getCompanyUrl($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('companyid', $id);
    $q = $ci->db->get('a_mvno');
    if ($q->num_rows() > 0) {
        return $q->row()->portal_url;
    }
}
function get_companyidby_url($url)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('portal_url', $url);
    $ci->db->select('companyid');
    $q = $ci->db->get('a_mvno');
    if ($q->num_rows() > 0) {
        return $q->row()->companyid;
    } else {
        return '2';
    }
}
function getCompanies()
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->distinct('companyid');
    $ci->db->select('companyid');
    $q = $ci->db->get('a_mvno');
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $companies[] = $row->companyid;
        }
        return $companies;
    } else {
        return 'Unknown';
    }
}
function getCompanieMvno()
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->distinct('companyid');
    $ci->db->order_by('companyname', 'asc');
    $q = $ci->db->get('a_mvno');
    return $q->result();
}
function insert_query_log($data)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->insert('a_debug_sql', $data);
}
function getPaymentGateway($companyid)
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('companyid', $companyid);
    $q = $ci->db->get("a_payment_gateway");
    return $q->result();
}
function getCompanySetting($cid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_mvno where companyid=?", array(
    $cid
    ));
    return $q->row();
}
function globofix($cid)
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_configuration where companyid=?", array(
    $cid
    ));
    foreach ($q->result_array() as $row) {
        $res[$row['name']] = trim($row['val']);
    }
    return (object) $res;
}
function getCompanyRange($iCompanyNbr)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_mvno where companyid=?", array(
    $iCompanyNbr
    ));
    return $q->row()->RangeNumber;
}


function isPost()
{
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        return true;
    } elseif ($_SERVER['REQUEST_METHOD'] == "PUT") {
        return true;
    } elseif ($_SERVER['REQUEST_METHOD'] == "PATCH") {
        return true;
    } else {
        return false;
    }
}
function orderStatus()
{
}
function getInvoiceStatus($id)
{
    if ($id == "52") {
        return "Unpaid";
    } elseif ($id == "53") {
        return "SEPA Presented";
    } elseif ($id == "54") {
        return "Paid";
    }
}
function iban_block($companyid, $iban)
{
    $iban = '%' . $iban . '%';
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_configuration where companyid= ? and name =? and val LIKE ?", array(
    $id,
    'block_iban',
    $iban
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function getClientidbyMagebo($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where mageboid= ?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return false;
    }
}
function has2f_authentication($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_admin_2fa where adminid= ?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getClientDetailidbyMagebo($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where mageboid= ?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getHelpdeskCategory()
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_helpdesk_category where companyid= ?", array(
    $_SESSION['companyid']
    ));
    return $q->result();
}
function getCategoryName($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_helpdesk_category where id= ?", array(
    $id
    ));
    return $q->row()->name;
}
function getNewProduct($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.*,b.name as product_name from a_subscription_changes a left join a.products b in b.id=a.new_pid where a.serviceid=? and a.date_commit  >= ?", array(
    $id,
    date('Y-m-d')
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function hasChangeProductPlanned($product)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.*,b.name from a_subscription_changes a left join a_products b on b.id=a.new_pid where serviceid =? and date_commit > ?", array(
    $product,
    date('Y-m-d')
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return array();
    }
}
function NeedProforma($userid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services where status in ('Active','Pending','New') and userid=?", array(
    $userid
    ));
    if ($q->num_rows() > 0) {
        return false;
    } else {
        return true;
    }
}
function getCustomernamebySerial($sn)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select b.* from a_services_mobile a left join a_services c on c.id=a.serviceid left join a_clients b on b.id=c.userid where a.msisdn_sn like ? order by a.id desc", array(
    '%' . $sn . '%'
    ));
    if ($q->num_rows() > 0) {
        return '<a href="' . base_url() . 'admin/client/detail/' . $q->row()->id . '">' . $q->row()->firstname . ' ' . $q->row()->lastname . ' (' . $q->row()->mvno_id . ')</a>';
    } else {
        return '';
    }
}
function getCustomerEmailBySerial($msisdn, $sn)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select b.* from a_services_mobile a left join a_services c on c.id=a.serviceid left join a_clients b on b.id=c.userid where a.msisdn_sn like ? and a.msisdn like ? order by a.id desc", array(
    '%' . $sn . '%',
    $msisdn
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->mvno_id . ";" . $q->row()->email . "\n";
    } else {
        return 'NA';
    }
}
function gen_uuid()
{
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x', mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0x0fff) | 0x4000, mt_rand(0, 0x3fff) | 0x8000, mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
}
function is_simcard_activate_before_porting($number)
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_simcard_porting_not_ok where donor_msisdn like ?", array(
    $number
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function xml2array($xml)
{
    $arr = array();
    foreach ($xml as $element) {
        $tag = $element->getName();
        $e   = get_object_vars($element);
        if (!empty($e)) {
            $arr[$tag][] = $element instanceof SimpleXMLElement ? xml2array($element) : $e;
        } else {
            $arr[$tag] = trim($element);
        }
    }
    return $arr;
}
function simplexml_to_array($xmlobj)
{
    $a = array();
    foreach ($xmlobj->children() as $node) {
        if (is_array($node)) {
            $a[$node->getName()] = simplexml_to_array($node);
        } else {
            $a[$node->getName()] = (string) $node;
        }
    }
    return $a;
}
function getCategoryidSimReplacement($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_helpdesk_category where name=? and companyid=?", array(
    'Simcard Replacement',
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return 0;
    }
}
function getDefaultDepartment($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_configuration where name=? and companyid=?", array(
    'helpdesk_default_deptid',
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->val;
    } else {
        return 0;
    }
}
function format_cdr_time($time)
{
    $time = str_replace('T', ' ', $time);
    $time = str_replace('+02:00', '', $time);
    $time = str_replace('+01:00', '', $time);
    return $time;
}
function isSimcardFree($number)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services_mobile where msisdn_sim=?", array(
    trim($number)
    ));
    if ($q->num_rows() > 0) {
        return false;
    } else {
        return $q->row();
    }
}

function isTeumSimcardFree($number)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_reseller_simcard where simcard=? and serviceid is NULL", array(
    trim($number)
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}

function getPdfTemplate($name, $companyid, $language)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $query  = "select * from a_products_pdf where companyid = " . $companyid . " and name like '" . $name . "' and language='" . $language . "'";
    $q      = $ci->db->query($query);
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getLastFileDate($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_sepa_items where companyid = ? order by date_transaction desc LIMIT 1", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->date_transaction;
    } else {
        return false;
    }
}
function getMvnoidbySN($sn, $msisdn)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select b.* from a_services_mobile a left join a_clients b on b.id=a.userid where a.msisdn_sn = ?  and a.msisdn = ?", array(
    $sn,
    $msisdn
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->mvno_id;
    } else {
        return "NA";
    }
}
function getWhmcsid($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where mageboid = ?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return false;
    }
}
function has_sso($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients_sso where userid= ? and valid=?", array(
    $id,
    '1'
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->username;
    } else {
        return false;
    }
}
function validate_deltausername($username)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients_sso where username= ?", array(
    $username
    ));
    if ($q->num_rows() > 0) {
        return false;
    } else {
        return true;
    }
}
function getSimType($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_mvno where companyid= ?", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        if ($q->row()->SimType == "PSEUDO POST PAID") {
            return "PRE PAID";
        } else {
            return $q->row()->SimType;
        }
    } else {
        return "POST PAID";
    }
}
function validate_email($email, $cid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where email LIKE ? and companyid=?", array(
    $email,
    $cid
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function hasPromotion($orderid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.promocode,b.notes from tblorders a left join tblpromotions b on b.code=a.promocode where a.id = ?", array(
    $orderid
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getCompanydataByUserID($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.*,b.portal_url,b.email_notification from a_clients a left join a_mvno b on b.companyid=a.companyid where a.id=?", array(
    $id
    ));
    return $q->row();
}
function generateToken()
{
    $key = encrypt_decrypt($email, 'encrypt');
}
function isStolen($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_mobile_stolen_number where serviceid=? and status=?", array(
    $id,
    1
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function test_const()
{
    $ci =& get_instance();
    echo COMPANYID;
}
function getPicture($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_admin where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->picture;
    } else {
        return 'nopic.png';
    }
}
function getPicturebyName($name)
{
    return 'nopic.png';
}
function count_invoices($mageboid, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('magebo', true);
    $q      = $ci->db->query("select a.* from tblInvoice a left join tblAddress b on b.iAddressNbr=b.iAddressNbr where b.iCompanyNbr=? and a.iAddressNbr=?", array(
    $companyid,
    $mageboid
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function getDeltaId($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where id=?", array(
    $id
    ));
    return $q->row()->mvno_id;
}
function count_services($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services where userid=? and status =?", array(
    $id,
    'Active'
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function get_client_ip_env($ip, $cid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_whitelist_ip where ip like ? and companyid=?", array(
    $ip,
    $cid
    ));
    if ($q->num_rows() > 0) {
        return $ip;
    } else {
        return false;
    }
}
function getAddonPrice($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from tblpricing where type=? and relid=?", array(
    'addon',
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->monthly;
    } else {
        return '0.00';
    }
}
function isMvno_Exist($id, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where mvno_id=? and companyid=?", array(
    $id,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return false;
    }
}
function getBundleId($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products_mobile_bundles where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->bundleid;
    } else {
        return false;
    }
}

function getBundlePricebyId($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products_mobile_bundles where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->recurring_total;
    } else {
        return false;
    }
}
function getBundleName($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products_mobile_bundles where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->bundleid;
    } else {
        return false;
    }
}
function getLastCompanyid()
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_mvno order by companyid desc limit 1");

    return $q->row()->companyid+1;
}
function getBundleMageboId($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products_mobile_bundles where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->bundleid;
    } else {
        return false;
    }
}
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
function getMageboProduct($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getMonthDif($date1, $date2)
{
    $ts1    = strtotime($date1);
    $ts2    = strtotime($date2);
    $year1  = date('Y', $ts1);
    $year2  = date('Y', $ts2);
    $month1 = date('m', $ts1);
    $month2 = date('m', $ts2);
    return (($year2 - $year1) * 12) + ($month2 - $month1);
}
function showIncomingCdr($pid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products where id=?", array(
    $pid
    ));
    if ($q->num_rows() > 0) {
        if ($q->row()->show_incoming_cdr == 1) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
function getMageboProductAddons($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products_mobile_bundles where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getpType($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        if ($q->row()->product_sub_type == "Prepaid") {
            return "PRE PAID";
        } else {
            return "POST PAID";
        }
    } else {
        return "POST PAID";
    }
}
function hasPrepaidProduct($id, $resellerid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products a left join a_reseller_products b on b.productid=a.id where companyid=? and product_sub_type=? and b.agentid=?", array(
    $id,
    'Prepaid',
    $resellerid
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function getAddonInformation($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products_mobile_bundles where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}

function getTotalRecurringfromAddon($addonid)
{
    foreach ($array as $key => $value) {
        if ($value > 1) {
            $res[] = $key;
        }
    }
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where("id", $addonid);
    $q = $ci->db->get('a_products_mobile_bundles');
    if ($q->num_rows() > 0) {
        return $q->row()->recurring_total;
    } else {
        return "0.00";
    }
}

function getTotalRecurringfromAddons($array)
{
    foreach ($array as $key => $value) {
        if ($value > 1) {
            $res[] = $key;
        }
    }
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->select('SUM(recurring_total) AS amount', false);
    $ci->db->where_in("id", $res);
    $q = $ci->db->get('a_products_mobile_bundles');
    if ($q->num_rows() > 0) {
        return $q->row()->amount;
    } else {
        return "0.00";
    }
}
function getStafRoles()
{
    $ci =& get_instance();
    $result[] = array(
    'description' => 'None',
    'name' => 'none'
    );
    $result[] = array(
    'description' => 'Superadmin All Permissions',
    'name' => 'superadmin'
    );
    $ci->db   = $ci->load->database('default', true);
    $q        = $ci->db->query("select * from a_role where companyid=? and name != ?", array(
    $_SESSION['cid'],
    'superadmin'
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result_array() as $row) {
            $result[] = $row;
        }
    }
    return $result;
}
function is_admin_exists($email)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_admin where email like ? and companyid=?", array(
    $email,
    $_SESSION['cid']
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function get_promotion_auto_assign($pid)
{
    $ci =& get_instance();
    $promotions = false;
    $ci->db     = $ci->load->database('default', true);
    $q          = $ci->db->query("select * from a_products where id =? and promotions is not NULL", array(
    $pid
    ));
    if ($q->num_rows() > 0) {
        $promotions = $q->row()->promotions;
    }
    return $promotions;
}
function validate_admin_email($email, $id, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_admin where email like ? and id != ? and companyid=?", array(
    $email,
    $id,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function allowed_controller($folder, $controller, $method, $role)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_admin_permissions where folder=? and controller=? and method=?", array(
    $folder,
    $controller,
    $method
    ));
    if ($q->num_rows() > 0) {
        $s = $ci->db->query("select * from a_admin_roles where rolename=? and perms=?", array(
        $role,
        $q->row()->id
        ));
        if ($s->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
function objectToArray($d)
{
    if (is_object($d)) {
        $d = get_object_vars($d);
    }
    if (is_array($d)) {
        return array_map(__FUNCTION__, $d);
    } else {
        return $d;
    }
}
function ogm($Getal1)
{
    $str_length = 10;
    $Getal1     = substr("0000000000{$Getal1}", -$str_length);
    $Rest       = $Getal1 % 97;
    if ($Rest < 10) {
        $Nul  = 0;
        $Rest = $Nul . $Rest;
    } elseif ($Rest == 0) {
        $Rest = 97;
    }
    $Merge = $Getal1 . $Rest;
    $Ogm   = "+++" . substr($Merge, 0, 3) . "/" . substr($Merge, 3, 4) . "/" . substr($Merge, 7, 5) . "+++";
    return $Ogm;
}
function ratingtype($ud)
{
    if ($ud == 1) {
        return 'phone';
    } elseif ($ud == 2) {
        return 'envelope';
    } elseif ($ud == 3) {
        return 'globe';
    } elseif ($ud == 4) {
        return 'cubes';
    } else {
        return 'cubes';
    }
}

function ratingtypev2($ud)
{
    if ($ud == 1) {
        return 'phone-portrait';
    } elseif ($ud == 2) {
        return 'mail';
    } elseif ($ud == 3) {
        return 'globe';
    } elseif ($ud == 4) {
        return 'cube';
    } else {
        return 'cube';
    }
}
function getTraficName($id)
{
    if ($id == 5) {
        return 'SMS';
    } elseif ($id == 1) {
        return 'VOICE';
    } elseif ($id == 2) {
        return 'DATA';
    }
}
function human_filesize($bytes, $decimals = 2)
{
    $res = $bytes * (1 / (1024 * 1024));
    return number_format(round($res, 2)) . ' MB';
}
function encrypt_decrypt($action, $string)
{
    $output         = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key     = 'EXOCOM';
    $secret_iv      = 'CRM2017';
    $key            = hash('sha256', $secret_key);
    $iv             = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } elseif ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}
function random_str($type = 'alphanum', $length = 12)
{
    switch ($type) {
        case 'basic':
            return mt_rand();
        break;
        case 'alpha':
        case 'alphanum':
        case 'alphacaps':
        case 'num':
        case 'nozero':
            $seedings              = array();
            $seedings['alpha']     = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $seedings['alphacaps'] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $seedings['alphanum']  = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $seedings['num']       = '0123456789';
            $seedings['nozero']    = '123456789';
            $pool                  = $seedings[$type];
            $str                   = '';
            for ($i = 0; $i < $length; $i++) {
                $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
            }
            return $str;
        break;
        case 'unique':
        case 'md5':
            return md5(uniqid(mt_rand()));
        break;
    }
}
function getNotificationEmail($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_mvno where companyid=?", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->email_notification;
    } else {
        return 0;
    }
}
function getPlatform($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services_mobile where serviceid=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->platform;
    } else {
        return 0;
    }
}

function getPlatformbyPid($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("SELECT b.* FROM `a_products` a left join a_api_data b on b.id=a.api_id
 where a.id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->platform;
    } else {
        return 0;
    }
}
function getNotificationOrderEmail($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_mvno where companyid=? and email_on_order=1", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->email_notification;
    } else {
        return false;
    }
}
function xml_to_array($contents, $get_attributes = 0)
{
    if (!$contents) {
        return array();
    }
    if (!function_exists('xml_parser_create')) {
        return array();
    }
    $parser = xml_parser_create();
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, $contents, $xml_values);
    xml_parser_free($parser);
    if (!$xml_values) {
        return;
    }
    $xml_array   = array();
    $parents     = array();
    $opened_tags = array();
    $arr         = array();
    $current =& $xml_array;
    foreach ($xml_values as $data) {
        unset($attributes, $value);
        extract($data);
        $result = '';
        if ($get_attributes) {
            $result = array();
            if (isset($value)) {
                $result['value'] = $value;
            }
            if (isset($attributes)) {
                foreach ($attributes as $attr => $val) {
                    if ($get_attributes == 1) {
                        $result['attr'][$attr] = $val;
                    }
                }
            }
        } elseif (isset($value)) {
            $result = $value;
        }
        if ($type == "open") {
            $parent[$level - 1] =& $current;
            if (!is_array($current) or (!in_array($tag, array_keys($current)))) {
                $current[$tag] = $result;
                $current =& $current[$tag];
            } else {
                if (isset($current[$tag][0])) {
                    array_push($current[$tag], $result);
                } else {
                    $current[$tag] = array(
                    $current[$tag],
                    $result
                    );
                }
                $last = count($current[$tag]) - 1;
                $current =& $current[$tag][$last];
            }
        } elseif ($type == "complete") {
            if (!isset($current[$tag])) {
                $current[$tag] = $result;
            } else {
                if ((is_array($current[$tag]) and $get_attributes == 0) or (isset($current[$tag][0]) and is_array($current[$tag][0]) and $get_attributes == 1)) {
                    array_push($current[$tag], $result);
                } else {
                    $current[$tag] = array(
                    $current[$tag],
                    $result
                    );
                }
            }
        } elseif ($type == 'close') {
            $current =& $parent[$level - 1];
        }
    }
    return ($xml_array);
}
function get_reseller_style($cid)
{
    if (file_exists(FCPATH . 'assets/clear/reseller/' . $cid . '-style.css')) {
        return $cid;
    } else {
        return 'default';
    }
}


function reseller_theme($companyid)
{
    //$CI =& get_instance();
    $setting = globofix($companyid);
    if (file_exists(APPPATH . 'views/themes/' . $setting->default_theme . '/reseller/' . $companyid)) {
        return 'themes/' . $setting->default_theme . '/reseller/' . $companyid . '/';
    } else {
        return 'themes/' . $setting->default_theme . '/reseller/default/';
    }
}
function admin_theme($theme)
{  echo $theme;
    // return 'themes/' . $theme . '/admins/';
return 'themes/clear/admins/';
}
function array_to_object($array)
{
    $obj = new stdClass;
    foreach ($array as $k => $v) {
        if (strlen($k)) {
            if (is_array($v)) {
                $obj->{$k} = array_to_object($v);
            } else {
                $obj->{$k} = $v;
            }
        }
    }
    return $obj;
}
function client_theme($theme)
{
    return 'themes/' . $theme . '/clients/';
}
function service_theme($server_type)
{

  //echo $server_type;
    if (in_array($server_type, array(
    'artilium',
    'mobile',
    'artiliumlegacy'
    ))) {
        return "subscriptions_detail_mobile";
    } elseif ($server_type == "billi") {
        return "subscriptions_detail_billi";
    } elseif ($server_type == "edp") {
        return "subscriptions_detail_edp";
    } elseif ($server_type == "speakup") {
        return "subscriptions_detail_speakup";
    } elseif ($server_type == "edpvoip") {
        return "subscriptions_detail_edpvoip";
    } elseif ($server_type == "ithagi") {
        return "subscriptions_detail_ithagi";
    } elseif ($server_type == "xdsl") {
        return "subscriptions_detail_xdsl";
    } elseif ($server_type == "teum") {
        return "subscriptions_detail_teum";
    } else {
        return "subscriptions_nosub";
    }
}
function extend_uri()
{
    $CI =& get_instance();
    $uri = $CI->uri->uri_string();
    return explode('/', $uri);
}
function getiAddressNbr($userid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where id=?", array(
    $userid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->mageboid;
    } else {
        return 0;
    }
}

function getClientbyMageboid($mageboid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where mageboid=?", array(
    $mageboid
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getMageboid($id)
{
    $fieldid = getCustomfieldsidbyName('iAddressNbr');
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from tblcustomfieldsvalues where relid=? and fieldid=?", array(
    $id,
    $fieldid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->value;
    } else {
        return 0;
    }
}
function dtf()
{
    return '
   <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
   <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
   ';
}
function dth()
{
    return '
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>
     <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
    ';
}
function getInvoiceMonth($invoiceid)
{
    $whmcsUrl = "https://probile.united-telecom.be/";
    $ch       = curl_init();
    curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'api.php?step=7&invoiceid=' . $invoiceid);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    $response = curl_exec($ch);
    if (curl_error($ch)) {
        die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);
    $jsondata = json_decode($response, true);
    return $jsondata;
}
function widget_mvno_kyc()
{
    $title      = 'KYC Overview';
    $whmcsUrl   = "https://probile.united-telecom.be/";
    $username   = "admin";
    $password   = "un!t3d";
    $postfields = array(
    'username' => $username,
    'password' => md5($password),
    'action' => 'dwhgetkyc',
    'responsetype' => 'json'
    );
    $ch         = curl_init();
    curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    $response = curl_exec($ch);
    if (curl_error($ch)) {
        die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);
    $jsondata = json_decode($response, true);
    $content  = "<script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization',
      'version':'1.1','packages':['corechart','bar']}]}\"></script>
      <script type=\"text/javascript\">
      google.setOnLoadCallback(function() {
   var data = new google.visualization.DataTable();
       data.addColumn({ type: 'date', id: 'DateTm' });
       data.addColumn({ type: 'number', id: 'CheckIns' });

 data.addRows([" . $jsondata['message'] . "]);
 var hTICKS= data.getDistinctValues(0);

     var options = {
       title: 'KYC',
       //width: 550,
       height: 400,
       legend: { position: \"none\" },
       chartArea: {width: '90%', height: '45%'},
        animation:{
         duration: 3000,
         easing: 'out',
         startup: true
       },
       hAxis: {
               //direction: -1,
               slantedText: true,
               slantedTextAngle: 90 ,
               format: 'MM/dd/yy',
               ticks : hTICKS,
               gridlines:{ color:'transparent' }
           },
     };
     console.log(data);
     var chart = new google.visualization.ColumnChart(document.getElementById('chart_divkyc'));
     chart.draw(data, options);
 })
    </script>

    <div id='chart_divkyc'> </div>";
    return $content;
}
function format_date($date)
{
    $d = explode(' ', $date);
    return $d[0];
}
function get_magebo_customer($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query('select a.* from a_clients a where a.mageboid=?', array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->firstname . ' ' . $q->row()->lastname;
    } else {
        return 'Unknown';
    }
}
function chartdata_kyc()
{
    $ci =& get_instance();
    $ci->config->load('artadwh');
    $ci->db  = $ci->load->database('dwh', true);
    $restest = $ci->db->query("SELECT TOP 14 Act.ModDate regdate,COUNT(DISTINCT  Act.szPinOrTable) as cnt
       FROM
       (SELECT szPinOrTable,Reseller,lParamID,szString1,ModState,SUBSTRING(CONVERT(varchar,Modify,120),1,10) as  ModDate
       FROM staging.parameters
       WHERE lParamID=80001 and ModState='A') Act
       LEFT JOIN
       (SELECT szPinOrTable,Reseller,lParamID,szString1,ModState,SUBSTRING(CONVERT(varchar,Modify,120),1,10) AS ModDate
       FROM staging.parameters
       WHERE lParamID=80003 and ModState='A') L
       ON Act.szPinOrTable=L.szPinOrTable
       WHERE Act.Reseller='" . $ci->config->item('ResellerKYC') . "'
       GROUP BY Act.ModDate ORDER BY Act.ModDate DESC");
    $i       = 0;
    while ($lineres = $restest->row_array()) {
        $res[$i]['regdate'] = $lineres['regdate'];
        $res[$i]['cnt']     = $lineres['cnt'];
        $ST                 = new DateTime($lineres['regdate']);
        $ChartDate          = "new Date(" . $ST->format('Y') . ", " . ($ST->format('n') - 1) . ", " . $ST->format('j') . ")";
        $dataTableRowsAR    = $dataTableRowsAR . "[" . $ChartDate . "," . $res[$i]['cnt'] . "],";
        $i++;
    }
    $dataTableRowsAR = trim($dataTableRowsAR, ",");
    return $dataTableRowsAR;
}
function custom_echo($x, $length)
{
    if (strlen($x) <= $length) {
        echo $x;
    } else {
        $y = substr($x, 0, $length) . '...';
        echo $y;
    }
}
function getPromoname($code)
{
    if (!$code) {
        return "None";
    }
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query('select a.* from a_promotion a where a.promo_code=?', array(
    $code
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->promo_name;
    } else {
        return 'Unknown';
    }
}

function getPromonamebyId($code)
{
    if (!$code) {
        return "None";
    }
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query('select a.* from a_promotion a where a.id=?', array(
    $code
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->promo_name;
    } else {
        return 'Unknown';
    }
}


function getInvoiceDescription($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query('select GPcRemark,iInvoiceDescription from a_products where id = ?', array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getCountryName($country)
{
    $c = getCountries();
    foreach ($c as $key => $k) {
        if ($key == $country) {
            return $k;
        }
    }
}
function getCountryNameLang($country, $lang)
{
    if ($lang == "dutch") {
        $t = "NL";
    } elseif ($lang == "english") {
        $t = "EN";
    } elseif ($lang == "french") {
        $t = "FR";
    } else {
        $t = "EN";
    }
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query('select * from a_countries_full where code = ?', array(
    $country
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->row_array() as $key => $value) {
            if ($key == $t) {
                $res = $value;
            }
        }
        if ($res) {
            return $res;
        } else {
            return getCountryName($country);
        }
    } else {
        return getCountryName($country);
    }
}
function getclient_sms_number($userid)
{
    $numbers = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query('select * from a_sms_notifications where userid = ?', array(
    $userid
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $numbers[] = $row->msisdn;
        }
    }
    return $numbers;
}
function getCountries()
{
    $countries = array(
    'AF' => 'Afghanistan',
    'AX' => 'Aland Islands',
    'AL' => 'Albania',
    'DZ' => 'Algeria',
    'AS' => 'American Samoa',
    'AD' => 'Andorra',
    'AO' => 'Angola',
    'AI' => 'Anguilla',
    'AQ' => 'Antarctica',
    'AG' => 'Antigua And Barbuda',
    'AR' => 'Argentina',
    'AM' => 'Armenia',
    'AW' => 'Aruba',
    'AU' => 'Australia',
    'AT' => 'Austria',
    'AZ' => 'Azerbaijan',
    'BS' => 'Bahamas',
    'BH' => 'Bahrain',
    'BD' => 'Bangladesh',
    'BB' => 'Barbados',
    'BY' => 'Belarus',
    'BE' => 'Belgium',
    'BZ' => 'Belize',
    'BJ' => 'Benin',
    'BM' => 'Bermuda',
    'BT' => 'Bhutan',
    'BO' => 'Bolivia',
    'BA' => 'Bosnia And Herzegovina',
    'BW' => 'Botswana',
    'BV' => 'Bouvet Island',
    'BR' => 'Brazil',
    'IO' => 'British Indian Ocean Territory',
    'BN' => 'Brunei Darussalam',
    'BG' => 'Bulgaria',
    'BF' => 'Burkina Faso',
    'BI' => 'Burundi',
    'KH' => 'Cambodia',
    'CM' => 'Cameroon',
    'CA' => 'Canada',
    'CV' => 'Cape Verde',
    'KY' => 'Cayman Islands',
    'CF' => 'Central African Republic',
    'TD' => 'Chad',
    'CL' => 'Chile',
    'CN' => 'China',
    'CX' => 'Christmas Island',
    'CC' => 'Cocos (Keeling) Islands',
    'CO' => 'Colombia',
    'KM' => 'Comoros',
    'CG' => 'Congo',
    'CD' => 'Congo, Democratic Republic',
    'CK' => 'Cook Islands',
    'CR' => 'Costa Rica',
    'CI' => 'Cote D\'Ivoire',
    'HR' => 'Croatia',
    'CU' => 'Cuba',
    'CY' => 'Cyprus',
    'CZ' => 'Czech Republic',
    'DK' => 'Denmark',
    'DJ' => 'Djibouti',
    'DM' => 'Dominica',
    'DO' => 'Dominican Republic',
    'EC' => 'Ecuador',
    'EG' => 'Egypt',
    'SV' => 'El Salvador',
    'GQ' => 'Equatorial Guinea',
    'ER' => 'Eritrea',
    'EE' => 'Estonia',
    'ET' => 'Ethiopia',
    'FK' => 'Falkland Islands (Malvinas)',
    'FO' => 'Faroe Islands',
    'FJ' => 'Fiji',
    'FI' => 'Finland',
    'FR' => 'France',
    'GF' => 'French Guiana',
    'PF' => 'French Polynesia',
    'TF' => 'French Southern Territories',
    'GA' => 'Gabon',
    'GM' => 'Gambia',
    'GE' => 'Georgia',
    'DE' => 'Germany',
    'GH' => 'Ghana',
    'GI' => 'Gibraltar',
    'GR' => 'Greece',
    'GL' => 'Greenland',
    'GD' => 'Grenada',
    'GP' => 'Guadeloupe',
    'GU' => 'Guam',
    'GT' => 'Guatemala',
    'GG' => 'Guernsey',
    'GN' => 'Guinea',
    'GW' => 'Guinea-Bissau',
    'GY' => 'Guyana',
    'HT' => 'Haiti',
    'HM' => 'Heard Island & Mcdonald Islands',
    'VA' => 'Holy See (Vatican City State)',
    'HN' => 'Honduras',
    'HK' => 'Hong Kong',
    'HU' => 'Hungary',
    'IS' => 'Iceland',
    'IN' => 'India',
    'ID' => 'Indonesia',
    'IR' => 'Iran, Islamic Republic Of',
    'IQ' => 'Iraq',
    'IE' => 'Ireland',
    'IM' => 'Isle Of Man',
    'IL' => 'Israel',
    'IT' => 'Italy',
    'JM' => 'Jamaica',
    'JP' => 'Japan',
    'JE' => 'Jersey',
    'JO' => 'Jordan',
    'KZ' => 'Kazakhstan',
    'KE' => 'Kenya',
    'KI' => 'Kiribati',
    'KR' => 'Korea',
    'KW' => 'Kuwait',
    'KG' => 'Kyrgyzstan',
    'LA' => 'Lao People\'s Democratic Republic',
    'LV' => 'Latvia',
    'LB' => 'Lebanon',
    'LS' => 'Lesotho',
    'LR' => 'Liberia',
    'LY' => 'Libyan Arab Jamahiriya',
    'LI' => 'Liechtenstein',
    'LT' => 'Lithuania',
    'LU' => 'Luxembourg',
    'MO' => 'Macao',
    'MK' => 'Macedonia',
    'MG' => 'Madagascar',
    'MW' => 'Malawi',
    'MY' => 'Malaysia',
    'MV' => 'Maldives',
    'ML' => 'Mali',
    'MT' => 'Malta',
    'MH' => 'Marshall Islands',
    'MQ' => 'Martinique',
    'MR' => 'Mauritania',
    'MU' => 'Mauritius',
    'YT' => 'Mayotte',
    'MX' => 'Mexico',
    'FM' => 'Micronesia, Federated States Of',
    'MD' => 'Moldova',
    'MC' => 'Monaco',
    'MN' => 'Mongolia',
    'ME' => 'Montenegro',
    'MS' => 'Montserrat',
    'MA' => 'Morocco',
    'MZ' => 'Mozambique',
    'MM' => 'Myanmar',
    'NA' => 'Namibia',
    'NR' => 'Nauru',
    'NP' => 'Nepal',
    'NL' => 'The Netherlands',
    'AN' => 'Netherlands Antilles',
    'NC' => 'New Caledonia',
    'NZ' => 'New Zealand',
    'NI' => 'Nicaragua',
    'NE' => 'Niger',
    'NG' => 'Nigeria',
    'NU' => 'Niue',
    'NF' => 'Norfolk Island',
    'MP' => 'Northern Mariana Islands',
    'NO' => 'Norway',
    'OM' => 'Oman',
    'PK' => 'Pakistan',
    'PW' => 'Palau',
    'PS' => 'Palestinian Territory, Occupied',
    'PA' => 'Panama',
    'PG' => 'Papua New Guinea',
    'PY' => 'Paraguay',
    'PE' => 'Peru',
    'PH' => 'Philippines',
    'PN' => 'Pitcairn',
    'PL' => 'Poland',
    'PT' => 'Portugal',
    'PR' => 'Puerto Rico',
    'QA' => 'Qatar',
    'RE' => 'Reunion',
    'RO' => 'Romania',
    'RU' => 'RUSSIA',
    'RW' => 'Rwanda',
    'BL' => 'Saint Barthelemy',
    'SH' => 'Saint Helena',
    'KN' => 'Saint Kitts And Nevis',
    'LC' => 'Saint Lucia',
    'MF' => 'Saint Martin',
    'PM' => 'Saint Pierre And Miquelon',
    'VC' => 'Saint Vincent And Grenadines',
    'WS' => 'Samoa',
    'SM' => 'San Marino',
    'ST' => 'Sao Tome And Principe',
    'SA' => 'Saudi Arabia',
    'SN' => 'Senegal',
    'RS' => 'Serbia',
    'SC' => 'Seychelles',
    'SL' => 'Sierra Leone',
    'SG' => 'Singapore',
    'SK' => 'Slovakia',
    'SI' => 'Slovenia',
    'SB' => 'Solomon Islands',
    'SO' => 'Somalia',
    'ZA' => 'South Africa',
    'GS' => 'South Georgia And Sandwich Isl.',
    'ES' => 'Spain',
    'LK' => 'Sri Lanka',
    'SD' => 'Sudan',
    'SR' => 'Suriname',
    'SJ' => 'Svalbard And Jan Mayen',
    'SZ' => 'Swaziland',
    'SE' => 'Sweden',
    'CH' => 'Switzerland',
    'SY' => 'Syrian Arab Republic',
    'TW' => 'Taiwan',
    'TJ' => 'Tajikistan',
    'TZ' => 'Tanzania',
    'TH' => 'Thailand',
    'TL' => 'Timor-Leste',
    'TG' => 'Togo',
    'TK' => 'Tokelau',
    'TO' => 'Tonga',
    'TT' => 'Trinidad And Tobago',
    'TN' => 'Tunisia',
    'TR' => 'Turkey',
    'TM' => 'Turkmenistan',
    'TC' => 'Turks And Caicos Islands',
    'TV' => 'Tuvalu',
    'UG' => 'Uganda',
    'UA' => 'Ukraine',
    'AE' => 'United Arab Emirates',
    'UK' => 'United Kingdom',
    'US' => 'USA',
    'UM' => 'United States Outlying Islands',
    'UY' => 'Uruguay',
    'UZ' => 'Uzbekistan',
    'VU' => 'Vanuatu',
    'VE' => 'Venezuela',
    'VN' => 'Viet Nam',
    'VG' => 'Virgin Islands, British',
    'VI' => 'Virgin Islands, U.S.',
    'WF' => 'Wallis And Futuna',
    'EH' => 'Western Sahara',
    'YE' => 'Yemen',
    'ZM' => 'Zambia',
    'ZW' => 'Zimbabwe'
    );
    return $countries;
}
function getLanguages()
{
    $lang = array(
    'dutch' => 'Dutch',
    'french' => 'French',
    'english' => 'English',
    'german' => 'German',
    'japanese' => 'Japanese',
    'spanish' => 'Spanish'
    );
    return $lang;
}
function getDefaultPackageid($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select id from a_products where companyid=?", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return 0;
    }
}
function getDefaultAgentid($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select id,agent from a_clients_agents where companyid=? and isdefault=1", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return 0;
    }
}
function get_agents($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select id,agent from a_clients_agents where companyid=?", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->result();
    } else {
        return false;
    }
}
function getContactTypeid($ContactType2Id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients_agents where id=?", array(
    $ContactType2Id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->ContactType2Id;
    } else {
        return 7;
    }
}
function validate_cd($date)
{
    $date = explode('-', $date);
    if (count($date) != 3) {
        return false;
    } else {
        $dateString      = $date[2] . '-' . $date[0] . '-' . $date[1];
        $lastDateOfMonth = date("t", strtotime($dateString));
        $m               = (int) $date[0];
        if ($m <= 12) {
            if (ltrim($date[1], '0') <= $lastDateOfMonth) {
                if ($date[2] >= date('Y')) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
function convert_contract($date)
{
    $d = explode('-', $date);
    return $d['2'] . '-' . $d['0'] . '-' . $d['1'];
}
function convert_to_contract($date)
{
    $d = explode('-', $date);
    return $d['1'] . '-' . $d['2'] . '-' . $d['0'];
}
function convert_from_be_nl($date)
{
    return str_replace('/', '-', $date);
}

function convert_date_to_uk_from_slash($date)
{
    if ($date) {
        $o = explode(' ', $date);
        $d = explode('/', $o[0]);
        return $d[2].'-'.$d[1].'-'.$d[0].' '.$o[1];
    }
    return  '';
}

function convert_date_to_uk_from_dash($date)
{
    if ($date) {
        $o = explode(' ', $date);
        $d = explode('-', $o[0]);
        return $d[2].'-'.$d[1].'-'.$d[0];
    }
    return  '';
}
function convert_to_dutch_format($date)
{
    $d = explode('-', $date);
    return $d['2'] . '-' . $d['1'] . '-' . $d['0'];
}
function convert_contract_dutch($date)
{
    $d         = explode('-', $date);
    $monthNum  = $d[0];
    $dateObj   = DateTime::createFromFormat('!m', $monthNum);
    $monthName = $dateObj->format('F');
    return $d['1'] . ' ' . $monthName . ' ' . $d['2'];
}
function convert_contract_dutch_number($date)
{
    if ($date) {
        $d         = explode('-', $date);
        $monthNum  = $d[0];
        $dateObj   = DateTime::createFromFormat('!m', $monthNum);
        $monthName = $dateObj->format('m');
        return $d['1'] . '-' . $monthName . '-' . $d['2'];
    } else {
        return date('Y-m-d');
    }
}
function convert_invoice_date($date)
{
    $d = explode('-', $date);
    return $d['1'] . '/' . $d['2'] . '/' . $d['0'];
}
function getCustomerVat($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where id=?", array(
    $id
    ));
    return $q->row()->vat;
}
function getCustomerCompanyName($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where id=?", array(
    $id
    ));
    return $q->row()->companyname;
}
function getCustomerName($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where id=?", array(
    $id
    ));
    return $q->row()->firstname . " " . $q->row()->lastname;
}
function logClient($data)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->insert('a_logs', $data);
}
function getPriceRecurring($id, $duration)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);


    if ($duration == 1) {
        $ci->db->select('monthly as price');
    } elseif ($duration == 3) {
        $ci->db->select('quarterly as price');
    } elseif ($duration == 6) {
        $ci->db->select('semi_annually as price');
    } elseif ($duration == 12) {
        $ci->db->select('annually as price');
    } elseif ($duration == 24) {
        $ci->db->select('bienially as price');
    } elseif ($duration == 36) {
        $ci->db->select('triennially as price');
    } else {
        $ci->db->select('annually as price');
    }
    $ci->db->where('packageid', $id);
    $q = $ci->db->get('a_pricing_specific');
    if ($q->num_rows()>0) {
        return number_format($q->row()->price, 4);
    } else {
        $s = $ci->db->query("select * from a_products where id=?", array($id));
        if ($s->num_rows()>0) {
            return number_format($s->row()->recurring_total, 4);
        } else {
            return "0";
        }
    }
}




function getContactId($gid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('id', $gid);
    $q = $ci->db->get('a_products_group');
    if ($q->num_rows() > 0) {
        return $q->row()->ContactId;
    } else {
        return 7;
    }
}
function getGroupid($pid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select b.* from a_services a left join a_products b on b.id=a.packageid where a.id=?", array(
    $pid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->gid;
    }
}
function getProductReloadAmount($gid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('id', $gid);
    $q = $ci->db->get('a_products_group');
    if ($q->num_rows() > 0) {
        return $q->row()->initial_reload_amount;
    } else {
        return 0;
    }
}
function getErrorSwap($servicename, $code)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services_mobile_error where servicename=? and error_code=?", array(
    $servicename,
    $code
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->description;
    } else {
        return 'Unkown';
    }
}
function getPricing($pid, $type)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where('id', $pid);
    $q = $ci->db->get('a_products');
    if ($type = "swap") {
        return $q->row()->mobile_swapcost;
    } elseif ($type == "change") {
        return $q->row()->mobile_changecost;
    } elseif ($type == "setup") {
        return $q->row()->setup;
    } else {
        return "0";
    }
}
function logAdmin($data)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->insert('a_logs', $data);
}
function getAdmins()
{
    $twoHoursAgo = strtotime("-6 hours");
    $ci =& get_instance();
    $companyid = $ci->session->cid;
    $ci->db    = $ci->load->database('default', true);
    $q         = $ci->db->query("select  id,concat(firstname,' ',lastname) as name,username,picture,phonenumber,lastseen from  a_admin where companyid=?  order by lastseen desc", array(
    $companyid
    ));
    return $q->result();
}
function convert_seconds($seconds)
{
    $timezone = +1;
    return gmdate("Y-m-d H:i:s", $seconds + 3600 * ($timezone + date("I")));
}
function isActiveSubscription($g)
{
    $d1     = new DateTime($g->iStartYear . "-" . $g->iStartMonth . "-01");
    $d2     = new DateTime(date('Y-m-d'));
    $months = $d1->diff($d2)->m + ($d1->diff($d2)->y * 12);
    if ($g->iTotalMonths >= $months) {
        return true;
    } else {
        return false;
    }
}
function next_month_date()
{
    return date('Y-m-01', strtotime('+1 month'));
}
function get_online_admin()
{
    $now = time() - 114000;
    $ci =& get_instance();
    $companyid = $ci->session->cid;
    $ci->db    = $ci->load->database('default', true);
    $q         = $ci->db->query("select  id,concat(firstname,' ',lastname) as name,username,picture,lastseen from  a_admin where companyid=? and lastseen >= ? order by lastseen desc", array(
    $companyid,
    $now
    ));
    if ($q->num_rows()) {
        return $q->result();
    } else {
        return false;
    }
}
function username_taken($username)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_admin where username like ?", array(
    $username
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function changetime($time)
{
    $d = new DateTime($time, new DateTimeZone('Europe/Brussels'));
    return $d->getTimestamp();
}
function logTicketClient($id, $message)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->insert('a_helpdesk_log', array(
    'ticketid' => $id,
    'companyid' => $_SESSION['cid'],
    'description' => $message
    ));
}
function getDeptName($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select  name from a_helpdesk_department where id=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->name;
    } else {
        return 'Support Department';
    }
}
function getTrendcatName($id)
{
    $ci =& get_instance();
    $q = $ci->db->query("select concat(b,' ',c) as name from import_agent where a=?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->name;
    } else {
        return 'Unknown';
    }
}
function getAdminName($id)
{
    $ci =& get_instance();
    $q = $ci->db->query("select concat(firstname,' ',lastname) as name from a_admin where id=?", array(
    $id
    ));
    return $q->row()->name;
}
function getAdmin($id)
{
    $ci =& get_instance();
    $q = $ci->db->query("select concat(firstname,' ',lastname) as name,email from a_admin where id=?", array(
    $id
    ));
    return $q->row();
}
function getMyPicture($id)
{
    $ci =& get_instance();
    $q = $ci->db->query("select * from a_admin where id=?", array(
    $id
    ));
    return $q->row()->picture;
}

function getMyPictureforTicket($id)
{
    $ci =& get_instance();
    if ($id == "0") {
        return base_url().'assets/img/staf/nopic.png';
    }

    $q = $ci->db->query("select * from a_admin where id=?", array(
    $id
    ));
    return base_url().'assets/img/staf/'.$q->row()->picture;
}
function getClientbyMvnoid($id)
{
    $ci =& get_instance();
    $q = $ci->db->query("select * from a_clients where mvno_id like ? and companyid='54'", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return 0;
    }
}
function getDepartmentid($name)
{
    if ($name == trim('technical')) {
        $id = 14;
    } elseif ($name == trim('supplier')) {
        $id = 17;
    } elseif ($name == trim('settlement')) {
        $id = 12;
    } elseif ($name == trim('risk')) {
        $id = 11;
    } elseif ($name == trim('Rating')) {
        $id = 10;
    } elseif ($name == trim('other')) {
        $id = 8;
    } elseif ($name == trim('Order')) {
        $id = 7;
    } elseif ($name == trim('Nummerbehoud')) {
        $id = 6;
    } elseif ($name == trim('lead')) {
        $id = 5;
    } elseif ($name == trim('financial')) {
        $id = 4;
    } elseif ($name == trim('DataBundel')) {
        $id = 3;
    } elseif ($name == trim('contact')) {
        $id = 2;
    } elseif ($name == trim('agent')) {
        $id = 1;
    } else {
        $id = 8;
    }
    return $id;
}
function getCustomfieldsidbyName($fieldname)
{
    return false;
}
function getCustomfieldsidbyNameProduct($fieldname)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from tblcustomfields where fieldname=? and type=?", array(
    $fieldname,
    'product'
    ));
    return $q->row()->id;
}
function getCustomfieldsidbyNameAddon($fieldname, $pid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from tblcustomfields where fieldname=? and type=? and relid=?", array(
    $fieldname,
    'addon',
    $pid
    ));
    return $q->row()->id;
}
function getUnityUsername($id, $type = "PRE PAID")
{
    if (in_array($id, array(
    53,
    54
    ))) {
        $type = "POST PAID";
    }
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_api_data where companyid=? and m_payment_type=?", array(
    $id,
    $type
    ));
    return $q->row();
}
function getUnityUsernames($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_api_data where companyid=?", array(
    $id
    ));
    return $q->result();
}
function getStatuses()
{
    return array(
    'Open',
    'In-Progress',
    'On-Hold',
    'Answered',
    'Awaiting-Reply',
    'Resolved',
    'Closed'
    );
}
function getDepartment($id)
{
    $res = array();
    $ci =& get_instance();
    $ci->db    = $ci->load->database('default', true);
    $companyid = $ci->session->cid;
    $q         = $ci->db->query("select * from a_helpdesk_department where companyid=? and id=?", array(
    $companyid,
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return array();
    }
}
function getDepartments($companyid)
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_helpdesk_department where companyid=?", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->result_array();
    } else {
        return array();
    }
}
function getSupportedLanguage($companyid)
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select val from a_configuration where companyid=? and name=?", array(
    $companyid,
    'supported_language'
    ));
    if ($q->num_rows() > 0) {
        if (!empty($q->row()->val)) {
            $res = explode(',', $q->row()->val);
        } else {
            $res = array(
            'dutch,english'
            );
        }
    } else {
        $res = array(
        'dutch,english'
        );
    }
    return $res;
}
function getTemplatename($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_email_templates where id=?", array(
    $id
    ));
    return trim($q->row()->name);
}
function hasTemplateLanguage($lang, $name, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_email_templates where name like ? and language like ? and companyid=? ", array(
    $name,
    $lang,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return false;
    }
}
function getStaf($companyid)
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select id,firstname,lastname,email from a_admin where companyid=? and master != 1 and status = 'Active'", array(
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->result_array();
    } else {
        return array();
    }
}

function getDonors($country)
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_mobile_donor_list where country=? order by operator_name asc", array(
    $country
    ));
    if ($q->num_rows() > 0) {
        return $q->result();
    } else {
        return array();
    }
}
function getTotalTickets($id)
{
    if (!empty($id)) {
        $ci =& get_instance();
        $q = $ci->db->query("select * from a_helpdesk_tickets  where userid=?", array(
        $id
        ));
        return $q->num_rows();
    } else {
        return 'Not registered';
    }
}
function getProductList()
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from tblproductgroups order by name ASC");
    if ($q->num_rows() > 0) {
        return $q->result_array();
    } else {
        return array();
    }
}
function getTarifs($cid)
{
    $res = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select id, name, name_desc from a_products_mobile_bundles where bundle_type=? and companyid=? order by name ASC", array(
    'tarif',
    $cid
    ));
    if ($q->num_rows() > 0) {
        return $q->result_array();
    } else {
        return array();
    }
}
function url_to_domain($url)
{
    $host = @parse_url($url, PHP_URL_HOST);
    if (!$host) {
        $host = $url;
    }
    if (substr($host, 0, 4) == "www.") {
        $host = substr($host, 4);
    }
    if (strlen($host) > 50) {
        $host = substr($host, 0, 47) . '...';
    }
    return $host;
}
function getEmailTemplates($companyid)
{
    $ci =& get_instance();
    $q = $ci->db->query(" SELECT name,description,date_created,date_modified,last_modifier,id
    FROM a_email_templates
     WHERE companyid='$companyid'
     AND language ='dutch'
");
}
function getInternetProducts($cid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q = $ci->db->query("SELECT id,name FROM `a_products_group` where companyid=? ORDER BY `a_products_group`.`id` DESC", array($ci->session->cid));
    return $q->result_array();
}
function getInternetProductsell($name, $companyid)
{
    $html = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.id,a.name from a_products a where a.product_type like ? and companyid= ? and status =1 order by name asc", array(
    $name,
    $companyid
    ));
    return $q->result_array();
}
function slack($message, $channel)
{
    mb_internal_encoding("UTF-8");
    $token                  = "xoxp-3463148205-3463148213-3463750905-4ab97d";
    $slackchannel           = $channel;
    $url                    = "https://slack.com/api/chat.postMessage";
    $postfieldsT["token"]   = $token;
    $postfieldsT["channel"] = $slackchannel;
    $postfieldsT["text"]    = $message;
    $ch                     = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postfieldsT);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $dataTT = curl_exec($ch);
    curl_close($ch);
}
function logEmailOut($data)
{
    $ci =& get_instance();
    $ci->load->database('default', true);
    $ci->db->insert('a_email_log', $data);
}
function exvat($vat, $price)
{
    if ($vat == 1 || $vat == 0) {
        return $price;
    } else {
        $vatDivisor     = 1 + ($vat / 100);
        $priceBeforeVat = $price / $vatDivisor;
        $vatAmount      = $price - $priceBeforeVat;
        return round($priceBeforeVat, 2);
    }
}
function exvat4($vat, $price)
{
    if ($vat == 1 || $vat == 0) {
        return $price;
    } else {
        $vatDivisor     = 1 + ($vat / 100);
        $priceBeforeVat = $price / $vatDivisor;
        $vatAmount      = $price - $priceBeforeVat;
        return round($priceBeforeVat, 4);
    }
}
function getcustomerfullname($id)
{
    $ci =& get_instance();
    $c = $ci->db->query("select * from clients where id=?", array(
    $id
    ));
    if ($c->num_rows() > 0) {
        return $c->row()->companyname . ' - ' . $c->row()->firstname . ' ' . $c->row()->lastname;
    } else {
        return '';
    }
}
function vat($vat, $price)
{
    if ($vat == 1 || $vat == 0) {
        return "0.00";
    } else {
        $vatDivisor     = 1 + ($vat / 100);
        $priceBeforeVat = $price / $vatDivisor;
        $vatAmount      = $price - $priceBeforeVat;
        return round($vatAmount, 2);
    }
}
function vat4($vat, $price)
{
    if ($vat == 1 || $vat == 0) {
        return "0.00";
    } else {
        $vatDivisor     = 1 + ($vat / 100);
        $priceBeforeVat = $price / $vatDivisor;
        $vatAmount      = $price - $priceBeforeVat;
        return round($vatAmount, 4);
    }
}
function includevat($vat, $in)
{
    if ($vat <= 0) {
        return number_format($in, 2);
    }
    $priceExcludingVat = preg_replace("/[^0-9,.]/", "", $in);
    if ($vat == 1 || $vat == 0) {
        return '0.00';
    } else {
        if (!is_array($priceExcludingVat)) {
            //echo $priceExcludingVat." ".$in." ".$vat;
            //exit;
            $vatToPay   = $priceExcludingVat / 100 * $vat;
            $totalPrice = $priceExcludingVat + $vatToPay;
        } else {
            $totalPrice = "0.00";
        }

        return number_format($totalPrice, 2);
    }
}
function includevat4($vat, $in)
{
    $priceExcludingVat = preg_replace("/[^0-9,.]/", "", $in);
    if ($vat == 1 || $vat == 0) {
        return '0.00';
    } else {
        $vatToPay   = ($priceExcludingVat / 100) * $vat;
        $totalPrice = $priceExcludingVat + $vatToPay;
        return number_format($totalPrice, 4);
    }
}
function getMobileActive($companyid, $userid = false)
{
    if (!$userid) {
        $userid = $ci->session->client['id'];
    }
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.id,a.status,a.userid,a.recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
       case when a.type  = 'mobile' THEN d.msisdn_sn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as sn
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status in ('Active','Suspended')
    and a.companyid = ?
    and a.type='mobile'
    and a.userid= ?
    group by a.id", array(
    $companyid,
    $userid

    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $array[] = array(
            'id' => $row->id,
            'number' => $row->domain,
            'domainstatus' => $row->status,
            'packagename' => $row->packagename,
            'sn' => $row->sn
            );
        }
    }
    return $array;
}
function getMobileActiveCli($userid)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.id,a.status,a.userid,a.recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
        case when a.type  = 'mobile' THEN d.msisdn_sn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as sn
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status in ('Active','Suspended')
    and a.companyid = ?
    and a.userid= ?
    group by a.id", array(
    $ci->session->cid,
    $userid
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $array[] = array(
            'id' => $row->id,
            'number' => $row->domain,
            'domainstatus' => $row->status,
            'packagename' => $row->packagename,
            'sn' => $row->sn
            );
        }
    }
    return $array;
}
function getActiveServices($userid)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.id,a.status,a.userid,a.recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status in ('Active','Suspended')
    and a.userid= ?
    group by a.id", array(
    $userid
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $array[] = array(
            'id' => $row->id,
            'number' => $row->domain,
            'domainstatus' => $row->status,
            'packagename' => $row->packagename
            );
        }
    }
    return $array;
}
function getSwapCost($packageid)
{
    $ci =& get_instance();
    $q = $ci->db->query("select * from a_products where id=?", array(
    $packageid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->mobile_swapcost;
    } else {
        return 0;
    }
}
function getChangeCost($packageid)
{
    $ci =& get_instance();
    $q = $ci->db->query("select * from a_products where id=?", array(
    $packageid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->mobile_changecost;
    } else {
        return 0;
    }
}
function getMobileActive_Admin($companyid, $clientid)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.id,a.status,a.userid,a.recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status in ('Active','Suspended')
    and a.companyid = ?
    and a.userid= ?
    group by a.id", array(
    $companyid,
    $clientid
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $array[] = array(
            'id' => $row->id,
            'number' => $row->domain,
            'domainstatus' => $row->status,
            'packagename' => $row->packagename
            );
        }
    }
    return $array;
}
function getMobileActive_Mobile($id)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.serviceid as id,a.msisdn as domain,f.status as domainstatus,g.name as packagename from a_services_mobile a left join a_services f on f.id=a.serviceid left join a_products g on g.id=f.packageid where a.userid = ? and f.status=? group by a.id", array(
    $id,
    'Active'
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            $array[] = array(
            'id' => $row->id,
            'number' => $row->domain,
            'domainstatus' => $row->domainstatus,
            'packagename' => $row->packagename
            );
        }
    }
    return $array;
}
function isActiveNumber($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select* from tblhosting where id  = ?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return array(
        'domain' => trim($q->row()->domain),
        'status' => strtoupper($q->row()->domainstatus),
        'username' => trim($q->row()->username)
        );
    } else {
        return false;
    }
}
function getSimcardInfo($number)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.*,b.status as domainstatus from a_services_mobile  a left join a_services b on b.id=a.serviceid where msisdn_sim like ? and b.status in ('Active','Suspended') ", array(
    '%' . $number . '%'
    ));
    if ($q->num_rows() > 0) {
        return $q->row_array();
    } else {
        return false;
    }
}
function convertToReadableSize($size)
{
    $base   = log($size) / log(1024);
    $suffix = array(
    " bytes",
    "kB",
    "MB",
    "GB",
    "TB"
    );
    $f_base = floor($base);
    $s      = round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
    if ($s == "NAN bytes") {
        return "0 bytes";
    } else {
        return $s;
    }
}
function convertToReadableSizeMB($size)
{
    $base   = log($size) / log(1000);
    $suffix = array(
    " bytes",
    "kB",
    "MB",
    "GB",
    "TB"
    );
    $f_base = floor($base);
    $s      = round(pow(1000, $base - floor($base)), 1) . $suffix[$f_base];
    if ($s == "NAN bytes") {
        return "0 bytes";
    } else {
        return $s;
    }
}

function convertToReadableSizeMBARTA($size)
{
    $base   = log($size) / log(1024);
    $suffix = array(
    " bytes",
    " kB",
    " MB",
    " GB",
    "TB"
    );
    $f_base = floor($base);
    $s      = round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
    if ($s == "NAN bytes") {
        return "0 bytes";
    } else {
        return $s;
    }
}
function formatSizeUnits($bytes)
{
    if ($bytes >= 1048576) {
        $bytes = ceil($bytes / 1048576) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = ceil($bytes / 1024) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}
function convertToReadableSizeKB($size)
{
    $base   = log($size) / log(1000);
    $suffix = array(
    " bytes",
    "kB",
    "MB",
    "GB",
    "TB"
    );
    $f_base = floor($base);
    $s      = round(pow(1000, $base - floor($base)), 1);
    if ($s == "NAN bytes") {
        return "0 bytes";
    } else {
        return $s;
    }
}


function format_name($client)
{
    if (empty($client->initial)) {
        $name = array(
        $client->firstname,
        $client->lastname
        );
    } else {
        $name = array(
        $client->initial,
        $client->lastname
        );
    }
    $name = array_filter($name);
    return trim(implode(' ', $name));
}
function flatme($array)
{
    $res = "";
    foreach ($array as $obj => $value) {
        $res .= $value . " ";
    }
    return $res;
}
function formatBytes($bytes, $precision = 2)
{
    $units = array(
    'B',
    'KB',
    'MB',
    'GB',
    'TB'
    );
    $bytes = max($bytes, 0);
    $pow   = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow   = min($pow, count($units) - 1);
    return round($bytes, $precision) . ' ' . $units[$pow];
}
function getPortinStatus($id)
{
    switch ($id) {
        case '0':
            $status = 'PortinPending';
            break;
        case '2':
            $status = 'PortinPending';
            break;
        case '4':
            $status = 'PortinPending';
            break;
        case '5':
            $status = 'PortinPending';
            break;
        case '24':
            $status = 'PortinPending';
            break;
        case '6':
            $status = 'PortinCancelled';
            break;
        case '7':
            $status = 'PortinRejected';
            break;
        case '25':
            $status = 'PortinAccepted';
            break;
        case '26':
            $status = 'Active';
            break;
        case '8':
            $status = 'PortinRejected';
            break;
        case '3':
            $status = 'PortinCancelled';
            break;
        case '27':
            $status = 'PortinFailed';
            break;
        case '21':
            $status = 'Active';
            break;
        case '30':
            $status = 'PortinPending';
            break;
        case '31':
            $status = 'PortinPending';
            break;
        case '32':
            $status = 'PortinCancelled';
            break;
        default:
            $status == 'PortinPending';
    }
    return $status;
}
function genMvnoId($companyid)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("SELECT * FROM a_clients where companyid = ? order by mvno_id desc LIMIT 1", array(
    $companyid
    ));
    if (is_numeric($q->row()->mvno_id)) {
        return $q->row()->mvno_id + 1;
    } else {
        return $companyid*1000000;
    }
}
function format_name_address($client)
{
    if (empty($client->initial)) {
        $name = array(
        $client->salutation,
        $client->firstname,
        $client->lastname
        );
    } else {
        $name = array(
        $client->salutation,
        $client->initial,
        $client->lastname
        );
    }
    $name = array_filter($name);
    return str_replace("'", "`", trim(implode(' ', $name)));
}

function format_name_address_no_salutation($client)
{
    $name = array(
        $client->initial,
        $client->firstname,
        $client->lastname
        );

    $name = array_filter($name);
    return str_replace("'", "`", trim(implode(' ', $name)));
}
function getProduct($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("SELECT * FROM a_products where id=?", array(
    $id
    ));
    return $q->row();
}
function getProductName($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("SELECT * FROM a_products where id=?", array(
    $id
    ));
    return $q->row()->name;
}
function getProductPrice($id, $contract)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("SELECT * FROM a_products where id=?", array(
    $id
    ));
    return $q->row()->name;
}
function getProductSell($companyid, $resellerid = false)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    if (!$resellerid) {
        $q = $ci->db->query("SELECT * FROM a_products where companyid=? and status = ? order by name asc", array(
        $companyid,
        1
        ));
    } else {
        $q = $ci->db->query("SELECT a.name,a.id,a.setup,a.recurring_total FROM a_products a left join a_reseller_products b on b.productid=a.id where a.companyid=? and a.status = ? and b.agentid=? order by a.name asc", array(
        $companyid,
        1,
        $resellerid
        ));
    }
    return $q->result();
}
function getPromotions($id = false)
{
    $array = array();
    $ci =& get_instance();
    if (!$id) {
        $id = $ci->session->cid;
    }
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_promotion where date_expired >= ? and companyid=?", array(
    date('Y-m-d'),
    $id
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result_array() as $row) {
            if ($row['date_valid'] <= date('Y-m-d')) {
                $array[] =$row;
            }
        }
    }
    return $array;
}
function getSubscriptionId($msisdn)
{
    $ci =& get_instance();
    $q      = $ci->db->query("select * from a_reseller_simcard where  MSISDN = ?", array($msisdn
    ));

    if ($q->num_rows()>0) {
        return $q->row()->SubscriptionId;
    } else {
        return false;
    }
}
function isTemplateActive($companyid, $name)
{
    $ci =& get_instance();

    $q      = $ci->db->query("select * from a_email_templates where name like ? and companyid=? and status = ?", array(
    $name,
    $companyid,
    1
    ));

    if ($q->num_rows()>0) {
        return true;
    } else {
        return false;
    }
}
function iAllowedInvoice($iCompanyNbr, $iAddresNbr)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('magebo', true);
    $q      = $ci->db->query("select * from  tblAddress b where b.iCompanyNbr = ? and b.iAddressNbr = ?", array(
    $iCompanyNbr,
    $iAddresNbr
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function isAllowed_Clientid($companyid, $clientid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where companyid = ? and id = ?", array(
    $companyid,
    $clientid
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function showInvoice($cid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_configuration where companyid = ? and name='disable_invoice' and val= ?", array(
    $cid,
    1
    ));
    if ($q->num_rows() > 0) {
        return false;
    } else {
        return true;
    }
}
function insertAPILog($data)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $data['companyid'] = $ci->session->cid;
    $ci->db->insert('a_apilog', $data);
}
function getAgenIdbyContactid($companyid, $id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients_agents where ContactType2Id = ? and companyid=?", array(
    $id,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->id;
    } else {
        return 5001;
    }
}
function getclient($id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where id = ?", array(
    $id
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getClientByContactId($companyid, $id)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where ContactType2Id = ? and companyid=?", array(
    $id,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function isAllowed_Service($companyid, $serviceid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services where companyid = ? and id = ?", array(
    $companyid,
    $serviceid
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function get_reseller_counter($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients_agents where companyid = ?", array(
    $companyid
    ));
    return $q->num_rows();
}
function get_freesimcard($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_reseller_simcard where companyid = ? and serviceid is NULL", array(
    $companyid
    ));
    return $q->num_rows();
}

function get_freesimcard_teum($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_reseller_simcard where companyid = ? and serviceid is NULL", array(
    $companyid
    ));
    return $q->num_rows();
}

function get_freesimcard_arta($companyid)
{
    $ci =& get_instance();

    $ci->local = $ci->load->database('default', true);

    $company = globofix($companyid);
    if ($company->mage_invoicing) {
        $s = $ci->local->query("select * from a_mvno where companyid=?", array($companyid));
        //log_message('error', $ci->local->last_query());
        if ($s->num_rows()>0) {
            $ci->db = $ci->load->database('magebo', true);
            $RangeNumber = $s->row()->RangeNumber;
            $q      = $ci->db->query("SELECT * from tblC_SimCard where iPincode is NULL and iCompanyRangeNbr = ?  order by cSIMCardNbr DESC", array(
                $RangeNumber
            ));
            //log_message('error', $ci->db->last_query());
            return $q->num_rows();
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}
function reseller_allow_perm($type, $agentid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->where("agentid", $agentid);
    $ci->db->where($type, '1');
    $q      = $ci->db->get("a_clients_agents_perms");
    //log_message('error', $ci->db->last_query());
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function getAddonsbySericeid($serviceid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services_addons where serviceid = ?", array(
    $serviceid
    ));
    if ($q->num_rows() > 0) {
        return $q->result();
    } else {
    }
}

function getAddonsbySericeidNo_Base($serviceid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.* from a_services_addons a left join a_products_mobile_bundles b on b.id=a.addonid where a.serviceid = ? and base_bundle != '1'", array(
    $serviceid
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getAddonsbyBundleID($bundleid, $serviceid=false)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    if ($serviceid) {
        $q      = $ci->db->query("select * from a_services_addons where arta_bundleid = ? and serviceid=?", array(
            $bundleid, $serviceid
            ));
    } else {
        $q      = $ci->db->query("select * from a_services_addons where arta_bundleid = ?", array(
            $bundleid
            ));
    }

    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
    }
}
function checkaddon_existance($addonid, $serviceid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services_addons where addonid = ? and serviceid=?", array(
    $addonid,
    $serviceid
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
        return false;
    }
}
function getAddonsbyID($bundleid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_services_addons where id = ?", array(
    $bundleid
    ));
    if ($q->num_rows() > 0) {
        return $q->row();
    } else {
    }
}
function isAllowed_reseller($resellerid, $clientid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients where agentid = ? and id = ?", array(
    $resellerid,
    $clientid
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function isAllowed_reseller_companyid($resellerid, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_clients_agents where id = ? and companyid = ?", array(
    $resellerid,
    $companyid
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function isAllowed_products_companyid($pid, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products where id = ? and companyid = ?", array(
    $pid,
    $companyid
    ));
    //log_message('error', $ci->db->last_query());
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}

function is_allowedAddons($aid, $companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from a_products_mobile_bundles where id = ? and companyid = ?", array(
    $aid,
    $companyid
    ));
    //log_message('error', $ci->db->last_query());
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}


function isAllowed_reseller_service($resellerid, $serviceid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.* from a_services a left join a_clients b on b.id=a.userid where b.agentid = ? and a.id = ?", array(
    $resellerid,
    $serviceid
    ));
    if ($q->num_rows() > 0) {
        return true;
    } else {
        return false;
    }
}
function getProductsellarray($companyid)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("SELECT id FROM a_products where companyid=?", array(
    $companyid
    ));
    foreach ($q->result() as $r) {
        $array[] = $r->id;
    }
    return $array;
}
function getCurrentProductAddonid($serviceid)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.id,a.name,b.monthly,b.msetupfee from tbladdons a left join tblpricing b on b.relid=a.id where b.type=?", array(
    'addon'
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            if (substr(trim(strtolower($row->name)), 0, 6) != "option" && substr(trim(strtolower($row->name)), 0, 6) != "tariff") {
                $array[] = $row->id;
            }
        }
    }
    $ci->db->where_in('addonid', $array);
    $ci->db->where('hostingid', $serviceid);
    $p = $ci->db->get('tblhostingaddons');
    return $p->row()->id;
}
function getCurrentProductidAddon($serviceid)
{
    $array = array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select a.id,a.name,b.monthly,b.msetupfee from tbladdons a left join tblpricing b on b.relid=a.id where b.type=?", array(
    'addon'
    ));
    if ($q->num_rows() > 0) {
        foreach ($q->result() as $row) {
            if (substr(trim(strtolower($row->name)), 0, 6) != "option" && substr(trim(strtolower($row->name)), 0, 6) != "tariff") {
                $array[] = $row->id;
            }
        }
    }
    $ci->db->where_in('addonid', $array);
    $ci->db->where('hostingid', $serviceid);
    $p = $ci->db->get('tblhostingaddons');
    return $p->row()->addonid;
}
function second2hms($seconds)
{
    $t = round($seconds);
    return sprintf('%02d:%02d:%02d', ($t / 3600), ($t / 60 % 60), $t % 60);
}

function s2m($seconds)
{
    $t = round($seconds);
    return $t/60;
}
function RatingUnitName($id)
{
    if ($id == 1) {
        return "Seconds";
    } elseif ($id == 2) {
        return "Sessions";
    } elseif ($id == 3) {
        return "Bytes";
    } else {
        return 'Unit';
    }
}
function secToHR($seconds)
{
    return round($seconds / 60, 2);
}
function getAddonid($type, $id)
{
    $ci =& get_instance();
    if ($type == "mobile") {
        $t  = 'a_products_mobile_bundles';
        $ty = "option";
    } elseif ($type == "xdsl") {
        $t  = 'a_products_xdsl_bundles';
        $ty = "option";
    } elseif ($type == "voip") {
        $t  = 'a_products_voip_bundles';
        $ty = "option";
    } else {
        $t  = 'a_products_mobile_bundles';
        $ty = "others";
    }
    $ci->db->where('id', $id);
    $q = $ci->db->get($t);
    return (object) array(
    'addon_type' => $q->row()->bundle_type,
    'recurring_total' => $q->row()->recurring_total,
    'name' => $q->row()->name,
    'bundleid' => $q->row()->bundleid,
    'cycle' =>  $q->row()->bundle_duration_type,
    'terms' =>  $q->row()->bundle_duration_value
    );
}
function getAddonSell($type, $companyid, $gid = false, $option_type = false)
{
    $array = (object) array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    if ($type == "mobile") {
        $t  = 'a_products_mobile_bundles';
        $ty = "option";
    } elseif ($type == "xdsl") {
        $t  = 'a_products_xdsl_bundles';
        $ty = "option";
    } elseif ($type == "voip") {
        $t  = 'a_products_voip_bundles';
        $ty = "option";
    } else {
        $t  = 'a_products_mobile_bundles';
        $ty = "others";
    }
    if ($gid) {
        $ci->db->where('agid', $gid);
    }
    if ($option_type) {
        $ci->db->where('bundle_type', $option_type);
    } else {
        $ci->db->where('bundle_type', $ty);
    }
    $ci->db->where('status', 1);
    $ci->db->where('companyid', $companyid);
    $q = $ci->db->get($t);
    //log_message('error', $ci->db->last_query());
    if ($q->num_rows() > 0) {
        $array = $q->result();
    }
    return $array;
}
function getAddonSellClient($type, $companyid, $gid = false)
{
    $array = (object) array();
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    if ($type == "mobile") {
        $t  = 'a_products_mobile_bundles';
        $ty = "option";
    } elseif ($type == "xdsl") {
        $t  = 'a_products_xdsl_bundles';
        $ty = "option";
    } elseif ($type == "voip") {
        $t  = 'a_products_voip_bundles';
        $ty = "option";
    } else {
        $t  = 'a_products_mobile_bundles';
        $ty = "others";
    }
    if ($gid) {
        $ci->db->where('agid', $gid);
    }
    $ci->db->where('show_customer', 1);
    $ci->db->where('status', 1);
    $ci->db->where('companyid', $companyid);
    $q = $ci->db->get($t);

    if ($q->num_rows() > 0) {
        $array = $q->result();
    }

    return $array;
}
function hasWelcomeLetter($id)
{
    $file = '%_' . $id . '_%';
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select * from tblclientsfiles where filename like ? order by id desc", array(
    $file
    ));
    if ($q->num_rows() > 0) {
        return $q->row()->filename;
    } else {
        return false;
    }
}
function getPostcodesList($companyid)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $q      = $ci->db->query("select distinct(postcode) from a_clients where companyid = ? order by postcode asc", array(
    $companyid
    ));

    return $q->result();
}
function addLogActivation($companyid, $serviceid, $status)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    if ($status == "NewActivation") {
        $ci->db->insert('a_event_status', array(
        'companyid' => $companyid,
        'sn' => getMySN($serviceid),
        'serviceid' => $serviceid,
        'status' => $status,
        'processed' => 1,
        'ack' => 1
        ));
    } else {
        $ci->db->insert('a_event_status', array(
        'companyid' => $companyid,
        'sn' => getMySN($serviceid),
        'serviceid' => $serviceid,
        'status' => $status
        ));
    }
}
function ilogClient($data)
{
    $ci =& get_instance();
    $ci->db = $ci->load->database('default', true);
    $ci->db->insert('a_logs', $data);
}
function sendEmailNotification($service, $subject, $message, $client)
{
    if (!empty($client->email_notification)) {
        $headers = "From: noreply@united-telecom.be" . "\r\n" . "BCC: mail@simson.one";
        $msg     = "Hello Team,\n";
        $msg .= "\n";
        $msg .= "Client ID         : " . $client->mvno_id . "\n";
        $msg .= "Artilium ID       : " . $client->mageboid . "\n";
        $msg .= "MSISDN Number     : " . $service->msisdn . "\n";
        $msg .= "Serial Number     : " . $service->msisdn_sn . "\n";
        $msg .= "Simcard Number    : " . $service->msisdn_sim . "\n";
        $msg .= "Please Open The client and check this " . $client->portal_url . "admin/client/detail/" . $client->id . "\n\n";
        $msg .= "Regards\n";
        $msg .= "\n";
        $msg .= "If you wish to recieved  this event to be sent to your webservices, please contact our support\n";
        mail($client->email_notification, $subject, $msg, $headers);
    }
    function csendEmailNotification($service, $subject, $message, $client)
    {
        if (!empty($client->email_notification)) {
            $this->data['setting'] = globofix($service->companyid);
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                 'protocol' => 'smtp',
                 'smtp_host' => $this->data['setting']->smtp_host,
                 'smtp_port' => $this->data['setting']->smtp_port,
                 'smtp_user' => $this->data['setting']->smtp_user,
                 'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                 'mailtype' => 'html',
                 'charset' => 'utf-8',
                 'starttls' => true,
                 'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $msg = "Hello Team,\n";
            $msg .= "\n";
            $msg .= "Client ID         : " . $client->mvno_id . "\n";
            $msg .= "Artilium ID       : " . $client->mageboid . "\n";
            $msg .= "MSISDN Number     : " . $service->msisdn . "\n";
            $msg .= "Serial Number     : " . $service->msisdn_sn . "\n";
            $msg .= "Simcard Number    : " . $service->msisdn_sim . "\n";
            $msg .= "Please Open The client and check this " . $client->portal_url . "admin/client/detail/" . $client->id . "\n\n";
            $msg .= "Regards\n";
            $msg .= "\n";
            $msg .= "If you wish to recieved  this event to be sent to your webservices, please contact our support\n";
            $body = $msg;
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email_notification);
            $this->email->bcc('mail@simson.one');
            $this->email->subject($subject);
            $this->email->message($body);
            $this->email->send();
        }
    }

    function createWHMCSticket($c)
    {
        $ci =& get_instance();
        $ci->db = $ci->load->database('default', true);
        $company = $ci->db->query('select * from a_mvno where companyid=?', array($c['companyid']));
        $email =explode('|', $company->row()->email_notification);
        $message = "";
        $message .="Move CLI error on the platform\n";
        $message .="SN: ".$c['mobile']->msisdn_sn." \n";
        $message .="PINCODE: ".$c['mobile']->msisdn." \n";
        $message .="Error Array: ".print_r($c['result'], true)." \n";
        $url = "https://my.united-telecom.be/includes/api.php";
        $postfieldsT["username"] = "united";
        $postfieldsT["password"]=md5("un!t3d");
        $postfieldsT["action"] = "openticket";
        $postfieldsT["name"] = $company->row()->companyname;
        $postfieldsT["email"] =  $email[0];
        $postfieldsT["markdown"]="true";
        $postfieldsT["deptid"] = "13"; //Technical Support
        $postfieldsT["subject"] = "MVNO Portal Incident ".rand(100000, 9999999);
        $postfieldsT["message"] = $message;
        $postfieldsT["priority"] = "Low";
        //$postfieldsT["serviceid"] = $serviceid;
        //print_r($postfieldsT);exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 100);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfieldsT);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $dataTT= curl_exec($ch);

        curl_close($ch);
        return json_decode($dataTT);
    }
}
