<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $setting->companyname; ?> <?php echo lang('Client Area'); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/lumen.css?version=1.4">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/custom.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Fontastic Custom icon font-->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/img/favicon.ico">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/jquery-ui/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/jquery-ui/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/css/delta.css">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script
    src="https://code.jquery.com/jquery-2.2.4.js"
    integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
    crossorigin="anonymous"></script>
    <script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
    integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
    crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/js/autocomplete.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script>
    $( function() {
    $( "#pickdate" ).datepicker( { dateFormat: 'yy-mm-dd'} );
    $( "#pickdate2" ).datepicker( { dateFormat: 'yy-mm-dd' } );
    $( "#pickdate3" ).datepicker( { dateFormat: 'yy-mm-dd' } );
    $( "#pickdate4" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
    $( "#pickdate5" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
    $( "#pickdate6" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
    } );
    </script>
  </head>
  <body>
    <header class="fixed-top">
      <div class="container">
        <div class="left-menu grid grid-65">
          <div class="grid grid-25 logo">
            <img src="<?php echo base_url(); ?>assets/clear/img/logo_delta.png">
          </div>
        </div>
      </div>
    </header>
    <hr />
    <hr />
    <div class="lewat"></div>
    <div class="container">
      <div class="breadcrumb-holder top-buffer">
        <ul class="breadcrumb  bg-primary">
          <?php foreach (extend_uri() as $uri) {?>
          <li class="breadcrumb-item"><?php echo lang(ucfirst(str_replace('_', ' ', $uri))); ?></li>
          <?php }?>
        </ul>
      </div>
      <?php if (!empty($this->session->flashdata('success'))) {?>
      <div class="row">
        <div class="col-sm-12">
          <div class="alert alert-success" role="alert">
            <strong class="text-center"><?php echo $this->session->flashdata('success'); ?></strong>
          </div>
        </div>
      </div>
      <?php }?>
      <?php if (!empty($this->session->flashdata('error'))) {?>
      <div class="row">
        <div class="col-sm-12">
          <div class="alert alert-danger" role="alert">
            <strong class="text-center"><center><?php echo $this->session->flashdata('error'); ?></center></strong>
          </div>
        </div>
      </div>
      <?php }?>
      <div class="row">
      	<div class='col-md-12'>
