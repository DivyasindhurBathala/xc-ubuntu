

		 <div class="card-header bg-primary text-light"><?php echo lang('Important Numbers/Code'); ?>:</div>
                <div class="table-responsive card">
                <div class="card-body mb-0">
                	<div class="row">
                		<div class="col-md-6 mb-0"><h5><?php echo lang('Klantenservice'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>+31 20 20 50 597</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang(' Voicemail'); ?></h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>+31 637 00 12 33</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Al uw gesprekken doorschakelen Activeren'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**21*telefoonnummer# [zend]</h5>
                		</div>
					</div>
					<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Al uw gesprekken doorschakelen Annuleren'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>##21# [zend]</h5>
                		</div>
                	</div>


                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Doorschakelen naar voicemail:'); ?> +</h5>
                		</div>
                		<div class="col-md-6 text-right">
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Activeren bij geen gehoor'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**61*telefoonnummer# [zend]</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Annuleren bij geen gehoor'); ?></h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>##61#) [zend]</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Activeren bij geen bereik'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**62*telefoonnummer# [zend]</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Annuleren bij geen bereik'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>##62# [zend]</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Activeren bij in gesprek'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>**67*telefoonnummer# [zend]</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Annuleren bij in gesprek'); ?></h5>
                		</div>
                		<div class="col-md-6 text-right"><h5> ##67# [zend]</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Alle actieve doorschakelingen annuleren'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5> ##002# [zend]</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('Toegangspunt'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"></h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('APN'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>internet</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('MMC'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>204</h5>
                		</div>
                	</div>

                	<div class="row">
                		<div class="col-md-6"><h5><?php echo lang('MNC'); ?> :</h5>
                		</div>
                		<div class="col-md-6 text-right"><h5>16</h5>
                		</div>
                	</div>





                	</div>
                </div>



