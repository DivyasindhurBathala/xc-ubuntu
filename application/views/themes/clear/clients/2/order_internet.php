<!DOCTYPE html>
<html>
<head>
  <title>Order xDSL United Telecom</title>
   <!--Made with love by Mutiullah Samim -->
<!-- 609191176703-hracmhuitcbj4sdsepbohhhg546pdapb.apps.googleusercontent.com -->
<!-- zLGkGARxVsZEVSGC5XVVce9h -->
  <!--Bootsrap 4 CDN-->
  <link href="https://client.united-telecom.be/assets/clear/css/main.css?version=1560178029" rel="stylesheet">
  <link href="https://client.united-telecom.be/assets/clear/css/custom.css?version=1.7" rel="stylesheet">
  <link rel="stylesheet" href="https://client.united-telecom.be/assets/clear/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="https://client.united-telecom.be/assets/clear/jquery-ui/jquery-ui.theme.min.css">
  <link href="https://client.united-telecom.be/assets/clear/icon_fonts_assets/feather/style.css"
    rel="stylesheet">
  <link href="https://client.united-telecom.be/assets/clear/css/component-custom-switch.css"
    rel="stylesheet">
  <link rel="stylesheet" href="https://client.united-telecom.be/assets/clear/css/tempusdominus-bootstrap-4.min.css" />
   <script src="https://client.united-telecom.be/assets/clear/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="https://client.united-telecom.be/assets/clear/bower_components/moment/moment.js"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
   
    <link href="https://client.united-telecom.be/assets/clear/css/chat.css?version=1.1" rel="stylesheet">
  <link href="https://client.united-telecom.be/assets/clear/css/screen.css" rel="stylesheet">
  <link href="https://client.united-telecom.be/assets/clear/css/emojionearea.min.css" rel="stylesheet">
  
    <link rel="stylesheet" href="https://client.united-telecom.be/assets/clear/css/frame.css?version=1.1" />
   <link href="https://client.united-telecom.be/assets/circle.css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
         <link href="https://client.united-telecom.be/assets/clear/css/jquery.dataTables.yadcf.css" rel="stylesheet">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
           <script>
    $( document ).ready(function() {
   var socket = io.connect('https://socket.united-telecom.be');
   socket.on('connect', function() {
    var pxm = '<?php echo $channel; ?>';
    socket.emit('subscribe', {"batch": true, "channel":pxm,"token":'<?php echo $this->encryption->encrypt('lainard'); ?>'});
       //console.log('joined :'+pxm);
  });
      socket.on('pxm', function (data) {
      var location = JSON.parse(data.message);
      if(location.type == "address"){
         $.ajax({
        url: 'https://client.united-telecom.be/admin/complete/checkaddress',
        dataType: 'json',
        type: 'post',
        data: {
        'action': 'preordering',
        'method': 'GetDetailedProductAvailability',
        'orderid': location.proximus_orderid,
        },
        success: function(data1) {
          console.log(data1);

        }
      });

      }
     
    if(location.type == "product"){
         $.ajax({
        url: 'https://client.united-telecom.be/admin/complete/checkaddress',
        dataType: 'json',
        type: 'post',
        data: {
        'action': 'preordering',
        'method': 'GetDetailedNetworkAvailability',
        'orderid': location.proximus_orderid,
      },
        success: function(data1) {
          console.log(data1);

        }
      });

      }
       if(location.type == "network"){
  $.ajax({
        url: 'https://client.united-telecom.be/admin/complete/getXDSLResult',
        dataType: 'json',
        type: 'post',
        data: {
        'orderid': location.proximus_orderid,
      },
        success: function(data1) {
          console.log(data1);
            if(data1.result == "success"){
            if(data1.vdsl == "OK"){
            var btn_href = '#stepContent2';
            $("#stepContent1_loader").hide('slow');
            $('#addresscheck').prop('disabled', false);
            $('#showsuccess').html('<div class="col-md-12"><div class="alert alert-dismissible alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Congratulation</strong> VDSL is available with Maximum Download '+data1.min_download+' Mbps and Upload '+data1.min_upload+' Mbps</div></div>');
            $('#d').attr("data-value", data1.min_download);
            $('#u').attr("data-value", data1.min_upload);
            $('.step-trigger[href="' + btn_href + '"]').click();
            $('#proximus_orderid').val(data1.orderid);
            return false;
            }else{
                  $("#stepContent1_loader").hide('slow');
               $("#stepContent1").show('slow');
    
                $('#addresscheck').prop('disabled', false);
                $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Oh snap!</strong> VDSL(2) is not available on this address</div></div>');
            }

            }else{
              $("#stepContent1_loader").hide('slow');
               $("#stepContent1").show('slow');
    
                $('#addresscheck').prop('disabled', false);
               $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error</strong> '+data1.message+'</div></div>');

            }

          }
      });

       }
   });


  socket.on('notification', function (data) {
       var chatuser = 'Simson5702';
      $.ajax({
        url:  window.location.protocol + "//" + window.location.host + "/admin/auth/read_envelope/",
        dataType: 'json',
        type: 'post',
        data:{
          user: chatuser
        },
        success: function (data) {
          console.log(data);
        }
      });

     setTimeout(function() {
        $.bootstrapGrowl(data.message, {
            type: 'info',
            offset: {from: 'bottom', amount: 1}, // 'top', or 'bottom'
            align: 'right',
            width: 'auto',
            delay: 10000,
            stackup_spacing: 10,
            allow_dismiss: true
        });
      }, 1000);
  });
   });
</script>
<style>
  .google-button {
  height: 40px;
  border-width: 0;
  background: white;
  color: #737373;
  border-radius: 5px;
  white-space: nowrap;
  box-shadow: 1px 1px 0px 1px rgba(0,0,0,0.05);
  transition-property: background-color, box-shadow;
  transition-duration: 150ms;
  transition-timing-function: ease-in-out;
  padding: 0;
  
  &:focus,
  &:hover {
    box-shadow: 1px 4px 5px 1px rgba(0,0,0,0.1);
  }
  
  &:active {
    background-color: #e5e5e5;
    box-shadow: none;
    transition-duration: 10ms;
  }
}
    
.google-button__icon {
  display: inline-block;
  vertical-align: middle;
  margin: 8px 0 8px 8px;
  width: 18px;
  height: 18px;
  box-sizing: border-box;
}

.google-button__icon--plus {
  width: 27px;
}

.google-button__text {
  display: inline-block;
  vertical-align: middle;
  padding: 0 24px;
  font-size: 14px;
  font-weight: bold;
  font-family: 'Roboto',arial,sans-serif;
}
</style>
</head>
<body>
<div class="container">
  <div class="content-i">
  <div class="content-box"><div class="element-wrapper">
    <div class="element-box">
      <form id="formdata">
        <div class="steps-w">
          <div class="step-triggers">
            <a class="step-trigger active" href="#stepContent1">Address Check</a>
            <a class="step-trigger" href="#stepContent2">Configure Product</a>
            <a class="step-trigger" href="#stepContent3">Billing Information</a>
            <a class="step-trigger" href="#stepContent4">Final Step</a>
          </div>
          <div class="step-contents">
            <div id="stepContent1_loader" style="display:none;">
              <center>
              <img src="<?php echo base_url(); ?>assets/img/f553e05cd57fc28d91e3a4b26870f24f.gif">
              </center>
            </div>
            <div class="step-content active" id="stepContent1">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="">Postcode</label><input class="form-control  ui-autocomplete-input zipcomplete" id="postcodenum" autocomplete="new-username" type="text" name="install_postcode">
                  </div>
                </div>
                <div class="col-sm-8">
                  <div class="form-group">
                    <label for="">City</label><input class="form-control" type="text" id="cityname" placeholder="City Name" name="install_city">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="">StreetName</label><input class="form-control ui-autocomplete-input streetcomplete" name="install_street" autocomplete="new-username" placeholder="Streetname" type="text" id="street">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="">HouseNumber</label><input class="form-control" placeholder="HouseNumber" type="text" id="housenumber" name="install_phousenumber">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="">Alphabet</label><input class="form-control" placeholder="Alphabet" type="text" id="alphabet" name="install_alphabet">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="">Mailbox</label><input class="form-control" placeholder="Mailbox" type="text" id="mailbox" name="install_mailbox">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for="">Block</label><input class="form-control" placeholder="Block" type="text" id="block" name="install_block">
                  </div>
                </div>
              </div>
              <div class="row">
                <div  id="errornya">
                </div>
              </div>
             
          <div class="form-buttons-w text-right">
            <button class="btn btn-primary step-trigger-btn" type="button" id="addresscheck"> Continue</button>
          </div>
        </div>
        <div class="step-content" id="stepContent2">
          <div class="row">
            <div class="col-md-12">
              <div id="showsuccess"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6" align="center">
              <canvas id="d" data-type="radial-gauge"

              data-units="Mbps"
              data-title="Download"
              data-width="200"
              data-height="200"
              data-bar-width="10"
              data-bar-shadow="5"
              data-color-bar-progress="rgba(50,200,50,.75)"
              ></canvas>
            </div>
            <div class="col-md-6" class="text-center">
              <canvas id="u"
              data-type="radial-gauge"

              data-units="Mbps"
              data-title="Upload"
              data-width="200"
              data-height="200"
              data-bar-width="10"
              data-bar-shadow="5"
              data-color-bar-progress="rgba(50,200,50,.75)"
              ></canvas>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for=""> Product Choose</label><select class="form-control" name="packageid" id="packageid">
                <?php foreach (getInternetProductsell('xdsl', $this->session->cid) as $row) {?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                <?php }?>
              </select>
            </div>
            <h2>Modem Option</h2>
              <div class="form-group">
              <label for=""> Modem Option</label>
              <select class="form-control" id="modem" name="modem">
                <option value="buy">Wifi/Voice modem €19.00 (Gedurende 6 maanden)</option>
                <option value="rent">Wifi/Voice modem €0.00 (Contract 2 jaar)</option>
              </select>
            </div>
 <h2>Internet Option</h2>
              <div class="form-group">
              <label for="">Internet Move</label>
              <select class="form-control" id="migration" name="migration">
                <option value="yes">Yes, I Move my Internet from Other provider such as EDPnet, Proximus, Scarlet, Billi and ETC</option>
                <option value="no" selected>New Rawcopper </option>
              </select>
            </div>

            <div id="migrationoption" style="display:none;">
             <div class="form-group">
              <label for="">Easyswitch ID</label>
<input class="form-control" autocomplete="new-username" placeholder="Optional" type="text" name="easyswitchid" id="easyswitchid">
            </div>
   <div class="form-group">
              <label for="">Circuit ID</label>
<input class="form-control" autocomplete="new-username" placeholder="Optional" type="number" name="circuitid" id="circuitid">
            </div>
</div>
 <h2>PSTN Option</h2>
              <div class="form-group">
              <label for="">Porting PSTN Number</label>
              <select class="form-control" id="voipmove">
                <option value="yes">Yes, Move PSTN Number to us</option>
                <option value="no" selected>No, Ordering new Number </option>
              </select>
            </div>
 <div id="voipmoveoption" style="display:none;">
   <div class="form-group">
              <label for="">PSTN Number to be Ported</label>
<input class="form-control" autocomplete="new-username" placeholder="Required..." type="number" name="voipnumber" id="voipnumber" maxlength="9">
            </div>
 </div>
 <h2>Customer Option</h2>
           <div class="form-group">
              <label for=""> Customer</label>
              <select class="form-control" id="customertype" name="customer_type">
                <option value="old">Existing Customer</option>
                <option value="new">New Customer</option>
              </select>
            </div>

            <div class="form-group" id="existing">
              <label for=""> Search Customer</label>
              <input class="form-control ui-autocomplete-input customerid" id="customerid" autocomplete="new-username" placeholder="search..." type="text" name="userid">
            </div>
          </div>
        </div>
        <div class="form-buttons-w text-right">
          <button class="btn btn-primary step-trigger-btn" type="button" id="step3" disabled> Continue</button>
        </div>
      </div>
      <div class="step-content" id="stepContent3">
        <div class="form-group">
          <label for=""> Email address</label><input class="form-control" placeholder="Enter email" name="email" type="email" id="email" required>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for=""> VAT.</label><input class="form-control" placeholder="leave empty if not a company entity" type="text" name="vat" id="vat">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="">Companyname</label><input class="form-control" placeholder="Leave empty if not a company entity" type="text" name="companyname" id="companyname">
            </div>
          </div>
        </div>
        <div class="row">
           <div class="col-sm-4">
            <div class="form-group">
              <label for=""> Gender</label>
          <select class="form-control" id="gender" name="gender">
                    <?php foreach (array('male','female') as $gender) {?>
                    <option value="<?php echo $gender; ?>"><?php echo lang(ucfirst($gender)); ?></option>
                    <?php }?>
                  </select>

            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for=""> Firstname</label><input class="form-control" placeholder="Firstname" type="text" name="firstname" id="firstname" required>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">Lastname</label><input class="form-control" placeholder="Lastname" type="text" name="lastname" id="lastname" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for=""> Address1</label><input class="form-control" placeholder="Address" type="text" name="address1" id="xaddress1" required>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="">Postcode</label><input class="form-control" placeholder="Postcode" type="text" name="postcode" id="xpostcode" required>
            </div>
          </div>

            <div class="col-sm-3">
            <div class="form-group">
              <label for="">City</label><input class="form-control" placeholder="City" type="text" name="city" id="xcity" required>
            </div>
          </div>
        </div>
    <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="">Phonenumber</label><input class="form-control" placeholder="Phonenumber" type="text" name="phonenumber" id="phonenumber" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="">National Number</label><input class="form-control" placeholder="National Number" type="text" name="nationalnr" id="nationalnr">
            </div>
          </div>
        </div>
         <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">Language</label>
                <select class="form-control" id="language" name="language">
                    <?php foreach (getLanguages() as $key => $lang) {?>
                    <option value="<?php echo $key; ?>"<?php if ($key == "dutch") {?> selected<?php }?>><?php echo lang($lang); ?></option>
                    <?php }?>
                  </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">Paymentmethod</label>
               <label for="Paymentmethod"> <?php echo lang('Paymentmethod'); ?></label>
                  <select class="form-control" id="default_paymentmethod" name="paymentmethod">
                    <?php foreach (array('banktransfer','directdebit') as $pmt) {?>
                    <option value="<?php echo $pmt; ?>"><?php echo ucfirst($pmt); ?></option>
                    <?php }?>
                  </select>
            </div>
          </div>

           <div class="col-sm-4">
            <div class="form-group">
              <label for=""> Country</label>
              <select class="form-control" name="country">
                <?php foreach (getCountries() as $key => $c) {?>
                <option value="<?php echo $key; ?>"<?php if ($key == "BE") {?> selected<?php }?>>  <?php echo $c; ?> </option>
                <?php }?>
              </select>
            </div>
          </div>
        </div>

           <div class="row">
          
          <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label for="exampleSelect1">
                      <?php echo lang('VAT Rates'); ?></label>
                    <select class="form-control" id="vat_rate" name="vat_rate">
                      <?php foreach(array('21','6','0') as $rate){ ?>
                      <option value="<?php echo $rate; ?>" <?php if ($setting->taxrate == $rate) {?> selected
                        <?php }?>>
                        <?php echo $rate; ?>
                      </option>
                      <?php } ?>
                    </select>
                  </fieldset>
                </div>
              </div>
            <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label>Payment Terms</label>
                    <input type="text" class="form-control" name="payment_duedays" id="payment_duedays" value="<?php echo $setting->payment_duedays; ?>">
                  </fieldset>
                </div>
              </div>

              <div class="col-sm-4">
              <div class="form-group">
                <fieldset>
                  <label for="exampleSelect1">E-Invoice</label>
                  <select class="form-control" id="invoice_email" name="invoice_email">
                    <option value="yes" selected="">Yes</option>
                    <option value="yes">No</option>
                  </select>
                  <small> choose yes if wish to recieved email invoices</small>
                </fieldset>
              </div>
            </div>
              
        </div>
         
       <div class="row">
        <div class="col-sm-6">
              <div class="form-group">
                <fieldset class="form-group"><div class="form-desc">
                  <?php echo lang('VAT exemption (BTW verlegd)'); ?>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt" value="0" checked="">
                    <?php echo lang('No, Apply tax on every invoices'); ?>
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt" value="1">
                    <?php echo lang('Yes, do not apply tax on invoices'); ?>
                  </label>
                </div>
              </fieldset>
            </div>
          </div>
            </div>
      
        <div class="form-buttons-w text-right">
          <a class="btn btn-primary step-trigger-btn text-white"  id="step4"> Continue</a>
        </div>
      </div>
      <div class="step-content" id="stepContent4">
     <table class="table table-striped">
      <tr>
        <td></td>
        <td align="right"></td>
      </tr>
       <tr>
        <td>Product</td>
        <td align="right" id="final_productname"></td>
      </tr>
       <tr>
        <td>Modem</td>
        <td align="right" id="final_modem"></td>
      </tr>
       <tr>
        <td>Order Type</td>
        <td align="right" id="final_order_type"></td>
      </tr>
       <tr>
        <td>Customer Name</td>
        <td align="right" id="final_customer_name"></td>
      </tr>
       <tr>
        <td>Customer Email</td>
        <td align="right"  id="final_customer_email"></td>
      </tr>
       <tr>
        <td>Date Install</td>
        <td align="right"><input type="text" name="dateRequested" class="form-control col-md-2" id="pickdate7" required></td>
      </tr>
       <tr>
        <td></td>
        <td align="right"  id="final_customer_2"></td>
      </tr>
     </table>
      <div class="form-buttons-w text-right">
        <input type="hidden" name="proximus_orderid" id="proximus_orderid" value="0"></div>
        <button class="btn btn-primary submit_btn" type="button"><i class="fa fa-save"></i> <?php echo lang('Submit'); ?></button>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</div>
</div>
<script>

  function disable_button(){
$('.submit_btn').prop('disabled', true);
$('.submit_btn').html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
var form_data=$("#formdata").serializeArray();
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/subscription/addorder_internet',
type: 'post',
dataType: "json",
data: form_data,
success: function(data) {
if(data.result){
window.location.href = window.location.protocol + '//' + window.location.host + '/admin/client/detail/'+data.id;
}else{
alert("There was an error: "+data.message);
$('.submit_btn').prop('disabled', false);
$('.submit_btn').html('<i class="fa fa-save"></i> <?php echo lang('Register Clients'); ?>');
}
}
});
}


$("#addresscheck").click(function() {
    $('#customerid').val('');
    $("#stepContent1").hide('slow');
    $("#stepContent1_loader").show('slow');
    $('#addresscheck').prop('disabled', true);
    var params = {
        'action': 'preordering',
        'method': 'FindGeographicLocation',
        'street': $("#street").val(),
        'postcode': $("#postcodenum").val(),
        'city': $("#cityname").val(),
        'number': $("#housenumber").val(),
        'alphabet': $("#alphabet").val(),
        'block': $("#block").val(),
        'mailbox': $("#mailbox").val(),
        'floor': $("#floor").val(),
        'channel': '<?php echo $channel; ?>'
    }
    $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/checkaddress',
        dataType: 'json',
        type: 'post',
        data: params,
        success: function(data) {
          console.log(data);
            $('#xaddress1').val(params.street+' '+params.number);
            $('#xpostcode').val(params.postcode);
            $('#xcity').val(params.city);
          if(data.result == "success"){
            if(data.vdsl == "OK"){
            var btn_href = '#stepContent2';
            $("#stepContent1_loader").hide('slow');
            $('#addresscheck').prop('disabled', false);
            $('#showsuccess').html('<div class="col-md-12"><div class="alert alert-dismissible alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Congratulation</strong> VDSL is available with Maximum Download '+data.min_download+' Mbps and Upload '+data.min_upload+' Mbps</div></div>');
            $('#d').attr("data-value", data.min_download);
            $('#u').attr("data-value", data.min_upload);
              $('.step-trigger[href="' + btn_href + '"]').click();
            $('#proximus_orderid').val(data.orderid);
            return false;

            }else{

              $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Oh snap!</strong> VDSL(2) is not available on this address</div></div>');
            }

          }else{
    $("#stepContent1").show('slow');
    $("#stepContent1_loader").hide('slow');
    $('#addresscheck').prop('disabled', false);
    $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Oh snap!</strong> '+data.message+'</div></div>');
          }

        },
        error: function(errorThrown) {
            console.log(errorThrown);
            $("#stepContent1").show();
            $("#stepContent1_loader").hide('slow');
            $('#addresscheck').prop('disabled', false);
        }
    });
});

$('#step4').click(function(){
     var packagename = $('#packageid').find(":selected").text();
     console.log(packagename);
     var modem = $('#modem').find(":selected").text();
      var migration = $('#migration').find(":selected").text();
       var firstname = $('#firstname').val();
       var lastname = $('#lastname').val();
        var customeremail = $('#email').val();
    $("#final_productname").html(packagename);
    $("#final_modem").html(modem);
   
    $("#final_order_type").html(migration);
    $("#final_customer_name").html(firstname+" "+lastname);
    $("#final_customer_email").html(customeremail);
  var btn_href = '#stepContent4';
                $('.step-trigger[href="' + btn_href + '"]').click();


});
$("#step3").click(function() {
    var userid = $('#customerid').val();
    console.log(userid);
    if (userid > 0) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/complete/getcustomerid',
            dataType: 'json',
            type: 'post',
            data: {
                'userid': userid
            },
            success: function(data) {
                console.log(data);

                $("#companyname").val(data.companyname);
                $("#firstname").val(data.firstname);
                $("#lastname").val(data.lastname);
                $("#email").val(data.email);
                $("#address1").val(data.address1);
                $("#city").val(data.city);
                $("#postcode").val(data.postcode);
                //$("#country").val(data.companyname);
                $("#phonenumber").val(data.companyname);
                //$("#companyname").val(data.companyname);
                var btn_href = '#stepContent3';
                $('.step-trigger[href="' + btn_href + '"]').click();
            },
            error: function(errorThrown) {}
        });
    }else{
var btn_href = '#stepContent3';
                $('.step-trigger[href="' + btn_href + '"]').click();
    }
});
$("#customertype").change(function() {
    var sel = $('#customertype').find(":selected").text();
    console.log(sel);
    if (sel == "Existing Customer") {
        $('#step3').prop('disabled', true);
        $('#existing').show('slow');
        $('#customerid').val('');
    } else {
      $('#step3').prop('disabled', false);
        $('#existing').hide('slow');
        $('#customerid').val('0');
        $("#companyname").val('');
        $("#firstname").val('');
                $("#lastname").val('');
                $("#email").val('');
                $("#address1").val('');
                $("#city").val('');
                $("#postcode").val('');
                //$("#country").val(data.companyname);
                $("#phonenumber").val('');
                //$("#companyname").val(data.companyname);
    }
});

$("#migration").change(function() {
    var sel = $('#migration').find(":selected").val();
    console.log(sel);
    if (sel == "yes") {
      $('#migrationoption').show('slow');

    } else {

 $('#migrationoption').hide('slow');

    }
});
$( "#voipnumber" ).keyup(function() {
   var cust = $('#customertype').find(":selected").val();
   var userid = $('#customerid').val();
 var len = $(this).val();
if(len.length == 9){
   if(cust != 'old'){
 $('#step3').prop('disabled', false);
}else{

if(userid > 0){
$('#step3').prop('disabled', false);

}

}
}else if(len.length > 9){

  alert('The maximum lenght is 9');
  $('#step3').prop('disabled', true);
}else{
   $('#step3').prop('disabled', true);
}


});
$("#voipmove").change(function() {
    var sel = $('#voipmove').find(":selected").val();
    var cust = $('#customertype').find(":selected").val();
if (sel == "yes") {


      var len = $("#voipnumber").val();
if(len.length == 9){
 $('#step3').prop('disabled', false);
}



    $('#voipmoveoption').show('slow');
     $('#step3').prop('disabled', true);
    } else {
      $('#step3').prop('disabled', false);
      $('#voipmoveoption').hide('slow');

    }
});
</script>
<script
src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
crossorigin="anonymous"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/chart.js/dist/Chart.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/ckeditor/ckeditor.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap-validator/dist/validator.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/dropzone/dist/dropzone.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/editable-table/mindmup-editabletable.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/tether/dist/js/tether.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/slick-carousel/slick/slick.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/util.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/alert.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/button.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/carousel.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/collapse.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/dropdown.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/modal.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/tab.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/tooltip.js"></script>
<script src="https://client.united-telecom.be/assets/clear/bower_components/bootstrap/js/dist/popover.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://client.united-telecom.be/assets/clear/js/demo_customizer.js?version=4.3.0"></script>
<script src="https://client.united-telecom.be/assets/clear/js/main.js?version=4.3.20"></script>
<script type="text/javascript" src="https://client.united-telecom.be/assets/clear/js/autocomplete.js?version=2.8"></script>
<script type="text/javascript" src="https://client.united-telecom.be/assets/clear/js/gauge.min.js"></script>
<script type="text/javascript" src="https://client.united-telecom.be/assets/clear/js/datepickers.js?version=4.0"></script>

<script type="text/javascript" src="https://client.united-telecom.be/assets/clear/js/chat.js?version=1.16"></script>
<script type="text/javascript" src="https://client.united-telecom.be/assets/clear/js/sound.js"></script>
<script type="text/javascript" src="https://client.united-telecom.be/assets/clear/js/emojionearea.min.js"></script>
<script type="text/javascript" src="https://client.united-telecom.be/assets/clear/js/jquery.bootstrap-growl.min.js"></script>
<script type="text/javascript" src="https://client.united-telecom.be/assets/clear/js/jquery.dataTables.yadcf.js"></script>
</div>
</div>

<script>
$( document ).ready(function() {
  <?php if($logged === false){ ?>
 $('#stopme').modal({backdrop: 'static'});
  <?php } ?>
 

});
</script>

<div class="modal" tabindex="-1" role="dialog" id="stopme">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('You need to Identify your self'); ?></h5>
         
        </button>
      </div>
      <div class="modal-body">
        <center>
       <a type="button" class="google-button" href="<?php echo $redirect; ?>">
  <span class="google-button__icon">
    <svg viewBox="0 0 366 372" xmlns="http://www.w3.org/2000/svg"><path d="M125.9 10.2c40.2-13.9 85.3-13.6 125.3 1.1 22.2 8.2 42.5 21 59.9 37.1-5.8 6.3-12.1 12.2-18.1 18.3l-34.2 34.2c-11.3-10.8-25.1-19-40.1-23.6-17.6-5.3-36.6-6.1-54.6-2.2-21 4.5-40.5 15.5-55.6 30.9-12.2 12.3-21.4 27.5-27 43.9-20.3-15.8-40.6-31.5-61-47.3 21.5-43 60.1-76.9 105.4-92.4z" id="Shape" fill="#EA4335"/><path d="M20.6 102.4c20.3 15.8 40.6 31.5 61 47.3-8 23.3-8 49.2 0 72.4-20.3 15.8-40.6 31.6-60.9 47.3C1.9 232.7-3.8 189.6 4.4 149.2c3.3-16.2 8.7-32 16.2-46.8z" id="Shape" fill="#FBBC05"/><path d="M361.7 151.1c5.8 32.7 4.5 66.8-4.7 98.8-8.5 29.3-24.6 56.5-47.1 77.2l-59.1-45.9c19.5-13.1 33.3-34.3 37.2-57.5H186.6c.1-24.2.1-48.4.1-72.6h175z" id="Shape" fill="#4285F4"/><path d="M81.4 222.2c7.8 22.9 22.8 43.2 42.6 57.1 12.4 8.7 26.6 14.9 41.4 17.9 14.6 3 29.7 2.6 44.4.1 14.6-2.6 28.7-7.9 41-16.2l59.1 45.9c-21.3 19.7-48 33.1-76.2 39.6-31.2 7.1-64.2 7.3-95.2-1-24.6-6.5-47.7-18.2-67.6-34.1-20.9-16.6-38.3-38-50.4-62 20.3-15.7 40.6-31.5 60.9-47.3z" fill="#34A853"/></svg>
  </span>
  <span class="google-button__text">Sign in with Google</span>
</a>
</center>


      </div>
      <div class="modal-footer">
      
      </div>
    </div>
  </div>
</div>

</body>
</html>
