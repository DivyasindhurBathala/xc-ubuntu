<html>
<head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">


<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/clear/js/autocomplete.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet" id="bootstrap-css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
       <script>
    $( document ).ready(function() {
  var socket = io.connect('https://socket.united-telecom.be');
  socket.on('connect', function() {
    var pxmi = rand(100000000,999999999999);
    socket.emit('subscribe', {"batch": true, "channel":pxmi,"token":token});
       //console.log('joined :'+pxm);
  });
      socket.on('pxmi', function (data) {
      var location = JSON.parse(data.message);
      if(location.type == "address"){
         $.ajax({
        url: 'https://client.united-telecom.be/admin/complete/checkaddress',
        dataType: 'json',
        type: 'post',
        data: {
        'action': 'preordering',
        'method': 'GetDetailedProductAvailability',
        'orderid': location.proximus_orderid,
        },
        success: function(data1) {
          console.log(data1);

        }
      });

      }
     
    if(location.type == "product"){
         $.ajax({
        url: 'https://client.united-telecom.be/admin/complete/checkaddress',
        dataType: 'json',
        type: 'post',
        data: {
        'action': 'preordering',
        'method': 'GetDetailedNetworkAvailability',
        'orderid': location.proximus_orderid,
      },
        success: function(data1) {
          console.log(data1);

        }
      });

      }
       if(location.type == "network"){
  $.ajax({
        url: 'https://client.united-telecom.be/admin/complete/getXDSLResult',
        dataType: 'json',
        type: 'post',
        data: {
        'orderid': location.proximus_orderid,
      },
        success: function(data1) {
          console.log(data1);
            if(data1.result == "success"){
            if(data1.vdsl == "OK"){
            var btn_href = '#stepContent2';
            $("#stepContent1_loader").hide('slow');
            $('#addresscheck').prop('disabled', false);
            $('#showsuccess').html('<div class="col-md-12"><div class="alert alert-dismissible alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Congratulation</strong> VDSL is available with Maximum Download '+data1.min_download+' Mbps and Upload '+data1.min_upload+' Mbps</div></div>');
            $('#d').attr("data-value", data1.min_download);
            $('#u').attr("data-value", data1.min_upload);
            $('.step-trigger[href="' + btn_href + '"]').click();
            $('#proximus_orderid').val(data1.orderid);
            return false;
            }else{
                  $("#stepContent1_loader").hide('slow');
               $("#stepContent1").show('slow');
    
                $('#addresscheck').prop('disabled', false);
                $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Oh snap!</strong> VDSL(2) is not available on this address</div></div>');
            }

            }else{
              $("#stepContent1_loader").hide('slow');
               $("#stepContent1").show('slow');
    
                $('#addresscheck').prop('disabled', false);
               $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error</strong> '+data1.message+'</div></div>');

            }

          }
      });

       }
   });


  socket.on('notification', function (data) {
       var chatuser = 'Simson5702';
      $.ajax({
        url:  window.location.protocol + "//" + window.location.host + "/admin/auth/read_envelope/",
        dataType: 'json',
        type: 'post',
        data:{
          user: chatuser
        },
        success: function (data) {
          console.log(data);
        }
      });

     setTimeout(function() {
        $.bootstrapGrowl(data.message, {
            type: 'info',
            offset: {from: 'bottom', amount: 1}, // 'top', or 'bottom'
            align: 'right',
            width: 'auto',
            delay: 10000,
            stackup_spacing: 10,
            allow_dismiss: true
        });
      }, 1000);
  });
   });
</script>
<!------ Include the above in your HEAD tag ---------->
<script>
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');

});
</script>
<script>
    $(document).ready(function () {
$( "#type_install" ).change(function() {

    var selectedt = $(this).children("option:selected").val();
      if(selectedt =="CHANGE"){
       $('#showchange').show('slow');
      }else{
         $('#showchange').hide('slow');
      }
});


    $('#final').click(function(){
        console.log('click order');
 console.log( $('#formulier' ).serializeArray() );
  event.preventDefault();


    });
    });
    </script>
<style>
body{ 
    margin-top:40px; 
}

.stepwizard-step p {
    margin-top: 10px;
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 14px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 1px;
    background-color: #ccc;
    z-order: 0;

}

.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}

.btn-circle {
  width: 30px;
  height: 30px;
  text-align: center;
  padding: 6px 0;
  font-size: 12px;
  line-height: 1.428571429;
  border-radius: 15px;
}
</style>
</head>
<body>

<div class="container">
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
            <p>Validate Address</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
            <p>Poduct Configuration & Billing Information</p>
        </div>
        <div class="stepwizard-step">
            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
            <p>Confirm and Finalize</p>
        </div>
    </div>
</div>
<form role="form" id="formulier">
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Postcode</label>
                    <input name="install_postcode"   maxlength="100" type="text" id="postcodenum" required="required" class="form-control ui-autocomplete-input zipcomplete" placeholder="3110" />
                </div>
              
            </div>
             <div class="col-md-8">
             
                <div class="form-group">
                    <label class="control-label">City</label>
                    <input  name="cityname"  maxlength="100" type="text" id="cityname"  required="required" class="form-control" placeholder="Rotselaar"  />
                </div>
               
            </div>
               <div class="col-md-6">
               
              
                <div class="form-group">
                    <label class="control-label">Street</label>
                    <input name="install_street"  maxlength="100" type="text" id="street" required="required" class="form-control ui-autocomplete-input streetcomplete" placeholder="Wingepark" />
                </div>
              
            </div>
               <div class="col-md-2">
            
              
                <div class="form-group">
                    <label class="control-label">Number</label>
                    <input name="install_phousenumber"  maxlength="100" type="text" id="housenumber" required="required" class="form-control" placeholder="12" />
                </div>
              
            </div>
              <div class="col-md-2">
         
                <div class="form-group">
                    <label class="control-label">Alphabet</label>
                    <input name="alphabet"  maxlength="100" type="text" id="install_alphabet" class="form-control" placeholder="A" />
                </div>
              
            </div>
              <div class="col-md-2">
             
                <div class="form-group">
                    <label class="control-label">Mailbox</label>
                    <input name="bus" maxlength="100" type="text"  class="form-control" placeholder="Mailbox" />
                </div>
               <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Validate Address</button>
            </div>

        </div>
    </div>
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
                 <h3>Product Confirguration</h3>
            <div class="col-md-12">
           
                <div class="form-group">
                <label class="control-label">Product Name</label>
                   <select name="pid" class="form-control">
                    <option value="">Surf&Talk VDSL** €39,99/m</option>
                     <option value="">Surf@Home VDSL €23,99€/m</option>
                   </select>
                </div>

                 <div class="form-group">
                <label class="control-label">Activation Fee</label>
                   <select name="setup" class="form-control">
                    <option value="">Activation Fee 59,99</option>
                   </select>
                </div>

                  <div class="form-group">
                <label class="control-label">Technician Installation</label>
                   <select name="setup" class="form-control">
                     <option value="remote" selected>Remote Installation €0.00</option>
                    <option value="onsite">Full installation €99</option>
                   </select>
                </div>


                <div class="form-group">
                <label class="control-label">Modem</label>
                   <select name="setup" class="form-control">
                     <option value="ownmodem">Eigen Modem €0.00</option>
                    <option value="contract" selected>Wifi/Voice modem0€ (Contract 2 jaar)</option>
                      <option value="renting">Wifi/Voice modem19€ (Gedurende 6 maanden)</option>
                   </select>
                </div>

  
            </div>
               <h3>Installation Situation</h3>
            <div class="col-md-12">

                <div class="form-group">
                    <label class="control-label">Installation Type</label>
                   <select name="type_install" class="form-control" id="type_install">
                     <option value="PROVIDE" selected>New Line</option>
                    <option value="CHANGE">Move from Other Provider ex: Scarlet, Dommel, EdpNet, Proximus</option>    
                   </select>
                </div> 
              

            </div>
               <div id="showchange" style="display:none;">
             <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Easyswitch ID (Optional)</label>
                    <input name="easy_switch" maxlength="200" type="text"  class="form-control" placeholder="Enter Company Name" />
                </div>
               
            </div>

              <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Migrate my Fixline (Optional)</label>
                    <input name="pstnnumber" maxlength="200" type="text"  class="form-control" placeholder="Enter Company Name" />
                </div>
               
            </div>


              <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Circuitid (Optional)</label>
                    <input name="circuitid" maxlength="200" type="text"  class="form-control" placeholder="Enter Company Name" />
                </div>
               
            </div>

            </div>


  <h3>Billing Information</h3>
                     
             <div class="col-md-6">
              
                <div class="form-group">
                    <label class="control-label">Company Name</label>
                    <input name="companyname" maxlength="200" type="text" class="form-control" placeholder="Enter Company Name" />
                </div>
               
            </div>

                <div class="col-md-6">
             
                <div class="form-group">
                    <label class="control-label">VAT</label>
                    <input name="vat" maxlength="200" type="text"  class="form-control" placeholder="Enter VAT" />
                </div>
               
            </div>
     
             <div class="col-md-6">
             
                <div class="form-group">
                    <label class="control-label">Firstname</label>
                    <input name="firstname" maxlength="200" type="text" class="form-control" placeholder="Enter FirstName" />
                </div>
               
            </div>

                <div class="col-md-6">
              
                <div class="form-group">
                    <label class="control-label">Lastname</label>
                    <input name="lastname" maxlength="200" type="text"  class="form-control" placeholder="Enter LastName" />
                </div>
               
            </div>



                <div class="col-md-6">
              
                <div class="form-group">
                    <label class="control-label">Address</label>
                    <input name="address1" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Address and number" />
                </div>
               
            </div>

                <div class="col-md-2">
               
                <div class="form-group">
                    <label class="control-label">Postcode</label>
                    <input name="postcode" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter Postcode" />
                </div>
               
            </div>

                <div class="col-md-2">
               
                <div class="form-group">
                    <label class="control-label">City</label>
                    <input name="city" maxlength="200" type="text" required="required" class="form-control" placeholder="Enter City" />
                </div>
               
            </div>
                <div class="col-md-2">
               
                <div class="form-group">
                    <label class="control-label">Country</label>
                     <select name="country" class="form-control">
                     <option value="BE" selected>Belgium</option>
                    <option value="NL">Netherlands</option>
                      <option value="LU">Luxemburg</option>
                   </select>

                </div>
               
            </div>
              <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">National Number</label>
                    <input name="phonenumber" maxlength="200" required="required" type="text" class="form-control" placeholder="Enter NationalNr." />
                </div>
            </div>

                <div class="col-md-4">

                <div class="form-group">
                    <label class="control-label">Language</label>
                    <select name="language" class="form-control">
                     <option value="dutch" selected>Dutch</option>
                    <option value="french">French</option>
                      <option value="english">English</option>
                   </select>
                </div>
            </div>
             <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Phonenumber</label>
                    <input name="phonenumber" required="required" maxlength="200" type="text" class="form-control" placeholder="Enter Contact Phonenumber" />
                </div>
            </div>
               
               
                  <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <input name="email" required="required" maxlength="200" type="text"  class="form-control" placeholder="Enter Email" />
                </div>
                     </div>
         
                <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Password</label>
                    <input name="password" required="required" maxlength="200" type="text"  class="form-control" placeholder="Enter Password" />
                </div>
                  <button class="btn btn-primary nextBtn btn-lg pull-right" id="final" type="button" >Next</button>
            </div>
             



        </div>
    </div>
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3> Step 3</h3>
                <button class="btn btn-success btn-lg pull-right" type="submit">Finish!</button>
            </div>
        </div>
    </div>
</form>
</div>
</body>
</html>