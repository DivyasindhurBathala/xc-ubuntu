
<script>
$("#finalize").click(function() {
  $("#finalize").hide();
  $("#loader").show();
  var datastring = $("#ordering").serialize();
   $.ajax({
            url: '<?php echo base_url(); ?>client/service/activate_mobile',
            dataType: 'json',
            type: 'post',
            data:datastring,
            success: function(data) {
              if(data.result == "success"){
                $("#loader").hide();
                window.location = window.location.protocol + '//' + window.location.host + '/admin/complete/success';
              }else{
                $("#finalize").show();
                $("#errorme").html(data.message);
                $("#errorme").show('slow');
                $("#loader").hide();
              }
            },
            error: function(errorThrown) {

              console.log(errorThrown);
            }
        });

  });

</script>