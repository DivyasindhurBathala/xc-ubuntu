<div class="row">
  <div class="col-md-12">
    <div class="card-header bg-primary text-light"><?php echo lang('List Support Ticket'); ?></div>
    <div class="card">
      <div class="card-body">
        <table class="table table-hover" id="helpdesk">
          <thead>
            <tr>
              <th width="30%">#<?php echo lang('ID'); ?></th>
              <th width="60%"><?php echo lang('Subject'); ?></th>
              <th width="10%"><?php echo lang('Status'); ?></th>

            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#helpdesk').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax":window.location.protocol + '//' + window.location.host + '/client/table/get_mytickets',
"aaSorting": [[1, 'desc']],
"language": {
 "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},

"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/client/helpdesk/detail/' + aData[3] + '"><strong>' + aData[0] + '</strong></a>');
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/client/helpdesk/detail/' + aData[3] + '"><strong>' + aData[1] + '</strong></a>');


//$('td:eq(1)', nRow).html('<a href="https://invoice.xmusix.eu/admin/clientssummary.php?userid=' + aData[5] + '">' + aData[1] + '</a>');
return nRow;
},

});
});
});
</script>