
<div class="card">
  <div class="card-header bg-primary text-light">
    <?php echo lang('Subscription'); ?>:
    <?php echo $service->packagename; ?> (
    <?php echo $service->orderstatus; ?>)
  </div>
  <div class="card-body">
  <form id="portindemand">
    <div class="row">
    

        <?php if ($service->orderstatus == "PortinPending") { ?>
        <div class="col-sm-12  table-responsive">
        <div class="form-group">
          <label for="exampleInputEmail1"><?php echo lang('SIMCARD'); ?></label>
          <input name="msisdn_sim" type="number" class="form-control" id="simcard" value="<?php echo $service->details->msisdn_sim; ?>"
            readonly>
          <small id="text" class="form-text text-muted"><?php echo lang('Your Simcard'); ?>.</small>
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo lang('Mobile Number'); ?></label>
            <input name="msisdn" type="number" class="form-control" id="msisdn" value="<?php echo $service->details->msisdn; ?>"
              readonly>
            <small id="text" class="form-text text-muted"><?php echo lang('Your Number'); ?>.</small>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo lang('SMS Verification Code'); ?></label>
            <input name="code" type="text" class="form-control" id="code" value="">
            <small id="emailHelp" class="form-text text-muted"><?php echo lang('Please enter SMS code that has been sent to your current number to confirm the portin request'); ?>.</small>
          </div>

          <button id="confirm" class="btn btn-primary btn-md"><?php echo lang('Confirm'); ?></button>
          <button id="resend" class="btn btn-primary btn-md"><?php echo lang('Send New Code'); ?></button>
          </div>
        <?php } else { ?>
        <div class="col-sm-12 mr-auto ml-auto table-responsive">
        <?php if ($service->orderstatus == "OrderWaiting") { ?>
          <center><?php echo lang('Your order is still Pending, one of our agents is now busy preparing your Simcard'); ?></center>
        <?php } else { ?>
          <center><?php echo lang('Your Activation is in progress'); ?></center>
          <?php if ($service->details->msisdn_status == "PortinAccepted") { ?>
      <?php if($logs){ ?>     
<style>
  ul.timeline {
    list-style-type: none;
    position: relative;
}
ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
    z-index: 400;
}
ul.timeline > li {
    margin: 20px 0;
    padding-left: 20px;
}
ul.timeline > li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #22c0e8;
    left: 20px;
    width: 20px;
    height: 20px;
    z-index: 400;
}
</style>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h4><?php echo lang('Your order Progress'); ?></h4>
      <ul class="timeline">
       <?php foreach($logs as $item){ ?>
        <?php if(!strpos($item->description, 'Order Received')){ ?>
        <li>
          <a href="#"></a>
          <a href="#" class="float-right"><h6><?php echo $item->date; ?></h6></a>
          <p><h6><?php echo $item->description; ?></h6></p>
        </li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </div>
</div>



<?php } ?>



            <?php } ?>
        <?php } ?>
       

        <?php if ($service->orderstatus == "ActivationRequested") { ?>
          <center><?php echo lang('Your simcard will be activated on '); ?> <?php echo convert_contract($service->date_contract); ?> 00:00:00</center>
        <?php } ?>
        </div>
        <?php } ?>
        
        
      </div>

    </div>
    </form>
  </div>
</div>
<?php // print_r($service); ?>
<script>
  $("#confirm").click(function () {
    $("#confirm").prop('disabled', true);
   if($('#code').val() == ''){
     alert('<?php echo lang('Please enter your code'); ?>');
     $('#code').focus();
   }
    var datastring = $("#portindemand").serialize();
    $.ajax({
      url: '<?php echo base_url(); ?>client/portinondemand/confirm_code',
      dataType: 'json',
      type: 'post',
      data: datastring,
      success: function (data) {
        console.log(data);
        if (data.result == "success") {
          $("#loader").hide();
          window.location = window.location.protocol + '//' + window.location.host +
            '/client/portinondemand/success';
        } else {
          $("#finalize").prop('disabled', false);
          
        }
      },
      error: function (errorThrown) {

        console.log(errorThrown);
      }
    });

  });

  $("#resend").click(function () {
    $("#resend").prop('disabled', true);
   
    var datastring = $("#portindemand").serialize();
    $.ajax({
      url: '<?php echo base_url(); ?>client/portinondemand/request_code',
      dataType: 'json',
      type: 'post',
      data: datastring,
      success: function (data) {
        console.log(data);
        if (data.result == "success") {
          $("#loader").hide();
          window.location = window.location.protocol + '//' + window.location.host +
            '/client/portinondemand/success';
        } else {
          $("#finalize").prop('disabled', false);
          
        }
      },
      error: function (errorThrown) {

        console.log(errorThrown);
      }
    });

  });
</script>