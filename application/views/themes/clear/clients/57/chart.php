<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
     <script type = "text/javascript">
         google.charts.load('current', {packages: ['corechart']});
      </script>
  <script language = "JavaScript">
         function drawChart() {
            // Define the chart to be drawn.
            var data = google.visualization.arrayToDataTable([
               ['Year', 'Asia'],
               ['2012',  900],
               ['2013',  1000],
               ['2014',  1170],
               ['2015',  1250],
               ['2016',  1530]
            ]);
            var options = {title: 'Population (in millions)'};
            // Instantiate and draw the chart.
            var chart = new google.visualization.ColumnChart(document.getElementById('chart'));
            chart.draw(data, options);
         }
         google.charts.setOnLoadCallback(drawChart);
      </script>
  </head>
  <body>
    <div id="chart" style="width: 500px; height: 400px;"></div>
  </body>
</html>