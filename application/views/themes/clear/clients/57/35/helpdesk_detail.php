<div class="row">
	<div class="col-md-12">
		<div class="card-header bg-primary text-light"><?php echo lang('Support Ticket'); ?>: <?php echo $ticket['tid']; ?>
		</div>
		<div class="table-responsive card">
			<div class="card-body">
				<table class="table table-bordered table-striped">
					<thead>
						<th><?php echo lang('Ticket ID'); ?></th>
						<th><?php echo lang('Ticket Status'); ?></th>
						<th><?php echo lang('Ticket Department'); ?></th>
						<th><?php echo lang('Ticket Priority'); ?></th>
					</thead>
					<tbody>
						<tr>
							<td><?php echo $ticket['tid']; ?></td>
							<td><?php echo $ticket['status']; ?></td>
							<td><?php echo getDeptName($ticket['deptid']); ?></td>
							<td class="text-center"><?php echo $ticket['priority']; ?></td>
						</tr>
					</tbody>
				</table>
				<hr />
				<?php if (!empty($ticket['replies'])) {?>
				<?php foreach ($ticket['replies'] as $r) {?>
				<div style="padding-bottom: 10px;">
					<div class="card col-md-12">
						<div class="card-body">
							<?php echo nl2br($r['message']); ?>
							<div class="text-right"><h6 class="card-subtitle mb-2 text-muted"><i><?php echo lang('posted by'); ?> <?php if ($r['isadmin']) {?> <span class="text-warning">Agent</span><?php } else {?><?php echo $r['name']; ?><?php }?> <?php echo lang('on'); ?> <?php echo $r['date']; ?></i></h6></div>
						</div>
					</div>
				</div>
				<?php }?>
				<div style="padding-top: 10px; padding-bottom: 10px;">
					<button class="float-right btn btn-md btn-success" onclick="reply();">Reply</button>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>
<script>
	function reply(){
		$('#replyModal').modal('toggle');
	}
</script>
<div aria-hidden="true" aria-labelledby="reply" class="modal"  id="replyModal" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
				<?php echo lang('Respond'); ?>
				</h5>
				<button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
			</div>
			<form method="post" action="<?php echo base_url(); ?>client/helpdesk/reply" enctype="multipart/form-data">
				<input type="hidden" name="name" value="<?php echo $this->session->client['firstname'] . ' ' . $this->session->client['lastname']; ?>">
				<input type="hidden" name="isadmin" value="0">
				<input type="hidden" name="ticketid" value="<?php echo $ticket['id']; ?>">
				<div class="modal-body" id="modalonly">
					<div class="form-group">
						<label for="message">	<?php echo lang('Message'); ?></label>
						<textarea name="message" class="form-control" id="message" rows="6"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" type="submit" id="bos"> 	<?php echo lang('Send'); ?></button>
				</div>
			</form>
		</div>
	</div>
</div>