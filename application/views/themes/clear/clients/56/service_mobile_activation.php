<div class="card">
  <div class="card-header bg-primary text-light">
    <?php echo lang('Subscription'); ?>:
    <?php echo $service->packagename; ?> (
    <?php echo $service->orderstatus; ?>)
  </div>
  <div class="card-body">
  <form id="portindemand">
    <div class="row">
    

      <?php if($service->orderstatus == "PortinPending"){ ?>
        <div class="col-sm-12  table-responsive">
        <div class="form-group">
          <label for="exampleInputEmail1"><?php echo lang('SIMCARD'); ?></label>
          <input type="email" class="form-control" id="simcard" value="<?php echo $service->details->msisdn_sim; ?>"
            disabled>
          <small id="text" class="form-text text-muted"><?php echo lang('Your Simcard'); ?>.</small>
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo lang('Mobile Number'); ?></label>
            <input type="email" class="form-control" id="msisdn" value="<?php echo $service->details->msisdn; ?>"
              disabled>
            <small id="text" class="form-text text-muted"><?php echo lang('Your Number'); ?>.</small>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo lang('SMS Verification Code'); ?></label>
            <input type="text" class="form-control" id="exampleInputEmail1" value="">
            <small id="emailHelp" class="form-text text-muted"><?php echo lang('Please enter SMS code that has been sent to your current number to confirm the portin request'); ?>.</small>
          </div>

          <button id="confirm" class="btn btn-primary btn-md"><?php echo lang('Confirm'); ?></button>
          <button id="confirm" class="btn btn-primary btn-md"><?php echo lang('Send New Code'); ?></button>
          </div>
      <?php } else { ?>
        <div class="col-sm-12 mr-auto ml-auto table-responsive">
        <?php if($service->orderstatus == "OrderWaiting"){ ?>
          <center><?php echo lang('Your order is still Pending, one of our agents is now busy preparing your Simcard'); ?></center>
        <?php } else { ?>
          <center>Your Activation is in progress</center>
        <?php } ?>
       

        <?php if($service->orderstatus == "ActivationRequested"){ ?>
          <center><?php echo lang('Your simcard will be activated on '); ?> <?php echo convert_contract($service->date_contract); ?> 00:00:00</center>
        <?php } ?>
        </div>
      <?php } ?>
        
        
      </div>

    </div>
    </form>
  </div>
</div>
<?php // print_r($service); ?>
<script>
  $("#finalize").click(function () {
    $("#finalize").hide();
    $("#loader").show();
    var datastring = $("#ordering").serialize();
    $.ajax({
      url: '<?php echo base_url(); ?>client/service/activate_mobile',
      dataType: 'json',
      type: 'post',
      data: datastring,
      success: function (data) {
        if (data.result == "success") {
          $("#loader").hide();
          window.location = window.location.protocol + '//' + window.location.host +
            '/admin/complete/success';
        } else {
          $("#finalize").show();
          $("#errorme").html(data.message);
          $("#errorme").show('slow');
          $("#loader").hide();
        }
      },
      error: function (errorThrown) {

        console.log(errorThrown);
      }
    });

  });
</script>