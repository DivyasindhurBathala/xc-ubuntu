<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
    <?php echo lang('Client Login'); ?>
  </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/lumen.css?version=1.0">

  <!-- Font Awesome CSS-->
  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/style.default.css?version=1.0"
    id="theme-stylesheet">
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/login.css?version1.0">
  <!-- Tweaks for older IEs-->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
</head>

<body>

    <?php if (!empty($this->session->flashdata('error'))) {?>
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-danger" role="alert">
        <strong class="text-center">
          <?php echo $this->session->flashdata('error'); ?></strong>
      </div>
    </div>
  </div>
    <?php }?>
    <?php if (!empty($this->session->flashdata('success'))) {?>
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-warning" role="alert">
        <strong class="text-center">
          <?php echo $this->session->flashdata('success'); ?></strong>
      </div>
    </div>
  </div>
    <?php }?>

  <!-- Logo & Information Panel-->


  <div class="login-page">
    <img src="<?php echo $setting->logo_site; ?>" width="360">
    <br />
    <br />
    <div class="form">
      <form id="login-form" method="post" action="<?php echo base_url(); ?>client/portinondemand/confirm_code">
        <input type="number" value="0"  name="msisdn" required="" />
        <input type="text" value="" placeholder="SMS Code"  name="code" required="" />
        <button type="submit"><i class="fa fa-check"></i>
            <?php echo lang('Confirm Port In'); ?></button>
        <br />
        <br />
      </form>
      
      <p class="message">
        <?php echo lang('Please enter your phonenumber and your SMS Code that we have sent you!'); ?>, <?php echo lang('do you wish to ?'); ?> <a href="<?php echo base_url(); ?>client/portinondemand/request_new"><?php echo lang('request new code'); ?></a>
        <br />
        <?php if ($pod) { ?>
            <?php echo lang('You still have '); ?> <span class="text-danger"><?php echo 3-$pod->pod_counter; ?></span> <?php echo lang('time(s) left to request The code'); ?>
        
        <?php } ?>
        </p>
    </div>
  </div>




  <!-- Javascript files-->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script>
    $('#logindelta').click(function () {
      window.location.href = '<?php echo base_url(); ?>sso.php?login=true';

    })
  </script>

    <?php if (!empty($this->session->pending)) { ?>
      <script>
      $( document ).ready(function() {
  $('#myModal').modal({backdrop: 'static', keyboard: false});
});
      </script>

    <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please wait</h4>
      </div>
      <div class="modal-body">
        <center><img src="<?php echo base_url(); ?>assets/img/loading.gif"></center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <?php } ?>
</body>

</html>