
<div class="card">
  <div class="card-header bg-primary text-light">
    <?php echo lang('Subscription'); ?>: <?php echo $service->packagename; ?>
  </div>
  <div class="card-body">
    <pre>
<?php //print_r($service); ?>
</pre>
    <table class="table table-bordered">
      <thead>
        <tr>
        <th><?php echo lang('Order Id'); ?></th>
        <th><?php echo lang('Order Date'); ?></th>
        <th><?php echo lang('Status'); ?></th>
        <th><?php echo lang('Subscription Type'); ?></th>
        </tr>
      </thead>
      <tbody>
        <tr>
        <td class="text-success"><?php echo $service->id; ?></td>
        <td><?php echo $service->regdate; ?></td>
        <td><?php echo $service->orderstatus; ?></td>
        <td><?php echo ucfirst($service->details->msisdn_type); ?> SIM</td> 
        </tr>
      </tbody>
    </table>
  </div>
</div>

<script>
$("#finalize").click(function() {
  $("#finalize").hide();
  $("#loader").show();
  var datastring = $("#ordering").serialize();
   $.ajax({
            url: '<?php echo base_url(); ?>client/service/activate_mobile',
            dataType: 'json',
            type: 'post',
            data:datastring,
            success: function(data) {
              if(data.result == "success"){
                $("#loader").hide();
                window.location = window.location.protocol + '//' + window.location.host + '/admin/complete/success';
              }else{
                $("#finalize").show();
                $("#errorme").html(data.message);
                $("#errorme").show('slow');
                $("#loader").hide();
              }
            },
            error: function(errorThrown) {

              console.log(errorThrown);
            }
        });

  });

</script>

