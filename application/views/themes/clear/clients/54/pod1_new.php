<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?php echo lang('Porting on demand Request'); ?>
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Font Awesome CSS-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/pod.css?version1.0">
    <!-- Tweaks for older IEs-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  
    <?php if ($listen) { ?>
  <script>
    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = false;
    var pusher = new Pusher('09ff32d32ccc7d0cbd67', {
    cluster: 'eu',
    encrypted: true
    });
    var channel = pusher.subscribe('pod');
    channel.bind('<?php echo $this->session->pod; ?>', function(data) {
      //console.log(data);
      if(data.hasil){
        window.location.href = "<?php echo base_url(); ?>client/portinondemand";

      }
      
   
    });
    </script>
    <?php } ?>
</head>

<body>

    <?php if (!empty($this->session->flashdata('error'))) {?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-danger" role="alert">
                <strong class="text-center">
                    <?php echo $this->session->flashdata('error'); ?></strong>
            </div>
        </div>
    </div>
    <?php }?>
    <?php if (!empty($this->session->flashdata('success'))) {?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-warning" role="alert">
                <strong class="text-center">
                    <?php echo $this->session->flashdata('success'); ?></strong>
            </div>
        </div>
    </div>
    <?php }?>

    <!-- Logo & Information Panel-->



    <!------ Include the above in your HEAD tag ---------->
    <?php
//print_r($setting);
    ?>
    <div class="container contact-form">
        <center>
            <img src="<?php echo $setting->logo_site; ?>" alt="rocket_contact" />
        </center>
        <form id="reg-form" method="post">
            <h3>
                <?php echo 'Please enter your phonenumber'; ?>
            </h3>
            <div class="row">
                <div class="col-sm-3">
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="text" name="msisdn" class="form-control" placeholder="Number" value="0" id="msisdn" />
                    </div>


                </div>
                <div class="col-sm-3">
                </div>

            </div>
            <br />
            <br />

            <div class="row">
                <div class="col-sm-3">
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <input type="button" id="submiti" class="btn btn-danger btn-md btn-block" value="Request Code" />
                       <center> <img height="100" src="<?php echo base_url(); ?>assets/img/lg.recycle-spinner.gif" id="loader" style="display:none;"></center>
                    </div>
                </div>
                <div class="col-sm-3">
                </div>
            </div>


        </form>
    </div>

    <script>
        $('#submiti').click(function () {
            $('#submiti').hide();
            $('#loader').show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>client/portinondemand/request_code",
                dataType: 'json',
                data: {
                    'msisdn': $('#msisdn').val()
                },
                success: function (response) {
                    console.log(response);
                    if (response.result == "success") {
                        window.location.href = "<?php echo base_url(); ?>client/portinondemand";
                    } else {
                        $('#loader').hide();
                        $('#submiti').show();
                        alert(response.message);
                    }
                    $('#loader').hide();
                }
            });

        });
    </script>
</body>

</html>