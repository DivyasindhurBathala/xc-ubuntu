<div class="row">
  <div class="col-md-6 mr-auto ml-auto">
    <div class="card-header bg-primary text-light"><?php echo lang('Please enter your Number and the SMS code'); ?></div>
    <div class="table-responsive card">
      <div class="card-body">
        <table class="table table-hover" id="dt_invoice">
          <thead>
            <tr>
              <th>#<?php echo lang('Invoicenum'); ?></th>
              <th><?php echo lang('Date'); ?></th>
              <th><?php echo lang('Duedate'); ?></th>
              <th><?php echo lang('Status'); ?></th>
              <th><?php echo lang('Total'); ?></th>
              <th><?php echo lang('Download'); ?></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>