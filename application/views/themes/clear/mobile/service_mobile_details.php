
<div class="container">
<div id="detail" class="tabsx">
<ul class="list-group">
  <li class="list-group-item">
    <span class="badge"><?php echo $mobile->msisdn; ?></span>
    <?php echo lang('Number'); ?>
  </li>
  <li class="list-group-item">
    <span class="badge"><?php echo $mobile->msisdn_sim; ?></span>
    <?php echo lang('Simcard'); ?>
  </li>
  <li class="list-group-item">
    <span class="badge"><?php echo $mobile->msisdn_puk1; ?></span>
    <?php echo lang('PUK1'); ?>
  </li>
  <li class="list-group-item">
    <span class="badge"><?php echo $mobile->msisdn_puk2; ?></span>
    <?php echo lang('PUK2'); ?>
  </li>
</ul>

  </div>
  <div id="usage" class="tabsx" style="display:none;">
  <table class="table table-striped table-responsive table-bordered" id="cdr">
   <thead>
      <tr class="active">
        <th><?php echo lang('Begintime'); ?></th>
        <th><?php echo lang('Destination'); ?></th>
        <th><?php echo lang('Duration'); ?></th>
        <th><?php echo lang('Cost'); ?></th>
</tr>
</thead>
      </table>
</div>
<div id="setting" class="tabsx"  style="display:none;">
<div id="loadingx2" style="display:none;">
                <img src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" height="100">
</div>
<table class="table table-striped table-responsive table-bordered">
<tbody id="packagesx">
      </tbody>
  </table>
</div>
</div>
<script>
$('#nav-detail').click(function(){
$('.tabsx').hide();
$('#detail').show();
});
$('#nav-usage').click(function(){
  $('.tabsx').hide();
$('#usage').show();
});
$('#nav-setting').click(function(){
  $('.tabsx').hide();
$('#setting').show();
});
$('.packageid').change(function() {
var sn = '<?php echo $sn->SN; ?>';
var id = $(this).attr("id");
var serviceid =  '<?php echo $service->id; ?>';
var msisdn = '<?php echo trim($service->domain); ?>';
if($(this).is(":checked")) {
var val = "1";
//var returnVal = confirm("Are you sure?");
//$(this).attr("checked", returnVal);
}else{
var val = "0";
}
$.ajax({
url: '<?php echo base_url(); ?>client/service/change_packagesetting',
type: 'post',
dataType: 'json',
success: function (data) {
console.log(data);
},
data: {'sn': sn, 'id': id, 'val':val, 'msisdn':msisdn, 'serviceid':serviceid}
});
});
</script>
<script>
$( document ).ready(function() {
$('#loadingx2').show('slow');
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/complete/get_service_detail/<?php echo $this->uri->segment(4); ?>',
dataType: 'json',
success: function (data) {
  $('#loadingx2').hide('slow');
  if(data.packages){
$('#packagesx').show();
if(data.packages.length > 1 && data.packages.length != 0){
data.packages.forEach(function (b) {
console.log(b);
if(b.Available == "1"){
var t =' checked';
}else{
var t = '';
}
$('#packagesx').append(' <tr> <td>'+b.CallModeDescription+'</td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="'+b.PackageDefinitionId+'" type="checkbox"'+t+'><label class="custom-switch-btn" for="'+b.PackageDefinitionId+'"></label> </div></td> </tr>');
});
}else{
if(data.packages.Available == "1"){
var t =' checked';
}else{
var t = '';
}
$('#packagesx').append(' <tr> <td>1'+data.packages.CallModeDescription+'</td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="'+data.packages.PackageDefinitionId+'" type="checkbox"'+t+'><label class="custom-switch-btn" for="'+data.packages.PackageDefinitionId+'"></label> </div></td> </tr>');
}
$("#packagesx").append('<script> $(".packageid").change(function() { var userid = "<?php echo $service->userid; ?>"; var serviceid = "<?php echo $service->id; ?>"; var sn = "<?php echo $mobile->msisdn_sn; ?>"; console.log(sn); var id = $(this).attr("id"); var msisdn = "<?php echo trim($service->domain); ?>"; if($(this).is(":checked")) { var val = "1"; }else{ var val = "0"; } $.ajax({ url: window.location.protocol + "//" + window.location.host + "/admin/subscription/change_packagesetting", type: "post", dataType: "json", success: function (data) { console.log(data); }, data: {"sn": sn, "id": id, "val":val, "msisdn":msisdn, "userid":userid, "serviceid":serviceid} }); });<\/script>');
}

}
});
});
</script>
<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#cdr').DataTable({
"autoWidth": false,
    "ajax": {
    "url": window.location.protocol + '//' + window.location.host + '/admin/table/get_cdr/<?php echo $this->uri->segment(4); ?>',
    "dataSrc": ""
      },
    "aaSorting": [[0, 'desc']],
    "searching": false,
     "bPaginate": false,
    "bLengthChange": false,
    "columns": [
            { "data": "Begintime" },
            { "data": "MaskDestination" },
            { "data": "DurationConnection" },
            { "data": "CurNumUnitsUsed" }
        ],
    "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },

  });
});
});



</script>

