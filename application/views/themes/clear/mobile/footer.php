
<div class="container" style="padding-bottom: 50px;">
<a href="<?php echo base_url(); ?>mobile/auth/logout" class="btn btn-block btn-danger"><i class="fa fa-sign-out"></i> <?php echo lang('Logout'); ?></a>
<a href="<?php echo base_url(); ?>mobile/dashboard/account" class="btn btn-block btn-primary"><i class="fa fa-user"></i> <?php echo lang('My Account'); ?></a>
</div>
</div>

<div id="footer">
  <div class="col-xs-12 navbar-default navbar-fixed-bottom">
  <div class="row navbar-default"   style="padding-top: 5px;padding-bottom:10px;font-weight:bold;">
    <div class="col-xs-3 text-center"><a href="<?php echo base_url(); ?>mobile" style="color: #fff;"><i class="fa fa-home"></i><br><?php echo lang('Home'); ?></a></div>
    <div class="col-xs-3 text-center"><a href="<?php echo base_url(); ?>mobile/invoice" style="color: #fff;"><i class="fa fa-credit-card"></i><br><?php echo lang('Invoices'); ?></a></div>
    <div class="col-xs-3 text-center"><a href="<?php echo base_url(); ?>mobile/service" style="color: #fff;"><i class="fa fa-cubes"></i><br><?php echo lang('Services'); ?></a></div>
    <div class="col-xs-3 text-center"><a href="<?php echo base_url(); ?>mobile/dashboard/changepassword" style="color: #fff;"><i class="fa fa-key"></i><br><?php echo lang('Setting'); ?></a></div>
  </div>
  </div>
</div>
</body>
</html>