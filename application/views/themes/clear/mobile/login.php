<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo lang('Mobile site'); ?> - <?php echo lang('Login'); ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<style>
    .modal-content{
        height: 100%;
        position: fixed;
            background-color: #3267bc;
        }
        .btn-link{
            color:white;
        }
        .modal-heading h2{
            color:#ffffff;
        }
        </style>
</head>

<div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-heading">
                <h2 class="text-center"><img src="<?php echo $setting->logo_site; ?>" height="70"></h2>
            </div>
            <hr />
            <div class="modal-body">
                <form action="<?php echo base_url(); ?>mobile/auth" method="post" role="form">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-user"></span>
                            </span>
                            <input type="text" class="form-control" name="username" type="email" placeholder="User Name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-lock"></span>
                            </span>
                            <input type="password" class="form-control" placeholder="<?php echo lang('Password'); ?>" name="password" />
                        </div>

                    </div>

                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-warning btn-block btn-lg"><?php echo lang('Login'); ?></button>

                    </div>
                    <div class="form-group">
                        <a href="<?php echo base_url(); ?>mobile/auth/reset" class="btn btn-warning btn-block btn-lg"><?php echo lang('Reset Password'); ?></a>   
                    </div>

                </form>
                <?php if (!empty($this->session->flashdata('error'))) {?>
          <div class="row">
            <div class="col-xs-12">
              <div class="alert alert-danger" role="alert">
                <strong class="text-center"><?php echo $this->session->flashdata('error'); ?></strong>
              </div>
            </div>
          </div>
                <?php }?>
            <?php if (!empty($this->session->flashdata('success'))) {?>
          <div class="row">
            <div class="col-xs-12">
              <div class="alert alert-warning" role="alert">
                <strong class="text-center"><?php echo $this->session->flashdata('success'); ?></strong>
              </div>
            </div>
          </div>
            <?php }?>
            </div>
        </div>
    </div>
    
  
</body>
</html>