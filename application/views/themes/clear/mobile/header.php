<!DOCTYPE html>
<html lang="en">

<head>
  <title>Mobile site</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link href="<?php echo base_url();?>assets/mobile/lumen.css?version=1." rel="stylesheet" id="bootstrap-css">
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/mobile/custom.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
   <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
</head>

<body>
  <div id="wrap" style="padding-bottom: 40px;">
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="#">
            <?php echo $title; ?></a>
            <?php if ($this->uri->segment(3) == "detail" && $this->uri->segment(2) == "service") { ?>
          <div class="col-xs-12 navbar-default">
            <div class="row navbar-default" id="nav-tab" role="tablist" style="padding-top: 5px;padding-bottom:10px;font-weight:bold;">
              <div class="col-xs-4 text-center"><a style="color: #fff;" class="active" id="nav-detail"><i class="fa fa-mobile"></i><br>
                  <?php echo lang('Details'); ?></a></div>
              <div class="col-xs-4 text-center"><a style="color: #fff;" class="" id="nav-usage"><i class="fa fa-list-alt"></i><br>
                  <?php echo lang('Usage'); ?></a></div>
              <div class="col-xs-4 text-center"><a style="color: #fff;" class="" id="nav-setting"><i class="fa fa-cog"></i><br>
                  <?php echo lang('Setting'); ?></a></div>
            </div>
          </div>
            <?php } else { ?>
          <div class="pull-right"><img src="<?php echo $setting->logo_site; ?>" height="30" style="padding-top: 10px; padding-right: 5px;"></div>
            <?php } ?>
        </div>

      </div>
    </nav>