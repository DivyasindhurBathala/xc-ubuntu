
<img  style="display:none;" id="busy" height="70" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif">
<div class="container"   style="padding-bottom:30px;">
<div class="row">
<div class="invoice">
</div>
</div>
</div>
<script>
$(document).ready(function()
{
$('#busy').show('slow');
$.ajax({
type : 'POST',
url  : window.location.protocol + '//' + window.location.host + '/client/table/getclient_invoices',
dataType: 'json',
cache: false,
success :  function(result)
{
    $('#busy').hide('slow');
    $('.invoice').html("");
    result.forEach(function(item){
        console.log(item);
        if(item.iInvoiceStatus == "52"){
            $('.invoice').append('<div class="col-xs-12"><div class="panel panel-xs panel-default"><div class="panel-heading"><h3 style="color: #fff;"><?php echo lang('Invoice'); ?>: '+item.iInvoiceNbr+'</h3></div><div class="panel-body"> <ul class="list-group"> <li class="list-group-item"> <span class="badge">'+item.dInvoiceDate.slice(0,-13)+'</span> <?php echo lang('Date'); ?> </li> <li class="list-group-item"> <span class="badge">'+item.dInvoiceDueDate.slice(0,-13)+'</span> <?php echo lang('Duedate'); ?> </li> <li class="list-group-item"> <span class="badge">€'+item.mInvoiceAmount+'</span> <?php echo lang('Amount'); ?> </li> <li class="list-group-item"> <span class="badge">UnPaid</span> <?php echo lang('Status'); ?> </li> </ul><a class="btn btn-md btn-primary btn-block" href="<?php echo base_url(); ?>mobile/invoice/download/'+item.iInvoiceNbr+'"><i class="fa fa-download"></i> Download</a><button class="btn  btn-primary  btn-md btn-block"><i class="fa fa-tasks"></i> CDR</button></div> </div></div></div>');
        }elseif(item.iInvoiceStatus == "53"){
            $('.invoice').append('<div class="col-xs-12"><div class="panel panel-xs panel-default"><div class="panel-heading"><h3 style="color: #fff;"><?php echo lang('Invoice'); ?>: '+item.iInvoiceNbr+'</h3></div><div class="panel-body"> <ul class="list-group"> <li class="list-group-item"> <span class="badge">'+item.dInvoiceDate.slice(0,-13)+'</span> <?php echo lang('Date'); ?> </li> <li class="list-group-item"> <span class="badge">'+item.dInvoiceDueDate.slice(0,-13)+'</span> <?php echo lang('Duedate'); ?> </li> <li class="list-group-item"> <span class="badge">€'+item.mInvoiceAmount+'</span> <?php echo lang('Amount'); ?> </li> <li class="list-group-item"> <span class="badge">Sepa Requested</span> <?php echo lang('Status'); ?> </li><a class="btn btn-md btn-primary btn-block" href="<?php echo base_url(); ?>mobile/invoice/download/'+item.iInvoiceNbr+'"><i class="fa fa-download"></i> Download</a><button class="btn  btn-primary  btn-md btn-block"><i class="fa fa-tasks"></i> CDR</button></div> </ul> </div></div>');
        }else{
            $('.invoice').append('<div class="col-xs-12"><div class="panel panel-xs panel-default"><div class="panel-heading"><h3 style="color: #fff;"><?php echo lang('Invoice'); ?>: '+item.iInvoiceNbr+'</h3></div><div class="panel-body"> <ul class="list-group"> <li class="list-group-item"> <span class="badge">'+item.dInvoiceDate.slice(0,-13)+'</span> <?php echo lang('Date'); ?> </li> <li class="list-group-item"> <span class="badge">'+item.dInvoiceDueDate.slice(0,-13)+'</span> <?php echo lang('Duedate'); ?> </li> <li class="list-group-item"> <span class="badge">€'+item.mInvoiceAmount+'</span> <?php echo lang('Amount'); ?> </li> <li class="list-group-item"> <span class="badge">Paid</span> <?php echo lang('Status'); ?> </li><a class="btn btn-md btn-primary btn-block" href="<?php echo base_url(); ?>mobile/invoice/download/'+item.iInvoiceNbr+'"><i class="fa fa-download"></i> Download</a><button class="btn  btn-primary  btn-md btn-block"><i class="fa fa-tasks"></i> CDR</button></div> </ul> </div></div>');
        }

});
   }
});
});
</script>