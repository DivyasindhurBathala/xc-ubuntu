<div class="main-panel" style="padding-top:30px; padding-left:10px;padding-right:10px;">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6 mr-auto ml-auto">
                <div class="card border-light">
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url(); ?>reseller/subscription/reloadcredit">
                            <h4 class="card-title"><?php echo lang('My Balance'); ?> : <?php echo $setting->currency; ?><?php echo $agent->reseller_balance; ?></h4>
                            <?php if (!empty($this->session->flashdata('error'))) {?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger" role="alert">
                                        <strong class="text-center">
                                            <?php echo $this->session->flashdata('error'); ?></strong>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                            <?php if (!empty($this->session->flashdata('success'))) {?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-warning" role="alert">
                                        <strong class="text-center">
                                            <?php echo $this->session->flashdata('success'); ?></strong>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <label class="control-label" for="agent">
                                            <?php echo lang('MSISDN Number'); ?><span class="text-danger">*</span></label>
                                        <input name="agent" class="form-control" id="agent" type="number"
                                            placeholder="0" value="" required<?php if($agent->reseller_balance <= 0){ ?> disabled<?php } ?>>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <label class="control-label" for="agent">
                                            <?php echo lang('Amount'); ?><span class="text-danger">*</span></label>
                                        <input name="agent" class="form-control" id="agent" type="number" step="any"
                                            placeholder="0" value="" required<?php if($agent->reseller_balance <= 0){ ?> disabled<?php } ?>>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                       <button type="submit" class="btn btn-primary btn"<?php if($agent->reseller_balance <= 0){ ?> disabled<?php } ?>><?php echo lang('Submit'); ?></button>

                                       <br />
                                       <?php if($agent->reseller_balance <= 0){ ?><span class="text-danger"><?php echo lang('You do not have miminum credit to allow topup'); ?> <?php echo lang('Please topup your account!'); ?></span><?php } ?>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>