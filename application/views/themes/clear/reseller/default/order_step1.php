<div class="main-panel" style="padding-top:30px; padding-left:10px;padding-right:10px;">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-sm-8 mr-auto ml-auto">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title">Add Orders - Step 1</h4>
            <form method="post" action="<?php echo base_url(); ?>reseller/dashboard/order_step1">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">
                      <?php echo lang('Product'); ?>:</label>
                    <select class="form-control" id="addonproduct" name="pid">
                      <?php foreach (getProductSell($this->session->cid, $_SESSION['reseller']['id']) as $row) {?>
                      <option value="<?php echo $row->id; ?>">
                        <?php echo $row->name; ?>
                        <?php if ($row->setup > 0) {echo 'Setup fee: '.$this->data['setting']->currency . number_format($row->setup, 2);}?>
                      </option>
                      <?php }?>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">
                      <?php echo lang('Contract Duration'); ?> (
                      <?php echo lang('Month'); ?>)</label>
                    <select class="form-control" id="contractduration" name="contractduration">
                        <option value="1">
                        <?php echo lang('Monthly'); ?> (1 Month)</option>
                      <option value="3">
                        <?php echo lang('Quarterly'); ?> (3 Months)</option>
                      <option value="6">
                        <?php echo lang('Semi Annually'); ?> (6 Months)</option>
                      <option value="12" selected>
                        <?php echo lang('Annually'); ?> (12 Months)</option>
                      <option value="24">
                        <?php echo lang('Bienially'); ?> (24 Months)</option>

                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="">
                      <?php echo lang('Price Recurring'); ?></label>
                    <input name="recurring" type="text" class="form-control" id="harga" value="" readonly>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                  <label for=""><?php echo lang('Promotion'); ?></label>
                    <select class="form-control" id="promotion" name="promo_code">
                      <option value="None">
                        <?php echo lang('None'); ?>
                      </option>
                      <?php foreach (getPromotions() as $p) {?>
                      <option value="<?php echo $p['promo_code']; ?>">
                        <?php echo $p['promo_code']; ?> -
                        <?php echo $p['promo_name']; ?>
                      </option>
                      <?php }?>
                    </select>
                  </div>
                </div>


                  <div class="col-md-12">

                 <div class="form-group" id="addonx">
                    <label for=""><?php echo lang('Options'); ?>:</label>
                    <?php foreach (getAddonSell('mobile', $this->session->cid) as $row) {?>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input name="addon[<?php echo $row->id; ?>]" type="checkbox" class="form-check-input optionaddon" value="<?php echo $row->id; ?>"<?php if ($row->id == "18") {?> id="handset_id"<?php }?>>
                      <?php echo str_replace('Option ', '', $row->name) . ' '.$this->data['setting']->currency . str_replace('.', ',', number_format($row->recurring_total, 2)) . lang('/month'); ?>         </label>
                    </div>
                    <?php }?>
                  </div>

                </div>


              </div>


              <div class="row">
                <div class="col-md-4">
                  <a class="btn btn-block btn-md btn-primary" href="<?php echo base_url(); ?>reseller/dashboard/order"><i
                      class="fa fa-arrow-left"></i> Back</a>
                </div>
                <div class="col-md-4">
                  <button type="submit" class="btn btn-block btn-md btn-primary"><i class="fa fa-arrow-right"></i> Next</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
<script>
  $(document).ready(function () {
    var product = $('#addonproduct').find(":selected").val();
    var contractduration = $('#contractduration').find(":selected").val();
    console.log(product + ' ' + contractduration);
    $.ajax({
      url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/' + product + '/' + contractduration,
      dataType: 'json',
      success: function (data) {
        $('#harga').val(data.price);
      },
      error: function (errorThrown) {
        console.log(errorThrown);
      }
    });

    $("#addonproduct").change(function () {
      var product = $('#addonproduct').find(":selected").val();
      var contractduration = $('#contractduration').find(":selected").val();
      $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/' + product + '/' + contractduration,
        dataType: 'json',
        success: function (data) {
          $('#harga').val(data.price);
        },
        error: function (errorThrown) {
          console.log(errorThrown);
        }
      });
    });
    $("#contractduration").change(function () {
      var product = $('#addonproduct').find(":selected").val();
      var contractduration = $('#contractduration').find(":selected").val();
      $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/' + product + '/' + contractduration,
        dataType: 'json',
        success: function (data) {
          $('#harga').val(data.price);
        },
        error: function (errorThrown) {
          console.log(errorThrown);
        }
      });
    });
  });
</script>