
<pre>
  <?php //print_r($service);?>
</pre>
<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;padding-bottom:50px;">
  <div class="content-wrapper">
    <div class="row">
      <div class="col-sm-4">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title"><?php echo lang('Client Detail'); ?></h4>
            <table class="table table-hover table-striped" width="100%">
              <tr>
                <td><?php echo lang('Client Name'); ?></td>
                <td>
                  <?php echo format_name($client); ?>
                </td>
              </tr>
              <tr>
                <td><?php echo lang('Client ID'); ?></td>
                <td>
                  <?php echo $client->mvno_id; ?>
                </td>
              </tr>
              <tr>
                <td><?php echo lang('Address'); ?></td>
                <td>
                  <?php echo $client->address1; ?>
                  <?php echo $client->postcode; ?><br />
                  <?php echo $client->city; ?>
                  <?php echo $client->country; ?>
                </td>
              </tr>
              <tr>
                <td><?php echo lang('Email'); ?></td>
                <td>
                  <?php echo $client->email; ?>
                </td>
              </tr>
              <tr>
                <td><?php echo lang('Phonenumber'); ?></td>
                <td>
                  <?php echo $client->phonenumber; ?>
                </td>
              </tr>
            </table>
            <hr />
            <?php if (!empty($_SESSION['id'])) {
    ?>
            <a class="btn btn-md btn-danger btn-block"
              href="<?php echo base_url(); ?>admin/agent/detail/<?php echo $this->session->reseller['id']; ?>">
              <i class="fa fa-angle-double-left"></i> <?php echo lang('Back To Admin Dashboard'); ?></a>
            <?php
} ?>
            <a class="btn btn-md btn-dark btn-block"
              href="<?php echo base_url(); ?>reseller/dashboard/viewclient/<?php echo $client->id; ?>"><i
                class="fa fa-arrow-circle-left"></i> <?php echo lang('Back To Client Summary'); ?></a>
            <!--
            <?php if ($service->details->msisdn_type== "new") {
        ?>
            <button type="button" data-toggle="modal" data-target="#newModal"
              class="btn btn-md btn-primary btn-block">Download Welcome Letter</button>
            <?php
    } ?>
            <?php if ($service->details->msisdn_swap) {
        ?>
            <button type="button" data-toggle="modal" data-target="#swapModal"
              class="btn btn-md btn-primary btn-block">Download Swap Letter</button>
            <?php
    } ?>
            <?php if ($service->details->msisdn_type== "porting") {
        ?>
            <button type="button"
              onclick="pdfshow('porting', '<?php echo $service->id; ?>');"
              class="btn btn-md btn-primary btn-block">Download Porting Letter</button>
            <?php
    } ?>
            -->
            <!-- <button type="button" id="cdr" class="btn btn-md btn-primary btn-block">CDR details</button> -->
            <?php //if($service->companyid == 33){?>
            <?php if ($service->orderstatus == "Suspended") {
        ?>
            <button type="button" id="unsuspend" class="btn btn-md btn-dark btn-block"><i class="fa fa-play"></i>
            <?php echo lang('UnSuspend'); ?></button>
            <?php
    } else {
        ?>
            <button type="button" id="suspend" class="btn btn-md btn-dark btn-block"><i class="fa fa-pause"></i>
            <?php echo lang('Suspend'); ?></button>
            <?php
    } ?>
            <button type="button" data-toggle="modal" data-target="#Topup" class="btn btn-md btn-dark btn-block"><i
                class="fa fa-arrow-circle-up"></i> Topup</button>
            <button type="button" id="open_usage" class="btn btn-md btn-dark btn-block"><i class="fa fa-list"></i> <?php echo lang('Usage
              History'); ?></button>
            <button type="button" id="addbundle" class="btn btn-md btn-dark btn-block"><i class="fa fa-plus-circle"></i>
              <?php echo lang('Add Bundle'); ?></button>
            <?php //}?>

            <!--  <button type="button" id="terminate" class="btn btn-md btn-primary btn-block">Terminate Service</button>
                <button type="button" id="upgrade" class="btn btn-md btn-primary btn-block">Upgrade/Downgrade</button>

              -->


          </div>
        </div>
      </div>

      <div class="col-sm-8">
        <div class="card border-light">
          <div class="card-body">
<?php if ($service->details->msisdn_status != 'Active') {
        ?>
    <div class="alert alert-dismissible alert-warning">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4 class="alert-heading"><?php echo lang('Warning'); ?>!</h4>
  <p class="mb-0"><?php echo lang('This portin request has not been completed. Once completed via Ticket, please click'); ?> <button type="button" onclick="completePortin()" class="btn btn-sm btn-success"> <?php echo lang('Here'); ?> </button> <?php echo lang('to complete this portin'); ?>.</p>
</div>
<?php
    } ?>



            <h4 class="card-title"><?php echo lang('Subscription Detail'); ?>
            <?php if ($service->details->msisdn_status == "PortinPending") {
        ?>
              <?php echo $service->details->donor_msisdn; ?>
           <?php
    } else {
        ?>
            <?php echo $service->domain; ?>
          <?php
    } ?>
            </h4>
            <table class="table table-striped table-bordered">
              <thead>
                <tr style="background-color: #033C73;color:#fff">
                  <th><?php echo lang('ProductName'); ?>
                  </th>
                  <th><?php echo lang('Date Start'); ?>
                  </th>
                   <th><?php echo lang('Simcard Type'); ?>
                  </th>
                  <th><?php echo lang('Recurring'); ?>
                  </th>
                  <th><?php echo lang('Status'); ?>
                  </th>
                </tr>
                <tr>
                  <td>
                    <?php echo $service->packagename; ?>
                  </td>
                  <td>
                    <?php echo convert_contract($service->date_contract); ?>
                  </td>

                   <td>
                    <?php echo $service->details->msisdn_type; ?>
                  </td>
                  <td><?php echo $setting->currency; ?>
                    <?php echo number_format($service->recurring, 2); ?>
                  </td>
                  <td>
                    <?php echo $service->orderstatus; ?>
                  </td>
                </tr>
              </thead>
            </table>
            <hr />
            <table class="table table-striped table-bordered">
              <thead>
                <tr style="background-color: #033C73;color:#fff">
                  <th><?php echo lang('MSISDN'); ?>
                  </th>
                  <th><?php echo lang('Simcard'); ?>
                  </th>
                  <th><?php echo lang('PUK1'); ?>
                  </th>
                  <th><?php echo lang('PUK2'); ?>
                  </th>

                </tr>
                <tr>
                  <td>
                    <?php echo $service->details->msisdn; ?>
                  </td>
                  <td>
                    <?php echo $service->details->msisdn_sim; ?>
                  </td>
                  <td>
                    <?php echo $service->details->msisdn_puk1; ?>
                  </td>
                  <td>
                    <?php echo $service->details->msisdn_puk2; ?>
                  </td>

                </tr>
              </thead>
            </table>

            <hr />
            <?php if ($bundles) {
        ?>
            <table class="table table-striper table-bordered">
              <thead>
                <tr class="bg-dark  text-white">
                  <th>
                    <?php echo lang('BundleID'); ?>
                  </th>
                  <th>
                    <?php echo lang('Name'); ?>
                  </th>
                  <th>
                    <?php echo lang('Renewal period'); ?>
                  </th>
                  <th>
                    <?php echo lang('Auto Renew Left'); ?>
                  </th>
                  <th>
                    <?php echo lang('Next Renewal'); ?>
                  </th>
                  <th class="text-right">
                    <?php echo lang('Action'); ?>
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($bundles as $bundle) {
            ?>
                <tr>
                  <td>
                    <?php echo $bundle->arta_bundleid; ?>
                  </td>
                  <td>
                    <?php echo $bundle->name; ?>
                  </td>

                  <td>
                    <?php echo $bundle->terms.' '.$bundle->cycle; ?>s
                  </td>
                  <td class="text-center">
                    <?php echo $bundle->teum_autoRenew; ?>
                  </td>
                  <td>
                    <?php echo convert_date_to_uk_from_dash($bundle->teum_NextRenewal); ?>
                    00:00:00
                  </td>
                  <td class='text-right'>

                    <?php if ($bundle->teum_NextRenewal < date('Y-m-d')) {
                ?>
                    <button type="button"
                      onclick="orderthisbundle(<?php echo $bundle->addonid; ?>, '<?php echo $bundle->name; ?>')"
                      class="btn btn-sm btn-warning"><?php echo lang('ReOrder this Bundle'); ?></button>
                    <?php
            } else {
                ?>
                    <?php if ($bundle->teum_autoRenew > 0 && $bundle->base_bundle != 1) {
                    ?>
                    <button type="button"
                      onclick="enable_disable_autorenew(<?php echo $bundle->id; ?>, '<?php echo $bundle->name; ?>', 0)"
                      class="btn btn-sm btn-warning"><?php echo lang('Disable auto Renew'); ?></button>
                    <?php
                } else {
                    ?>
                    <button type="button"
                      onclick="enable_disable_autorenew(<?php echo $bundle->id; ?>, '<?php echo $bundle->name; ?>', 1)"
                      class="btn btn-sm btn-primary"><?php echo lang('Enable auto Renew'); ?></button>
                    <?php
                } ?>
                    <?php
            } ?>
                  </td>
                </tr>
                <?php
        } ?>
              </tbody>
            </table>

            <hr />
            <?php
    } ?>
            <div class="usage" style="display:none;">

              <form id="getusage">
                <div class="row">
                  <div class="col-4">
                    <input type="text" id="pickdate2" name="from" placeholder="FROM" class="form-control"
                      value="<?php echo date('Y-m-d', strtotime('-30 days')); ?>"
                      readonly>
                  </div>
                  <div class="col-4">
                    <input type="text" id="pickdate3" name="to" placeholder="TO" class="form-control"
                      value="<?php echo date('Y-m-d'); ?>"
                      readonly>
                  </div>
                  <div class="col-4">
                    <button class='btn btn-dark' type="button" onclick="getUsageNow()"><i class="fas fa-cog fa-spin"
                        style="display:none;"></i> <?php echo lang('Get usage'); ?>
                    </button>
                  </div>
                </div>
              </form>
              <br />
              <br />
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th><?php echo lang('From'); ?>
                    </th>
                    <th><?php echo lang('To'); ?>
                    </th>
                    <th><?php echo lang('Date'); ?>
                    </th>
                    <th><?php echo lang('Duration'); ?>
                    </th>
                    <th><?php echo lang('Amount'); ?>
                    </th>
                    <th><?php echo lang('Type'); ?>
                    </th>
                  </tr>
                </thead>
                <tbody id="usages">
                </tbody>
              </table>
              <hr />
            </div>
            <table class="table" id="bundlesx_table">
              <thead>
                <tr class="bg-dark  text-white">

                  <th width="30%">
                    <?php echo lang('Allowance'); ?>
                  </th>
                  <th width="20%">
                    <?php echo lang('Value'); ?>
                  </th>
                  <th width="20%">
                    <?php echo lang('Unit'); ?>
                  </th>

                  <th width="30%" class="text-right">
                    <?php echo lang('Expiration Date'); ?>

                  </th>
                </tr>
              </thead>
              <tbody id="tbody_bundlesx">
                <?php if ($service_info->resultCode == "0") {
        ?>
                <?php foreach ($service_info->resources as $row) {
            ?>
                <tr>
                  <td><?php echo $row->resourceName; ?>
                  </td>
                  <?php if ($row->resourceUnit == "SECOND") {
                ?>
                  <td><?php echo $row->resourceValue/60; ?>
                  </td>
                  <?php
            } elseif ($row->resourceUnit == "CENT") {
                ?>
                  <td><?php echo number_format(($row->resourceValue/100), 2); ?>
                  </td>

                  <?php
            } elseif ($row->resourceUnit == "KB") {
                ?>
                  <td><?php echo convertToReadableSizeKB($row->resourceValue*1000); ?>
                  </td>
                  <?php
            } elseif ($row->resourceUnit == "SMS") {
                ?>
                  <td><?php echo $row->resourceValue; ?>
                  </td>
                  <?php
            } else {
                ?>
                  <td><?php echo $row->resourceValue; ?>
                  </td>
                  <?php
            } ?>

                  <?php if ($row->resourceUnit == "SECOND") {
                ?>
                  <td>Minutes</td>
                  <?php
            } elseif ($row->resourceUnit == "CENT") {
                ?>
                  <td><?php echo $service_info->currency; ?>
                  </td>

                  <?php
            } elseif ($row->resourceUnit == "KB") {
                ?>
                  <td>MB</td>
                  <?php
            } elseif ($row->resourceUnit == "SMS") {
                ?>
                  <td><?php echo $row->resourceUnit; ?>
                  </td>
                  <?php
            } else {
                ?>
                  <td><?php echo $row->resourceUnit; ?>
                  </td>
                  <?php
            } ?>



                  <td class="text-right">
                    <?php echo convert_date_to_uk_from_dash($bundle->teum_NextRenewal); ?>
                    00:00:00
                  </td>

                </tr>
                <?php
        } ?>
                <?php
    } ?>
              </tbody>
            </table>
            <div id="loadingx1" style="display:none;">
              <center>
                <img
                  src="<?php echo base_url(); ?>assets/img/loader1.gif"
                  height="100" class="text-center">
              </center>
            </div>

            <hr />

          </div>
        </div>
      </div>


    </div>
  </div>
</div>

<form>
  <input type="hidden" id="msisdn"
    value="<?php echo trim($service->domain); ?>">
</form>


<div class="modal fade" id="portingModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="height: 700px;">
      <input type="hidden" name="SN"
        value="<?php echo $service->details->msisdn_sn; ?>">
      <input type="hidden" name="userid"
        value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn"
        value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid"
        value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Porting Letter'); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div id="portingPdf"></div>
            <table class="table table-striped">
              <div id="loadingx20" style="display:none;">
                <img
                  src="<?php echo base_url(); ?>assets/img/loader1.gif"
                  height="100">
              </div>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>


<div class="modal fade" id="newModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="height: 700px;">
      <input type="hidden" name="SN"
        value="<?php echo $service->details->msisdn_sn; ?>">
      <input type="hidden" name="userid"
        value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn"
        value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid"
        value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Welcome Letter'); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div id="newPdf"></div>
            <table class="table table-striped">
              <div id="loadingx30" style="display:none;">
                <img
                  src="<?php echo base_url(); ?>assets/img/loader1.gif"
                  height="100">
              </div>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>


<div class="modal fade" id="Suspend">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" style="height: 700px;">
      <input type="hidden" name="SN"
        value="<?php echo $service->details->msisdn_sn; ?>">
      <input type="hidden" name="userid"
        value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn"
        value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid"
        value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title"><?php echo lang('Swap Letter'); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div id="swapPdf"></div>
            <table class="table table-striped">
              <div id="loadingx40" style="display:none;">
                <img
                  src="<?php echo base_url(); ?>assets/img/loader1.gif"
                  height="100">
              </div>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>




<div class="modal fade" id="Package">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="bundleform" method="post"
        action="<?php echo base_url(); ?>reseller/subscription/Teum_addBundle">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Add 30 days bundle'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group bundle_select1" style="display:none;">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('This will activate Bundle for 30 days and auto renew depend on the amount month you have chosen'); ?></label>
                  <br />
                  <select class="form-control" id="bundleid1" name="bundleid">
                  </select>
                </fieldset>
              </div>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Auto Renew for how many months 30 days= 1 month'); ?></label>
                  <select class="form-control" name="month">
                    <option value="1"><?php echo lang('One Time'); ?>
                    </option>
                    <option value="1">1 <?php echo lang('Months'); ?>
                    </option>
                    <option value="3">3 <?php echo lang('Months'); ?>
                    </option>
                    <option value="6">6 <?php echo lang('Months'); ?>
                    </option>
                    <option value="12">12 <?php echo lang('Months'); ?>
                    </option>
                    <option value="24">24 <?php echo lang('Months'); ?>
                    </option>

                  </select>
                </fieldset>
              </div>


              <div class="form-group bundle_loading1">
                <center><img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>

            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-dark" onclick="disable_buttn('bundle')">
            <?php echo lang('Add Bundle'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" id="Topup">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="topupform" method="post"
        action="<?php echo base_url(); ?>reseller/subscription/Teum_Topup">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Topup Credit'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">

              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Please add Amount to topup'); ?>
                    (£)</label>
                  <input type="number" name="amount" step="any" id="amount" placeholder="10.00" class="form-control"
                    required>
                </fieldset>
              </div>


              <div class="form-group bundle_loading1" style="display:none;">
                <center><img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>

            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-dark" onclick="disable_buttn('topup')">
            <?php echo lang('Topup Credit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  function orderthisbundle(id, name) {
    $('#bundleidvalue').val(id);
    $('#ReorderBundleModal').modal('toggle');
  }

  function getUsageNow() {
    $('.fa-cog').show();
    $.ajax({
      url: '<?php echo base_url(); ?>reseller/subscription/Teum_Usage/<?php echo $this->session->cid; ?>',
      type: 'post',
      dataType: 'json',
      success: function(data) {
        if (data.statusCode == "0") {
          $('#usages').html('');
          if (data.usages.length > 0) {
            data.usages.forEach(function(el) {
              $('.fa-cog').hide('slow');
              $('#usages').append('<tr><td>' + el.from + '</td><td>' + el.to + '</td><td>' + el.date +
                '</td><td>' + el.duration + '</td><td>' + el.amount + '</td><td>' + el.serviceType +
                '</td></tr>');
            });
          }
        }
      },
      data: {
        'from': $('#pickdate2').val(),
        'to': $('#pickdate3').val(),
        'serviceid': '<?php echo $service->id; ?>'
      }

    });

  }

  function disable_buttn(type) {
    $('.btn').prop('disabled', true);
    if (type == "topup") {
      if ($('#amount').val() == '') {
        $('.btn').prop('disabled', false);
        alert('Please enter the value of the amount');
        $('#amount').focus();
      } else {
        $('#topupform').submit();
      }

    } else if (type == 'renew') {
      $('#renewform').submit();
    } else if (type == 'reorder') {
      $('#reorderform').submit();
     } else if (type == 'complete_portin') {
      $('#complete_portin').submit();
    } else {
      $('#bundleform').submit();
    }

  }
</script>
<script src="<?php echo base_url(); ?>assets/clear/js/pdfobject.js"></script>
<script>
  function getUsageNow() {
    $.ajax({
      url: '<?php echo base_url(); ?>reseller/subscription/Teum_Usage/<?php echo $this->session->cid; ?>',
      type: 'post',
      dataType: 'json',
      success: function(data) {
        if (data.statusCode == "0") {
          $('#usages').html('');
          if (data.usages.length > 0) {
            data.usages.forEach(function(el) {
              //$('#usages').html('');
              $('#usages').append('<tr><td>' + el.from + '</td><td>' + el.to + '</td><td>' + el.date +
                '</td><td>' + el.duration + '</td><td>' + el.amount + '</td><td>' + el.serviceType +
                '</td></tr>');
            });
          }
        }
      },
      data: {
        'from': $('#pickdate2').val(),
        'to': $('#pickdate3').val(),
        'serviceid': '<?php echo $service->id; ?>'
      }
    });

  }

  function enable_disable_autorenew(id, name, type) {
    $('#RenewModal').modal('toggle');
    $('#addonid').val(id);
    $('#typeaction').val(type);
    if (type == 1) {
      $('.autorenewmonth').show('slow');
      $('.disaling').html('');
      $('.paketname').html("Enable Auto renew :" + name);
    } else {
      $('.autorenewmonth').hide();
      $('.disaling').html('This will disable auto renew on the next Renewal Date');
      $('.paketname').html("Disable Auto renew :" + name);

    }
    //$('#delete_subscription').prop('disabled', true);
  }

  function pdfshow(type, serviceid) {

    $('#' + type + 'Modal').modal('toggle');
    if (type == "porting") {
      $('#loadingx20').show();
    }
    if (type == "new") {
      $('#loadingx30').show();
    }
    if (type == "swap") {
      $('#loadingx40').show();
    }

    PDFObject.embed(window.location.protocol + '//' + window.location.host + '/reseller/subscription/download_letter/' +
      serviceid + '/' + type, "#" + type + 'Pdf', {
        height: "600px"
      });
    $('#loadingx20').hide();
    $('#loadingx30').hide();
    $('#loadingx40').hide();

  }
</script>
<script>
  $('#open_usage').click(function() {
    $('.usage').show('slow');
  });
  $("#simcard").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/reseller/table/search_simcard',
        type: 'post',
        dataType: "json",
        data: {
          keyword: request.term
        },
        success: function(data) {
          $('#continue').prop('disabled', true); // display the selected tex
          response(data);
        }
      });
    },
    select: function(event, ui) {
      $('#simcard').val(ui.item.label); // display the selected tex
      $('#continue').prop('disabled', false); // display the selected tex

      return false;
    }
  });
</script>
<script>
  $('#addbundle').click(function() {
    var msisdn = '<?php echo trim($service->domain); ?>';
    $('#Package').modal('toggle');
    $.ajax({
      url: '<?php echo base_url(); ?>reseller/subscription/getBundleList/<?php echo $this->session->cid; ?>',
      type: 'post',
      dataType: 'json',
      success: function(data) {
        $('.bundle_loading1').hide();
        $('#bundleid1').html(data.html);
        $('.bundle_select1').show();

      },
      <?php if ($this->session->cid == 55) {
        ?>
      data: {
        'msisdn': msisdn,
        'type': 'option',
        'agid': '<?php echo $service->gid; ?>',
        'serviceid': '<?php echo $service->id; ?>'
      }
      <?php
    } else {
        ?>
      data: {
        'msisdn': msisdn,
        'type': 'option',
        'serviceid': '<?php echo $service->id; ?>'
      }
      <?php
    }?>
    });


    //$('#BundleModal').modal('toggle');
  });
</script>
<script>
  $(function() {
    $("#pickdate").datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      yearRange: "1902:2018"
    });
    $("#pickdate1").datepicker({
      dateFormat: 'yy-mm-dd'
    });
    $("#pickdate2").datepicker({
      dateFormat: 'yy-mm-dd'
    });
    $("#pickdate3").datepicker({
      dateFormat: 'yy-mm-dd'
    });
  });
</script>

<div class="modal fade" id="ReorderBundleModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="reorderform" method="post"
        action="<?php echo base_url(); ?>reseller/subscription/Teum_addBundle">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="bundleid" id="bundleidvalue" value="">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Add 30 days bundle'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">

              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Auto Renew for how many months, 1month = 30 days'); ?>
                  </label>
                  <select class="form-control" name="month">
                    <option value="1"><?php echo lang('One Time'); ?>
                    </option>
                    <option value="2">1 <?php echo lang('Months'); ?>
                    </option>
                    <option value="3">3 <?php echo lang('Months'); ?>
                    </option>
                    <option value="6">6 <?php echo lang('Months'); ?>
                    </option>
                    <option value="12">12 <?php echo lang('Months'); ?>
                    </option>
                    <option value="24">24 <?php echo lang('Months'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>

            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="disable_buttn('reorder')">
            <?php echo lang('Add Bundle'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="RenewModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="renewform" method="post"
        action="<?php echo base_url(); ?>reseller/subscription/Teum_Enable_Disable_Autorenew">

        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typeaction" id="typeaction" value="0">
        <input type="hidden" name="addonid" id="addonid" value="0">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <span class="paketname text-danger"></span>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="disaling">
              </div>
              <div class="form-group autorenewmonth"> <label for="">
                  <?php echo lang('Auto renew for '); ?>
                  (<?php echo lang('Month'); ?> 1
                  month = 30 days)</label>
                <select class="form-control" id="contractduration" name="ContractDuration">
                  <option value="1" selected><?php echo lang("One time"); ?>
                  </option>

                  <option value="3">3 <?php echo lang("Months"); ?>
                  </option>
                  <option value="6">6 <?php echo lang("Months"); ?>
                  </option>
                  <option value="12">12 <?php echo lang("Months"); ?>
                  </option>
                  <option value="24">24 <?php echo lang("Months"); ?>
                  </option>
                  <option value="36">36 <?php echo lang("Months"); ?>
                  </option>
                   <option value="600"><?php echo lang("Indefinite"); ?>
                  </option>
                </select>
              </div>
              <div class="form-group bundle_loading3" style="display:none;">
                <center>
                  <img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="disable_buttn('renew')">
            <?php echo lang('Submit'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="CompletePortin">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="complete_portin" method="post"
        action="<?php echo base_url(); ?>reseller/subscription/Teum_CompletePortin/<?php echo $service->id; ?>">

        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">

        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <span class="paketname text-danger">Complete portin: <?php echo $service->details->donor_msisdn; ?></span>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="disaling">
              </div>
              <div class="form-group autorenewmonth"> <label for="">
                  <?php echo lang('Are you sure the number has been portedin?'); ?>
                 </label>

              </div>
              <div class="form-group bundle_loading3" style="display:none;">
                <center>
                  <img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="disable_buttn('complete_portin')">
            <?php echo lang('Submit'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>


<script>
  function completePortin(){

    $('#CompletePortin').modal('toggle')


  }
</script>