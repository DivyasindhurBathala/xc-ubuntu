<?php //print_r($_SESSION); ?>
<div class="main-panel" style="padding-top:60px;padding-bottom:30px; padding-left:10px;padding-right:10px;">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-8 mr-auto ml-auto">
                <div class="card border-light">
                    <div class="card-body">
                        <h4 class="card-title">Add Orders - Step 2</h4>
                        <form method="post" action="<?php echo base_url(); ?>reseller/dashboard/order_step2">
                        <input type="hidden" name="donor_type" value="0">
                            <div class="form-group">
                                <label for="">
                                    <?php echo lang('SIM Order'); ?></label>
                                <select class="form-control" id="migration" name="msisdn_type">
                                    <option value="porting">
                                        <?php echo lang('Portability'); ?>
                                    </option>
                                    <option value="new" selected>
                                        <?php echo lang('New Number'); ?>
                                    </option>
                                </select>
                            </div>
                            <div id="migrationoption" style="display:none;">
                                <div class="form-group">
                                    <label for="">
                                        <?php echo lang('Mobile Number'); ?> :
                                        <?php echo getcountrycode($setting->country_base); ?>XXXXXXXXXX</label>
                                    <input class="form-control" autocomplete="new-username"
                                        placeholder="<?php echo lang('REQUIRED'); ?>" type="number" name="msisdn"
                                        id="msisdn" value="">
                                </div>
                                <div class="form-group">
                                    <label for="">
                                        <?php echo lang('PAC Code'); ?> : ABC123456</label>
                                    <input class="form-control" autocomplete="new-username"
                                        placeholder="<?php echo lang('REQUIRED'); ?>" type="text" name="donor_accountnumber"
                                        id="AccountNumber" value="">
                                </div>


                              
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <a class="btn btn-block btn-md btn-dark"
                                        href="<?php echo base_url(); ?>reseller/dashboard/order_step1"><i
                                            class="fa fa-arrow-left"></i> Back</a>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-block btn-md btn-dark"><i
                                            class="fa fa-arrow-right"></i> Next</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card border-light">
                    <div class="card-body">
                        <h4 class="card-title">Order Summary - Step 3</h4>
                        <table class="table table-striped table-hover">
                            <tr>
                                <td>Product Order</td>
                                <td>
                                    <?php echo getProductName($_SESSION['order']['pid']); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>Recurring Amount</td>
                                <td>
                                    <?php echo $_SESSION['order']['recurring']; ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Client ID</td>
                                <td>
                                    <?php echo $_SESSION['order']['clientid']; ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Client Name</td>
                                <td>
                                    <?php echo $client->firstname.' '.$client->lastname; ?>
                                </td>
                            </tr>

                            <tr>
                                <td>Type Order</td>
                                <td>
                                    <?php if ($_SESSION['order']['msisdn_type'] == "new") { ?>
                                    New Number
                                    <?php } else { ?>
                                    Porting Number
                                    <?php } ?>
                                </td>
                            </tr>

                            <?php if ($_SESSION['order']['msisdn_type'] != "new") { ?>
                            <tr>
                                <td>Number to Port</td>
                                <td>
                                    <?php echo $_SESSION['order']['msisdn']; ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        var mig = $('#migration').find(":selected").val();
        console.log(mig);
        if (mig == "porting") {
              $('#AccountNumber').prop('required', true);
              $('#msisdn').prop('required', true);
            $('#migrationoption').show('slow');
        } else {
            $('#migrationoption').hide('slow');
            $('#AccountNumber').prop('required', false);
            $('#msisdn').prop('required', false);
        }
      
        $("#migration").change(function() {
            var sel = $('#migration').find(":selected").val();
            if (sel == "porting") {
              $('#AccountNumber').prop('required', true);
              $('#msisdn').prop('required', true);
                $('#migrationoption').show('slow');
            } else {
              $('#AccountNumber').prop('required', false);
              $('#msisdn').prop('required', false);
                $('#migrationoption').hide('slow');
                $('#step3').prop('disabled', false);
            }
        });
    });
    </script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/clear/js/datepickers.js?version=1.11"></script>