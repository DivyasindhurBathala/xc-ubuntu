<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;padding-bottom:50px;">
<div class="content-wrapper">
<div class="row">
<?php if (reseller_allow_perm('reload', $_SESSION['reseller']['id'])) { ?>
    <?php if (strtolower($_SESSION['reseller']['reseller_type']) == "prepaid") { ?>
<div class="col-sm-4 mr-auto ml-auto">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title"><?php echo lang('Reload'); ?></h4>
    <div class="form-group">
      <label for="msisdn"><?php echo lang('MSISDN'); ?></label>
      <input type="text" class="form-control" name="msisdn" id="msisdn" placeholder="44XXXXXXXXXX"<?php if (getResellerBalance($_SESSION['reseller']['id']) < 10) {
            ?>disabled<?php
                                                                                                  } ?>>
    </div>
    <div class="form-group">
      <label for="msisdn"><?php echo lang('Amount'); ?> (<?php echo $setting->currency; ?>)</label>
      <input type="text" class="form-control"  name="amount" id="amount" placeholder="10.00"<?php if (getResellerBalance($_SESSION['reseller']['id']) < 10) {
            ?>disabled<?php
                                                                                            } ?>>
    </div>
    <button class="btn btn-primary" disabled><?php echo lang('Submit'); ?></button>
  </div>
</div>
</div>
    <?php } ?>
<?php } ?>


<?php if (reseller_allow_perm('addorder', $_SESSION['reseller']['id'])) { ?>
<div class="col-sm-12">

<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title"><?php echo langa('List Orders'); ?></h4>
     <table id="orders" class="table table-striped table-bordered">
        <thead>
          <tr>
          <th><?php echo lang('#Client ID'); ?></th>
                <th><?php echo lang('Package'); ?></th>
                <th><?php echo lang('Contact'); ?></th>
                <th><?php echo lang('Status'); ?></th>
                <th><?php echo lang('Amount'); ?></th>
                <th><?php echo lang('Identifier'); ?></th>
              </tr>
        </thead>
        <tbody>
</tbody>
</table>
  </div>
</div>

 
</div>
<?php } ?>


</div>
    </div>
</div>

 <script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/reseller/table/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#orders').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/reseller/table/getservices',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData[8] == date ){
$('td:eq(0)', nRow).html(aData[0] + '<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/new-sticker.png" height="16">');
}
$('td:eq(0)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[10]+'</a>');
$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/' + aData[9] + '">'+aData[2]+'</a>');
$('td:eq(3)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[3]+'</a>');

$('td:eq(4)', nRow).html('<?php echo $this->data['setting']->currency; ?>' + aData[4]);
return nRow;
},
});

});
});

</script>
