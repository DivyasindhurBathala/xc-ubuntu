<div class="main-panel" style="padding-top:30px; padding-left:10px;padding-right:10px;">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-sm-6" style="padding-top:20px;">
                <div class="card border-light">
                    <div class="card-body">
                        <form method="post" action="<?php echo base_url(); ?>reseller/dashboard/setting">
                            <h4 class="card-title">My Account</h4>
                          <!--
                                <?php if (!empty($this->session->flashdata('error'))) {?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger" role="alert">
                                        <strong class="text-center">
                                            <?php echo $this->session->flashdata('error'); ?></strong>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                            <?php if (!empty($this->session->flashdata('success'))) {?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-warning" role="alert">
                                        <strong class="text-center">
                                            <?php echo $this->session->flashdata('success'); ?></strong>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                            -->
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <label class="control-label" for="agent">
                                            <?php echo lang('Agent Name'); ?><span class="text-danger">*</span></label>
                                        <input name="agent" class="form-control" id="agent" type="text" placeholder="Company Name" value="<?php echo $agent->agent; ?>"
                                         required readonly>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <label class="control-label" for="contact_name">
                                            <?php echo lang('Contact Name'); ?></label>
                                        <input name="contact_name" class="form-control" id="contact_name" type="text" placeholder="<?php echo lang('Contact Name'); ?>"
                                         value="<?php echo $agent->contact_name; ?>" required>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="email">
                                            <?php echo lang('Email'); ?></label>
                                        <input name="email" class="form-control" id="email" type="text" placeholder="<?php echo lang('Email'); ?>"
                                         value="<?php echo $agent->email; ?>" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="phonenumber">
                                            <?php echo lang('Phonenumber'); ?></label>
                                        <input name="phonenumber" class="form-control" id="phonenumber" type="text" placeholder="<?php echo lang('Phonenumber'); ?>"
                                         value="<?php echo $agent->phonenumber; ?>" required>
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="email">
                                            <?php echo lang('Address'); ?></label>
                                        <input name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address1'); ?>"
                                         value="<?php echo $agent->address1; ?>" required>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="postcode">
                                            <?php echo lang('Postcode'); ?></label>
                                        <input name="postcode" class="form-control" id="postcode" type="text" placeholder="<?php echo lang('Postcode'); ?>"
                                         value="<?php echo $agent->postcode; ?>" required>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label class="control-label" for="city">
                                            <?php echo lang('City'); ?></label>
                                        <input name="city" class="form-control" id="city" type="text" placeholder="<?php echo lang('city'); ?>" value="<?php echo $agent->city; ?>"
                                         required>
                                    </div>
                                </div>


                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="Language">
                                            <?php echo lang('Language'); ?></label>


                                        <select class="form-control" id="language" name="language">
                                            <?php foreach (getLanguages() as $key => $lang) {?>
                                            <option value="<?php echo $key; ?>" <?php if ($key==$agent->language) {
                                                ?> selected
                                                            <?php }?>>
                                                <?php echo lang($lang); ?>
                                            </option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="Language">
                                            <?php echo lang('Country'); ?></label>
                                        <select class="form-control" id="country" name="country">
                                            <?php foreach (getCountries() as $key => $country) {?>
                                            <option value="<?php echo $key; ?>" <?php if ($key==$agent->country) {
                                                ?> selected
                                                            <?php }?>>
                                                <?php echo $country; ?>
                                            </option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <button class="btn btn-block btn-dark btn-md" type="submit">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-6"  style="padding-top:20px;">
            <div class="card border-light">
                    <div class="card-body">
                    <div class="card-header">
                    <?php echo lang('API Key'); ?>
                    <div class="float-right">
                    <button class="btn btn-primary btn-sm" id="AddAPI"><?php echo lang('Create New'); ?></button>
                    </div>
                    </div>
                    <table id="tokens" class="table table-striped table-bordered">
            <thead>
                <tr>
                <th><?php echo lang('Key'); ?></th>
                <th><?php echo lang('Date Created'); ?></th>
                <th><?php echo lang('Creted By'); ?></th>
                <th><?php echo lang('Tool'); ?></th>
              </tr>
            </thead>
            <tbody>
        </tbody>
        </table>
                    </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="KeyModal" tabindex="-1" role="dialog" aria-labelledby="ModalKeyLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>reseller/dashboard/key_add">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('New Key Generation'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo lang('Do you wish to generate new Token?'); ?><br />
                    <?php echo lang('You may generate up to 3 Keys.'); ?>
                    <br />
                    <div class="form-group" style="display:none;" id="hostbox">
                        <label for="exampleInputEmail1"><?php echo lang('Host Referal, ex: domain.com'); ?></label>
                        <input name="host" type="text" class="form-control" id="hostinput" aria-describedby="host" placeholder="domain.com">
                        <small id="Host" class="form-text text-muted"><?php echo lang('Enter your host referal for autologin'); ?></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo lang('Generate'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="IPModal" tabindex="-1" role="dialog" aria-labelledby="ModalIpLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>reseller/dashboard/ip_add">
                <input type="hidden" name="creator" value="<?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname'); ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Adding IPv4/6'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">IPv4/IPv6</label>
                        <input name="ip" type="text" class="form-control" id="ipv4" aria-describedby="IPHelp" placeholder="XXX.XXX.XXX.XXX">
                        <small id="emailHelp" class="form-text text-muted"><?php echo lang('Enter your IPv4/6 without space'); ?></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function()
{
$('#AddAPI').click(function(){
$('#KeyModal').modal('toggle');
});
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#tokens').DataTable({
    "autoWidth": false,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": true,
    "ajax": window.location.protocol + '//' + window.location.host + '/reseller/table/gettokens',
    "aaSorting": [[0, 'asc']],
        "language": {
      "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
    $('td:eq(3)', nRow).html('<a href="#" class="btn btn-sm btn-danger close" onclick="confirmation_delete_key(\''+aData[3]+'\');"><i class="fa fa-trash"></i></a>');
              return nRow;
        },

  });
});
});
</script>
<script>
function confirmation_delete_key(id) {
var answer = confirm("<?php echo lang('Do you wish to delete this api key'); ?>?")
if (answer) {
window.location.replace(window.location.protocol + "//" + window.location.host + "/reseller/dashboard/delete_key/" + id);
} else {
console.log("<?php echo lang('cancelled delete key'); ?> ");
}
}
</script>