<br />
<br />
<div class="main-panel" style="padding-bottom:60px;padding-top:40px; padding-left:10px;padding-right:10px;">
  <div class="content-wrapper">
    <div class="row">
    	 <div class="col-sm-4">
       <table class="table table-hover table-striped" width="100%">
              <tr>
                <td>Client Name</td>
                <td>
                  <?php echo format_name($client); ?>
                </td>
              </tr>
              <tr>
                <td>Client ID</td>
                <td>
                  <?php echo $client->mvno_id; ?>
                </td>
              </tr>
              <tr>
                <td>Address</td>
                <td>
                  <?php echo $client->address1; ?>
                  <?php echo $client->postcode; ?><br />
                  <?php echo $client->city; ?>
                  <?php echo $client->country; ?>
                </td>
              </tr>
              <tr>
                <td>Email</td>
                <td>
                  <?php echo $client->email; ?>
                </td>
              </tr>
              <tr>
                <td>Phonenumber</td>
                <td>
                  <?php echo $client->phonenumber; ?>
                </td>
              </tr>
            </table>
    	 </div>
      <div class="col-sm-8">
        <div class="card border-light">
          <div class="card-body">
            <h4 class="card-title">Assign Simcard (<?php echo ucfirst($service->details->msisdn_type); ?>) Number</h4>
               <div class="row">
                
                <div class="col-sm-6">

                	<?php if($service->details->msisdn_type == "porting"){ ?>
                        <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number To be Ported'); ?></label>
                  <input class="form-control" autocomplete="new-username" type="text" name="donor_msisdn" id="donor_msisdn" value="<?php echo $service->details->donor_msisdn; ?>">
                  </div>

                	  <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number'); ?></label>
                  <input class="form-control" autocomplete="new-username" type="text" name="msisdn" id="msisdn" value="<?php echo $service->details->msisdn; ?>">
                	</div>
                <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="simcard">
                        <?php echo lang('Simcard Number'); ?></label>
                      <input name="vat" class="form-control  ui-autocomplete-input" id="simcard" autocomplete="new-username" type="text" placeholder="89XXXXXXXXXXXXXXXXXXXX" value="<?php if(!empty($service->details->msisdn_sim)){ echo $service->details->msisdn_sim;  } ?>">
                    </fieldset>
                  </div>
                  <div class="row">
                  <div class="col-sm-6">
                  <input type="hidden" name="donor_provider" value="">
						</div>
						  <div class="col-sm-6">
                    <input type="hidden" name="donor_type" value="0">
						</div>
					</div>
					<div class="row">
				<div class="form-group col-sm-6">
                    <fieldset>
                      <label class="control-label" for="account_number">
                        <?php echo lang('PAC Number'); ?></label>
                      <input name="vat" class="form-control" id="donor_accountnumber" autocomplete="new-username" type="text" value="<?php echo $service->details->donor_accountnumber; ?>">
                    </fieldset>
                  </div>
                  <div class="form-group col-sm-6">
                  
                  </div>
              </div>
                	<?php }else{ ?>

                    <div class="row">
                  <div class="form-group col-md-4">
                    <fieldset>
                      <label class="control-label" for="simcard">
                        <?php echo lang('Simcard Number'); ?></label>
                  <input name="vat" class="form-control  ui-autocomplete-input" id="simcard" autocomplete="new-username" type="text" placeholder="89XXXXXXXXXXXXXXXXXXXX" value="<?php echo $service->details->msisdn_sim; ?>"<?php if(!empty($service->details->msisdn_sim)){ ?> readonly<?php } ?>>
                    </fieldset>
                  </div>
                  <div class="form-group col-md-4">
                    <fieldset>
                      <label class="control-label" for="simcard">
                        <?php echo lang('Msisdn'); ?></label>
                      <input class="form-control" id="msisdn_new" autocomplete="new-username" type="text"  value="<?php echo $service->details->msisdn; ?>" readonly>
                    </fieldset>
                  </div>
                  <div class="form-group col-md-4">
                    <fieldset>
                      <label class="control-label" for="simcard">
                        <?php echo lang('Package'); ?></label>
                      <input  class="form-control" id="packagename" autocomplete="new-username" type="text" value="<?php echo $service->packagename; ?>" readonly>
                    </fieldset>
                  </div>
                  </div>
                  <?php } ?>
                  <button class="pull-right btn btn-md btn-primary" id="continue" disabled>Activate</button> 
                  <div class="row">
                  <div class="col-md-4 ml-auto mr-auto loader" style="display: none;">
                    <img src="<?php echo base_url(); ?>/assets/img/lg.rotating-balls-spinner.gif">
                    <br />
                    <?php echo lang('Please wait...'); ?>
                  </div>
                </div>
                </div>
            </div>

        </div>
    </div>
</div>

</div>
</div>
</div>

<script>
$('#continue').click(function(){
	$('#continue').prop('disabled', true); // display the selected tex
   $('.loader').show();
  $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/reseller/subscription/activate_mobile/<?php echo $this->uri->segment(4); ?>',
    type: 'post',
    dataType: "json",
    data: {
     serviceid: '<?php echo $this->uri->segment(4); ?>',
     <?php if($service->details->msisdn_type == "porting"){ ?>
     donor_provider:  $('#donor_provider').val(),
     donor_msisdn: $('#msisdn').val(),
     donor_accountnumber: $('#donor_accountnumber').val(),
     donor_type: $('#donor_type').val(),
     date_wish: $('#date_wish').val(),
     <?php } ?>
     msisdn_sim:  $('#simcard').val()
    },
    success: function( data ) {
    	 $('.loader').hide();
    if(data.result == "success"){
    window.location.href = window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/<?php echo $this->uri->segment(4); ?>';
    }else{
    	alert(data.message);
    }
    }
   });

});
$( "#donor_type" ).change(function() {
var sel = $( "#donor_type option:selected" ).val();
if(sel == "0"){
$('#donor_accountnumber').prop('required', false);
}else{
$('#donor_accountnumber').prop('required', true);
}

});

$( "#simcard" ).autocomplete({
  source: function( request, response ) {
   $('#pelanggan').hide('slow');
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/reseller/table/search_simcard',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
    $('#continue').prop('disabled', true); // display the selected tex
     response( data );
    }
   });
  },
  select: function (event, ui) {
   $('#simcard').val(ui.item.value); // display the selected tex
   $('#msisdn_new').val(ui.item.msisdn);
   $('#continue').prop('disabled', false); // display the selected tex
  
   return false;
  }
 });

	</script>
	<script>
$( document ).ready(function() {
var sel = $( "#donor_type option:selected" ).val();
var msisdn_sim = $( "#simcard" ).val();
if(msisdn_sim){
  $('#continue').prop('disabled', false);
}
if(sel == "0"){
$('#donor_accountnumber').prop('required', false);
}else{
$('#donor_accountnumber').prop('required', true);
}
});
</script>