<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;padding-bottom:50px;">
<div class="content-wrapper">
<div class="row">
<div class="col-sm-12">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title">List Orders</h4>
     <table id="clients" class="table table-striped table-bordered">
        <thead>
          <tr>
          <th><?php echo lang('#Clientid'); ?></th>
                <th><?php echo lang('Customername'); ?></th>
                <th><?php echo lang('Email'); ?></th>
                <th><?php echo lang('Phonenumber'); ?></th>
                <th><?php echo lang('Address'); ?></th>
                <th><?php echo lang('Status'); ?></th>
              </tr>
        </thead>
        <tbody>
</tbody>
</table>
  </div>
</div>
</div>
</div>
    </div>
</div>

 <script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/reseller/table/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#clients').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/reseller/table/getclients',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData[8] == date ){
$('td:eq(0)', nRow).html(aData[0] + '<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/new-sticker.png" height="16">');
}
$('td:eq(0)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/' + aData[6] + '">'+aData[0]+'</a>');
$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/' + aData[6] + '">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/' + aData[6] + '">'+aData[2]+'</a>');
$('td:eq(3)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/' + aData[6] + '">'+aData[3]+'</a>');
return nRow;
},
});

});
});

</script>
