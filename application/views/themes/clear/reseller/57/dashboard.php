<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;padding-bottom:50px;">
<div class="content-wrapper">
<div class="row">
<?php if (reseller_allow_perm('reload', $_SESSION['reseller']['id'])) {
    ?>
    <?php if (strtolower($_SESSION['reseller']['reseller_type']) == "prepaid") {
        ?>


<div class="col-sm-4">
   <div class="modal-dialog">
    <div class="modal-content">
      <form id="topupform" method="post"
        action="<?php echo base_url(); ?>reseller/subscription/Teum_Topup">
       
        <input type="hidden" name="serviceid" id="topup_serviceid"
          value="<?php echo $service->id; ?>">
          <input type="hidden" name="type" value="json">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Reload MSISDN'); ?>
          </h4>
         
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
            <div class="form-group">
                <label for="msisdn"><?php echo lang('MSISDN'); ?></label>
                <input type="text" name="msisdn"  id="topup_msisdn" placeholder="44XXXXXXXXXX"  class="form-control  ui-autocomplete-input is-invalid topup_search">
                </div>

                <div class="form-group">
                <label for="msisdn"><?php echo lang('Amount'); ?></label>
                <input type="text" class="form-control" name="amount"  id="amount" placeholder="10.00">
                </div>
             
            


              <div class="form-group bundle_loading2">
                <center><img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>

            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-dark" id="topup" onclick="disable_buttn('topup')" disabled>
            <?php echo lang('Add Topup'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php if (reseller_allow_perm('addbundle', $_SESSION['reseller']['id'])) {
            ?>
<div class="col-sm-4">
   <div class="modal-dialog">
    <div class="modal-content">
      <form id="bundleform" method="post"
        action="<?php echo base_url(); ?>reseller/subscription/Teum_addBundle">
    
        <input type="hidden" name="serviceid" id="bundle_serviceid"
          value="<?php echo $service->id; ?>">
   
        <input type="hidden" name="type" value="json">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Add 30 days bundle'); ?>
          </h4>
        
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
            <div class="form-group">
                <label for="msisdn"><?php echo lang('MSISDN'); ?></label>
                <input type="text" name="msisdn"  id="bundle_msisdn" placeholder="44XXXXXXXXXX"  class="form-control ui-autocomplete-input is-invalid bundle_search">
                </div>
              <div class="form-group bundle_select1" style="display:none;">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('This will activate Bundle for 30 days and auto renew depend on the amount month you have chosen'); ?></label>
                  <br />
                  <select class="form-control" id="bundleid1" name="bundleid">
                  </select>
                </fieldset>
              </div>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Auto Renew for how many months 30 days= 1 month'); ?></label>
                  <select class="form-control" name="month">
                    <option value="1"><?php echo lang('One Time'); ?>
                    </option>
                    <option value="1">1 <?php echo lang('Months'); ?>
                    </option>
                    <option value="3">3 <?php echo lang('Months'); ?>
                    </option>
                    <option value="6">6 <?php echo lang('Months'); ?>
                    </option>
                    <option value="12">12 <?php echo lang('Months'); ?>
                    </option>
                    <option value="24">24 <?php echo lang('Months'); ?>
                    </option>

                  </select>
                </fieldset>
              </div>


              <div class="form-group bundle_loading1">
                <center><img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>

            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-dark" id="bundle" onclick="disable_buttn('bundle')" disabled>
            <?php echo lang('Add Bundle'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php
        } ?>
<div class="col-sm-4">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title"><?php echo lang('Credit History'); ?></h4>
     <table id="history" class="table table-striped table-bordered">
        <thead>
          <tr>
                <th><?php echo lang('Date'); ?></th>
                <th><?php echo lang('Type'); ?></th>
                <th><?php echo lang('Amount'); ?></th>
                <th><?php echo lang('Msisdn'); ?></th>
              </tr>
        </thead>
        <tbody>
</tbody>
</table>
  </div>
</div>
</div>
<?php
    } ?>
<?php
} ?>
<?php if (reseller_allow_perm('addorder', $_SESSION['reseller']['id'])) {
        ?>
<div class="col-sm-12">
<div class="card border-light">
  <div class="card-body">
    <h4 class="card-title"><?php echo lang('List Orders'); ?></h4>
     <table id="orders" class="table table-striped table-bordered">
        <thead>
          <tr>
          <th><?php echo lang('#Client ID'); ?></th>
                <th><?php echo lang('Package'); ?></th>
                <th><?php echo lang('Contact'); ?></th>
                <th><?php echo lang('Status'); ?></th>
                <th><?php echo lang('Amount'); ?></th>
                <th><?php echo lang('Identifier'); ?></th>
              </tr>
        </thead>
        <tbody>
</tbody>
</table>
  </div>
</div>

 
</div>
<?php
    } ?>
</div>
    </div>
</div>

 <script>
  $(document).ready(function()
{
  $( "#amount" ).keyup(function() {
  amount = $('#amount').val();
  msisdn = $('#topup_msisdn').val();
  if(amount  > '0'  && msisdn > 0 ){
    $('#topup').prop('disabled', false);
  }else{
    $('#topup').prop('disabled', true);
  }
});
//"option", "appendTo", ".eventInsForm"
 $( ".topup_search" ).autocomplete({
  minLength: 4,
  source: function( request, response ) {
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/reseller/subscription/getsimcard/',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
      $('#topup').prop('disabled', true);
     response( data );
    }
   });
  },
  select: function (event, ui) {
    console.log(ui.item.value);
    $('#topup_msisdn').addClass('is-valid').removeClass('is-invalid');
    $('#topup_msisdn').val(ui.item.msisdn);
    $('#topup_serviceid').val(ui.item.serviceid);
    var amount = $('#amount').val();
    if(amount >0){
      $('#topup').prop('disabled', false);
    }else{
      $('#topup').prop('disabled', true);
    }
  
   return false;
  }
 });
 $( ".bundle_search" ).autocomplete({
  minLength: 4,
  source: function( request, response ) {
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/reseller/subscription/getsimcard/',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
      
        $('#bundle').prop('disabled', true);
     response( data );
    }
   });
  },
  select: function (event, ui) {
    console.log(ui.item.value);
    $('#bundle_msisdn').addClass('is-valid').removeClass('is-invalid');
    $('#bundle_msisdn').val(ui.item.msisdn);
    $('#bundle_serviceid').val(ui.item.serviceid);
    $('#bundle').prop('disabled', false);
   return false;
  }
 });

  var msisdn = '<?php echo trim($service->domain); ?>';
    $('#Package').modal('toggle');
    $.ajax({
      url: '<?php echo base_url(); ?>reseller/subscription/getBundleList/<?php echo $this->session->cid; ?>',
      type: 'post',
      dataType: 'json',
      success: function(data) {
        $('.bundle_loading1').hide();
        $('#bundleid1').html(data.html);
        $('.bundle_select1').show();

      },
      <?php if ($this->session->cid == 55) {
        ?>
      data: {
        'msisdn': msisdn,
        'type': 'option',
        'agid': '<?php echo $service->gid; ?>',
        'serviceid': '<?php echo $service->id; ?>'
      }
      <?php
    } else {
        ?>
      data: {
        'msisdn': msisdn,
        'type': 'option',
        'serviceid': '<?php echo $service->id; ?>'
      }
      <?php
    }?>
    });
  $('.bundle_loading1').hide();
  $('.bundle_loading2').hide();
$.getJSON(window.location.protocol + '//' + window.location.host + '/reseller/table/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#orders').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/reseller/table/getservices',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData[8] == date ){
$('td:eq(0)', nRow).html(aData[0] + '<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/new-sticker.png" height="16">');
}
$('td:eq(0)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[10]+'</a>');
$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/reseller/dashboard/viewclient/' + aData[9] + '">'+aData[2]+'</a>');
$('td:eq(3)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/reseller/subscription/detail/' + aData[0] + '">'+aData[3]+'</a>');

$('td:eq(4)', nRow).html('<?php echo $this->data['setting']->currency; ?>' + aData[4]);
return nRow;
},
});

});
});

</script>
<script>
  $(document).ready(function()
{
 $.getJSON(window.location.protocol + '//' + window.location.host + '/reseller/table/get_lang', function(data) {
 $('#history').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/reseller/table/get_reseller_credit_usage',
"aaSorting": [[0, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(2)', nRow).html('- <?php echo $this->data['setting']->currency; ?>' + aData[2]);
return nRow;
},
});

});
});
</script>
<script>
 function disable_buttn(data){
$('#'+data).prop('disabled', true);
if(data == "bundle"){
  $('.bundle_loading1').show();
  $('#bundleform').submit();
}else if(data == "topup"){
  $('.bundle_loading2').show();
  $('#topupform').submit();
}
 }



</script>
