</div>
<nav class="navbar fixed-bottom navbar-expand-sm navbar-dark bg-dark">
      <a class="navbar-brand" href="#"><?php echo $setting->companyname; ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">

          <li class="nav-item">
            <a class="nav-link disabled" href="#">ArtiPanel 1.5c</a>
          </li>

        </ul>
      </div>
    </nav>
<script src="<?php echo base_url(); ?>assets/clear/reseller/bootstrap.min.js"></script>
 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/clear/css/progress.css">
 <?php if ($_SESSION['reseller']['reseller_type'] == "Prepaid") {
    ?>
 <div class="modal" id="reloadx">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo lang('Reload My Account'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p><?php echo lang('Please enter amount you wish to add'); ?></p>
        <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">

                                        <label class="control-label" for="amount">
                                            <?php echo lang('Amount'); ?><span class="text-danger">*</span></label>
                                        <input name="amount" class="form-control" id="amount" type="number" step="any"
                                            placeholder="0" value="" required>

                                    </div>
                                </div>
                            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="reload('cash');"><?php echo lang('Cash'); ?></button>
        <button type="button" class="btn btn-primary" onclick="reload('banktransfer');"><?php echo lang('Bank Transfer'); ?></button>
        <button type="button" class="btn btn-primary" onclick="reload('creditcard');"><?php echo lang('Credit Card'); ?></button>
      
      </div>
    </div>
  </div>
</div>
<script>
  function reload(type){
    var agentid = '<?php echo $_SESSION['reseller']['id']; ?>';
  var amount = $('#amount').val();
  $.ajax({
url: '<?php echo base_url(); ?>reseller/pay/reload/'+agentid+'/'+type,
type: 'post',
dataType: 'json',
success: function (i) {
  window.location.replace(i.url);
},
data: {agentid: agentid,  amount:amount, type:'reseller_reload', description:'Reload Reseller Credit',name:'reload '+agentid, qty:'1'},
});
  }
  
  </script>
 <?php
} ?>
</body>

</html>