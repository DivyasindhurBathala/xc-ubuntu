<div aria-hidden="true" aria-labelledby="replyticket" id="replyticket" class="modal fade bd-assign-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="assigntoother">
        Reply Ticket
        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
      </div>
      <div class="modal-body">
        <form id="replyForm" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>reseller/helpdesk/reply/<?php echo $this->uri->segment(4); ?>">
          <input type="hidden" name="relid" value="<?php echo $this->uri->segment(4); ?>">
          <input type="hidden" name="old_deptid" value="<?php echo $ticket['deptid']; ?>">
          <input type="hidden" name="old_assigne" value="<?php echo $ticket['admin']; ?>">
           <input type="hidden" name="old_priority" value="<?php echo $ticket['priority']; ?>">
            <input type="hidden" name="old_categoryid" value="<?php echo $ticket['categoryid']; ?>">
          <fieldset>
            <div class="row">
            <div class="form-group col-md-4">
              <label for="deptid">Department</label>
              <select class="form-control" id="deptid" name="deptid">
                <?php foreach (getDepartments($this->session->cid) as $dep) {?>
                <option value="<?php echo $dep['id']; ?>"<?php if ($dep["id"] == $ticket['deptid']) {
                    ?> selected<?php
                               }?>><?php echo $dep['name']; ?></option>
                <?php }?>
              </select>
            </div>
            <div class="form-group col-md-4">
              <label for="status">Change Status</label>
              <select class="form-control" id="status" name="status">
                <?php foreach (getStatuses() as $status) {?>
                <option value="<?php echo $status; ?>"<?php if ($status == "Answered") {
                    ?> selected<?php
                               }?>><?php echo $status; ?></option>
                <?php }?>
              </select>
            </div>
             <div class="form-group col-md-4">
              <label for="priority">Change Priority</label>
              <select class="form-control" id="priority" name="priority">
                <?php foreach (array('4', '3', '2', '1') as $prio) {?>
                <option value="<?php echo $prio; ?>"<?php if ($prio == $ticket['priority']) {
                    ?> selected<?php
                               }?>><?php echo $prio; ?></option>
                <?php }?>
              </select>
            </div>
          </div>
           <div class="row">
            <div class="form-group col-md-6">
              <label for="assigne">Assign to</label>
              <select class="form-control" id="assigne" name="assigne">
                <?php foreach (getStaf($this->session->cid) as $staf) {?>
                <option value="<?php echo $staf['id']; ?>"<?php if ($staf['id'] == $ticket['admin']) {
                    ?> selected<?php
                               }?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                <?php }?>
              </select>
            </div>
               <div class="form-group col-md-6">
              <label for="categoryid">Change Category</label>
              <select class="form-control" id="categoryid" name="categoryid">
                <?php foreach (getHelpdeskCategory($this->session->cid) as $row) {?>
                    <option value="<?php echo $row->id; ?>"<?php if ($ticket['categoryid'] == $row->id) {
                        ?> selected<?php
                                   }?>><?php echo $row->name; ?></option>
                <?php }?>
              </select>
            </div>
          </div>
            <div class="form-group">
              <label for="message">Message</label>
              <textarea class="form-control" id="message" rows="10" name="message" required></textarea>
            </div>
            <div class="form-group">
              <label for="multiple">Attachment</label>
              <input type="file" name="attachments[]" class="form-control" multiple="multiple">
            </div>
            <button type="submit" id="replybutton" class="btn btn-primary btn-block btn-success"><i class="fa fa-reply"></i> Submit</button>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<div aria-hidden="true" aria-labelledby="replyticket" id="closeticket" class="modal fade bd-assign-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="assigntoother">
        Close ticket
        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
      </div>
      <div class="modal-body">
        <center>Do you wish to cloe the ticket?</center>
        <br />
        <br />
        <center><h2> <?php echo $ticket['tid']; ?></h2></center>
        <br />
        <form enctype="multipart/form-data" id="myForm" method="post" action="<?php echo base_url(); ?>admin/helpdesk/close/<?php echo $this->uri->segment(4); ?>">
          <div class="form-group">
            <label for="exampleTextarea">Resolution</label>
            <textarea class="form-control" id="exampleTextarea" rows="10" name="message" required></textarea>
          </div>
          <input type="hidden" name="id" value="<?php echo $this->uri->segment(4); ?>">
          <input type="hidden" name="status" value="Closed">
          <button type="submit" id="closeticketing" onclick="close_ticket()"class="btn btn-primary btn-block btn-success"><i class="fa fa-reply"></i> Submit</button>
        </fieldset>
      </form>
    </div>
  </div>
</div>
</div>

<div class="main-panel" style="padding-top:40px; padding-left:10px;padding-right:10px;">
<div class="content-wrapper">
<div class="row">
<div class="col-sm-3">
<div class="card border-light">
  <div class="card-body">

          <table class="table table-striped">
            <tr>
              <td><?php echo lang('Client ID'); ?></td>
              <td><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->mvno_id; ?></a></td>
            </tr>
            <tr>
              <td><?php echo lang('Customer Name'); ?></td>
              <td><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->initial . ' ' . $client->firstname . ' ' . $client->lastname; ?></a></td>
            </tr>
            <tr>
              <td><?php echo lang('Ticket Department'); ?></td>
              <td><?php echo $ticket['deptname']; ?></td>
            </tr>
            <tr>
              <td><?php echo lang('Assigne'); ?></td>
              <td><?php echo $ticket['assigned']; ?></td>
            </tr>
            <tr>
              <td><?php echo lang('Ticket Category'); ?></td>
              <td><?php echo $ticket['categoryname']; ?></td>
            </tr>
            <tr>
              <td><?php echo lang('Date Created'); ?></td>
              <td><?php echo $ticket['date']; ?></td>
            </tr>
             <tr>
              <td><?php echo lang('Status'); ?></td>
              <td><strong><?php echo $ticket['status']; ?></strong></td>
            </tr>

             <tr>
              <td><?php echo lang('Priority'); ?></td>
              <td><strong<?php if ($ticket['priority'] < 3) {
                    ?> class="text-danger"<?php
                         }?>><?php echo $ticket['priority']; ?></strong></td>
            </tr>
          </table>
          <hr />
          <button id="close_ticket" class="btn btn-success btn-block"><i class="fa fa-window-close"></i> Close Ticket</button>
          <button id="assign_ticket" class="btn btn-success btn-block"><i class="fa fa-users"></i> Assign to others</button>
          <button id="reply_ticket" class="btn btn-success btn-block"><i class="fa fa-comment"></i> Respond to ticket</button>
           <button id="reply_ticket" class="btn btn-success btn-block" onclick="confirm_visible('<?php echo $ticket['id']; ?>');"><i class="fa fa-eye"></i> Set Visible to Client</button>
        </div>
      </div>
     
     
    </div>
    
    <div class="col-sm-9">
    <div class="card border-light">
  <div class="card-body">
    <?php if (!empty($ticket['replies'])) {?>
          <?php foreach ($ticket['replies'] as $key => $row) {?>
          <?php if ($row['isadmin']) {?>
    <section class="comment-list">
          <!-- First Comment -->
          <article class="row">
            <div class="col-md-2 col-sm-2 hidden-xs">
              <figure class="thumbnail">
              <img alt="" src="<?php echo base_url(); ?>assets/img/client.png" height="70">
                <figcaption class="text-center"></figcaption>
              </figure>
            </div>
            <div class="col-md-10 col-sm-10">
              <div class="panel panel-default arrow left">
                <div class="panel-body">
                  <header class="text-left">
                    <div class="comment-user"><i class="fa fa-user"></i><?php echo $row['name']; ?></div>
                    <time class="comment-date" datetime="16-12-2014 01:05"><i class="fa fa-clock-o"></i> <?php if ($key == "0") {?>posted on<?php } else {?>replied on<?php }?></span><span class="value"><?php echo date('d/m/Y H:i:s', strtotime($row['date'])); ?></span></time>
                  </header>
                  <div class="comment-post">
                    <p>
                    <?php echo $row['message']; ?>
                    </p>
                  </div>
                  <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                </div>
              </div>
            </div>
          </article>
          <?php } else{ ?>
            <article class="row">
            <div class="col-md-10 col-sm-10">
              <div class="panel panel-default arrow right">
                <div class="panel-body">
                  <header class="text-right">
                    <div class="comment-user"><i class="fa fa-user"></i> <?php echo $row['name']; ?></div>
                    <time class="comment-date" datetime="16-12-2014 01:05"><?php if ($key == "0") {?>posted on<?php } else {?>replied on<?php }?></span><span class="value"><?php echo date('d/m/Y H:i:s', strtotime($row['date'])); ?></span></time>
                  </header>
                  <div class="comment-post">
                    <p>
                    <?php echo $row['message']; ?>
                       </p>
                  </div>
                  <p class="text-right"><a href="#" class="btn btn-default btn-sm"><i class="fa fa-reply"></i> reply</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-2 col-sm-2 hidden-xs">
              <figure class="thumbnail">
              <img alt="" src="<?php echo base_url(); ?>assets/img/client.png" height="70">
                <figcaption class="text-center"><?php echo $row['name']; ?></figcaption>
              </figure>
            </div>
          </article>
          
          <?php } ?>
          <?php } ?>
          <?php } ?>
          </section>






  </div>
  </div>
   </div>
   </div>
   </div>
   </div>

   <script>
        function confirm_visible(id){

          var t = confirm('Are you sure?, this will allow customer to see all history conversations of this ticket');
          if(t){

             $.ajax({
      url: window.location.protocol + '//' + window.location.host + '/admin/complete/set_ticket_visible/<?php echo $this->session->cid; ?>',
      type: 'POST',
      data: {
        id: id
      },
      dataType: 'json',
      success: function(response){
      console.log(response);

      }
      });

          }


        }
      function close_ticket(){
      $('#closeticketing').prop('disabled', true);
      $('#closeticket').modal('toggle');
      document.getElementById("myForm").submit();
      }
      $('#assign_ticket').click(function(){
      $('#assigntosomeone').modal('toggle');
      });
      $('#replybutton').click(function(){
      $('#replyticket').modal('toggle');
      $('#replybutton').prop('disabled', true);
      document.getElementById("replyForm").submit();
      });
      $('#reply_ticket').click(function(){
      $('#replyticket').modal('toggle');
      });
      $('#close_ticket').click(function(){
      $('#closeticket').modal('toggle');
      $('#closeticket').modal('toggle');
      });
      </script>
        <script>
      $('document').ready(function() {
      $("#replybutton").click(function(event) {
      $('#replybox').show();
      $('#replybutton').hide();
      });
      $(".support-ticket").click(function(event) {
      var id =  $(this).attr('id');
      window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/'+id);
      });
      });

      </script>

<div aria-hidden="true" aria-labelledby="assign" id="assigntosomeone" class="modal fade bd-assign-modal-lg" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="assigntoother">
             Assign Ticket to
              </h5>
              <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> ×</span></button>
            </div>
            <form method="post" action="<?php echo base_url(); ?>admin/helpdesk/assigntother">
              <input type="hidden" name="ticketid" value="<?php echo $ticket['id']; ?>">
              <div class="modal-body" id="modalonly">
                <div class="form-group">
                  <label for="departmen">Agent</label>
                  <select class="form-control" id="departmen" name="adminid">
                    <?php foreach (getStaf($this->session->cid) as $staf) {?>
                      <?php if ($this->session->id != $staf['id']) {?>
                    <option value="<?php echo $staf['id']; ?>"><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                  <?php }?>
                    <?php }?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="departmen">Department</label>
                  <select class="form-control" id="departmen" name="deptid">
                    <?php foreach (getDepartments($this->session->cid) as $row) {?>
                    <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                    <?php }?>
                  </select>
                </div>
                <div class="modal-footer">
                  <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-reply"></i> Save changes</button>
                </div>
              </form>
            </div>
          </div>
        </div>