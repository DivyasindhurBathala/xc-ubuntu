<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Client List'); ?>
        <div class="float-right">
          <!-- <a class="btn btn-rounded btn-success" id="agentexport" href="<?php echo base_url(); ?>admin/agent/export_agents"><i
              class="fa fa-file-excel"></i>
            <?php echo lang('Export Agents'); ?></a> -->
       
        </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
          <?php echo lang('Accept Change request by Agent'); ?>
        </h5>
        <div class="table-responsive">

        

        		<?php //print_r($changes); ?>

        <form method="post" action="<?php echo base_url(); ?>admin/subscription/change_service">
       	<input type="hidden" name="agent_request" value="<?php echo $this->uri->segment(4); ?>">
        <input type="hidden" name="msisdn" value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="sn" value="<?php echo trim($mobile->msisdn_sn); ?>">
        <input type="hidden" name="requestor" value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
        <input type="hidden" name="old_pid" value="<?php echo $service->packageid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to Change the Package?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">

                <label class="control-label" for="msisdn">
                  <?php echo lang('CLI(Number)'); ?></label>
                <input class="form-control" type="text" value="<?php echo trim($service->domain); ?>" readonly>

              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Product'); ?>:</label>
                <select class="form-control" id="addonproduct" name="new_pid">
                  <?php foreach (getProductSell($this->session->cid) as $row) {?>
                  <option value="<?php echo $row->id; ?>"<?php if($changes->new_pid == $row->id){ ?> selected<?php } ?>>
                    <?php echo $row->name; ?>
                    <?php if ($row->setup > 0) {echo 'Setup fee: &euro;' . number_format($row->setup, 2);}?>
                  </option>
                  <?php }?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Contract'); ?> (
                  <?php echo lang('Month'); ?>)</label>
                <select class="form-control" id="contractduration" name="ContractDuration">

                  <option value="3"<?php if($changes->terms == "3"){ ?> selected<?php } ?>>
                    <?php echo lang('Quarterly'); ?> (3 Months)</option>
                  <option value="6"<?php if($changes->terms == "6"){ ?> selected<?php } ?>>
                    <?php echo lang('Semi Annually'); ?> (6 Months)</option>
                  <option value="12"<?php if($changes->terms == "12"){ ?> selected<?php } ?>>
                    <?php echo lang('Annually'); ?> (12 Months)</option>
                  <option value="24"<?php if($changes->terms == "24"){ ?> selected<?php } ?>>
                    <?php echo lang('Bienially'); ?> (24 Months)</option>

                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Price Recurring'); ?></label>
                <input name="new_price" type="text" class="form-control" id="harga" value="">
              </div>
            </div>

            <div class="col-sm-12">
              <div class="form-group">

                <label class="control-label" for="msisdn">
                  <?php echo lang('Date Start'); ?></label>
                <input name="date_commit" class="form-control" id="firstdate" type="text" value="<?php echo next_month_date(); ?>"
                  required readonly>
                <span class="text-help">
                  <?php echo lang('Please choose the date this package to be activated'); ?></span>

              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">

                <label for="exampleSelect1">
                  <?php echo lang('Charge Customer?'); ?></label>
                <select class="form-control" id="changecharge" name="charge">
                  <option value="YES" <?php if ($this->session->cid == "54") {?> selected
                    <?php }?>>Yes</option>
                  <option value="NO" <?php if ($this->session->cid != "54") {?> selected
                    <?php }?>>No</option>
                </select>

              </div>
            </div>

            <div class="col-sm-6" style="display:none;" id="changeamount">
              <div class="form-group">
                <label class="control-label" for="amount">
                  <?php echo lang('Amount'); ?> (€)</label>
                <input name="amount" id="charechangeamount" class="form-control" type="number" value="<?php echo round($service->mobile_changecost, 2); ?>">

              </div>
            </div>


          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes Change Package'); ?></button>
        </div>
      </form>



        </div>
    </div>
</div>
</div>
</div>
<script>
	    $(document).ready(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
//console.log(product+' '+contractduration);
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});
$("#contractduration").change(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});

$("#addonproduct").change(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});
</script>
