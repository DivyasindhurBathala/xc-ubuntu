<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
                <?php echo lang('Client'); ?>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                    <?php echo lang('Add New Clients'); ?>
                    <pre>
                    <?php if ($this->session->email == "simson.parlindungan@united-telecom.be") {
    ?>
                    </pre>
                    <?php //print_r($setting);?>
                    <?php
} ?>

                    <?php if ($setting->country_base == "UK") {
        ?>
                    <div class="float-right"><button type="button" onclick="GenAnn()"
                            class="btn btn-sm btn-primary"><?php echo lang('Generate Anonymous'); ?></button></div>
                    <?php
    } ?>

                </h5>
                <div class="form-desc">
                    <?php echo lang('All Fields supose to be filled correctly'); ?>
                </div>
                <form method="post" id="formdata">
                    <div class="row">
                        <?php if (in_array($this->session->cid, array(53, 56))) {
        ?>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="vat"><?php echo lang('Relation ID'); ?><span
                                            class="text-danger">*</span></label>
                                    <input name="mvno_id" class="form-control" id="deltaid" type="text"
                                        placeholder="<?php echo lang('Delta Customerid'); ?>"
                                        <?php if (!empty($_SESSION['registration']['mvno_id'])) {
            ?>
                                        value="<?php echo $_SESSION['registration']['mvno_id']; ?>" <?php
        } ?> required>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="vat"><?php echo lang('Delta Username'); ?><span
                                            class="text-danger"></span></label>
                                    <input name="sso_username" class="form-control" id="sso_username" type="text"
                                        placeholder="<?php echo lang('Please enter valid Delta Username for SSO'); ?>"
                                        <?php if (!empty($_SESSION['registration']['sso_username'])) {
            ?>
                                        value="<?php echo $_SESSION['registration']['sso_username']; ?>" <?php
        } ?>>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="vat"><?php echo lang('VAT'); ?></label>
                                    <input name="vat" class="form-control" id="vat" type="text"
                                        placeholder="<?php echo lang('Vat Number'); ?>"
                                        <?php if (!empty($_SESSION['registration']['vat'])) {
            ?>
                                        value="<?php echo $_SESSION['registration']['vat']; ?>" <?php
        } ?>>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label"
                                        for="companyname"><?php echo lang('Companyname'); ?></label>
                                    <input name="companyname" class="form-control" id="companyname" type="text"
                                        placeholder="<?php echo lang('Comapnyname'); ?>"
                                        <?php if (!empty($_SESSION['registration']['companyname'])) {
            ?>
                                        value="<?php echo $_SESSION['registration']['companyname']; ?>" <?php
        } ?>>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <?php
    } else {
        ?>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <fieldset>
                                <label class="control-label" for="vat"><?php echo lang('Relation ID'); ?><span
                                        class="text-danger"></span></label>
                                <input name="mvno_id" class="form-control" id="deltaid" type="text"
                                    placeholder="<?php echo lang('Your Customerid'); ?>"<?php if ($setting->mvno_id_increment) {
            ?> readonly<?php
        } ?>>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <fieldset>
                                <label class="control-label" for="vat"><?php echo lang('VAT'); ?></label>
                                <input name="vat" class="form-control" id="vat" type="text"
                                    placeholder="<?php echo lang('Vat Number'); ?>"
                                    <?php if (!empty($_SESSION['registration']['vat'])) {
            ?>
                                    value="<?php echo $_SESSION['registration']['vat']; ?>" <?php
        } ?>>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <fieldset>
                                <label class="control-label"
                                    for="companyname"><?php echo lang('Companyname'); ?></label>
                                <input name="companyname" class="form-control" id="companyname" type="text"
                                    placeholder="<?php echo lang('Comapnyname'); ?>"
                                    <?php if (!empty($_SESSION['registration']['companyname'])) {
            ?>
                                    value="<?php echo $_SESSION['registration']['companyname']; ?>" <?php
        } ?>>
                            </fieldset>
                        </div>
                    </div>
            </div>
            <?php
    } ?>
            <div class="row">

                <div class="col-md-3">
                    <div class="form-group">
                        <label for=""><?php echo lang('Agent/Reseller'); ?> <a
                                href="<?php echo base_url(); ?>admin/agent">(Show/Add agents)</a></label>
                        <select class="form-control" name="agentid" id="agentid">
                            <?php foreach (get_agents($this->session->cid) as $a) {
        ?>
                            <option value="<?php echo $a->id; ?>" <?php if (getDefaultAgentid($this->session->cid) == $a->id) {
            ?> selected<?php
        } ?>><?php echo $a->agent; ?></option>
                            <?php
    }?>
                        </select>
                    </div>
                </div>


                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="gender"> <?php echo lang('Gender'); ?><span
                                class="text-danger">*</span></label>
                        <select class="form-control" id="gender" name="gender">
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for=""><?php echo lang('Initial'); ?></label><input class="form-control"
                            placeholder="<?php echo lang('Initial'); ?>" name="initial" type="text" id="Initial">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for=""><?php echo lang('Salutation'); ?></label><input class="form-control"
                            placeholder="<?php echo lang('Salutation'); ?>" name="salutation" type="text"
                            id="salutation">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="firstname"><?php echo lang('Firstname'); ?><span
                                    class="text-danger">*</span></label>
                            <input name="firstname" class="form-control" id="firstname" type="text"
                                placeholder="<?php echo lang('Firstname'); ?>"
                                <?php if (!empty($_SESSION['registration']['firstname'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['firstname']; ?>" <?php
    }?> required>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="lastname"><?php echo lang('Lastname'); ?><span
                                    class="text-danger">*</span></label>
                            <input name="lastname" class="form-control" id="lastname" type="text"
                                placeholder="<?php echo lang('Lastname'); ?>"
                                <?php if (!empty($_SESSION['registration']['lastname'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['lastname']; ?>" <?php
    }?> required>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="address1"><?php echo lang('Address'); ?><span
                                    class="text-danger">*</span></label>
                            <input name="address1" class="form-control" id="address1" type="text"
                                placeholder="<?php echo lang('Address'); ?>"
                                <?php if (!empty($_SESSION['registration']['address1'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['address1']; ?>" <?php
    }?> required>
                        </fieldset>
                    </div>
                </div>

                <div class="col-sm-1">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="housenumber"><?php echo lang('Housenumber'); ?><span
                                    class="text-danger">*</span></label>
                            <input name="housenumber" class="form-control" id="housenumber" type="text"
                                placeholder="<?php echo lang('Number'); ?>"
                                <?php if (!empty($_SESSION['registration']['housenumber'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['housenumber']; ?>" <?php
    }?> required>
                        </fieldset>
                    </div>
                </div>

                <div class="col-sm-1">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="alphabet"><?php echo lang('Alphabet'); ?><span
                                    class="text-danger"></span></label>
                            <input name="alphabet" class="form-control" id="alphabet" type="text"
                                placeholder="<?php echo lang('alphabet'); ?>"
                                <?php if (!empty($_SESSION['registration']['alphabet'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['alphabet']; ?>" <?php
    }?> required>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="Postcode"><?php echo lang('Postcode'); ?><span
                                    class="text-danger">*</span></label>
                            <input name="postcode" class="form-control" id="postcode" type="text" placeholder="NNNN XX"
                                <?php if (!empty($_SESSION['registration']['postcode'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['postcode']; ?>" <?php
    }?> required>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="City"><?php echo lang('City'); ?><span
                                    class="text-danger">*</span></label>
                            <input name="city" class="form-control" id="city" type="text"
                                <?php if (!empty($_SESSION['registration'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['city']; ?>" <?php
    }?>
                                placeholder="<?php echo lang('City'); ?>" required>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <fieldset>
                            <label for="exampleSelect1"><?php echo lang('Country'); ?><span
                                    class="text-danger">*</span></label>
                            <select class="form-control" id="country" name="country">
                                <?php foreach (getCountries() as $key => $country) {
        ?>
                                <option value="<?php echo $key; ?>" <?php if ($setting->country_base == $key) {
            ?>
                                    selected<?php
        } ?>><?php echo $country; ?></option>
                                <?php
    }?>
                            </select>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <fieldset>
                            <label for="exampleSelect1"><?php echo lang('Languages'); ?></label>
                            <select class="form-control" id="language" name="language">
                                <?php foreach (getLanguages() as $key => $lang) {
        ?>
                                <option value="<?php echo $key; ?>" <?php if ($key == "dutch") {
            ?> selected<?php
        } ?>>
                                    <?php echo lang($lang); ?></option>
                                <?php
    }?>
                            </select>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3">
                    <fieldset>
                        <label class="control-label" for="Phonenumber"><?php echo lang('Contact Number'); ?><span
                                class="text-danger">*</span></label>
                        <input name="phonenumber" class="form-control" id="phonenumber" type="number"
                            placeholder="<?php echo lang('Contact Number'); ?>"
                            <?php if (!empty($_SESSION['registration']['phonenumber'])) {
        ?>
                            value="<?php echo $_SESSION['registration']['phonenumber']; ?>" <?php
    }?> required>
                    </fieldset>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="Email"><?php echo lang('Email'); ?><span
                                    class="text-danger">*</span></label>
                            <input name="email" class="form-control" id="email" type="email"
                                placeholder="<?php echo lang('Email address'); ?>"
                                <?php if (!empty($_SESSION['registration']['email'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['email']; ?>" <?php
    }?> required>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <fieldset>
                            <label class="control-label" for="NationalNr"><?php echo lang('NationalNr'); ?></label>
                            <input name="nationalnr" class="form-control" id="NationalNr" type="number"
                                <?php if (!empty($_SESSION['registration']['nationalnr'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['nationalnr']; ?>" <?php
    }?>
                                placeholder="<?php echo lang('NationalNr'); ?>">
                        </fieldset>
                    </div>
                </div>
            </div>

            <?php if ($setting->mobile_platform == "TEUM") {
        ?>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <fieldset>
                            <label for="id_type"><?php echo lang('Identification Type'); ?></label>
                            <select class="form-control" id="id_type" name="id_type">
                                <?php foreach (array('Passport', 'DrivingLicence', 'DNI', 'NIF', 'NIE', 'CIF', 'Visa') as $type) {
            ?>
                                <option value="<?php echo $type; ?>"><?php echo lang($type); ?></option>
                                <?php
        } ?>
                            </select>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3">
                    <fieldset>
                        <label class="control-label" for="Phonenumber"><?php echo lang('Identification Number'); ?><span
                                class="text-danger"></span></label>
                        <input name="idcard_number" class="form-control" id="idcard_number" type="text"
                            placeholder="<?php echo lang('ID Number'); ?>"
                            <?php if (!empty($_SESSION['registration']['idcard_number'])) {
            ?>
                            value="<?php echo $_SESSION['registration']['idcard_number']; ?>" <?php
        } ?>>
                    </fieldset>
                </div>
            </div>
            <?php
    } else {
        ?>

                   <?php
    } ?>

            <div class="row" <?php if (!$setting->mage_invoicing) {
        ?> style="display:none;" <?php
    } ?>>

                <div class="col-sm-3">
                    <div class="form-group">
                        <fieldset>
                            <label for="exampleSelect1">E-Invoice</label>
                            <select class="form-control" id="invoice_email" name="invoice_email">
                                <option value="yes" selected="">Yes</option>
                                <option value="yes">No</option>
                            </select>
                            <small> choose yes if wish to recieved email invoices</small>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <fieldset>
                            <label for="exampleSelect1"><?php echo lang('IBAN'); ?></label>
                            <input name="iban" class="form-control" id="iban" type="text"
                                <?php if (!empty($_SESSION['registration']['iban'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['iban']; ?>" <?php
    }?>
                                placeholder="<?php echo lang('IBAN'); ?>" required>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <fieldset>
                            <label for=""><?php echo lang('BIC'); ?></label>
                            <input class="form-control" placeholder="ASPNLXX" type="text" name="bic" id="bic"
                                <?php if (!empty($_SESSION['registration']['iban'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['iban']; ?>" <?php
    }?>>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <fieldset>
                            <label for=""><?php echo lang('Date of Birth'); ?></label>
                            <input class="form-control" placeholder="1997-01-01" type="text" name="date_birth"
                                id="pickdate" <?php if (!empty($_SESSION['registration']['date_birth'])) {
        ?>
                                value="<?php echo $_SESSION['registration']['date_birth']; ?>" <?php
    }?>>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <fieldset>
                            <label for="Paymentmethod"> <?php echo lang('Paymentmethod'); ?></label>
                            <select class="form-control" id="default_paymentmethod" name="paymentmethod">
                                <?php foreach (array('banktransfer','directdebit') as $pmt) {
        ?>
                                <option value="<?php echo $pmt; ?>"><?php echo ucfirst($pmt); ?></option>
                                <?php
    }?>
                            </select>
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <fieldset>
                            <label>Payment Terms</label>
                            <input type="text" class="form-control" name="payment_duedays" id="payment_duedays"
                                value="<?php echo $setting->payment_duedays; ?>">
                        </fieldset>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <fieldset>
                            <label for="exampleSelect1">
                                <?php echo lang('VAT Rates'); ?></label>
                            <select class="form-control" id="vat_rate" name="vat_rate">
                                <?php foreach (array('21','6','0') as $rate) {
        ?>
                                <option value="<?php echo $rate; ?>" <?php if ($setting->taxrate == $rate) {
            ?> selected
                                    <?php
        } ?>>
                                    <?php echo $rate; ?>
                                </option>
                                <?php
    } ?>
                            </select>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="row" <?php if (!$setting->mage_invoicing) {
        ?> style="display:none;" <?php
    } ?>>
                <div class="col-sm-4">
                    <div class="form-group">
                        <fieldset class="form-group">
                            <div class="form-desc">
                                <?php echo lang('VAT exemption (BTW verlegd)'); ?>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt"
                                        value="0" checked="">
                                    <?php echo lang('No, Apply tax on every invoices'); ?>
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt"
                                        value="1">
                                    <?php echo lang('Yes, do not apply tax on invoices'); ?>
                                </label>
                            </div>
                        </fieldset>
                    </div>
                </div>

            </div>
            <div class="row">

                <div class="col-sm-12">
                    <div class="form-group">
                        <button type="button" class="mr-2 mb-2 btn btn-primary btn-lg btn-block submit_btn" id="AddCustomer"><i class="fa fa-save"></i>
                            <?php echo lang('Register Clients'); ?></button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
<script>

$( document ).ready(function() {
    <?php if ($setting->mobile_platform == "TEUM") { ?>
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/client/gen_anonymous',
type: 'post',
dataType: "json",
data: {form_data:1},
success: function(data) {
Object.keys(data).forEach(function(k){
    console.log(k + ' - ' + data[k]);
     $('#'+k).val(data[k]);
});
}
});

    <?php } ?>
function GenAnn() {
    $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/client/gen_anonymous',
        type: 'post',
        dataType: "json",
        data: {
            form_data: 1
        },
        success: function(data) {
            Object.keys(data).forEach(function(k) {
                console.log(k + ' - ' + data[k]);
                $('#' + k).val(data[k]);
            });


        }
    });

}


});
</script>

<script>
$('#AddCustomer').click(function(){
    $('.submit_btn').prop('disabled', true);
    $('.submit_btn').html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
    //$( "#spin" ).removeClass( "fa fa-save" ).addClass( "fa fa-cog fa-spin" );
    var form_data = $("#formdata").serializeArray();
    $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/client/add',
        type: 'post',
        dataType: "json",
        data: form_data,
        success: function(data) {
            if (data.result) {
                window.location.href = window.location.protocol + '//' + window.location.host +
                    '/admin/client/detail/' + data.id;
            } else {
                alert("There was an error: " + data.message);
                $('.submit_btn').prop('disabled', false);
                $('.submit_btn').html('<i class="fa fa-save"></i> <?php echo lang('Register Clients'); ?>');
            }
        }
    });
});


</script>