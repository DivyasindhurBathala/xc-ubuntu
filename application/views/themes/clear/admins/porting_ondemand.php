<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Porting On Demand'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Enter confirmation Code'); ?>
        </h5>
        <div class="table-responsive">
          <form method="post" action="<?php echo base_url(); ?>admin/subscription/" id="dt">
            <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="vat">MSISDN<span class="text-danger">*</span></label>
                    <input name="msisdn" id="msisdn" class="form-control"  type="text" placeholder="31XXXXXXXXX" value="" required="">
                  </fieldset>
                </div>
              </div>
            </div>
             <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="vat">SMS Code<span class="text-danger">*</span></label>
                    <input name="code" id="code" class="form-control"  type="text" placeholder="" value="" required="">
                  </fieldset>
                </div>
              </div>
            </div>

                <div class="row">
              <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                  <button class="btn btn-block btn-primary" onclick="SaveVerificationCode();" type="button">Submit</button>
                  </fieldset>
                </div>
              </div>
            </div>


          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function SaveVerificationCode(){


  var msisdn =$('#msisdn').val();
  var code = $('#code').val();


  $.ajax({
  url: window.location.protocol + '//' + window.location.host + '/admin/subscription/porting_on_demand',
  type: 'post',
  dataType: 'json',
  data: {
    msisdn: msisdn,
    code: code
  },
  success: function(data) {
  console.log(data);


  },
  error: function(XMLHttpRequest, textStatus, errorThrown) {
  //alert("Error  accour while sending your email");
  console.log(errorThrown);

  }
  });
}
  </script>