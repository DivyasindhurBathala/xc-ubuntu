<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
				<?php echo lang('Subscriptions'); ?><div class=float-right>
					<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/subscription/add_mobile"><i
							class="fa fa-plus-circle"></i>
						<?php echo lang('Add Subscription'); ?><?php //echo lang('Add Subscription'); ?></a>
					<?php if ($this->session->role == "superadmin") {?>
					<a class="btn btn-primary btn-sm"
						href="<?php echo base_url(); ?>admin/subscription/export_subscription"><i
							class="fa fa-file-excel"></i>
						<?php echo lang('Export Subscription'); ?><?php //echo lang('Add Subscription'); ?></a>
					<?php }?>
				</div>
			</h6>
			<div class="element-box">
				<h5 class="form-header">
					<?php echo lang('Subscription List'); ?>
				</h5>
				<?php //if(strpos($this->session->email, 'united-telecom.be')){ ?>
				<div class="row">
					<div class="col-md-6">
						<div id="columnchart_material" style="width: 100%; height: 200px;"></div>
					</div>
					<div class="col-md-6">
						<div id="piechart" style="width: 100%; height: 200px;"></div>
					</div>
				</div>
				<?php //} ?>
				<div class="table-responsive">
        <div class="row">
            <div class="col-sm-3">
              <a class="element-box el-tablo" href="#">
                <div class="label">
                    <?php echo lang('Total'); ?>
                    <?php echo lang('Active Subscriptions'); ?>
                </div>
                <div class="value">
                    <?php echo $billing_stats['active']; ?>
                </div>
                <div class="trending trending-up-basic">
                  <span></span><i class="os-icon os-icon-arrow-up2"></i>
                </div>
              </a>
            </div>

            <div class="col-sm-3">
              <a class="element-box el-tablo" href="#">
                <div class="label">
                    <?php echo lang('Total'); ?>
                    <?php echo lang('Suspended Subscriptions'); ?>
                </div>
                <div class="value">
                <?php echo $billing_stats['suspended']; ?>
                </div>
                <div class="trending trending-up-basic">
                  <span></span><i class="os-icon os-icon-arrow-up2"></i>
                </div>
              </a>
            </div>

            <div class="col-sm-3">
              <a class="element-box el-tablo" href="#">
                <div class="label">
                    <?php echo lang('Total'); ?>
                    <?php echo lang('Pending Subscriptions'); ?>
                </div>
                <div class="value">
                <?php echo $billing_stats['pending']; ?>
                </div>
                <div class="trending trending-up-basic">
                  <span></span><i class="os-icon os-icon-arrow-up2"></i>
                </div>
              </a>
            </div>

            <div class="col-sm-3">
              <a class="element-box el-tablo" href="#">
                <div class="label">
                    <?php echo lang('Total'); ?>
                    <?php echo lang('Cancelled Subscriptions'); ?>
                </div>
                <div class="value">
                <?php echo $billing_stats['cancelled']; ?>
                </div>
                <div class="trending trending-up-basic">
                  <span></span><i class="os-icon os-icon-arrow-up2"></i>
                </div>
              </a>
            </div>
            </div>
					<table class="table table-striped table-lightfont" id="services">
						<thead>
							<tr>
								<th><?php echo lang('ID#'); ?></th>
								<th><?php echo lang('Package'); ?></th>
								<th><?php echo lang('Customer'); ?></th>
								<th><?php echo lang('Provisioning Status'); ?></th>
								<th><?php echo lang('Recurring'); ?></th>
								<th><?php echo lang('Identity'); ?></th>
								<th><?php echo lang('Date Contract'); ?></th>
                <th><?php echo lang('Billing Status'); ?></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
							<tr>
								<th><?php echo lang('ID'); ?></th>
								<th><?php echo lang('Package'); ?></th>
								<th><?php echo lang('Customer'); ?></th>
								<th><?php echo lang('Provisioning Status'); ?></th>
								<th><?php echo lang('Recurring'); ?></th>
								<th><?php echo lang('Identity'); ?></th>
								<th><?php echo lang('Date Contract'); ?></th>
                <th><?php echo lang('Billing Status'); ?></th>
							</tr>
						</tfoot>
					</table>
				</div>




			</div>
		</div>
	</div>
</div>

<?php if ($stats) { ?>
<script type="text/javascript">
google.charts.load('current', {
	'packages': ['bar']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
	var data = google.visualization.arrayToDataTable([ <?php echo implode(',', $stats); ?> ]);
	var options = {
		chart: {
			title: 'New Orders'
		}
	};
	var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
	chart.draw(data, google.charts.Bar.convertOptions(options));
}
</script>
<?php } ?>

<?php if ($chartStatus) { ?>
<script type="text/javascript">
google.charts.load('current', {
	'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

	var data = google.visualization.arrayToDataTable([ <?php echo implode(',', $chartStatus); ?> ]);

	var options = {
		title: 'Status Activities'
	};

	var chart = new google.visualization.PieChart(document.getElementById('piechart'));

	chart.draw(data, options);
}
</script>
<?php } ?>

<!-- The Suspend Modal -->
<div class="modal fade" id="NewModal">

	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="<?php echo base_url(); ?>admin/subscription/add">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Choose Type'); ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<a class="btn btn-lg btn-block btn-md btn-secondary"
								href="<?php echo base_url(); ?>admin/subscription/add_internet"><i
									class="fa fa-laptop fa-5x"></i><br />Internet</a>
						</div>
						<div class="col-md-6">
							<a class="btn btn-lg btn-block btn-md btn-primary"
								href="<?php echo base_url(); ?>admin/subscription/add_mobile"><i
									class="fa fa-mobile fa-5x"></i><br />Mobile</a>
						</div>

					</div>
				</div>
				<div class="modal-footer">

				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="ImportWhmcs">

	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" id="importxdsltable">
				<div class="modal-header">
					<h5 class="modal-title" id="import_title"></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div id="loader" style="display:none;">
						<center>
							<img height="150" src="<?php echo base_url(); ?>assets/img/loader1.gif">
						</center>
					</div>
					<div id="resultid">
					</div>
					<div id="bodyid">
						<div class="form-group">
							<label for="exampleInputEmail1"><?php echo lang('WHMCS Serviceid'); ?></label>
							<input type="number" class="form-control" name="whmcsid" id="serviceidx"
								aria-describedby="serviceid" placeholder="123456">
							<small id="emailHelp"
								class="form-text text-muted"><?php echo lang('Make sure the ID on the URL Bar'); ?>.</small>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1"><?php echo lang('EDPNET CID'); ?></label>
							<input type="number" name="circuitid" class="form-control" id="circuitid"
								aria-describedby="circuitid" placeholder="1023333533456">
							<small id="emailHelp"
								class="form-text text-muted"><?php echo lang('Enter EDPNET Circuitid (optional)'); ?>.</small>
						</div>
					</div>
				</div>
				<div class="modal-footer">

					<button class="btn btn-md btn-primary" id="importnow"
						type="button"><?php echo lang('Import Now'); ?></button>
				</div>
				<input type="hidden" name="serviceid" value="" id="mvnoserviceid">
				<input type="hidden" name="userid" value="" id="userid">
			</form>
		</div>
	</div>
</div>


<script>
$(document).ready(function() {
	$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
		<?php
		if (!empty($this->uri-> segment(4))) {
			?>
			var status = '<?php echo $this->uri->segment(4); ?>'; <?php
		} else {
			?>
			var status = ''; <?php
		} ?>
		$('#services').DataTable({
			"initComplete": function() {
				this.api().columns().eq(0).each(function(index) {
					const column = this.column(index);
					const title = $(column.header()).text();
					if (index === 1 || index === 6 || index === 3) {
						var select = $(`
                    <select class="form-control">
                        <option value="">Please choose</option>
                    </select>
                `)
							.appendTo($(column.footer()).empty())
							.on('change', function() {
								var val = $.fn.dataTable.util.escapeRegex($(this).val());
								column
									.search(val ? '^' + val + '$' : '', true, false)
									.draw();
							});
						column.data().unique().sort().each(function(d, j) {
							select.append('<option value="' + d + '">' + d + '</option>')
						});
					} else {
						var input = $(`
                    <input class="form-control" type="text" placeholder="Search ${title}" />
                `)
							.appendTo($(column.footer()).empty())
							.on('keyup change', function() {
								var val = $.fn.dataTable.util.escapeRegex($(this).val());
								column
									.search(val)
									.draw();
							});
					}
				});
			},
			"autoWidth": false,
			"processing": true,
			"orderCellsTop": true,
			"ordering": true,
			"serverSide": false,
			"pageLength": 10,
			"ajax": window.location.protocol + '//' + window.location.host +
				'/admin/table/getservices/' + status,
			"aaSorting": [
				[0, 'desc']
			],

			"language": {
				"url": window.location.protocol + '//' + window.location.host +
					"/assets/clear/js/datatables/lang/" + data.result + ".json"
			},
			"fnRowCallback": function(nRow, aData, iDisplayIndex) {
				if (aData[3] == "Terminated") {

					var color = "text-danger";
				} else if (aData[3] == "Active") {

					var color = "text-dark";
				} else if (aData[3] == "ActivationRequested") {
					var color = "text-primary";
				} else if (aData[3] == "PortinRejected") {

					var color = "text-danger";
				} else if (aData[3] == "PortOutRequest") {

					var color = "text-dark";
				} else if (aData[3] == "OrderWaiting") {
					var color = "text-info";

				} else if (aData[3] == "PortinCancelled") {

					var color = "text-danger";
				} else if (aData[3] == "PortinPending") {
					var color = "text-primary";

				}
				$('td:eq(3)', nRow).html('<span class="' + color + '">' + aData[3] + '</span>');

				$('td:eq(4)', nRow).html(data.currency + aData[4]);
				$('td:eq(2)', nRow).html('<a class="showprogress ' + color + '" href="' + window
					.location.protocol + '//' + window.location.host + '/admin/client/detail/' +
					aData[9] + '">' + aData[2] + '</a>');
				$('td:eq(0)', nRow).html('<a class="showprogress ' + color + '" href="' + window
					.location.protocol + '//' + window.location.host +
					'/admin/subscription/detail/' + aData[11] + '">' + aData[0] + '</a>');
				$('td:eq(1)', nRow).html('<a class="showprogress ' + color + '" href="' + window
					.location.protocol + '//' + window.location.host +
					'/admin/subscription/detail/' + aData[11] + '">' + aData[1] + '</a>');
				if (aData[5] == "") {
					if (aData[10] == "2") {
						$('td:eq(5)', nRow).html(
							'<button  class="btn btn-primary btn-xs showprogress" onclick="importxDSL(' +
							aData[0] + ')">Import from WHMCS</button>');
					}
				} else {
					$('td:eq(5)', nRow).html('<a  class="showprogress ' + color + '" href="' + window
						.location.protocol + '//' + window.location.host +
						'/admin/subscription/detail/' + aData[11] + '">' + aData[5] + '</a>');
				}
				$(nRow).addClass(color);
				return nRow;
			},
		});
	});
});
</script>
<script>
function importxDSL(serviceid) {
	$('#import_title').html('Importing #' + serviceid);
	$('#mvnoserviceid').val(serviceid);
	$('#ImportWhmcs').modal('toggle');
}

function Processorder() {
	$.ajax({
		url: '<?php echo base_url(); ?>admin/super/processImport',
		type: 'post',
		dataType: 'json',
		data: $('#importxdsltable').serialize(),
		success: function(data) {
			if (data.result == "success") {
				window.location.href = window.location.protocol + '//' + window.location.host +
					'/admin/subscription/';

			} else {

				window.location.href = window.location.protocol + '//' + window.location.host +
					'/admin/subscription/';
			}
		}

	});


}

$('#importnow').click(function() {
	$('#importnow').prop('disabled', true);
	$('#bodyid').hide();
	$('#loader').show('slow');
	var whmcsid = $('#serviceidx').val();
	var serviceid = $('#mvnoserviceid').val();
	var edpnetcid = $('#circuitid').val();
	$.ajax({
		url: '<?php echo base_url(); ?>admin/super/getWhmcsOrder',
		type: 'post',
		dataType: 'json',
		data: {
			'serviceid': whmcsid,
			'localid': serviceid,
			'circuitid': edpnetcid
		},
		success: function(data) {
			console.log(data);
			$('#userid').val(data.userid);
			$('#circuitid').val(data.xdslCircuitID);
			if (data.result == "success") {
				$('#loader').hide('slow');
				$('#importnow').prop('disabled', true);
				$('#resultid').html(
					'<span class="text-success"><strong>Congratulation we found these information: #' +
					whmcsid +
					'</strong></span><table class="table table-border table-hover"><tr><td>Address</td><td>' +
					data.oaStreet + ' ' + data.oaHouseNumber + ' ' + data.oaHouseNumberAlfa + ' ' + data
					.oaZipCode + ' ' + data.oaCity + '</td></tr> <tr><td>Circuitid</td><td>' + data
					.xdslCircuitID + '</td></tr><tr><td>PPPOE USER</td><td>' + data.dsllogin +
					'</td></tr><tr><td>PPPOE PASS</td><td>' + data.dslpass +
					'</td></tr> <tr><td>REALM</td><td>' + data.realm +
					'</td></tr><tr><td>CircuitID EDPNET</td><td>' + edpnetcid +
					'</td></tr> </table><br /><br /><button class="btn btn-primary btn-sm" type="button" onclick="Processorder();">Confirm Import</button> <input type="hidden" name="street"  id="street" value="' +
					data.oaStreet + '"> <input type="hidden" name="number" id="number" value="' + data
					.oaHouseNumber + '"> <input type="hidden" name="alpha" id="alpha" value="' + data
					.oaHouseNumberAlfa + '"> <input type="hidden" name="city" id="city" value="' + data
					.oaCity + '"> <input type="hidden" name="postcode" id="postcode" value="' + data
					.oaZipCode + '">  <input type="hidden" name="username" id="username" value="' + data
					.dsllogin + '"> <input type="hidden" name="password" id="password" value="' + data
					.dslpass + '"><input type="hidden" name="realm" id="realm" value="' + data.realm + '">'
					);
			} else {
				$('#importnow').prop('disabled', false);
				$('#bodyid').show();
				$('#loader').hide('slow');
				$('#resultid').html('<span class="text-danger">Sorry we could not found order id: #' +
					whmcsid + '</span>');


			}

		}

	});
});
</script>