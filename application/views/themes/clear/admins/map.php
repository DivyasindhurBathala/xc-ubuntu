<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Client Mapping'); ?> 
      
      </h6>
      <div class="element-box">
        <h5 class="form-header">
         
        </h5>
         <div class="table-responsive">
        <div id="map" style="width: 100%; height: 500px;"></div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
    var locations = [<?php echo implode(',', $maps); ?>];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(-33.92, 151.25),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>

