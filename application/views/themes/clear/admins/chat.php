<script>
  $( document ).ready(function() {
    $("#loading").hide("slow");
});
</script>
<div class="content-i">
            <div class="content-box">
              <div class="full-chat-w">
                <div class="full-chat-i">
                  <div class="full-chat-left">
                    <div class="os-tabs-w">
                      <ul class="nav nav-tabs upper centered">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="tab" href="#tab_overview"><i class="os-icon os-icon-mail-14"></i><span>Chats</span></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#tab_sales"><i class="os-icon os-icon-ui-93"></i><span>Contacts</span></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#tab_sales"><i class="os-icon os-icon-ui-02"></i><span>Favorites</span></a>
                        </li>
                      </ul>
                    </div>
                    <div class="chat-search">
                      <div class="element-search">
                        <input placeholder="Search users by name..." type="text">
                      </div>
                    </div>
                    <div class="user-list">
                    	<?php foreach (getAdmins() as $stf) {?>
                      <?php if ($stf->id != $this->session->userdata('id')) {?>
                      <div class="user-w" onclick="javascript:chatWith('<?php echo $stf->id; ?>','<?php echo $stf->name; ?>' )">
                        <div class="avatar with-status status-green">
                          <img alt="" src="<?php echo base_url(); ?>assets/img/staf/<?php echo $stf->picture; ?>">
                        </div>
                        <div class="user-info">
                          <div class="user-date">
                            12 min
                          </div>
                          <div class="user-name">
                            <?php echo $stf->name; ?>
                          </div>
                          <div class="last-message">
                           <i class="os-icon os-icon-phone-15"></i> <?php echo $stf->phonenumber; ?>
                          </div>
                        </div>
                      </div>
                 <?php }?>
                       <?php }?>
                    </div>
                  </div>
                  <div class="full-chat-middle">
                    <div class="chat-head">
                      <div class="user-info">
                        <span class="penerima"></span>
                      </div>
                     <!-- <div class="user-actions">
                        <a href="#"><i class="os-icon os-icon-mail-07"></i></a><a href="#"><i class="os-icon os-icon-phone-18"></i></a><a href="#"><i class="os-icon os-icon-phone-15"></i></a>
                      </div>
                    -->
                    </div>
                    <div class="chat-content-w">
                      <div class="chat-content" id="scroll">
                        <center><img id="loading" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif"></center>

                      </div>
                    </div>
                    <div class="chat-controls">
                      <form>
                        <input type="hidden" id="chatboxtitle" value="">
                      </form>
                      <div class="chat-input">
                        <input placeholder="Type your message here..." type="text" class="chatboxtextarea" onkeydown="checkChatBoxInputKey(event,this);">
                      </div>
                      <div class="chat-input-extra">
                        <div class="chat-extra-actions">

                        </div>
                        <div class="chat-btn">
                          <a class="btn btn-primary btn-sm" href="#">Reply</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="full-chat-right">
                    <div class="user-intro">
                      <div class="avatar">
                        <img alt="" src="<?php echo base_url(); ?>assets/img/staf/<?php echo $this->session->userdata('picture'); ?>">
                      </div>
                      <div class="user-intro-info">
                        <h5 class="user-name">
                         <?php echo $this->session->userdata('firstname'); ?> <?php echo $this->session->userdata('lastname'); ?>
                        </h5>
                        <div class="user-sub">
                         <?php echo $this->session->userdata('city'); ?>
                        </div>
                        <div class="user-social">
                         <?php echo $this->session->userdata('email'); ?><br />
                         <?php echo $this->session->userdata('phonenumber'); ?>
                        </div>
                      </div>
                    </div>
                    <div class="chat-info-section">
                      <div class="ci-header">
                        <i class="os-icon os-icon-documents-03"></i><span>Shared Files</span>
                      </div>
                      <div class="ci-content">
                        <div class="ci-file-list">
                          <ul>
                            <li>
                              <a href="#">Annual Revenue.pdf</a>
                            </li>
                            <li>
                              <a href="#">Expenses.xls</a>
                            </li>
                            <li>
                              <a href="#">Business Plan.doc</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="chat-info-section">
                      <div class="ci-header">
                        <i class="os-icon os-icon-documents-07"></i><span>Shared Photos</span>
                      </div>
                      <div class="ci-content">
                        <div class="ci-photos-list">
                          <img alt="" src="<?php echo base_url(); ?>assets/clear/img/portfolio9.jpg"><img alt="" src="<?php echo base_url(); ?>assets/clear/img/portfolio2.jpg"><img alt="" src="<?php echo base_url(); ?>assets/clear/img/portfolio12.jpg"><img alt="" src="<?php echo base_url(); ?>assets/clear/img/portfolio14.jpg">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
<!--
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages ps ps--theme_default" data-ps-id="1bc71ad8-99de-ce98-63cb-b8a587ba5cef">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;"><div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__scrollbar-y-rail" style="top: 0px; right: 0px;"><div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>

            -->
              <!--------------------
              END - Chat Popup Box
              -------------------->
            </div>
          </div>


