<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Bank File List'); ?> 
       <div class="close">
       <button class="btn btn-md btn-primary" id="upload" data-toggle="modal" data-target="#Upload"><i class="fa fa-upload"></i> Upload New Directdebit File</button>
       </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
         <?php echo lang('Daily Files'); ?>
        </h5>
         <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="sepa">
            <thead>
              <tr>
                <th><?php echo lang('Date'); ?></th>
                <th><?php echo lang('Filename'); ?></th>
                <th><?php echo lang('Uploader'); ?></th>
                <th><?php echo lang('BankIndexId'); ?></th>
               
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#sepa').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_sepa_directdebit/<?php echo $this->session->cid; ?>',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
//"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
//$('td:eq(0)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/sepa/get_transaction/' + aData[4] + '">'+aData[0]+'</a>');
//$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/sepa/get_/' + aData[4] + '">'+aData[1]+'</a>');
//return nRow;
//},
});

});
});

</script>
<div class="modal fade" id="Upload" tabindex="-1" role="dialog" aria-labelledby="Upload" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="<?php echo base_url(); ?>admin/sepa/upload_sepa_directdebit" enctype="multipart/form-data">
         
          <input type="hidden" name="uploaded" value="<?php echo $this->session->firstname.' '.$this->session->lastname; ?>">
      
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Sepa Amendment'); ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           
          <div class="form-group">
              <label for="exampleTextarea"> <?php echo lang('Company'); ?>:</label>
              <select name='companyid' class="form-control">
              <?php foreach(getCompanieMvno() as $row){ ?>
                <?php if($row->companyid == $this->session->cid){ ?>
              <option value="<?php echo $row->companyid; ?>"><?php echo $row->companyname; ?></option>
              <?php } ?>
               <?php } ?>

              </select>
            </div>

              <div class="form-group">
              <label for="exampleTextarea"> <?php echo lang('IbankFileIndex'); ?>(optional):</label>
              <input class="form-control" name="iBankIndex" value="" required>
            </div>

         <div class="form-group">
      <label for="exampleInputFile">XML File</label>
      <input type="file" class="form-control-file" name="file" id="exampleInputFile" aria-describedby="fileHelp">
      <small id="fileHelp" class="form-text text-muted">Make sure that you have validate the file before uploading.</small>
    </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal"> <?php echo lang('Close'); ?></button>
            <button type="submit" class="btn btn-primary"> <?php echo lang('Save changes'); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>
