<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Invoices'); ?>
            <div class="close">
                <!--
                <button class="btn btn-primary btn-rounded" data-target=".bd-exportpdf-modal-lg" data-toggle="modal"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>  <?php echo lang('Export Invoices'); ?> (pdf) </button>
                <button class="btn btn-rounded btn-success" data-target=".bd-export-modal-lg" data-toggle="modal"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                <?php echo lang('Export Invoices (excel)'); ?> </button>
                -->
                <div class="float-right">
                <?php if (in_array($this->session->role, array("superadmin","finance"))) {
    ?>
                   <!--  <button class="btn btn-rounded btn-secondary" onclick="export_invoices()"><i class="fa fa-plus-circle"></i>  <?php echo lang('Export Invoices'); ?></button>  -->
                    <button data-toggle="modal" data-target="#ExportInvoice" class="btn  btn-rounded btn-secondary"><i class="fa fa-plus-circle"></i>  <?php echo lang('Export Invoices'); ?></button>
                     <a href="<?php echo base_url(); ?>admin/invoice/overdue" class="btn btn-rounded btn-secondary"><i class="fa fa-plus-circle"></i>  <?php echo lang('Show Invoice Overdue'); ?></a> 
                <?php
} ?>
              
                 </div>
            </div>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                <!-- <?php echo lang('List Invoices'); ?> -->
                <pre>
<?php //print_r($stats);?>
</pre>
                </h5>
                <div id="columnchart_material" style="width: 100%; height: 200px;"></div>
                <div class="table-responsive">

                    <table class="table table-striped table-lightfont table-hover" id="invoices">
                         <tfoot>
            <tr>
                <th>Invoice Number</th>
                <th>Billing ID</th>
                <th>Invoice Date</th>
                <th>Invoice Duedate</th>
                <th>Amount</th>
                 <th>Status</th>
            </tr>
        </tfoot>
                        <thead>
                            <th> <div align="left"><?php echo lang('Invoice Number'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Billing ID'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Invoice Date'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Invoice Duedate'); ?> </div></th>
                            <th> <div align="right"><?php echo lang('Amount'); ?> </div></th>
                            <th> <div align="right"><?php echo lang('Status'); ?> </div></th>
                        </thead>

                    </table>
                </div>
            </div>

        </div>
    </div>
</div>




<script>
  $(document).ready(function()
{
  <?php if ($this->session->cid != 2) {
        ?>
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#invoices').DataTable({

   "initComplete": function () {
        this.api().columns().eq(0).each( function (index) {
            const column = this.column(index);
            const title = $(column.header()).text();
            if(index === 2 || index === 3 || index === 5){
                var select = $(`
                    <select class="form-control">
                        <option value="">Please choose</option>
                    </select>
                `)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                });
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                });
            }else{
                var input = $(`
                    <input class="form-control" type="text" placeholder="Search ${title}" />
                `)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'keyup change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val )
                        .draw();
                });
            }
        });
    },
   "autoWidth": false,
    "ajax": {
    "url": window.location.protocol + '//' + window.location.host + '/admin/table/getinvoices/<?php echo $this->session->cid; ?>',
    "dataSrc": ""
      },
    "aaSorting": [[0, 'desc']],
    "columns": [
            { "data": "iInvoiceNbr" },
            { "data": "cName" },
            { "data": "dInvoiceDate" },
            { "data": "dInvoiceDueDate" },
            { "data": "mInvoiceAmount" },
            { "data": "iInvoiceStatus" }
        ],
    "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

      $('td:eq(4)', nRow).html(data.currency+ aData.mInvoiceAmount);
      $('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/get_mageboid/' + aData.iAddressNbr + '">' + aData.cName + '</a>');
      $('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData.iInvoiceNbr + '/'+aData.iAddressNbr+'">' + aData.iInvoiceNbr + '</a>');
      //$('td:eq(3)', nRow).html(aData.dInvoiceDueDate.slice(0, 10));
      //$('td:eq(2)', nRow).html(aData.dInvoiceDate.slice(0, 10));
        if(aData.iInvoiceStatus == "Paid"){

            $('td:eq(5)', nRow).html('<strong class="text-success">'+aData.iInvoiceStatus+'</strong>');
        }else if(aData.iInvoiceStatus == "Unpaid"){
            $('td:eq(5)', nRow).html('<strong class="text-danger">'+aData.iInvoiceStatus+'</strong>');
        }else{
            $('td:eq(5)', nRow).html('<strong class="text-warning">'+aData.iInvoiceStatus+'</strong>');
        }
              return nRow;
        },
  });
});
<?php
    } else {
        ?>

$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
  $('#invoices').DataTable({
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_invoices/<?php echo $this->session->cid; ?>',
            "aaSorting": [
                [0, 'desc']
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
              $('td:eq(4)', nRow).html(data.currency+ aData[4]);
                //$('td:eq(5)', nRow).html(data.currency + aData[5]);
                //$('td:eq(2)', nRow).html('<a href="' + window.location.protocol + '//' + window.location.host + '/admin/subscription/download_welcome_letter/' + aData[1] + '"><i class="fa fa-download"></i> Download</a>');
                return nRow;
            },
        });
      });
<?php
    } ?>
    //set initial state.
    //$('#checkbox1').val($(this).is(':checked'));

    $('#checkbox1').change(function() {
        if($(this).is(":checked")) {

              $('#pickdate13x1').val('2018-01-01');
              $('#pickdate13x2').val('<?php echo date('Y-m-d'); ?>');
           
        }else{
            $('#pickdate13x1').val();
            $('#pickdate13x2').val();
        }
            
    });



});



</script>

<script>

function export_invoices(){
window.location.href = window.location.protocol + '//' + window.location.host +'/admin/invoice/export_invoices';   
}
</script>

  <?php if ($stats) {
        ?>
      <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([<?php echo implode(',', $stats); ?>]);

        var options = {
          chart: {
            title: 'Invoices Statistic'
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
  <?php
    } ?>

<div class="modal fade" id="ExportInvoice" tabindex="-1" role="dialog" aria-labelledby="ExportInvoice" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/invoice/export_invoices">
        <input type="hidden" name="agentid" value="<?php echo $agent->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Export Invoice'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="form-check col-sm-12">
            <label class="form-check-label">
            <input type="checkbox" id="checkbox1" class="form-check-input" value="">All
             </label>
            </div>

            <div class="form-group col-sm-12">
              <label for="comission_type">From date:</label>
            
              <input type="text" name="date_start" class="form-control" id="pickdate13x1" readonly required>

            </div>
            <div class="form-group col-sm-12">
              <label for="comission_type">To date:</label>
            
              <input type="text" name="date_end" class="form-control" id="pickdate13x2" readonly required>
              
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Submit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>


