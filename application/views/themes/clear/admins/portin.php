<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Mobile Subscription Requests'); ?> 
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Pending Port In Requests'); ?> : <i>Last Updated: <?php echo $last_updated; ?></i>
        <div class="float-right">
        <a href="<?php echo base_url(); ?>admin/subscription/pendingporting" class="btn btn-xs btn-primary">All</a>
        <a href="<?php echo base_url(); ?>admin/subscription/pendingporting/Rejected" class="btn btn-xs btn-primary">Rejected</a>
        <a href="<?php echo base_url(); ?>admin/subscription/pendingporting/Accepted" class="btn btn-xs btn-primary">Accepted</a>
        <a href="<?php echo base_url(); ?>admin/subscription/pendingporting/Pending" class="btn btn-xs btn-primary">Pending</a>
        <a href="<?php echo base_url(); ?>admin/subscription/pendingporting/Fails" class="btn btn-xs btn-primary">Fails</a>
        <button type="button" class="btn btn-xs btn-primary" onclick="ExportList()">Export</button>
        </div>
        </h5>
        <div class="table-responsive">
          <div class="row">
            <div class="col-md-12">

              <table class="table table-striped table-bordered" id="cdr">
                <thead>
                  <tr class="active">
                
                    <th><?php echo lang('MSISDN'); ?></th>
                    <th><?php echo lang('RegDate'); ?></th>
                    <th><?php echo lang('ActionDate'); ?></th>
                    <th><?php echo lang('CustomerName'); ?></th>
                    <th><?php echo lang('Status'); ?></th>
                    <th><?php echo lang('Remark'); ?></th>
                    <th><?php echo lang('button'); ?></th>
                  </tr>
                </thead>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
function OpenAccept(msisdn, sn){
$("#numberaccept").html('<?php echo lang('Accept'); ?> :'+msisdn);
$("#accept_number").val(msisdn);
$("#accept_sn").val(sn);
$('#PortExecute').modal({
show: false
});
$('#PortReject').modal({
show: false
});
$('#PortAccept').modal({
show: true
});
$('#PortCancel').modal({
show: false
});
}
function openNotes(sn){
$.ajax({
url: '<?php echo base_url(); ?>admin/subscription/getPortinPending_Notes',
type: 'post',
dataType: 'json',
success: function (data) {
 $('#OpenNoteme').modal('toggle');
 $('#tbnotes').html(data.html);
},
data: {'msisdn':sn}
});
}
function addNotes(sn){
  $('#xsn').val(sn);
  $('#addNotes').modal('toggle');

}

function submitAddnoteForm(){

  var notes= $('#notes').val();
  var agent= $('#agent').val();
  var msisdn= $('#xsn').val();
$.ajax({
url: '<?php echo base_url(); ?>admin/subscription/addPortinPending_Notes/'+msisdn,
type: 'post',
dataType: 'json',
success: function (data) {
if(data.result){
 $('#addNotes').modal('toggle');
}else{
  alert('Error while adding notes');
}

},
data: { 'description': notes, 'agent': agent,'msisdn':msisdn }
});


}


function ExportList(){

$("#exportlist").prop('disabled', true);
$.ajax({
url: '<?php echo base_url(); ?>admin/subscription/export_pending_portin',
type: 'post',
dataType: 'json',
success: function (data) {
if(data.result){
  alert('Your request has been queued, the list will be sent to <?php echo $this->session->email; ?>');
}else{
  alert('There is already request for you in the queue to be sent to <?php echo $this->session->email; ?>');
}

},
data: {'msisdn':'tptp', 'type': 'option'}
});


}


function OpenPod(msisdn){
  window.location.href= '<?php echo base_url(); ?>client/portinondemand/'+msisdn;

}
function OpenReject(msisdn){
$("#numberreject").html('<?php echo lang('Reject'); ?> :'+msisdn);
$("#reject_number").val(msisdn);
$('#PortAccept').modal({
show: false
});
$('#PortExecute').modal({
show: false
});
$('#PortReject').modal({
show: true
});
$('#PortCancel').modal({
show: false
});
}


function OpenCancel(sn){
  console.log(sn);
$("#number_cancel").html('<?php echo lang('Cancel MSISDN:'); ?> :'+sn);
$("#cancel_number").val(sn);
$('#PortAccept').modal({
show: false
});
$('#PortExecute').modal({
show: false
});
$('#PortReject').modal({
show: false
});
$('#PortCancel').modal({
show: true
});
}


function OpenExecute(msisdn){


window.location.href= '<?php echo base_url(); ?>admin/subscription/open_reject/'+msisdn;

}
</script>



<div class="modal fade" id="PortAccept">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/accept_portout/">
        <input type="hidden" name="msisdn" id="accept_number" value="">
        <input type="hidden" name="SN" id="accept_sn" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="numberaccept"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
            <?php echo lang('Do you wish to accept this port?'); ?>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Accept'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="PortReject">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/reject_portout/">
        <input type="hidden" name="msisdn" id="reject_number" value="">
        <input type="hidden" name="SN" id="reject_sn" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="numberreject"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger"><?php echo lang('Reject'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="OpenNoteme">
  <div class="modal-dialog modal-lg">

    <div class="modal-content">
         <div class="modal-header">
          <h4 class="modal-title">Notes</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
          <div class="modal-body">
     <table class="table table-striped">
      <thead>
        <tr>
      <th><?php echo lang('Date'); ?></th>
      <th><?php echo lang('Agent'); ?></th>
      <th><?php echo lang('Notes'); ?></th>
    </tr>
    </thead>
    <tbody id="tbnotes">
    </tbody>
     </table>
   </div>
    </div>
  </div>
</div>
<div class="modal fade" id="PortExecute">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/execute_portin/">
        <input type="hidden" name="msisdn" id="execute_number" value="">
        <input type="hidden" name="SN" id="execute_sn" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="numberexecute"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger"><?php echo lang('Execute'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="PortCancel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/cancel_portin/">

        <input type="hidden" name="SN" id="cancel_number" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="number_cancel"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
            <?php echo lang('Do you wish to cancel this porting request?'); ?>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Cancel Porting Request'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#cdr').DataTable({
"autoWidth": false,
"ajax": {
"url": window.location.protocol + '//' + window.location.host + '/admin/subscription/get_pending/<?php echo $this->uri->segment(4); ?>',
"dataSrc": ""
},
"aaSorting": [[0, 'desc']],
"pageLength": 50,
"columns": [
              { mData: 'MSISDN' },
              { mData: 'EnteredDate' },
              { mData: 'ActionDate' },
              { mData: 'CustomerName' },
              { mData: 'Status' },
              { mData: 'Remark' },
              { mData: 'button' }
],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
  "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

     $('td:eq(0)', nRow).html(aData.MSISDN+' <button  onclick="addNotes('+aData.MSISDN+');"><i class="fa fa-plus-circle"></i></button> '+aData.InfoNote);
 
    return nRow;
        },
});
});



});
</script>




<div class="modal fade" id="addNotes">
  <div class="modal-dialog">

    <div class="modal-content">
         <div class="modal-header">
          <h4 class="modal-title"><div id="numberaccept"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
          <div class="modal-body">
     <div class="table-responsive">
        <form id="addNoteform">
      <div class="form-group">
  <label for="exampleFormControlTextarea1">Notes</label>
  <textarea class="form-control rounded-0" id="notes" rows="6"></textarea>
</div>
  <input id="xsn" type="hidden" value="">
    <input id="agent" type="hidden" value="<?php echo $this->session->firstname.' '.$this->session->lastname; ?>">
  <button class="btn btn-primary" type="button" onclick="submitAddnoteForm()">Submit</button>
          </form>


     </div>
   </div>
    </div>
  </div>
</div>
 
