<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Services
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				Create Subscription <div class="close">
				</div>
				</h5>
				<div class="table-responsive">
					<form method="post"  action="<?php echo base_url(); ?>admin/subscription/add">
						<fieldset>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<label for="exampleInputEmail1">Customer</label>
										<input name="userid" type="text" class="form-control ui-autocomplete-input ui-autocomplete-loading" id="customerid" aria-describedby="search" placeholder="Serach..." required>
										<small id="emailHelp" class="form-text text-muted">Please search customer by firstname, lastname, companyname or id</small>
									</div>
									<div class="col-sm-6">
										<label for="exampleInputEmail1">Customer Name</label>
										<input type="text" class="form-control" id="customername" aria-describedby="search">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-4">
										<label for="exampleSelect1">Choose Product Group</label>
										<select class="form-control" id="groupid" name="gid">
											<option value="0" disabled selected>Select Group...</option>
											<?php foreach (getProductList() as $product) {?>
											<option value="<?php echo $product['id']; ?>"><?php echo $product['name']; ?></option>
											<?php }?>
										</select>
									</div>
									<div class="col-sm-4">
										<label for="exampleSelect1">Choose packageid</label>
										<select class="form-control" id="packageids" name="packageid">
											<option value="0" disabled selected>Choose Product</option>
										</select>
									</div>
									<div class="col-sm-4">
										<label for="exampleSelect1">Choose Billing Cycle</label>
										<select class="form-control" id="exampleSelect1" name="cycle">
											<option value="monthly">Monthly</option>
											<option value="quarterly">Quarterly</option>
											<option value="semiannually">Semi-Annually</option>
											<option value="annually">Annually</option>
											<option value="biennially">Bienually</option>
											<option value="triennially">Trienually</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<label for="exampleInputEmail1">Override Pricing</label>
										<input name="recurring" type="number" step="any" class="form-control" id="recurring" aria-describedby="search" placeholder="10.2">
										<small id="emailHelp" class="form-text text-muted">This will be the pricing recurred to customer</small>
									</div>
									<div class="col-sm-6">
										<label for="exampleInputEmail1">Status</label>
										<select class="form-control" id="exampleSelect1" name="status">
											<option value="Active">Active</option>
											<option value="Pending">Pending</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-check">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="optionsRadios" id="no" value="no" checked="">
											No
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="optionsRadios" id="internet" value="internet">
											Option Internet
										</label>
									</div>
									<div class="form-check">
										<label class="form-check-label">
											<input type="radio" class="form-check-input" name="optionsRadios" id="mobile" value="mobile">
											Option Mobile
										</label>
									</div>
								</div>
								<div class="col-md-6">
									<div id="showinternet" style="display:none;">
										<div class="input-group"><input id="domain" name="domainnew" type="text" class="form-control" placeholder="domain.com">  <span class="input-group-btn"> <button class="btn btn-success checkdom" id="checkdomain" type="button">Check Domain!</button> </span>
									</div>
									<small id="showok" style="display:none;" class="form-text text-success"><strong>Congratulation, Domain available click submit!</strong></small>
									<small id="shownok" style="display:none;" class="form-text text-danger"><strong>Sorry, Domain is not available</strong></small>
								</div>
								<div id="showmobile"  style="display:none;">
									<label for="exampleInputEmail1">Enter Domain</label><input name="domaintransfer" type="text" class="form-control" id="domain">
								</div>
							</div>
						</div>
					</fieldset>
					<button type="submit" class="btn btn-primary">Submit</button>
				</fieldset>
			</form>
		</div>
	</div>
</div>
</div>
</div>
<script>
	$(document).ready(function() {
$('input[type=radio][name=optionsRadios]').change(function() {
if (this.value == 'no') {
$("#showmobile").hide();
$("#showinternet").hide();
}else if (this.value == 'mobile') {
$("#showmobile").show();
$("#showinternet").hide();
}else if (this.value == 'internet') {
$("#showmobile").hide();
$("#showinternet").show();
}
});
});
</script>
<script>
$(document).ready(function() {
$( "#checkdomain" ).click(function() {
	var domain =$('#domain').val();
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/whois/check',
type: 'post',
dataType: 'json',
data: {
domain:domain
},
success: function (data) {
console.log(data);
if(data.result){
$("#showok").show();
}else{
$("#shownok").show();
}
},
error: function(XMLHttpRequest, textStatus, errorThrown) {
//alert("Error  accour while sending your email");
console.log(errorThrown);
}
});
});
});
</script>
<script>
	$( "#groupid" ).change(function() {
		//e.preventDefault();
			var id=$('select[name=gid]').val();
$.ajax({
type : 'POST',
url : window.location.protocol + '//' + window.location.host + '/admin/subscription/getproducts',
data: {'id':id},
dataType: 'json',
success : function (d) {
var $el = $("#packageids");
$el.empty(); // remove old options
$.each(d, function(key,value) {
$el.append($("<option></option>")
.attr("value", value).text(value.name));
});
},
});
});
</script>