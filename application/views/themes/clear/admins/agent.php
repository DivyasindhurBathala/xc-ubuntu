<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Agent List'); ?>
        <div class="float-right">
          <!-- <a class="btn btn-rounded btn-success" id="agentexport" href="<?php echo base_url(); ?>admin/agent/export_agents"><i
            class="fa fa-file-excel"></i>
          <?php echo lang('Export Agents'); ?></a> -->
          <button class="btn btn-rounded btn-success" id="newagent" data-toggle="modal" data-target="#addAgent"><i
              class="fa fa-plus-circle"></i>
            <?php echo lang('Add New Agent'); ?></button>
        </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
          <?php echo lang('List Agent'); ?>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="clients">
            <thead>
              <tr>
                <th>
                  <?php echo lang('#ID'); ?>
                </th>
                <th>
                  <?php echo lang('Agent'); ?>
                </th>
                <th>
                  <?php echo lang('Contact'); ?>
                </th>
                <th>
                  <?php echo lang('Reseller Type'); ?>
                </th>
                <th>
                  <?php echo lang('Active Service'); ?>
                </th>
                <th>
                  <?php echo lang('Last Commission'); ?>
                </th>
                <th>
                  <?php echo lang('Comission Type'); ?>
                </th>
                <th>
                  <?php echo lang('Comission Value'); ?>
                </th>
                <th>
                  <?php echo lang('Earnings'); ?>
                </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(
      data) {
      var x = new Date();
      var y = x.getFullYear().toString();
      var m = (x.getMonth() + 1).toString();
      var d = x.getDate().toString();
      (d.length == 1) && (d = '0' + d);
      (m.length == 1) && (m = '0' + m);
      var date = y + '-' + m + '-' + d;
      $('#clients').DataTable({
        "autoWidth": false,
        "processing": true,
        "orderCellsTop": true,
        "ordering": true,
        "serverSide": true,
        "colReorder": true,
        "ajax": window.location.protocol + '//' + window.location.host +
          '/admin/table/get_agents',
        "aaSorting": [
          [1, 'desc']
        ],
        "language": {
          "url": window.location.protocol + '//' + window.location.host +
            "/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
        "fnRowCallback": function(nRow, aData, iDisplayIndex) {
          if (aData[8] == date) {
            $('td:eq(0)', nRow).html(aData[0] + '<img src="' + window.location
              .protocol + '//' + window
              .location.host + '/assets/img/new-sticker.png" height="16">');
          }
          $('td:eq(0)', nRow).html('<a  href="' + window.location.protocol + '//' +
            window.location.host +
            '/admin/agent/detail/' + aData[0] + '">' + aData[0] + '</a>');
          $('td:eq(1)', nRow).html('<a  href="' + window.location.protocol + '//' +
            window.location.host +
            '/admin/agent/detail/' + aData[0] + '">' + aData[1] + '</a>');
          $('td:eq(2)', nRow).html('<a  href="' + window.location.protocol + '//' +
            window.location.host +
            '/admin/agent/detail/' + aData[0] + '">' + aData[2] + '</a>');
          $('td:eq(3)', nRow).html('<a href="' + window.location.protocol + '//' +
            window.location.host +
            '/admin/agent/detail/' + aData[0] + '">' + aData[9] + '</a>');
          if (aData[8] > 0) {
            $('td:eq(8)', nRow).html('<strong><span class="text-success">' + data
              .currency + aData[8] +
              '</span>');
          } else {
            $('td:eq(8)', nRow).html('<strong><span class="text-success">' + data
              .currency +
              '0.0</span>');
          }

          if (aData[5] > 0) {
            $('td:eq(5)', nRow).html('<strong><span class="text-success">' + data
              .currency + aData[5] +
              '</span>');
          } else {
            $('td:eq(5)', nRow).html('<strong><span class="text-success">' + data
              .currency +
              '0.0</span>');
          }
          return nRow;
        },
      });
    });
  });
</script>


<div class="modal fade" id="addAgent" tabindex="-1" role="dialog" aria-labelledby="addAgent" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/agent/add">
        <input type="hidden" name="companyid"
          value="<?php echo $this->session->cid; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Register Agent'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="agent"><?php echo lang('Companyname'); ?></label>
              <input name="agent" type="text" class="form-control" id="companyname">
            </div>
            <div class="form-group col-sm-6">
              <label for="contact_name"><?php echo lang('Contact Name'); ?></label>
              <input name="contact_name" type="text" class="form-control" id="agent">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-sm-5">
              <label for="address1"><?php echo lang('Address1'); ?></label>
              <input name="address1" type="text" class="form-control" id="email">
            </div>
            <div class="form-group col-sm-3">
              <label for="postcode"><?php echo lang('Postcode'); ?></label>
              <input name="postcode" type="text" class="form-control" id="password">
            </div>
            <div class="form-group col-sm-4">
              <label for="city"><?php echo lang('City'); ?></label>
              <input name="city" type="text" class="form-control" id="password">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-sm-6">
              <label for="phonenumber"><?php echo lang('Phonenumber'); ?></label>
              <input name="phonenumber" type="text" class="form-control" id="text">
            </div>
            <div class="form-group col-sm-6">
              <label for="country"><?php echo lang('Country'); ?></label>
              <select name="country" class="form-control" name="country">
                <?php foreach (getCountries() as $key => $row) { ?>
                <option value="<?php echo $key; ?>" <?php if ($setting->country_base == $key) {?>
                  selected<?php } ?>><?php echo $row; ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>



          <div class="row commission">
            <div class="form-group col-sm-4">
              <label for="reseller_type"><?php echo lang('Reseller Type'); ?></label>
              <select class="form-control" name="reseller_type">
                <?php foreach (array('Postpaid','Prepaid','Internal') as $type) { ?>
                <option value="<?php echo $type; ?>"><?php echo $type; ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <div class="balance col-sm-8" style="display:none;">
            <div class="form-group">
              <label for="balance"><?php echo lang('Start Balance'); ?></label>
              <input name="reseller_balance" type="number" step="any" class="form-control" id="balance" value="0.00">
             </div>
             <div class="form-group ">
              <label for="balance"><?php echo lang('Discount'); ?> %</label>
              <input name="discount" type="number" step="any" class="form-control" id="discount" value="10.00">
             </div>

             <div class="form-group ">
              <label for="balance"><?php echo lang('Minimum Topup Amount'); ?></label>
              <input name="min_topup" type="number" step="any" class="form-control" id="min_topup" value="100.00">
             </div>

             <div class="form-group ">
              <label for="balance"><?php echo lang('Maximum Topup Amount'); ?></label>
              <input name="max_topup" type="number" step="any" class="form-control" id="max_topup" value="500.00">
             </div>
            </div>

            <div class="form-group col-sm-4">
              <label for="comission_type"><?php echo lang('Comission Type'); ?></label>
              <select class="form-control" name="comission_type">
                <?php foreach (array('Percentage','FixedAmount') as $type) { ?>
                <option value="<?php echo $type; ?>"><?php echo $type; ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group col-sm-4">
              <label for="comission_value"><?php echo lang('Comission Value'); ?></label>
              <input name="comission_value" type="number" step="any" class="form-control" id="comission_value">
            </div>
          </div>

          <div class="row commission">
            <div class="form-group col-sm-6">
              <label for="email"><?php echo lang('Email'); ?></label>
              <input name="email" type="email" class="form-control" id="email">
            </div>
            <div class="form-group col-sm-6">
              <label for="password"><?php echo lang('Password'); ?></label>
              <input name="password" type="text" class="form-control"
                value="<?php echo random_string('alnum', 10); ?>"
                id="password">
            </div>
          </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Submit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
$( document ).ready(function() {
  var reseller_type  = $( "#reseller_type option:selected" ).val();
  if(reseller_type == "Postpaid"){
    $("#comission_type").val('Percentage');
    $('.balance').hide();
    $('.commission').show();
    $('#discount').val('0.00');
    $('#min_topup').val('0.00');
    $('#max_topup').val('0.00');
  }else if(reseller_type == "Prepaid"){
    $("#comission_type").val('Percentage');
    $('.balance').show();
    $('.commission').hide();
  }else{
    $("#comission_type").val('None');
    $('.balance').hide();
    $('.commission').hide();
  }
  $( "#reseller_type" ).change(function() {
    var reseller_type  = $( "#reseller_type option:selected" ).val();
if(reseller_type == "Postpaid"){
  $("#comission_type").val('Percentage');
  $('.balance').hide();
  $('.commission').show();
  $('#discount').val('0.00');
    $('#min_topup').val('0.00');
    $('#max_topup').val('0.00');
}else if(reseller_type == "Prepaid"){
    $("#comission_type").val('Percentage');
    $('.balance').show();
    $('.commission').hide();
}else{
  $("#comission_type").val('None');
    $('.balance').hide();
    $('.commission').hide();
}
});
});
</script>