
<button class="mr-2 mb-2 btn btn-primary" data-target=".bd-export-modal-lg" data-toggle="modal"> Large modal</button>

          <section class="tables">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="collapse" id="collapseExample">
          <div class="card card-block">
            <form>
              <fieldset>
                <div class="form-group">
                  <label for="staticEmail" class="col-sm-2 col-form-label">Search Customer</label>
                  <div class="col-sm-12">
                    <input type="text"  class="form-control ui-autocomplete-input ui-autocomplete-loading client" autocomplete="off"  id="customer" value="">
                  </div>
                </div>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
   <div class="col-lg-12"  style="display:none;" id="creteria">
        <div class="card">

          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Please select criteria below:</h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">

            <ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#bydate">By Date Range</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#bycustomer">By Customer</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#bystatus">By Status</a>
  </li>

</ul>
<div id="myTabContent" class="tab-content">

  <div class="tab-pane fade show active" id="bydate">
    <fieldset>
    <legend>Please specify Range: </legend>
    <form method="post" action="<?php echo base_url(); ?>admin/warehouse/export_invoice_bydate">
      <div class="form-group">
      <label for="exampleInputEmail1">From: </label>
      <input name="start" type="text" class="form-control col-sm-3" id="pickdate"" readonly>

    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">To:</label>
      <input name="end" type="text" class="form-control col-sm-3" id="pickdate2" readonly>

    </div>

     <div class="form-group">
      <label for="exampleInputEmail1"></label>
      <button type="submit" class="btn btn-md btn-primary col-md-3"><i class="fa fa-file-excel"></i> Export</button>

    </div>
  </form>
</fieldset>
   </div>
  <div class="tab-pane fade" id="bycustomer">
        <fieldset>
    <legend>Please specify Customer: </legend>
    <form method="post" action="<?php echo base_url(); ?>admin/warehouse/export_invoice_byclient">
      <div class="form-group">
      <label for="exampleInputEmail1">Customer ID: </label>
      <input name="userid" type="text" class="form-control col-sm-3  ui-autocomplete-input ui-autocomplete-loading clientsearch" id="userid">

    </div>

  <div class="form-group" id="showme" style="display:none;">
      <label for="exampleInputEmail1">Customer Name: </label>
      <input  type="text" class="form-control col-sm-3  ui-autocomplete-input ui-autocomplete-loading clientsearch" id="customername" readonly>

    </div>
     <div class="form-group">
      <label for="exampleInputEmail1"></label>
      <button type="submit" class="btn btn-md btn-primary col-md-3"><i class="fa fa-file-excel"></i> Export</button>

    </div>
  </form>
</fieldset>
     </div>
  <div class="tab-pane fade" id="bystatus">
      <fieldset>
    <legend>Please specify Customer: </legend>
    <form method="post" action="<?php echo base_url(); ?>admin/warehouse/export_invoice_bystatus">
      <div class="form-group">
      <label for="exampleInputEmail1">Status: </label>

    <select class="form-control" id="exampleSelect1" name="status">
        <option value="Paid">Paid</option>
        <option value="Unpaid">Unpaid</option>
        <option value="Credited">Credited</option>
        <option value="Draft">Draft</option>

      </select>


    </div>


     <div class="form-group">
      <label for="exampleInputEmail1"></label>
      <button type="submit" class="btn btn-md btn-primary col-md-3"><i class="fa fa-file-excel"></i> Export</button>

    </div>
  </form>
</fieldset>
     </div>
  <div class="tab-pane fade" id="byamount">

   </div>
</div>

            </div>
          </div>
        </div>
      </div>

      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <button class="btn btn-primary btn-sm" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-plus-circle"></i> <?php echo lang('New Invoice'); ?></button>
            <button class="btn btn-primary btn-sm"><i class="fa fa-bank"></i> <?php echo lang('Load Coda'); ?></button>
           <button class="btn btn-primary btn-sm" id="exportinvoice"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <?php echo lang('Export Invoices'); ?></button>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4"><?php echo lang('List Invoices'); ?></h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="dt_invoices">
                <thead>
                  <tr>
                    <th><?php echo lang('#Invoicenum'); ?></th>
                    <th><?php echo lang('Customer'); ?></th>
                    <th><?php echo lang('Date'); ?></th>
                    <th><?php echo lang('Duedate'); ?></th>
                    <th><?php echo lang('Total'); ?></th>
                    <th><?php echo lang('Status'); ?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



    <div aria-hidden="true" aria-labelledby="ExportModal" class="modal fade bd-export-modal-lg" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
              Modal title
            </h5>
            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
          </div>
          <div class="modal-body">
               <form method="post" action="<?php echo base_url(); ?>admin/warehouse/export_invoice_bystatus">
      <div class="form-group">
      <label for="exampleInputEmail1">Status: </label>

    <select class="form-control" id="exampleSelect1" name="status">
        <option value="Paid">Paid</option>
        <option value="Unpaid">Unpaid</option>
        <option value="Credited">Credited</option>
        <option value="Draft">Draft</option>

      </select>


    </div>


     <div class="form-group">
      <label for="exampleInputEmail1"></label>
      <button type="submit" class="btn btn-md btn-primary col-md-3"><i class="fa fa-file-excel"></i> Export</button>

    </div>
  </form>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button><button class="btn btn-primary" type="button"> Save changes</button>
          </div>
        </div>
      </div>
    </div>


