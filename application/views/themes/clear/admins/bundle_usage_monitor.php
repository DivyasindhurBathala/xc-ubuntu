<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Bundle Usage Logs'); ?> 
     
      </h6>
      <div class="element-box">
        <h5 class="form-header">
           <!-- <?php echo lang('List Bundle usage Warning'); ?> -->
        </h5>
     
         <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="bundle">
            <thead>
              <tr>
                <th><?php echo lang('Date'); ?></th>
                <th><?php echo lang('Msisdn'); ?></th>
                <th><?php echo lang('Customername'); ?></th>
                <th><?php echo lang('Description'); ?></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
   $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#bundle').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_bundleusage_log/<?php echo $this->session->cid; ?>',
"aaSorting": [[0, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"createdRow": function( nRow, aData, dataIndex){
  if(aData[7] >= 2){
  $(nRow).addClass('text-danger');
}
            },
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
$('td:eq(1)', nRow).html('<a href="<?php echo base_url(); ?>admin/subscription/detail/'+aData[5]+'">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a href="<?php echo base_url(); ?>admin/client/detail/'+aData[4]+'">'+aData[2]+'</a>');



return nRow;
},
});


});
});
</script>