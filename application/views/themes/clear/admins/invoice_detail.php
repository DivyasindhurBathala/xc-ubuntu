<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			<?php echo lang('Invoice Detail'); ?> [<a href="<?php echo base_url(); ?>admin/client/detail/<?php echo getWhmcsid($this->uri->segment(5)); ?>"><?php echo lang('Back to Client Details'); ?></a>]
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				</h5>
				<div class="table-responsive">
					<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="close">
									<?php if ($this->session->cid == "54") {?>

										<button class="btn btn-primary btn-sm" onclick="changeInvoiceDueDate('<?php echo $invoice->iInvoiceNbr; ?>');"><i class="fa fa-calendar"></i> <?php echo lang('Change Duedate'); ?></button>

									<?php }?>
									<a class="btn btn-sm btn-secondary" href="<?php echo base_url(); ?>admin/invoice/download/<?php echo $invoice->iInvoiceNbr; ?>" target="_blank"><i class="fa fa-file-pdf"></i> <?php echo lang('Download Invoice'); ?></a>
									<a class="btn btn-sm btn-secondary" href="<?php echo base_url(); ?>admin/invoice/downloadcdr/<?php echo $invoice->iInvoiceNbr; ?>" target="_blank"><i class="fa fa-file-excel"></i> <?php echo lang('Download CDR'); ?></a>
									<button class="btn btn-primary btn-sm" id="sendinvoice"  onclick="confirmation_send('<?php echo $this->uri->segment(4); ?>');"><i class="fa fa-envelope"></i> <?php echo lang('Send Invoice'); ?></button>
									<?php if (in_array($invoice->iInvoiceStatus, array("51", "52", "53"))) {?>
									<?php 	if(reminderbutton($this->uri->segment(4))){ ?>
									<button class="btn btn-primary btn-sm" id="disablereminder"  onclick="disable_reminder();"><i class="fa fa-stop"></i> <?php echo lang('Disable Reminder'); ?></button>
										<?php }else{ ?>
											<button class="btn btn-primary btn-sm" id="enablereminder"  onclick="enable_reminder();"><i class="fa fa-stop"></i> <?php echo lang('Enable Reminder'); ?></button>
								
										<?php } ?>
											<?php } ?>
									<?php if ($this->session->role == "superadmin") {?>
										
									<button class="btn btn-primary btn-sm" onclick="addPayment('<?php echo $invoice->iInvoiceNbr; ?>');"><i class="fa fa-credit-card"></i> <?php echo lang('Apply Payment'); ?></button>
								
									<?php }?>

							
								</div>
								<div class="card-header d-flex align-items-center">
									<h3 class="h4"><?php echo lang('Invoice Details'); ?> #<?php echo $invoice->iInvoiceNbr; ?></h3>

								</div>
								<div class="card-body">
									<div class="row">
										<div class="col-md-12">
											<table class="table table-bordered table-striped">
												<thead>
													<tr class="text-white bg-primary">
														<th><?php echo lang('Client ID'); ?></th>
														<th><?php echo lang('Invoice Date'); ?></th>
														<th><?php echo lang('Invoice DueDate'); ?></th>
														<th><?php echo lang('Invoice Status'); ?></th>
														<th><?php echo lang('Payment Reference'); ?></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><?php echo $invoice->iAddressNbr; ?></td>
														<td><?php echo str_replace(' 00:00:00.000', '', $invoice->dInvoiceDate); ?></td>
														<td><?php echo str_replace(' 00:00:00.000', '', $invoice->dInvoiceDueDate); ?></td>
														<td><?php if (in_array($invoice->iInvoiceStatus, array("51", "52", "53"))) {?>
										<?php if ($invoice->dInvoiceDueDate > date('Y-m-d')) {?>
										<img src="<?php echo base_url(); ?>assets/img/unpaid.png"  height="50">
										<?php } elseif ($invoice->dInvoiceDueDate <= date('Y-m-d')) {?>
										<img width="200" src="<?php echo base_url(); ?>assets/img/Overdue.png" height="50">
										<?php }?>
										<?php } elseif ($invoice->iInvoiceStatus == "54") {?>
										<img src="<?php echo base_url(); ?>assets/clear/img/paid.png"  height="50">
										<?php } else {?>
										<?php }?>
										</td>
														<td><?php echo $invoice_ref; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="row" style="padding-top: 20px;">
										<div class="col-md-4">
											<p class="lead"><?php echo lang('Payment Methods'); ?>:</p>
											<div class="row">
												<div class="col-md-12">
													<table class="table table-borderless table-sm">
														<tbody>
															<tr>
																<td><?php echo lang('Bank'); ?>:</td>
																<td class="text-right"></td>
															</tr>
															<tr>
																<td><?php echo lang('Begunstigde'); ?>:</td>
																<td class="text-right"><?php echo $setting->companyname; ?></td>
															</tr>
															<tr>
																<td><?php echo lang('IBAN'); ?>:</td>
																<td class="text-right"><?php echo $setting->bank_acc; ?></td>
															</tr>
															<tr>
																<td><?php echo lang('VAT'); ?>:</td>
																<td class="text-right"><?php echo $setting->vat; ?></td>
															</tr>
															<tr>
																<td><?php echo lang('kvk'); ?>:</td>
																<td class="text-right"><?php echo $setting->company_registration; ?></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
										<div class="col-md-2">
										</div>
										<div class="col-md-6">
											<p class="lead"><?php echo lang('Customer Information'); ?></p>
											<div class="row">
												<div class="col-md-12">
													<table class="table table-borderless table-sm">
														<tbody>
															<tr>
																<td><i class="fa fa-home"></i> <?php echo lang('Companyname'); ?>:</td>
																<td class="text-right"><?php if (getWhmcsid($this->uri->segment(5))) {?><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo getWhmcsid($this->uri->segment(5)); ?>"><?php echo $client->firstname . ' ' . $client->lastname; ?></a>  <?php } else {?>
																<?php echo $client->firstname . ' ' . $client->lastname; ?>
																<?php }?>
															</td>
														</tr>
														<tr>
															<td><i class="fa fa-address-card"></i> <?php echo lang('Adress'); ?></td>
															<td class="text-right"><?php echo trim($client->address1 . ' ' .$client->housenumber.' '.$client->alphabet). $client->postcode . ' ' . $client->city; ?></td>
														</tr>
														 	<tr>
															<td><i class="fa fa-address-card"></i> <?php echo lang('Email'); ?></td>
															<td class="text-right"><?php echo $client->email; ?></td>
														</tr>

													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<div class="row" style="padding-top: 20px;">
									<div class="table-responsive col-sm-12">
										<table class="table table-v2">
											<thead>
												<tr>
													<th>#</th>
													<th><?php echo lang('Item & Description'); ?></th>
													<th class="text-right"><?php echo lang('Subtotal'); ?></th>
													<th class="text-right"><?php echo lang('Total'); ?></th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($invoice->items as $key => $item) {?>
												<?php $subtotal[] = $item['mUnitPrice'];?>
												<tr>
													<th scope="row"><?php echo $key + 1; ?></th>
													<td>
														<?php echo nl2br($item['cInvoiceDetailDescription']); ?>
													</td>
													<td class="text-right">&euro;<?php if ($invoice->mInvoiceAmount < 0) {?>-<?php }?><?php echo round($item['mUnitPrice'], 2); ?></td>
													<td class="text-right">&euro;<?php if ($invoice->mInvoiceAmount < 0) {?>-<?php }?><?php echo round($item['mUnitPrice'], 2); ?></td>
												</tr>
												<?php }?>
											</tbody>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-md-9">

									</div>
									<div class="col-md-3">
										<p class="lead"><?php echo lang('Total due'); ?></p>
										<div class="table-responsive">
											<table class="table">
												<tbody>
													<tr>
														<td><?php echo lang('Subtotal'); ?></td>
														<td class="text-right">&euro;<?php if ($invoice->mInvoiceAmount < 0) {?>-<?php }?><?php echo round(array_sum($subtotal), 2); ?></td>
													</tr>
													<tr>
														<td><?php echo lang('Btw'); ?> <?php echo round($invoice->items[0]['rVATPercentage'], 2); ?>%</td>
														<td class="text-right">&euro;<?php echo $invoice->mInvoiceAmount -  array_sum($subtotal); ?></td>
													</tr>
													<tr>
														<td class="text-bold-800"><?php echo lang('Total'); ?></td>
														<td class="text-bold-800 text-right"> &euro;<?php echo round($invoice->mInvoiceAmount, 2); ?></td>
													</tr>
													<tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container-fluid">
						<?php if (!empty($transactions)) {?>
						<div class="card bg-light bg-secondary">
							<div class="card-header d-flex align-items-center">
								<h3 class="h4"><?php echo lang('Transactions'); ?> </h3>
							</div>
							<div class="card-body">
								<div class="table-responsive col-sm-12">
									<table class="table">
										<thead>
											<tr>
												<th>#TRANSID</th>
												<th class="text-right"><?php echo lang('Date'); ?></th>
												<th class="text-right"><?php echo lang('Method'); ?></th>
												<th class="text-right"><?php echo lang('Total'); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($transactions as $item) {?>
											<tr>
												<td>
													<?php echo $item['transid']; ?>
												</td>
												<td class="text-right"><?php echo date('d/m/Y', strtotime($item['date'])); ?></td>
												<td class="text-right"><?php echo ucfirst($item['paymentmethod']) ?></td>
												<td class="text-right">&euro;<?php echo $item['amount']; ?></td>
											</tr>
											<?php }?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="fade"></div>
<div id="modal"  class="modal">
	<img id="loader" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" />
</div>

<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
		<?php echo lang('Payment List'); ?>
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				</h5>
				<div class="table-responsive">
					<div class="row">
						<div class="col-lg-12">
						<table class="table table-striped table-bordered">
						<thead>
						<tr>
						<th>Payment Id</th>
						<th>Date</th>
						<th>Amount</th>
						<th>Remarks</th>
						</tr>
						</thead>
						<?php if ($payments) {?>
						<tbody>
						<?php foreach ($payments as $p) {?>
						<tr>
						<td><?php echo $p->iPaymentNbr; ?></td>
						<td><?php echo $p->dPaymentDate; ?></td>
						<td>€<?php echo number_format($p->mAssignedAmount, 2); ?></td>
						<td><?php echo $p->cPaymentFormDescription; ?></td>
						</tr>
						<?php }?>
						</tbody>
						<?php }?>

						<?php if ($rejects) {?>
						<tbody>
						<?php foreach ($rejects as $p) {?>
						<tr>
						<td><?php echo $p->end2endid; ?></td>
						<td><?php echo $p->date_transaction; ?></td>
						<td><?php echo $p->amount; ?></td>
						<td><?php echo $p->return_code; ?></td>
						</tr>
						<?php }?>
						</tbody>
						<?php }?>

						</table>
						</div>
						</div>
						</div>
						</div>
						</div>
						</div>
						</div>




<script type="text/javascript">
$( "#addpayment" ).click(function() {
$('#addcreditblock').hide("slow");
$('#addpaymentblock').show("slow");
});
$( "#addpaymenthide" ).click(function() {
$('#addpaymentblock').hide("slow");
});
$( "#addcredit" ).click(function() {
$('#addpaymentblock').hide("slow");
$('#addcreditblock').show("slow");
});
$( "#addcredithide" ).click(function() {
$('#addcreditblock').hide("slow");
});
</script>
<script type="text/javascript">
function changeInvoiceDueDate($invoiceid){
$('#duedateModal').modal('toggle');

}
function confirmation_send(invoiceid) {
$("#sendinvoice").prop('disabled', true);
var answer = confirm("<?php echo lang('Do you wish to send this invoice to customer Email'); ?>?")
if (answer){
//var invoiceid = $("#invoiceid").val();
openModal();
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/invoice/send',
type: 'post',
dataType: 'json',
data: {
invoiceid:invoiceid,
userid:'<?php echo $this->uri->segment(5); ?>',
whmcsid: '<?php echo $whmcsid; ?>'
},
success: function (data) {
console.log(data);
if(data.result){
window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/invoice/detail/"+invoiceid+"/<?php echo $this->uri->segment(5); ?>/success_send");
}
closeModal();
},
error: function(XMLHttpRequest, textStatus, errorThrown) {
alert("<?php echo lang('Error  accour while sending your email'); ?>");
closeModal();
}
});
}
else{
console.log("hello ");
$("#sendinvoice").prop('disabled', false);
}
}
function openModal() {
document.getElementById('modal').style.display = 'block';
document.getElementById('fade').style.display = 'block';
}
function closeModal() {
document.getElementById('modal').style.display = 'none';
document.getElementById('fade').style.display = 'none';
}
function addPayment(){
$('#paymentModal').modal('toggle');
}
</script>
<!--
	'DOMICILIATION'
'CREDITCARD'
'CHEQUE'
'CASH'
'BANK TRANSACTION'
'BANKRUPT RECUPERATION'
'INTERN'
'RES'
'MONAS'
'PAYPAL'
'SEPA FIRST'
'SEPA REOCCURRING'
-->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Apply Payment'); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="payments">
					<div class="form-group">
						<label for="recipient-name" class="col-form-label"><?php echo lang('Amount'); ?>: (EUR)</label>
						<input type="number" step="any" class="form-control" id="mPaymentAmount" placeholder="1.00" name="mPaymentAmount">
					</div>
					<div class="form-group">
						<label for="message-text" class="col-form-label"><?php echo lang('Paymentmethod'); ?>:</label>
						<select class="form-control" id="paymentmethod" name="cPaymentForm">
							<?php foreach (array('DOMICILIATION', 'CREDITCARD', 'CHEQUE', 'CASH', 'BANK TRANSACTION', 'BANKRUPT RECUPERATION', 'INTERN', 'RES', 'MONAS', 'PAYPAL', 'SEPA FIRST', 'SEPA REOCCURRING') as $paymentmethod) {?>
							<option value="<?php echo $paymentmethod; ?>"<?php if ($paymentmethod == "BANK TRANSACTION") {?> selected<?php }?>><?php echo lang($paymentmethod); ?></option>
							<?php }?>
						</select>
					</div>
					<div class="form-group">
						<label for="message-text" class="col-form-label"><?php echo lang('Transaction ID'); ?>:</label>
						<input class="form-control" id="Transaction" name="cPaymentFormDescription"></input>
					</div>
					<div class="form-group">
						<label for="message-text" class="col-form-label"><?php echo lang('Transaction Date'); ?>:(MM/DD/YYYY)</label>
						<input class="form-control" id="magebo_fin_date" name="dPaymentDate" readonly value="<?php echo date('m/d/Y'); ?>"></input>
					</div>
					<input type="hidden" name="iInvoiceNbr" value="<?php echo $this->uri->segment(4); ?>">
					<input type="hidden" name="step" value="1">
					<input type="hidden" name="iAddressNbr" value="<?php echo $this->uri->segment(5); ?>">
					<input type="hidden" name="cPaymentRemark" value="Executed by: <?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="apply_payment"><?php echo lang('Submit'); ?></button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="duedateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="<?php echo base_url(); ?>admin/invoice/update_duedate_invoice">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Change Invoice Duedate'); ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

					<div class="form-group">
						<label for="recipient-name" class="col-form-label"><?php echo lang('New Duedate'); ?></label>
						<input class="form-control" id="pickdate4" name="dInvoiceDueDate" readonly required>
					</div>

					<input type="hidden" name="iAddressNbr" value="<?php echo $this->uri->segment(5); ?>">
					<input type="hidden" name="iInvoiceNbr" value="<?php echo $this->uri->segment(4); ?>">

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="apply_payment"><?php echo lang('Submit'); ?></button>
			</div>
		</form>
		</div>
	</div>
</div>


<div class="modal fade" id="disableReminderx" tabindex="-1" role="dialog" aria-labelledby="disableReminderx" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="<?php echo base_url(); ?>admin/invoice/disable_reminder">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Disable reminders'); ?> <?php echo $this->uri->segment(4); ?> </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<center><?php echo lang('Do you wish to disable reminder for this Invoice when due?'); ?></center>
					<input type="hidden" name="iAddressNbr" value="<?php echo $this->uri->segment(5); ?>">
					<input type="hidden" name="iInvoiceNbr" value="<?php echo $this->uri->segment(4); ?>">

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="disableid"><?php echo lang('Submit'); ?></button>
			</div>
		</form>
		</div>
	</div>
</div>

<div class="modal fade" id="enableReminderx" tabindex="-1" role="dialog" aria-labelledby="enableReminderx" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" action="<?php echo base_url(); ?>admin/invoice/enable_reminder">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Enable reminders'); ?> <?php echo $this->uri->segment(4); ?> </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">

				<center><?php echo lang('Do you wish to enable reminder for this Invoice when due?'); ?></center>
					<input type="hidden" name="iAddressNbr" value="<?php echo $this->uri->segment(5); ?>">
					<input type="hidden" name="iInvoiceNbr" value="<?php echo $this->uri->segment(4); ?>">

			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="disableid"><?php echo lang('Submit'); ?></button>
			</div>
		</form>
		</div>
	</div>
</div>


<script>

	$('#disablereminder').click(function(){
		$('#disableReminderx').modal('toggle');
	});
	$('#enablereminder').click(function(){
		$('#enableReminderx').modal('toggle');
	});

	$('#apply_payment').click(function(){
		var mPaymentAmount = $('#mPaymentAmount').val();
		var cPaymentFormDescription = $('#Transaction').val();
		if(mPaymentAmount <= 0){
alert('Amount must be >= 0.00 Euro');
$('#mPaymentAmount').focus();
return;
		}else if(cPaymentFormDescription.length < 1){
			alert('Transaction Code may not be Empty');
			$('#Transaction').focus();
			return;
		}
	var data = $('#payments').serialize();
$.ajax({
url: '<?php echo base_url(); ?>admin/invoice/apply_payment',
type: 'post',
dataType: 'json',
data: data,
success: function (data) {
console.log(data);
if(data.result){
$('#payments').modal('toggle');
window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/invoice/detail/<?php echo $this->uri->segment(4); ?>/<?php echo $this->uri->segment(5); ?>");
}
},
error: function(XMLHttpRequest, textStatus, errorThrown) {
alert("<?php echo lang('Error  accour while sending your email'); ?>");
}
});
});
</script>