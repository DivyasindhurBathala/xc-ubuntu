<div class="content-i">
	<div class="content-box"><div class="element-wrapper">
		<div class="element-box">
			   <h5 class="form-header">
      <?php echo lang('Assign SIMCARD'); ?>  </h5>
			<form method="post" action="<?php echo base_url(); ?>admin/subscription/assign_Mobile/<?php echo $this->uri->segment(4); ?>">

				<input type="hidden" name="serviceid" value="<?php echo $this->uri->segment(4); ?>">

				<div class="row">
					<!--
					<div class="col-md-12">
						<div class="form-group has-success">
							<label class="form-control-label" for="inputSuccess1">MSISDN</label>
							<input type="text" value="<?php echo $service_setting->MSISDN->value; ?>" class="form-control is-valid" name="customfields[<?php echo $service_setting->MSISDN->id; ?>]">
							<div class="valid-feedback">MSISDN NUMBER</div>
						</div>
					</div>
					-->
					<div class="col-md-12">
						<div class="form-group has-success">
							<label class="form-control-label" for="inputSuccess13"><?php echo lang('SIMCARD'); ?></label>
							<input type="text"  id="simnumberx" value="<?php echo $service_setting->SIMNbr->value; ?>" class="form-control  ui-autocomplete-input is-valid pengkor" name="customfields[<?php echo $service_setting->SIMNbr->id; ?>]" autocomplete="new-password" placeholder="SIMCARD">
							<div class="valid-feedback"><?php echo lang('SIMCARD NUMBER'); ?>.</div>
						</div>
					</div>
					<!--
					<div class="col-md-6">
						<div class="form-group has-success">
							<label class="form-control-label" for="inputSuccess1">PUK1</label>
							<input type="text" value="<?php echo $service_setting->PUK1->value; ?>" class="form-control is-valid" name="customfields[<?php echo $service_setting->PUK1->id; ?>]">
							<div class="valid-feedback">PUK1.</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-success">
							<label class="form-control-label" for="inputSuccess1">PUK2</label>
							<input type="text" value="<?php echo $service_setting->PUK2->value; ?>" class="form-control is-valid" name="customfields[<?php echo $service_setting->PUK2->id; ?>]">
							<div class="valid-feedback">PUK2</div>
						</div>
					</div>
				-->
				</div>
				<div class="row">

					<div class="col-md-12">
						<div class="form-group">
							<label for="exampleSelect1"><?php echo lang('Order (Number) Type'); ?></label>
							<select class="form-control" id="number_type" name="number_type">
								<option value="NEW"<?php if (empty($service_setting->Porting->value)) {?> selected<?php }?>><?php echo lang('New Number'); ?></option>
								<option value="PORTING"<?php if (!empty($service_setting->Porting->value)) {?> selected<?php }?>>Porting</option>
							</select>
						</div>
					</div>
				</div>
					<div class="row" id="numbering"<?php if (empty($service_setting->Porting->value)) {?> style="display:none"<?php }?>>
					<div class="col-md-12">
						<div class="form-group has-success">
							<label class="form-control-label" for="inputSuccess1"><?php echo lang('DONOR MSISDN'); ?></label>
							<input type="text" value="<?php echo $service_setting->PortMSISDN->value; ?>" class="form-control is-valid" name="customfields[<?php echo $service_setting->PortMSISDN->id; ?>]">
								<div class="valid-feedback"><?php echo lang('Enter number you wish to port'); ?></div>

						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group has-success">
							<label class="form-control-label" for="inputSuccess1"><?php echo lang('DONOR Provider'); ?></label>
							<select class="form-control" id="number_type" name="customfields[<?php echo $service_setting->PortDonorOperator->id; ?>]">
							<?php foreach (getDonors('NL') as $row) {?>
								<option value="<?php echo $row->operator_code; ?>"<?php if ($service_setting->PortDonorOperator->value == $row->operator_code) {?> selected<?php }?>><?php echo $row->operator_name; ?></option>
							<?php }?>
							</select>

							<!--<input type="text" value="<?php echo $service_setting->PortDonorOperator->value; ?>" class="form-control is-valid" name="customfields[<?php echo $service_setting->PortDonorOperator->id; ?>]"> -->
								<div class="valid-feedback"><?php echo lang('Choose the correct Provider'); ?></div>

						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-success">
							<label class="form-control-label" for="inputSuccess1"><?php echo lang('DONOR SIMCARD Number'); ?></label>
							<input type="text" value="<?php echo $service_setting->PortDonorSim->value; ?>" class="form-control is-valid" name="customfields[<?php echo $service_setting->PortDonorSim->id; ?>]">
							<div class="valid-feedback"><?php echo lang('Enter Current Simcard of your customer'); ?></div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group has-success">
							<label class="form-control-label" for="inputSuccess1"><?php echo lang('DONOR Account Number'); ?></label>
							<input type="text" value="<?php echo $service_setting->PortAccountNumber->value; ?>" class="form-control is-valid" name="customfields[<?php echo $service_setting->PortAccountNumber->id; ?>]">
							<div class="valid-feedback"><?php echo lang('Enter Your Customer Account number at current provider (optional)'); ?></div>
						</div>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-md btn-primary"><== Assign SIMCARD ==></button>
						<a href="<?php echo base_url(); ?>admin/subscription" class="btn btn-primary btn-md"><i class="fa fa-arrow-left"></i> <?php echo lang('Back to Subscription List'); ?></a>

						<a href="javascript:void(0)" onlick="confirm_delete();" class="btn btn-danger btn-md"><i class="fa fa-trash"></i> <?php echo lang('Cancel this Order'); ?></a>
					</div>
				</div>

			</form>
		</div>
	</div>
	<script>



		 $("#number_type").change(function(){
        if($( "#number_type option:selected" ).val() == "PORTING"){
            $("#numbering").show();

        } else {
            $("#numbering").hide();
        }
    });

		 function confirm_delete(){
		 	 var ans = confirm('Are you sure to Delete this Record !!');

		 	 if(ans){
				window.replace.href = '<?php echo base_url(); ?>admin/subscription/reject_mobile_pending/<?php echo $this->uri->segment(4); ?>';
		 	 }

		 }
</script>