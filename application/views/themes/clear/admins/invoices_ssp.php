<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Invoices'); ?>
            <div class="close">
            </div>
            </h6>
            <div class="element-box">
               
                <div class="table-responsive">


                    <table class="table table-striped table-lightfont table-hover" id="invoices">
                         <tfoot>
            <tr>
                <th>Invoice Number</th>
                <th>Billing ID</th>
                <th>Invoice Date</th>
                <th>Invoice Duedate</th>
                <th>Amount</th>
                 <th>Status</th>
            </tr>
        </tfoot>
                        <thead>
                            <th> <div align="left"><?php echo lang('Invoice Number'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Billing ID'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Invoice Date'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Invoice Duedate'); ?> </div></th>
                            <th> <div align="right"><?php echo lang('Amount'); ?> </div></th>
                            <th> <div align="right"><?php echo lang('Status'); ?> </div></th>
                        </thead>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {

          $('#invoices').DataTable({
        "initComplete": function () {
        this.api().columns().eq(0).each( function (index) {
            const column = this.column(index);
            const title = $(column.header()).text();
            if(index === 2 || index === 3 || index === 5){
                var select = $(`
                    <select class="form-control">
                        <option value="">Please choose</option>
                    </select>
                `)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                });
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                });
            }else{
                var input = $(`
                    <input class="form-control" type="text" placeholder="Search ${title}" />
                `)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'keyup change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val )
                        .draw();
                });
            }
        });
    },
            "autoWidth": false,
            "processing": true,
            "orderCellsTop": true,
            "ordering": true,
            "serverSide": true,
            "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_invoices/',
            "aaSorting": [
                [0, 'desc']
            ],
            "language": {
                "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
            },
            "fnRowCallback": function(nRow, aData, iDisplayIndex) {
             $('td:eq(4)', nRow).html('€'+ aData[4]);
             $('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData[0]  + '/'+aData[6] +'">' + aData[0]  + '</a>');
             $('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[8] +'">' + aData[1]  + '</a>');
        if(aData[5] == "Paid"){
            $('td:eq(5)', nRow).html('<strong class="text-success">'+ aData[5]+'</strong>');
        }else if( aData[5] == "Unpaid"){
            $('td:eq(5)', nRow).html('<strong class="text-danger">'+ aData[5]+'</strong>');
        }else{
            $('td:eq(5)', nRow).html('<strong class="text-warning">'+ aData[5]+'</strong>');
        }
                return nRow;
            },
        });
           });
    </script>