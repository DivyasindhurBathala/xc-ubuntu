<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Proforma Invoices'); ?> <div class="close">
       <!--  <button class="btn btn-primary btn-rounded" data-target=".bd-exportpdf-modal-lg" data-toggle="modal"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>  <?php echo lang('Export Proforma Invoices'); ?> (pdf) </button>
          <button class="btn btn-rounded btn-success" data-target=".bd-export-modal-lg" data-toggle="modal"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
            <?php echo lang('Export Proforma Invoices (excel)'); ?> </button>
          <button class="btn btn-rounded btn-secondary" id="newinvoice"><i class="fa fa-plus-circle"></i> <?php echo lang('New Proforma Invoice'); ?></button> -->
        </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('List Proforma Invoices'); ?>
        </h5>
        <div id="new" class="table-responsive" style="display:none;">
           <form>
              <fieldset>
                <div class="form-group">
                  <label for="staticEmail" class="col-sm-2 col-form-label"> <?php echo lang('Search Customer'); ?></label>
                  <div class="col-sm-12">
                    <input type="text"  class="form-control ui-autocomplete-input ui-autocomplete-loading searchclient" autocomplete="off"  id="customer" value="">
                  </div>
                </div>
              </fieldset>
            </form>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="invoices">
            <thead>
              <tr>
                <th><?php echo lang('#Invoicenum'); ?></th>
                <th><?php echo lang('Customer'); ?></th>
                <th><?php echo lang('Date'); ?></th>
                <th><?php echo lang('Duedate'); ?></th>
                <th><?php echo lang('Total'); ?></th>
                <th><?php echo lang('Status'); ?></th>

              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div aria-hidden="true" aria-labelledby="ExportModal" class="modal fade bd-export-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
        <?php echo lang('Export to Excel Report'); ?>
        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url(); ?>admin/proforma/export_invoice_bystatus">
          <div class="form-group">
            <label for="exampleInputEmail1"><?php echo lang('Status'); ?>: </label>
            <select class="form-control" id="exampleSelect1" name="status">
              <option value="Paid"><?php echo lang('Paid'); ?></option>
              <option value="Unpaid"> <?php echo lang('Unpaid'); ?></option>
              <option value="Credited"> <?php echo lang('Credited'); ?></option>
              <option value="Draft"> <?php echo lang('Draft'); ?></option>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1"></label>
            <button type="submit" class="btn btn-md btn-primary col-md-3"><i class="fa fa-file-excel"></i> <?php echo lang('Export'); ?></button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> <?php echo lang('Close'); ?></button>
      </div>
    </div>
  </div>
</div>


<div aria-hidden="true" aria-labelledby="ExportModalPdf" class="modal fade bd-exportpdf-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
        <?php echo lang('Export PDF Files'); ?>
        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url(); ?>admin/proforma/export_invoice_pdf_bydate">
          <h5><?php echo lang('Export By Date'); ?></h5>
<div class="row">
<div class="col-md-6">
           <div class="form-group">
      <label for="exampleInputEmail1">From:</label>
      <input name="start" type="text" class="form-control" id="pickdate" aria-describedby="emailHelp" placeholder="<?php echo date('Y-m-d'); ?>" required>
      <small id="emailHelp" class="form-text text-muted">start date (including)</small>
    </div>

</div>
<div class="col-md-6">
           <div class="form-group">
      <label for="exampleInputEmail1">To:</label>
      <input name="end" type="text" class="form-control" id="pickdate2" aria-describedby="emailHelp" required>
      <small id="emailHelp" class="form-text text-muted">End date (including)</small>
    </div>

</div>
</div>

          <div class="form-group">
            <label for="exampleInputEmail1"></label>
            <button type="submit" class="btn btn-md btn-primary col-md-3"><i class="fa fa-file-excel"></i> Export</button>
          </div>
        </form>

        <hr />

      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button>
      </div>
    </div>
  </div>
</div>


<div id="fade"></div>
<div id="modal"  class="modal">
  <img id="loader" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" />
</div>

<!-- Modal -->
<script type="text/javascript">
$( "#newinvoice" ).click(function() {
$('#new').show('slow');
});
</script>
<script type="text/javascript">
function delete_invoice(id) {
var answer = confirm("Do you wish to delete this invoice?")
if (answer){
openModal();
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/proforma/delete',
type: 'post',
dataType: 'json',
data: {
invoiceid:id
},
success: function (data) {
console.log(data);
if(data.result){
window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/proforma/success_delete');
}
closeModal();
},
error: function(XMLHttpRequest, textStatus, errorThrown) {
alert("Error  accour while deleting your email");
closeModal();
}
});
}
else{
console.log("hello ");
}
}
function openModal() {
document.getElementById('modal').style.display = 'block';
document.getElementById('fade').style.display = 'block';
}
function closeModal() {
document.getElementById('modal').style.display = 'none';
document.getElementById('fade').style.display = 'none';
}
</script>