<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Quotes
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				Quotes details <div class="close">
					<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/quote/edit/<?php echo $quote['id']; ?>"><i class="fa fa-edit"></i> Modify Quote</a>
					<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/quote/download/<?php echo $quote['id']; ?>" target="_blank"><i class="fa fa-download"></i> Download Quote</a>
					<button class="btn btn-primary btn-sm" id="sendquote"  onclick="confirmation_send();"><i class="fa fa-envelope"></i> Send Quote</button>
					<?php if ($quote['status'] != 'Paid' || $quote['status'] != 'Draft') {?>
					<button class="btn btn-primary btn-sm" id="addpayment"><i class="fa fa-recycle"></i> Convert to Invoice</button>
					<?php }?>
				</div>
				</h5>
				<div class="table-responsive">
					<div class="row">
						<div class="col-md-4">
							<p class="lead">Payment Methods:</p>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-borderless table-sm">
										<tbody>
											<tr>
												<td>Bank name:</td>
												<td class="text-right">ING BANK</td>
											</tr>
											<tr>
												<td>Acc name:</td>
												<td class="text-right">Exocom (Tim Claesen)</td>
											</tr>
											<tr>
												<td>IBAN:</td>
												<td class="text-right">BE68 3631 6950 6934</td>
											</tr>
											<tr>
												<td>BIC code:</td>
												<td class="text-right">BBRUBEBB</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
						</div>
						<div class="col-md-4">
							<p class="lead">Customer Information</p>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-borderless table-sm">
										<tbody>
											<tr>
												<td><i class="fa fa-bank"></i> Companyname:</td>
												<td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->companyname; ?></a></td>
											</tr>
											<tr>
												<td><i class="fa fa-user"></i> Customer name:</td>
												<td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->firstname . ' ' . $client->lastname; ?></a></td>
											</tr>
											<tr>
												<td><i class="fa fa-address-card"></i> Adress:</td>
												<td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->address1 . ' <br />' . $client->postcode . ' ' . $client->city; ?></a></td>
											</tr>
											<tr>
												<td><i class="fa fa-certificate"></i> BTW</td>
												<td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->vat; ?></a></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="table-responsive col-sm-12">
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>Item & Description</th>
										<th class="text-right">Subtotal</th>
										<th class="text-right">TaxRate(Amount@%)</th>
										<th class="text-right">Total</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($quote['items'] as $key => $item) {?>
									<tr>
										<th scope="row"><?php echo $key + 1; ?></th>
										<td>
											<?php echo nl2br($item['description']); ?>
										</td>
										<td class="text-right">&euro;<?php echo exvat($item['taxrate'], $item['total']); ?></td>
										<td class="text-right">&euro;<?php echo vat($item['taxrate'], $item['total']); ?> @<?php echo $item['taxrate']; ?>%</td>
										<td class="text-right">&euro;<?php echo $item['total']; ?></td>
									</tr>
									<?php }?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-9">
						</div>
						<div class="col-md-3">
							<p class="lead">Total due</p>
							<div class="table-responsive">
								<table class="table">
									<tbody>
										<tr>
											<td>Sub Total</td>
											<td class="text-right">&euro;<?php echo $quote['subtotal']; ?></td>
										</tr>
										<tr>
											<td>Btw <?php echo $quote['items'][0]['taxrate']; ?>%</td>
											<td class="text-right">&euro;<?php echo $quote['tax']; ?></td>
										</tr>
										<tr>
											<td class="text-bold-800">Total</td>
											<td class="text-bold-800 text-right"> &euro;<?php echo $quote['total']; ?></td>
										</tr>
										<tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="container-fluid">
								<?php if (!empty($transactions)) {?>
								<div class="card bg-light bg-secondary">
									<div class="card-header d-flex align-items-center">
										<h3 class="h4">Transactions </h3>
									</div>
									<div class="card-body">
										<div class="table-responsive col-sm-12">
											<table class="table">
												<thead>
													<tr>
														<th>#TRANSID</th>
														<th class="text-right">Date</th>
														<th class="text-right">Method</th>
														<th class="text-right">Total</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach ($transactions as $item) {?>
													<tr>
														<td>
															<?php echo $item['transid']; ?>
														</td>
														<td class="text-right"><?php echo date('d/m/Y', strtotime($item['date'])); ?></td>
														<td class="text-right"><?php echo ucfirst($item['paymentmethod']) ?></td>
														<td class="text-right">&euro;<?php echo $item['amount']; ?></td>
													</tr>
													<?php }?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<?php }?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form>
		<input type="hidden" id="quoteid" value="<?php echo $quote['id']; ?>">
	</form>
	<div id="fade"></div>
	<div id="modal" class="modal">
		<img id="loader" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" />
	</div>
	<script type="text/javascript">
	$( "#addpayment" ).click(function() {
	$('#addcreditblock').hide("slow");
	$('#addpaymentblock').show("slow");
	});
	$( "#addpaymenthide" ).click(function() {
	$('#addpaymentblock').hide("slow");
	});
	$( "#addcredit" ).click(function() {
	$('#addpaymentblock').hide("slow");
	$('#addcreditblock').show("slow");
	});
	$( "#addcredithide" ).click(function() {
	$('#addcreditblock').hide("slow");
	});
	</script>
	<script type="text/javascript">
	function confirmation_send() {
	$("#sendquote").prop('disabled', true);
	var answer = confirm("Do you wish to send this quote to customer Email?")
	if (answer){
	var quoteid = $("#quoteid").val();
	openModal();
	$.ajax({
	url: window.location.protocol + '//' + window.location.host + '/admin/quote/send',
	type: 'post',
	dataType: 'json',
	data: {
	quoteid:quoteid
	},
	success: function (data) {
	console.log(data);
	if(data.result){
	window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/quote/detail/"+quoteid+"/success_send");
	}
	closeModal();
	},
	error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("Error  accour while sending your email");
				closeModal();
				}
	});
	}
	else{
	console.log("hello ");
	$("#sendinvoice").prop('disabled', false);
	}
	}
	function openModal() {
	document.getElementById('modal').style.display = 'block';
	document.getElementById('fade').style.display = 'block';
	}
	function closeModal() {
	document.getElementById('modal').style.display = 'none';
	document.getElementById('fade').style.display = 'none';
	}
	</script>