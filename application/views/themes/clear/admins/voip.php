<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			VOIP
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('List voip number'); ?> <div class="close"> <a class="btn btn-primary btn-sm" onclick="AddVOIP()" href="javascript:void(0)"><i class="fa fa-plus-circle"></i> <?php echo lang('Add voip number'); ?></a></div>
				</h5>
				<div class="table-responsive">
					<table class="table table-striped table-lightfont" id="voip">
						<thead>
							<tr>
								<th><?php echo lang('ID'); ?></th>
								<th><?php echo lang('Number'); ?></th>
								<th><?php echo lang('Date Created'); ?></th>
								<th><?php echo lang('Created by'); ?></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function AddVOIP(){
$('#NewVOIP').modal('toggle');
	}

	function DeleteVOIP(id){
$('#number_delete').val(id);
$('#numberx').html(id);

$('#DeleteVOIP').modal('toggle');
	}
</script>
<div class="modal fade" id="NewVOIP">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title"><?php echo lang('Please enter correct information'); ?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form method="post" action="<?php echo base_url(); ?>admin/voip/add_number">
				<!-- Modal body -->
				<div class="modal-body">
					<div class="form-group">
						<fieldset>
							<label class="control-label" for="vat"><?php echo lang('Number'); ?>: <span class="text-danger"><?php echo lang('Always start with 31'); ?></span></label>
							<input name="Prefix" class="form-control" id="prefix" type="text" placeholder="31XXXXXXXXX" required="">
						</fieldset>
					</div>
					<div class="form-group">
						<fieldset>
							<label class="control-label" for="vat"><?php echo lang('Date Active'); ?></label>
							<input name="ValidFrom" class="form-control" id="pickdate4" type="text" value="<?php echo date('Y-m-d'); ?>" required="">
						</fieldset>
					</div>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="modal fade" id="DeleteVOIP">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title"><?php echo lang('Are you sure that you wish to delete this Number?'); ?></h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
				<div class="modal-body">
					<h2 id="numberx"></h2>
			<form method="post" action="<?php echo base_url(); ?>admin/voip/del_number">
				<!-- Modal body -->

				<!-- Modal footer -->
				<div class="modal-footer">
					<input type="hidden" id="number_delete" name="Prefix" value=""></div>
					<button type="submit" class="btn btn-primary"><?php echo lang('Delete'); ?></button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
console.log(date);
$('#voip').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getvoip',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

$('td:eq(0)', nRow).html('<a href="javascript:void(0)" onclick="DeleteVOIP(\''+aData[0]+'\')"><i class="fa fa-trash text-danger"></i> '+aData[0]+'</a>');

$('td:eq(7)', nRow).html('<a class="btn btn-sm btn-success" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[7] + '"><i class="fa fa-search"></i> Details</a>');
return nRow;
},
});
});
});
</script>