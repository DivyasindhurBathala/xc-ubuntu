<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo lang('Admin 2 factor Authentication'); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/lumen.css?version=1.0">
    <!-- Font Awesome CSS-->
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/style.default.css?version=1.0" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/client/login.css?version1.0">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <?php if (!empty($this->session->flashdata('error'))) {
    ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-danger" role="alert">
          <strong class="text-center"><?php echo $this->session->flashdata('error'); ?></strong>
        </div>
      </div>
    </div>
    <?php
}?>
    <?php if (!empty($this->session->flashdata('success'))) {
        ?>
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-warning" role="alert">
          <strong class="text-center"><?php echo $this->session->flashdata('success'); ?></strong>
        </div>
      </div>
    </div>
    <?php
    }?>
    <div class="login-page">
      <img src="<?php echo $setting->logo_site; ?>" width="360">
      <br />
      <br />
      <div class="form">
        <form id="login-form" method="post" action="<?php echo base_url(); ?>admin/auth/auth_google">
          <input type="text" placeholder="Code" name="code" required=""/>
          <label for="login-username" class="label-material"><?php echo lang('Enter Token From Google Authenticator'); ?></label>
          <br />
          <button type="button" id="logmein"><i class="fa fa-key"></i> <?php echo lang('Confirm Token'); ?></button>
          <p class="message"><?php echo lang('If you lost your device please '); ?>? <a href="<?php echo base_url(); ?>admin/auth/reset_backup_code"><?php echo lang('Reset With Backup Code'); ?></a></p>
          <a href="https://apps.apple.com/fr/app/google-authenticator/id388497605" target="_blank"><img src="<?php echo base_url(); ?>assets/img/play-store.png" height="44"></a>  <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en" target="_blank"><img src="<?php echo base_url(); ?>assets/img/app-store.png" height="44"> </a>
        </form>
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script>

$('#logmein').click(function(){
  $('#logmein').prop('disabled', true);
  $('#logmein').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
  $('#login-form').submit();
});
  </script>
  
  </body>
</html>
<!DOCTYPE html>
<html>

<head>
    <title><?php echo lang('Admin Password Reset'); ?></title>
    <!--Made with love by Mutiullah Samim -->

    <!--Bootsrap 4 CDN-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!--Fontawesome CDN-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
        integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!--Custom styles-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <link rel="stylesheet" type="text/css"
        href="<?php echo base_url(); ?>assets/clear/reseller/<?php echo get_reseller_style($this->session->cid); ?>-style.css?version=1.3">
</head>

<body>
    <div class="container">
        <div class="d-flex justify-content-center h-100">
            <div class="cardi">
            <form id="login-form" action="<?php echo base_url(); ?>admin/auth/auth_google" method="post">

                <div class="card-header">
                    <div class="text-center">
                        <img src="<?php echo $setting->logo_site; ?>" height="80">
                    </div>
                </div>



            
                    <div class="card-body">
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" class="form-control" name="code" placeholder="Enter Code from Google Authenticator" required>

                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-center links">

                        </div>
                        <div class="d-flex justify-content-right">
                            <div class="form-group">
                                <button id="logmein" type="button"  class="btn float-right btn-block login_btn"><?php echo lang('Submit'); ?></button>
                            </div>
                        </div>
                        <?php if (!empty($this->session->flashdata('error'))) {
        ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger" role="alert">
                                        <strong
                                            class="text-center"><?php echo $this->session->flashdata('error'); ?></strong>
                                    </div>
                                </div>
                            </div>
                            <?php
    }?>
                            <?php if (!empty($this->session->flashdata('success'))) {
        ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="alert alert-warning" role="alert">
                                        <strong
                                            class="text-center"><?php echo $this->session->flashdata('success'); ?></strong>
                                    </div>
                                </div>
                            </div>
                            <?php
    }?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>

$('#logmein').click(function(){
  $('#logmein').prop('disabled', true);
  $('#logmein').html('<i class="fa fa-spinner fa-spin"></i> Please wait...');
  $('#login-form').submit();
});
  </script>
</body>
</html>
