<?php if (in_array($service->status, array( "Terminated","Cancelled"))) {
    ?>
<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Services Summary'); ?> [<a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $service->userid; ?>"><?php echo lang('Back to Client Details'); ?></a>]
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        </h5>
        <div class="table-responsive">

          <div class="row">
            <div class="col-lg-4">
              <div class="card bg-default mb-3">
                <div class="card-header bg-primary text-white" style="pading: 5px; padding-bottom:5px; padding-left:5px;">
                  <h4 class="card-title text-light"><i class="fa fa-cubes"></i>
                    <?php echo $service->packagename; ?>
                  </h4>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-hover" width="100%">
                    <tr>
                      <td>
                        <?php echo lang('Product Brand'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->product_brand; ?></strong></td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Service ID'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->id; ?></strong></td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Date Reg'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->date_created; ?></strong></td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Product Name'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->packagename; ?></strong></td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Recurring'); ?>
                      </td>
                      <td align="right"><strong><?php echo $setting->currency; ?><?php echo number_format($service->recurring, 2); ?></strong></td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Billingcycle'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo lang($service->billingcycle); ?></strong></td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Status'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo lang($service->status); ?></strong></td>
                    </tr>
                    <?php if ($service->promocode) {
        ?>
                    <tr>
                      <td>
                        <?php echo lang('Promocode'); ?>T<a href="#" onclick="removePromotion();"><i class="fa fa-trash text-danger"></i></a>
                      </td>
                      <td align="right"><strong>
                          <?php echo getPromoname($service->promocode); ?></strong></td>
                    </tr>
                    <?php
    } ?>
                    <tr>
                      <td>
                        <?php echo lang('Contract date'); ?> <small>DD-MM-YYYY</small>
                      </td>
                      <td align="right"><strong>
                        <?php if ($setting->mage_invoicing) {
        ?>
                          <?php echo convert_contract($service->date_contract); ?> (<?php echo $service->contract_terms; ?>) </strong></td>
                        <?php
    } ?>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('BillingID'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $client->mageboid; ?></strong></td>
                    </tr>
                    <?php if ($service->details->msisdn_type == "porting" && $service->details->date_wish != "0000-00-00") {
        ?>
                    <tr>
                      <td>
                        <?php echo lang('Porting Date Wish'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->details->date_wish; ?></strong></td>
                    </tr>
                    <?php
    } ?>
                    <tr>
                      <td>
                        <?php echo lang('iGeneralPricing'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->iGeneralPricingIndex; ?></strong></td>
                    </tr>

                    <tr>
                      <td>
                        <?php echo lang('Last Changes'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->details->date_modified; ?></strong></td>
                    </tr>

                  </table>
                </div>
              </div>
            </div>


            <div class="col-lg-8">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">
                      <table class="table table-striper table-bordered">
                        <thead>
                          <tr class="bg-success  text-white">
                            <th>
                              <?php echo lang('Client ID'); ?>
                            </th>
                            <th>
                              <?php echo lang('Contact'); ?>
                            </th>
                            <th>
                              <?php echo lang('Address'); ?>
                            </th>
                            <th>
                              <?php echo lang('Email'); ?>
                            </th>
                            <th>
                              <?php echo lang('Phonenumber'); ?>
                            </th>
                          </tr>
                        </thead>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo trim($client->id); ?></a></td>
                          <td><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->firstname . ' ' . $client->lastname; ?></a></td>
                          <td>
                            <?php echo $client->address1 . ' ' . $client->postcode . ' ' . $client->city; ?>
                          </td>
                          <td>
                            <?php echo $client->email; ?>
                          </td>
                          <td>
                            <?php echo $client->phonenumber; ?>
                          </td>
                        </tr>
                      </table>
                      <hr />
                      <h1 class="text-center text-danger"><?php echo lang('MSISDN'); ?>:
                        <?php echo $service->details->msisdn; ?>
                        <br />
                        <?php echo lang('SN'); ?>:
                        <?php echo $service->details->msisdn_sn; ?>
                        <br />
                      </h1>
                      <h1 class="text-center text-danger"><?php echo lang('Date End'); ?>:
                        <?php echo $service->date_terminate; ?>
                      </h1>
                      <?php if ($this->session->email == "simson.parlindungan@united-telecom.be") {
        ?>
                        <a href="<?php echo base_url(); ?>admin/subscription/delete_service/<?php echo $service->id; ?>">
                        <?php echo lang('Delete'); ?>
                        </a>
                      <?php
    } ?>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <?php
} else {
        ?>
  <script>
    $( document ).ready(function() {
      console.log('ready to rock n roll');
$('#loadingx1').show('slow');
$('#loadingx2').show('slow');
$('#loadingx3').show('slow');
$('#loadingx4').show('slow');
$('#loadingx5').show('slow');
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/complete/get_service_detail/<?php echo $this->uri->segment(4); ?>',
dataType: 'json',
success: function (data) {
  console.log('service details',data);
 $('#CountryGroup').val(data.CountryGroup);
  if(data.sum.length >1){
    $('#sum_assign').show();
    data.sum.forEach(function (b) {
      if(b.Ignored == 1){
 $('#sumx').append('<tr><td>'+b.Name+'</td><td>'+b.ValidFrom+'</td><td><select class="form-control ceilling" id="opt'+b.SumAssignmentId+'"></select></td><td><button onclick="ActivatePlan('+b.SumAssignmentId+')" type="button" class="btn btn-sm btn-success float-right"><i class="fa fa-play"></i> Reactivate This Month</button></td></tr>');
      }else{

 $('#sumx').append('<tr><td>'+b.Name+'</td><td>'+b.ValidFrom+'</td><td><select class="form-control ceilling" id="opt'+b.SumAssignmentId+'"></select></td><td><button onclick="IgnorePlan('+b.SumAssignmentId+')" type="button" class="btn btn-sm btn-primary class="float-right""><i class="fa fa-pause"></i> Ignore This Month</button></td></tr>');
      }

      if(b.ActionSet.GetSumPlanActionSetsResult.Data.Code){
        $('#opt'+b.SumAssignmentId).append('<option value="'+b.ActionSet.GetSumPlanActionSetsResult.Data.Id+'">'+b.ActionSet.GetSumPlanActionSetsResult.Data.Description+'</option>');

      }else{
        b.ActionSet.GetSumPlanActionSetsResult.Data.forEach(function (c) {
          if(b.SumSetId == c.Id){
            $('#opt'+b.SumAssignmentId).append('<option value="'+c.Id+'" selected>'+c.Description+'</option>');
          }else{
            $('#opt'+b.SumAssignmentId).append('<option value="'+c.Id+'">'+c.Description+'</option>');
          }

      });
        }

    });
    $('#scp').html('<script>$(\'.ceilling\').change(function(){var PlanId = this.id;var SN="<?php echo $mobile->msisdn_sn; ?>"; var SetId = $( "#"+PlanId+" option:selected" ).val();$.ajax({ url: window.location.protocol + "//" + window.location.host + "/admin/subscription/change_call_ceilling", type: "post", dataType: "json", data: {"sn": SN, "PlanId": PlanId, "SetId":SetId, "serviceid":<?php echo $this->uri->segment(4); ?>},success: function (data) {}}); });');
  }else{
    $('#addSum').show();
  }
$('#loadingx1').hide('slow');
$('#loadingx2').hide('slow');
$('#loadingx3').hide('slow');
$('#loadingx4').hide('slow');
$('#loadingx5').hide('slow');
if(data.bundles){
////console.log(data.bundles);
$('#tbody_bundlesx').show();
//$('#datax').html(data.cdr.data+' Bytes');
//$('#smsx').html(data.cdr.sms +' sms');
//$('#voicex').html(data.cdr.voice);
if(data.bundles.length >= 1){
data.bundles.forEach(function (b) {
 // console.log(b);
if(b.Percentage > 90){
var text = "text-danger";
var coli = "bg-danger";
}else{
var text = "text-light";
var coli = "bg-success";
}
$('#tbody_bundlesx').append(' <tr><td><i class="fa fa-'+b.icon+'"></i></td><td>'+b.szBundle+'</td><td>'+b.ValidFrom+' to '+b.ValidUntil+'</td> <td>'+b.UsedValue+' / '+b.AssignedValue+'</td><td><div style="text-align:center; font-weight:bold">'+b.Percentage+'%.</div></td><td><button class="btn btn-sm btn-danger" type="button" onclick="changeExpireBundle('+b.BundleAssignId+')"><i class="fa fa-calendar-alt"></i></button></td></tr>');
});
}else{
if(data.bundles.Percentage){
if(data.bundles.Percentage > 90){
var text = "text-danger";
var coli = "bg-danger";
}else{
var text = "text-light";
var coli = "bg-success";
}
$('#tbody_bundlesx').append(' <tr><td><i class="fa fa-'+data.bundles.icon+'"></i></td><td>'+data.bundles.szBundle+'</td><td>'+data.bundles.ValidFrom+' to '+data.bundles.ValidUntil+'</td> <td>'+data.bundles.UsedValue+' / '+data.bundles.AssignedValue+'</td><td><div style="text-align:center; font-weight:bold">'+data.bundles.Percentage+'%.</div></td><td><button class="btn btn-sm btn-danger" type="button" onclick="changeExpireBundle('+data.bundles.BundleAssignId+')"><i class="fa fa-calendar-alt"></i></button></td></tr>');
}else{
  $('#tbody_bundlesx').append(' <tr><td colspan="5" class="text-center"><?php echo lang('No Bundle assigned'); ?></td></tr>');
}


}
}else{
  $('#tbody_bundlesx').hide();

}
if(data.packages){
$('#packagesx').show();
if(data.packages.length > 1 && data.packages.length != 0){
data.packages.forEach(function (b) {
//console.log(b);
if(b.Available == "1"){
var t =' checked';
}else{
var t = '';
}
$('#packagesx').append(' <tr> <td>'+b.CallModeDescription+'</td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="'+b.PackageDefinitionId+'" type="checkbox"'+t+'><label class="custom-switch-btn" for="'+b.PackageDefinitionId+'"></label> </div></td> </tr>');
});
}else{
if(data.packages.Available == "1"){
var t =' checked';
}else{
var t = '';
}
$('#packagesx').append(' <tr> <td>'+data.packages.CallModeDescription+'</td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="'+data.packages.PackageDefinitionId+'" type="checkbox"'+t+'><label class="custom-switch-btn" for="'+data.packages.PackageDefinitionId+'"></label> </div></td> </tr>');
}
$("#packagesx").append('<script> $(".packageid").change(function() { var userid = "<?php echo $service->userid; ?>"; var serviceid = "<?php echo $service->id; ?>"; var sn = "<?php echo $mobile->msisdn_sn; ?>"; console.log(sn); var id = $(this).attr("id"); var msisdn = "<?php echo trim($service->domain); ?>"; if($(this).is(":checked")) { var val = "1"; }else{ var val = "0"; } $.ajax({ url: window.location.protocol + "//" + window.location.host + "/admin/subscription/change_packagesetting", type: "post", dataType: "json", success: function (data) { console.log(data); }, data: {"sn": sn, "id": id, "val":val, "msisdn":msisdn, "userid":userid, "serviceid":serviceid} }); });<\/script>');
}

/*
if(data.bundle_list){
$('#bundle_listx').show();
if(data.bundle_list.length > 1 && data.bundle_list.length != 0){
data.bundle_list.forEach(function (d) {
$('#bundle_listx').append('<tr> <td>'+d.name.Description+'</td><td>'+d.ValidFrom.slice(0, -15)+'</td> <td>'+d.ValidUntil.slice(0, -15)+'</td><td>'+d.UsedValue.slice(0, -7)+'/'+d.AssignedValue.slice(0, -7)+'  </td><td><div class="progress"> <div class="progress-bar" role="progressbar" style="width: '+d.Percentage.toFixed(2)+'%;" aria-valuenow="'+d.Percentage.toFixed(2)+'" aria-valuemin="0" aria-valuemax="100"></div> </div></td></tr>');
});
}else{
}
}
*/
}
});

});
</script>
  <?php //print_r($service);?>
  <div class="content-i">
    <div class="content-box">
      <div class="element-wrapper">
        <h6 class="element-header">
        <?php echo lang('Services Summary'); ?> [<a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $service->userid; ?>"><?php echo lang('Back to Client Details'); ?></a>]

            <div class="float-right"><button class="btn btn-sm btn-primary" onclick="MoveServicesNow()">Move Subscription to another Customer</button></div>

        </h6>
        <div class="element-box">
          <h5 class="form-header">
          </h5>
          <div class="table-responsive">
          <?php if ($service->orderstatus != "Active"  && $service->details->msisdn_type == "porting") {
            ?>
            <div class="row">
              <div class="col-sm-12">

                  <div class="alert alert-warning" role="alert">
                <h6 class="text-danger"> <i><?php echo lang('Attention'); ?>:  <?php echo lang('This Service is now Active but the Portin may have not been completed, because the contract date is already due on'); ?>:  <?php echo convert_contract_dutch_number($service->date_contract); ?></i></h6>
                  </div>


              </div>
            </div>

          <?php
        } ?>

            <?php if ($service->status == "Suspended") {
            ?>
            <div class="row">
              <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                  <strong><?php echo lang('This service is currently suspended, it does not mean billing would stop'); ?></strong>
                </div>
              </div>
            </div>
            <?php
        } ?>
            <div class="row">
              <div class="col-lg-4">
                <div class="card bg-default mb-3">
                  <div class="card-header bg-primary text-white" style="pading: 5px; padding-bottom:5px; padding-left:5px;">
                    <h4 class="card-title text-light"><i class="fa fa-cubes"></i>
                      <?php echo $service->packagename; ?>
                    </h4>
                  </div>
                  <div class="card-body">
                    <table class="table table-striped">
                      <tr>
                        <td>
                          <?php echo lang('Product Brand'); ?>
                        </td>
                        <td align="right"><strong>
                            <?php echo $service->product_brand; ?></strong></td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('Service ID'); ?>
                        </td>
                        <td align="right"><strong>
                            <?php echo $service->id; ?></strong></td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('Date Reg'); ?>
                        </td>
                        <td align="right"><strong>
                            <?php echo convert_from_be_nl($service->date_created); ?> </strong></td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('Product Name'); ?>
                        </td>
                        <td align="right"><strong>
                            <?php echo $service->packagename; ?></strong></td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('Recurring'); ?>
                        </td>
                        <td align="right"><strong><?php echo $setting->currency; ?><?php echo number_format($service->recurring, 2); ?></strong></td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('Billingcycle'); ?>
                        </td>
                        <td align="right"><strong>
                            <?php echo lang($service->billingcycle); ?></strong></td>
                      </tr>
                      <tr>
                        <td>

                          <?php echo lang('Status'); ?>
                        </td>
                        <td align="right"><strong>
                            <?php echo lang($service->orderstatus); ?></strong></td>
                      </tr>

                        <?php if ($service->orderstatus == "PortinAccepted") {
            ?>

                            <tr>
                        <td>

                          <?php echo lang('ActionDate'); ?>
                        </td>
                        <td align="right" class="blink_me text-danger"><strong id="actiondatex"></strong></td>
                      </tr>

                          <?php
        } ?>


                      <?php if ($service->promocode) {
            ?>
                      <tr>
                        <td>
                          <?php echo lang('Promocode'); ?>
                        </td>
                        <td align="right"><strong>

                            <?php echo getPromoname($service->promocode); ?></strong></td>
                      </tr>
                      <?php
        } ?>
                      <?php if ($setting->mage_invoicing) {
            ?>
                      <tr>
                        <td>

                          <?php echo lang('Contract date'); ?> <a href="#" data-toggle="tooltip" data-placement="top" title="Format: DD-MM-YYYY"><i class="fa fa-info-circle"></i></a>

                        </td>
                        <td align="right" class="text-success"><strong>
                            <?php echo convert_contract_dutch_number($service->date_contract); ?></strong></td>
                      </tr>
                       <?php
        } ?>
                       <tr>
                        <td>
                          <?php echo lang('Contract Length'); ?> <button  type="button" onclick="ChangeContractTerm()"><i class="fa fa-edit"></i></button>
                        </td>
                        <td align="right"><strong>
                            <?php echo $service->contract_terms; ?> <?php echo lang('month'); ?></strong></td>
                      </tr>
                      <?php if ($this->session->cid == 54) {
            ?>
                         <tr>
                        <td>
                          <?php echo lang('First Possible Cancellation'); ?>  <a href="#" data-toggle="tooltip" data-placement="top" title="Format: DD-MM-YYYY"><i class="fa fa-info-circle"></i></a>
                        </td>
                        <td align="right" class="text-danger"><strong>
                           <?php echo str_replace('Eind contractdatum: ', '', calculate_contractend_date($service->date_contract, $service->contract_terms)); ?></strong></td>
                      </tr>
                      <?php
        } else {
            ?>
                         <tr>
                        <td>
                          <?php echo lang('First Possible Cancellation'); ?>  <a href="#" data-toggle="tooltip" data-placement="top" title="Format: DD-MM-YYYY"><i class="fa fa-info-circle"></i></a>
                        </td>
                        <td align="right"><strong>
                           <?php echo str_replace('Eind contractdatum: ', '', calculate_contractend_date($service->date_contract, $service->contract_terms)); ?></strong></td>
                      </tr>

                       <?php
        } ?>

                      <tr>
                        <td>
                          <?php echo lang('BillingID'); ?>
                        </td>
                        <td align="right"><strong>
                            <?php echo $client->mageboid; ?></strong></td>
                      </tr>
                      <?php if ($service->details->msisdn_type == "porting" && $service->details->date_wish != "0000-00-00") {
            ?>
                      <tr>
                        <td>
                          <?php echo lang('Porting Date Wish'); ?>  <a href="#" data-toggle="tooltip" data-placement="top" title="Format: DD-MM-YYYY"><i class="fa fa-info-circle"></i></a>
                        </td>
                        <td align="right" class="text-success"><strong>
                            <?php echo convert_to_dutch_format($service->details->date_wish); ?></strong></td>
                      </tr>
                      <?php
        } ?>
                      <tr>
                        <td>
                          <?php echo lang('iGeneralPricing'); ?>
                        </td>
                        <td align="right"><strong>
                            <?php echo $service->iGeneralPricingIndex; ?></strong></td>
                      </tr>
                      <tr>
                      <td>
                        <?php echo lang('Last Changes'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->details->date_modified; ?></strong></td>
                    </tr>
                    </table>
                  </div>
                </div>
                <?php if ($service->status == "Pending") {
            ?>
                <a href="<?php echo base_url(); ?>subscription/provisionmobile/<?php echo $service->id; ?>" class="btn btn-primary btn-lg btn-block">
                  <?php echo lang('Provision Mobile'); ?></a>
                <?php
        } else {
            ?>
                <?php if (!isStolen($service->id)) {
                ?>

                <button type="button" data-toggle="modal" data-target="#StolenModal" class="btn btn-danger btn-lg btn-block"><i
                    class="fa fa-angry"></i>
                  <?php echo lang('Stolen Phone?'); ?></button>
                <?php
            } else {
                ?>
                <div class="alert alert-danger text-white" role="alert">
                  <center> <?php echo lang('This phone was reported stolen by'); ?>
                    <?php echo isStolen($service->id)->reported_by; ?> on
                    <?php echo isStolen($service->id)->date_report; ?>
                  </center>
                </div>

                <button type="button" data-toggle="modal" data-target="#unStolenModal" class="btn btn-danger btn-lg btn-block"><i
                    class="fa fa-angry"></i>
                  <?php echo lang('Remove Stolen Phone'); ?></button>

                <?php
            } ?>
                <?php if ($service->details->bar == 0) {
                ?>
                   <button type="button" data-toggle="modal" data-target="#BlockOutgoingModal" class="btn btn-danger btn-lg btn-block"><i
                    class="fa fa-angry"></i>
                  <?php echo lang('Block Outbound'); ?></button>
              <?php
            } else {
                ?>
                        <button type="button" data-toggle="modal" data-target="#UnBlockOutgoingModal" class="btn btn-danger btn-lg btn-block"><i
                    class="fa fa-angry"></i>
                  <?php echo lang('Enable Outbound'); ?></button>
              <?php
            } ?>


                <?php if ($service->status != "Suspended") {
                ?>
                <button type="button" data-toggle="modal" data-target="#SuspendModal" class="btn btn-primary btn-lg btn-block"><i
                    class="fa fa-stop"></i>
                  <?php echo lang('Suspend'); ?></button>
                <?php
            } else {
                ?>
                <button type="button" data-toggle="modal" data-target="#UnsuspendModal" class="btn btn-primary btn-lg btn-block"><i
                    class="fa fa-play-circle"></i>
                  <?php echo lang('Unsuspend'); ?></button>
                <?php
            } ?>

                <?php if ($this->session->companyid == '54') {
                ?>
                  <br />
                  <div class="row">
                    <div class="col-md-6">
                  <button type="button" data-toggle="modal" data-target="#CdrModal" class="btn btn-primary btn-lg btn-block"><i
                    class="fa fa-list-ul"></i>
                  <?php echo lang('CDR'); ?></button>
                </div>
                  <div class="col-md-6">
                    <button type="button" data-toggle="modal" data-target="#CdrInModal" class="btn btn-primary btn-lg  btn-block"><i
                    class="fa fa-list-ul"></i>
                  <?php echo lang('CDR In'); ?></button>
                </div>
                </div>
              </br>
                  <?php
            } else {
                ?>
                     <button type="button" data-toggle="modal" data-target="#CdrModal" class="btn btn-primary btn-lg btn-block"><i
                    class="fa fa-list-ul"></i>
                  <?php echo lang('CDR'); ?></button>
                  <?php
            } ?>

                <button type="button" data-toggle="modal" data-target="#LangModal" class="btn btn-primary btn-lg btn-block"><i
                    class="fa fa-flag"></i>
                  <?php echo lang('Change Language'); ?></button>
                <?php if (empty($service->date_terminate)) {
                ?>
                <button type="button" data-toggle="modal" data-target="#SwapModal" class="btn btn-danger btn-lg btn-block"><i
                    class="fa fa-recycle"></i>
                  <?php echo lang('SWAP Simcard'); ?></button>
                <?php
            } ?>
                <?php if (!empty($service->date_terminate)) {
                ?>
                <br />
                <br />
                <div class="alert alert-danger text-white" role="alert">
                  <center> You have Planned this service to be terminated on
                    <?php echo $service->date_terminate; ?>
                  </center>
                   <button type="button" data-toggle="modal" data-target="#CancelTerminateModal" class="btn btn-danger btn-lg btn-block"><i
                    class="fa fa-fire"></i>
                  <?php echo lang('Cancel This Cancellation'); ?></button>
                </div>

                <?php
            } else {
                ?>

                <button type="button" data-toggle="modal" data-target="#TerminateModal" class="btn btn-danger btn-lg btn-block"><i
                    class="fa fa-fire"></i>
                  <?php echo lang('Terminate Subscription'); ?></button>
                <?php
            } ?>
                <?php if (!$product_change) {
                ?>
                <?php if (empty($service->date_terminate)) {
                    ?>
                <button type="button" data-toggle="modal" data-target="#ChangeService" class="btn btn-primary btn-lg btn-block"><i
                    class="fa fa-upload"></i>
                  <?php echo lang('Change Subscription'); ?></button>
                <?php
                } ?>
                <?php
            } else {
                ?>
                <br />
                <br />
                <div class="alert alert-warning" role="alert">
                  <center> <?php echo lang('You have Planned Product change on'); ?>
                    <?php echo $product_change->date_commit; ?> <?php echo lang('to'); ?>  <strong><?php echo $product_change->name; ?></strong>
                    <button class="btn btn-sm btn-warning" onclick="cancelChanges('<?php echo $service->id; ?>')"><?php echo lang('Cancel this request'); ?></button>
                  </center>
                </div>
                <?php
            } ?>




                <!-- <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-lg btn-block">Reset Balance</button> -->
                <?php
        } ?>

                <button class="btn btn-primary btn-lg btn-block" id="open_logs"><i class="fa fa-cubes"></i>
                  <?php echo lang('Open Platform Logs'); ?></button>
              </div>
              <div class="col-lg-8">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card">
                      <div class="card-body">
                        <table class="table table-striper table-bordered">
                          <thead>
                            <tr class="bg-success  text-white"">
                            <th><?php echo lang('Client ID'); ?></th>
                            <th><?php echo lang('Contact'); ?></th>
                            <th><?php echo lang('Address'); ?></th>
                            <th><?php echo lang('Email'); ?></th>
                            <th><?php echo lang('Phonenumber'); ?></th>
                          </tr>
                        </thead>
                        <tr>
                          <td><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->id; ?></a></td>
                              <td><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->firstname . ' ' . $client->lastname; ?></a></td>
                              <td>
                                <?php echo $client->address1 . ' ' . $client->postcode . ' ' . $client->city; ?>
                              </td>
                              <td>
                                <?php echo $client->email; ?>
                              </td>
                              <td>
                                <?php echo $client->phonenumber; ?>
                              </td>
                            </tr>
                        </table>
                        <hr />
                        <table class="table" id="bundlesx_table">
                          <thead>
                            <tr class="bg-primary  text-white">
                            <th width=" 5%">
                              <?php echo lang('Type'); ?>
                              </th>
                              <th width="30%">
                                <?php echo lang('Package'); ?>
                              </th>
                              <th width="30%">
                                <?php echo lang('ValidFrom'); ?>
                              </th>
                              <th width="25%">
                                <?php echo lang('Usage'); ?>
                              </th>
                              <th width="5%">
                                <?php echo lang('Percentage'); ?>
                              </th>
                              <th width="5%"></th>
                            </tr>
                          </thead>
                          <tbody id="tbody_bundlesx">
                          </tbody>
                        </table>
                        <div id="loadingx1" style="display:none;">
                          <center>
                            <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100" class="text-center">
                          </center>
                        </div>
                        <center>

                          <button class="btn btn-success" id="addbundle"><i class="fa fa-plus-circle"></i>
                            <?php echo lang('Add 30 days Bundle'); ?></button>
                      <?php if ($service->product_sub_type == "Postpaid") {
            ?>
                          <button class="btn btn-success" id="addbundleunbilled"><i class="fa fa-plus-circle"></i>
                            <?php echo lang('Add Extra Bundle'); ?></button>
                      <?php
        } ?>
                          <button class="btn btn-success" id="addSum" style="display:none;"><i class="fa fa-plus-circle"></i>
                            <?php echo lang('Add Call Ceilling'); ?></button>
                          <a class="btn btn-success" target="_blank" href="<?php echo base_url(); ?>admin/subscription/download_welcome_letter/<?php echo $this->uri->segment(4); ?>"><i
                              class="fa fa-file-pdf"></i>
                            <?php echo lang('Welcome Letter'); ?></a>
                          <?php if ($service->details->msisdn_type == "porting") {
            ?>
                          <a class="btn btn-success" target="_blank" href="<?php echo base_url(); ?>admin/subscription/download_porting_letter/<?php echo $this->uri->segment(4); ?>"><i
                              class="fa fa-file-pdf"></i>
                            <?php echo lang('Porting Letter'); ?></a>
                          <?php
        } ?>
                          <!--
                          <?php if ($this->session->cid == 54) {
            ?>
                          <button class="btn btn-success" target="_blank" onclick="SendSIMSentEmail('<?php echo $this->uri->segment(4); ?>');"><i
                              class="fa fa-file-pdf"></i>
                            <?php echo lang('Send SIM Sent'); ?></button>
                          <?php
        } ?>
                        -->
                          <?php if ($service->details->msisdn_swap == 1) {
            ?>

                          <a class="btn btn-success" target="_blank" href="<?php echo base_url(); ?>admin/subscription/download_swap_letter/<?php echo $this->uri->segment(4); ?>"><i
                              class="fa fa-file-pdf"></i>
                            <?php echo lang('Swap Letter'); ?></a>
                          <?php
        } ?>

                            <?php if ($service->product_sub_type == "Prepaid") {
            ?>
                               <button data-toggle="modal" data-target="#addReload" class="btn btn-success"><i class="fa fa-plus-circle"></i>
                            <?php echo lang('Reload Credit'); ?></button>
                            <?php
        } ?>
                           <?php if ($this->session->master == 1) {
            ?>
                              <a class="btn btn-success" href="<?php echo base_url(); ?>admin/subscription/addsimporting/<?php echo $this->uri->segment(4); ?>"><i class="fa fa-plus-circle"></i>
                            <?php echo lang('Add Portability Number to Magebo'); ?></a>
                          <?php
        } ?>
          <?php if (strpos($this->session->email, "@united-telecom.be")) {
            ?>
              <br />
              <br />
							<button class="btn btn-success col-sm-6 mr-auto ml-auto" id="btn_arta2" type="button"><i	class="fa fa-recycle"></i><?php echo lang('Fix Bundle Include Arta'); ?></button>
              <br />
              <br />
							<button class="btn btn-success col-sm-6 mr-auto ml-auto" id="btn_arta3" type="button"><i	class="fa fa-recycle"></i><?php echo lang('Fix Bundle Without Arta'); ?></button>
              <br />
              <br />
							<?php if ($service->status == "Cancelled" and empty($service->details->msisdn)) {
                ?>
							<a href="<?php echo base_url(); ?>admin/subscription/delete_service/<?php echo $this->uri->segment(4); ?>"
								class="btn btn-success">Delete
								Service</a>
							<?php
            } ?>

							<?php
        } ?>
                        </center>



 

			




                        <hr />
                        <table class="table table-striped table-bordered" id="sum_assign" style="display:none;padding-bottom:20px;">
                          <thead>
                            <tr class="bg-primary  text-white">
                              <th width="25%">
                                <?php echo lang('Call ceiling'); ?>
                              </th>
                              <th width="25%">
                                <?php echo lang('ValidFrom'); ?>
                              </th>
                              <th width="25%">
                                <?php echo lang('UsagePlan'); ?>
                              </th>
                               <th width="25%">
                              <?php echo lang('Action'); ?>
                              </th>
                            </tr>
                          </thead>
                          <tbody id="sumx">
                          </tbody>
                          <div id="scp"></div>
                        </table>
                        <hr />


                        <!--
                      <div class="text-center">
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-recycle"></i> Suspend</button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-fire"></i> Terminate</button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-list"></i> Logs</button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-arrow-circle-up"></i> Move Service</button>
                      </div>
                      -->
                      </div>
                    </div>
                  </div>
                </div>
                <!-- module import -->
                <div class="row">
                  <div class="col-md-6">
                    <div class="card z-depth-1">
                      <div class="card-header" style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">
                        <h4 class="card-title"><i class="fa fa-mobile"></i>
                          <?php echo $service->domain; ?>

                        </h4>
                      </div>
                      <div class="card-body">
                        <table class="table table-striped">
                            <?php if ($service->product_sub_type == "Prepaid") {
            ?>
                               <tr>
                            <td>
                              <?php echo lang('Credit Balance'); ?>
                            </td>
                            <td>
                            <span class="float-left text-success">
                            <?php echo $setting->currency; ?>  <?php echo number_format($credit, 2); ?>
                          </span>
                            </td>
                          </tr>
                          <?php
        } ?>


                          <tr>
                            <td>
                              <?php echo lang('SerialNumber'); ?>
                            </td>
                            <td>
                              <?php echo $mobile->msisdn_sn; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php echo lang('SIMCARD NO'); ?>.</td>
                            <td>
                              <?php echo $mobile->msisdn_sim; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php echo lang('PIN'); ?>
                            </td>
                            <td>
                              <?php echo $setting->default_pin; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php echo lang('PUK1'); ?>
                            </td>
                            <td>
                              <?php echo $mobile->msisdn_puk1; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php echo lang('PUK2'); ?>
                            </td>
                            <td>
                              <?php echo $mobile->msisdn_puk2; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php echo lang('SIMCARD Type'); ?>.</td>
                            <td>
                              <?php echo 'TRIO'; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php echo strtoupper(lang('Languages')); ?> (
                              <?php echo strtoupper(lang('Voicemail')); ?>)</td>
                            <td>
                              <?php echo lang(strtoupper(getVoiceLang($mobile->msisdn_languageid))); ?>
                            </td>
                          </tr>
                          <?php if ($setting->allow_country_group_change == 1) {
            ?>
                           <tr>
                            <td>
                              <?php echo strtoupper(lang('Country Group')); ?></td>
                            <td>
                              <select id="CountryGroup" class="form-control">
                              <?php foreach (array('0','42') as $group) {
                ?>
                                <option value="<?php echo $group; ?>"><?php echo $group; ?></option>
                              <?php
            } ?>
                            </select>
                            </td>
                          </tr>
                        <?php
        } ?>
                          <tr>
                            <td>
                              <?php echo lang('SOURCE TYPE'); ?>
                            </td>
                            <?php if (strtolower($mobile->msisdn_type) == "new") {
            ?>
                            <td><button class="btn btn-md btn-success" onclick="OpenPorting()" data-toggle="tooltip"
                                data-placement="top" title="Porting?"><i class="fa fa-mobile"></i>
                                <?php echo lang(strtoupper($mobile->msisdn_type)); ?></button></td>
                            <?php
        } else {
            ?>
                            <?php if (is_simcard_activate_before_porting(trim($mobile->msisdn))) {
                ?>
                            <td><button class="btn btn-md btn-success" onclick="OpenPorting()" data-toggle="tooltip"
                                data-placement="top" title="Porting?"><i class="fa fa-mobile"></i>
                                <?php echo lang(lang(strtoupper($mobile->msisdn_type))); ?></button></td>
                            <?php
            } else {
                ?>
                            <td>
                              <?php echo lang(strtoupper($mobile->msisdn_type)); ?>
                            </td>
                            <?php
            } ?>
                            <?php
        } ?>
                          </tr>
                          <?php if ($mobile->msisdn_type == "porting") {
            ?>
                          <tr>
                            <td>
                              <?php echo lang('TYPE Porting'); ?>
                            </td>
                            <td>
                              <?php echo lang($mobile->donor_type); ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php echo lang('DonorMSISDN'); ?>
                            </td>
                            <td>
                              <?php echo $mobile->donor_msisdn; ?>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <?php echo lang('DonorSimCardNbr'); ?>
                            </td>
                            <td>
                              <?php echo $mobile->donor_sim; ?>
                            </td>
                          </tr>
                          <?php
        } ?>
                        </table>
                        <hr />
                        <div class="text-center">
                          <!--
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-recycle"></i> <?php echo lang('Suspend'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-fire"></i> <?php echo lang('Terminate'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-list"></i> <?php echo lang('Logs'); ?></button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-arrow-circle-up"></i> <?php echo lang('Move Service'); ?></button>
                        -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="card z-depth-1">
                      <div class="card-header" style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px;">
                        <h4 class="card-title"><i class="fa fa-mobile"></i>
                          <?php echo $service->domain; ?>
                          <div class="float-right"><button class="btn btn-clear" id="EnableAll"><i class="fa fa-wifi fa-2x"></i></button>
                            <button class="btn btn-clear" id="AdvanceSet"><i class="fa fa-cog fa-2x"></i></button></div>
                        </h4>
                      </div>
                      <div class="card-body">
                        <table class="table table-striped" id="packagesx">
                          <div id="loadingx2" style="display:none;">
                            <center>
                            <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100">
                          </center>
                          </div>
                        </table>
                        <hr />
                        <div class="text-center">
                          <!--
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-recycle"></i> Suspend</button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-fire"></i> Terminate</button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-list"></i> Logs</button>
                        <button id="suspend" class="btn btn-primary btn-md"><i class="fa fa-arrow-circle-up"></i> Move Service</button>
                        -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php if ($setting->whmcs_ticket) {
            ?>
                        <hr />
                        <table class="table table-striped table-bordered tik" id="ticket_tables">
                          <thead>
                            <tr class="bg-primary  text-white">
                              <th width="15%">
                                <?php echo lang('Ticket ID'); ?>
                              </th>
                              <th width="15%">
                                <?php echo lang('Date'); ?>
                              </th>
                              <th width="55%">
                                <?php echo lang('Subject'); ?>
                              </th>
                              <th width="15%">
                                <?php echo lang('Department'); ?>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>

                        </table>
                        <?php
        } ?>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
  <form>
    <input type="hidden" id="msisdn" value="<?php echo trim($service->domain); ?>">
  </form>
  <script>
    $(document).ready(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
//console.log(product+' '+contractduration);
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
$('#CountryGroup').change(function() {
  var countrygroup = $('#CountryGroup').find(":selected").val();
var id = '<?php echo $service->id; ?>';
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/changeCountryGroup/'+id+'/'+countrygroup,
dataType: 'json',
data:{
  serviceid: id,
  countrygroup: countrygroup
},
success: function(data) {

alert('<?php echo lang('Country group has been modified to'); ?> '+countrygroup);

},
error: function(errorThrown) {

}
});

});
$("#contractduration").change(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});

$("#addonproduct").change(function() {
var product = $('#addonproduct').find(":selected").val();
var contractduration = $('#contractduration').find(":selected").val();
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getPriceProduct/'+product+'/'+contractduration,
dataType: 'json',
success: function(data) {
$('#harga').val(data.price);
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
});

$('#tickets').click(function() {

$('.tik').show();

});
$('#addbundle').click(function() {
var msisdn = '<?php echo trim($service->domain); ?>';
$('#BundleModal').modal('toggle');
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getBundleList/<?php echo $this->session->cid; ?>',
type: 'post',
dataType: 'json',
success: function (data) {
$('.bundle_loading1').hide();
$('#bundleid1').html(data.html);
$('.bundle_select1').show();

},
<?php if ($this->session->cid == 55) {
            ?>
data: {'msisdn':msisdn, 'type': 'option', 'agid': '<?php echo $service->gid; ?>'}
<?php
        } else {
            ?>
  data: {'msisdn':msisdn, 'type': 'option'}
<?php
        } ?>
});
});
$('#addbundleunbilled').click(function() {
var msisdn = '<?php echo trim($service->domain); ?>';
$('#BundleModalTarif').modal('toggle');
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getBundleList/<?php echo $this->session->cid; ?>',
type: 'post',
dataType: 'json',
success: function (data) {

// console.log(data);
$('#pricex').val(data.price);
$('.bundle_loading2').hide();
$('#bundleid2').html(data.html);
$('.bundle_select2').show();
},
<?php if ($this->session->cid == 55) {
            ?>
data: {'msisdn':msisdn, 'type': 'tarif', 'agid': '<?php echo $service->gid; ?>'}
<?php
        } else {
            ?>
  data: {'msisdn':msisdn, 'type': 'tarif'}
<?php
        } ?>
});
});

$('#addSum').click(function() {
var sn = '<?php echo trim($service->details->msisdn_sn); ?>';
var serviceid = '<?php echo trim($service->id); ?>';
var y = confirm('Are you sure?');
if(y){
  $.ajax({
url: '<?php echo base_url(); ?>admin/subscription/addSum/<?php echo $this->session->cid; ?>',
type: 'post',
dataType: 'json',
success: function (data) {

$('#addSum').hide();

window.location.href= window.location.protocol + '//' + window.location.host +"/admin/subscription/detail/"+serviceid;
},
data: {'sn':sn}
});
}
});
});
</script>
  <script>
    function IgnorePlan(id){

      $('#SumPlanIdActionIgnore').modal('toggle');
      $('#SUMAssignmentIdIgnore').val(id);
    }

     function ActivatePlan(id){

      $('#SumPlanIdActionActivate').modal('toggle');
      $('#SUMAssignmentIdActivate').val(id);
    }
     function RemovePlan(id){

      $('#SumPlanIdActionRemove').modal('toggle');
       $('#SUMAssignmentIdRemove').val(id);
    }
    function OpenPorting(){
$('#PortingExist').modal('toggle');
}
</script>
  <div class="modal fade" id="PortingExist">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="portingdata" method="post" action="<?php echo base_url(); ?>admin/subscription/addPorting">
          <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
          <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
          <input type="hidden" name="orderid" value="<?php echo $service->id; ?>">
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Add Porting'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row" id="portingbody">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number'); ?> :31XXXXXXXXXX</label>
                  <input class="form-control" autocomplete="new-username" placeholder="<?php echo lang('REQUIRED'); ?>"
                    type="number" name="MsisdnNumber" id="msisdn" value="31" required>
                </div>
                <div class="form-group">
                  <label for="">Type</label>
                  <select class="form-control" id="type_number" name="PortingType">
                    <option value="0" selected>
                      <?php echo lang('Prepaid'); ?>
                    </option>
                    <option value="1">
                      <?php echo lang('Postpaid'); ?>
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Current Provider'); ?></label>
                  <select class="form-control" id="provider" name="provider">
                    <?php foreach (getDonors('NL') as $row) {
            ?>
                    <option value="<?php echo $row->operator_code; ?>" selected>
                      <?php echo $row->operator_name; ?>
                    </option>
                    <?php
        } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('SIMcard Number'); ?></label>
                  <input class="form-control pengkor" autocomplete="new-username" placeholder="" type="text" name="simnumber"
                    id="simnumberx" required>
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Customer Type'); ?></label>
                  <select class="form-control" id="customertype1" name="customertype1">
                    <option value="0" selected>
                      <?php echo lang('Residential'); ?>
                    </option>
                    <option value="1">
                      <?php echo lang('Bussiness'); ?>
                    </option>
                  </select>
                </div>
                <div class="form-group" style="display:none;" id="acct">
                  <label for="">
                    <?php echo lang('Client Number from other provider'); ?></label>
                  <input class="form-control" autocomplete="new-username" placeholder="" type="text" name="AccountNumber"
                    id="AccountNumber">
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('PortIN Date Wish'); ?>:</label>
                  <input class="form-control" autocomplete="new-username" placeholder="<?php echo date('Y-m-d'); ?>"
                    type="text" name="PortInWishDate" id="pickdate8" required>
                </div>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" id="portingbutton" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- The Bundle List Modal -->
  <div class="modal fade" id="BundleModalTarif">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="<?php echo base_url(); ?>admin/subscription/addBundleTarif">
          <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
          <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
          <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
          <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
          <input type="hidden" name="typebar" value="2">
          <!-- Modal Header SimCardNbr -->
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Add Extra Bundle'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group bundle_select2" style="display:none;">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('The bundle you are ordering will be auto renew for next month until the date you specified below  and it will be auto prorata for the current month if aplicable:'); ?></label>
                    <br />
                    <select class="form-control" id="bundleid2" name="bundleid">
                    </select>
                  </fieldset>
                </div>
                <?php if ($service->reseller_type == "Prepaid") {
            ?>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Deduct Current Reseller Balance?'); ?>
                  </label>
                  <select class="form-control" name="reseller_charge">
                    <option value="No"><?php echo lang('No'); ?>
                    </option>
                    <option value="Yes"><?php echo lang('Yes'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            <?php
        } ?>
                <div class="form-group bundle_loading2">
                  <center><img src="<?php echo base_url(); ?>assets/img/loader1.gif"></center>
                </div>

                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="charge">
                      <?php echo lang('Charge Customer?'); ?> (<?php echo $setting->currency; ?>) <?php echo lang('each Month until Valid Until'); ?></label>
                    <select class="form-control" id="bundlechargeid2" name="charge">
                      <option value="1" selected>Yes</option>
                      <option value="0">No</option>
                    </select>
                  </fieldset>
                </div>

                <div class="form-group chargex">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('Price'); ?></label>
                    <input type="text" class="form-control" id="pricex" name="price" value="">
                  </fieldset>
                </div>

                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('VALID FROM'); ?></label>
                    <input type="text" class="form-control" id="pickdate13a" name="from" value="<?php echo date('Y-m-d\TH:i:s'); ?>"
                      required readonly>
                  </fieldset>
                </div>


                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('VALID UNTIL'); ?></label>
                    <input type="text" class="form-control" id="pickdate15" name="to" value="" readonly>
                  </fieldset>
                </div>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">
              <?php echo lang('Add Bundle'); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>



  <div class="modal fade" id="BlockOutgoingModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="blockout">
          <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
          <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
          <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
          <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
          <input type="hidden" name="typebar" value="2">
          <!-- Modal Header SimCardNbr -->
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Block Originating/Outbound Traffic'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <center>
                  <?php echo lang("Do you wish to proceed"); ?>?</center>
                <table class="table table-striped">
                  <div id="loadingx40" style="display:none;">
                    <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100">
                  </div>
                </table>
                <button type="button" id="blockoriginating" class="btn btn-block btn-primary btn-md"><?php echo lang('Save Changes'); ?></button>
              </div>
            </div>
        </form>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>




  <div class="modal fade" id="UnBlockOutgoingModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="unblockout">
          <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
          <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
          <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
          <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
          <input type="hidden" name="typebar" value="2">
          <!-- Modal Header SimCardNbr -->
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Block Originating/Outbound Traffic'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <center>
                  <?php echo lang("Do you wish to proceed"); ?>?</center>
                <table class="table table-striped">
                  <div id="loadingx40" style="display:none;">
                    <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100">
                  </div>
                </table>
                <button type="button" id="unblockoriginating" class="btn btn-block btn-primary btn-md"><?php echo lang('Save Changes'); ?></button>
              </div>
            </div>
        </form>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>

<div class="modal" id="addReload">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="reload">

      <input type="hidden" id="reload_userid" value="<?php echo $service->userid; ?>">

      <input type="hidden" id="reload_serviceid" value="<?php echo $service->id; ?>">

      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Reload Credit'); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">

         <div class="row" id="loadingreload"  style="display:none;">
          <div class="col-sm-12">
            <table class="table table-striped">
              <div id="loadingx40" class="text-center">
                <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100">
              </div>
            </table>
          </div>
        </div>
            <div class="form-group" id="datareload">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('Credit to add'); ?></label>
                    <input type="text" class="form-control" id="reload_credit" name="credit" value="10" required>
                  </fieldset>
                </div>
                <?php if ($service->reseller_type == "Prepaid") {
            ?>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Deduct Current Reseller Balance?'); ?>
                  </label>
                  <select class="form-control" name="reseller_charge" id="ChargeReseller">
                    <option value="No"><?php echo lang('No'); ?>
                    </option>
                    <option value="Yes"><?php echo lang('Yes'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            <?php
        } ?>
        <div class="row">
          <div class="col-md-12">
          <button type="button" id="ReloadNow" class="btn btn-block btn-primary btn-md"><?php echo lang('Reload'); ?></button>
        </div>
        </div>
      </div>
    </form>
      <!-- Modal footer -->
    </div>
  </div>
</div>


<div class="modal fade" id="AdvanceSetting">
  <div class="modal-dialog">
    <div class="modal-content">
      <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
      <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Advance Setting'); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <table class="table table-striped" id="showAdvance">
              <div id="loadingx40" style="display:none;">
                <img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100">
              </div>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>

<div class="modal fade" id="EnableAllSetting">
  <div class="modal-dialog">
    <div class="modal-content">
      <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
      <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Enable All Services'); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <center><?php echo lang('Do you wish to enable all services including Roaming,Premium,Data,International & ETC'); ?>?</center>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" id="enableallsetting" class="btn btn-primary">
          <?php echo lang('Submit'); ?></button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="BundleModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/addBundle">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn" value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Add Billed Bundle'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group bundle_select1" style="display:none;">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('This will activate Bundle for 30 days from the date you choose below, and customer will be invoiced'); ?></label>
                  <br />
                  <select class="form-control" id="bundleid1" name="bundleid">
                  </select>
                </fieldset>
              </div>
              <div class="form-group bundle_loading1">
                <center><img src="<?php echo base_url(); ?>assets/img/loader1.gif"></center>
              </div>
              <?php if ($service->reseller_type == "Prepaid") {
            ?>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Deduct Current Reseller Balance?'); ?>
                  </label>
                  <select class="form-control" name="reseller_charge">
                    <option value="No"><?php echo lang('No'); ?>
                    </option>
                    <option value="Yes"><?php echo lang('Yes'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            <?php
        } ?>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('VALID FROM'); ?></label>
                  <input type="text" class="form-control" id="pickdate13" name="from" value="<?php echo date('Y-m-d\TH:i:s'); ?>"
                    required readonly>
                </fieldset>
              </div>
              <?php if ($this->session->master) {
            ?>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('VALID UNTIL'); ?></label>
                  <input type="text" class="form-control" id="pickdate15a" name="to" value="<?php echo date('Y-m-t\TH:i:s'); ?>"
                    required readonly>
                </fieldset>
              </div>
              <?php
        } ?>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Add Bundle'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The Stolen Modal -->
<div class="modal fade" id="StolenModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/mobile_stolen">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="SimCardNbr" value="<?php echo $mobile->msisdn_sim; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Phone Stolen?'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('Create Ticket SIM Replacement'); ?></label>
                  <select class="form-control" id="ticket" name="ticket">
                    <option value="1">
                      <?php echo lang('NO'); ?>
                    </option>
                    <option value="2">
                      <?php echo lang('YES'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?></label>
                  <input name="msisdn" class="form-control" type="text" value="<?php echo $service->domain; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">
            <?php echo lang('Report Stolen'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" id="unStolenModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/mobile_unstolen">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="SimCardNbr" value="<?php echo $mobile->msisdn_sim; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Phone Found or Simcard Replaced?'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">

            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?></label>
                  <input name="msisdn" class="form-control" type="text" value="<?php echo $service->domain; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">
            <?php echo lang('Remove Stolen Record'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- The Suspend Modal -->
<div class="modal fade" id="SuspendModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/suspend">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <input type="hidden" name="status" value="0">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish do suspend?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>?</label>
                  <input name="msisdn" class="form-control" type="text" value="<?php echo $service->domain; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label for="Reason">Reason</label>
                <textarea class="form-control" id="Reason" name="suspend_reason" rows="4" required></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-secondary" data-dismiss="modal"><i class="fa fa-thumbs-down"></i>
            <?php echo lang('No'); ?></button>
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="LangModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/update_sim_language">
        <input type="hidden" name="msisdn" value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to change Platform Language'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('Languages'); ?> (
                    <?php echo lang('VoiceMail'); ?>)</label>
                  <select class="form-control" id="exampleSelect1" name="language">
                    <option value="1" <?php if ($client->language == "english") {
            ?> selected
                      <?php
        } ?>>
                      <?php echo lang('English'); ?>
                    </option>
                    <option value="2" <?php if ($client->language == "french") {
            ?> selected
                      <?php
        } ?>>
                      <?php echo lang('French'); ?>
                    </option>
                    <option value="3" <?php if ($client->language == "dutch") {
            ?> selected
                      <?php
        } ?>>
                      <?php echo lang('Dutch'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?></label>
                  <input name="msisdn" class="form-control" id="msisdn" type="text" value="<?php echo $service->domain; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-secondary" data-dismiss="modal"><i class="fa fa-thumbs-down"></i>
            <?php echo lang('No'); ?></button>
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The Unsuspend Modal -->
<div class="modal fade" id="UnsuspendModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/unsuspend">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish do unsuspend?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?></label>
                  <input name="msisdn" class="form-control" type="text" value="<?php echo $service->domain; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The Swap Modal -->

<!-- The Unsuspend Modal -->
<div class="modal fade" id="ChangeContractTerm">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/change_contract_terms">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to change the contract terms?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-sm-12">
               <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Contract Terms'); ?></label>
                    <input type="text" name="msisdn" value="<?php echo $service->details->msisdn; ?>" class="form-control" readonly>
                </fieldset>
              </div>

              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Contract Terms'); ?></label>
                  <select name="contract_terms" class="form-control">
                    <?php foreach (array(1,3,6,12,24) as $row) {
            ?>
                      <option value="<?php echo $row; ?>"<?php if ($service->contract_terms == $row) {
                ?> selected<?php
            } ?>><?php echo $row; ?> Month(s)</option>
                    <?php
        } ?>
                  </select>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The Swap Modal -->


<div class="modal fade" id="SwapModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/swap_simcard">
        <input type="hidden" name="msisdn" value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to swap SIMcard?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="<?php echo trim($mobile->msisdn); ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Current SIMCARD'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="<?php echo $mobile->msisdn_sim; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('New SIMCARD'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="" required>
                  <span class="text-help">
                    <?php echo lang('This will revoke old simcard, please make sure the simcard has been received by customer before executing this'); ?></span>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <fieldset>
                  <label for="exampleSelect1">
                    <?php echo lang('Charge Customer for SWAP'); ?></label>
                  <select class="form-control" id="swapcharge" name="charge">
                    <option value="YES" <?php if ($this->session->cid == "54") {
            ?> selected
                      <?php
        } ?>>Yes</option>
                    <option value="NO" <?php if ($this->session->cid != "54") {
            ?> selected
                      <?php
        } ?>>No</option>
                  </select>
                </fieldset>
              </div>
            </div>

            <div class="col-sm-6" style="display:none;" id="swapamount">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="amount">
                    <?php echo lang('Amount'); ?> (<?php echo $setting->currency; ?>)</label>
                  <input name="amount" id="chargeamount" class="form-control" type="number" value="<?php echo round($service->mobile_swapcost, 2); ?>">
                </fieldset>
              </div>
            </div>


          </div>


        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes Swap Simcard'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The ChangeService Modal -->
<div class="modal fade" id="ChangeService">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/change_service">
        <input type="hidden" name="msisdn" value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="sn" value="<?php echo trim($mobile->msisdn_sn); ?>">
        <input type="hidden" name="requestor" value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
        <input type="hidden" name="old_pid" value="<?php echo $service->packageid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to Change the Package?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">

                <label class="control-label" for="msisdn">
                  <?php echo lang('CLI(Number)'); ?></label>
                <input class="form-control" type="text" value="<?php echo trim($service->domain); ?>" readonly>

              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Product'); ?>:</label>
                <select class="form-control" id="addonproduct" name="new_pid">
                  <?php foreach (getProductSell($this->session->cid) as $row) {
            ?>
                  <option value="<?php echo $row->id; ?>">
                    <?php echo $row->name; ?>
                    <?php if ($row->setup > 0) {
                echo 'Setup fee: '.$setting->currency.' ' . number_format($row->setup, 2);
            } ?>
                  </option>
                  <?php
        } ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Contract'); ?> (
                  <?php echo lang('Month'); ?>)</label>
                <select class="form-control" id="contractduration" name="ContractDuration">
                  <option value="3">
                    <?php echo lang('Quarterly'); ?> (3 Months)</option>
                  <option value="6">
                    <?php echo lang('Semi Annually'); ?> (6 Months)</option>
                  <option value="12" selected>
                    <?php echo lang('Annually'); ?> (12 Months)</option>
                  <option value="24">
                    <?php echo lang('Bienially'); ?> (24 Months)</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Price Recurring'); ?></label>
                <input name="new_price" type="text" class="form-control" id="harga" value="">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="msisdn">
                  <?php echo lang('Date Start'); ?></label>
                <input name="date_commit" class="form-control" id="firstdate" type="text" value="<?php echo next_month_date(); ?>"
                  required readonly>
                <span class="text-help">
                  <?php echo lang('Please choose the date this package to be activated'); ?></span>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label" for="keep_start_contract_date">
                  <?php echo lang('Keep Start Contract date'); ?></label>
                  <select class="form-control" id="keep_start_contract_date" name="keep_start_contract_date">
                  <option value="Yes">
                     <?php echo lang('Yes'); ?></option>
                  <option value="No">
                    <?php echo lang('No'); ?></option>
                </select>
                <span class="text-help">
                  <?php echo lang('If you wish not to change contract Date'); ?></span>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">

                <label for="exampleSelect1">
                  <?php echo lang('Charge Customer?'); ?></label>
                <select class="form-control" id="changecharge" name="charge">
                  <option value="YES" <?php if ($this->session->cid == "54") {
            ?> selected
                    <?php
        } ?>>Yes</option>
                  <option value="NO" <?php if ($this->session->cid != "54") {
            ?> selected
                    <?php
        } ?>>No</option>
                </select>

              </div>
            </div>

            <div class="col-sm-6" style="display:none;" id="changeamount">
              <div class="form-group">
                <label class="control-label" for="amount">
                  <?php echo lang('Amount'); ?> (<?php echo $setting->currency; ?>)</label>
                <input name="amount" id="charechangeamount" class="form-control" type="number" value="<?php echo round($service->mobile_changecost, 2); ?>">

              </div>
            </div>


          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes Change Package'); ?></button>
            <br />
            <br />
            <small><?php echo lang('Any extra bundles you\'ve assigned manually will be terminated too on the date you choose'); ?></small>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="TerminateModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/terminate_sim">
        <input type="hidden" name="msisdn" value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to Terminate This SIM?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="<?php echo $service->domain; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Current SIMCARD'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="<?php echo $mobile->msisdn_sim; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <div class="form-desc">
                  <?php echo lang('Date Cancellation'); ?>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input cancel" name="date_cancellation" id="date_cancellation1"
                      value="now" checked="">
                    <?php echo lang('Now'); ?> (<?php echo lang('immediately'); ?>)</label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input cancel" name="date_cancellation" id="date_cancellation2"
                      value="future">
                    <?php echo lang('Future'); ?>
                  </label>
                </div>
              </div>
            </div>
            <div class="col-sm-12 datefuture" style="display:none;">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Date Termination'); ?></label>
                  <input name="date" class="form-control" type="text" id="pickdate66" value="<?php echo date("Y-m-d", strtotime("tomorrow")); ?>" readonly>
                <i><span class="text-help text-danger"><?php echo lang('If you choose'); ?> <?php echo date("Y-m-d", strtotime("tomorrow")); ?> <?php echo lang('then the simcard will stop to function at'); ?> <?php echo date(" Y-m-d", strtotime("tomorrow")); ?> 00:00:00</span></i>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes Cancel Service'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="CancelTerminateModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/cancel_terminate_sim">
        <input type="hidden" name="msisdn" value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to Cancel this Cancellation?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="<?php echo $service->domain; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Current SIMCARD'); ?></label>
                  <input name="new_simnumber" class="form-control" type="text" value="<?php echo $mobile->msisdn_sim; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i>
            <?php echo lang('Yes Cancel The Cancellation'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>


<!-- The CDR Modal -->
<div class="modal fade" id="CdrModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Call Detail records'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="cdr">
            <thead>
              <tr>
                <th>
                  <?php echo lang('Date'); ?>
                </th>
                <th>
                  <?php echo lang('Number'); ?>
                </th>
                <th>
                  <?php echo lang('Description'); ?>
                </th>
                <th>
                  <?php echo lang('Duration'); ?>
                </th>
                <th>
                  <?php echo lang('Type'); ?>
                </th>
                <th>
                  <?php echo lang('Cost'); ?>
                </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <?php echo lang('Close'); ?></button>
        <a class="btn btn-md btn-primary" href="<?php echo base_url(); ?>admin/subscription/export_livecdr/<?php echo trim($service->domain); ?>/<?php echo $service->id; ?>/<?php echo trim($mobile->msisdn_sn); ?>"><i
            class="fa fa-file-excel"></i>
          <?php echo lang('Export Excel'); ?></a>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="SumPlanIdActionIgnore">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="<?php echo base_url(); ?>admin/subscription/setSumPlanAction">
          <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
          <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
          <input type="hidden" name="orderid" value="<?php echo $service->id; ?>">
           <input type="hidden" name="sn" value="<?php echo $service->details->msisdn_sn; ?>">
           <input type="hidden" name="State" value="Ignore">
           <input type="hidden" name="SUMAssignmentId" id="SUMAssignmentIdIgnore" value="">
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Do you wish to disable SumPlan for current month'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number'); ?></label>
                  <input class="form-control" autocomplete="new-username" placeholder="<?php echo lang('REQUIRED'); ?>"
                    type="number" name="MsisdnNumber" id="msisdn" value="<?php echo $service->details->msisdn; ?>" required readonly>
                </div>

              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">
              <?php echo lang('Process'); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>


<div class="modal fade" id="SumPlanIdActionActivate">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="<?php echo base_url(); ?>admin/subscription/setSumPlanAction">
          <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
          <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
          <input type="hidden" name="orderid" value="<?php echo $service->id; ?>">
           <input type="hidden" name="sn" value="<?php echo $service->details->msisdn_sn; ?>">
           <input type="hidden" name="State" value="Reactivate">
           <input type="hidden" name="SUMAssignmentId" id="SUMAssignmentIdActivate" value="">
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Do you wish to disable SumPlan for current month'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number'); ?></label>
                  <input class="form-control" autocomplete="new-username" placeholder="<?php echo lang('REQUIRED'); ?>"
                    type="number" name="MsisdnNumber" id="msisdn" value="<?php echo $service->details->msisdn; ?>" required readonly>
                </div>

              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">
              <?php echo lang('Process'); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="SumPlanIdActionRemove">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="post" action="<?php echo base_url(); ?>admin/subscription/setSumPlanAction">
          <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
          <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
          <input type="hidden" name="orderid" value="<?php echo $service->id; ?>">
           <input type="hidden" name="sn" value="<?php echo $service->details->msisdn_sn; ?>">
           <input type="hidden" name="SUMAssignmentId" id="SUMAssignmentIdRemove" value="">
            <input type="hidden" name="State" value="Disable">
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Do you wish to remove this sumPlan forerver?'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number'); ?></label>
                  <input class="form-control" autocomplete="new-username" placeholder="<?php echo lang('REQUIRED'); ?>"
                    type="number" name="MsisdnNumber" id="msisdn" value="<?php echo $service->details->msisdn; ?>" required readonly>
                </div>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">
              <?php echo lang('Process'); ?></button>
          </div>
        </form>
      </div>
    </div>
  </div>



<!-- The CDR Modal -->
<div class="modal fade" id="CdrInModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Incoming Call Detail records'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="cdrin">
            <thead>
              <tr>
                <th>
                  <?php echo lang('Date'); ?>
                </th>
                <th>
                  <?php echo lang('Number'); ?>
                </th>
                <th>
                  <?php echo lang('Description'); ?>
                </th>
                <th>
                  <?php echo lang('Duration'); ?>
                </th>
                <th>
                  <?php echo lang('Type'); ?>
                </th>
                <th>
                  <?php echo lang('Cost'); ?>
                </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
       <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">
          <?php echo lang('Close'); ?></button>
        <a class="btn btn-md btn-primary" href="<?php echo base_url(); ?>admin/subscription/export_livecdr/<?php echo trim($service->domain); ?>/<?php echo $service->id; ?>/<?php echo trim($mobile->msisdn_sn); ?>"><i
            class="fa fa-file-excel"></i>
          <?php echo lang('Export Excel'); ?></a>
        -->
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="MoveService">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Do you wish to move this subscription to another customer'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <form action="<?php echo base_url(); ?>admin/subscription/move_subscription" method="post">
        <input type="hidden" name="serviceid" value="<?php echo $mobile->serviceid; ?>">
           <input type="hidden" name="olduserid" value="<?php echo $service->userid; ?>">
        <div class="modal-body">
          <div class="table-responsive">
            <div class="form-group">
              <fieldset>
                <label class="control-label" for="msisdn">
                  <?php echo lang('Artilium ID'); ?></label>
                <input name="userid" class="form-control ui-autocomplete-input customerid" type="text" id="customerid" required>
              </fieldset>
            </div>

              <div class="form-group" id="hidecustomer1">
              <fieldset>
                <label class="control-label" for="msisdn">
                  <?php echo lang('CustomerName'); ?></label>
                <input name="name" class="form-control" type="text" id="customername" readonly="">
              </fieldset>
            </div>

             <!--<div class="form-group" id="hidecustomer2">
              <fieldset>
                <label class="control-label" for="msisdn">
                  <?php echo lang('Relationid'); ?></label>
                <input name="relationid" class="form-control" type="text" id="relationidx" readonly="">
              </fieldset>
            </div>
  -->

          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" id="movecustomer" class="btn btn-primary">
            <?php echo lang('Move Subscription'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" id="ExpireBundleModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Change the Expired date of the Bundle'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <form id="expBundle">
        <input type="hidden" name="BundleAssignId" value="" id="assignid">
        <input type="hidden" name="sn" value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="serviceid" value="<?php echo $this->uri->segment(4); ?>">
         <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
        <div class="modal-body">
          <div class="table-responsive">
            <div class="form-group">
              <fieldset>
                <label class="control-label" for="msisdn">
                  <?php echo lang('Date Valid Until'); ?></label>
                <input name="ValidUntil" class="form-control" type="text" id="pickdate15d" value="<?php echo date("Y-m-t\T23:59:59"); ?>" readonly>
              </fieldset>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" id="changeValidBundle" class="btn btn-primary">
            <?php echo lang('Save Changes'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The CDR Modal -->
<div class="modal fade" id="Plt">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Platform Logs records'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="platformlog">
            <thead>
              <tr>
                <th>
                  <?php echo lang('Date'); ?>
                </th>
                <th>
                  <?php echo lang('Command'); ?>
                </th>
                <th>
                  <?php echo lang('Value'); ?>
                </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <?php echo lang('Close'); ?></button>
        <a class="btn btn-md btn-primary" href="<?php echo base_url(); ?>admin/subscription/export_livecdr/<?php echo trim($service->domain); ?>/<?php echo $service->id; ?>/<?php echo trim($mobile->msisdn_sn); ?>"><i
            class="fa fa-file-excel"></i>
          <?php echo lang('Export Excel'); ?></a>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function (data) {
      $('#cdr').DataTable({
        "autoWidth": false,
        "ajax": {
          "url": window.location.protocol + '//' + window.location.host + '/admin/table/get_cdr/<?php echo trim($service->id); ?>/<?php echo $service->companyid; ?>',
          "dataSrc": ""
        },
        "aaSorting": [
          [0, 'desc']
        ],
        "columns": [{
            "data": "Begintime"
          },
          {
            "data": "MaskDestination"
          },
          {
            "data": "DestinationCountry"
          },
          {
            "data": "DurationConnection"
          },
          {
            "data": "TrafficTypeId"
          },
          {
            "data": "CurNumUnitsUsed"
          },

        ],
        "language": {
          "url": window.location.protocol + '//' + window.location.host +
            "/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {

          if (aData["TrafficTypeId"] == "0") {
            $('td:eq(4)', nRow).html('-');
          }
          if (aData["TrafficTypeId"] == 1) {

            if(aData["TypeCallId"] == 2){
              $('td:eq(4)', nRow).html('<strong>Voice Incoming</strong>');
            }else{
              $('td:eq(4)', nRow).html('<strong>Voice</strong>');
            }
          } else if (aData["TrafficTypeId"] == 2) {
            $('td:eq(4)', nRow).html('<strong>Data</strong>');
          } else if (aData["TrafficTypeId"] == 5) {
            $('td:eq(4)', nRow).html('<strong>SMS</strong>');
          }
          return nRow;
        },
      });
         $('#cdrin').DataTable({
        "autoWidth": false,
        "ajax": {
          "url": window.location.protocol + '//' + window.location.host + '/admin/complete/get_cdr_in/<?php echo trim($service->id); ?>/<?php echo $service->companyid; ?>',
          "dataSrc": ""
        },
        "aaSorting": [
          [0, 'desc']
        ],
        "columns": [{
            "data": "Datetime"
          },
          {
            "data": "Destination"
          },
          {
            "data": "PinCode"
          },
          {
            "data": "Volume"
          },
          {
            "data": "TrafficType"
          },
          {
            "data": "Price"
          },

        ],
        "language": {
          "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
        /*"fnRowCallback": function (nRow, aData, iDisplayIndex) {

          if (aData["TrafficTypeId"] == "0") {
            $('td:eq(4)', nRow).html('-');
          }
          if (aData["TrafficTypeId"] == 1) {

            if(aData["TypeCallId"] == 2){
              $('td:eq(4)', nRow).html('<strong>Voice Incoming</strong>');
            }else{
              $('td:eq(4)', nRow).html('<strong>Voice</strong>');
            }
          } else if (aData["TrafficTypeId"] == 2) {
            $('td:eq(4)', nRow).html('<strong>Data</strong>');
          } else if (aData["TrafficTypeId"] == 5) {
            $('td:eq(4)', nRow).html('<strong>SMS</strong>');
          }
          return nRow;
        },
        */
      });
    });
  });
  $(document).ready(function () {

    var sel = $('#changecharge').find(":selected").val();
    if (sel == "YES") {
      $('#changeamount').show('slow');
    } else {
      $("#changeamount").hide('slow');
    }

    var sel = $('#swapcharge').find(":selected").val();

    if (sel == "YES") {
      $('#swapamount').show('slow');

    } else {
      $("#swapamount").hide('slow');

    }


    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function (data) {
      $('#platformlog').DataTable({
        "autoWidth": false,
        "ajax": {
          "url": window.location.protocol + '//' + window.location.host +
            '/admin/table/platform_log/<?php echo trim($mobile->msisdn_sn); ?>',
          "dataSrc": ""
        },
        "aaSorting": [
          [0, 'desc']
        ],
        "columns": [{
            "data": "Date"
          },
          {
            "data": "Command"
          },
          {
            "data": "Value"
          },
        ],
        "language": {
          "url": window.location.protocol + '//' + window.location.host +
            "/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
          if (aData["TrafficTypeId"] == "0") {
            $('td:eq(3)', nRow).html('-');
          }
          if (aData["TrafficTypeId"] == 1) {
            $('td:eq(4)', nRow).html('<strong>Voice</strong>');
          } else if (aData["TrafficTypeId"] == 2) {
            $('td:eq(4)', nRow).html('<strong>Data</strong>');
          } else if (aData["TrafficTypeId"] == 5) {
            $('td:eq(4)', nRow).html('<strong>SMS</strong>');
          }
          return nRow;
        },
      });
    });
  });
  $('#open_logs').click(function () {
    $('#Plt').modal('toggle');
  });
  $('#fixbundle2').click(function () {
    var id = '<?php echo $service->id; ?>';
    var r = confirm("Are you sure? this will add bundle according to the products include arta!");
    if (r == true) {
      window.location.href = window.location.protocol + '//' + window.location.host +
        "/admin/subscription/fixbundle/" + id + "/2";
    }
  });
  $('#fixbundle3').click(function () {
    var id = '<?php echo $service->id; ?>';
    var r = confirm("<?php echo lang('Are you sure? this will add bundle according to the products! exclude arta'); ?>");
    if (r == true) {
      window.location.href = window.location.protocol + '//' + window.location.host +
        "/admin/subscription/fixbundle/" + id + "/3";
    }
  });
  $('#fixbundlePorting').click(function () {
    var id = '<?php echo $service->id; ?>';
    var r = confirm("<?php echo lang('Are you sure? this will fix porting issue on magebo'); ?>!");
    if (r == true) {
      window.location.href = window.location.protocol + '//' + window.location.host +
        "/admin/subscription/fixbundlePorting/" + id;
    }
  });
$('#bundleid2').change(function () {
    var sel = $('#bundlechargeid2').find(":selected").val();
    var packageid = $('#bundleid2').find(":selected").val();

    if (sel == 1) {
      $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/complete/get_bundlePrice/' +
        packageid,
        dataType: 'json',
        success: function (data) {
          if (data.result) {
            $('#pricex').val(data.price);
          }
        }
      });

      $('.chargex').prop('required', true);
      $('.chargex').show();
    } else {
      $('#pricex').val('0.00');
      $('.chargex').prop('required', false);
      $('.chargex').hide();
    }


});

$('#btn_arta2').click(function(){
  $('#btn_arta2').prop('disabled', true);
  var t = confirm('Are you sure, this will create Bundle in Arta Platform');
  if(t){
    window.location.replace('<?php echo base_url(); ?>admin/subscription/fixbundle/<?php echo $this->uri->segment(4); ?>/2');
  }
});
$('#btn_arta3').click(function(){
  $('#btn_arta3').prop('disabled', true);
var t = confirm('Are you sure, this will create Bundle in Magebo Only');
if(t){
  window.location.replace('<?php echo base_url(); ?>admin/subscription/fixbundle/<?php echo $this->uri->segment(4); ?>/3');
}
});
  $("#bundlechargeid2").change(function () {
    var sel = $('#bundlechargeid2').find(":selected").val();
    var packageid = $('#bundleid2').find(":selected").val();
    //console.log(packageid);
    if (sel == 1) {
      $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/complete/get_bundlePrice/' +
          packageid,
        dataType: 'json',
        success: function (data) {
          if (data.result) {
            $('#pricex').val(data.price);
          }
        }
      });

      $('.chargex').prop('required', true);
      $('.chargex').show();
    } else {
      $('#pricex').val('0.00');
      $('.chargex').prop('required', false);
      $('.chargex').hide();
    }
  });


  $("#customertype1").change(function () {
    var sel = $('#customertype1').find(":selected").val();
    //console.log(sel);
    if (sel == "1") {
      $('#acct').show('slow');
      $('#AccountNumber').prop('required', true);
    } else {
      $("#acct").hide('slow');
      $('#AccountNumber').prop('required', false);
    }
  });

  $("#swapcharge").change(function () {
    var sel = $('#swapcharge').find(":selected").val();
    //console.log(sel);
    if (sel == "YES") {
      $('#swapamount').show('slow');

    } else {
      $("#swapamount").hide('slow');

    }
  });

  $("#changecharge").change(function () {
    var sel = $('#changecharge').find(":selected").val();
   // console.log(sel);
    if (sel == "YES") {
      $('#changeamount').show('slow');

    } else {
      $("#changeamount").hide('slow');

    }
  });



  $('.cancel').on('change', function () {
    var radioValue = $("input[name=date_cancellation]:checked").val();
    if (radioValue == "future") {
      $('.datefuture').show(200);
    } else {
      $('.datefuture').hide();
    }
  });
</script>
<script>
  $('#EnableAll').click(function () {
    $('#EnableAllSetting').modal('show');
  });
  $('#enableallsetting').click(function () {
    window.location.href = window.location.protocol + '//' + window.location.host +
      '/admin/subscription/enable_all_services/<?php echo trim($mobile->serviceid); ?>';
  });
  $('#AdvanceSet').click(function () {
    $.ajax({
      url: window.location.protocol + '//' + window.location.host +
        '/admin/complete/get_packages_detail_complete/<?php echo trim($mobile->msisdn_sn); ?>',
      dataType: 'json',
      success: function (data) {
        if (data.packages) {
          $('#AdvanceSetting').modal('show');
          if (data.packages.length > 1 && data.packages.length != 0) {
            $('#showAdvance').show();
            $('#showAdvance').html('');
            data.packages.forEach(function (b) {

              if (b.Available == "1") {
                var t = ' checked';
              } else {
                var t = '';
              }

              $('#showAdvance').append(' <tr> <td>' + b.CallModeDescription +
                '</td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="' +
                b.PackageDefinitionId + '" type="checkbox"' + t +
                '><label class="custom-switch-btn" for="' + b.PackageDefinitionId +
                '"></label> </div></td> </tr>');
            });
          } else {
            if (data.packages.Available == "1") {
              var t = ' checked';
            } else {
              var t = '';
            }
            $('#showAdvance').append(' <tr> <td>' + data.packages.CallModeDescription +
              '</td><td><div class="custom-switch custom-switch-label-onoff"><input class="custom-switch-input packageid" id="' +
              data.packages.PackageDefinitionId + '" type="checkbox"' + t +
              '><label class="custom-switch-btn" for="' + data.packages.PackageDefinitionId +
              '"></label> </div></td> </tr>');
          }
          $("#showAdvance").append(
            '<script> $(".packageid").change(function() { var userid = "<?php echo $service->userid; ?>"; var serviceid = "<?php echo $service->id; ?>"; var sn = "<?php echo $mobile->msisdn_sn; ?>"; console.log(sn); var id = $(this).attr("id"); var msisdn = "<?php echo trim($service->domain); ?>"; if($(this).is(":checked")) { var val = "1"; }else{ var val = "0"; } $.ajax({ url: window.location.protocol + "//" + window.location.host + "/admin/subscription/change_packagesetting", type: "post", dataType: "json", success: function (data) { console.log(data); }, data: {"sn": sn, "id": id, "val":val, "msisdn":msisdn, "userid":userid, "serviceid":serviceid} }); });<\/script>'
          );
        } else {
          alert('<?php echo lang('There is an error to complete your request'); ?>');
        }
      }
    });
  });
</script>
<?php //print_r($service);?>
<?php
    }?>
<script>
  $('#ReloadNow').click(function(){
    $('#loadingreload').show();
    $('#datareload').hide();
    $('#ReloadNow').prop('disabled', true);
    var ChargeReseller = $('#ChargeReseller').find(":selected").val();
     $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/subscription/reloadCredit/',
        dataType: 'json',
        type: 'post',
        data: {
          serviceid: $('#reload_serviceid').val(),
          credit: $('#reload_credit').val(),
          userid: $('#reload_userid').val(),
          reseller_charge: ChargeReseller,
        },
        success: function (data) {
          if (data.result) {
            console.log(data);
               $('#addReload').modal('toggle');
          window.location.href = window.location.protocol + '//' + window.location.host +'/admin/subscription/detail/<?php echo $service->id; ?>';

          }else{
            $('#ReloadNow').prop('disabled', false);
             $('#loadingreload').hide();
              $('#datareload').show();
            alert('<?php echo lang('there has been an error, please try again later'); ?>.');
          }
        }
      });



  })
  $('#changeValidBundle').click(function () {
    var str = $("#expBundle").serialize();
    $.ajax({
      url: window.location.protocol + '//' + window.location.host +
        '/admin/subscription/updatebundleValidity/<?php echo trim($mobile->msisdn_sn); ?>',
      dataType: 'json',
      type: 'POST',
      data: str,
      success: function (data) {
        //console.log(data);
        $('#changeValidBundle').prop('disabled', true);
        $('#ExpireBundleModal').modal('hide');
        window.location.href = window.location.protocol + '//' + window.location.host +
          '/admin/subscription/detail/<?php echo $mobile->serviceid; ?>';

      }
    });

  });
  function cancelChanges(serviceid){
var y = confirm('<?php echo lang('Are you sure?, this will delete the records of subscription changes'); ?>');
if(y){
  $.ajax({
url: '<?php echo base_url(); ?>admin/subscription/cancelUpgradeChanges/<?php echo $this->session->cid; ?>',
type: 'post',
dataType: 'json',
success: function (data) {
window.location.href= window.location.protocol + '//' + window.location.host +"/admin/subscription/detail/"+serviceid;
},
data: {serviceid:serviceid}
});
}

  }
  function SubmitPorting(){
    $('#portingbutton').prop('disabled', true);
    $('#portingbody').html('<img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100" class="text-center">');
    $("#portingdata").submit();
  }
  function ChangeContractTerm(){

    $('#ChangeContractTerm').modal('toggle');
  }
  function MoveServicesNow(){

    $('#MoveService').modal('toggle');

  }
  function changeExpireBundle(id) {
    $('#assignid').val(id);
    $('#ExpireBundleModal').modal('toggle');

  }

  $('#blockoriginating').click(function () {
    $('#loadingx40').show();
    console.log('block');
    var str = $("#blockout").serialize();
    $('#blockoriginating').prop('disabled', true);
    $.ajax({
      url: window.location.protocol + '//' + window.location.host +
        '/admin/subscription/block_mobile_originating/<?php echo trim($mobile->msisdn_sn); ?>',
      dataType: 'json',
      type: 'POST',
      data: str,
      success: function (data) {
        console.log(data);
        $('#BlockOutgoingModal').modal('toggle');
        window.location.href = window.location.protocol + '//' + window.location.host +
          '/admin/subscription/detail/' + data.serviceid;

      }
    });


  });

   $('#unblockoriginating').click(function () {
    $('#loadingx40').show();
    console.log('block');
    var str = $("#unblockout").serialize();
    $('#unblockoriginating').prop('disabled', true);
    $.ajax({
      url: window.location.protocol + '//' + window.location.host +
        '/admin/subscription/unblock_mobile_originating/<?php echo trim($mobile->msisdn_sn); ?>',
      dataType: 'json',
      type: 'POST',
      data: str,
      success: function (data) {
        console.log(data);
        $('#UnBlockOutgoingModal').modal('toggle');
      //  window.location.href = window.location.protocol + '//' + window.location.host +
       //   '/admin/subscription/detail/' + data.serviceid;

      }
    });


  });


</script>

<script>
  $(document).ready(function()
{
  <?php if ($service->orderstatus == "PortinAccepted") {
        ?>
  $.ajax({
  url: window.location.protocol + '//' + window.location.host + '/admin/complete/get_porting_status/<?php echo $this->uri->segment(4); ?>/<?php echo $this->session->cid; ?>',
  dataType: 'json',
  success: function (data) {
  if(data.result){
    console.log(data);
  $('#actiondatex').html(data.date);
  }
  }
  });
  <?php
    } ?>


$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
 $('#ticket_tables').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"pageLength": 5,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getclient_tickets/<?php echo $service->userid; ?>',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},

"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

$('td:eq(0)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/' + aData[6] + '">'+aData[0]+'</a>');
return nRow;
},

});

});
});

</script>