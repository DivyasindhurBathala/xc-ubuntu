<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			 <?php echo lang('Creditnote'); ?>
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				 <?php echo lang('Creditnote Details'); ?> #<?php echo $cn['creditnotenum']; ?> <div class="close">
					<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/creditnote/download/<?php echo $cn['id']; ?>" target="_blank"><i class="fa fa-download"></i>  <?php echo lang('Download Creditnote'); ?></a>
					<button class="btn btn-primary btn-sm" id="sendnotaid"  onclick="confirmation_send();"><i class="fa fa-envelope"></i>  <?php echo lang('Send Credinote'); ?></button>
				</div>
				</h5>
				<hr />
				<div class="table-responsive">
					<div class="row top-buffer">
						<div class="col-md-4">
							<p class="lead"> <?php echo lang('Company Information'); ?>:</p>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-borderless table-sm">
										<tbody>
											<tr>
												<td><?php echo lang('Companyname'); ?>:</td>
												<td class="text-right"><?php echo $setting->companyname; ?></td>
											</tr>
											<tr>
												<td><?php echo lang('Address'); ?></td>
												<td class="text-right"><?php echo $setting->address; ?></td>
											</tr>
											<tr>
												<td><?php echo lang('Phone'); ?>:</td>
												<td class="text-right"><?php echo $setting->phonenumber; ?></td>
											</tr>
											<tr>
												<td><?php echo lang('Email'); ?></td>
												<td class="text-right"><?php echo $setting->sender_email; ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
						</div>
						<div class="col-md-4">
							<p class="lead"><?php echo lang('Customer Information'); ?></p>
							<div class="row">
								<div class="col-md-12">
									<table class="table table-borderless table-sm">
										<tbody>
											<tr>
												<td><i class="fa fa-bank"></i> <?php echo lang('Companyname'); ?>:</td>
												<td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->companyname; ?></a></td>
											</tr>
											<tr>
												<td><i class="fa fa-user"></i> <?php echo lang('Customer name'); ?>:</td>
												<td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->firstname . ' ' . $client->lastname; ?></a></td>
											</tr>
											<tr>
												<td><i class="fa fa-address-card"></i> <?php echo lang('Adress'); ?>:</td>
												<td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->address1 . ' <br />' . $client->postcode . ' ' . $client->city; ?></a></td>
											</tr>
											<tr>
												<td><i class="fa fa-certificate"></i> <?php echo lang('BTW'); ?></td>
												<td class="text-right"><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->vat; ?></a></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="row top-buffer">
						<div class="table-responsive col-sm-12">
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th><?php echo lang('Item & Description'); ?></th>
										<th class="text-right"><?php echo lang('Invoicenum'); ?></th>
										<th class="text-right"><?php echo lang('Subtotal'); ?></th>
										<th class="text-right"><?php echo lang('TaxRate(Amount@%)'); ?></th>
										<th class="text-right"><?php echo lang('Total'); ?></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th scope="row">1</th>
										<td>
											<?php echo nl2br($cn['notes']); ?>
										</td>
										<td class="text-right"><?php echo $cn['invoicenum']; ?></td>
										<td class="text-right">&euro;<?php echo $cn['subtotal']; ?></td>
										<td class="text-right">&euro;<?php echo $cn['tax']; ?></td>
										<td class="text-right">&euro;<?php echo $cn['total']; ?></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="row top-buffer">
						<div class="col-md-9">
						</div>
						<div class="col-md-3">
							<p class="lead"><?php echo lang('Total Creditnote'); ?></p>
							<div class="table-responsive">
								<table class="table">
									<tbody>
										<tr>
											<td><?php echo lang('Sub Total'); ?></td>
											<td class="text-right">&euro;<?php echo $cn['subtotal']; ?></td>
										</tr>
										<tr>
											<td><?php echo lang('Btw'); ?></td>
											<td class="text-right">&euro;<?php echo $cn['tax']; ?></td>
										</tr>
										<tr>
											<td class="text-bold-800"><?php echo lang('Total'); ?></td>
											<td class="text-bold-800 text-right"> &euro;<?php echo $cn['total']; ?></td>
										</tr>
										<tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<form>
		<input type="hidden" name="notaid" id="notaid" value="<?php echo $cn['id']; ?>">
	</form>
	<div id="fade"></div>
	<div id="modal"  class="modal">
		<img id="loader" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" />
	</div>
<script type="text/javascript">
		$("#addpayment").click(function() {
		$('#addcreditblock').hide("slow");
		$('#addpaymentblock').show("slow");
		});
		$("#addpaymenthide").click(function() {
		$('#addpaymentblock').hide("slow");
		});
		$("#addcredit").click(function() {
		$('#addpaymentblock').hide("slow");
		$('#addcreditblock').show("slow");
		});
		$("#addcredithide").click(function() {
		$('#addcreditblock').hide("slow");
		});
</script>
<script type="text/javascript">
		function confirmation_send() {
    $("#sendnotaid").prop('disabled', true);
    var answer = confirm("<?php echo lang('Do you wish to send this creditnote to customer Email?'); ?>")
    if (answer) {
        var notaid = $("#notaid").val();
        openModal();
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/creditnote/send',
            type: 'post',
            dataType: 'json',
            data: {
                notaid: notaid
            },
            success: function(data) {
                console.log(data);
                if (data.result) {
                    window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/creditnote/detail/' + notaid + '/success_send');
                }
                closeModal();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("Error  accour while sending your email");
                closeModal();
            }
        });
    } else {
        console.log("hello ");
        $("#sendnotaid").prop('disabled', false);
    }
}

function openModal() {
    document.getElementById('modal').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
}

function closeModal() {
    document.getElementById('modal').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
}
		</script>