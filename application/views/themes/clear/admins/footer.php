</div>
</div>
<div class="display-type" id="copyright">
</div>
</div>
<footer class="fixed-bottom" id="footers">
<div class="footerex">
<div class="container-fluid">

<span class="text-info"> UnitedPortal V<?php echo PT_VERSION; ?> <a class="text-info" href="<?php echo base_url(); ?>Changeslog.txt" target="_blank"> | ChangeLogs</a> | <a class="text-info" href="<?php echo base_url(); ?>docs" target="_blank"> Documentation </a> | <?php echo gethostname(); ?> </span>

</div>
</div>
</footer>
<form>
<input type="hidden" id="myname" value="<?php echo $_SESSION['firstname'] . ' ' . substr(trim($_SESSION['lastname']), 0, 1) . '.'; ?>">
<input type="hidden" id="myid" value="<?php echo $_SESSION['id']; ?>">
<input type="hidden" id="yourid" value="">
<input type="hidden" id="yourname" value="">
<input type="hidden" id="mypic" value="<?php echo $_SESSION['picture']; ?>">
<input type="hidden" id="cidx" value="<?php echo $_SESSION['cid']; ?>">
</form>
<script
src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/popper.js/dist/umd/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/chart.js/dist/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap-validator/dist/validator.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/dropzone/dist/dropzone.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/editable-table/mindmup-editabletable.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/tether/dist/js/tether.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/slick-carousel/slick/slick.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/util.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/alert.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/button.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/carousel.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/collapse.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/dropdown.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/modal.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/tab.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/tooltip.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/bootstrap/js/dist/popover.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/js/demo_customizer.js?version=4.3.0"></script>
<script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/js/main.js?version=4.3.20"></script>
<?php if ($this->uri->segment(3) == "product_edit") {
    ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
<?php
}?>
<?php if (!empty($dtt)) {
        ?>
    <?php echo dth(); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/js/datatables/admin/<?php echo $dtt; ?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
<?php
    }?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/js/autocomplete.js?version=3.0"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/js/gauge.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/js/datepickers.js?version=5.2"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/clear/js/chat.js?version=1.16"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/clear/js/sound.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/clear/js/emojionearea.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/clear/js/jquery.bootstrap-growl.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/clear/js/jquery.dataTables.yadcf.js"></script>
<script>
$('.showprogress').on('click', function (e) {
    loadingScreen(2000);
});
function showrotating(){


        $('#loadingController').modal({
                backdrop: 'static',
        keyboard: false});

}
  function loadingScreen(responseTime) {
        $('#loaders').hide();
        $('#footers').hide();
        $('#load').show();
        setTimeout(function() {
            $('#load').hide();
               $('#loaders').show();
                $('#footers').show();
        }, responseTime);
    }
</script>
<div class="modal modali" id="loadingController">
  <div class="modal-dialog modal-dialogi" role="document">
    <div class="modal-content">
      <div class="modal-header">
      
      </div>
      <div class="modal-body">
        <center><img src="<?php echo base_url(); ?>assets/img/loader1.gif" height="100"></center>
        <span><h5 class="loading-text"><?php echo lang('Please wait while we loading all tables for you'); ?></h5></span>
      </div>
      </div>
      </div>
      </div>
</body>
</html>