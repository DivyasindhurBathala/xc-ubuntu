<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Company List'); ?>
        <div class="float-right">
          <!-- <a class="btn btn-rounded btn-success" id="agentexport" href="<?php echo base_url(); ?>admin/agent/export_agents"><i
            class="fa fa-file-excel"></i>
          <?php echo lang('Export Agents'); ?></a> -->
          <button class="btn btn-rounded btn-success" id="newagent" data-toggle="modal" data-target="#addAgent"><i
              class="fa fa-plus-circle"></i>
            <?php echo lang('Add New Company'); ?></button>
        </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
          <?php echo lang('Company Hierarchy'); ?>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="clients">
            <thead>
              <tr>
                <th>
                  <?php echo lang('#CompanyId'); ?>
                </th>
                <th>
                  <?php echo lang('CompanyName'); ?>
                </th>
                <th>
                  <?php echo lang('Parent CompanyId'); ?>
                </th>
               
              </tr>
            </thead>
            <tbody>

            <?php
            foreach($data as $row)
            {
            echo "<tr>";
            echo "<td>".$row->companyid."</td>";
            echo "<td>".$row->companyname."</td>";
            echo "<td>".$row->parentcompanyid."</td>";
            echo "</tr>";
            $i++;
            }
            ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="addAgent" tabindex="-1" role="dialog" aria-labelledby="addAgent" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/company/add">
        <input type="hidden" name="company_id_later"
          value="<?php echo $this->session->cid; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Enter Company Details'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-sm-6">
              <label for="company_id"><?php echo lang('CompanyId'); ?></label>
              <input name="company_id" type="text" class="form-control" id="companyname">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-sm-5">
              <label for="company_name"><?php echo lang('CompanyName'); ?></label>
              <input name="company_name" type="text" class="form-control" id="email">
            </div>
          </div>

          <div class="row">
            <div class="form-group col-sm-6">
              <label for="parent_companyid"><?php echo lang('Parent CompanyId'); ?></label>
              <input name="parent_companyid" type="text" class="form-control" id="text">
            </div>
          </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Submit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>