<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('System Logs'); ?> 
       
      </h6>
      <div class="element-box">
        <h5 class="form-header">
    
        </h5>
       
         <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="clients">
            <thead>
              <tr>
                <th width="15%"><?php echo lang('#Date'); ?></th>
                <th width="15%"><?php echo lang('Client'); ?></th>
                <th width="40%"><?php echo lang('Description'); ?></th>
                <th width="15%"><?php echo lang('IP'); ?></th>
                <th width="15%"><?php echo lang('User'); ?></th>
               
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
 $('#clients').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"pageLength": 10,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getcompany_logs',
"aaSorting": [[0, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData[5] >0){
    $('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[5] + '"><i class="fa fa-cubes"></i> '+aData[2]+'</a>');  
}
$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><i class="fa fa-user"></i> '+aData[1]+'</a>');
$('td:eq(3)', nRow).html('<a href="http://whois.domaintools.com/' + aData[3] + '" target="_blank">'+aData[3]+'</a>');

return nRow;
},
});
});
</script>