<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Quotes
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo ('Create New Quote'); ?>
        </h5>
        <div class="table-responsive">
          <form method="post" action="<?php echo base_url(); ?>admin/quote/create/">
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="tax"><?php echo lang('Subject'); ?></label>
                    <input type="text"  id="subject" name="subject" class="form-control" required>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tax"><?php echo lang('Enter Customer ID'); ?></label>
                    <input type="text"  id="userid" name="userid" value="<?php if (!empty($this->uri->segment(4))) {echo $this->uri->segment(4);}?>"class="form-control ui-autocomplete-input ui-autocomplete-loading clientquote" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tax"><?php echo lang('Customer'); ?></label>
                    <input type="text" value="<?php if (!empty($this->uri->segment(4))) {echo $client->firstname . ' ' . $client->lastname;}?>" name="duedate" class="form-control is-valid" id="customername" <?php if (!empty($this->uri->segment(4))) {?>readonly<?php }?> required>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tax"><?php echo ('Choose VAT Percentage'); ?></label>
                    <select class="form-control" id="tax" name="taxrate">
                      <option value="0">0%</option>
                      <option value="6">6%</option>
                      <option value="21">21%</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tax"><?php echo ('Valid Until'); ?></label>
                    <input type="text" value="<?php echo date('Y-m-d'); ?>" name="duedate" class="form-control is-valid" id="pickdate">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-6">
                  <label class="form-control-label" for="inputSuccess1"><strong><?php echo ('Description'); ?></strong></label>
                  <textarea class="form-control ui-autocomplete-input ui-autocomplete-loading products" autocomplete="off" id="description0" style="margin-top: 0px; margin-bottom: 0px; height: 36px;" name="line[0][description]"></textarea>
                </div>
                <div class="col-sm-2">
                  <label class="form-control-label" for="inputSuccess1"><?php echo ('Qty'); ?></label>
                  <input type="text" value="1" name="line[0][qty]" class="form-control is-valid" id="qty0">
                </div>
                <div class="col-sm-2">
                  <label class="form-control-label" for="inputSuccess1"><?php echo ('Price / pcs'); ?></label>
                  <input type="text" value="" name="line[0][pcs]" class="form-control is-valid" id="pcs0">
                </div>
                <div class="col-sm-2">
                  <label class="form-control-label" for="inputSuccess1"><?php echo ('Total'); ?></label>
                  <input type="text" value="" name="line[0][total]" class="form-control is-valid" id="total0">
                </div>
              </div>
            </div>
            <div class="form-group has-success field">
              <div class="row">
                <div class="col-sm-6">
                  <textarea id="description1" style="margin-top: 0px; margin-bottom: 0px; height: 36px;" class="products form-control ui-autocomplete-input ui-autocomplete-loading" autocomplete="off" name="line[1][description]"></textarea>
                </div>
                <div class="col-sm-2">
                  <input type="text" value="1" name="line[1][qty]"  class="form-control is-valid" id="qty1">
                </div>
                <div class="col-sm-2">
                  <input type="text" value="" name="line[1][pcs]"  class="form-control is-valid" id="pcs1">
                </div>
                <div class="col-sm-2">
                  <input type="text" value=""  name="line[1][total]" class="form-control is-valid" id="total1">
                </div>
              </div>
            </div>
            <div class="form-group has-success field">
              <div class="row">
                <div class="col-sm-12">
                  <button type="submit" class="btn btn-primary btn-block btn-lg">Save Changes</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
$('input').on('change', function () {
var id = $(this).attr('id');
var nomor = id.replace(/\D/g,'');
var qty = $('#qty'+nomor).val();
var price = $('#pcs'+nomor).val();
var tax = $( "#tax option:selected" ).val();
var total = qty*price;
$( "#total"+nomor ).val(total);
console.log(qty+' '+price);
});
$( "#tax" ).change(function() {
});
});
</script>