<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Services<div class=float-right>
          <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/subscription/add_mobile"><i class="fa fa-plus-circle"></i> <?php echo lang('Add Subscription'); ?><?php //echo lang('Add Subscription'); ?></a>
            <?php if ($this->session->role == "superadmin") {?>
          <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/subscription/export_subscription"><i class="fa fa-file-excel"></i> <?php echo lang('Export Subscription'); ?><?php //echo lang('Add Subscription'); ?></a>
            <?php }?>
         </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Subscription List'); ?>
        </h5>
        <div class="table-responsive">
            <table class="table table-striped table-lightfont" id="services">
              <thead>
                <tr>
                  <th><?php echo lang('ID#'); ?></th>
                  <th><?php echo lang('Package'); ?></th>
                  <th><?php echo lang('Customer'); ?></th>
                  <th><?php echo lang('Status'); ?></th>
                  <th><?php echo lang('Recurring'); ?></th>
                  <th><?php echo lang('Identity'); ?></th>
                  <th><?php echo lang('Date Contract'); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>




      </div>
    </div>
  </div>
</div>



<!-- The Suspend Modal -->
<div class="modal fade" id="NewModal">

  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/add">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Choose Type</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <div class="row">
          <div class="col-md-6">
        <a class="btn btn-lg btn-block btn-md btn-secondary" href="<?php echo base_url(); ?>admin/subscription/add_internet"><i class="fa fa-laptop fa-5x"></i><br/>Internet</a>
      </div>
        <div class="col-md-6">
        <a class="btn btn-lg btn-block btn-md btn-primary" href="<?php echo base_url(); ?>admin/subscription/add_mobile"><i class="fa fa-mobile fa-5x"></i><br/>Mobile</a>
      </div>

        </div>
      </div>
      <div class="modal-footer">

      </div>
    </form>
    </div>
  </div>
</div>


<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
    <?php if (!empty($this->uri->segment(3))) {?>
    var status = '<?php echo $this->uri->segment(3); ?>';
    <?php } else {?>
    var status = '';
    <?php }?>
$('#services').DataTable({
    "autoWidth": false,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": true,
    "pageLength": 50,
    "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getservices/'+status,
    "aaSorting": [[0, 'desc']],

    "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

      $('td:eq(2)', nRow).html('<a  class="showprogress" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[9] + '">' + aData[2] + '</a>');
      $('td:eq(0)', nRow).html('<a  class="showprogress" href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[8] + '">' + aData[0] + '</a>');
      $('td:eq(1)', nRow).html('<a  class="showprogress" href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[8] + '">' + aData[1] + '</a>');
      $('td:eq(5)', nRow).html('<a  class="showprogress" href="'+window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/' + aData[8] + '">' + aData[5] + '</a>');
              return nRow;
        },
  });
});
});



</script>

