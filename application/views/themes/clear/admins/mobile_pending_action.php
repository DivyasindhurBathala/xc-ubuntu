<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Mobile Subscription Requests'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Pending Port In Requests'); ?>
        </h5>
        <div class="table-responsive">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped table-bordered" id="cdr">
                <thead>
                  <tr class="active">
                    <th><?php echo lang('Operation Type'); ?></th>
                    <th><?php echo lang('RequestType'); ?></th>
                    <th><?php echo lang('DonorOperator'); ?></th>
                    <th><?php echo lang('MSISDN'); ?></th>
                    <th><?php echo lang('ActionDate'); ?></th>
                    <th><?php echo lang('CustomerName'); ?></th>
                    <th><?php echo lang('Status'); ?></th>
                    <th><?php echo lang('button'); ?></th>
                  </tr>
                </thead>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
function OpenAccept(msisdn, sn){
$("#numberaccept").html('<?php echo lang('Accept'); ?> :'+msisdn);
$("#accept_number").val(msisdn);
$("#accept_sn").val(sn);
$('#PortExecute').modal({
show: false
});
$('#PortReject').modal({
show: false
});
$('#PortAccept').modal({
show: true
});
$('#PortCancel').modal({
show: false
});
}
function OpenReject(msisdn){
$("#numberreject").html('<?php echo lang('Reject'); ?> :'+msisdn);
$("#reject_number").val(msisdn);
$('#PortAccept').modal({
show: false
});
$('#PortExecute').modal({
show: false
});
$('#PortReject').modal({
show: true
});
$('#PortCancel').modal({
show: false
});
}


function OpenCancel(sn){
  console.log(sn);
$("#number_cancel").html('<?php echo lang('Cancel MSISDN:'); ?> :'+sn);
$("#cancel_number").val(sn);
$('#PortAccept').modal({
show: false
});
$('#PortExecute').modal({
show: false
});
$('#PortReject').modal({
show: false
});
$('#PortCancel').modal({
show: true
});
}


function OpenExecute(msisdn){


window.location.href= '<?php echo base_url(); ?>admin/subscription/open_reject/'+msisdn;

}
</script>
<div class="modal fade" id="PortAccept">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/accept_portout/">
        <input type="hidden" name="msisdn" id="accept_number" value="">
        <input type="hidden" name="SN" id="accept_sn" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="numberaccept"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
            <?php echo lang('Do you wish to accept this port?'); ?>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Accept'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="PortReject">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/reject_portout/">
        <input type="hidden" name="msisdn" id="reject_number" value="">
        <input type="hidden" name="SN" id="reject_sn" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="numberreject"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger"><?php echo lang('Reject'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="PortExecute">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/execute_portin/">
        <input type="hidden" name="msisdn" id="execute_number" value="">
        <input type="hidden" name="SN" id="execute_sn" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="numberexecute"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger"><?php echo lang('Execute'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="PortCancel">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/cancel_portin/">

        <input type="hidden" name="MSISDN" id="cancel_number" value="">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><div id="number_cancel"></div></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="table-responsive">
            <?php echo lang('Do you wish to cancel this porting request?'); ?>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Cancel Porting Request'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#cdr').DataTable({
"autoWidth": false,
"ajax": {
"url": window.location.protocol + '//' + window.location.host + '/admin/subscription/get_pending',
"dataSrc": ""
},
"pageLength": 50,
"aaSorting": [[0, 'desc']],
"columns": [
  { mData: 'OperationType' } ,
              { mData: 'RequestType' },
              { mData: 'DonorOperator' },
              { mData: 'MSISDN' },
              { mData: 'ActionDate' },
              { mData: 'CustomerName' },
              { mData: 'Status' },
              { mData: 'button' }
],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData["TrafficTypeId"] == "0"){
$('td:eq(3)', nRow).html('-');
}
if(aData["TrafficTypeId"] == 1){
$('td:eq(4)', nRow).html('<strong>Voice</strong>');
}else if(aData["TrafficTypeId"] == 2){
$('td:eq(4)', nRow).html('<strong>Data</strong>');
}else if(aData["TrafficTypeId"] == 5){
$('td:eq(4)', nRow).html('<strong>SMS</strong>');
}
return nRow;
},
});
});
});
</script>
