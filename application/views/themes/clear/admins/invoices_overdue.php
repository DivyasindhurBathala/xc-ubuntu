<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Invoices'); ?>
            <div class="close">
                <!--
                <button class="btn btn-primary btn-rounded" data-target=".bd-exportpdf-modal-lg" data-toggle="modal"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>  <?php echo lang('Export Invoices'); ?> (pdf) </button>
                <button class="btn btn-rounded btn-success" data-target=".bd-export-modal-lg" data-toggle="modal"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                <?php echo lang('Export Invoices (excel)'); ?> </button>
                -->
                <div class="float-right">
                <?php if (in_array($this->session->role, array("superadmin","finance"))) { ?>
                    <button class="btn btn-rounded btn-secondary" onclick="export_invoices()"><i class="fa fa-plus-circle"></i>  <?php echo lang('Export Invoices'); ?></button> 
                     <a href="<?php echo base_url();?>admin/invoice" class="btn btn-rounded btn-secondary"><i class="fa fa-plus-circle"></i>  <?php echo lang('Show All Invoices'); ?></a> 
                <?php } ?>
              
                 </div>
            </div>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                <?php echo lang('List Invoices'); ?>
                </h5>
                <div class="table-responsive">

                    <table class="table table-striped table-lightfont table-hover" id="invoices">
                         <tfoot>
            <tr>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
                 <th>Salary</th>
            </tr>
        </tfoot>
                        <thead>
                            <th> <div align="left"><?php echo lang('Invoice Number'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Billing ID'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Invoice Date'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Invoice Duedate'); ?> </div></th>
                            <th> <div align="right"><?php echo lang('Amount'); ?> </div></th>
                            <th> <div align="right"><?php echo lang('Status'); ?> </div></th>
                        </thead>

                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#invoices').DataTable({

   "initComplete": function () {
        this.api().columns().eq(0).each( function (index) {
            const column = this.column(index);
            const title = $(column.header()).text();
            if(index === 1 || index === 2 || index === 5){
                var select = $(`
                    <select class="form-control">
                        <option value="">Please choose</option>
                    </select>
                `)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val ? '^'+val+'$' : '', true, false )
                        .draw();
                });
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                });
            }else{
                var input = $(`
                    <input class="form-control" type="text" placeholder="Search ${title}" />
                `)
                    .appendTo( $(column.footer()).empty() )
                    .on( 'keyup change', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());
                    column
                        .search( val )
                        .draw();
                });
            }
        });
    },
   "autoWidth": false,
    "ajax": {
    "url": window.location.protocol + '//' + window.location.host + '/admin/table/getinvoices/<?php echo $this->session->cid; ?>/overdue',
    "dataSrc": ""
      },
    "aaSorting": [[0, 'desc']],
    "columns": [
            { "data": "iInvoiceNbr" },
            { "data": "cName" },
            { "data": "dInvoiceDate" },
            { "data": "dInvoiceDueDate" },
            { "data": "mInvoiceAmount" },
            { "data": "iInvoiceStatus" }
        ],
    "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

      $('td:eq(4)', nRow).html('€'+ aData.mInvoiceAmount);
      //$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData.iAddressNbr + '/'+aData.iInvoiceNbr+'">' + aData.cName + '</a>');
      $('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData.iInvoiceNbr + '/'+aData.iAddressNbr+'">' + aData.iInvoiceNbr + '</a>');
      $('td:eq(3)', nRow).html(aData.dInvoiceDueDate.slice(0, 10));
      $('td:eq(2)', nRow).html(aData.dInvoiceDate.slice(0, 10));
        if(aData.iInvoiceStatus == "Paid"){

            $('td:eq(5)', nRow).html('<strong class="text-success">'+aData.iInvoiceStatus+'</strong>');
        }else if(aData.iInvoiceStatus == "Unpaid"){
            $('td:eq(5)', nRow).html('<strong class="text-danger">'+aData.iInvoiceStatus+'</strong>');
        }else{
            $('td:eq(5)', nRow).html('<strong class="text-warning">'+aData.iInvoiceStatus+'</strong>');
        }
              return nRow;
        },



  });
});
});



</script>

<script>
function export_invoices(){
window.location.href = window.location.protocol + '//' + window.location.host +'/admin/invoice/export_invoices';
    
}
</script>