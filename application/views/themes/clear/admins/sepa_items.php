<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Bank File List'); ?> 
      </h6>
      <div class="element-box">
        <h5 class="form-header">
            <?php echo lang('Daily Files downloaded from FTP'); ?>
        </h5>
         <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="sepa">
            <thead>
              <tr>
                <th><?php echo lang('Date'); ?></th>
                <th><?php echo lang('Amount'); ?></th>
                <th><?php echo lang('End2endId'); ?></th>
                <th><?php echo lang('Mandateid'); ?></th>
                <th><?php echo lang('BatchPaymentid'); ?></th>
                <th><?php echo lang('Status'); ?></th>
                <th><?php echo lang('Date'); ?></th>
              <th></th>
               
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#sepa').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_sepa_items/<?php echo $this->session->cid; ?>/<?php echo $this->uri->segment(4); ?>',
"aaSorting": [[6, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

//$('td:eq(1)', nRow).html('€'+ aData[1]);
//$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData.iAddressNbr + '/'+aData.iInvoiceNbr+'">' + aData.cName + '</a>');
if(aData[5] == "Pending"){
$('td:eq(7)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/sepa/process_item/' + aData[7] + '"><i class="fa fa-cog"></i> Process</a>');
}
// $('td:eq(2)', nRow).html(aData.dInvoiceDate.slice(0, 10));
        return nRow;
  },
});

});
});

</script>