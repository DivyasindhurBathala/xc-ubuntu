<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Client Invoice Creation'); ?>:  <a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>"><?php echo $client->firstname . ' ' . $client->lastname . ' - ' . $client->companyname; ?></a>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        </h5>
        <div class="table-responsive">
          <form method="post" action="<?php echo base_url(); ?>admin/invoice/create/<?php echo $client->id; ?>">
            <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tax"><?php echo lang('Choose VAT Percentage'); ?></label>
                    <select class="form-control" id="tax" name="taxrate">
                      <option value="0" selected>0%</option>
                      <option value="6">6%</option>
                      <option value="21">21%</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="tax"><?php echo lang('Pick Duedate'); ?> <?php if (!empty($client->duedays)) {?> (this customer has <?php echo $client->duedays; ?> <?php echo lang('days duedate set in his profile for duedate'); ?><?php }?></label>
                    <input type="text" value="<?php echo $date; ?>" name="duedate" class="form-control is-valid" id="pickdate" value="<?php echo date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $client->duedays . ' days')); ?>">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group has-success">
              <div class="row">
                <div class="col-sm-6">
                  <label class="form-control-label" for="inputSuccess1"><strong><?php echo lang('Description'); ?></strong></label>
                  <textarea class="form-control ui-autocomplete-input ui-autocomplete-loading products" autocomplete="off" id="description0" style="margin-top: 0px; margin-bottom: 0px; height: 36px;" name="line[0][description]"></textarea>
                </div>
                <div class="col-sm-2">
                  <label class="form-control-label" for="inputSuccess1"><?php echo lang('Qty'); ?></label>
                  <input type="text" value="1" name="line[0][qty]" class="form-control is-valid" id="qty0">
                </div>
                <div class="col-sm-2">
                  <label class="form-control-label" for="inputSuccess1"><?php echo lang('Price / pcs'); ?></label>
                  <input type="text" value="" name="line[0][pcs]" class="form-control is-valid" id="pcs0">
                </div>
                <div class="col-sm-2">
                  <label class="form-control-label" for="inputSuccess1"><?php echo lang('Total'); ?></label>
                  <input type="text" value="" name="line[0][total]" class="form-control is-valid" id="total0">
                </div>
              </div>
            </div>
            <div class="form-group has-success field">
              <div class="row">
                <div class="col-sm-6">
                  <textarea id="description1" style="margin-top: 0px; margin-bottom: 0px; height: 36px;" class="products form-control ui-autocomplete-input ui-autocomplete-loading" autocomplete="off" name="line[1][description]"></textarea>
                </div>
                <div class="col-sm-2">
                  <input type="text" value="1" name="line[1][qty]"  class="form-control is-valid" id="qty1">
                </div>
                <div class="col-sm-2">
                  <input type="text" value="" name="line[1][pcs]"  class="form-control is-valid" id="pcs1">
                </div>
                <div class="col-sm-2">
                  <input type="text" value=""  name="line[1][total]" class="form-control is-valid" id="total1">
                </div>
              </div>
            </div>
            <div class="form-group has-success field">
              <div class="row">
                <div class="col-sm-12">
                  <button type="submit" class="btn btn-primary btn-block"><?php echo lang('Save Changes'); ?></button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!--
vat = 21.5;
//The price, excluding VAT.
$priceExcludingVat = 10;
//Calculate how much VAT needs to be paid.
$vatToPay = ($priceExcludingVat / 100) * $vat;
//The total price, including VAT.
$totalPrice = $priceExcludingVat + $vatToPay;
//Print out the final price, with VAT added.
//Format it to two decimal places with number_format.
echo number_format($totalPrice, 2);
-->
<script type="text/javascript">
$( document ).ready(function() {
$('input').on('change', function () {
var id = $(this).attr('id');
var nomor = id.replace(/\D/g,'');
var qty = $('#qty'+nomor).val();
var price = $('#pcs'+nomor).val();
var tax = $( "#tax option:selected" ).val();
var total = qty*price;
$( "#total"+nomor ).val(total);
console.log(qty+' '+price);
});
$( "#tax" ).change(function() {
});
});
</script>