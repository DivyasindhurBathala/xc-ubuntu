<?php //print_r($client->stats);?>

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
                <?php echo $title; ?>
            </h6>
            <div class="element-box">
                <div class="table-responsive">
                    <ul id="tabsJustified" class="nav nav-tabs">
                        <li class="nav-item"><a href="#sum" data-target="#sum" data-toggle="tab"
                                class="nav-link small text-uppercase active"><i class="icon-feather-aperture"></i>
                                <?php echo lang('Summary'); ?></a></li>
                        <li class="nav-item"><a href="#cservices" data-target="#cservices" data-toggle="tab"
                                class="nav-link small text-uppercase"><i class="fa fa-cube"></i>
                                <?php echo lang('Subscriptions'); ?></a></li>
                        <?php if ($setting->mage_invoicing) {
    ?>
                        <li class="nav-item"><a href="#factuur" data-target="#factuur" data-toggle="tab"
                                class="nav-link small text-uppercase"><i class="fa fa-credit-card"></i>
                                <?php echo lang('Invoices'); ?></a></li>
                        <!-- <li class="nav-item"><a href="#creditnotes" data-target="#creditnotes" data-toggle="tab" class="nav-link small text-uppercase"><i class="fa fa-file"></i> <?php echo lang('Creditnotes'); ?></a></li> -->
                        <li class="nav-item"><a href="#payments" data-target="#payments" data-toggle="tab"
                                class="nav-link small text-uppercase"><i class="fa fa-tasks"></i>
                                <?php echo lang('Payments'); ?></a></li>
                        <?php
}?>
                        <?php if ($setting->whmcs_ticket) {
        ?>
                        <li class="nav-item"><a href="#helpdesk" data-target="#helpdesk" data-toggle="tab"
                                class="nav-link small text-uppercase"><i class="fa fa-life-ring"></i>
                                <?php echo lang('Tickets'); ?></a></li>
                        <?php
    }?>
                        <li class="nav-item"><a href="#contactTabs" data-target="#contactTabs" data-toggle="tab"
                                class="nav-link small text-uppercase"><i class="fa fa-user"></i>
                                <?php echo lang('Contacts'); ?></a></li>
                        <li class="nav-item"><a href="#logs" data-target="#logs" data-toggle="tab"
                                class="nav-link small text-uppercase"><i class="fa fa-list-alt"></i>
                                <?php echo lang('Logs'); ?></a></li>
                        <li class="nav-item"><a href="#notes" data-target="#notes" data-toggle="tab"
                                class="nav-link small text-uppercase"><i class="icon-feather-message-square"></i>
                                <?php echo lang('Notes'); ?></a></li>
                        <!--
            <li class="nav-item"><a href="#documents" data-target="#documents" data-toggle="tab" class="nav-link small text-uppercase"><i class="icon-feather-message-square"></i> <?php echo lang('Documents'); ?></a></li>
          -->
                        <li class="nav-item"><a href="#emails" data-target="#emails" data-toggle="tab"
                                class="nav-link small text-uppercase"><i class="fa fa-envelope"></i>
                                <?php echo lang('Email Log'); ?></a></li>
                    </ul>
                    <br>
                    <div id="tabsJustifiedContent" class="tab-content">
                        <div id="sum" class="tab-pane fade  active show">
                            <div class="card-header"></div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <!-- Statistics -->
                                        <div class="col-lg-4">
                                            <div class="card">

                                                <div class="card-header d-flex align-items-center">
                                                    <h3 class="h4"><i class="fa fa-user"></i>
                                                        #<?php echo getDeltaId($this->uri->segment(4)); ?></h3>
                                                </div>
                                                <div class="card-body">

                                                    <table class="table table-striped table-hover">
                                                        <?php if ($client->companyname) {
        ?>
                                                        <tr>
                                                            <td><?php echo lang('Companyname'); ?></td>
                                                            <td class="text-right"><?php echo $client->companyname; ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo lang('Btw'); ?></td>
                                                            <td class="text-right"><?php echo $client->vat; ?></td>
                                                        </tr>
                                                        <?php
    }?>
                                                        <tr>
                                                            <td><?php echo lang('Contactname'); ?></td>
                                                            <td class="text-right">
                                                                <?php echo $client->firstname . ' ' . $client->lastname; ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo lang('Address'); ?></td>
                                                            <td class="text-right">
                                                                <?php echo trim($client->address1." ".$client->housenumber ." ". $client->alphabet); ?>
                                                                <br /><?php echo $client->postcode . ' ' . $client->city; ?>
                                                            </td>
                                                        </tr>
                                                        <?php if ($setting->mage_invoicing) {
        ?>
                                                        <tr>
                                                            <td><?php echo lang('Paymentmethod'); ?></td>
                                                            <td class="text-right">
                                                                <?php  if ($client->paymentmethod == "directdebit") {
            ?><img src="<?php echo base_url(); ?>assets/img/DD.png" height="20">
                                                                <?php
        } elseif ($client->paymentmethod=="others") {
            ?>

                                                                <img src="<?php echo base_url(); ?>assets/img/others.png"
                                                                    height="20">
                                                                <?php
        } else {
            ?><img src="<?php echo base_url(); ?>assets/img/BT.png" height="20"><?php
        } ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo lang('Payment Terms'); ?></td>
                                                            <td class="text-right">
                                                                <?php echo ucfirst($client->payment_duedays); ?>
                                                                <?php echo lang('days'); ?></td>
                                                        </tr>
<?php
    } ?>
                                                        <tr>
                                                            <td><?php echo lang('Email'); ?></td>
                                                            <td class="text-right"><?php echo $client->email; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo lang('Phonenumber'); ?></td>
                                                            <td class="text-right"><?php echo $client->phonenumber; ?>
                                                            </td>
                                                        </tr>

                                                        <?php if ($setting->mobile_platform == "TEUM") {
        ?>
                                                        <tr>
                                                            <td><?php echo lang('TEUM CustomerID'); ?></td>
                                                            <td class="text-right">
                                                                <?php echo $client->teum_CustomerId; ?></td>
                                                        </tr>
                                                        <?php
    } ?>

                                                        <?php if ($setting->create_arta_contact == "1") {
        ?>
                                                        <tr>
                                                            <td><?php echo lang('Artilium ContactType2Id'); ?></td>
                                                            <td class="text-right">
                                                                <?php echo $client->ContactType2Id; ?></td>
                                                        </tr>
                                                        <?php
    } ?>
                                                        <?php if (getSmsNumbers($client->id)) {
        ?>
                                                        <?php foreach (getSmsNumbers($client->id) as $key => $sms) {
            ?>
                                                        <tr>
                                                            <td>SMS <?php echo $key+1; ?><a href="#"
                                                                    onclick="DeleteSMS('<?php echo $sms; ?>');"> <i
                                                                        class="fa fa-trash text-danger"></i></a></td>
                                                            <td class="text-right">+<?php echo $sms; ?></td>
                                                        </tr>

                                                        <?php
        } ?>
                                                        <?php
    } ?>
                                                        <tr>
                                                            <td><?php echo lang('Artilium Reference'); ?></td>
                                                            <td class="text-right"><?php echo $client->id; ?></td>
                                                        </tr>
                                                        <?php if ($setting->mage_invoicing == 1) {
        ?>
                                                        <tr>
                                                            <td><?php echo lang('MageboId'); ?></td>
                                                            <td class="text-right">
                                                                <?php if ($client->mageboid > 0) {
            ?>
                                                                <strong> <?php echo $client->mageboid; ?></strong>
                                                                <?php
        } else {
            ?>
                                                                <button class="btn btn-primary btn-success"
                                                                    type="button" id="push_magebo"
                                                                    onclick="push_customer('<?php echo $client->id; ?>')"><i
                                                                        class="fa fa-plus-circle"></i> <?php echo lang('Push Customer to
                                                                    Magebo'); ?></button>
                                                                <?php
        } ?>
                                                            </td>
                                                        </tr>
                                                        <?php
    } ?>
       <tr>
                                                            <td><?php echo lang('Last Seen'); ?></td>
                                                            <td class="text-right">
                                                            <?php if ($client->lastseen) {
        ?>
                                                                <?php echo $client->lastseen; ?>
                                                            <?php
    } else {
        ?>
                                                            <?php echo lang('Never loggedin'); ?>
                                                            <?php
    } ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo lang('Last Updated'); ?></td>
                                                            <td class="text-right">

                                                                <?php echo $client->date_modified; ?>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <?php //print_r($financial);?>
                                                    <?php if ($client->paymentmethod == "directdebit") {
        ?>
                                                    <div class="label" style="padding-top: 10px;">
                                                        <?php echo lang('SEPA MANDATE'); ?>
                                                        <?php if (!empty($financial->SEPA_AMENDMENT)) {
            ?>
                                                        <div class="float-right text-danger"><small><?php echo lang('Amend'); ?>:
                                                                <?php echo $financial->SEPA_AMENDMENT; ?></small></div>
                                                        <?php
        } ?>
                                                    </div>
                                                    <table class="table table-stripred">
                                                        <tr>
                                                            <td><?php echo lang('MANDATE ID'); ?></td>
                                                            <td class="text-right">
                                                                <?php echo $financial->SEPA_MANDATE; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo lang('IBAN'); ?></td>
                                                            <td class="text-right"><?php echo $financial->IBAN; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo lang('BIC'); ?></td>
                                                            <td class="text-right"><?php echo $financial->BIC; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?php echo lang('SIGNATURE DATE'); ?></td>
                                                            <td class="text-right">
                                                                <?php echo $financial->SEPA_MANDATE_SIGNATURE_DATE; ?>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td><?php echo lang('STATUS'); ?></td>
                                                            <td class="text-right">
                                                                <?php echo $financial->SEPA_STATUS; ?></td>
                                                        </tr>


                                                    </table>


                                                    <?php
    }?>


                                                    <hr />


                                                    <a class="btn btn-primary btn-block btn-lg"
                                                        href="<?php echo base_url(); ?>admin/client/edit/<?php echo $this->uri->segment(4); ?>"><i
                                                            class="fa fa-edit"></i>
                                                        <?php echo lang('Edit Client'); ?></a>
                                                    <a href="<?php echo base_url(); ?>admin/client/login_as_client/<?php echo $client->id; ?>"
                                                        class="btn btn-lg btn-dark btn-md btn-block"><i
                                                            class="fa fa-user"></i>
                                                        <?php echo lang('Login as client'); ?></a>
                                                    <a href="#" onclick="confirmation_password();"
                                                        class="btn btn-lg btn-primary btn-md btn-block"><i
                                                            class="fa fa-key"></i>
                                                        <?php echo lang('Send Password'); ?></a>
                                                    <?php if ($this->session->master) {
        ?>
                                                    <a href="#" onclick="confirmation_delete();"
                                                        class="btn btn-lg btn-warning btn-md btn-block"><i
                                                            class="fa fa-trash"></i>
                                                        <?php echo lang('Delete Customer'); ?></a>

                                                    <?php
    }?>
                                                    <button type="button" onclick="add_sms_number();"
                                                        class="btn btn-lg btn-primary btn-md btn-block"><i
                                                            class="fa fa-phone"></i>
                                                        <?php echo lang('Add SMS Number'); ?></button>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="chart col-lg-6 col-12">
                                                    <div class="card">
                                                        <div class="card-header d-flex align-items-center">
                                                            <h3 class="h4"><i class="fa fa-cubes"></i>
                                                                <?php echo lang('Statistic'); ?></h3>
                                                        </div>
                                                        <div class="card-body">
                                                            <?php //print_r($stats);?>
                                                            <table class="table table-striped table-hover">
                                                                <?php if ($setting->mage_invoicing) {
        ?>
                                                                <tr>
                                                                    <td><a href="#"
                                                                            onclick="open_tab('factuur');"><?php echo lang('Total Invoices'); ?></a>
                                                                    </td>
                                                                    <td class="text-right"><a href="#"
                                                                            onclick="open_tab('factuur');"><?php echo $setting->currency; ?><?php echo str_replace('.', ',', number_format($client->stats->invoice_paid + $client->stats->invoice_unpaid, 2)); ?></a>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td><?php echo lang('Total Creditnote'); ?></td>
                                                                    <td class="text-right">
                                                                        <?php echo $setting->currency; ?><?php echo str_replace('.', ',', '0.00'); ?>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td><a href="#"
                                                                            onclick="open_tab('factuur');"><?php echo lang('Balance'); ?></a>
                                                                    </td>
                                                                    <td class="text-right"><a href="#" <?php if ($balance < 0) {
            ?> class="text-danger" <?php
        } ?> onclick="open_tab('factuur');"><?php echo $setting->currency; ?><?php echo $balance; ?></a>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td><a href="#"
                                                                            onclick="open_tab('factuur');"><?php echo lang('Paid Invoices'); ?></a>
                                                                    </td>
                                                                    <td class="text-right"><a href="#"
                                                                            onclick="open_tab('factuur');"><?php echo $setting->currency; ?><?php echo str_replace('.', ',', number_format($client->stats->invoice_paid, 2)); ?>
                                                                            ( <?php echo $client->stats->count_paid; ?>
                                                                            )</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td><a href="#"
                                                                            onclick="open_tab('factuur');"><?php echo lang('Unpaid Invoices'); ?></a>
                                                                    </td>
                                                                    <td class="text-right"><a href="#"
                                                                            onclick="open_tab('factuur');"><?php echo $setting->currency; ?><?php echo str_replace('.', ',', number_format($client->stats->invoice_unpaid, 2)); ?>
                                                                            (
                                                                            <?php echo $client->stats->count_unpaid; ?>
                                                                            )</a></td>
                                                                </tr>
                                                                <?php
    }?>
                                                                <tr>
                                                                    <td><a href="#"
                                                                            onclick="open_tab('cservices');"><?php echo lang('Total Active Services'); ?></a>
                                                                    </td>
                                                                    <td class="text-right"><a href="#"
                                                                            onclick="open_tab('cservices');"><?php echo $client->stats->active_service; ?></a>
                                                                    </td>
                                                                </tr>
                                                                <?php if ($setting->whmcs_ticket) {
        ?>
                                                                <tr>
                                                                    <td><a href="#"
                                                                            onclick="open_tab('helpdesk');"><?php echo lang('Support Ticket'); ?></a>
                                                                    </td>
                                                                    <td class="text-right"><a href="#"
                                                                            onclick="open_tab('helpdesk');"><?php echo $client->stats->open_ticket; ?>
                                                                            /
                                                                            <?php echo $client->stats->closed_ticket; ?></a>
                                                                    </td>
                                                                </tr>
                                                                <?php
    }?>
                                                            </table>

                                                            <br />
                                                            <br />
                                                            <a class="element-box el-tablo" href="#"
                                                                style="padding-top: 10px;">
                                                                <div class="label" style="padding-top: 10px;">
                                                                    <strong> <?php echo lang('Referral'); ?></strong>
                                                                </div>
                                                                <div class="value">
                                                                    <?php echo $client->refferal; ?>
                                                                </div>
                                                            </a>

                                                            <br />
                                                            <br />
                                                            <?php if ($client->agentid > 0) {
        ?>
                                                            <a class="element-box el-tablo"
                                                                href="<?php echo base_url(); ?>admin/agent/detail/<?php echo $client->agentid; ?>"
                                                                style="padding-top: 10px;">

                                                                <?php
    } else {
        ?>
                                                                <a class="element-box el-tablo" href="#"
                                                                    style="padding-top: 10px;">
                                                                    <?php
    }?> <div class="label" style="padding-top: 10px;">
                                                                        <?php echo lang('Agent/Reseller'); ?>
                                                                    </div>
                                                                    <div class="value">
                                                                        <?php echo $client->agent; ?>
                                                                    </div>
                                                                </a>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="chart col-lg-6 col-12">
                                                    <div class="card">
                                                        <div class="card-header d-flex align-items-center">
                                                            <h3 class="h4"><i class="fa fa-arrows-alt"></i>
                                                                <?php echo lang('Navigation'); ?></h3>
                                                        </div>
                                                        <div class="card-body">
                                                            <!--
                              <a class="btn btn-md btn-warning btn-block text-left" href="<?php echo base_url(); ?>admin/invoice/create/<?php echo $client->id; ?>"><i class="fa fa-credit-card"></i> <?php echo lang('New Invoice'); ?></a>
                              <button class="btn btn-md btn-primary btn-block text-left" href="#" onclick="alert('Please open the invoice you wish to credit and click creditnote');"><i class="fa fa-archive"></i> <?php echo lang('New Creditnote'); ?></button>
                              -->
                                                            <button class="btn btn-md btn-info btn-block text-left"
                                                                data-toggle="modal" data-target="#addnote"><i
                                                                    class="fa fa-comments"></i>
                                                                <?php echo lang('New Notes'); ?></button>
                                                            <a class="btn btn-md btn-success btn-block text-left"
                                                                href="<?php echo base_url(); ?>admin/client/contact_add/<?php echo $client->id; ?>"><i
                                                                    class="fa fa-user"></i>
                                                                <?php echo lang('New Contacts'); ?></a>
                                                            <?php if ($setting->whmcs_ticket) {
        ?>
                                                            <button class="btn btn-md btn-primary btn-block text-left"
                                                                data-toggle="modal" data-target="#addTicket"><i
                                                                    class="fa fa-life-ring"></i>
                                                                <?php echo lang('New Ticket'); ?></button>
                                                            <?php
    }?>


                                                            <a href="<?php echo base_url(); ?>admin/subscription/add_mobile/<?php echo $this->uri->segment(4); ?>"
                                                                class="btn btn-md btn-primary btn-block text-left"
                                                                id="addOrder"><i class="fa fa-mobile"></i>
                                                                <?php echo lang('Order Mobile Subscription'); ?></a>
                                                            <?php if ($this->session->cid == 2) {
        ?>
                                                            <a href="<?php echo base_url(); ?>admin/subscription/add_internet/<?php echo $this->uri->segment(4); ?>"
                                                                class="btn btn-md btn-primary btn-block text-left"
                                                                id="addOrder"><i class="fa fa-desktop"></i>
                                                                <?php echo lang('Order xDSL Subscription'); ?></a>
                                                            <?php
    } ?>
                                                            <button class="btn btn-md btn-primary btn-block text-left"
                                                                data-toggle="modal" data-target="#SendEmail"><i
                                                                    class="fa fa-envelope"></i>
                                                                <?php echo lang('Send Email'); ?></button>
                                                            <?php if ($setting->mage_invoicing) {
        ?>
                                                            <?php if ($client->paymentmethod == "directdebit") {
            ?>
                                                            <button class="btn btn-md btn-primary btn-block text-left"
                                                                data-toggle="modal" data-target="#SepaAmend"><i
                                                                    class="fa fa-university"></i>
                                                                <?php echo lang('SEPA Amendment'); ?></button>
                                                            <?php
        } ?>
                                                            <?php
    }?>
    <?php if ($setting->mage_invoicing) {
        ?>
                                                            <button class="btn btn-md btn-primary btn-block text-left"
                                                                data-toggle="modal" data-target="#AddNextInvoiceItem"><i
                                                                    class="fa fa-plus-circle"></i>
                                                                <?php echo lang('Add Next Invoice Items'); ?></button>
                                                                <?php
    }?>
     <?php if ($setting->mage_invoicing == 1) {
        ?>
                                                            <?php if ($this->session->master) {
            ?>
                                                            <button class="btn btn-md btn-primary btn-block text-left"
                                                                data-toggle="modal"
                                                                data-target="#CreatePricingMagebo"><i
                                                                    class="fa fa-university"></i>
                                                                <?php echo lang('Push Subscription'); ?></button>
                                                            <?php
        } ?>
    <?php
    }?>
                                                            <button class="btn btn-md btn-block text-left btn-primary"
                                                                onclick="assignReseller();"><i class="fa fa-user"></i>
                                                                <?php echo lang('Assign to a Reseller/Agent'); ?></button>
                                                            <?php if ($this->session->cid == 54) {
        ?>
                                                            <button type="button"
                                                                class="btn btn-md btn-block text-left btn-danger"
                                                                onclick="openLastReminderBank('<?php echo $this->uri->segment(4); ?>');"><i
                                                                    class="fa fa-envelope"></i>
                                                                <?php echo lang('Last Reminder Id/Bank'); ?></button>
                                                            <button type="button"
                                                                class="btn btn-md text-left btn-block btn-danger"
                                                                onclick="openReminderBank('<?php echo $this->uri->segment(4); ?>');"><i
                                                                    class="fa fa-envelope"></i>
                                                                <?php echo lang('Reminder Id/Bank'); ?></button>
                                                            <!--  <button class="btn btn-md btn-block text-left btn-primary" onclick="EditCiot();"><i class="fa fa-user"></i> <?php echo lang('Update CIOT Information'); ?></button>
                               -->
                                                            <button class="btn btn-md btn-block text-left btn-primary"
                                                                onclick="sendPortinReminder();"><i
                                                                    class="fa fa-user"></i>
                                                                <?php echo lang('Send Portin Reminder'); ?></button>
                                                            <?php
    } ?>
                                                        </div>
                                                    </div>
                                                    <br />
                                                    <br />
                                                    <a class="element-box el-tablo" href="#" style="padding-top: 10px;">
                                                        <div class="label" style="padding-top: 10px;">
                                                            <?php echo lang('Current out of bundle usage'); ?>
                                                        </div>
                                                        <div class="value">
                                                            <div id="consumption"></div>
                                                        </div>
                                                    </a>
                                                    <?php if ($this->session->cid == "54") {
        ?>
                                                    <br />
                                                    <br />
                                                    <a class="element-box el-tablo" href="#" style="padding-top: 10px;">
                                                        <div class="label" style="padding-top: 10px;">
                                                            <strong> <?php echo lang('Location'); ?></strong>
                                                        </div>
                                                        <div class="value">
                                                            <?php echo $client->location; ?>
                                                        </div>
                                                    </a>

                                                    <?php
    } ?>
                                                    <?php if ($client->dunning_profile > 0) {
        ?>

                                                    <a class="element-box el-tablo" href="#" style="padding-top: 10px;">
                                                        <div class="label" style="padding-top: 10px;">
                                                            <?php echo lang('Customer Received Reminder:'); ?>
                                                        </div>
                                                        <div class="value<?php if ($client->dunning_profile == 1) {
            ?> text-warning<?php
        } elseif ($client->dunning_profile == 2) {
            ?> text-danger<?php
        } ?>">
                                                            <?php echo $client->dunning_profile; ?>
                                                        </div>
                                                    </a>

                                                    <?php
    }?>



                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="chart col-lg-12 col-12">
                                                    <div class="element-content">
                                                        <?php if ($setting->whmcs_ticket) {
        ?>
                                                        <div class="row" id="lasttickets">
                                                            <div class="col-sm-12">
                                                                <h5><i class="fa fa-life-ring"></i>
                                                                    <?php echo lang('Last 5 Tickets'); ?></h5>
                                                                <ul id="ticketitems" class="element-box el-tablo">
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <?php
    }?>
                                                        <div class="row" id="lastnotes">
                                                            <div class="col-sm-12">
                                                                <h5><i class="fa fa-sticky-note"></i>
                                                                    <?php echo lang('Last 5 sticky notes'); ?></h5>
                                                                <table class="table table-striped">
                                                                </table>
                                                                <ul id="noteitems" class="element-box el-tablo">
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--
                    <div class="chart col-lg-2 col-12">
                      <div class="card">
                        <div class="card-header d-flex align-items-center">
                          <h3 class="h4"><i class="fa fa-bars"></i> <?php echo lang('Iban List'); ?> <a href="#" data-toggle="modal" data-target="#addIban"><i class="fa fa-plus-circle"></i></a></h3>
                        </div>
                        <div class="card-body">
                          <table class="table table-striped table-hover">
                            <?php if (!empty($ibans)) {
        ?>
                            <?php foreach ($ibans as $iban) {
            ?>
                            <tr>
                              <td><?php echo $iban['iban']; ?></td>
                            </tr>
                            <?php
        } ?>
                            <?php
    } else {
        ?>
                            <tr>
                              <td><?php echo lang('No IBAN Found'); ?></td>
                            </tr>
                            <?php
    }?>
                          </table>
                        </div>
                      </div>
                    </div>
                    -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="cservices" class="tab-pane fade">
                            <div class="card-header"><?php echo lang('List Subscription'); ?></div>
                            <div class="table-responsive card">
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="services">
                                        <thead>
                                            <tr>
                                                <th><?php echo lang('Productname'); ?></th>
                                                <th><?php echo lang('DateReg'); ?></th>
                                                <th><?php echo lang('Status'); ?></th>
                                                <th><?php echo lang('Recurring'); ?></th>
                                                <th><?php echo lang('Identifiers'); ?></th>
                                                <th><?php echo lang('Cycle'); ?></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                    <?php if ($msubs) {
        ?>
                                    <hr />
                                    <h5><?php echo lang('Recurring Subscription Manually created'); ?></h5>
                                    <table class="table table-striped table-lightfont" id="mservices">
                                        <thead>
                                            <tr>
                                                <th>#<?php echo lang('Items'); ?></th>
                                                <th><?php echo lang('Amount'); ?></th>
                                                <th><?php echo lang('Month Left'); ?></th>
                                                <th><?php echo lang('End Date'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($msubs as $sub) {
            ?>
                                            <tr>
                                                <td><?php echo $sub->description; ?></td>
                                                <td><?php echo $setting->currency; ?><?php echo number_format($sub->amount, 2); ?>
                                                </td>
                                                <td><?php if ($sub->terms == 600) {
                ?>Forever<?php
            } else {
                ?><?php
            } ?>
                                                </td>
                                                <td><?php echo $sub->end_date; ?></td>

                                            </tr>
                                            <?php
        } ?>
                                        </tbody>
                                    </table>
                                    <?php
    }?>



                                    <?php if ($nextInvoices) {
        ?>
                                    <hr />
                                    <h5><?php echo lang('Manual Subscription'); ?></h5>
                                    <table class="table table-striped table-lightfont" id="mservices">
                                        <thead>
                                            <tr>
                                                <th>#<?php echo lang('Items'); ?></th>
                                                <th><?php echo lang('Amount'); ?></th>
                                                <th><?php echo lang('Months'); ?></th>
                                                <th><?php echo lang('Description'); ?></th>
                                                <th><?php echo lang('Date Execute'); ?></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($nextInvoices as $sub) {
            ?>

                                            <tr>
                                                <td><?php echo $sub->iGeneralPricingIndex; ?></td>
                                                <td><?php echo $setting->currency; ?><?php echo number_format($sub->mUnitPrice, 2); ?>
                                                </td>
                                                <td><?php if ($sub->terms == 600) {
                ?><?php echo lang('Forever'); ?><?php
            } else {
                ?><?php echo $sub->terms; ?>
                                                    <?php echo lang('month'); ?><?php
            } ?></td>
                                                <td><?php echo $sub->cInvoiceDescription; ?></td>
                                                <td><?php echo $sub->dContractDate; ?></td>
                                                <td><?php if ($sub->terms != 600) {
                ?><button class="btn btn-sm btn-primary" type="button"
                                                        onclick="stopManualSubs('<?php echo $sub->iGeneralPricingIndex; ?>')"><i
                                                            class="fa fa-trash"></i>
                                                        <?php echo lang('Stop This Subscription'); ?></button><?php
            } ?>
                                                </td>
                                            </tr>
                                            <?php
        } ?>
                                        </tbody>
                                    </table>
                                    <?php
    }?>


                                    <hr />


                                </div>
                            </div>
                        </div>
                        <?php if ($setting->mage_invoicing) {
        ?>
                        <div id="factuur" class="tab-pane fade">
                            <div class="card-header"><?php echo lang('List Invoices'); ?></div>
                            <div class="table-responsive card">
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="dt_invoice">
                                        <thead>
                                            <tr>
                                                <th>#<?php echo lang('Invoicenum'); ?></th>
                                                <th><?php echo lang('Date'); ?></th>
                                                <th><?php echo lang('Duedate'); ?></th>
                                                <th><?php echo lang('Status'); ?></th>
                                                <th><?php echo lang('Amount'); ?></th>
                                                <th><?php echo lang('Type'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-header"><?php echo lang('List Proforma Invoices'); ?></div>
                            <div class="table-responsive card">
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="dt_proformas">
                                        <thead>
                                            <tr>
                                                <th>#<?php echo lang('ProformaNumber'); ?></th>
                                                <th><?php echo lang('Date'); ?></th>
                                                <th><?php echo lang('Duedate'); ?></th>

                                                <th><?php echo lang('Amount'); ?></th>
                                                <th><?php echo lang('Status'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>

                        <!--

            <div id="creditnotes" class="tab-pane fade">
              <div class="card-header"><?php echo lang('List Creditnotes'); ?></div>
              <div class="table-responsive card">
                <div class="card-body">
                  <table class="table table-striped table-lightfont" id="dt_creditnotes">
                    <thead>
                      <tr>
                        <th>#<?php echo lang('Number'); ?></th>
                        <th><?php echo lang('Date'); ?></th>
                        <th><?php echo lang('Total'); ?></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
-->




                        <div id="payments" class="tab-pane fade">
                            <div class="card-header"><?php echo lang('List Payments'); ?></div>
                            <div class="table-responsive card">
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont">
                                        <thead>

                                            <tr>
                                                <th>#<?php echo lang('PaymentNumber'); ?></th>
                                                <th><?php echo lang('PaymentDate'); ?></th>
                                                <th><?php echo lang('PaymentFrom'); ?></th>
                                                <th><?php echo lang('Amount'); ?></th>
                                                <th><?php echo lang('Description'); ?></th>
                                                <th><?php echo lang('Remarks'); ?></th>
                                                <th><?php echo lang('Assigned'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($payments) {
            ?>
                                            <?php foreach ($payments as $p) {
                ?>
                                            <tr>
                                                <td><?php echo $p['Payment']; ?></td>
                                                <td><?php echo str_replace(' 00:00:00.000', '', $p['Paymentdate']); ?>
                                                </td>

                                                <td><?php echo $p['Paymentform']; ?></td>
                                                <td><?php echo $setting->currency; ?><?php echo number_format($p['Amount'], 2); ?>
                                                </td>
                                                <td><?php echo $p['Description']; ?></td>
                                                <td><?php echo $p['Remark']; ?></td>
                                                <?php if ($p['Assigned'] == 1) {
                    ?>
                                                <td class="text-success"><?php echo lang('Assigned'); ?></td>
                                                <?php
                } else {
                    ?>
                                                <td class="text-danger"><?php echo lang('Unassign'); ?></td>
                                                <?php
                } ?>


                                            </tr>
                                            <?php
            } ?>
                                            <?php
        } else {
            ?>
                                            <tr>
                                                <td colspan="7" class="text-center">Geen resultaten aanwezig in de tabel
                                                </td>


                                            </tr>
                                            <?php
        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <?php
    }?>
     <div id="emails" class="tab-pane fade">
                            <div class="card-header"><?php echo lang('List Emails'); ?> <i class="text-danger"><?php echo lang('any email logs older than 1 year will be automaticly deleted'); ?></i></div>
                            <div class="table-responsive card">
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="dt_emails">
                                        <thead>
                                            <tr>
                                                <th>#<?php echo lang('date'); ?></th>
                                                <th><?php echo lang('to'); ?></th>
                                                <th><?php echo lang('Subject'); ?></th>
                                                <th><?php echo lang('Status'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <?php if ($setting->whmcs_ticket) {
        ?>
                        <div id="helpdesk" class="tab-pane fade">
                            <div class="card-header"><?php echo lang('List Tickets'); ?>
                            <div class="float-right"> <button class="btn btn-md btn-info btn-block text-left"
                                            data-toggle="modal" data-target="#addTicket"><i class="fa fa-comments"></i>
                                            <?php echo lang('Add Ticket'); ?></button></div>
                                            </div>
                            <div class="table-responsive card">
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="dt_tickets">
                                        <thead>
                                            <tr>
                                                <th>#<?php echo lang('TicketID'); ?></th>
                                                <th><?php echo lang('Date'); ?></th>
                                                <th><?php echo lang('Subject'); ?></th>
                                                <th><?php echo lang('Department'); ?></th>
                                                <th><?php echo lang('Status'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
    }?>
                        <div id="contactTabs" class="tab-pane fade">
                            <div class="table-responsive card">
                                <div class="card-header"><?php echo lang('List Contacts'); ?>
                                    <div class="float-right"><a
                                            href="<?php echo base_url(); ?>admin/client/contact_add/<?php echo $this->uri->segment(4); ?>"
                                            class="btn btn-xs btn-primary"><i class="fa fa-plus-circle"></i>
                                            <?php echo lang('Add Contact'); ?></a></div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="dt_contacts">
                                        <thead>
                                            <tr>
                                                <th><?php echo lang('Customer'); ?></th>
                                                <th><?php echo lang('Role/Function'); ?></th>
                                                <th><?php echo lang('Email'); ?></th>
                                                <th><?php echo lang('Telephone'); ?></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="credit" class="tab-pane fade">
                            <div class="table-responsive card">
                                <div class="card-header"><?php echo lang('List Creditnotes'); ?></div>
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="dt_cn">
                                        <thead>
                                            <tr>
                                                <th><?php echo lang('Number'); ?></th>
                                                <th><?php echo lang('Date'); ?></th>
                                                <th><?php echo lang('Total'); ?></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="logs" class="tab-pane fade">
                            <div class="table-responsive card">
                                <div class="card-header"><?php echo lang('List Logs'); ?></div>
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="dt_logs">
                                        <thead>
                                            <tr>
                                                <th width="15%"><?php echo lang('Date'); ?></th>
                                                <th width="60%"><?php echo lang('Description'); ?></th>
                                                <th width="15%"><?php echo lang('User'); ?></th>
                                                <th width="10%"><?php echo lang('IP'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="notes" class="tab-pane fade">
                            <div class="table-responsive card">
                                <div class="card-header"><?php echo lang('List Notes'); ?>
                                    <div class="float-right"> <button class="btn btn-md btn-info btn-block text-left"
                                            data-toggle="modal" data-target="#addnote"><i class="fa fa-comments"></i>
                                            <?php echo lang('New Notes'); ?></button></div>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped table-lightfont" id="dt_notes">
                                        <thead>
                                            <tr>
                                                <th width="10%"><?php echo lang('Date'); ?></th>
                                                <th width="70%"><?php echo lang('Description'); ?></th>
                                                <th width="10%"><?php echo lang('Admin'); ?></th>
                                                <th width="10%"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>





                        <div id="documents" class="tab-pane fade">
                            <div class="table-responsive card">
                                <div class="card-header"><?php echo lang('List Document Created'); ?>
                                    <div class="close">
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <table class="table table-striped table-lightfont" id="dt_documents">
                                            <thead>
                                                <tr>
                                                    <th width="10%"><?php echo lang('Description'); ?></th>
                                                    <th width="80%"><?php echo lang('Filename'); ?></th>
                                                    <th width="10%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <hr />
                                        <form method="post">
                                            <div class="input-group col-md-6 ml-auto mr-auto">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="inputGroupFile02"
                                                        name="file">
                                                    <label class="custom-file-label"
                                                        for="inputGroupFile02"><?php echo lang('Choose file'); ?></label>
                                                </div>
                                                <div class="input-group-append">
                                                    <button class="input-group-text"
                                                        id=""><?php echo lang('Upload'); ?></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($this->session->id == 1) {
        ?>
<?php //print_r($mageboid);?>
<?php
    }?>
<section class="dashboard-header">
    <div class="container-fluid">
</section>
<div id="fade"></div>
<div id="modal" class="modal">
    <img id="loader" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" />
</div>
<form>
    <input type="hidden" id="userid" value="<?php echo $client->id; ?>">
    <input type="hidden" id="mageboid" value="<?php echo $client->mageboid; ?>">
</form>
<script type="text/javascript">
function sendPortinReminder(id) {
    $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/subscription/get_pending_msisdn',
        type: 'post',
        dataType: 'json',
        data: {
            id: '<?php echo $client->id; ?>',
            companyid: '<?php echo $this->session->cid; ?>'
        },
        success: function(data) {
            if (data.result) {
                $('#sendPendingPortinReminder').modal('toggle');
                $('#portinserviceid').html(data.data);
            } else {
                alert('<?php echo lang('there is no orders in pending, and porting request'); ?>')

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("<?php echo lang('Error  accour while deleting your notes'); ?>");
        }
    });



}

function delete_note(id) {
    var answer = confirm(" <?php echo lang('Do you wish delete this note?'); ?>")
    if (answer) {
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/client/delete_note',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                companyid: '<?php echo $this->session->cid; ?>'
            },
            success: function(data) {
                if (data.result) {
                    alert("Note has been deleted");
                    window.location.replace(window.location.protocol + "//" + window.location.host +
                        "/admin/client/detail/<?php echo $client->id; ?>");
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("<?php echo lang('Error  accour while deleting your notes'); ?>");
            }
        });
    }
}


function DeleteSMS(msisdn) {
    var answer = confirm("<?php echo lang('Are you sure?'); ?>");
    if (answer) {
        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/admin/client/delete_sms_notification',
            type: 'post',
            dataType: 'json',
            data: {
                userid: '<?php echo $client->id; ?>',
                msisdn: msisdn
            },
            success: function(data) {
                window.location.replace(window.location.protocol + "//" + window.location.host +
                    "/admin/client/detail/<?php echo $client->id; ?>");
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("<?php echo lang('Error  accour while deleting your sms number'); ?>");
            }
        });

    } else {

        console.log('cancelled');
    }
}

function assignReseller() {
    $('#ResellerAssign').modal('toggle');
}

function edit_note(id) {
    $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/client/get_note/' + id,
        type: 'post',
        dataType: 'json',
        data: {
            id: id
        },
        success: function(data) {
            $( "#stickyedit" ).prop( "checked", false );
            if (data.result) {
                $('#notetext').val(data.description);
                $('#noteid').val(id);
                if(data.sticky == "0"){
                    $( "#stickyedit" ).prop( "checked", false );
                }else{

                    $( "#stickyedit" ).prop( "checked", true );
                }

                $('#edit_note').modal('toggle');
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("<?php echo lang('Error  accour while deleting your notes'); ?>");
        }
    });
}

function EditCiot() {
    window.location.replace(window.location.protocol + "//" + window.location.host +
        "/admin/client/edit_ciot/<?php echo $client->id; ?>");
}

function send_email() {
    $('#SendEmail').modal('toggle');
}

function push_customer(id) {
    var answer = confirm("<?php echo lang('Do you wish create customer in magebo?'); ?>");
    if (answer) {
        $('#push_magebo').prop('disabled', true);
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/complete/addMageboClient/',
            type: 'post',
            dataType: 'json',
            data: {
                id: id
            },
            success: function(data) {
                if (data.result == "success") {
                    window.location.replace(window.location.protocol + "//" + window.location.host +
                        "/admin/client/detail/" + id);
                } else {
                    alert('ERROR: ' + data.message);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("<?php echo lang('Error  accour while adding your customer'); ?>");
            }
        });
    }
}

function add_sms_number() {

    $('#addsmsnumber').modal('toggle');


}

function confirmation_password() {
    var answer = confirm(" <?php echo lang('Do you wish to reset and send customer new password?'); ?>");
    if (answer) {
        var userid = $("#userid").val();
        openModal();
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/client/send_password',
            type: 'post',
            dataType: 'json',
            data: {
                userid: userid
            },
            success: function(data) {
                if (data.result) {
                    window.location.replace(window.location.protocol + "//" + window.location.host +
                        "/admin/client/detail/" + userid + "/success_send");
                }
                closeModal();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //alert("Error  accour while sending your email");
                console.log(errorThrown);
                closeModal();
            }
        });
    } else {
        console.log("hello ");
    }
}

function confirmation_delete() {
    var userid = $("#userid").val();
    var answer = confirm("<?php echo lang('Do you wish to delete this customer id ?'); ?>: " + userid);
    if (answer) {
        window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/client/delete/" +
            userid);
    }
}

function openModal() {
    document.getElementById('modal').style.display = 'block';
    document.getElementById('fade').style.display = 'block';
}

function closeModal() {
    document.getElementById('modal').style.display = 'none';
    document.getElementById('fade').style.display = 'none';
}

function assignInvoice(mageboid) {


}

function openReminderBank(userid) {

    $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/client/getServices',
        type: 'post',
        dataType: 'json',
        data: {
            userid: userid
        },
        success: function(data) {
            if (data.result == 'success') {
                $('#reminder1_serviceid').append(data.html);
                $('#showreminder1').modal('toggle');
            } else {
                alert("Error  accour while geeting pending orders");
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("<?php echo lang('Error  accour while geeting pending orders'); ?>");
        }
    });



    /*
      var serviceid  = "<?php echo $this->uri->segment(4); ?>";
        var c = confirm('are you sure ?, This will send out email to your customer for Reminder ID & Bank');
        if(c){

              window.location.href= window.location.protocol + '//' + window.location.host + '/admin/subscription/send_custom_email/service/'+id+'/custom_id_bank_reminder';


        }
        */
}

function deleteSubscription(id) {

    alert('This action is not allowed');
}

function openLastReminderBank(userid) {

    $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/client/getServices',
        type: 'post',
        dataType: 'json',
        data: {
            userid: userid
        },
        success: function(data) {
            if (data.result == 'success') {
                $('#reminder2_serviceid').html(data.html);
                $('#showreminder2').modal('toggle');
            } else {
                alert("<?php echo lang('Error  accour while geeting pending orders'); ?>");
            }

        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Error  accour while geeting pending orders");
        }
    });


    /*
      var serviceid  = "<?php echo $this->uri->segment(4); ?>";
        var c = confirm('are you sure ?, This will send out email to your customer for Last Reminder ID & Bank');
        if(c){

              window.location.href= window.location.protocol + '//' + window.location.host + '/admin/subscription/send_custom_email/service/'+id+'/custom_last_id_bank_reminder';


        }
        */
}

function SendCreditnote(id) {

    var answer = confirm(" <?php echo lang('Do you wish to send this note to your customer?'); ?>")
    if (answer) {
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/invoice/send_creditnote',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                companyid: '<?php echo $this->session->cid; ?>'
            },
            success: function(data) {
                if (data.result) {
                    alert("Note has been deleted");
                    window.location.replace(window.location.protocol + "//" + window.location.host +
                        "/admin/client/detail/<?php echo $client->id; ?>");
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("<?php echo lang('Error  accour while sending creditnote'); ?>");
            }
        });
    }
}
</script>
<?php if ($download) {
        ?>
<script>
$(window).on('load', function() {
    $('#show_download').modal('show');
});
</script>
<div class="modal fade" id="show_download" tabindex="-1" role="dialog" aria-labelledby="addnote" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post"
                action="<?php echo base_url(); ?>admin/subscription/download_welcome_letter/<?php echo $this->uri->segment(6); ?>">
                <input type="hidden" name="adminid" value="<?php echo $_SESSION['id']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Download Welcome Letter?'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <center><img src="<?php echo base_url(); ?>assets/img/pdf-logo.png"></center>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('No'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('YES'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
    }?>
<!-- Modal -->
<div class="modal fade" id="SepaAmend" tabindex="-1" role="dialog" aria-labelledby="SepaAmend" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/client/sepa_amend">
                <input type="hidden" name="admin"
                    value="<?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <input type="hidden" name="companyid" value="<?php echo $client->companyid; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Sepa Amendment'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('TYPE SEPA'); ?>:</label>
                        <select name="type" class="form-control">
                            <option value=""><?php echo lang('SELECT TYPE'); ?>..</option>
                            <?php if ($financial->SEPA_MANDATE) {
        ?>
                            <option value="AMEND">AMEND</option>
                            <?php
    }?>
                            <option value="NEW">NEW</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('IBAN'); ?>:</label>
                        <input class="form-control" name="iban" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">
                            <?php echo lang('Mandateid'); ?>:(<?php echo lang('Optional'); ?>)</label>
                        <input class="form-control" name="mandateid" value="">
                    </div>

                    <div class="form-group">
                        <label for="exampleTextarea">
                            <?php echo lang('BIC'); ?>(<?php echo lang('Optional, Works Guarantee on'); ?>
                            BE,NL,FR,DE,UK,LU):</label>
                        <input class="form-control" name="bic" value="">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('Save changes'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="CreatePricingMagebo" tabindex="-1" role="dialog" aria-labelledby="CreatePricingMagebo"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="syncpricing">
                <input type="hidden" name="admin"
                    value="<?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <input type="hidden" name="companyid" value="<?php echo $client->companyid; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Push Subscription'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <center><?php echo lang('Are you sure?'); ?></center>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="sync_pricing_cancel">
                        <?php echo lang('Close'); ?></button>
                    <button type="button" class="btn btn-primary" id="sync_pricing">
                        <?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="AddNextInvoiceItem" tabindex="-1" role="dialog" aria-labelledby="AddNextInvoiceItem"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/client/add_extra_nextinvoice_items">
                <input type="hidden" name="admin"
                    value="<?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <input type="hidden" name="companyid" value="<?php echo $client->companyid; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Add Extra Invoice Items'); ?><?php echo lang('BillingID'); ?>:
                        <?php echo $client->mageboid; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="type"> <?php echo lang('Type Item'); ?></label>
                            <select name="type" class="form-control" id="line_type">
                                <option value="RECUR"><?php echo lang('RECURRING'); ?></option>
                                <option value="ONETIME"><?php echo lang('ONE TIME'); ?></option>
                            </select>
                        </div>
                        <div class="form-group col-md-6" id="recurring" style="display:none;">
                            <label for="terms"> <?php echo lang('How Many Month'); ?></label>
                            <input name="terms" id="terms" class="form-control" value="1">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="description"> <?php echo lang('Description'); ?></label>
                            <input name="description" class="form-control" type="text"
                                value="Toestel kosten Samsung S6 - 24 mnd" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="vat_rate" class="text-right"> <?php echo lang('Amount'); ?>
                                <?php echo lang('Vat'); ?></label>
                            <select name="vat_rate" class="form-control">
                                <option value="0">0</option>
                                <?php if ($client->vat_rate > 0) {
        ?>
                                <option value="<?php echo $client->vat_rate; ?>" selected>
                                    <?php echo $client->vat_rate; ?></option>
                                <?php
    }?>

                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleTextarea"> <?php echo lang('Amount'); ?>
                                <?php echo lang('Amount Include VAT'); ?></label>
                            <input name="amount" class="form-control text-right" type="number" step="any"
                                placeholder="<?php echo lang('Amount Include VAT'); ?>" required>
                        </div>


                    </div>
                    <small><?php echo lang('You may add Extra item for next invoicing, if the amount is lower than 0 then it will be treated as discount'); ?></small>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Cancel'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="addnote" tabindex="-1" role="dialog" aria-labelledby="addnote" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/client/add_note">
                <input type="hidden" name="admin"
                    value="<?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <input type="hidden" name="companyid" value="<?php echo $client->companyid; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Create Note for customer'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2><?php echo $client->firstname; ?> <?php echo $client->lastname; ?> </h2>
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('Notes'); ?></label>
                        <textarea name="note" class="form-control" id="exampleTextarea" rows="4" required></textarea>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" name="sticky" type="checkbox" value="1">
                                <?php echo lang('Set note to be sticky'); ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('Save changes'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="showreminder1" tabindex="-1" role="dialog" aria-labelledby="addnote" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/subscription/send_reminder_custom/reminder_id_2">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Do you wish to send email: Reminder Id/Bank?'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label">Select related order</label>
                        <select name="serviceid" id="reminder1_serviceid" class="form-control">

                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('No'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('YES'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="showreminder2" tabindex="-1" role="dialog" aria-labelledby="showreminder2"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/subscription/send_reminder_custom/reminder_id_1">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Do you wish to send email: Last Reminder Id/Bank?'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="form-label"><?php echo lang('Select related order'); ?></label>
                        <select name="serviceid" id="reminder2_serviceid" class="form-controlm-">

                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('No'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('YES'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php if (get_agents($this->session->cid)) {
        ?>
<div class="modal fade" id="ResellerAssign" tabindex="-1" role="dialog" aria-labelledby="ResellerAssign"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/client/assign_agent">
                <input type="hidden" name="admin"
                    value="<?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <input type="hidden" name="companyid" value="<?php echo $client->companyid; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Assign this Customer to Reseller'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2><?php echo $client->firstname; ?> <?php echo $client->lastname; ?> </h2>

                    <div class="form-group">
                        <label class="form-label"> </label>
                        <select name="agentid" class="form-control">
                            <option value="0">None</option>
                            <?php foreach (get_agents($this->session->cid) as $agent) {
            ?>
                            <option value="<?php echo $agent->id; ?>" <?php if ($client->id == $agent->id) {
                ?> selected<?php
            } ?>><?php echo $agent->agent; ?></option>
                            <?php
        } ?>
                        </select>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('Save changes'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
    } ?>
<?php if (getUnpaidInvoices($client->mageboid)) {
        ?>
<div class="modal fade" id="assignInvoice" tabindex="-1" role="dialog" aria-labelledby="assignInvoice"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/client/assign_agent">
                <input type="hidden" name="admin"
                    value="<?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <input type="hidden" name="companyid" value="<?php echo $client->companyid; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Assign this Customer to Reseller'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2><?php echo $client->firstname; ?> <?php echo $client->lastname; ?> </h2>

                    <div class="form-group">
                        <label class="form-label"> </label>
                        <select name="agentid" class="form-control">
                            <?php foreach (getUnpaidInvoices($client->mageboid) as $invoice) {
            ?>
                            <option value="<?php echo $invoice->iInvoiceNbr; ?>"><?php echo $invoice->iInvoiceNbr; ?>
                                <?php echo $invoice->mInvoiceAmount; ?></option>
                            <?php
        } ?>
                        </select>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('Save changes'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
    } ?>

<div class="modal fade" id="addsmsnumber" tabindex="-1" role="dialog" aria-labelledby="addsmsnumber" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="smsnumber">

                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <?php echo lang('Add Sms  number for invoice notification'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('Msisdn'); ?>:</label>
                        <input type="number" class="form-control" id="msisdn" name="msisdn" required>
                        <span
                            class="text-help"><?php echo lang('Ex: 316343224564, please insert always with leading country code'); ?></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="addsmsNumberNow" class="btn btn-primary">
                        <?php echo lang('Add Number'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit portin send -->
<div class="modal fade" id="sendPendingPortinReminder" tabindex="-1" role="dialog" aria-labelledby="addnote"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/subscription/send_portin_email">
                <input type="hidden" name="admin"
                    value="<?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <input type="hidden" name="email_type" value="service">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Send Portin reminders'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2><?php echo $client->firstname; ?> <?php echo $client->lastname; ?> </h2>
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('Subscription'); ?></label>
                        <select name="serviceid" class="form-control" id="portinserviceid">

                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('Reminder Type'); ?></label>
                        <select name="reminder_type" class="form-control" id="reminder_type">
                            <option value="portin_initiation">Portin Initiation Email</option>
                            <option value="portin_reminder_verification">Reminder Verification</option>

                        </select>
                    </div>

                </div>
                <div class="modal-footer">

                    <button type="submit" class="btn btn-primary"> <?php echo lang('Send Reminder'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Note -->
<div class="modal fade" id="edit_note" tabindex="-1" role="dialog" aria-labelledby="addnote" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/client/edit_note/">
                <input type="hidden" name="admin"
                    value="<?php echo $_SESSION['firstname'] . ' ' . $_SESSION['lastname']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <input type="hidden" name="id" id="noteid" value="">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Create Note for customer'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h2><?php echo $client->firstname; ?> <?php echo $client->lastname; ?> </h2>
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('Notes'); ?></label>
                        <textarea name="note" class="form-control" id="notetext" rows="4"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" name="sticky" id="stickyedit" type="checkbox" value="1">
                                <?php echo lang('Set note to be sticky'); ?>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        <?php echo lang('Close'); ?></button>
                    <button type="submit" class="btn btn-primary"> <?php echo lang('Save changes'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="addTicket" id="addTicket" class="modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    <?php echo lang('New Support Ticket'); ?>
                    <?php echo $client->firstname . ' ' . $client->lastname; ?> (<?php echo $client->mvno_id; ?>)
                </h5>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">
                        ×</span></button>
            </div>
            <form method="post" action="<?php echo base_url(); ?>admin/helpdesk/newticket"
                enctype="multipart/form-data">
                <input type="hidden" name="admin"
                    value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <div class="modal-body" id="modalonly">
                    <div class="form-group">
                        <label for="subject"><?php echo lang('Subject'); ?></label>
                        <input name="subject" class="form-control" placeholder="Title" type="text" required>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="departmen"><?php echo lang('Category'); ?></label>
                            <select class="form-control" id="departmen" name="categoryid">
                                <?php foreach (getHelpdeskCategory($this->session->cid) as $row) {
        ?>
                                <option value="<?php echo $row->id; ?>"><?php echo $row->name; ?></option>
                                <?php
    }?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="departmen"><?php echo lang('Department'); ?></label>
                            <select class="form-control" id="departmen" name="deptid">
                                <?php foreach (getDepartments($this->session->cid) as $row) {
        ?>
                                <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                <?php
    }?>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="priority"><?php echo lang('Change Priority'); ?></label>
                            <select class="form-control" id="priority" name="priority">
                                <?php foreach (array('4', '3', '2', '1') as $prio) {
        ?>
                                <option value="<?php echo $prio; ?>" <?php if ($prio == 3) {
            ?> selected<?php
        } ?>>
                                    <?php echo $prio; ?></option>
                                <?php
    }?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleSelect1"><?php echo lang('Assign to'); ?></label>
                        <select class="form-control" id="exampleSelect1" name="assigne">
                            <?php foreach (getStaf($this->session->cid) as $staf) {
        ?>
                            <option value="<?php echo $staf['id']; ?>" <?php if ($staf['id'] == $this->session->id) {
            ?> selected<?php
        } ?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                            <?php
    }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="file"><?php echo lang('Attachments'); ?></label>
                        <input name="attachments[]" type="file" class="form-control-file" id="attachments"
                            aria-describedby="fileHelp" multiple="">
                        <small id="fileHelp" class="form-text text-muted"><?php echo lang('Allowed type'); ?> docx, doc,
                            xls, xlsx, pdf, gif,
                            png, jpg.</small>
                    </div>
                    <div class="form-group">
                        <label for="message"><?php echo lang('Message'); ?></label>
                        <textarea name="message" class="form-control" id="message" rows="6" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="admin"
                        value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
                    <button class="btn btn-primary" type="submit" id="bos"> <?php echo lang('Save changes'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="SendEmail" tabindex="-1" role="dialog" aria-labelledby="SendEmail" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/client/send_email">
                <input type="hidden" name="adminid" value="<?php echo $_SESSION['id']; ?>">
                <input type="hidden" name="userid" value="<?php echo $client->id; ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> <?php echo lang('Send Email to Customer'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('From'); ?>:</label>
                        <input class="form-control" name="from"
                            value="<?php echo $setting->smtp_name; ?> <<?php echo $setting->smtp_sender; ?>>" required
                            readonly>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('To'); ?>:</label>
                        <input class="form-control" name="to" value="<?php echo $client->email; ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea"> <?php echo lang('Subject'); ?>:</label>
                        <input class="form-control" name="subject" required>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="cc" value="" checked="">
                            <?php echo lang('Tick here if you wish to recieved black carbon copy of this email'); ?>
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea"><?php echo lang('Message'); ?>:</label>
                        <textarea class="form-control texteditor" name="message" id="summernote" rows="10"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary"> <?php echo lang('Send Email'); ?></button>
                    </div>
            </form>
        </div>
    </div>
</div>
<script>
function assignPaymentTobeProcess(data) {

    console.log(data);
    var res = data.split(",");


    $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/sepa/getUnPaidInvoice',
        type: 'post',
        dataType: "json",
        data: {
            iAddressNbr: res[0]
        },
        success: function(data) {
            if (data.length > 0) {
                $('#PLiPaymentNbr').val(res[1]);
                $('#PLcName').val(res[2]);
                $('#PLiAddressNbr').val(res[0]);
                $('#PLAmountPaid').val(res[3]);
                data.forEach(function(element) {
                    $('#PLiInvoiceNbr').append('<option value="' + element.iInvoiceNbr + '">' +
                        element.iInvoiceNbr + ' - <?php echo $setting->currency; ?>' + element
                        .mInvoiceAmount + '</option>');
                    console.log(element.iInvoiceNbr);
                });
                $('#AssignPaymentLeft').modal('toggle');
            } else {
                alert('<?php echo lang('No Open Invoice Found for BillingID'); ?>: ' + res[0]);

            }



        }
    });




}

function open_tab(tab) {
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    //$('.nav-tabs a[href="#' + tab + '"]').tab('show');
}

function stopManualSubs(id) {

    var t = confirm('Are you sure?');
    if (t) {
        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/admin/subscription/disable_invoicing',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                userid: '<?php echo $client->id; ?>',
                companyid: '<?php echo $this->session->cid; ?>'
            },
            success: function(data) {
                if (data.result) {
                    alert("Subscription has been disable");
                    window.location.replace(window.location.protocol + "//" + window.location.host +
                        "/admin/client/detail/<?php echo $client->id; ?>");
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("<?php echo lang('Error  accour while disabling services'); ?>");
            }
        });


    }
}
</script>



<script>
var default_texteditor_path = 'https://portal.central-services.nl/assets/grocery_crud/texteditor';
</script>
<script src="https://portal.central-services.nl/assets/grocery_crud/texteditor/ckeditor/ckeditor.js"></script>
<script src="https://portal.central-services.nl/assets/grocery_crud/texteditor/ckeditor/adapters/jquery.js"></script>
<script src="https://portal.central-services.nl/assets/grocery_crud/js/jquery_plugins/config/jquery.ckeditor.config.js">
</script>
<script>
$(document).ready(function() {
    var sel = $('#line_type').find(":selected").val();
    if (sel == "RECUR") {
        $('#recurring').show();
        $('#terms').prop('required', true);
    } else {
        $('#recurring').hide();
        $('#terms').val('1');
        $('#terms').prop('required', false);
    }

    $("#line_type").change(function() {
        var sel = $('#line_type').find(":selected").val();
        if (sel == "RECUR") {
            $('#recurring').show();
            $('#terms').prop('required', true);
        } else {
            $('#recurring').hide();
            $('#terms').val('1');
            $('#terms').prop('required', false);
        }



    });



});
</script>
<script>
$('#sync_pricing').click(function() {
    $('#sync_pricing_cancel').hide();
    $('#sync_pricing').prop('disabled', true);
    $('#sync_pricing').html('<i class="fas fa-cog fa-spin"></i> Please wait....');
    //method="post" action="<?php echo base_url(); ?>admin/client/sync_general_pricing"

    $.ajax({
        url: window.location.protocol + '//' + window.location.host +
            '/admin/client/sync_general_pricing',
        type: 'post',
        dataType: "json",
        data: $('#syncpricing').serializeArray(),
        success: function(data) {
            if (data.result) {

                $('#CreatePricingMagebo').modal('toggle');


            }
        }
    });


});
$('#addsmsNumberNow').click(function() {
    $('#addsmsNumberNow').prop('disabled', true);
    var userid = '<?php echo $client->id; ?>';
    var msisdn = $('#msisdn').val();
    $.ajax({
        url: window.location.protocol + '//' + window.location.host + '/admin/client/add_sms_number',
        type: 'post',
        dataType: "json",
        data: {
            userid: userid,
            msisdn: msisdn
        },
        success: function(data) {
            window.location.replace(window.location.protocol + "//" + window.location.host +
                "/admin/client/detail/<?php echo $client->id; ?>");
        }
    });

});
</script>
<div class="modal" id="AssignPaymentLeft">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('Assign Payment'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="assigmeform">
                    <div class="form-group has-success">
                        <label class="form-control-label" for="inputSuccess1"><?php echo lang('Payment ID');?></label>
                        <input name="PLiPaymentNbr" type="text" value="" class="form-control" id="PLiPaymentNbr"
                            required readonly>
                    </div>
                    <div class="form-group has-success">
                        <label class="form-control-label" for="inputSuccess1"><?php echo lang('BILLING ID'); ?></label>
                        <input name="PLiAddressNbr" type="text" value="" class="form-control" id="PLiAddressNbr"
                            required readonly>
                    </div>
                    <div class="form-group has-success">
                        <label class="form-control-label" for="inputSuccess1"><?php echo lang('Customer'); ?></label>
                        <input name="PLcName" type="text" value="" class="form-control" id="PLcName" required readonly>
                    </div>

                    <div class="form-group has-success">
                        <label class="form-control-label" for="inputSuccess1"><?php echo lang('Amount'); ?></label>
                        <input name="PLAmountPaid" type="text" value="" class="form-control" id="PLAmountPaid" required
                            readonly>
                    </div>

                    <div class="form-group has-success">
                        <label class="form-control-label" for="inputSuccess1"><?php echo lang('INVOICE'); ?></label>
                        <select name="PLiInvoiceNbr" class="form-control" id="PLiInvoiceNbr">

                        </select>
                    </div>

                    <button class="btn btn-primary" type="button"
                        id="AssignComplete"><?php echo lang('Assign Payment'); ?></button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
function SuspendService(id) {
    var t = confirm('Are you sure?');

    if (t) {

        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/admin/subscription/platform_suspend/' + id,
            type: 'post',
            dataType: "json",
            success: function(data) {
                alert('Serviceid ' + id + ' <?php echo lang('has been suspended'); ?>');
            }
        });
    }
}

function unSuspendService(id) {
    var t = confirm('Are you sure?');

    if (t) {

        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/admin/subscription/platform_unsuspend/' + id,
            type: 'post',
            dataType: "json",
            success: function(data) {
                alert('Serviceid ' + id + ' <?php echo lang('has been suspended'); ?>');
            }
        });
    }

}
</script>