<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Pending Orders Need Action'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">

        </h5>
        <div class="table-responsive">

          <table class="table table-striped table-lightfont" id="pendingOrders">
            <thead>
              <tr>
                <th>
                    <?php echo lang('ID'); ?>
                </th>
                <th>
                    <?php echo lang('Identifiers'); ?>
                </th>
                <th>
                    <?php echo lang('CustomerName'); ?>
                </th>
                <th>
                    <?php echo lang('Date'); ?>
                </th>
                <th> 
                    <?php echo lang('Package'); ?>
                </th>
                <th>
                    <?php echo lang('Status'); ?>
                </th>

              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#pendingOrders').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"pageLength": 50,
"ajax":window.location.protocol + '//' + window.location.host + '/admin/table/get_pending_orders',
"aaSorting": [[0, 'desc']],
"language": {
 "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

$('td:eq(2)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
<?php if($this->session->cid == '54'){ ?>
                            if(aData[11] == "1"){

                                 $('td:eq(2)', nRow).html('<a class="text-success" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');

                            }else{
                                   if(aData[10] == "Paid"){
                               $('td:eq(2)', nRow).html('<i class="fas fa-exclamation-circle"></i> <a class="text-success" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
                             }else{
                                $('td:eq(2)', nRow).html('<i class="fas fa-exclamation-circle"></i> <a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
                             }
                            }


   if(aData[10] == "Paid"){

                                 $('td:eq(0)', nRow).html('<strong><span class="text-success">'+aData[0]+'</span></storng>');
                                // $('td:eq(2)', nRow).html('<strong><span class="text-success">'+aData[2]+'</span></storng>');
                                 $('td:eq(3)', nRow).html('<strong><span class="text-success">'+aData[3]+'</span></storng>');
                                 $('td:eq(4)', nRow).html('<strong><span class="text-success">'+aData[4]+'</span></storng>');

                              }
$('td:eq(5)', nRow).html('<button type="button" class="btn btn-sm btn-primary" onclick="openMe(\''+aData[0]+'\');"><?php echo lang('Accept'); ?></button> <button type="button" class="btn btn-sm btn-warning" onclick="openMeEdit(\''+aData[0]+'\');"><?php echo lang('Modify'); ?></button> <button type="button" class="btn btn-sm btn-danger" onclick="openMeReject(\''+aData[0]+'\');"><?php echo lang('Reject'); ?></button> <button type="button" class="btn btn-sm btn-danger" onclick="openLastReminderBank(\''+aData[0]+'\');"><?php echo lang('Last Reminder Id/Bank'); ?></button> <button type="button" class="btn btn-sm btn-danger" onclick="openReminderBank(\''+aData[0]+'\');"><?php echo lang('Reminder Id/Bank'); ?></button>');
<?php }else{ ?>
$('td:eq(5)', nRow).html('<button type="button" class="btn btn-sm btn-primary" onclick="openMe(\''+aData[0]+'\');"><?php echo lang('Accept'); ?></button> <button type="button" class="btn btn-sm btn-warning" onclick="openMeEdit(\''+aData[0]+'\');"><?php echo lang('Modify'); ?></button> <button type="button" class="btn btn-sm btn-danger" onclick="openMeReject(\''+aData[0]+'\');"><?php echo lang('Reject'); ?></button>');
<?php } ?>

return nRow;
},

});
});
});


</script>
<script>
  function openMeEdit(id){

location.href = "<?php echo base_url(); ?>admin/subscription/edit_order/"+id;
  }
 function openMe(id){
  console.log(id);
  $('#accept_order').modal('toggle');
  $('#acceptx').prop('disabled', true);
   $.ajax({
            url: '<?php echo base_url(); ?>admin/complete/getorderdetails/'+id,
            type: 'post',
            dataType: 'json',
            success: function (data) {
              console.log(data);
              if(data.msisdn_sim){
                if(data.msisdn_sim == '1111111111111111111'){
                  $('#acceptx').prop('disabled', true);
                  } else{
                    $('#acceptx').prop('disabled', false); 
                  }
                $("#simnumberx").val(data.msisdn_sim);
                
              }else{
                $('#acceptx').prop('disabled', true);

              }
              $("#details").html(data.result);
              $("#serviceidx").val(data.serviceid);
               $("#useridx").val(data.userid);

            },
            data: {'orderid': id}
        });

   }
function openReminderBank(id){

  var serviceid  = "<?php echo $this->uri->segment(4); ?>";
    var c = confirm('are you sure ?, This will send out email to your customer for Reminder ID & Bank');
    if(c){

          window.location.href= window.location.protocol + '//' + window.location.host + '/admin/subscription/send_custom_email/service/'+id+'/custom_id_bank_reminder';
      

    }
}

function openLastReminderBank(id){

  var serviceid  = "<?php echo $this->uri->segment(4); ?>";
    var c = confirm('are you sure ?, This will send out email to your customer for Last Reminder ID & Bank');
    if(c){

          window.location.href= window.location.protocol + '//' + window.location.host + '/admin/subscription/send_custom_email/service/'+id+'/custom_last_id_bank_reminder';
      

    }
}
function openMeReject(id){
  $('#reject_order').modal('toggle');
   $.ajax({
            url: '<?php echo base_url(); ?>admin/complete/getorderdetails',
            type: 'post',
            dataType: 'json',
            success: function (data) {
              console.log(data);
              $("#detailsx").html(data.result);
              $("#serviceid").val(data.serviceid);

            },
            data: {'orderid': id}
        });

   }


</script>
<div class="modal fade" id="reject_order">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/reject_mobile">
        <input type="hidden" name="serviceid" id="serviceid">
        <input type="hidden" name="action" value="reject">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Review Before Accept'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body" id="detailsx">

        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label for="exampleTextarea">
                <?php echo lang('Reason of Reject'); ?></label>
            <textarea class="form-control" id="exampleTextarea" rows="3" name="reject_reason" required><?php echo lang('Test order'); ?></textarea>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Reject Order'); ?></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>



<div class="modal fade" id="accept_order">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/activate_mobile">
        <input type="hidden" name="serviceid" id="serviceidx">
        <input type="hidden" name="action" value="accept">
        <input type="hidden" name="userid" id="useridx">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Review Before Accepting order'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div id="details">
          </div>
       
          <div class="col-12">
            <label for="exampleInputEmail1">Assign Simcard:</label>
            <input name="msisdn_sim" type="text" id="simnumberx"  class="form-control  ui-autocomplete-input is-valid pengkor" required>
            <small id="emailHelp" class="form-text text-muted">Please assign <?php echo $setting->companyname; ?> simcard here <span class="text-danger"><strong>* Required</strong></span></small>
          </div>
       
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="acceptx" disabled>
            <?php echo lang('Accept Order'); ?></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>