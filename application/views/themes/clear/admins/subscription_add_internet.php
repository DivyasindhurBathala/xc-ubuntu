<div class="content-i">
  <div class="content-box"><div class="element-wrapper">
    <div class="element-box">
      <form id="formdata">
        <div class="steps-w">
          <div class="step-triggers">
            <a class="step-trigger active" href="#stepContent1"><?php echo lang('Address Check'); ?></a>
            <a class="step-trigger" href="#stepContent2"><?php echo lang('Configure Product'); ?></a>
            <a class="step-trigger" href="#stepContent3"><?php echo lang('Billing Information'); ?></a>
            <a class="step-trigger" href="#stepContent4"><?php echo lang('Final Step'); ?></a>
          </div>
          <div class="step-contents">
            <div id="stepContent1_loader" style="display:none;">
              <center>
              <img src="<?php echo base_url(); ?>assets/img/f553e05cd57fc28d91e3a4b26870f24f.gif">
              </center>
            </div>
            <div class="step-content active" id="stepContent1">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for=""><?php echo lang('Postcode'); ?></label><input class="form-control  ui-autocomplete-input zipcomplete" id="postcodenum" autocomplete="new-username" type="text" name="install_postcode">
                  </div>
                </div>
                <div class="col-sm-8">
                  <div class="form-group">
                    <label for=""><?php echo lang('City'); ?></label><input class="form-control" type="text" id="cityname" placeholder="City Name" name="install_city">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for=""><?php echo lang('StreetName'); ?></label><input class="form-control ui-autocomplete-input streetcomplete" name="install_street" autocomplete="new-username" placeholder="Streetname" type="text" id="street">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for=""><?php echo lang('HouseNumber'); ?></label><input class="form-control" placeholder="HouseNumber" type="text" id="housenumber" name="install_phousenumber">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for=""><?php echo lang('Alphabet'); ?></label><input class="form-control" placeholder="Alphabet" type="text" id="alphabet" name="install_alphabet">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for=""><?php echo lang('Mailbox'); ?></label><input class="form-control" placeholder="Mailbox" type="text" id="mailbox" name="install_mailbox">
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <label for=""><?php echo lang('Block'); ?></label><input class="form-control" placeholder="Block" type="text" id="block" name="install_block">
                  </div>
                </div>
              </div>
              <div class="row">
                <div  id="errornya">
                </div>
              </div>
              <!--
              <div class="form-group">
                <label for=""> States with Offices</label><select class="form-control select2" multiple="true">
                <option selected="true">
                  New York
                </option>
                <option selected="true">
                  California
                </option>
                <option>
                  Boston
                </option>
                <option>
                  Texas
                </option>
                <option>
                  Colorado
                </option>
              </select>
            </div>
            <div class="form-group">
              <label for=""> Regular select</label><select class="form-control">
              <option>
                Select State
              </option>
              <option>
                New York
              </option>
              <option>
                California
              </option>
              <option>
                Boston
              </option>
              <option>
                Texas
              </option>
              <option>
                Colorado
              </option>
            </select>
          </div>
          <div class="form-group">
            <label> Example textarea</label><textarea class="form-control" rows="3"></textarea>
          </div>
          -->
          <div class="form-buttons-w text-right">
            <button class="btn btn-primary step-trigger-btn" type="button" id="addresscheck"> <?php echo lang('Continue'); ?></button>
          </div>
        </div>
        <div class="step-content" id="stepContent2">
          <div class="row">
            <div class="col-md-12">
              <div id="showsuccess"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6" align="center">
              <canvas id="d" data-type="radial-gauge"

              data-units="Mbps"
              data-title="Download"
              data-width="200"
              data-height="200"
              data-bar-width="10"
              data-bar-shadow="5"
              data-color-bar-progress="rgba(50,200,50,.75)"
              ></canvas>
            </div>
            <div class="col-md-6" class="text-center">
              <canvas id="u"
              data-type="radial-gauge"

              data-units="Mbps"
              data-title="Upload"
              data-width="200"
              data-height="200"
              data-bar-width="10"
              data-bar-shadow="5"
              data-color-bar-progress="rgba(50,200,50,.75)"
              ></canvas>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for=""><?php echo lang('Choose Product'); ?></label><select class="form-control" name="packageid" id="packageid">
                <?php foreach (getInternetProductsell('xdsl', $this->session->cid) as $row) {?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                <?php }?>
              </select>
            </div>
            <h2><?php echo lang('Modem Option'); ?></h2>
              <div class="form-group">
              <label for=""> <?php echo lang('Modem Option'); ?></label>
              <select class="form-control" id="modem" name="modem">
                <option value="1"><?php echo lang('Wifi/Voice modem'); ?> Geen Modem</option>
                <option value="2"><?php echo lang('Wifi/Voice modem'); ?> €19.00 (Gedurende 6 maanden)</option>
                <option value="3" selected><?php echo lang('Wifi/Voice modem'); ?> €4.00 per maand</option>
                <option value="4"><?php echo lang('Wifi/Voice modem'); ?> €0.00 (Contract 2 jaar)</option>
              </select>
            </div>
      <h2><?php echo lang('Internet Option'); ?></h2>
              <div class="form-group">
              <label for=""><?php echo lang('Internet Move'); ?></label>
              <select class="form-control" id="migration" name="migration">
                <option value="yes">Yes, I Move my Internet from Other provider such as EDPnet, Proximus, Scarlet, Billi and ETC</option>
                <option value="no" selected>New Rawcopper </option>
              </select>
            </div>

            <div id="migrationoption" style="display:none;">
             <div class="form-group">
              <label for="">Easyswitch ID</label>
<input class="form-control" autocomplete="new-username" placeholder="Optional" type="text" name="easyswitchid" id="easyswitchid">
            </div>
   <div class="form-group">
              <label for="">Circuit ID</label>
<input class="form-control" autocomplete="new-username" placeholder="Optional" type="number" name="circuitid" id="circuitid">
            </div>
</div>
 <h2>PSTN Option</h2>
              <div class="form-group">
              <label for="">Porting PSTN Number</label>
              <select class="form-control" id="voipmove">
                <option value="yes">Yes, Move PSTN Number to us</option>
                <option value="no" selected>No, Ordering new Number </option>
              </select>
            </div>
 <div id="voipmoveoption" style="display:none;">
   <div class="form-group">
              <label for="">PSTN Number to be Ported</label>
<input class="form-control" autocomplete="new-username" placeholder="Required..." type="number" name="voipnumber" id="voipnumber" maxlength="9">
            </div>
 </div>
 <h2><?php echo lang('Customer Option'); ?></h2>
           <div class="form-group">
              <label for=""><?php echo lang('Customer'); ?></label>
              <select class="form-control" id="customertype" name="customer_type">
                <option value="old"><?php echo lang('Existing Customer'); ?></option>
                <option value="new"><?php echo lang('New Customer'); ?></option>
              </select>
            </div>

            <div class="form-group" id="existing">
              <label for=""><?php echo lang('Search Customer'); ?></label>
              <input class="form-control" id="customeridO" value="<?php echo $this->uri->segment(4); ?>"  type="text" name="userid">
            </div>
          </div>
        </div>
        <div class="form-buttons-w text-right">
          <button class="btn btn-primary step-trigger-btn" type="button" id="step3"<?php if(empty($this->uri->segment(4))){ ?> disabled<?php } ?>> Continue</button>
        </div>
      </div>
      <div class="step-content" id="stepContent3">
        <div class="form-group">
          <label for=""><?php echo lang('Email address'); ?></label><input class="form-control" placeholder="Enter email" name="email" type="email" id="email" required>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for=""><?php echo lang('VAT'); ?>.</label><input class="form-control" placeholder="leave empty if not a company entity" type="text" name="vat" id="vat">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for=""><?php echo lang('Companyname'); ?></label><input class="form-control" placeholder="Leave empty if not a company entity" type="text" name="companyname" id="companyname">
            </div>
          </div>
        </div>
        <div class="row">
           <div class="col-sm-4">
            <div class="form-group">
              <label for=""> Gender</label>
          <select class="form-control" id="gender" name="gender">
                    <?php foreach (array('male','female') as $gender) {?>
                    <option value="<?php echo $gender; ?>"><?php echo lang(ucfirst($gender)); ?></option>
                    <?php }?>
                  </select>

            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for=""> Firstname</label><input class="form-control" placeholder="Firstname" type="text" name="firstname" id="firstname" required>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">Lastname</label><input class="form-control" placeholder="Lastname" type="text" name="lastname" id="lastname" required>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for=""> Address1</label><input class="form-control" placeholder="Address" type="text" name="address1" id="xaddress1" required>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label for="">Postcode</label><input class="form-control" placeholder="Postcode" type="text" name="postcode" id="xpostcode" required>
            </div>
          </div>

            <div class="col-sm-3">
            <div class="form-group">
              <label for="">City</label><input class="form-control" placeholder="City" type="text" name="city" id="xcity" required>
            </div>
          </div>
        </div>
    <div class="row">
          <div class="col-sm-6">
            <div class="form-group">
              <label for="">Phonenumber</label><input class="form-control" placeholder="Phonenumber" type="text" name="phonenumber" id="phonenumber" required>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <label for="">National Number</label><input class="form-control" placeholder="National Number" type="text" name="nationalnr" id="nationalnr">
            </div>
          </div>
        </div>
         <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">Language</label>
                <select class="form-control" id="language" name="language">
                    <?php foreach (getLanguages() as $key => $lang) {?>
                    <option value="<?php echo $key; ?>"<?php if ($key == "dutch") {?> selected<?php }?>><?php echo lang($lang); ?></option>
                    <?php }?>
                  </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label for="">Paymentmethod</label>
               <label for="Paymentmethod"> <?php echo lang('Paymentmethod'); ?></label>
                  <select class="form-control" id="default_paymentmethod" name="paymentmethod">
                    <?php foreach (array('banktransfer','directdebit') as $pmt) {?>
                    <option value="<?php echo $pmt; ?>"><?php echo ucfirst($pmt); ?></option>
                    <?php }?>
                  </select>
            </div>
          </div>

           <div class="col-sm-4">
            <div class="form-group">
              <label for=""> Country</label>
              <select class="form-control" name="country">
                <?php foreach (getCountries() as $key => $c) {?>
                <option value="<?php echo $key; ?>"<?php if ($key == "BE") {?> selected<?php }?>>  <?php echo $c; ?> </option>
                <?php }?>
              </select>
            </div>
          </div>
        </div>

           <div class="row">
          
          <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label for="exampleSelect1">
                      <?php echo lang('VAT Rates'); ?></label>
                    <select class="form-control" id="vat_rate" name="vat_rate">
                      <?php foreach(array('21','6','0') as $rate){ ?>
                      <option value="<?php echo $rate; ?>" <?php if ($setting->taxrate == $rate) {?> selected
                        <?php }?>>
                        <?php echo $rate; ?>
                      </option>
                      <?php } ?>
                    </select>
                  </fieldset>
                </div>
              </div>
            <div class="col-sm-4">
                <div class="form-group">
                  <fieldset>
                    <label>Payment Terms</label>
                    <input type="text" class="form-control" name="payment_duedays" id="payment_duedays" value="<?php echo $setting->payment_duedays; ?>">
                  </fieldset>
                </div>
              </div>

              <div class="col-sm-4">
              <div class="form-group">
                <fieldset>
                  <label for="exampleSelect1">E-Invoice</label>
                  <select class="form-control" id="invoice_email" name="invoice_email">
                    <option value="yes" selected="">Yes</option>
                    <option value="yes">No</option>
                  </select>
                  <small> choose yes if wish to recieved email invoices</small>
                </fieldset>
              </div>
            </div>
              
        </div>
         
       <div class="row">
        <div class="col-sm-6">
              <div class="form-group">
                <fieldset class="form-group"><div class="form-desc">
                  <?php echo lang('VAT exemption (BTW verlegd)'); ?>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt" value="0" checked="">
                    <?php echo lang('No, Apply tax on every invoices'); ?>
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="vat_exempt" id="vat_exempt" value="1">
                    <?php echo lang('Yes, do not apply tax on invoices'); ?>
                  </label>
                </div>
              </fieldset>
            </div>
          </div>
            </div>
      
        <div class="form-buttons-w text-right">
          <a class="btn btn-primary step-trigger-btn text-white"  id="step4"> Continue</a>
        </div>
      </div>
      <div class="step-content" id="stepContent4">
     <table class="table table-striped">
      <tr>
        <td></td>
        <td align="right"></td>
      </tr>
       <tr>
        <td>Product</td>
        <td align="right" id="final_productname"></td>
      </tr>
       <tr>
        <td>Modem</td>
        <td align="right" id="final_modem"></td>
      </tr>
       <tr>
        <td>Order Type</td>
        <td align="right" id="final_order_type"></td>
      </tr>
       <tr>
        <td>Customer Name</td>
        <td align="right" id="final_customer_name"></td>
      </tr>
       <tr>
        <td>Customer Email</td>
        <td align="right"  id="final_customer_email"></td>
      </tr>
       <tr>
        <td>Date Install</td>
        <td align="right"><input type="text" name="dateRequested" class="form-control col-md-2" id="pickdate7" required></td>
      </tr>
       <tr>
        <td></td>
        <td align="right"  id="final_customer_2"></td>
      </tr>
     </table>
      <div class="form-buttons-w text-right">
        <input type="hidden" name="proximus_orderid" id="proximus_orderid" value="0"></div>
        <button class="btn btn-primary submit_btn" onclick="disable_button()" type="button"><i class="fa fa-save"></i> <?php echo lang('Submit'); ?></button>
      </div>
    </div>
  </div>
</div>
</form>
</div>
</div>
<script>

  function disable_button(){
$('.submit_btn').prop('disabled', true);
$('.submit_btn').html('<i class="fas fa-spinner fa-spin"></i> Please wait...');
var form_data=$("#formdata").serializeArray();
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/subscription/addorder_internet',
type: 'post',
dataType: "json",
data: form_data,
success: function(data) {
if(data.result){
window.location.href = window.location.protocol + '//' + window.location.host + '/admin/client/detail/'+data.id;
}else{
alert("There was an error: "+data.message);
$('.submit_btn').prop('disabled', false);
$('.submit_btn').html('<i class="fa fa-save"></i> <?php echo lang('Register Clients'); ?>');
}
}
});
}


$("#addresscheck").click(function() {
   // $('#customeridO').val('');
    $("#stepContent1").hide('slow');
    $("#stepContent1_loader").show('slow');
    $('#addresscheck').prop('disabled', true);
    var params = {
        'action': 'preordering',
        'method': 'FindGeographicLocation',
        'street': $("#street").val(),
        'postcode': $("#postcodenum").val(),
        'city': $("#cityname").val(),
        'number': $("#housenumber").val(),
        'alphabet': $("#alphabet").val(),
        'block': $("#block").val(),
        'mailbox': $("#mailbox").val(),
        'floor': $("#floor").val(),
        'channel': '<?php echo $this->session->id; ?><?php echo trim($this->session->chatusername); ?>'
    }
    $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/checkaddress',
        dataType: 'json',
        type: 'post',
        data: params,
        success: function(data) {
            $('#xaddress1').val(params.street+' '+params.number);
            $('#xpostcode').val(params.postcode);
            $('#xcity').val(params.city);

          if(data.result == "success"){
            if(data.vdsl == "OK"){
            var btn_href = '#stepContent2';
            $("#stepContent1_loader").hide('slow');
            $('#addresscheck').prop('disabled', false);
            $('#showsuccess').html('<div class="col-md-12"><div class="alert alert-dismissible alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Congratulation</strong> VDSL is available with Maximum Download '+data.min_download+' Mbps and Upload '+data.min_upload+' Mbps</div></div>');
            $('#d').attr("data-value", data.min_download);
            $('#u').attr("data-value", data.min_upload);
              $('.step-trigger[href="' + btn_href + '"]').click();
            $('#proximus_orderid').val(data.orderid);
            return false;

            }else{

              $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Oh snap!</strong> VDSL(2) is not available on this address</div></div>');
            }

          }else{
    $("#stepContent1").show('slow');
    $("#stepContent1_loader").hide('slow');
    $('#addresscheck').prop('disabled', false);
    $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Oh snap!</strong> '+data.message+'</div></div>');
          }

        },
        error: function(errorThrown) {
            console.log(errorThrown);
            $("#stepContent1").show();
            $("#stepContent1_loader").hide('slow');
            $('#addresscheck').prop('disabled', false);
        }
    });
});

$('#step4').click(function(){
     var packagename = $('#packageid').find(":selected").text();
     console.log(packagename);
     var modem = $('#modem').find(":selected").text();
      var migration = $('#migration').find(":selected").text();
       var firstname = $('#firstname').val();
       var lastname = $('#lastname').val();
        var customeremail = $('#email').val();
    $("#final_productname").html(packagename);
    $("#final_modem").html(modem);
   
    $("#final_order_type").html(migration);
    $("#final_customer_name").html(firstname+" "+lastname);
    $("#final_customer_email").html(customeremail);
  var btn_href = '#stepContent4';
                $('.step-trigger[href="' + btn_href + '"]').click();


});
$("#step3").click(function() {
    var userid = $('#customeridO').val();
    console.log(userid);
    if (userid > 0) {
        $.ajax({
            url: '<?php echo base_url(); ?>admin/complete/getcustomeridx',
            dataType: 'json',
            type: 'post',
            data: {
                'userid': userid
            },
            success: function(data) {
                console.log(data);

                $("#companyname").val(data.companyname);
                $("#firstname").val(data.firstname);
                $("#lastname").val(data.lastname);
                $("#email").val(data.email);
                $("#address1").val(data.address1);
                $("#city").val(data.city);
                $("#postcode").val(data.postcode);
                //$("#country").val(data.companyname);
                $("#phonenumber").val(data.phonenumber);
                //$("#companyname").val(data.companyname);
                var btn_href = '#stepContent3';
                $('.step-trigger[href="' + btn_href + '"]').click();
            },
            error: function(errorThrown) {}
        });
    }else{
var btn_href = '#stepContent3';
                $('.step-trigger[href="' + btn_href + '"]').click();
    }
});
$("#customertype").change(function() {
    var sel = $('#customertype').find(":selected").text();
    console.log(sel);
    if (sel == "Existing Customer") {
        $('#step3').prop('disabled', true);
        $('#existing').show('slow');
        $('#customeridO').val('');
    } else {
      $('#step3').prop('disabled', false);
        $('#existing').hide('slow');
        $('#customeridO').val('0');
        $("#companyname").val('');
        $("#firstname").val('');
                $("#lastname").val('');
                $("#email").val('');
                $("#address1").val('');
                $("#city").val('');
                $("#postcode").val('');
                //$("#country").val(data.companyname);
                $("#phonenumber").val('');
                //$("#companyname").val(data.companyname);
    }
});

$("#migration").change(function() {
    var sel = $('#migration').find(":selected").val();
    console.log(sel);
    if (sel == "yes") {
      $('#migrationoption').show('slow');

    } else {

 $('#migrationoption').hide('slow');

    }
});
$( "#voipnumber" ).keyup(function() {
   var cust = $('#customertype').find(":selected").val();
   var userid = $('#customeridO').val();
 var len = $(this).val();
if(len.length == 9){
   if(cust != 'old'){
 $('#step3').prop('disabled', false);
}else{

if(userid > 0){
$('#step3').prop('disabled', false);

}

}
}else if(len.length > 9){

  alert('The maximum lenght is 9');
  $('#step3').prop('disabled', true);
}else{
   $('#step3').prop('disabled', true);
}


});
$("#voipmove").change(function() {
    var sel = $('#voipmove').find(":selected").val();
    var cust = $('#customertype').find(":selected").val();
if (sel == "yes") {


      var len = $("#voipnumber").val();
if(len.length == 9){
 $('#step3').prop('disabled', false);
}



    $('#voipmoveoption').show('slow');
     $('#step3').prop('disabled', true);
    } else {
      $('#step3').prop('disabled', false);
      $('#voipmoveoption').hide('slow');

    }
});
</script>