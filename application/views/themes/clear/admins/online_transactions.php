<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Invoices'); ?>
            <div class="close">
                <!--
                <button class="btn btn-primary btn-rounded" data-target=".bd-exportpdf-modal-lg" data-toggle="modal"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>  <?php echo lang('Export Invoices'); ?> (pdf) </button>
                <button class="btn btn-rounded btn-success" data-target=".bd-export-modal-lg" data-toggle="modal"><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                <?php echo lang('Export Invoices (excel)'); ?> </button>
                -->
                <!-- <button class="btn btn-rounded btn-secondary" id="newinvoice"><i class="fa fa-plus-circle"></i>  <?php echo lang('New Invoices'); ?></button> -->
            </div>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                <?php echo lang('List Invoices'); ?>
                </h5>
                <div class="table-responsive">

                    <table class="table table-striped table-lightfont table-hover" id="portoutlist">
                        <thead>
                            <th> <div align="left"><?php echo lang('Processing Date'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Invoice'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Customer'); ?> </div></th>
                            <th> <div align="left"><?php echo lang('Method'); ?> </div></th>
                            <th> <div align="right"><?php echo lang('Amount'); ?> </div></th>
                            <th> <div align="right"><?php echo lang('Transid'); ?> </div></th>

                        </thead>

                    </table>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
 $('#portoutlist').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"pageLength": 50,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_online_transactions',
"aaSorting": [[0, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},

"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

//$('td:eq(0)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[7] + '">'+aData[0]+'</a>');

if ( aData[1].charAt( 0 ) == '8' ) {
$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/proforma/detailbyNumber/' + aData[1]+'">'+aData[1]+'</a>');

}else{
$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData[1] + '/'+aData[7]+'">'+aData[1]+'</a>');

}

$('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '">'+aData[2]+'</a>');
$('td:eq(3)', nRow).html('<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/'+aData[3]+'.png" height="20">');
$('td:eq(4)', nRow).html('<span class="text-success"><strong>€'+aData['4']+'</strong></span>');


return nRow;
},

});

});
});
</script>