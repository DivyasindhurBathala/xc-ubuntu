<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			<?php echo lang('Reminders'); ?>

			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('List Reminders Logs'); ?>
				<div class="float-right">
				<?php echo lang('Last successful run on'); ?>: <?php echo $setting->last_reminder_run; ?>
				</div>
				</h5>
				<div class="table-responsive">

					<table class="table table-striped table-lightfont table-hover" id="portoutlist">
						<thead>
							<th> <div align="left"><?php echo lang('Date'); ?> </div></th>
							<th> <div align="left"><?php echo lang('Customer'); ?> </div></th>
							<th> <div align="left"><?php echo lang('InvoiceNumber'); ?> </div></th>
							<th> <div align="left"><?php echo lang('Type Reminder'); ?> </div></th>
							<th> <div align="right"><?php echo lang('Amount'); ?> </div></th>
						</thead>

					</table>
				</div>
			</div>

		</div>
	</div>
</div>
<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
 $('#portoutlist').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"pageLength": 50,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_reminder',
"aaSorting": [[0, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},

"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData[2] + '/'+aData[8]+'">'+aData[2]+'</a>');
//$('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '">'+aData[2]+'</a>');
//$('td:eq(3)', nRow).html('<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/'+aData[3]+'.png" height="20">');
//$('td:eq(4)', nRow).html('<span class="text-success"><strong>€'+aData['4']+'</strong></span>');
return nRow;
},

});

});
});
</script>