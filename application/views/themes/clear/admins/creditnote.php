<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Creditnotes'); ?> <div class="float-right">
          <button class="btn btn-rounded btn-success" data-target=".bd-cn-modal-lg" data-toggle="modal"><i class="fa fa-file"></i>  <?php echo lang('Create CreditFactuur'); ?></button>
        </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
            <?php echo lang('List CreditNote'); ?>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="dt_cn">
            <thead>
              <tr>
                <th><?php echo lang('Number'); ?></th>
                <th><?php echo lang('Customer'); ?></th>
                <th><?php echo lang('Date'); ?></th>
                <th><?php echo lang('Total'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div aria-hidden="true" id="CNModal" aria-labelledby="CNModal" class="modal fade bd-cn-modal-lg" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Create Creditnote'); ?> (Pdf Created 15 minutes after creation)
        </h5>
        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
      </div>
      <div class="modal-body">
        <form id="cn">
          <input type="hidden" name="adminname" value="<?php echo $this->session->lastname . " " . $this->session->lastname; ?>">
          <input type="hidden" name="date" value="<?php echo date('Y-m-d'); ?>">
          <div class="form-group">
            <label for="exampleSelect1"><?php echo lang('Payment Method'); ?></label>
            <select class="form-control" id="exampleSelect1" name="paymentmethod">
              <option value="creditnote"><?php echo lang('CreditNote'); ?></option>
            </select>
          </div>
          <div class="form-group">
            <label for="invoicenum"><?php echo lang('Invoice Number'); ?></label>
            <input name="invoicenum" type="text"  class="form-control ui-autocomplete-input ui-autocomplete-loading invoicecreditnote" id="invoicenum" required>
            <small class="form-text text-muted"><?php echo lang('Search InvoiceNumber.'); ?></small>
          </div>
            <div class="row" id="hideme" style="display:none;">

             <div class="form-group col-md-4">
            <label for="invoicenum"><?php echo lang('Billing ID'); ?></label>
            <input name="iAddressNbr" type="text"  class="form-control" id="iAddressNbr" readonly>
            <small class="form-text text-muted"><?php echo lang('Billing ID.'); ?></small>
          </div>
          <div class="form-group  col-md-4">
            <label for="cName"><?php echo lang('Customer Name'); ?></label>
            <input name="cName" type="text"  class="form-control" id="cName" readonly>
            <small class="form-text text-muted"><?php echo lang('Customer Name'); ?></small>
          </div>
          <div class="form-group  col-md-4">
            <label for="mInvoiceAmount"><?php echo lang('Invoice Amount'); ?></label>
            <input name="mInvoiceAmount" type="text"  class="form-control" id="mInvoiceAmount" readonly>
            <small class="form-text text-muted"><?php echo lang('Invoice Amount'); ?></small>
          </div>
        </div>
          <div class="form-group">
            <label for="amount"><?php echo lang('Amount (€)'); ?> <?php echo lang('Incl. VAT'); ?></label>
            <input name="amount" type="number" step="any" class="form-control" id="amount" aria-describedby="emailHelp" placeholder="10.0" autocomplete="off" value="" required>
            <small class="form-text text-muted"><?php echo lang('Add amount to creditnote.'); ?></small>
          </div>
          <div class="form-group">
            <label for="description"><?php echo lang('Reason Creditnote'); ?></label>
            <input name="description" type="text" class="form-control" id="description" placeholder="<?php echo lang('Please specify the reason of credinote'); ?>" value="" required>
          </div>
          <div class="form-group">
            <button class="btn btn-success btn-sm btn-lg" id="create_cn" type="button" disabled><i id="push" class="fa fa-credit-card"></i> <?php echo lang('Publish Creditnote'); ?></button>
          </div>
        </form>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#dt_cn').DataTable({
   "autoWidth": false,
    "ajax": {
    "url": window.location.protocol + '//' + window.location.host + '/admin/table/getcreditnotes/<?php echo $this->session->cid; ?>',
    "dataSrc": ""
      },
    "aaSorting": [[0, 'desc']],
    "columns": [
            { "data": "iInvoiceNbr" },
            { "data": "cName" },
            { "data": "dInvoiceDate" },
            { "data": "dInvoiceDueDate" },
            { "data": "mInvoiceAmount" },
            { "data": "iInvoiceStatus" }
        ],
    "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

      $('td:eq(4)', nRow).html('€'+ aData.mInvoiceAmount);
      //$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/' + aData.iAddressNbr + '/'+aData.iInvoiceNbr+'">' + aData.cName + '</a>');
      //$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/creditnote/detail/' + aData.iInvoiceNbr + '/'+aData.iAddressNbr+'">' + aData.iInvoiceNbr + '</a>');
      $('td:eq(3)', nRow).html(aData.dInvoiceDueDate.slice(0, 10));
      $('td:eq(2)', nRow).html(aData.dInvoiceDate.slice(0, 10));
      $('td:eq(5)', nRow).html('<a href="<?php echo base_url(); ?>admin/creditnote/download/'+aData.iInvoiceNbr+'" class="btn btn-sm btn-primary"><i class="fa fa-download text-light"></i> <span class="text-light">Download</span></a> <a class="btn btn-sm btn-primary" onclick="sendCn('+aData.iInvoiceNbr+')"><i class="fa fa-envelope text-light"></i> <span class="text-light">Send</span></a> ');
              return nRow;
        },



  });
});
});



$('#create_cn').click(function(){
var amount = $('#amount').val();
var invoicenum = $('#invoicenum').val();
var description = $('#description').val();
var mInvoiceAmount = $('#mInvoiceAmount').val();
var iAddressNbr = $('#iAddressNbr').val();
var iInvoiceNbr = $('#iInvoiceNbr').val();
if(invoicenum.length == 0){
alert('Invoice Number must bee filled in');
return;
}else if(amount.length == 0){
  alert('Amount must be filled in');
  return;
}else if(description.length == 0){
  alert('Reason credit note must bee filled in');
  return;
}
$('#push').prop('disabled', true);
$('#push').removeClass('fa fa-credit-card').addClass("fa fa-spinner fa-spin");
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/creditnote/publish',
type: 'post',
dataType: 'json',
data: {
invoicenum:invoicenum,
mInvoiceAmount:mInvoiceAmount,
iInvoiceNbr:iInvoiceNbr,
iAddressNbr: iAddressNbr,
amount: amount,
description:description
},
success: function (data) {
  if(data.result == "success"){
    window.location.href = window.location.protocol + '//' + window.location.host + '/admin/creditnote/';
  }else{
    $('#push').removeClass('fa fa-spinner fa-spin').addClass("fa fa-credit-card");
    $('#push').prop('disabled', false);
    alert(data.message);

  }

},
error: function(XMLHttpRequest, textStatus, errorThrown) {
alert("<?php echo lang('Error  accour while sending your request'); ?>");

}
});

});

</script>
<script>
  function sendCn(invoiceid){
  console.log(invoiceid);
    $.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/creditnote/send',
type: 'post',
dataType: 'json',
data: {
invoiceid:invoiceid
},
success: function (data) {
  console.log(data);
  if(data.result){
    window.location.href = window.location.protocol + '//' + window.location.host + '/admin/creditnote/';
  }else{
    alert(data.message);

  }

},
error: function(XMLHttpRequest, textStatus, errorThrown) {
alert("<?php echo lang('Error  accour while sending your request'); ?>");

}
});
  }
</script>