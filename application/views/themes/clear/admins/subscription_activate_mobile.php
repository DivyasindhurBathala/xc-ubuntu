<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <div class="element-box">
                <h5 class="form-header">
                    <?php if ($service->orderstatus == "PortinRejected") {
    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-dismissible alert-warning">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <h4 class="alert-heading"><?php echo lang('Reason Rejection'); ?>:
                                </h4>
                                <p class="mb-0">
                                    <h6 class="text-danger"> <?php echo $service->details->porting_remark; ?>
                                    </h6>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php
}?>
                    <?php echo lang('Activate Simcard'); ?>
                    <a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>">
                        (<?php echo lang('Back to client summary'); ?>)</a>
                    <span class="text-success float-right">
                        <?php echo lang('Sim Status'); ?>:
                        <?php echo lang($service->orderstatus); ?>
                        <?php if ($service->orderstatus == "ActivationRequested" && $this->session->cid == "54") {
        ?>
                        <button class="btn btn-xs btn-primary" onclick="activate_now();"><i
                                class="fa fa-fire text-danger"></i> <?php echo lang('Activate Now'); ?></button>
                        <?php
    }?>
                    </span>

                </h5>
                <form id="target" method="post"
                    action="<?php echo base_url(); ?>admin/subscription/activate_mobile/<?php echo $this->uri->segment(4); ?>">
                    <input type="hidden" name="serviceid" value="<?php echo $this->uri->segment(4); ?>">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess12">
                                    <?php echo lang('Customerid'); ?></label>
                                <input type="text" value="<?php echo $client->mvno_id; ?>" class="form-control"
                                    disabled>
                                <div class="valid-feedback">
                                    <?php echo lang('Customerid'); ?>.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess12">
                                    <?php echo lang('Customer Name'); ?></label>
                                <input type="text" value="<?php echo $client->firstname . ' ' . $client->lastname; ?>"
                                    class="form-control" disabled>
                                <div class="valid-feedback">
                                    <?php echo lang('Customer Name'); ?>.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess12">
                                    <?php echo lang('Email'); ?></label>
                                <input type="text" value="<?php echo $client->email; ?>" class="form-control" disabled>
                                <div class="valid-feedback">
                                    <?php echo lang('Email'); ?>.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess12">
                                    <?php echo lang('Artilium ID'); ?></label>
                                <input type="text" value="<?php echo $client->id; ?>" class="form-control" disabled>
                                <div class="valid-feedback">
                                    <?php echo lang('Artilium ID'); ?>.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess12">
                                    <?php echo lang('SIMCARD'); ?>
                                    (<?php echo lang('enter 6 last digit, it will shows you the full digits'); ?></label>
                                <input type="text" autocomplete="new-username" id="simnumberx"
                                    value="<?php echo $service->details->msisdn_sim; ?>"
                                    class="form-control  ui-autocomplete-input is-valid pengkor" name="msisdn_sim">
                                <div class="valid-feedback">
                                    <?php echo lang('SIMCARD NUMBER'); ?>.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('SIMCARD MSISDN'); ?></label>
                                <input type="text" id="msisdn_sn"
                                    value="<?php echo trim($service->details->msisdn_sn); ?>"
                                    class="form-control is-valid" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleSelect1">
                                    <?php echo lang('Order (Number) Type'); ?></label>
                                <select class="form-control" id="msisdn_type" name="msisdn_type">
                                    <option value="new" <?php if ($service->details->msisdn_type == "new") {
        ?> selected
                                        <?php
    }?>>
                                        <?php echo lang('New Number'); ?>
                                    </option>
                                    <option value="porting" <?php if ($service->details->msisdn_type == "porting") {
        ?>
                                        selected <?php
    }?>>
                                        <?php echo lang('Porting'); ?>
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('Package Type'); ?>
                                    <span class="text-danger"></span></label>
                                <input type="text" value="<?php echo $service->packagename; ?>"
                                    class="form-control is-valid" readonly>
                                <div class="valid-feedback">
                                    <?php echo lang('Package Type Ordered'); ?>
                                </div>
                            </div>
                        </div>
                        <?php if ($setting->mobile_platform =="TEUM") {
        ?>
                        <?php if (getAddonsbySericeid($this->uri->segment(4))) {
            ?>
                        <?php foreach (getAddonsbySericeid($this->uri->segment(4)) as $key => $row) {
                ?>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('Package ID'); ?>
                                    <span class="text-danger"><?php echo $key+1; ?></span></label>
                                <input type="text" value="<?php echo $row->arta_bundleid; ?>"
                                    class="form-control is-valid" readonly>
                                <div class="valid-feedback">
                                    <?php echo $row->name; ?>
                                </div>
                            </div>
                        </div>

                        <?php
            } ?>
                        <?php
        } ?>

                        <?php
    } ?>
                        <?php if ($setting->mobile_platform =="ARTA") {
        ?>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="Promotion">
                                    <?php echo lang('Promotion Code'); ?></label>
                                <select class="form-control" id="promotion_code" name="promotion_code">
                                    <option value=""><?php echo lang('None'); ?>
                                    </option>
                                    <?php foreach (getPromotions($this->session->cid) as $row) {
            ?>
                                    <option value="<?php echo $row['promo_code']; ?>"
                                        <?php if ($service->promocode == $row['promo_code']) {
                ?> selected<?php
            } ?>>
                                        <?php echo $row['promo_name'] ?>
                                    </option>
                                    <?php
        } ?>

                                </select>
                            </div>
                        </div>
                        <?php
    } ?>
                        <?php if ($setting->mobile_platform =="ARTA") {
        ?>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('Contract Date'); ?>
                                    <span class="text-danger">MM-DD-YYYY</span></label>
                                <input type="text" value="<?php echo $service->date_contract; ?>"
                                    class="form-control is-valid" name="date_contract" id="pickdate20" readonly>
                                <div class="valid-feedback">

                                </div>
                            </div>
                        </div>
                        <?php
    } ?>
                    </div>
                    <div class="row" id="numbering" <?php if ($service->details->msisdn_type == "new") {
        ?>
                        style="display:none" <?php
    }?>>
                        <div class="col-md-4">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('DONOR MSISDN'); ?></label>
                                <input type="text" value="<?php echo $service->details->donor_msisdn; ?>"
                                    class="form-control is-valid" name="donor_msisdn">
                                <div class="valid-feedback"><?php echo lang('Enter number you wish to port'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('DONOR Provider'); ?></label>
                                <select class="form-control" id="donor_operator" name="donor_provider">
                                    <?php foreach (getDonors($setting->country_base) as $row) {
        ?>
                                    <option value="<?php echo $row->operator_code; ?>"
                                        <?php if ($service->details->donor_provider == $row->operator_code) {
            ?>
                                        selected<?php
        } ?>>
                                        <?php echo $row->operator_name; ?>
                                    </option>
                                    <?php
    }?>
                                </select>
                                <div class="valid-feedback">
                                    <?php echo lang('Choose the correct Provider'); ?>
                                </div>
                            </div>
                        </div>
                        <?php if ($setting->mobile_platform =="ARTA") {
        ?>
                        <div class="col-md-4">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('DONOR Type'); ?></label>
                                <select class="form-control" id="donor_type" name="donor_type">

                                    <option value="1" <?php if ($service->details->donor_type == "1") {
            ?> selected
                                        <?php
        } ?>><?php echo lang('POST PAID'); ?>
                                    </option>
                                    <option value="0" <?php if ($service->details->donor_type == "0") {
            ?> selected
                                        <?php
        } ?>><?php echo lang('PRE PAID'); ?>
                                    </option>

                                </select>
                                <div class="valid-feedback">
                                    <?php echo lang('Choose the correct Donor Type'); ?>
                                </div>
                            </div>
                        </div>




                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('DONOR SIMCARD Number'); ?></label>
                                <input type="text" value="<?php echo $service->details->donor_sim; ?>"
                                    class="form-control is-valid" name="donor_sim">
                                <div class="valid-feedback">
                                    <?php echo lang('Enter Current Simcard of your customer'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('DONOR Account Number'); ?></label>
                                <input type="text" value="<?php echo $service->details->donor_accountnumber; ?>"
                                    class="form-control is-valid" name="donor_accountnumber" id="donor_accountnumber">
                                <div class="valid-feedback text-danger">
                                    <?php echo lang('Enter Your Customer Account number at current provider (optional)'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group has-success">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('Wish Date'); ?>
                                    <span class="text-danger">YYYY-MM-DD</span></label>
                                <input type="text" id="pickdate8" value="<?php echo $service->details->date_wish; ?>"
                                    class="form-control is-valid" name="date_wish" readonly>
                                <div class="valid-feedback">
                                    <?php echo lang('Enter Your Port In Wish date'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <?php if ($service->details->msisdn_type == "porting" && in_array($service->orderstatus, array("ActivationRequested", "PortinAccepted"))) {
            ?>
                            <div id="loading">
                                <center>
                                    <img src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif"
                                        height="70" class="text-center">
                                </center>
                            </div>
                            <div class="form-group has-success actiondate" style="display:none;">
                                <label class="form-control-label" for="inputSuccess1">
                                    <?php echo lang('Action Date'); ?>
                                    <span class="text-danger">YYYY-MM-DD</span></label>
                                <input type="text" id="actiondatex" value="" class="form-control is-valid" readonly>
                                <div class="valid-feedback">
                                    <?php echo lang('the action date will take place'); ?>
                                </div>
                            </div>
                            <?php
        } ?>
                        </div>


                        <?php
    } ?>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button onclick="push_Activation()" type="button" class="btn btn-sm btn-success"
                                <?php if (in_array(trim($service->orderstatus), array("ActivationRequested", "OrderActive", "ActivationPending", "PortInWaitValidateVerificationCode","PortinPending", "PortinAccepted", "PortInValidateVerificationCodeAccepted", "Active"))) {
        ?>
                                disabled <?php
    }?>>

                                <?php if ($service->details->msisdn_type == "porting") {
        ?>
                                    <== <?php echo lang('Request PORTIN'); ?>==>
                                <?php
    } else {
        ?>
                                    <== <?php echo lang('Activate SIMCARD'); ?>==>
                                <?php
    } ?>
                               
                            </button>
                            <a href="<?php echo base_url(); ?>admin/subscription" class="btn btn-primary btn-sm"><i
                                    class="fa fa-cubes"></i>
                                <?php echo lang('Back to Subscription List'); ?></a>


                            <?php if ($service->details->msisdn_type == "porting") {
        ?>
                            <a class="btn btn-sm btn-success" target="_blank"
                                href="<?php echo base_url(); ?>admin/subscription/download_porting_letter/<?php echo $this->uri->segment(4); ?>"><i
                                    class="fa fa-file-pdf"></i>
                                <?php echo lang('Porting Letter'); ?></a>
                            <?php
    } else {
        ?>
                            <?php if ($setting->mobile_platform =="ARTA") {
            ?>
                            <a target="_blank"
                                href="<?php echo base_url(); ?>admin/subscription/download_welcome_letter/<?php echo $this->uri->segment(4); ?>"
                                class="btn btn-primary btn-sm"><i class="fa fa-file-pdf"></i>
                                <?php echo lang('Welcome Letter'); ?></a>
                            <?php
        } ?>
                            <?php
    }?>
                            <?php if ($this->session->cid == 54) {
        ?>
                            <button class="btn btn-success btn-sm" type="button"
                                onclick="SendSIMSentEmail('<?php echo $this->uri->segment(4); ?>');"><i
                                    class="fa fa-file-pdf"></i>
                                <?php echo lang('Send SIM Sent'); ?></button>

                            <button class="btn btn-success btn-sm" type="button"
                                onclick="SendPortingVerification('<?php echo $this->uri->segment(4); ?>');"><i
                                    class="fa fa-file-pdf"></i>
                                <?php echo lang('Send Reminder Verification'); ?></button>


                            <?php
    } ?>

                            <br />


                        </div>
                    </div>
                    <hr />

                    <div class="row">
                        <div class="col-md-12">
                            <?php if (in_array(trim($service->orderstatus), array("PortinPending", "PortinRejected", "ActivationError", "PortinCancelled", "PortinFailed"))) {
        ?>
                            <a type="button" class="btn btn-sm btn-success"
                                href="<?php echo base_url(); ?>client/nummerbehoud/request_new" target="_blank"><i
                                    class="fa fa-envelope"></i>
                                <?php echo lang('Request new SMS Code'); ?></a>
                            <a type="button" class="btn btn-sm btn-success"
                                href="<?php echo base_url(); ?>client/nummerbehoud" target="_blank"><i
                                    class="fa fa-envelope"></i>
                                <?php echo lang('Cofirm Porting Code'); ?></a>
                            <?php
    }?>
                            <?php if ($setting->mobile_platform =="ARTA") {
        ?>
                            <button type="button" class="btn btn-sm btn-success" id="updateContractDate"><i
                                    class="fa fa-recycle"></i>
                                <?php echo lang('Change Contract Date'); ?></button>
                            <?php if ($service->orderstatus == "ActivationPending" || $service->orderstatus == "ActivationRequested") {
            ?>
                            <?php echo lang('This Order has been passed activation process, please wait until this changes to Active'); ?>
                            <?php
        } ?>
                            <button type="button" class="btn btn-sm btn-danger" onclick="confirm_cancellation();"><i
                                    class="fa fa-trash"></i>
                                <?php echo lang('Cancel Order'); ?></button>
                            <?php if ($service->details->msisdn_type == "porting") {
            ?>
                            <button type="button" class="btn btn-sm btn-success" onclick="resetme()"><i
                                    class="fa fa-recycle"></i> <?php echo lang('Reset POD Counter'); ?></button>
                            <?php
        } ?>

                            <?php if (in_array($this->session->cid, array(53,56))) {
            ?>
                            <?php if ($service->details->msisdn_type == "porting") {
                ?>
                            <button class="btn btn-success btn-sm" type="button"
                                onclick="ConvertToNewNumber('<?php echo $this->uri->segment(4); ?>');"><i
                                    class="fa fa-file-pdf"></i>
                                <?php echo lang('Convert to New Activation'); ?></button>
                            <?php
            } ?>
                            <?php
        } ?>

                            <?php
    } ?>
                            <!--
							<?php if ($this->session->email == "simson.parlindungan@united-telecom.be") {
        ?>
							<i class="fa fa-user"></i>
							<?php echo $client->mageboid; ?>
							<a class="btn btn-success"
								href="<?php echo base_url(); ?>admin/subscription/fixbundle/<?php echo $this->uri->segment(4); ?>/2"><i
									class="fa fa-recycle"></i>
								<?php echo lang('Fix Bundle Include Arta'); ?></a>

							<a class="btn btn-success"
								href="<?php echo base_url(); ?>admin/subscription/fixbundle/<?php echo $this->uri->segment(4); ?>/3"><i
									class="fa fa-recycle"></i>
								<?php echo lang('Fix Bundle Without Arta'); ?></a>



							<?php if ($service->status == "Cancelled" and empty($service->details->msisdn)) {
            ?>
							<a href="<?php echo base_url(); ?>admin/subscription/delete_service/<?php echo $this->uri->segment(4); ?>"
								class="btn btn-success">Delete
								Service</a>
							<?php
        } ?>

							<?php
    }?>

							-->
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>

    <script>
    $("#promotion_code").change(function() {

        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/admin/subscription/change_promotion',
            type: 'POST',
            dataType: 'json',
            data: {
                serviceid: '<?php echo $this->uri->segment(4); ?>',
                promo_code: $('#promotion_code').val()
            },
            success: function(data) {

                if (data.result) {
                    alert(data.message);
                    //window.location.href= window.location.protocol + '//' + window.location.host + '/admin/subscription';
                } else {
                    alert(data.message);
                }
            }

        });

    });
    $("#msisdn_type").change(function() {
        if ($("#msisdn_type option:selected").val() == "porting") {
            $("#numbering").show();
        } else {
            $("#numbering").hide();
        }
    });

    $('#updateContractDate').click(function() {
        $('#update_contract_date').modal('toggle');


    });
    function ConvertToNewNumber(serviceid){

        var t = confirm('<?php echo lang('are you sure, this will remove the portin information but you still need to cancell ongoing portin from portinpending'); ?>');
        if(t){

            $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/admin/subscription/convert_order_portin_to_new_number',
            type: 'POST',
            dataType: 'json',
            data: {
                serviceid: serviceid
            },
            success: function(data) {

                if (data.result) {
                    alert(data.message);
                    //window.location.href= window.location.protocol + '//' + window.location.host + '/admin/subscription';
                } else {
                    alert(data.message);
                }
            }

        });
        }
    }
    function resetme() {

        $('#resetCounter').modal('toggle');
    }

    function activate_now() {

        $('#activateNow').modal("toggle");
    }

    function push_Activation() {



        var donor_type = $('#donor_type').find(":selected").val();
        var donor_accountnumber = $("#donor_accountnumber").val();
        var simcard = $("#simnumberx").val();

        var dw = $('#pickdate8').val();

        var msisdn_type = $("#msisdn_type option:selected").val();


        if (msisdn_type == "porting") {
            var date1 = '<?php echo date('Y-m-d H:i:s'); ?>';
            var date2 = dw + ' 00:00:00';
            var date1Updated = new Date(date1.replace(/-/g, '/'));
            var date2Updated = new Date(date2.replace(/-/g, '/'));
            //console.log(date1Updated >= date2Updated);

            if (date1Updated >= date2Updated) {
                alert('<?php echo lang('Please make sure your date wish is in the future more than 10 days'); ?> ' +
                    date1Updated + ' ' + date2Updated);
                $('#pickdate8').focus();
                return;
            }

        }


        if (simcard.length > 0) {
            if (donor_type == "1") {
                if (donor_accountnumber == '' || donor_accountnumber == '0') {
                    alert(
                        '<?php echo lang('Please fill Account Number if you choose Postpaid as Donor Type otherwise choose Pre Paid'); ?>'
                    );
                    $("#donor_accountnumber").focus();
                    return;
                } else {
                    $('#loadingController').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $("#target").submit();
                }
            } else {
                $('#loadingController').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $("#target").submit();
            }

        } else {
            alert('Please fill SIMCARD Number');
            $("#simnumberx").focus();
            return;
        }
    }

    function SendPortingVerification(id) {

        var serviceid = "<?php echo $this->uri->segment(4); ?>";
        var c = confirm(
            '<?php echo lang('are you sure ?, This will send out email to your customer'); ?>'
        );
        if (c) {

            window.location.href = window.location.protocol + '//' + window.location.host +
                '/admin/subscription/send_custom_email/service/' + id + '/portin_reminder_verification';


        }

    }

    function SendSIMSentEmail(id) {
        var serviceid = "<?php echo $this->uri->segment(4); ?>";
        var c = confirm(
            '<?php echo lang('are you sure ?, This will send out email to your customer'); ?>'
        );
        if (c) {

            window.location.href = window.location.protocol + '//' + window.location.host +
                '/admin/subscription/send_custom_email/service/' + id + '/custom_sim_shipped_email';


        }

    }

    function confirm_cancellation() {
        var serviceid = "<?php echo $this->uri->segment(4); ?>";
        var c = confirm(
            '<?php echo lang('are you sure ?, however if the order is a porting and status porting is not Porting Accepted, it wont cancel porting request'); ?>'
        );
        if (c) {

            $.ajax({
                url: window.location.protocol + '//' + window.location.host +
                    '/admin/subscription/cancel_service',
                type: 'POST',
                dataType: 'json',
                data: {
                    serviceid: serviceid
                },
                success: function(data) {

                    if (data.result) {
                        alert(data.message);
                        window.location.href = window.location.protocol + '//' + window.location.host +
                            '/admin/subscription';
                    } else {
                        alert(data.message);
                    }
                }

            });

        }
    }
    </script>
    <?php if ($service->details->msisdn_type == "porting" && in_array($service->orderstatus, array("ActivationRequested", "PortinAccepted"))) {
        ?>
    <script>
    $(document).ready(function() {
        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/admin/complete/get_porting_status/<?php echo $this->uri->segment(4); ?>/<?php echo $this->session->cid; ?>',
            dataType: 'json',
            success: function(data) {
                if (data.result) {
                    $('.actiondate').show();
                    $('#loading').hide();
                    $('#actiondatex').val(data.date);
                } else {
                    $('#loading').hide();
                    $('.actiondate').hide();
                }
            }
        });
        var msisdn_sim = $('#simnumberx').val();
        //console.log(msisdn_sim);
        if (msisdn_sim == "1111111111111111111") {
            $('#incorrectsimcard').modal('toggle');
        }
    });
    </script>
    <?php
    }?>
    <div class="modal fade" id="incorrectsimcard" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <?php echo lang('Change Contract date'); ?>
                    </h5>
                </div>
                <div class="modal-body">
                    <center>
                        <h1><?php echo lang('Please insert correct simcard'); ?></h1>
                    </center>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="activateNow" tabindex="-1" role="dialog" aria-labelledby="activateNow"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="post" action="<?php echo base_url(); ?>admin/subscription/activate_now">
                    <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
                    <input type="hidden" name="userid" value="<?php echo $service->userid; ?>">
                    <input type="hidden" name="msisdn" value="<?php echo $service->details->msisdn; ?>">
                    <input type="hidden" name="sn" value="<?php echo $service->details->msisdn_sn; ?>">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            <?php echo lang('Do you wish to activate this subscription now?, this will change the contract date to: '); ?><?php echo date('d/m/Y'); ?>
                        </h5>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-md btn-primary" type="submit"><?php echo lang('Save Changes'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="update_contract_date" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <form method="post" action="<?php echo base_url(); ?>admin/subscription/update_contractdate">
            <input type="hidden" name="serviceid" value="<?php echo $this->uri->segment(4); ?>">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">
                            <?php echo lang('Change contract date'); ?>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <center>
                            <div class="col-md-6">
                                <div class="form-group has-success">
                                    <label class="form-control-label" for="inputSuccess1">
                                        <?php echo lang('Contract Date'); ?>
                                        <span class="text-danger">MM-DD-YYYY</span></label>
                                    <input type="text" value="<?php echo $service->date_contract; ?>"
                                        class="form-control is-valid" name="date_contract" id="pickdate22" readonly>
                                    <div class="valid-feedback">
                                        <?php echo lang('Enter Your contract Date'); ?>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn-success"><?php echo lang('Save Changes'); ?></button>
                        </center>
                    </div>
                </div>
            </div>

        </form>
    </div>



    <div class="modal fade" id="loadingController" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">
                        <?php echo lang('Please wait....'); ?>
                    </h5>
                </div>
                <div class="modal-body">
                    <center>
                        <img id="loader" src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" />
                    </center>
                </div>
            </div>
        </div>
    </div>

    <div class="content-i usagex" style="display:none;">
        <div class="content-box">
            <div class="element-wrapper">
                <div class="element-box">
                    <h5 class="form-header">
                        <?php echo lang('Activate Simcard'); ?>
                        <span class="text-success close">
                            <?php echo lang('Sim Status'); ?>:
                            <?php echo lang($service->orderstatus); ?></span>
                    </h5>
                    <table class="table" id="bundlesx_table">
                        <thead>
                            <tr class="bg-primary  text-white"">
                          <th width=" 5%">
                                <?php echo lang('Type'); ?>
                                </th>
                                <th width="40%">
                                    <?php echo lang('Package'); ?>
                                </th>
                                <th width="20%">
                                    <?php echo lang('ValidFrom'); ?>
                                </th>
                                <th width="20%">
                                    <?php echo lang('Usage'); ?>
                                </th>
                                <th width="15%">
                                    <?php echo lang('Percentage'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tbody_bundlesx">
                        </tbody>
                    </table>
                    <div id="loadingx1" style="display:none;">
                        <center>
                            <img src="<?php echo base_url(); ?>assets/img/lg.rotating-balls-spinner.gif" height="100"
                                class="text-center">
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $('#loadingx1').show('slow');
        $('#loadingx2').show('slow');
        $('#loadingx3').show('slow');
        $('#loadingx4').show('slow');
        $('#loadingx5').show('slow');
        $.ajax({
            url: window.location.protocol + '//' + window.location.host +
                '/admin/complete/get_service_detail/<?php echo $this->uri->segment(4); ?>',
            dataType: 'json',
            success: function(data) {

                $('#loadingx1').hide('slow');
                $('#loadingx2').hide('slow');
                $('#loadingx3').hide('slow');
                $('#loadingx4').hide('slow');
                $('#loadingx5').hide('slow');
                if (data.bundles) {
                    //console.log(data);
                    $('.usagex').show();

                    $('#tbody_bundlesx').show();
                    //$('#datax').html(data.cdr.data + ' Bytes');
                    //$('#smsx').html(data.cdr.sms + ' sms');
                    //$('#voicex').html(data.cdr.voice);
                    if (data.bundles.length >= 1) {

                        data.bundles.forEach(function(b) {

                            if (b.Percentage > 90) {
                                var text = "text-danger";
                                var coli = "bg-danger";

                            } else {
                                var text = "text-light";
                                var coli = "bg-success";

                            }

                            $('#tbody_bundlesx').append(' <tr><td><i class="fa fa-' + b
                                .icon +
                                '"></i></td><td>' + b.szBundle +
                                '</td><td>' + b.ValidFrom + '</td> <td>' + b.UsedValue +
                                ' / ' + b.AssignedValue +
                                '</td><td><div class="progress" style="height:30px"><div style="height:30px" class="progress-bar ' +
                                coli + '" role="progressbar" style="width: ' + b
                                .Percentage + '%;" aria-valuenow="' + b.Percentage +
                                '" aria-valuemin="0" aria-valuemax="100"><span class="' +
                                text + '">' + b.Percentage +
                                '%</span></div> </div></td></tr>');
                        });
                    }
                    /*
                    else {
                    	if (data.bundles.Percentage > 90) {
                    		var text = "text-danger";
                    		var coli = "bg-danger";
                    	} else {
                    		var text = "text-light";
                    		var coli = "bg-success";

                    	}
                    	$('#tbody_bundlesx').append(' <tr><td><i class="fa fa-' + data.bundles.icon + '"></i></td><td>' + data.bundles
                    		.szBundle + '</td><td>' + data.bundles.ValidFrom + '</td> <td>' + data.bundles.UsedValue + ' / ' + data.bundles
                    		.AssignedValue +
                    		'</td><td><div class="progress" style="height:30px"><div style="height:30px" class="progress-bar ' + coli +
                    		'" role="progressbar" style="width: ' + data.bundles.Percentage + '%;" aria-valuenow="' + data.bundles.Percentage +
                    		'" aria-valuemin="0" aria-valuemax="100"><span class="' + text + '">' + data.bundles.Percentage +
                    		'%</span></div> </div></td></tr>');
                    }
                    */
                }

            }
        });
    });
    </script>

    <?php // print_r($setting);?>

    <div class="modal fade" id="resetCounter">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <h2><?php echo lang('Do you wish to reset the counter'); ?>?
                        <form method="post"
                            action="<?php echo base_url(); ?>admin/subscription/reset_porting_on_demand_counter">
                            <input type="hidden" name="serviceid" value="<?php echo $service->id; ?>">
                            <button class="btn btn-primary btn-md" type="submit">Reset</button>

                        </form>

                </div>

            </div>
        </div>
    </div>



    <div class="modal fade" id="RequestPorting">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post"
                    action="<?php echo base_url(); ?>admin/subscription/portingonDemand_RequestPorting/<?php echo $service->details->msisdn; ?>">
                    <input type="hidden" name="serviceid" id="<?php echo $service->id; ?>">
                    <input type="hidden" name="msisdn" id="0<?php echo substr($service->details->msisdn, 2); ?>">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h6>
                            <?php echo lang('Do you wish to re-send new Porting Code?, this will cancel the previous One'); ?>
                        </h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div id="details">

                        </div>

                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <?php echo lang('Request New Code'); ?></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <?php echo lang('Cancel'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ConfirmPorting">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="<?php echo base_url(); ?>admin/subscription/PortingonDemand_ConfirmPorting">
                    <input type="hidden" name="serviceid" id="<?php echo $service->id; ?>">
                    <input type="hidden" name="userid" id="<?php echo $service->userid; ?>">
                    <input type="hidden" name="msisdn" id="<?php echo $service->details->msisdn; ?>">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h6>
                            <?php echo lang('Please enter SMS code that Client received'); ?>
                            on <?php echo $service->details->msisdn; ?>
                        </h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div id="details row">
                            <div class="col-sm-12">
                                <div class="from-group">
                                    <label class="label-control"><?php echo lang('SMS Code'); ?></label>
                                    <input type="text" name="code" value="" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">
                            <?php echo lang('Confirm Code'); ?></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <?php echo lang('Cancel'); ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>