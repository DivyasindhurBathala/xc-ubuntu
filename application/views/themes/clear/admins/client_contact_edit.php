<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Client'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Edit Contact'); ?>
        </h5>
        <div class="table-responsive">
            <form method="post" action="<?php echo base_url(); ?>admin/client/contact_edit/<?php echo $this->uri->segment(4); ?>">
            	<input name="userid" value="<?php echo $contact['userid']; ?>" type="hidden">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="role"><?php echo lang('Role'); ?></label>
                      <input  name="role" class="form-control" id="role" type="text" placeholder="<?php echo lang('Role/Function'); ?>" value="<?php echo $contact['role']; ?>">
                    </fieldset>
                  </div>
                </div>

                  <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="role"><?php echo lang('Email Invoice'); ?></label>
                     <select class="form-control" id="invoiceemails"  name="invoiceemails" >
                        <?php foreach (array('0' => 'No', '1' => 'Yes') as $key => $em) {?>
                        <option value="<?php echo $key; ?>"<?php if ($key == $contact['invoiceemails']) {?> selected<?php }?>><?php echo $em; ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>

              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="firstname"><?php echo lang('Firstname'); ?></label>
                      <input  name="firstname" class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Firstname'); ?>" value="<?php echo $contact['firstname']; ?>"  required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="lastname"><?php echo lang('Lastname'); ?></label>
                      <input  name="lastname" class="form-control" id="lastname" type="text" placeholder="<?php echo lang('Lastname'); ?>" value="<?php echo $contact['lastname']; ?>" required>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="address1"><?php echo lang('Address'); ?></label>
                      <input  name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address'); ?>" value="<?php echo $contact['address1']; ?>" required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Postcode"><?php echo lang('Postcode'); ?></label>
                      <input  name="postcode" class="form-control" id="postcode" type="text" placeholder="<?php echo lang('Postcode'); ?>"  value="<?php echo $contact['postcode']; ?>" required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="City"><?php echo lang('City'); ?></label>
                      <input  name="city" class="form-control" id="city" type="text" value="<?php echo $contact['city']; ?>"  placeholder="<?php echo lang('City'); ?>" required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label for="exampleSelect1"><?php echo lang('Country'); ?></label>
                      <select class="form-control" id="country"  name="country" >
                        <?php foreach (getCountries() as $key => $country) {?>
                        <option value="<?php echo $key; ?>"<?php if ($key == $contact['country']) {?> selected<?php }?>><?php echo $country; ?></option>
                        <?php }?>
                      </select>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div class="row">


                <div class="col-sm-4">
                  <fieldset>
                    <label class="control-label" for="Phonenumber"><?php echo lang('Phonenumber'); ?></label>
                    <input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="<?php echo lang('Phonenumber'); ?>" value="<?php echo $contact['phonenumber']; ?>"  required>
                  </fieldset>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Email"><?php echo lang('Email'); ?></label>
                      <input name="email"  class="form-control" id="email" type="email" placeholder="<?php echo lang('Email address'); ?>" value="<?php echo $contact['email']; ?>"  required>
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="Gsm"><?php echo lang('Gsm'); ?>.</label>
                      <input name="gsm"  class="form-control" id="Gsm" type="number" value="<?php echo preg_replace('/\D/', '', $contact['gsm']); ?>"  placeholder="<?php echo lang('Gsm'); ?>">
                    </fieldset>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> <?php echo lang('Save'); ?></button>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <a href="#" class="btn btn-md btn-danger btn-block" onclick="confirm_delete(<?php echo $contact['id']; ?>);"><i class="fa fa-trash"></i> <?php echo lang('Delete'); ?></a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

		<script type="text/javascript">
		function confirm_delete(userid) {

		var answer = confirm("<?php echo lang('Do you wish to delete this contact'); ?>?");
		if (answer){

		window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/client/contact_delete/"+userid+"/<?php echo $contact['userid']; ?>");

		}
		}
		</script>