<!DOCTYPE html>
<html>

<head>
  <title>
    <?php echo $setting->companyname; ?> CRM</title>
  <meta charset="utf-8">
  <meta content="ie=edge" http-equiv="x-ua-compatible">
  <meta content="template language" name="keywords">
  <meta content="Tamerlan Soziev" name="author">
  <meta content="Admin dashboard html template" name="description">
  <meta content="width=device-width, initial-scale=1" name="viewport">

  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/clear/img/favicon.ico" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url(); ?>assets/clear/img/favicon.ico" type="image/x-icon">

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/clear/img/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/clear/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/clear/img/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/clear/img/site.webmanifest">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
    crossorigin="anonymous">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/select2/dist/css/select2.min.css"
    rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/dropzone/dist/dropzone.css"
    rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"
    rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/fullcalendar/dist/fullcalendar.min.css"
    rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css"
    rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/slick-carousel/slick/slick.css"
    rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/main.css?version=<?php echo time(); ?>"
    rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/custom.css?version=1.8" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/jquery-ui/jquery-ui.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/jquery-ui/jquery-ui.theme.min.css">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/icon_fonts_assets/feather/style.css"
    rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/component-custom-switch.css"
    rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/tempusdominus-bootstrap-4.min.css" />
  <!-- Tweaks for older IEs-->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  <script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/bower_components/moment/moment.js"></script>

    <?php if ($this->uri->segment(2) == "calendar") {
    ?>
        <?php
}?>
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/chat.css?version=1.1" rel="stylesheet">
   <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/animate.css?version=1.1" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/screen.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/emojionearea.min.css" rel="stylesheet">
    <?php if ($this->uri->segment(3) == "view_log") {
        ?>
  <script src="<?php echo base_url(); ?>assets/clear/js/jquery-timing.min.js"></script>
        <?php
    }?>

    <?php if ($this->uri->segment(2) == "masterdb") {
        ?>
        <?php
        foreach ($css_files as $file) : ?>
  <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
        <?php
    } ?>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/frame.css?version=1.1" />
   <link href="<?php echo base_url(); ?>assets/circle.css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <?php if ($this->uri->segment(3) == "map") {
        ?>
     <script src="https://maps.google.com/maps/api/js?key=AIzaSyBV9Brkc-wpC0r-rtUxKKnasBTx5LFlQoA" 
          type="text/javascript"></script>
        <?php
    } ?>
     <link href="<?php echo base_url(); ?>assets/<?php echo $setting->default_theme; ?>/css/jquery.dataTables.yadcf.css" rel="stylesheet">
     <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
        <?php if (!empty($this->session->id)) {
        ?>
  <script>
    $( document ).ready(function() {
  var socket = io.connect('https://socket.united-telecom.be');
  socket.on('connect', function() {
    var channel = <?php echo $this->session->cid; ?>;
    var pxm = "<?php echo $this->session->id; ?><?php echo trim($this->session->chatusername); ?>";
    var chatuser = '<?php echo $this->session->chatusername; ?>';
    var token = '<?php echo $this->encryption->encrypt($this->session->cid.'#'.$this->session->id."#".$this->session->chatusername.'#'.time()); ?>';
    socket.emit('subscribe', {"batch": true,"channel":channel,"token":token});
    socket.emit('subscribe', {"batch": true,"channel":chatuser,"token":token});
            <?php if (!empty($this->uri->segment(3))) {
            ?>
                <?php  if ($this->uri->segment(3) == "add_internet") {
                ?>
       socket.emit('subscribe', {"batch": true, "channel":pxm,"token":token});
       //console.log('joined :'+pxm);
                    <?php
            } ?>

                <?php
        } ?>

  });
   socket.on('chat', function (data) {
    console.log(data);
     chatWith(data.from);
      $("#chatbox_"+data.from+" .chatboxcontent").append('<strong>'+data.from+':</strong> '+data.message+'<br /><br />');
      $("#chatbox_"+data.from+" .chatboxcontent").scrollTop($("#chatbox_"+data.from+" .chatboxcontent")[0].scrollHeight);
      $.playSound(window.location.protocol + "//" + window.location.host + "/assets/notification_sound.mp3");
   });

      socket.on('pxm', function (data) {
      var location = JSON.parse(data.message);
      if(location.type == "address"){
         $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/checkaddress',
        dataType: 'json',
        type: 'post',
        data: {
        'action': 'preordering',
        'method': 'GetDetailedProductAvailability',
        'orderid': location.proximus_orderid,
        },
        success: function(data1) {
          console.log(data1);

        }
      });

      }
     
    if(location.type == "product"){
         $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/checkaddress',
        dataType: 'json',
        type: 'post',
        data: {
        'action': 'preordering',
        'method': 'GetDetailedNetworkAvailability',
        'orderid': location.proximus_orderid,
      },
        success: function(data1) {
          console.log(data1);

        }
      });

      }
       if(location.type == "network"){
  $.ajax({
        url: '<?php echo base_url(); ?>admin/complete/getXDSLResult',
        dataType: 'json',
        type: 'post',
        data: {
        'orderid': location.proximus_orderid,
      },
        success: function(data1) {
          console.log(data1);
            if(data1.result == "success"){
            if(data1.vdsl == "OK"){
            var btn_href = '#stepContent2';
            $("#stepContent1_loader").hide('slow');
            $('#addresscheck').prop('disabled', false);
            $('#showsuccess').html('<div class="col-md-12"><div class="alert alert-dismissible alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Congratulation</strong> VDSL is available with Maximum Download '+data1.min_download+' Mbps and Upload '+data1.min_upload+' Mbps</div></div>');
            $('#d').attr("data-value", data1.min_download);
            $('#u').attr("data-value", data1.min_upload);
            $('.step-trigger[href="' + btn_href + '"]').click();
            $('#proximus_orderid').val(data1.orderid);
            return false;
            }else{
                  $("#stepContent1_loader").hide('slow');
               $("#stepContent1").show('slow');
    
                $('#addresscheck').prop('disabled', false);
                $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Oh snap!</strong> VDSL(2) is not available on this address</div></div>');
            }

            }else{
              $("#stepContent1_loader").hide('slow');
               $("#stepContent1").show('slow');
    
                $('#addresscheck').prop('disabled', false);
               $('#errornya').html('<div class="col-md-12"><div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error</strong> '+data1.message+'</div></div>');

            }

          }
      });

       }
   });


  socket.on('notification', function (data) {
       var chatuser = '<?php echo $this->session->chatusername; ?>';
      $.ajax({
        url:  window.location.protocol + "//" + window.location.host + "/admin/auth/read_envelope/",
        dataType: 'json',
        type: 'post',
        data:{
          user: chatuser
        },
        success: function (data) {
          console.log(data);
        }
      });

     setTimeout(function() {
        $.bootstrapGrowl(data.message, {
            type: 'info',
            offset: {from: 'bottom', amount: 1}, // 'top', or 'bottom'
            align: 'right',
            width: 'auto',
            delay: 10000,
            stackup_spacing: 10,
            allow_dismiss: true
        });
      }, 1000);
  });
   });
</script>
            <?php
    } ?>

</head>
<body class="menu-position-side menu-side-left full-screen">
  <div id="load" style="display:none;"></div>
  <div class="all-wrapper solid-bg-all" id="loaders">
    <div class="layout-w">
      <!--------------------
        START - Mobile Menu
        -------------------->
      <div class="menu-mobile menu-activated-on-click color-scheme-light">
        <div class="mm-logo-buttons-w">
          <a class="mm-logo" href="<?php echo base_url(); ?>"><img src="<?php echo $setting->logo_site; ?>" height="70"><span>
                <?php echo $setting->companyname; ?></span></a>
          <div class="mm-buttons">
            <div class="content-panel-open">
              <div class="os-icon os-icon-grid-circles"></div>
            </div>
            <div class="mobile-menu-trigger">
              <div class="os-icon os-icon-hamburger-menu-1"></div>
            </div>
          </div>
        </div>
        <div class="menu-and-user">
          <div class="logged-user-w">
            <div class="avatar-w">
              <img alt="" src="<?php echo base_url(); ?>assets/img/staf/<?php echo $this->session->userdata('picture'); ?>">
            </div>
            <div class="logged-user-info-w">
              <div class="logged-user-name">
                <?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname'); ?>
              </div>
              <div class="logged-user-role">
                <?php echo $this->session->userdata('companyname'); ?>
              </div>
            </div>
          </div>
          <!--------------------
            START - Mobile Menu List
            -------------------->
            <?php $this->load->view('themes/' . $setting->default_theme . '/admins/menu_desktop');?>
          <!--------------------
            END - Mobile Menu List
            -------------------->
          <div class="mobile-menu-magic">
            <h4>
                <?php echo $setting->companyname; ?>
            </h4>

          </div>



        </div>
      </div>
      <!--------------------
        END - Mobile Menu
        -------------------->
      <!--------------------
        START - Main Menu
        -------------------->
      <div class="menu-w color-scheme-light color-style-transparent menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
       <div style="background: linear-gradient(to bottom right, #008ecf, #1d609a);">
        <img class="mm-logo" src="<?php echo $setting->logo_site; ?>" height="70">
      </div>
        <div class="logged-user-w avatar-inline">
          <div class="logged-user-i">
            <div class="avatar-w">
              <img alt="" src="<?php echo base_url(); ?>assets/img/staf/<?php echo $this->session->userdata('picture'); ?>">
            </div>
            <div class="logged-user-info-w">
              <div class="logged-user-name">
                <?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname'); ?>
              </div>
              <div class="logged-user-role">
                <?php echo $this->session->userdata('role'); ?>
              </div>
            </div>
            <div class="logged-user-toggler-arrow">
              <div class="os-icon os-icon-chevron-down"></div>
            </div>
            <div class="logged-user-menu color-style-bright">
              <div class="logged-user-avatar-info">
                <div class="avatar-w">
                  <img alt="" src="<?php echo base_url(); ?>assets/img/staf/<?php echo $this->session->userdata('picture'); ?>">
                </div>
                <div class="logged-user-info-w">
                  <div class="logged-user-name">
                    <?php echo $this->session->userdata('firstanme') . ' ' . $this->session->userdata('lastname'); ?>
                  </div>
                  <div class="logged-user-role">
                    <?php echo $this->session->userdata('role'); ?>
                  </div>
                </div>
              </div>
              <div class="bg-icon">
                <i class="os-icon os-icon-wallet-loaded"></i>
              </div>
              <ul>
                <li>
                  <a href="<?php echo base_url(); ?>admin/dashboard/myaccount"><i class="os-icon os-icon-user-male-circle2"></i><span>
                        <?php echo lang('Profile Setting'); ?></span></a>
                </li>
                <li>
                  <a href="<?php echo base_url(); ?>admin/auth/logout"><i class="os-icon os-icon-signs-11"></i><span>
                        <?php echo lang('Logout'); ?></span></a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <h1 class="menu-page-header">
            <?php echo lang('Page Header'); ?>
        </h1>
        <?php $this->load->view('themes/' . $setting->default_theme . '/admins/menu_desktop');?>
        <div class="side-menu-magic">
          <h4>
            <?php echo $setting->companyname; ?>
          </h4>

        </div>

        <div class="row">
            <?php
            $online = get_online_admin();
            ?>
            <?php //if($this->session->email =='simson.parlindungan@united-telecom.be'){?>
          <div id="frame">
            <div id="sidepanel">
              <div id="contacts">
                <ul>
                    <?php if ($online) {
                ?>
                        <?php foreach ($online as $row) {
                    ?>
                            <?php if ($row->id != $this->session->id) {
                        ?>
                  <li class="contact">
                    <a href="#" onclick="chatWith('<?php echo $row->username; ?>');">
                      <div class="wrap">
                        <span class="contact-status <?php if (time()-$row->lastseen <= 600) {
                            ?>online<?php
                        } ?>"></span>
                        <img src="<?php echo base_url(); ?>assets/img/staf/<?php echo $row->picture; ?>" alt="" />
                        <div class="meta">
                          <p class="name">
                                <?php echo $row->name; ?><br /><small>
                                <?php echo convert_seconds($row->lastseen); ?></small></p>
                          <p class="preview"></p>
                        </div>
                      </div>
                    </a>
                  </li>
                                <?php
                    } ?>
                            <?php
                } ?>
                        <?php // }?>
                </ul>
              </div>
              <!-- <div id="bottom-bar">
            <button id="addcontact"><i class="fa fa-user-plus fa-fw" aria-hidden="true"></i> <span>Add contact</span></button>
            <button id="settings"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> <span>Settings</span></button>
    </div>
  -->
            </div>
          </div>





                        <?php
            } ?>
        </div>

      </div>
      <!--------------------
        END - Main Menu
        -------------------->
      <div class="content-w">
        <!--------------------
          START - Top Bar
          -------------------->
        <div class="top-bar color-scheme-transparent">
         <!-- <h6 class="text-danger showcount" style="display:block;background-color: #eef442;">Planned IoT Upgrade on 02/07 - 03/07, service maybe disrupted during this time  <img src="<?php echo base_url(); ?>assets/img/hb.gif" height="20"></h6>  -->
          <!--------------------
            START - Top Menu Controls
            -------------------->
          <div class="top-menu-controls">
            <?php if ($this->session->master) {
                ?>

            <div class="form-group element-switch float-left" style="width: 300px;">
              <select class="custom-select" id="companychange">
               
                <?php foreach (getCompanieMvno() as $comp) {
                    ?>
                    <?php if ($comp->companyid == $this->session->cid) {
                        ?>
                <option value="<?php echo $comp->id; ?>" selected>
                        <?php echo $comp->companyname; ?> #<?php echo $comp->companyid; ?>
                </option>
                        <?php
                    } else {
                        ?>
  <option value="<?php echo $comp->id; ?>">
    <?php echo $comp->companyname; ?> #<?php echo $comp->companyid; ?>
                </option>
    <?php
                    } ?>
                    <?php
                } ?>
              </select>
            </div>
            <script>
              $('#companychange').change(function() {
      var sel = $('#companychange').find(":selected").val();
      window.open(
  '<?php echo base_url(); ?>admin/setting/switch_company/'+sel,
  '_blank' // <- This is what makes it open in a new window.
);

    //  window.location.href= "<?php echo base_url(); ?>admin/setting/switch_company/"+sel;
    });
    </script>
                <?php
            } ?>
            <div class="element-search" style="width: 450px;">
              <input class="search ui-autocomplete-input ui-autocomplete-loading clientdetails" placeholder=" <?php echo lang('Start typing to search...'); ?>"
                type="text">
            </div>
            <!--------------------
              START - Messages Link in secondary top menu
              -------------------->
            <div class="messages-notifications os-dropdown-trigger os-dropdown-position-left">
              <i class="os-icon os-icon-mail-14"></i>
              <div class="new-messages-count" id="newnoti">
                0
              </div>
              <div class="os-dropdown light message-list">
                <ul id="event_items">

                </ul>

              </div>
            </div>
            <div class="logged-user-w">
              <div class="logged-user-i">
                <?php foreach (explode(',', $setting->supported_language) as $lang) {
                ?>
                <div class="avatar-w<?php if ($this->session->language != $lang) {
                    ?> close<?php
                } ?>">
                  <a href="<?php echo base_url(); ?>admin/dashboard/switchlang/<?php echo $lang; ?>">
                  <img alt="" src="<?php echo base_url(); ?>assets/clear/img/flags-icons/<?php echo $lang; ?>.png">
                </a>
                </div>
                    <?php
            } ?>
              </div>
            </div>
            <!--------------------
              END - User avatar and menu in secondary top menu
              -------------------->
          </div>
          <!--------------------
            END - Top Menu Controls
            -------------------->
        </div>
        <!--------------------
          END - Top Bar
          -------------------->
        <!--------------------
          START - Breadcrumbs
          -------------------->
        <ul class="breadcrumb">
            <?php foreach (extend_uri() as $uri) {
                ?>
          <li class="breadcrumb-item">
                <?php echo ucfirst($uri); ?>
          </li>
                <?php
            }?>

        </ul>
       
       

        <!--------------------
          END - Breadcrumbs
          -------------------->
        <?php if (!empty($this->session->flashdata('success'))) {
                ?>

         <div class="row">
            <div class="col-sm-12">
        <div class="container-fluid">
         
              <div class="alert alert-success" role="alert">
                <strong>
                  <?php echo $this->session->flashdata('success'); ?></strong>
              </div>
            </div>
          </div>
        </div>
        <script>
          /*
        setTimeout(function() {
        $.bootstrapGrowl('<?php echo $this->session->flashdata('success'); ?>', {
            type: 'success',
            offset: {from: 'bottom', amount: 1}, // 'top', or 'bottom'
            align: 'right',
            width: 'auto',
            delay: 10000,
            stackup_spacing: 10,
            allow_dismiss: true
        });
      }, 2000);
      */
        </script>

            <?php
            }?>
        <?php if (!empty($this->session->flashdata('error'))) {
                ?>
            <script>
              /*
        setTimeout(function() {
        $.bootstrapGrowl(' <?php echo $this->session->flashdata('error'); ?>', {
            type: 'danger',
            offset: {from: 'bottom', amount: 1}, // 'top', or 'bottom'
            align: 'right',
            width: 'auto',
            delay: 10000,
            stackup_spacing: 10,
            allow_dismiss: true
        });
      }, 2000);
      */
        </script>


        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="alert alert-danger" role="alert">
                <strong>
                  <?php echo $this->session->flashdata('error'); ?></strong>
              </div>
            </div>
          </div>
        </div>
   
            <?php
            }?>


<script>
  /*
// Set the date we're counting down to
var countDownDate = new Date("Jul 2, 2019 18:00:00").getTime();
//$('.showcount').show();
// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
*/
</script>