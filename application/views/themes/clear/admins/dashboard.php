<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Welcome to your dashboard'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
            <?php echo lang('Dashboard Statistics'); ?>
        </h5>
        <div class="form-desc">
        </div>
        <div class="element-content">
          <div class="row">

             <div class="col-sm-6">
               <div id="columnchart" style="width: 100%; height: 180px;"></div>
                <?php if ($serv) {
    ?>
               <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([<?php echo implode(',', $serv); ?>]);
        var options = {
          chart: {
            title: '<?php echo lang('New Orders'); ?>'
          },
          legend: {position: 'none'},
           is3D:true
        };
        var chart = new google.charts.Bar(document.getElementById('columnchart'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
                <?php
} ?>
             </div>
            <div class="col-sm-6">
               <div id="columnchart_material" style="width: 100%; height: 180px;"></div>
                <?php if ($gr) {
        ?>
      <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([<?php echo implode(',', $gr); ?>]);
        var options = {
          chart: {
            title: ' <?php echo lang('New Clients of'); ?> <?php echo $stats['client']; ?>  <?php echo lang('total clients'); ?>'

            /* subtitle: 'Total Client:' */
          },
          legend: {position: 'none'},
          colors: ['green','green'],
          is3D:true
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));
        var view = new google.visualization.DataView(data);
view.setColumns([0, 1, {
    type: 'string',
    role: 'annotation',
    sourceColumn: 1,
    calc: 'stringify'
}]);

        chart.draw(view, google.charts.Bar.convertOptions(options));
      }
    </script>
                <?php
    } ?>

            </div>


          </div>


          <div class="row">
            <div class="col-sm-3">
              <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/subscription">
                <div class="label">
                    <?php echo lang('Total'); ?>
                    <?php echo lang('Active Subscriptions'); ?>
                </div>
                <div class="value">
                    <?php echo $stats['subscriptions']['active']; ?>
                </div>
                <div class="trending trending-up-basic">
                  <span></span><i class="os-icon os-icon-arrow-up2"></i>
                </div>
              </a>
            </div>
            <div class="col-sm-3">
              <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/subscription/pendingorders">
                <div class="label">
                    <?php echo lang('Order Need Action'); ?>
                </div>
                <div class="value">
                    <?php echo $stats['subscriptions']['new']; ?>
                </div>
                <div class="trending trending-down-basic">
                  <!-- <span><?php echo round($stats['percentunpaid'], 2); ?>% -->
                  </span><i class="os-icon os-icon-arrow-down"></i>
                </div>
              </a>
            </div>

            <div class="col-sm-3">
              <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/subscription/index/pendingactivation">
                <div class="label">
                    <?php echo lang('Pending Activation'); ?>
                </div>
                <div class="value">
                    <?php echo $stats['subscriptions']['pending']; ?>
                </div>
                <div class="trending trending-down-basic">
                  <!--    <span><?php echo round($stats['percentpaid'], 2); ?>%</span><i class="os-icon os-icon-arrow-down"></i> -->
                </div>
              </a>
            </div>

            <div class="col-sm-3">
              <a class="element-box el-tablo text-danger" id="portani" href="<?php echo base_url(); ?>admin/subscription/pending_porting_out">
                <div class="label">
                    <?php echo lang('Port Out Request'); ?>
                </div>
                <div class="value portout">
                   0
                </div>
                <div class="trending trending-down-basic">
                  </span><i class="os-icon os-icon-arrow-down"></i>
                </div>
              </a>
            </div>
            <div class="col-sm-3">
              <a class="element-box el-tablo text-success"  href="<?php echo base_url(); ?>admin/agent">
                <div class="label">
                    <?php echo lang('Resellers'); ?>
                </div>
                <div class="value">
                   <?php echo get_reseller_counter($this->session->cid); ?>
                </div>
                <div class="trending trending-down-basic">
                  </span><i class="os-icon os-icon-arrow-down"></i>
                </div>
              </a>
            </div>
    <?php if($setting->mobile_platform == "TEUM"){ ?>


      <div class="col-sm-3">
    <a class="element-box el-tablo <?php if( get_freesimcard_teum($this->session->cid) >= 100) { ?>text-success<?php } else { ?>text-danger<?php } ?>"  href="#">
                <div class="label">
                    <?php echo lang('Free Simcard Catalogue'); ?>
                </div>
                <div class="value">

                   <?php echo get_freesimcard($this->session->cid); ?>
                </div>
                <div class="trending trending-down-basic">
                  </span><i class="os-icon os-icon-arrow-down"></i>
                </div>
              </a>
            </div>
    <?php } ?>

    <?php if($setting->mobile_platform == "ARTA"){ ?>

      <div class="col-sm-3">
    <a class="element-box el-tablo <?php if( get_freesimcard_arta($this->session->cid) >= 100) { ?>text-success<?php } else { ?>text-danger<?php } ?>"  href="#">
                <div class="label">
                    <?php echo lang('Free Simcard Catalogue'); ?>
                </div>
                <div class="value">

                   <?php echo get_freesimcard_arta($this->session->cid); ?>
                </div>
                <div class="trending trending-down-basic">
                  </span><i class="os-icon os-icon-arrow-down"></i>
                </div>
              </a>
            </div>

    <?php } ?>

          </div>
            <?php if ($setting->mage_invoicing) {
        ?>
            <div class="row">
               <div class="col-sm-3">
              <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/invoice">
                <div class="label">
                    <?php echo lang('Avarage Invoice Amount'); ?>
                </div>
                <div class="value">
                    <?php echo $setting->currency; ?><?php echo number_format($invoicesum->avg, 2); ?>
                </div>
                <div class="trending trending-down-basic">
                  <!-- <span><?php echo round($stats['percentunpaid'], 2); ?>% -->
                  </span><i class="os-icon os-icon-arrow-down"></i>
                </div>
              </a>
            </div>

             <div class="col-sm-3">
              <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/invoice">
                <div class="label">
                    <?php echo lang('Total Invoices Created'); ?>
                </div>
                <div class="value">
                    <?php echo $invoicesum->total; ?>
                </div>
                <div class="trending trending-down-basic">
                  <!-- <span><?php echo round($stats['percentunpaid'], 2); ?>% -->
                  </span><i class="os-icon os-icon-arrow-down"></i>
                </div>
              </a>
            </div>

              <div class="col-sm-3">
              <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/invoice">
                <div class="label">
                    <?php echo lang('Unpaid'); ?>
                    <?php echo lang('Invoices'); ?> (
                    <?php echo date('Y'); ?>)
                </div>
                <div class="value">
                  <?php echo $setting->currency; ?>
                    <?php echo $stats['total_amount_unpaid']; ?>
                </div>
                <div class="trending trending-down-basic">
                  <!-- <span><?php echo round($stats['percentunpaid'], 2); ?>% -->
                  </span><i class="os-icon os-icon-arrow-down"></i>
                </div>
              </a>
            </div>

            <div class="col-sm-3">
              <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/invoice">
                <div class="label">
                    <?php echo lang('Total Paid Invoices'); ?> (
                    <?php echo date('Y'); ?>)
                </div>
                <div class="value">
                <?php echo $setting->currency; ?>
                    <?php echo $stats['total_this_year_paid']; ?>
                </div>
                <div class="trending trending-down-basic">
                  <!--    <span><?php echo round($stats['percentpaid'], 2); ?>%</span><i class="os-icon os-icon-arrow-down"></i> -->
                </div>
              </a>
            </div>

            </div>
            <div class="row">
               <div class="col-sm-3">
               <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/invoice/invoice_reminders">
                <div class="label">
                <?php echo lang('Today Reminder 1'); ?>
                </div>
                <div class="value">

                    <?php echo $stats['reminder1']; ?>
                </div>
                <div class="trending trending-down-basic">
                  <!--    <span><?php echo round($stats['percentpaid'], 2); ?>%</span><i class="os-icon os-icon-arrow-down"></i> -->
                </div>
              </a>
               </div>
               <div class="col-sm-3">
               <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/invoice/invoice_reminders">
                <div class="label">
                <?php echo lang('Today Reminder 2'); ?>
                </div>
                <div class="value">

                    <?php echo $stats['reminder2']; ?>
                </div>
                <div class="trending trending-down-basic">
                  <!--    <span><?php echo round($stats['percentpaid'], 2); ?>%</span><i class="os-icon os-icon-arrow-down"></i> -->
                </div>
              </a>
               </div>
               <div class="col-sm-3">
               <a class="element-box el-tablo" href="<?php echo base_url(); ?>admin/invoice/invoice_reminders">
                <div class="label">
                    <?php echo lang('Today Reminder 3'); ?>
                </div>
                <div class="value">

                    <?php echo $stats['reminder3']; ?>
                </div>
                <div class="trending trending-down-basic">
                  <!--    <span><?php echo round($stats['percentpaid'], 2); ?>%</span><i class="os-icon os-icon-arrow-down"></i> -->
                </div>
              </a>
               </div>
               </div>
            <div class="row">
              <div class="col-sm-12">
                 <div id="columnchart_invoices" style="width: 100%; height: 200px;"></div>
              </div>
            </div>
            <?php
    } ?>

        </div>

      </div>
    </div>
  </div>
</div>

<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Pending Orders Need Action'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">

        </h5>
        <div class="table-responsive">

          <table class="table table-striped table-lightfont" id="pendingOrders">
            <thead>
              <tr>
                <th width="5%">
                    <?php echo lang('ID'); ?>
                </th>
                <th width="5%">
                    <?php echo lang('Identifiers'); ?>
                </th>
                <th width="15%">
                    <?php echo lang('CustomerName'); ?>
                </th>
                <th width="10%">
                    <?php echo lang('Date'); ?>
                </th>
                <th width="15%">
                    <?php echo lang('Package'); ?>
                </th>
                <th width="50%">
                    <?php echo lang('Status'); ?>
                </th>

              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function()
{
    <?php if ($setting->mobile_platform == "ARTA") {
        ?>
  $.ajax({
           url: '<?php echo base_url(); ?>admin/complete/get_pending_port_out_pending/',
           dataType: 'json',
           success: function (data) {
              var ignore='<?php echo $this->session->disable_dashboard_notification; ?>';
              if(data.total > 0){
                $("#portani").addClass("animated flash infinite");
              }
               $(".portout").html(data.total);
              if(ignore != 1){
              if(data.total >= 1){


              $('#portoutpending').modal({backdrop: 'static', keyboard: false});
              $('#portoutx').html('<h3 class="text-danger"> <?php echo lang("Do you know processing portout must be done as soon as it arrived?, if they aren\'t allowed yet, please rejected them. At this moment you have"); ?> '+data.total+'  <?php echo lang('pending request(s)'); ?></h3>');
              }
              }
           },

       });

    <?php
    } ?>


$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
                        $('#pendingOrders').DataTable({
                        "autoWidth": false,
                        "processing": true,
                        "orderCellsTop": true,
                        "ordering": true,
                        "serverSide": true,
                        "pageLength": 50,
                        "ajax":window.location.protocol + '//' + window.location.host + '/admin/table/get_pending_orders',
                        "aaSorting": [[0, 'desc']],
                        "language": {
                        "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
                        },
                        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                          $('td:eq(2)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
                          if(aData[9] == "54"){
                            if(aData[11] == "1"){
if(aData[10] == "Paid"){
                                 $('td:eq(2)', nRow).html('<a class="text-success" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');

                               }else{
                                  $('td:eq(2)', nRow).html('<a class="text-primary" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
                               }


                            }else{
                            if(aData[10] == "Paid"){
                               $('td:eq(2)', nRow).html('<i class="fas fa-exclamation-circle"></i> <a class="text-success" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
                             }else{
                                $('td:eq(2)', nRow).html('<i class="fas fa-exclamation-circle"></i> <a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
                             }
                            }

                            if(aData['8'].length <= 0){

                              $('td:eq(5)', nRow).html('<button type="button" class="btn btn-sm btn-primary" onclick="openMe(\''+aData[0]+'\');"><?php echo lang('Accept'); ?></button> <button type="button" class="btn btn-sm btn-warning" onclick="openMeEdit(\''+aData[0]+'\');"><?php echo lang('Modify'); ?></button> <button type="button" class="btn btn-sm btn-danger" onclick="openMeReject(\''+aData[0]+'\');"><?php echo lang('Reject'); ?></button> <a href="<?php echo base_url(); ?>admin/client/edit/'+aData[6]+'" target="_blank" class="btn btn-sm btn-warning"><?php echo lang('No BSN'); ?></a> <button type="button" class="btn btn-sm btn-danger" onclick="openLastReminderBank(\''+aData[0]+'\');"><?php echo lang('Last Reminder Id/Bank'); ?></button> <button type="button" class="btn btn-sm btn-danger" onclick="openReminderBank(\''+aData[0]+'\');"><?php echo lang('Reminder Id/Bank'); ?></button>');

                            }else{

                              $('td:eq(5)', nRow).html('<button type="button" class="btn btn-sm btn-primary" onclick="openMe(\''+aData[0]+'\');"><?php echo lang('Accept'); ?></button> <button type="button" class="btn btn-sm btn-warning" onclick="openMeEdit(\''+aData[0]+'\');"><?php echo lang('Modify'); ?></button> <button type="button" class="btn btn-sm btn-danger" onclick="openMeReject(\''+aData[0]+'\');"><?php echo lang('Reject'); ?></button>');


                              }

                          if(aData[10] == "Paid"){

                                 $('td:eq(0)', nRow).html('<strong><span class="text-success">'+aData[0]+'</span></storng>');
                                // $('td:eq(2)', nRow).html('<a class="text-success" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><strong>' + aData[2] + '</strong></a>');
                                 $('td:eq(3)', nRow).html('<strong><span class="text-success">'+aData[3]+'</span></storng>');
                                 $('td:eq(4)', nRow).html('<strong><span class="text-success">'+aData[4]+'</span></storng>');

                              }
                          }else{


                            $('td:eq(5)', nRow).html('<button type="button" class="btn btn-sm btn-primary" onclick="openMe(\''+aData[0]+'\');"><?php echo lang('Accept'); ?></button> <button type="button" class="btn btn-sm btn-warning" onclick="openMeEdit(\''+aData[0]+'\');"><?php echo lang('Modify'); ?></button> <button type="button" class="btn btn-sm btn-danger" onclick="openMeReject(\''+aData[0]+'\');"><?php echo lang('Reject'); ?></button>');

                          }
                           return nRow;
                        },

                        });
                        }
                    );
});


</script>
<script>
  function openReminderBank(id){


    var c = confirm(' <?php echo lang('are you sure ?, This will send out email to your customer for Reminder ID & Bank'); ?>');
    if(c){
          window.location.href= window.location.protocol + '//' + window.location.host + '/admin/subscription/send_custom_email/service/'+id+'/custom_id_bank_reminder';

    }
}

function openLastReminderBank(id){


    var c = confirm(' <?php echo lang('are you sure ?, This will send out email to your customer for Last Reminder ID & Bank'); ?>');
    if(c){

          window.location.href= window.location.protocol + '//' + window.location.host + '/admin/subscription/send_custom_email/service/'+id+'/custom_last_id_bank_reminder';


    }
}
  function openMeEdit(id)
{

    location.href = "<?php echo base_url(); ?>admin/subscription/edit_order/"+id;
}
function openMe(id)
{
    console.log(id);
    $('#accept_order').modal('toggle');
    $('#acceptx').prop('disabled', true);
    $.ajax({
           url: '<?php echo base_url(); ?>admin/complete/getorderdetails/'+id,
           type: 'post',
           dataType: 'json',
           success: function (data) {
             console.log(data);
             if(data.msisdn_sim){
                if(data.msisdn_sim == '1111111111111111111'){
                  $('#acceptx').prop('disabled', true);
                  } else{
                    $('#acceptx').prop('disabled', false);
                  }
                $("#simnumberx").val(data.msisdn_sim);

              }else{
                $('#acceptx').prop('disabled', true);

              }
             $("#details").html(data.result);
             $("#serviceidx").val(data.serviceid);
              $("#useridx").val(data.userid);
           },
           data: {'orderid': id}
       });
}

function openMeReject(id)
{
    $('#reject_order').modal('toggle');

    $.ajax({
            url: '<?php echo base_url(); ?>admin/complete/getorderdetails',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                $("#detailsx").html(data.result);
                $("#serviceid").val(data.serviceid);
            },
            data: {'orderid': id}
        });
}


</script>
<div class="modal fade" id="reject_order">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/reject_mobile">
        <input type="hidden" name="serviceid" id="serviceid">
        <input type="hidden" name="action" value="reject">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Review Before Accept'); ?>
          </h4>

        </div>

        <!-- Modal body -->
        <div class="modal-body" id="detailsx">

        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Reject Order'); ?></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="portoutpending">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

        <input type="hidden" name="serviceid" id="serviceid">
        <input type="hidden" name="action" value="reject">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Portout request need to be process'); ?>
          </h4>

        </div>

        <!-- Modal body -->
        <div class="modal-body" id="portoutx">

        </div>

        <!-- Modal footer -->
        <div class="modal-footer">


           <button type="button" class="btn btn-primary float-left" onclick="disable_notification()">
            <?php echo lang('Disable this notification for me'); ?></button>


          <a href="<?php echo base_url(); ?>admin/subscription/pending_porting_out" class="btn btn-primary">
            <?php echo lang('Process Portout request Now'); ?></a>

        </div>

    </div>
  </div>
</div>

<div class="modal fade" id="accept_order">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/subscription/activate_mobile">
        <input type="hidden" name="serviceid" id="serviceidx">
        <input type="hidden" name="action" value="accept">
        <input type="hidden" name="userid" id="useridx">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Review Before Accepting order'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">

          <div class="modal-body">
            <div id="details">
            </div>
            <?php //if ($this->session->cid == "55") {?>
            <div class="col-12">
              <label for="exampleInputEmail1"> <?php echo lang('Assign Simcard'); ?>:</label>
              <input name="msisdn_sim" type="text" id="simnumberx" class="form-control  ui-autocomplete-input is-valid pengkor">
              <small id="emailHelp" class="form-text text-muted"> <?php echo lang('Please assign'); ?> <?php echo $setting->companyname; ?>  <?php echo lang('simcard here'); ?> <span class="text-danger"><strong>*  <?php echo lang('Required'); ?></strong></span></small>
            </div>
            <?php //}?>
          </div>



        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="acceptx" disabled>
            <?php echo lang('Accept Order'); ?></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php if ($this->session->picture == "nopic.png") {
        ?>
<script>
  $(document).ready(function()
{
console.log('Checking Picture');
$('#pictureid').modal({
  backdrop: 'static',
    keyboard: false
});
});
</script>

<div class="modal fade" id="pictureid">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/dashboard/upload_picture" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?php echo $this->session->id; ?>">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Please upload your avatar/picture'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div id="details">

            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="file"> <?php echo lang('Picture'); ?></label>
                    <input name="picture" class="form-control" type="file">
                  </fieldset>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="avatar"><img src="https://mijnmobiel.delta.nl/assets/img/staf/nopic.png" class="img-circle"
                    height="70" style="padding-bottom: 10px;"></div>

              </div>
            </div>
          </div>
          </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Upload Picture'); ?></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <?php echo lang('Close'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php
    }?>

  <?php if($invoices){ ?>
      <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([<?php echo implode(',', $invoices); ?>]);

        var options = {
          chart: {
            title: ' <?php echo lang('Invoices Statistic'); ?>'
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_invoices'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
  <?php } ?>
  <script>
    function disable_notification(){
      var t = confirm('<?php echo lang('Are you sure?, this notification will be disabled on your next login to the system, you will be logged out'); ?>');

      if(t){
         window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/dashboard/disable_portout_notification');
      }

    }
  </script>
