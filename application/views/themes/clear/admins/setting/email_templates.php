<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Setting'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('Email Templates'); ?>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="template">
            <thead>
              <tr>

                <th><?php echo lang('Templates Name'); ?></th>
                <th><?php echo lang('Description'); ?></th>
                 <th><?php echo lang('Subject'); ?></th>
               
                <th><?php echo lang('Date Updated'); ?></th>
                <th><?php echo lang('Edited By'); ?></th>
                 <th><?php echo lang('Status'); ?></th>

              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#template').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"pageLength": 50,
"ajax":window.location.protocol + '//' + window.location.host + '/admin/table/get_email_templates',
"aaSorting": [[0, 'asc']],
"language": {
 "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/setting/edit_email_templates/' + aData[5] + '"><strong>' + aData[0] + '</strong></a>');
if(aData[6] == "Enabled"){
$('td:eq(5)', nRow).html('<strong class="text-success">'+aData[6]+'</strong>');
}else{
$('td:eq(5)', nRow).html('<strong class="text-danger">'+aData[6]+'</strong>');
}

return nRow;
},

});
});
});
</script>