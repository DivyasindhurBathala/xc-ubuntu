<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Setting
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Add Department'); ?>
				</h5>
				<div class="table-responsive">
					<form method="post" action="<?php echo base_url(); ?>admin/setting/add_department">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="name"><?php echo lang('Name'); ?></label>
										<input  name="name" class="form-control" id="name" type="text" placeholder="Department Name">
									</fieldset>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="email"><?php echo lang('Email'); ?></label>
										<input  name="email" class="form-control" id="email" type="text">
									</fieldset>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<fieldset>
										<label for="smtp_type">Import Type</label>
										<select class="form-control" id="cred" name="smtp_type">
											<?php foreach (array('mail', 'none') as $t) {?>
											<option value="<?php echo $t; ?>"><?php echo strtoupper($t); ?></option>
											<?php }?>
										</select>
									</fieldset>
								</div>
							</div>


							<div id="cre">


								<div class="col-sm-12">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="username"><?php echo lang('Email'); ?></label>
										<input  name="pop3_user" class="form-control" id="username" type="text" placeholder="Email">
									</fieldset>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="password"><?php echo lang('Password'); ?></label>
										<input  name="pop3_password" class="form-control" id="password" type="password">
									</fieldset>
								</div>
							</div>
								<div class="col-sm-12">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="host"><?php echo lang('Host'); ?></label>
										<input  name="pop3_host" class="form-control" id="host" type="text" placeholder="host.domain.com">
									</fieldset>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="port"><?php echo lang('Port'); ?></label>
										<input  name="pop3_port" class="form-control" id="port" type="text" placeholder="110">
									</fieldset>
								</div>
							</div>

								<div class="col-sm-12">
								<div class="form-group">
									<fieldset>
										<button type="submit" class="btn btn-md btn-success btn-block">Submit</button>
									</fieldset>
								</div>
							</div>
							</div>




						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(".toggle-password").click(function() {
$("#passwordnya").prop('type', 'text');
});
</script>
<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
$('#cred').on('change', function (e) {
var optionSelected = $("option:selected", this);
var valueSelected = this.value;
console.log(valueSelected);
if(valueSelected == "none"){
$('#cre').html('');
}else{
$('#cre').html('<div class="col-sm-12"><div class="form-group"><fieldset><label class="control-label" for="username"><?php echo lang('Email'); ?></label><input name="username" class="form-control" id="username" type="text" placeholder="Email"></fieldset></div></div><div class="col-sm-12"><div class="form-group"><fieldset><label class="control-label" for="password"><?php echo lang('Password'); ?></label><input name="password" class="form-control" id="password" type="password"></fieldset></div></div><div class="col-sm-12"><div class="form-group"><fieldset><label class="control-label" for="host"><?php echo lang('Host'); ?></label><input name="host" class="form-control" id="host" type="text" placeholder="host.domain.com"></fieldset></div></div><div class="col-sm-12"><div class="form-group"><fieldset><label class="control-label" for="port"><?php echo lang('Port'); ?></label><input name="port" class="form-control" id="port" type="text" placeholder="110"></fieldset></div></div>');
}
});
</script>