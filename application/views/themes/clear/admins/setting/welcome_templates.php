<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Setting
      </h6>
      <?php if($this->session->cid == '54'){ ?>
      <div class="close"><a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>admin/setting/pdf_sync_template">Syncronize To All Products Template</a></div>
      <?php } ?>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('PDF documents Template available'); ?>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="template">
            <thead>
              <tr>

                <th><?php echo lang('Templates Name'); ?></th>
                <th><?php echo lang('Description'); ?></th>
                <th><?php echo lang('Date Created'); ?></th>
                <th><?php echo lang('Date Updated'); ?></th>
                <th><?php echo lang('Edited By'); ?></th>

              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#template').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax":window.location.protocol + '//' + window.location.host + '/admin/table/get_pdf_templates',
"aaSorting": [[0, 'asc']],
"language": {
 "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {

$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/setting/edit_welcome_templates/' + aData[0] + '"><strong>' + aData[0] + '</strong></a>');
/*$('td:eq(0)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/' + aData[6] + '"><strong>' + aData[0] + '</strong></a>');
if(aData[7] > 0){
$('td:eq(1)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[7] + '"><strong>' + aData[1] + '</strong></a>');

}
*/
//$('td:eq(1)', nRow).html('<a href="https://invoice.xmusix.eu/admin/clientssummary.php?userid=' + aData[5] + '">' + aData[1] + '</a>');
return nRow;
},

});
});
});
</script>