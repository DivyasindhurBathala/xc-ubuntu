<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Setting
			</h6>
			<div class="close"><a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>admin/setting/email_templates">Back to List</a></div>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang($template->description); ?>
				</h5>
				<div class="table-responsive">
					<form id="templates" method="post" action="<?php echo base_url(); ?>admin/Setting/edit_welcome_templates/<?php echo $this->uri->segment(4); ?>">
						<input type="hidden" name="templateid" value="<?php echo $this->uri->segment(4); ?>">
						<div class="row">

							<?php foreach ($lang as $l) {?>

							<div class="col-lg-6 col-sm-12">
								
								<div class="form-group">
									<label for="exampleTextarea h2"><img src="<?php echo base_url(); ?>assets/clear/img/flags/shiny/32/<?php echo $l; ?>.png"> <?php echo ucfirst($l); ?></label>
									<?php //print_r($template);?>
									<textarea class="form-control<?php if (!in_array($template->name, array('header', 'footer'))) {?> texteditor<?php }?>"  rows="30" name="<?php echo $l; ?>"<?php if (in_array($template->name, array('header', 'footer'))) {?> rows="40"<?php }?>>
									<?php if ($l == 'english') {?>
									<?php if (isset($tpl['english'])) {?>
									<?php echo $tpl['english']->body; ?>
									<?php } else {?>
									<?php echo $tpl['default_body']; ?>
									<?php }?>
									<?php }?>
									<?php if ($l == 'dutch') {?>
									<?php if (isset($tpl['dutch'])) {?>
									<?php echo $tpl['dutch']->body; ?>
									<?php } else {?>
									<?php echo $tpl['default_body']; ?>
									<?php }?>
									<?php }?>
									<?php if ($l == 'french') {?>
									<?php if (isset($tpl['french'])) {?>
									<?php echo $tpl['french']->body; ?>
									<?php } else {?>
									<?php echo $tpl['default_body']; ?>
									<?php }?>
									<?php }?>
									<?php if ($l == 'japanese') {?>
									<?php if (isset($tpl['japanese'])) {?>
									<?php echo $tpl['japanese']->body; ?>
									<?php } else {?>
									<?php echo $tpl['default_body']; ?>
									<?php }?>
									<?php }?>
									</textarea>
								</div>
							</div>
							<?php }?>
						</div>
						<div class="row">
							<div class="col-lg-6 mr-auto ml-auto">
								<button type="submit" class="btn btn-lg btn-primary btn-block"><i class="fa fa-save"></i> Save Changes</button>
							</div>
						</div>

						  <div class="row" style="padding-top:20px;padding-bottom:20px;">

                            <?php	$variable = json_decode($template->vars); ?>  
							<?php	if($variable){ ?>
							<table class="table table-bordered">
											<thead>
											<tr>
											<th width="20%">Variable</th>
											<th width="80%">Description</th>
											</tr>
											</thead>
											<tbody>
											<?php foreach($variable as $v){ ?>
											<tr>
											<td><strong>{$<?php echo $v->var; ?>}</strong></td>
											<td><?php echo $v->desc; ?></td>
											</tr>
											<?php } ?>
											</tbody>
							</table>  
							<?php } ?> 
                        </div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var default_texteditor_path = '<?php echo base_url(); ?>assets/grocery_crud/texteditor';
</script>
<script src="<?php echo base_url(); ?>assets/grocery_crud/texteditor/ckeditor/ckeditor.js"></script>
            <script src="<?php echo base_url(); ?>assets/grocery_crud/texteditor/ckeditor/adapters/jquery.js"></script>
            <script src="<?php echo base_url(); ?>assets/grocery_crud/js/jquery_plugins/config/jquery.ckeditor.config.js"></script>