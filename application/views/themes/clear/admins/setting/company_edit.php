<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Roles
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Company Summarry'); ?>
				</h5>
				<div class="table-responsive">
					<table class="table table-striped table-lightfont" id="company">
						<thead>
							<tr>
								<th><?php echo lang('Company ID'); ?></th>
								<th><?php echo lang('Name'); ?></th>
								<th><?php echo lang('URL'); ?></th>
								<th><?php echo lang('Invoicing'); ?></th>
								<th><?php echo lang('Ticketing'); ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>