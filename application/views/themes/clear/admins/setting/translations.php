<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
               <?php echo lang('Admin Panel Translations'); ?>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                    <?php if (!empty($trans)) { ?>
                        <?php echo lang('Currently editing'); ?> <?php echo $this->uri->segment(4); ?>
                    <?php } else { ?>
                        <?php echo lang('Please Choose the Language'); ?>
                    <?php } ?>

                </h5>

                <div class="row">
                
                <div class='col-md-6'>
                <h5><?php echo lang('Admin & Reseller'); ?></h5>
                <div class="row">
                    <?php foreach ($lang as $lo) { ?>
                    <div class="col-md-3">
                        <a class="element-box el-tablo"
                            href="<?php echo base_url(); ?>admin/setting/translations/<?php echo $lo; ?>/admin"
                            style="padding-top: 10px;">
                            <div class="label" style="padding-top: 10px;">
                            <img alt="" src="<?php echo base_url(); ?>assets/clear/img/flags-icons/<?php echo $lo; ?>.png" height="25">
                            <br />
                                <strong> <?php echo strtoupper($lo); ?></strong>
                            </div>
                            <div class="value">
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                    </div>
                    </div>
                    <div class='col-md-6'>
                    <h5><?php echo lang('Clients'); ?></h5>
                    <div class="row">
                    <?php foreach ($lang as $lo) { ?>
                    <div class="col-md-3">
                        <a class="element-box el-tablo"
                            href="<?php echo base_url(); ?>admin/setting/translations/<?php echo $lo; ?>/client"
                            style="padding-top: 10px;">
                            <img alt="" src="<?php echo base_url(); ?>assets/clear/img/flags-icons/<?php echo $lo; ?>.png" height="25">
                            <br />
                            <div class="label" style="padding-top: 10px;">
                                <strong> <?php echo strtoupper($lo); ?></strong>
                            </div>
                            <div class="value">
                            </div>
                        </a>
                    </div>
                    <?php } ?>
                    </div>
                    </div>
                </div>
                <?php if ($trans) { ?>
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <th width="45%"><?php echo lang('Key'); ?></th>
                                <th width="50%"><?php echo lang('Translation'); ?></th>
                                <th width="5%"><button type="button" onclick="addNew();"><i class="fa fa-plus-circle"></i></button></th>
                            </thead>
                            <tbody id="lg">
                                <form id="barbarian">
                                    <tr>
                                        <td colspan="3" class="text-center"><?php echo lang('Click Save Button once translate to commit your changes'); ?></td>
                                    </tr>
                                    <?php $k = 0; ?>
                                    <?php foreach ($trans as $value) { ?>
                                    <tr>
                                        <td>
                                            <?php echo $value['name']; ?>
                                        </td>
                                        <td>
                                            <input name="<?php echo $value['name']; ?>" class="form-control" type="text" id="<?php echo $value['id']; ?>"
                                                value="<?php echo $value['value']; ?>" required="">
                                        </td>
                                        <td>
                                            <button type="button" onclick="saveTransation(<?php echo $value['id']; ?>)"
                                                class="btn btn-sm btn-primary"><i class="fa fa-save"></i></button>
                                        </td>
                                    </tr>
                                
                               
                                    <?php } ?>
                                </form>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script>
    function addNew(){


        $('#Addtranslation').modal('toggle');

    }
    function Addtranslation(){

 event.preventDefault();
    $.ajax({
        url: '/admin/setting/addTranslation/<?php echo $this->uri->segment(4); ?>',
        type: 'POST',
        dataType: 'json',
        data: $('#translate').serializeArray(),
        success: function(result) {
            console.log(result);
            if(result.result){
              console.log('translated');
              window.location.href =  '/admin/setting/translations/<?php echo $this->uri->segment(4); ?>';
            }

        }

    });
    }
    function saveTransation(id) {
    event.preventDefault();
    $.ajax({
        url: '/admin/setting/saveTranslation/<?php echo $this->uri->segment(4); ?>',
        type: 'POST',
        dataType: 'json',
        data: {
            id: id,
            value: $('#'+id).val(),
            filename:'<?php echo $this->uri->segment(5); ?>'
        },
        success: function(result) {
            console.log(result);
            if(result.result){
                $('#'+id).addClass("border-success");
            }

        }

    });

}
</script>

<div class="modal fade" id="Addtranslation">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="post" id="translate" action="<?php echo base_url(); ?>admin/setting/add_translation">
        <input type="hidden" name="companyid" value="<?php echo $this->session->cid; ?>">
        <input type="hidden" name="filename" value="<?php echo $this->uri->segment(5); ?>">
        <div class="modal-header">
          <h4 class="modal-title"><?php echo lang('Add Porting'); ?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for=""><?php echo lang('Keyword'); ?></label>
                <input class="form-control" autocomplete="new-username"  type="text" name="keyword" required>
              </div>
              
            </div>

             <?php foreach(explode(',', $setting->supported_language) as $lang){ ?>
                <div class="col-sm-12">
              <div class="form-group">
                <label for=""><?php echo lang(ucfirst($lang)); ?></label>
                <input class="form-control" autocomplete="new-username"  type="text" name="lang[<?php echo $lang; ?>]" required>
              </div>
              
            </div>
             <?php } ?>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="Addtranslation()"><?php echo lang('Submit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>