
<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            Pricing
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                <?php echo lang('Product Pricing (Incl. VAT)'); ?> <div class="close"> <a class="btn btn-primary btn-sm" href="javascript::void(0)" onclick="add_role()"><i class="fa fa-plus-circle"></i> <?php echo lang('Add Company'); ?></a></div>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-lightfont" id="company">
                        <thead>
                            <tr>
                                <th><?php echo lang('Product ID'); ?></th>
                                <th><?php echo lang('Name'); ?></th>
                                <th><?php echo lang('Contact Monthly'); ?></th>
                                <th><?php echo lang('Contact Quarterly'); ?></th>
                                <th><?php echo lang('Contact Semi-Annually'); ?></th>
                                <th><?php echo lang('Contact Annually'); ?></th>
                                <th><?php echo lang('Contact Bienially'); ?></th>
                                <th>Save</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($products as $product) {?>
                        <tr>
                        <form method="post" action="<?php echo base_url(); ?>admin/setting/edit_pricing">
                        <input type="hidden" name="id" value="<?php echo $product->id; ?>" class="form-control">
                                <td><?php echo $product->id; ?></td>
                                <td><input  name="name" step="any" type="text" value="<?php echo $product->name; ?>" class="form-control" disabled></td>
                                <td><input  name="monthly"  step="any" type="text" value="<?php echo $product->monthly; ?>" class="form-control"></td>
                                <td><input  name="quarterly" step="any" type="text" value="<?php echo $product->quarterly; ?>" class="form-control"></td>
                                <td><input  name="semi_annually" step="any" type="text" value="<?php echo $product->semi_annually; ?>" class="form-control"></td>
                                <td><input  name="annually" step="any" type="text" value="<?php echo $product->annually; ?>" class="form-control"></td>
                                <td><input  name="bienially" step="any" type="text" value="<?php echo $product->bienially; ?>" class="form-control"></td>
                                <td><button type="submit" class="btn btn-md btn-primary">Save</button></td>
                                </form>

                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

