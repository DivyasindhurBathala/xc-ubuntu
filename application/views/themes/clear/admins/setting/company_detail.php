<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Company'); ?>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                <?php echo lang('Company Summary'); ?>
                </h5>
                <hr />
                <div class="row">
                    <div class="col-lg-6">
                        <form method="post"  id="form_body">
                            <fieldset class="form-group">
                                <h5><?php echo lang('Company Information'); ?></h5>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="companyid"><?php echo lang('Company Id'); ?></label>
                                            <input type="text" class="form-control" id="companyid" name="companyid" value="<?php echo $company->companyid; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="companyname"><?php echo lang('Company Name'); ?></label>
                                            <input type="text" class="form-control" id="companyname" name="companyname" value="<?php echo $company->companyname; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="RangeNumber"><?php echo lang('RangeNumber'); ?></label>
                                            <input type="text" class="form-control" id="RangeNumber" name="RangeNumber" value="<?php echo $company->RangeNumber; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="email_notification"><?php echo lang('Email Notification'); ?></label>
                                            <input type="text" class="form-control" id="email_notification" name="email_notification" value="<?php echo $company->email_notification; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="portal_url"><?php echo lang('Company Portal Url (trailing /)'); ?></label>
                                            <input type="text" class="form-control" id="portal_url" name="portal_url" value="<?php echo $company->portal_url; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="footer_link"><?php echo lang('Footer Links'); ?></label>
                                    <textarea class="form-control" id="footer_link" rows="10"><?php echo $company_config->footer_link; ?></textarea>
                                </div>
                                <fieldset class="form-group">
                                    <h5><?php echo lang('Sub Configuration'); ?></h5>
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <label for="mage_invoicing"><?php echo lang('Invoice Magebo'); ?></label>
                                            <select class="form-control" id="mage_invoicing" name="mage_invoicing">
                                                <option value="1"<?php if ($company_config->mage_invoicing == "1") {
    ?> selected<?php
}?>>Yes</option>
                                                <option value="0"<?php if ($company_config->mage_invoicing == "0") {
        ?> selected<?php
    }?>>No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="whmcs_ticket"><?php echo lang('Helpdesk Ticketing'); ?></label>
                                            <select class="form-control" id="whmcs_ticket"  name="whmcs_ticket">
                                                <option value="1"<?php if ($company_config->whmcs_ticket == "1") {
        ?> selected<?php
    }?>>Yes</option>
                                                <option value="0"<?php if ($company_config->whmcs_ticket == "0") {
        ?> selected<?php
    }?>>No</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="mage_invoicing"><?php echo lang('Multiple Language'); ?></label>
                                            <select class="form-control" id="multilang" name="multilang">
                                                <option value="1"<?php if ($company_config->multilang == "1") {
        ?> selected<?php
    }?>>Yes</option>
                                                <option value="0"<?php if ($company_config->multilang == "0") {
        ?> selected<?php
    }?>>No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="currency col-md-2">
                                            <div class="form-group">
                                                <label for="currency"><?php echo lang('Currency'); ?></label>
                                                <input type="text" class="form-control" id="currency" name="currency" value="<?php echo $company_config->currency; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group department_id" style="display:none;">
                                                <label for="helpdesk_default_deptid"><?php echo lang('Default Department'); ?></label>
                                                <?php //print_r(getDepartments($this->uri->segment(4)));?>
                                                <select class="form-control" id="helpdesk_default_deptid" name="helpdesk_default_deptid">
                                                    <?php foreach (getDepartments($this->uri->segment(4)) as $row) {
        ?>
                                                    <option value="<?php echo $row['id']; ?>"<?php if ($row['id'] == getDefaultDepartment($this->uri->segment(4))) {
            ?> selected<?php
        } ?>><?php echo $row['name']; ?></option>
                                                    <?php
    }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="taxrate col-md-2">
                                            <div class="form-group">
                                                <label for="taxrate"><?php echo lang('Tax Rate'); ?></label>
                                                <input type="text" class="form-control" id="taxrate" name="taxrate" value="<?php echo $company_config->taxrate; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="trademark_text"><?php echo lang('Footer Trademark'); ?></label>
                                                <input type="text" class="form-control" id="trademark_text" name="trademark_text" value="<?php echo $company_config->trademark_text; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-lg-6">
                                <fieldset class="form-group">
                                    <h5>Email Setting</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="smtp_sender"><?php echo lang('Sender Email'); ?></label>
                                                <input type="text" class="form-control" id="smtp_sender" name="smtp_sender" value="<?php echo $company_config->smtp_sender; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="smtp_name"><?php echo lang('Sender Name'); ?></label>
                                                <input type="text" class="form-control" id="smtp_name" name="smtp_name" value="<?php echo $company_config->smtp_name; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="smtp_user"><?php echo lang('SMTP Username'); ?></label>
                                                <input type="text" class="form-control" id="smtp_user" name="smtp_user" value="<?php echo $company_config->smtp_user; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="smtp_pass"><?php echo lang('SMTP Password'); ?></label>
                                                <input type="text" class="form-control" id="smtp_pass" name="smtp_pass" value="<?php echo $this->encryption->decrypt($company_config->smtp_pass); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="smtp_port"><?php echo lang('SMTP Port'); ?></label>
                                                <input type="text" class="form-control" id="smtp_port" name="smtp_port" value="<?php echo $company_config->smtp_port; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="smtp_host"><?php echo lang('SMTP Host'); ?></label>
                                                <input type="text" class="form-control" id="smtp_host" name="smtp_host" value="<?php echo $company_config->smtp_host; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <h5><?php echo lang('Online Payment'); ?></h5>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="multilang" id="multilang1" value="1" <?php if ($company_config->multilang == "yes") {
        ?> checked=""<?php
    }?>>
                                            <?php echo lang('Accept Online Payment'); ?>
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="multilang" id="multilang2" value="0" <?php if ($company_config->multilang == "no") {
        ?> checked=""<?php
    }?>>
    <?php echo lang('Do Not accept Online Payment'); ?>
                                        </label>
                                    </div>
                                </fieldset>
                                <fieldset class="form-group">
                                    <h5><?php echo lang('Event Handler'); ?></h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="rabbit_user"><?php echo lang('RabbitMQ Username'); ?></label>
                                                <input type="text" class="form-control" id="rabbit_user" name="rabbit_user" value="<?php echo $company_config->rabbit_user; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="rabbit_password"><?php echo lang('RabbitMQ Password'); ?></label>
                                                <input type="text" class="form-control" id="rabbit_password" name="rabbit_password" value="<?php echo $company_config->rabbit_password; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="rabbit_host"><?php echo lang('RabbitMQ Host'); ?></label>
                                                <input type="text" class="form-control" id="rabbit_host" name="rabbit_host" value="<?php echo $company_config->rabbit_host; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="rabbit_port"><?php echo lang('RabbitMQ Port'); ?></label>
                                                <input type="text" class="form-control" id="rabbit_port" name="rabbit_port" value="<?php echo $company_config->rabbit_port; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="rabbit_vhost"><?php echo lang('RabbitMQ Vhost'); ?></label>
                                                <input type="text" class="form-control" id="rabbit_vhost" name="rabbit_vhost" value="<?php echo $company_config->rabbit_vhost; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="rabbit_event_queue"><?php echo lang('RabbitMQ Queuename'); ?></label>
                                                <input type="text" class="form-control" id="rabbit_event_queue" name="rabbit_event_queue" value="<?php echo $company_config->rabbit_event_queue; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="rabbit_event_routingkey"><?php echo lang('RabbitMQ RoutingKey'); ?></label>
                                                <input type="text" class="form-control" id="rabbit_event_routingkey" name="rabbit_event_routingkey" value="<?php echo $company_config->rabbit_event_routingkey; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block" id="send_form"><i id="idx" class="fa fa-save"></i> <?php echo lang('Save Configuration'); ?></button>
                        <h4 class="text-success show-success" style="display: none;"><center><?php echo lang('Configuration has been applied'); ?>!</center></h4>
                    </div>
                </div>
            </div>
        </div>
        <pre>
            <?php //print_r($company_config);?>
            <?php //print_r($company);?>
        </pre>
        <script>
            $( "#whmcs_ticket" ).change(function() {
                var whmcs_ticket = $( "#whmcs_ticket option:selected" ).val();
                if(whmcs_ticket == "1"){
                    console.log(whmcs_ticket);
                    $('.department_id').show('slow');
                }else{
                    $('.department_id').hide('slow');

                }
            });
        $( "#send_form" ).click(function() {
        $( "#send_form" ).prop('disabled', true);
        $( "#idx" ).removeClass( "fa fa-save" ).addClass( "fa fa-gear fa-spin" );
        var datastring = $("#form_body").serialize();
        $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>admin/setting/save_company_config/<?php if (!empty($this->uri->segment(4))) {
        ?><?php echo $this->uri->segment(4); ?><?php
    } else {
        ?><?php echo $_SESSION['cid']; ?><?php
    } ?>",
        data: datastring,
        dataType: "json",
        success: function(data) {
        if(data.result){
        $( "#send_form" ).hide('slow');
        $('.show-success').show('slow');
        }
        },
        error: function() {
        alert('error handling here');
        }
        });
        });
        $( document ).ready(function() {
        var whmcs_ticket = $( "#whmcs_ticket option:selected" ).val();
        if(whmcs_ticket == "1"){
        console.log(whmcs_ticket);
        $('.department_id').show('slow');
        }else{
            $('.department_id').hide('slow');
        }
        });
        </script>