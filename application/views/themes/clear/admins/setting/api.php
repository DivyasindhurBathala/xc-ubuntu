<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Setting API Keys'); ?><div class="close">
                <button class="btn btn-primary btn-sm"  type="button" data-toggle="modal" data-target="#KeyModal"><i class="fa fa-plus-circle"></i> <?php echo lang('New Token'); ?></button>
            </div>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                <?php echo lang('API Keys'); ?>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-lightfont" id="tokens">
                        <thead>
                            <tr>
                                <th><?php echo lang('Key'); ?></th>
                                <th><?php echo lang('Date Created'); ?></th>
                                <th>Creator</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Setting Allowed IPs'); ?><div class="close">
                <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#IPModal"><i class="fa fa-plus-circle"></i> <?php echo lang('Add New IP'); ?></button>
            </div>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                <?php echo lang('Whitelist IPs'); ?>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-lightfont" id="ips">
                        <thead>
                            <tr>
                                <th><?php echo lang('IPv4/IPv6'); ?></th>
                                <th><?php echo lang('Date Created'); ?></th>
                                <th>Creator</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--
<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
            <?php echo lang('Api Logs'); ?>
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                <?php echo lang('List Calls Made'); ?>
                </h5>

                    <table class="table table-striped table-lightfont" id="logs" witdh="900px">
                        <thead>
                            <tr>
                                <th witdh="10%"><?php echo lang('Env'); ?></th>
                                <th witdh="15%"><?php echo lang('Date'); ?></th>
                                <th witdh="15%"><?php echo lang('Method'); ?></th>
                                <th witdh="30%"><?php echo lang('Response'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

            </div>
        </div>
    </div>
</div>
-->

<div class="modal fade" id="KeyModal" tabindex="-1" role="dialog" aria-labelledby="ModalKeyLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/setting/key_add">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('New Key Generation'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo lang('Do you wish to generate new Token?'); ?><br />
                    <?php echo lang('You may generate up to 5 Keys.'); ?>
                    <br />
                    <div class="form-check">
                        <label class="form-check-label">
                            <input name="sso" type="checkbox" class="form-check-input" id="sso">
                            <?php echo lang('Allow SSO'); ?>
                        </label>
                    </div>
                    <div class="form-group" style="display:none;" id="hostbox">
                        <label for="exampleInputEmail1"><?php echo lang('Host Referal, ex: domain.com'); ?></label>
                        <input name="host" type="text" class="form-control" id="hostinput" aria-describedby="host" placeholder="domain.com">
                        <small id="Host" class="form-text text-muted"><?php echo lang('Enter your host referal for autologin'); ?></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo lang('Generate'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="IPModal" tabindex="-1" role="dialog" aria-labelledby="ModalIpLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="<?php echo base_url(); ?>admin/setting/ip_add">
                <input type="hidden" name="creator" value="<?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname'); ?>">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Adding IPv4/6'); ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">IPv4/IPv6</label>
                        <input name="ip" type="text" class="form-control" id="ipv4" aria-describedby="IPHelp" placeholder="XXX.XXX.XXX.XXX">
                        <small id="emailHelp" class="form-text text-muted"><?php echo lang('Enter your IPv4/6 without space'); ?></small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
function confirmation_delete_key(id) {
var answer = confirm("<?php echo lang('Do you wish to delete this api key'); ?>?")
if (answer) {
window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/setting/delete_key/" + id);
} else {
console.log("<?php echo lang('cancelled delete key'); ?> ");
}
}
    function confirmation_delete_ip(id) {
var answer = confirm("<?php echo lang('Do you wish to delete this ip'); ?>?")
if (answer) {
window.location.replace(window.location.protocol + "//" + window.location.host + "/admin/setting/delete_ip/" + id);
} else {
console.log("cancelled delete ip ");
}
}
</script>
<script>
    $( document ).ready(function() {
    $(document).ready(function () {
    var ckbox = $('#sso');

    $('input').on('click',function () {
        if (ckbox.is(':checked')) {
            $('#hostbox').show();
            $('#hostinput').prop('required',true);
        } else {
            $('#hostbox').hide();
            $('#hostinput').prop('required',false);
        }
    });
});
});

    </script>