<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Setting<div class="close">
          <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/setting/add_department"><i class="fa fa-plus-circle"></i> <?php echo lang('New Department'); ?></a>
        </div>
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Invoice Configuration'); ?>
				</h5>
				<div class="table-responsive">
					<table class="table table-striped table-lightfont" id="dt_cn">
						<thead>
							<tr>
								<th><?php echo lang('ID'); ?></th>
								<th><?php echo lang('Name'); ?></th>
								<th><?php echo lang('Email'); ?></th>

							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(".toggle-password").click(function() {
$("#passwordnya").prop('type', 'text');
});
</script>