<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Company Setting'); ?>:  <span class="text-danger"><strong><?php echo $company->companyname; ?> (<?php echo $company->companyid; ?>)</strong></span>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
          <?php echo lang('Company Summary'); ?>
          <div class="float-right"><button class="btn btn-md btn-primary" type="button" data-toggle="modal" data-target="#importSubscribers"><?php echo lang('Import Subscribers'); ?></button></div>
        </h5>
        <hr />
        <div class="row">
          <div class="col-lg-6">
            <form method="post"  id="form_body">
              <fieldset class="form-group">
                <h5>
                <?php echo lang('Company Information'); ?>

                </h5>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="companyid"><?php echo lang('Companyname'); ?></label>
                      <input type="text" class="form-control" id="companyname1" name="companyname" value="<?php echo $company_config->companyname; ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="company_registration"><?php echo lang('Company Registration'); ?></label>
                      <input type="text" class="form-control" id="company_registration" name="company_registration" value="<?php echo $company_config->company_registration; ?>">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="vat"><?php echo lang('VAT Number'); ?></label>
                      <input type="text" class="form-control" id="vat" name="vat" value="<?php echo $company_config->vat; ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="companyid"><?php echo lang('Company Id'); ?></label>
                      <input type="text" class="form-control" id="companyid" name="companyid" value="<?php echo $company->companyid; ?>" readonly>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="companyname"><?php echo lang('Company Brand'); ?></label>
                      <input type="text" class="form-control" id="companyname" name="companyname" value="<?php echo $company->companyname; ?>" readonly>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="RangeNumber"><?php echo lang('RangeNumber'); ?></label>
                      <input type="text" class="form-control" id="RangeNumber" name="RangeNumber" value="<?php echo $company->RangeNumber; ?>" readonly>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="email_notification"><?php echo lang('Email Notification'); ?></label>
                      <input type="text" class="form-control" id="email_notification" name="email_notification" value="<?php echo $company->email_notification; ?>">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="email_notification"><?php echo lang('Portin Notification'); ?></label>
                      <select class="form-control" id="email_portin_event" name="email_portin_event">
                        <option value="1"<?php if ($company->email_portin_event == "1") {
    ?> selected<?php
}?>>Yes</option>
                        <option value="0"<?php if ($company->email_portin_event == "0") {
        ?> selected<?php
    }?>>No</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="email_notification"><?php echo lang('Order Notification'); ?></label>
                      <select class="form-control" id="email_on_order"  name="email_on_order">
                        <option value="1"<?php if ($company->email_on_order == "1") {
        ?> selected<?php
    }?>>Yes</option>
                        <option value="0"<?php if ($company->email_on_order == "0") {
        ?> selected<?php
    }?>>No</option>
                      </select>

                    </div>
                  </div>


                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="portal_url">Company Portal Url (trailing /)</label>
                      <input type="text" class="form-control" id="portal_url" name="portal_url" value="<?php echo $company->portal_url; ?>" readonly>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="footer_link">Footer Links</label>
                  <textarea class="form-control" id="footer_link" name="footer_link" rows="10"><?php echo $company_config->footer_link; ?></textarea>
                </div>
                <fieldset class="form-group">
                  <h5>Sub Configuration</h5>
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for="mage_invoicing">Invoice Magebo</label>
                      <select class="form-control" id="mage_invoicing" name="mage_invoicing">
                        <option value="1"<?php if ($company_config->mage_invoicing == "1") {
        ?> selected<?php
    }?>>Yes</option>
                        <option value="0"<?php if ($company_config->mage_invoicing == "0") {
        ?> selected<?php
    }?>>No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="whmcs_ticket">Helpdesk Ticketing</label>
                      <select class="form-control" id="whmcs_ticket" name="whmcs_ticket">
                        <option value="1"<?php if ($company_config->whmcs_ticket == "1") {
        ?> selected<?php
    }?>>Yes</option>
                        <option value="0"<?php if ($company_config->whmcs_ticket == "0") {
        ?> selected<?php
    }?>>No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="multilang">Multiple Language</label>
                      <select class="form-control" id="multilang" name="multilang">
                        <option value="1"<?php if ($company_config->multilang == "1") {
        ?> selected<?php
    }?>>Yes</option>
                        <option value="0"<?php if ($company_config->multilang == "0") {
        ?> selected<?php
    }?>>No</option>
                      </select>
                    </div>
                  </div>
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for="mvno_id_increment">Auto Customerid</label>
                      <select class="form-control" id="mvno_id_increment">
                        <option value="1"<?php if ($company_config->mvno_id_increment == "1") {
        ?> selected<?php
    }?>>Yes</option>
                        <option value="0"<?php if ($company_config->mvno_id_increment == "0") {
        ?> selected<?php
    }?>>No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="proforma_invoice">Proforma Invoice</label>
                      <select class="form-control" id="proforma_invoice" name="proforma_invoice">
                        <option value="1"<?php if ($company_config->proforma_invoice == "1") {
        ?> selected<?php
    }?>>Yes</option>
                        <option value="0"<?php if ($company_config->proforma_invoice == "0") {
        ?> selected<?php
    }?>>No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="Prorata">Prorata Invoice</label>
                      <select class="form-control" id="Prorata" name="Prorata">
                        <option value="1"<?php if ($company_config->Prorata == "1") {
        ?> selected<?php
    }?>>Yes</option>
                        <option value="0"<?php if ($company_config->Prorata == "0") {
        ?> selected<?php
    }?>>No</option>
                      </select>
                    </div>
                  </div>

                  <div class="row">
                   <div class="col-md-2">
                    <div class="form-group">
                      <label for="email_notification"><?php echo lang('Currency'); ?></label>
                      <input type="text" class="form-control" id="currency" name="currency" value="<?php echo $company_config->currency; ?>">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group department_id" style="display:none;">
                      <label for="helpdesk_default_deptid">Default Department</label>
                      <?php //print_r(getDepartments($this->uri->segment(4)));?>
                      <select class="form-control" id="helpdesk_default_deptid" name="helpdesk_default_deptid">
                        <?php foreach (getDepartments($this->session->cid) as $row) {
        ?>
                          <option value="<?php echo $row['id']; ?>"<?php if ($row['id'] == getDefaultDepartment($this->uri->segment(4))) {
            ?> selected<?php
        } ?>><?php echo $row['name']; ?></option>
                          <?php
    }?>
                      </select>
                    </div>
                  </div>
                  <div class="taxrate col-md-2">
                    <div class="form-group">
                      <label for="taxrate">Tax Rate</label>
                      <input type="text" class="form-control" id="taxrate" name="taxrate" value="<?php echo $company_config->taxrate; ?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="payment_duedays">Invoice Duedays</label>
                      <input type="text" class="form-control" id="payment_duedays" name="payment_duedays" value="<?php echo $company_config->payment_duedays; ?>">
                    </div>
                  </div>

                </div>

                <div class="row">
                 <div class="col-md-12">
                  <div class="form-group">
                    <label for="trademark_text">Footer Trademark</label>
                    <input type="text" class="form-control" id="trademark_text" name="trademark_text" value="<?php echo $company_config->trademark_text; ?>">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="logo_site">Logo</label>
                    <input type="text" class="form-control" id="logo_site" name="logo_site" value="<?php echo $company_config->logo_site; ?>">
                  </div>
                </div>
              </div>

              <?php if ($company_config->mage_invoicing == "1") {
        ?>

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group  has-success">
                      <label for="taxrate"><?php echo lang('First Invoice Reminder'); ?></label>
                      <input type="text" class="form-control" id="reminder_1" name="reminder_1" value="<?php echo $company_config->reminder_1; ?>" readonly>
                      <div class="valid-feedback"> <?php echo lang('(x) day after invoice duedate'); ?>.</div>
                    </div>
                  </div>
                  <div class="taxrate col-md-4">
                    <div class="form-group  has-success">
                      <label for="taxrate"><?php echo lang('Second Invoice Reminder'); ?></label>
                      <input type="text" class="form-control" id="reminder_2" name="reminder_2" value="<?php echo $company_config->reminder_2; ?>" readonly>
                      <div class="valid-feedback"><?php echo lang('(x) day after invoice duedate'); ?>.</div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group  has-success">
                      <label for="trademark_text"><?php echo lang('Third  Invoice Reminder'); ?> (PDF)</label>
                      <input type="text" class="form-control" id="reminder_3" name="reminder_3" value="<?php echo $company_config->reminder_3; ?>" readonly>
                      <div class="valid-feedback"> <?php echo lang('(x) day after invoice duedate'); ?>.</div>

                    </div>
                  </div>
                </div>

                <?php
    }?>



            </fieldset>
          </div>
          <div class="col-lg-6">
            <fieldset class="form-group">
              <h5><?php echo lang('Email Setting'); ?></h5>
              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input smtp_setting" name="smtp_type" id="smtp1" value="smtp" <?php if ($company_config->smtp_type == "smtp") {
        ?> checked=""<?php
    }?>>
                  <?php echo lang('SMTP'); ?>
                </label>
              </div>
              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input smtp_setting" name="smtp_type" id="smtp2" value="mail" <?php if ($company_config->smtp_type == "mail") {
        ?> checked=""<?php
    }?>>
                  <?php echo lang('PHP MAIL'); ?>
                </label>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="smtp_sender"><?php echo lang('Sender Email'); ?></label>
                    <input type="text" class="form-control" id="smtp_sender" name="smtp_sender" value="<?php echo $company_config->smtp_sender; ?>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="smtp_name"><?php echo lang('Sender Name'); ?></label>
                    <input type="text" class="form-control" id="smtp_name" name="smtp_name" value="<?php echo $company_config->smtp_name; ?>">
                  </div>
                </div>
              </div>
              <div class="row smtp">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="smtp_user"><?php echo lang('SMTP Username'); ?></label>
                    <input type="text" class="form-control" id="smtp_user" name="smtp_user" value="<?php echo $company_config->smtp_user; ?>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="smtp_pass"><?php echo lang('SMTP Password'); ?></label>
                    <input type="password" class="form-control" id="smtp_pass" name="smtp_pass" value="<?php echo $this->encryption->decrypt($company_config->smtp_pass); ?>">
                  </div>
                </div>
              </div>
              <div class="row smtp">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="smtp_port"><?php echo lang('SMTP Port'); ?></label>
                    <input type="text" class="form-control" id="smtp_port" name="smtp_port" value="<?php echo $company_config->smtp_port; ?>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="smtp_host"><?php echo lang('SMTP Host'); ?></label>
                    <input type="text" class="form-control" id="smtp_host" name="smtp_host" value="<?php echo $company_config->smtp_host; ?>">
                  </div>
                </div>
              </div>
            </fieldset>
            <fieldset class="form-group">
              <h5><?php echo lang('Online Payment'); ?></h5>
              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="online_payment" id="online_payment1" value="yes" <?php if ($company_config->online_payment == "yes") {
        ?> checked=""<?php
    }?>>
                  <?php echo lang('Accept Online Payment'); ?>
                </label>
              </div>
              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="online_payment" id="online_payment2" value="no" <?php if ($company_config->online_payment == "no") {
        ?> checked=""<?php
    }?>>
                  <?php echo lang('Do Not accept Online Payment'); ?>
                </label>
              </div>
            </fieldset>


            <fieldset class="form-group">
              <h5><?php echo lang('Default Paymentmethod'); ?></h5>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <select class="form-control" id="default_paymentmethod" name="default_paymentmethod">
                    <?php foreach (getPaymentGateway($this->session->cid) as $pm) {
        ?>
                      <option value="<?php echo $pm->name; ?>"<?php if ($company_config->default_paymentmethod == $pmt) {
            ?> selected<?php
        } ?>><?php echo $pm->description; ?></option>
                      <?php
    }?>
                  </select>
                </div>
              </div>
            </div>
          </fieldset>


          <fieldset class="form-group">
            <h5><?php echo lang('Pricing Setting'); ?></h5>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                 <select class="form-control" id="specific_pricing" name="specific_pricing">
                  <?php foreach (array('0' => 'Simple', '1'=>'Complex') as $key => $pm) {
        ?>
                    <option value="<?php echo $key; ?>"<?php if ($company_config->specific_pricing == $key) {
            ?> selected<?php
        } ?>><?php echo $pm; ?></option>
                    <?php
    }?>
                </select>
              </div>
            </div>
          </div>
        </fieldset>



        <fieldset class="form-group">
          <h5><?php echo lang('Sms Notifications'); ?></h5>
          <div class="form-check">
            <label class="form-check-label">
              <input type="radio" class="form-check-input enable_sms" name="enable_sms" id="enable_sms1" value="1" <?php if ($company_config->enable_sms == "1") {
        ?> checked=""<?php
    }?>>
              <?php echo lang('Yes'); ?> (<?php echo $setting->currency; ?>50.00 will be charge to you monthly + <?php echo $setting->currency; ?>0.25/sms )
            </label>
          </div>
          <div class="form-check">
            <label class="form-check-label">
              <input type="radio" class="form-check-input enable_sms" name="enable_sms" id="enable_sms2" value="0" <?php if ($company_config->enable_sms == "0") {
        ?> checked=""<?php
    }?>>
              No
            </label>
          </div>

          <div class="row sms">
            <div class="col-md-12">
              <div class="form-group">
                <label for="sms_senderid"><?php echo lang('SenderName (Number or String Max 8 Chars)'); ?> <i><?php echo lang('it needs a manual validation'); ?></i></label>
                <input type="text" class="form-control" id="sms_senderid" name="sms_senderid" value="<?php echo $company_config->sms_senderid; ?>" readonly>
              </div>
            </div>

             <div class="col-md-12">
              <div class="form-group">
                <label for="sms_content"><?php echo lang('SMS Portin Initiation (leave empty to disable)'); ?> (160 chars/sms)</i></label>
                <input type="text" class="form-control" id="sms_content" name="sms_content" value="<?php echo $company_config->sms_content; ?>">
              </div>
            </div>

             <div class="col-md-12">
              <div class="form-group">
                <label for="sms_content_reminder1"><?php echo lang('Invoice Reminder 1 (leave empty to disable)'); ?> (160 chars/sms)</i></label>
                <input type="text" class="form-control" id="sms_content_reminder1" name="sms_content_reminder1" value="<?php echo $company_config->sms_content_reminder1; ?>">
              </div>
            </div>


             <div class="col-md-12">
              <div class="form-group">
                <label for="sms_content_reminder2"><?php echo lang('Invoice Reminder 2 (leave empty to disable)'); ?>  (160 chars/sms)</i></label>
                <input type="text" class="form-control" id="sms_content_reminder2" name="sms_content_reminder2" value="<?php echo $company_config->sms_content_reminder2; ?>">
              </div>
            </div>

          </div>
        </fieldset>


        <fieldset class="form-group">
          <h5>Event Handler</h5>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="rabbit_user">RabbitMQ Username</label>
                <input type="text" class="form-control" id="rabbit_user" name="rabbit_user" value="<?php echo $company_config->rabbit_user; ?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="rabbit_password">RabbitMQ Password</label>
                <input type="text" class="form-control" id="rabbit_password" name="rabbit_password" value="<?php echo $company_config->rabbit_password; ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="rabbit_host">RabbitMQ Host</label>
                <input type="text" class="form-control" id="rabbit_host" name="rabbit_host" value="<?php echo $company_config->rabbit_host; ?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="rabbit_port">RabbitMQ Port</label>
                <input type="text" class="form-control" id="rabbit_port" name="rabbit_port" value="<?php echo $company_config->rabbit_port; ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="rabbit_vhost">RabbitMQ Vhost</label>
                <input type="text" class="form-control" id="rabbit_vhost" name="rabbit_vhost" value="<?php echo $company_config->rabbit_vhost; ?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="rabbit_event_queue">RabbitMQ Queuename</label>
                <input type="text" class="form-control" id="rabbit_event_queue" name="rabbit_event_queue" value="<?php echo $company_config->rabbit_event_queue; ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="rabbit_event_routingkey">RabbitMQ RoutingKey</label>
                <input type="text" class="form-control" id="rabbit_event_routingkey" name="rabbit_event_routingkey" value="<?php echo $company_config->rabbit_event_routingkey; ?>">
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <button class="btn btn-lg btn-primary btn-block" id="send_form"><i id="idx" class="fa fa-save"></i> Save Configuration</button>
    <h4 class="text-success show-success" style="display: none;"><center>Configuration has been applied!</center></h4>
  </div>
</div>
</div>
</div>
<pre>
  <?php //print_r($company_config);?>
  <?php //print_r($company);?>
</pre>
<script>
 $( ".smtp_setting" ).change(function() {
   var SMTP = $('#smtp1').is(':checked');

   if(SMTP){
    console.log('smtp  checked');
    $('.smtp').show();
  }else{
   $('.smtp').hide();
   console.log('smtp not checked');
 }

});

 $( ".enable_sms" ).change(function() {
  var SMS = $('#enable_sms1').is(':checked');

  if(SMS){
    console.log('sms notification enabled');
    $('.sms').show();
  }else{

   $('.sms').hide();
 }


});

 $( "#whmcs_ticket" ).change(function() {
  var whmcs_ticket = $( "#whmcs_ticket option:selected" ).val();
  if(whmcs_ticket == "1"){
    console.log(whmcs_ticket);
    $('.department_id').show('slow');
  }else{
    $('.department_id').hide('slow');

  }
});
$( "#import" ).click(function() {
  $( "#import" ).prop('disabled', true);
});
 $( "#send_form" ).click(function() {
  $( "#send_form" ).prop('disabled', true);
  $( "#idx" ).removeClass( "fa fa-save" ).addClass( "fa fa-gear fa-spin" );
  var datastring = $("#form_body").serialize();
  $.ajax({
    type: "POST",
    url: "<?php echo base_url(); ?>admin/setting/save_mycompany_config/",
    data: datastring,
    dataType: "json",
    success: function(data) {
      if(data.result){
        $( "#send_form" ).hide('slow');
        $('.show-success').show('slow');
      }
    },
    error: function() {
      alert('error handling here');
    }
  });
});
 $( document ).ready(function() {
  var SMTP = $('#smtp1').is(':checked');
  var SMS = $('#enable_sms1').is(':checked');
  if(SMTP){
    console.log('smtp  checked');
    $('.smtp').show();
  }else{

   $('.smtp').hide();
 }

 if(SMS){
  console.log('sms notification enabled');
  $('.sms').show();
}else{

 $('.sms').hide();
}



var whmcs_ticket = $( "#whmcs_ticket option:selected" ).val();
if(whmcs_ticket == "1"){
  console.log(whmcs_ticket);
  $('.department_id').show('slow');
}else{
  $('.department_id').hide('slow');
}
});
</script>

<!-- Modal -->
<div class="modal fade" id="importSubscribers" tabindex="-1" role="dialog" aria-labelledby="importSubscribers" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Import Subscribers'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open_multipart('master/setting/Import_CoreServer_Data');?>
      <div class="modal-body">
        <h5 class="text-danger">Warning: Please do not use this feature before consulting with our developer.</h5>
        Download example <a href="<?php echo base_url(); ?>assets/datas/29377f0951168e22b50f38fc9c701275.csv" target="_blank"> here</a>
          <input type="file" name="userfile" size="20" class="form-control" required/>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="companyid" value="<?php echo $this->session->cid; ?>">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="submit" id="import"  class="btn btn-primary">Import</button>
      </div>
      </form>
    </div>
  </div>
</div>