    <div class="content-i">
        <div class="content-box">
            <div class="element-wrapper">
                <h6 class="element-header">
                <?php echo lang('Setting'); ?>
                </h6>
                <div class="float-right"><a class="btn btn-primary btn-xs" href="<?php echo base_url(); ?>admin/setting/email_templates"><?php echo lang('Back to List'); ?></a></div>
                <div class="element-box">
                    <h5 class="form-header">
                    <?php echo lang('Edit Email Templates'); ?>: <?php echo $template->name; ?> (<?php echo $template->description; ?>)
                    </h5>
                    <div class="table-responsive">
                        <form id="templates" method="post" action="<?php echo base_url(); ?>admin/Setting/edit_email_templates/<?php echo $this->uri->segment(4); ?>">
                            <input type="hidden" name="templateid" value="<?php echo $this->uri->segment(4); ?>">
                            <div class="row">

                                <?php foreach ($lang as $l) {?>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="Subject"><?php echo lang('Subject'); ?></label>
                                        <input name="subject-<?php echo $l; ?>" type="text" class="form-control" value="<?php echo getSubject($template->name, $l, $this->session->cid); ?>" aria-describedby="emailHelp" placeholder="Enter Subject">
                                    </div>
                                    <div class="form-group">
                                        <label for="textedit h2"><img src="<?php echo base_url(); ?>assets/clear/img/flags/shiny/32/<?php echo $l; ?>.png"> <?php echo ucfirst($l); ?></label>
                                        <textarea class="form-control<?php if (!in_array($template->name, array('header', 'footer'))) {
                                            ?> texteditor<?php
                                                                     }?>"  name="<?php echo $l; ?>"<?php if (in_array($template->name, array('header', 'footer'))) {
    ?> rows="40"<?php
}?>>
                                        <?php if ($l == 'english') {?>
                                            <?php if (isset($tpl['english'])) {?>
                                                <?php echo $tpl['english']->body; ?>
                                            <?php } else {?>
                                        <?php echo $tpl['default_body']; ?>
                                            <?php }?>
                                        <?php }?>
                                        <?php if ($l == 'dutch') {?>
                                            <?php if (isset($tpl['dutch'])) {?>
                                                <?php echo $tpl['dutch']->body; ?>
                                            <?php } else {?>
                                        <?php echo $tpl['default_body']; ?>
                                            <?php }?>
                                        <?php }?>
                                        <?php if ($l == 'french') {?>
                                            <?php if (isset($tpl['french'])) {?>
                                                <?php echo $tpl['french']->body; ?>
                                            <?php } else {?>
                                        <?php echo $tpl['default_body']; ?>
                                            <?php }?>
                                        <?php }?>
                                        <?php if ($l == 'japanese') {?>
                                            <?php if (isset($tpl['japanese'])) {?>
                                                <?php echo $tpl['japanese']->body; ?>
                                            <?php } else {?>
                                        <?php echo $tpl['default_body']; ?>
                                            <?php }?>
                                        <?php }?>

                                          <?php if ($l == 'spanish') {?>
                                                <?php if (isset($tpl['spanish'])) {?>
                                                    <?php echo $tpl['spanish']->body; ?>
                                                <?php } else {?>
                                        <?php echo $tpl['default_body']; ?>
                                                <?php }?>
                                          <?php }?>

                                          <?php if ($l == 'german') {?>
                                                <?php if (isset($tpl['german'])) {?>
                                                    <?php echo $tpl['german']->body; ?>
                                                <?php } else {?>
                                        <?php echo $tpl['default_body']; ?>
                                                <?php }?>
                                          <?php }?>
                                        </textarea>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 mr-auto ml-auto">
                                    <button type="submit" class="btn btn-lg btn-primary btn-block"><i class="fa fa-save"></i> <?php echo lang('Save Changes'); ?></button>
                                    <?php if ($template->status == 1) { ?>
                                      <button type="button" onclick="disable_template('<?php echo $this->uri->segment(4); ?>')" class="btn btn-lg btn-danger btn-block"><i class="fa fa-paused"></i> <?php echo lang('Disable this email'); ?></button>
                                    <?php } else { ?>
                                          <button type="button" onclick="enable_template('<?php echo $this->uri->segment(4); ?>')" class="btn btn-lg btn-success btn-block"><i class="fa fa-play"></i> <?php echo lang('Enable this email'); ?></button>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row" style="padding-top:20px;padding-bottom:20px;">

                                <?php	$variable = json_decode($template->variables); ?>  
                                <?php	if ($variable) { ?>
                                <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                <th width="20%"><?php echo lang('Variable'); ?></th>
                                                <th width="80%"><?php echo lang('Description'); ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($variable as $v) { ?>
                                                <tr>
                                                <td><strong>{$<?php echo $v->var; ?>}</strong></td>
                                                <td><?php echo $v->desc; ?></td>
                                                </tr>
                                                <?php } ?>
                                                </tbody>
                                </table>  
                                <?php } ?> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var default_texteditor_path = '<?php echo base_url(); ?>assets/grocery_crud/texteditor';
    </script>
    <script src="<?php echo base_url(); ?>assets/grocery_crud/texteditor/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url(); ?>assets/grocery_crud/texteditor/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/grocery_crud/js/jquery_plugins/config/jquery.ckeditor.config.js"></script>
    <script>
        function disable_template(id){

            var t = confirm('<?php echo lang('Are your sure?, system will skip sending email hooks on for this template'); ?>');
            if(t){

                window.location.href = '<?php echo base_url(); ?>admin/setting/disable_email_templates/<?php echo $this->uri->segment(4); ?>';

            }

        }

         function enable_template(id){

            var t = confirm('<?php echo lang('Are your sure?, system will  send email hooks on for this template'); ?>');
            if(t){

                window.location.href = '<?php echo base_url(); ?>admin/setting/enable_email_templates/<?php echo $this->uri->segment(4); ?>';

            }

        }

    </script>