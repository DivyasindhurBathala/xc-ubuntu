<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">
                Companies
            </h6>
            <div class="element-box">
                <h5 class="form-header">
                    <?php echo lang('List Company'); ?>
                    <div class="float-right"> <button class="btn btn-primary btn-sm" class="button"
                            onclick="add_company()"><i class="fa fa-plus-circle"></i> <?php echo lang('Add Company'); ?></button>
                    </div>
                </h5>
                <div class="table-responsive">
                    <table class="table table-striped table-lightfont" id="company">
                        <thead>
                            <tr>
                                <th><?php echo lang('Company ID'); ?>
                                </th>
                                <th><?php echo lang('Name'); ?>
                                </th>
                                <th><?php echo lang('URL'); ?>
                                </th>
                                <th><?php echo lang('Invoicing'); ?>
                                </th>
                                <th><?php echo lang('Ticketing'); ?>
                                </th>
                                <th><?php echo lang('Currency'); ?>
                                </th>
                                <th><?php echo lang('Action'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function add_company() {
        $('#addCompany').modal('toggle');
    }

    function edit_company(companyid) {
        console.log(companyid);
        window.location.href = window.location.protocol + '//' + window.location.host +
            '/master/setting/company_detail/' + companyid;
    }

    function add_superadmin_company(companyid) {
        $('#addUser').modal('toggle');
        $('#companyidx').val(companyid);
    }

    function OpenProduct(companyid) {
        window.location.href = window.location.protocol + '//' + window.location.host +
            '/master/setting/company_products/' + companyid;
    }

    function GenerateWebserver(id) {

        var t = confirm('are you sure to enable this url in webserver?');
        if (t) {

            window.location.href = window.location.protocol + '//' + window.location.host +
                '/master/setting/gen_webserver/' + id;

        }
    }
</script>
<script>
    $(document).ready(function() {
        var urlPortal = $("#company_url").val();

        if(urlPortal == "https://CHANGE_HERE.portal.pareteum.cloud/"){

         $('.add_company_btn').prop('disabled', true);

   }
    $('#mage_invoicing').on('change',function(){
        //var optionValue = $(this).val();
        //var optionText = $('#dropdownList option[value="'+optionValue+'"]').text();
        var optionText = $("#mage_invoicing option:selected").val();
        if(optionText == 1){
            $("#companyid").html('<label for=""><?php echo lang('Companyid Required if you Use Invoicing, consult United telecom'); ?></label><input class="form-control" autocomplete="new-username" type="text" name="companyid" required>');

        }else{
            $("#companyid").html('');

        }
    });
        $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(
            data) {
            $('#company').DataTable({
                "autoWidth": false,
                "processing": true,
                "orderCellsTop": true,
                "ordering": true,
                "serverSide": true,
                "ajax": window.location.protocol + '//' + window.location.host +
                    '/admin/table/getcompanies',
                "aaSorting": [
                    [0, 'desc']
                ],
                "language": {
                    "url": window.location.protocol + '//' + window.location.host +
                        "/assets/clear/js/datatables/lang/" + data.result + ".json"
                },
                "fnRowCallback": function(nRow, aData, iDisplayIndex) {


                    if(aData[3] == "disabled"){
                        $('td:eq(3)', nRow).html('<b><span class="text-danger">'+aData[3]+'</span></b>');
                    }else{
                        $('td:eq(3)', nRow).html('<b><span class="text-success">'+aData[3]+'</span></b>');
                    }

                    if(aData[4] == "disabled"){
                        $('td:eq(4)', nRow).html('<b><span class="text-danger">'+aData[4]+'</span></b>');
                    }else{
                        $('td:eq(4)', nRow).html('<b><span class="text-success">'+aData[4]+'</span></b>');
                    }

                    $('td:eq(0)', nRow).html(
                        '<a  href="javascript::void(0);" onclick="edit_company(\'' +
                        aData[0] + '\');">' + aData[0] + '</a>');
                    $('td:eq(1)', nRow).html(
                        '<a  href="javascript::void(0);" onclick="edit_company(\'' +
                        aData[0] + '\');">' + aData[1] + ' ('+aData[8]+')</a>');
                    if (aData[7] == "1") {
                        $('td:eq(6)', nRow).html(
                            '<a  href="#" onclick="add_superadmin_company(\'' + aData[
                            6] +
                            '\');"><i class="fa fa-user-plus"></i></a> <a  href="#" onclick="OpenProduct(\'' +
                            aData[0] + '\');"><i class="fa fa-cubes"></i></a>');
                    } else if (aData[7] == "0") {
                        $('td:eq(6)', nRow).html(
                            '<a  href="#" onclick="add_superadmin_company(\'' + aData[
                            6] +
                            '\');"><i class="fa fa-user-plus"></i></a> <a  href="#" onclick="OpenProduct(\'' +
                            aData[0] +
                            '\');"><i class="fa fa-cubes"></i></a> <a  href="#" onclick="GenerateWebserver(\'' +
                            aData[6] + '\');"><i class="fa fa-globe"></i></a>');
                    }
                    return nRow;
                },
            });
        });
    });
</script>
<div class="modal fade" id="addCompany">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="pushCompany"
                action="<?php echo base_url(); ?>master/setting/add_companies">

                <div class="modal-header">
                    <h4 class="modal-title"><?php echo lang('Add Company'); ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" id="companyid">

                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('Company Name'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="companyname"
                                    required>
                            </div>
                            <div class="row">
                            <div class="form-group col-md-6">
                                <label for=""><?php echo lang('Country'); ?></label>
                                <select class="form-control" id="country" name="country">
                                <?php foreach (getCountries() as $key => $country) {
    ?>
                                <option value="<?php echo $key; ?>" <?php if ($setting->country_base == $key) {
        ?>
                                    selected<?php
    } ?>><?php echo $country; ?></option>
                                <?php
}?>
                            </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for=""><?php echo lang('Currency'); ?></label>
                                <select class="form-control" id="currency" name="currency">
                                <?php //print_r($currencies);?>
<?php foreach ($currencies as $key => $cur) {
        ?>
<option value="<?php echo $cur->symbol_native; ?>"><?php echo $key." - ".$cur->name." - <span class='text-danger'>".$cur->symbol_native; ?></span></option>
<?php
    } ?>
</select>

                            </div>
                            </div>
                            <div class="form-group">
                                <label for=""><?php echo lang('Portal URL (ending with traling /)'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="company_url" id="company_url" value="https://CHANGE_HERE.portal.pareteum.cloud/"
                                    required>
                            </div>
                            <div class="form-group">
                                <fieldset>
                                    <label for="platform"><?php echo lang('Platform'); ?></label>
                                    <select class="form-control" id="platform" name="platform">

                                        <option value="ARTA"><?php echo lang('ARTA Platform'); ?></option>
                                        <option value="TEUM" selected><?php echo lang('Core Server'); ?></option>

                                    </select>
                                </fieldset>
                            </div>


                           <div class="row">
                            <div class="form-group col-sm-6">
                                <fieldset>
                                    <label for="whmcs_ticket"><?php echo lang('Helpdesk'); ?></label>
                                    <select class="form-control" id="whmcs_ticket" name="whmcs_ticket">

                                        <option value="1"><?php echo lang('Yes'); ?></option>
                                        <option value="0"><?php echo lang('No'); ?></option>

                                    </select>
                                </fieldset>
                            </div>

                            <div class="form-group  col-sm-6">
                                <fieldset>
                                    <label for="mage_invoicing"><?php echo lang('Invoicing'); ?></label>
                                    <select class="form-control" id="mage_invoicing" name="mage_invoicing">
                                        <option value="1"><?php echo lang('Yes'); ?></option>
                                        <option value="0" selected><?php echo lang('No'); ?></option>
                                    </select>
                                </fieldset>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer  smtp_sender. smtp_name-->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary add_company_btn"><?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="editCompany">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form method="post"
                action="<?php echo base_url(); ?>master/setting/edit_companies">
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo lang('Edit Company'); ?>
                    </h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for=""><?php echo lang('Role Name'); ?></label>
                                <input class="form-control" autocomplete="new-username" type="text" name="name"
                                    required>
                            </div>
                            <div class="form-group">
                                <fieldset>
                                    <label for="exampleSelect1">Copy Permission from Role</label>
                                    <select class="form-control" id="copy" name="copy">
                                        <?php foreach (getStafRoles() as $role) {
        ?>
                                        <option
                                            value="<?php echo $role['name']; ?>">
                                            <?php echo $role['description']; ?>
                                        </option>
                                        <?php
    }?>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="addUser">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo lang('Add Superuser'); ?>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">


                <form method="post"
                    action="<?php echo base_url(); ?>master/setting/add_superadmin/">
                    <input type="hidden" name="companyid" id="companyidx">
                    <input type="hidden" name="role" value="superadmin">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="firstname"><?php echo lang('Firstname'); ?></label>
                                    <input name="firstname" class="form-control" id="firstname" type="text"
                                        placeholder="Firstname" value="" required>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="lastname"><?php echo lang('Lastname'); ?></label>
                                    <input name="lastname" class="form-control" id="lastname" type="text"
                                        placeholder="Lastname" value="" required>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="address1"><?php echo lang('Address'); ?></label>
                                    <input name="address1" class="form-control" id="address1" type="text"
                                        placeholder="Address" value="" required>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="Postcode"><?php echo lang('Postcode'); ?></label>
                                    <input name="postcode" class="form-control" id="postcode" type="text"
                                        placeholder="Postcode" value="" required>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="City"><?php echo lang('City'); ?></label>
                                    <input name="city" class="form-control" id="city" type="text" placeholder="City"
                                        value="" required>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label for="exampleSelect1"><?php echo lang('Country'); ?></label>
                                    <select class="form-control" id="country" name="country">
                                        <?php foreach (getCountries() as $key => $country) {
        ?>
                                        <option
                                            value="<?php echo $key; ?>"
                                            <?php if ($key == "NL") {
            ?> selected<?php
        } ?>><?php echo $country; ?>
                                        </option>
                                        <?php
    }?>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label for="exampleSelect1"><?php echo lang('Languages'); ?></label>
                                    <select class="form-control" id="language" name="language">
                                        <?php foreach (getLanguages() as $key => $lang) {
        ?>
                                        <option
                                            value="<?php echo $key; ?>"
                                            <?php if ($key == "dutch") {
            ?> selected<?php
        } ?>><?php echo $lang; ?>
                                        </option>
                                        <?php
    }?>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="Phonenumber"><?php echo lang('Phonenumber'); ?></label>
                                    <input name="phonenumber" class="form-control" id="phonenumber" type="number"
                                        placeholder="Phonenumber" value="" required>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="Email"><?php echo lang('Email'); ?></label>
                                    <input name="email" class="form-control" id="email" type="email"
                                        placeholder="Email address" value="" required>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="NationalNr"><?php echo lang('NationalNr'); ?>.</label>
                                    <input name="nationalnr" class="form-control" id="NationalNr" type="number"
                                        placeholder="NationalNr" value="">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <fieldset>
                                    <label class="control-label" for="Password"><?php echo lang('Password'); ?>.</label>
                                    <input name="password" class="form-control" id="Password" type="text"
                                        value="<?php echo random_string('alnum', 8); ?>"
                                        readonly>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-md btn-primary btn-block"><i
                                        class="fa fa-save"></i> <?php echo lang('Save Administrator'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$('.add_company_btn').click(function(){
$('.add_company_btn').prop('disabled', true);
$('#pushCompany').submit();

})
$('#company_url').keyup(function(){

var urlPortal = $("#company_url").val();
    if(urlPortal == "https://CHANGE_HERE.portal.pareteum.cloud/"){
        $('.add_company_btn').prop('disabled', true);
    }else{
$.ajax({
type: "POST",
url: "<?php echo base_url(); ?>admin/complete/check_url/",
data: {
    url: urlPortal
},
dataType: "json",
success: function(data) {
  if(data.result){
    $('.add_company_btn').prop('disabled', true);
  }else{
    $('.add_company_btn').prop('disabled', false);
  }
},
error: function() {
  alert('error handling here');
}
});

    }

});
</script>