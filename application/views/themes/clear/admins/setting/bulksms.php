
<?php if(empty($this->uri->segment(4))){ ?>
	<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Mass SMS
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Please choose the criteria below'); ?>
				</h5>
				<div class="table-responsive">
					<form id="bulksms" method="post" action="<?php echo base_url(); ?>admin/setting/send_mass_sms">
					<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label for="products">Products</label>
											<select name="products[]" class="form-control" multiple>
												<?php foreach(getProductSell($this->session->cid) as $product){ ?>
													<option value="<?php echo $product->id; ?>"><?php echo $product->name; ?></option>
												<?php } ?>

											</select>
										</div>
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<label for="products">Subscription Status</label>
											<select name="status" class="form-control">
												<?php foreach(array('Active','Pending','All') as $status){ ?>
													<option value="<?php echo $status; ?>"><?php echo $status; ?></option>
												<?php } ?>

											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="postcodes">Postcode</label>
											<select name="postcodes[]" class="form-control" multiple>
												<?php foreach(getPostcodesList($this->session->cid) as $pc){ ?>
													<option value="<?php echo $pc->postcode; ?>"><?php echo $pc->postcode; ?></option>
												<?php } ?>

											</select>
											
										</div>
									</div>
								</div>
						
									<div class="row">
									<div class="col-md-6">
											<label for="postcodes">Message</label>
											<textarea class="form-control" name="message"></textarea>
											<span class="text-help">If you add characters more than 160 the sms will be send in two seperate text and you will be charge as 2 sms @25 cent/sms</span>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<button class="btn btn-md btn-primary" type="button" id="send">Send SMS</button>
									</div>
								</div>
							</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Batches
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('List Batches'); ?> 
				</h5>
				<div class="table-responsive">
					<table class="table table-striped table-lightfont" id="batches">
						<thead>
							<tr>
								<th width="10%"><?php echo lang('BatchID'); ?></th>
								<th width="10%"><?php echo lang('Date'); ?></th>
								<th width="55%"><?php echo lang('Message'); ?></th>
								<th width="15%"><?php echo lang('Total SMS'); ?></th>
								<th width="10%"><?php echo lang('Action'); ?></th>
						
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
		$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#batches').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_sms_log_batch',
"aaSorting": [[0, 'asc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 $('td:eq(4)', nRow).html('<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/setting/send_mass_sms/view/'+aData[0]+'">View Batches</a> <button class="btn btn-primary btn-sm" onclick="CancelBatch('+aData[0]+')">Cancel</button>');
//$('td:eq(1)', nRow).html('<a  href="javascript::void(0);" onclick="edit_company(\''+aData[0]+'\');">'+aData[1]+'</a>');
return nRow;
},
});
});
});
</script>	


<script>
	function CancelBatch(id){
		var t=confirm('Are you sure? this only work if the status of the sms still in Queued otherwise this command will be ignored');
		if(t){
			console.log(id);
			$.ajax({
url: window.location.protocol + '//' + window.location.host + '/admin/setting/cancel_sms_batch',
type: 'post',
dataType: "json",
data: {batchid:id},
success: function(data) {
alert('Batchid '+id+' has been cancelled');
 
}
});
			
		}
		
	}
</script>

<script>
	$('#send').click(function(){
		$('#send').prop('disabled', true);
		event.preventDefault();
		$('#bulksms').submit();

	});

</script>

<?php }else{ ?>

<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Mass SMS
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('List SMS'); ?> 
				</h5>
				<div class="table-responsive">
					<table class="table table-striped table-lightfont" id="company">
						<thead>
							<tr>
								<th width="10%"><?php echo lang('Date Created'); ?></th>
								<th width="10%"><?php echo lang('Recipient'); ?></th>
								<th width="15%"><?php echo lang('CustomerID'); ?></th>
								<th width="45%"><?php echo lang('Message'); ?></th>
								<th width="10%"><?php echo lang('Date Sent'); ?></th>
								<th width="10%"><?php echo lang('Status'); ?></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
		$(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#company').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/get_sms_log',
"aaSorting": [[0, 'asc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 $('td:eq(2)', nRow).html(aData[9]+' <b>'+aData[2]+'</b>');
//$('td:eq(1)', nRow).html('<a  href="javascript::void(0);" onclick="edit_company(\''+aData[0]+'\');">'+aData[1]+'</a>');
return nRow;
},
});
});
});
</script>	
<?php } ?>
