<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Roles
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        <?php echo lang('List Roles'); ?> <div class="close"> <a class="btn btn-primary btn-sm" href="javascript::void(0)" onclick="add_role()"><i class="fa fa-plus-circle"></i> <?php echo lang('Add New Role'); ?></a></div>
        </h5>
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="clients">
            <thead>
              <tr>
                <th><?php echo lang('ID'); ?></th>
                <th><?php echo lang('Role'); ?></th>
                <th><?php echo lang('Description'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
function add_role(){
$('#AddRole').modal('toggle');
}

function delete_role(id){
var t = confirm('Are you sure?');
if(t  == true){
  window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/setting/delete_role/' + id);
}
}
</script>
<div class="modal fade" id="AddRole">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url(); ?>admin/setting/add_role">
        <input type="hidden" name="companyid" value="<?php echo $this->session->cid; ?>">
        <div class="modal-header">
          <h4 class="modal-title"><?php echo lang('Add Porting'); ?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for=""><?php echo lang('Role Name'); ?></label>
                <input class="form-control" autocomplete="new-username"  type="text" name="name" required>
              </div>
              <div class="form-group">
                <fieldset>
                  <label for="exampleSelect1">Copy Permission from Role</label>
                  <select class="form-control" id="copy" name="copy">
                    <?php foreach (getStafRoles() as $role) {?>
                    <option value="<?php echo $role['name']; ?>"><?php echo $role['description']; ?></option>
                    <?php }?>
                  </select>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Submit'); ?></button>
        </div>
      </form>
    </div>
  </div>
</div>