<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			<?php echo lang('Setting'); ?>
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Edit Staf'); ?>
				</h5>
				<div class="table-responsive">
					<form method="post" action="<?php echo base_url(); ?>admin/setting/staf_edit/<?php echo $staf->id; ?>" enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?php echo $staf->id; ?>">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="firstname"><?php echo lang('Firstname'); ?></label>
										<input  name="firstname" class="form-control" id="firstname" type="text" placeholder="<?php echo lang('Firstname'); ?>"  value="<?php echo $staf->firstname; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="lastname"><?php echo lang('Lastname'); ?></label>
										<input  name="lastname" class="form-control" id="lastname" type="text" placeholder="<?php echo lang('Lastname'); ?>"  value="<?php echo $staf->lastname; ?>" required>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="address1"><?php echo lang('Address'); ?></label>
										<input  name="address1" class="form-control" id="address1" type="text" placeholder="<?php echo lang('Address'); ?>"  value="<?php echo $staf->address1; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="Postcode"><?php echo lang('Postcode'); ?></label>
										<input  name="postcode" class="form-control" id="postcode" type="text" placeholder="<?php echo lang('Postcode'); ?>"  value="<?php echo $staf->postcode; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="City"><?php echo lang('City'); ?></label>
										<input  name="city" class="form-control" id="city" type="text" placeholder="<?php echo lang('City'); ?>"  value="<?php echo $staf->city; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label for="exampleSelect1"><?php echo lang('Country'); ?></label>
										<select class="form-control" id="country"  name="country" >
											<?php foreach (getCountries() as $key => $country) {?>
											<option value="<?php echo $key; ?>"<?php if ($key == $staf->country) {?> selected<?php }?>><?php echo $country; ?></option>
											<?php }?>
										</select>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label for="exampleSelect1"><?php echo lang('Languages'); ?></label>
										<select class="form-control" id="language" name="language" >
											<?php foreach (getLanguages() as $key => $lang) {?>
											<option value="<?php echo $key; ?>"<?php if ($key == $staf->language) {?> selected<?php }?>><?php echo $lang; ?></option>
											<?php }?>
										</select>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="Phonenumber"><?php echo lang('Phonenumber'); ?></label>
										<input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="<?php echo lang('Phonenumber'); ?>"  value="<?php echo $staf->phonenumber; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="Email"><?php echo lang('Email'); ?></label>
										<input name="email"  class="form-control" id="email" type="email" placeholder="<?php echo lang('Email address'); ?>"  value="<?php echo $staf->email; ?>" required>
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="NationalNr"><?php echo lang('NationalNr'); ?>.</label>
										<input name="nationalnr"  class="form-control" id="<?php echo lang('NationalNr'); ?>" type="number" placeholder="<?php echo lang('NationalNr'); ?>" value="<?php echo $staf->nationalnr; ?>">
									</fieldset>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="file"><?php echo lang('Picture'); ?></label>
										<input name="picture"  class="form-control" type="file">
									</fieldset>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="avatar"><img src="<?php echo base_url(); ?>assets/img/staf/<?php echo $staf->picture; ?>" height="70"></div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<fieldset>
										<label class="control-label" for="Password"><?php echo lang('Password'); ?>.</label>
										<input name="password"  class="form-control" id="password" type="password" placeholder="<?php echo lang('Leave empty for no change'); ?>">
									</fieldset>
								</div>
							</div>
						</div>
						<?php if ($this->session->role == "superadmin") {?>
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<fieldset class="form-group">
										<label for="exampleSelect1"> <?php echo lang('Role/Function'); ?></label>
										<select class="form-control" id="role" name="role">
											<?php foreach (getStafRoles() as $role) {?>
											<option value="<?php echo $role['name']; ?>"<?php if ($staf->role == $role['name']) {?> selected<?php }?>><?php echo $role['description']; ?></option>
											<?php }?>
										</select>
										<!--
										<option value="superadmin"> <?php echo lang('Administrator'); ?></option>
										<option value="sales"> <?php echo lang('Sales'); ?></option>
										<option value="administratie"> <?php echo lang('Administratie medewerker'); ?></option>
										<option value="finance"> <?php echo lang('Finance'); ?></option>
									</select>
									-->
								</fieldset>
							</div>
						</div>
					</div>
					<?php }?>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
									<button type="submit" class="btn btn-lg btn-primary btn-block"><i class="fa fa-save"></i>  <?php echo lang('Save Changes'); ?></button>
							</div>
						</div>
						<?php if ($this->session->role == "superadmin") {?>
							<?php if ($staf->status == 'Active') {?>
<div class="col-sm-12">
							<div class="form-group">

								<a href="<?php echo base_url(); ?>admin/setting/disable_admin_account/<?php echo $staf->id; ?>" class="btn btn-lg btn-danger btn-block"><i class="fa fa-stop"></i>  <?php echo lang('Disable Account'); ?></a>
							</div>
						</div>
								<?php } else {?>
									<div class="col-sm-12">
							<div class="form-group">

								<a href="<?php echo base_url(); ?>admin/setting/enable_admin_account/<?php echo $staf->id; ?>" class="btn btn-lg btn-info btn-block"><i class="fa fa-play"></i>  <?php echo lang('Enable Account'); ?></a>
							</div>
						</div>
								<?php }?>

							<?php }?>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>