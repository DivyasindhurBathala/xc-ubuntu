<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			<?php echo lang('Setting'); ?>
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Invoice Configuration'); ?>
				</h5>
				<div class="table-responsive">
					<div class="row">
			<div class="col-lg-3">
			</div>
			<div class="col-lg-6">
				<div class="card text-black bg-white">
					<div class="card-header bg-success"><?php echo lang('Congratulation'); ?>!</div>
					<div class="card-body">
						<h4 class="card-title"><?php echo lang('You are using the latest version of UnitedCRM'); ?></h4>
						<p class="card-text"><?php echo lang('United Version. 1.33'); ?></p>
						<p><?php echo lang('Check our latest version on'); ?> <a href="https://www.exocom.be"><?php echo lang('United development Site</p></p>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
			</div>
		</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(".toggle-password").click(function() {
$("#passwordnya").prop('type', 'text');
});
</script>