<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Roles
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Edit Role'); ?> :<?php echo ucfirst($role->name); ?>
				<div class="close"><a href="<?php echo base_url(); ?>admin/setting/roles"><i class="fa fa-arrow-left"></i> [Back To List]</a></div>
				</h5>
				<input type="checkbox" id="checkAll"> [<?php echo lang('Check all'); ?>]
				<hr />
				<div class="table-responsive">
					<form method="post" action="<?php echo base_url(); ?>admin/setting/edit_role/<?php echo $this->uri->segment(4); ?>">

					<input type="hidden" name="rolename" value="<?php echo $role->name; ?>">
					<div class="row">
							<div class="col-md-4">
							<?php foreach ($permissions['0'] as $row) {?>

							<div class="form-check">
								<label class="form-check-label">
									<input name="permissions[<?php echo $row['id']; ?>]" type="checkbox" class="form-check-input"<?php if ($row['checked'] == 1) {?> checked<?php }?>>
									<?php echo ucfirst($row['folder']) . ' ' . ucfirst($row['controller']) . ' ' . ucfirst(str_replace('_', ' ', $row['method'])); ?>
								</label>
							</div>
							<?php }?>
						</div>

							<div class="col-md-4">
							<?php foreach ($permissions['1'] as $row) {?>

							<div class="form-check">
								<label class="form-check-label">
									<input  name="permissions[<?php echo $row['id']; ?>]" type="checkbox" class="form-check-input"<?php if ($row['checked'] == 1) {?> checked<?php }?>>
									<?php echo ucfirst($row['folder']) . ' ' . ucfirst($row['controller']) . ' ' . ucfirst(str_replace('_', ' ', $row['method'])); ?>
								</label>
							</div>
							<?php }?>
						</div>

							<div class="col-md-4">
							<?php foreach ($permissions['2'] as $row) {?>

							<div class="form-check">
								<label class="form-check-label">
									<input  name="permissions[<?php echo $row['id']; ?>]" type="checkbox" class="form-check-input"<?php if ($row['checked'] == 1) {?> checked<?php }?>>
									<?php echo ucfirst($row['folder']) . ' ' . ucfirst($row['controller']) . ' ' . ucfirst(str_replace('_', ' ', $row['method'])); ?>
								</label>
							</div>
							<?php }?>
						</div>


				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<button type="submit" class="btn btn-md btn-primary"><?php echo lang('Save Permissions'); ?></button>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>

</div>
</div>
<script>
	$( document ).ready(function() {
	$("#checkAll").click(function(){
		console.log('toto');
    $('input:checkbox').not(this).prop('checked', this.checked);
});
});
</script>