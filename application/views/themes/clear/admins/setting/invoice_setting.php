<div class="content-i">
	<div class="content-box">
		<div class="element-wrapper">
			<h6 class="element-header">
			Setting
			</h6>
			<div class="element-box">
				<h5 class="form-header">
				<?php echo lang('Invoice Configuration'); ?>
				</h5>
				<div class="table-responsive">
					<form method="post" action="<?php echo base_url(); ?>admin/setting/invoices" class="form-horizontal" enctype="multipart/form-data">
							<fieldset>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2">Document Logo</label>
									<div class="col-sm-6">
										<input name="logo" type="file" class="form-control" value="<?php echo $setting->invoice_logo; ?>">
										<small id="emailHelp" class="form-text text-muted">Size must be 929 x 211 Pixel, this will be used on your Invoice, Creditnote and Quote</small>
									</div>

									<div class="col-sm-4">
											<?php if (file_exists(FCPATH . 'assets/img/' . $setting->invoice_logo)) {?>
										<img src="<?php echo base_url(); ?>assets/img/<?php echo $setting->invoice_logo; ?>" height="30">
											<?php }?>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2">Document Footer Logo</label>
									<div class="col-sm-6">
										<input name="footer" type="file" class="form-control" value="<?php echo $setting->invoice_footer; ?>">
										<small  id="emailHelp" class="form-text text-muted">Size must be 1240 x 118 Pixel, this will be used on your Invoice, Creditnote and Quote</small>
									</div>
									<div class="col-sm-4">
											<?php if (file_exists(FCPATH . 'assets/img/' . $setting->invoice_footer)) {?>
										<img src="<?php echo base_url(); ?>assets/img/<?php echo $setting->invoice_footer; ?>" height="30">
										<?php }?>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2">Next InvoiceNumber</label>
									<div class="col-sm-10">
										<input name="invoicenum"  min="<?php echo $setting->invoicenum; ?>"  type="number" class="form-control" value="<?php echo $setting->invoicenum; ?>">
										<small id="emailHelp" class="form-text text-muted">This number can be lower than <?php echo $setting->invoicenum; ?></small>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleTextarea" class="col-sm-2">Invoice Prefix</label>
									<div class="col-sm-10">
										<input name="invoice_prefix"  type="text" class="form-control" value="<?php echo $setting->invoice_prefix; ?>">
										<small id="emailHelp" class="form-text text-muted">Please use ISO Character</small>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2">Next CreditnoteNumber</label>
									<div class="col-sm-10">
										<input name="creditnotenum"  min="<?php echo $setting->creditnotenum; ?>"  type="number" class="form-control" value="<?php echo $setting->creditnotenum; ?>">
										<small id="emailHelp" class="form-text text-muted">This number can be lower than <?php echo $setting->creditnotenum; ?></small>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleTextarea" class="col-sm-2">Creditnote Prefix</label>
									<div class="col-sm-10">
										<input name="creditnote_prefix"  type="text" class="form-control" value="<?php echo $setting->creditnote_prefix; ?>">
										<small id="emailHelp" class="form-text text-muted">Please use ISO Character</small>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2">Next QuoteNumber</label>
									<div class="col-sm-10">
										<input name="quotenum"   min="<?php echo $setting->quotenum; ?>" type="number" class="form-control" value="<?php echo $setting->quotenum; ?>">
										<small id="emailHelp" class="form-text text-muted">This number can be lower than <?php echo $setting->quotenum; ?></small>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2">QuoteNumber Prefix</label>
									<div class="col-sm-10">
										<input name="quote_prefix"  type="text" class="form-control" value="<?php echo $setting->quote_prefix; ?>">
										<small id="emailHelp" class="form-text text-muted">Please use ISO Character</small>
									</div>
								</div>
								<div class="form-group row">
									<label for="exampleInputEmail1" class="col-sm-2"></label>
									<div class="col-sm-10">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</fieldset>
							</form>
				</div>
			</div>
		</div>
	</div>
</div>
