<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Superadmin Tools'); ?> 
       <div class="close">
       
       </div>
      </h6>
      <div class="element-box">
        
         <div class="table-responsive">
         <div class="row">
        <div class="col-md-3">
        
        <button class="btn btn-block btn-primary">Block Premier Telecom</button>
        <button class="btn btn-block btn-primary">Block Premier ZnacTelecom</button>
        </div>
        <div class="col-md-3">
        <button class="btn btn-block btn-primary">Send Ello Reminder</button>
        <button class="btn btn-block btn-primary">Send Delta Invoices</button>
        <button class="btn btn-block btn-primary">Send Delta Reminder</button>
        </div>
        <div class="col-md-3">
        </div>
        <div class="col-md-3">
        </div>
         </div>
        
        </div>
      </div>
    </div>
  </div>
</div>