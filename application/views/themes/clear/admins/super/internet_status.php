

<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Internet Status Tools'); ?> 
       <div class="close">
       
       </div>
      </h6>
      <div class="element-box">
         <div class="table-responsive">
         
         <div class="row">
        <div class="col-md-4">
        	<h2>User Information</h2>
        	<table class="table table-striped table-hover">
        	<tr>
        		<td>Username:</td>
        		<td><?php echo $xdsl->username; ?></td>
        	</tr>

        		<tr>
        		<td>Password:</td>
        			<td><?php echo $xdsl->password; ?></td>
        	</tr>
        		<tr>
        		<td>IP:</td>
        		<td><?php echo $xdsl->ipaddress; ?></td>
        	</tr>
        		<tr>
        		<td>Status:</td>
        			<td><img src="<?php echo base_url(); ?>assets/img/<?php echo $xdsl->status; ?>.gif" height="40"></td>
        	</tr>
        	<tr>
        		<td>Proximus Product:</td>
        			<td><?php echo strtoupper($xdsl->offer); ?></td>
        	</tr>

        	<tr>
        		<td>Last Auth:</td>
        			<td><?php echo strtoupper($xdsl->last_auth); ?></td>
        	</tr>

        	<tr>
        		<td>Last Auth Status:</td>
        			<td><?php echo strtoupper($xdsl->last_auth_status); ?></td>
        	</tr>
        	</table>
        	<form method="post" action="<?php echo base_url(); ?>admin/super/getinternet">
        	  
       <div class="form-group">
      <label for="exampleInputPassword1">Username</label>
      <input type="text" class="form-control" id="exampleInputPassword1" name="username">
    </div>
    <div class="form-group">
      <label for="exampleSelect1">realm</label>
      <select class="form-control" id="exampleSelect1" name="realm">
        <option value="unitedadsl">UNITEDADSL</option>
        <option value="uniteddsltest" selected>UNITEDDSLTEST</option>
        <option value="happymanny">HAPPYMANNY</option>
        <option value="united">UNITED</option>
       <option value="znaktelecom">ZNAKTELECOM</option>
        <option value="telenatie">TELENATIE</option>
      </select>
    </div>
      <div class="form-group">
    <button class="btn btn-primary btn-block btn-md" type="submit">Submit</button>
</div>
</form>
        </div>
        <div class="col-md-8">
<h2>Session History</h2>
  	<table class="table table bordered table-hover" id="session">
  		<thead>
  			<tr class="active">
  			<th>Start</th>
  				<th>Stop</th>
  					<th>Bits Upload</th>
  						<th>Bits Download</th>
  						<th>Ip Address</th>
  						<th>Description</th>
  		</tr>
  	</thead>
  	<tbody>
     	<?php if($xdsl->sessions){ ?>
<?php foreach($xdsl->sessions as $row){ ?>
	<tr>
<td><?php echo $row->acctstarttime; ?></td>
<td><?php echo $row->acctstoptime; ?></td>
<td><?php echo $row->acctoutputoctets; ?></td>
<td><?php echo $row->acctinputoctets; ?></td>
<td><?php echo $row->framedipaddress; ?></td>
<td><?php echo $row->acctterminatecause; ?></td>

	</tr>


<?php } ?>
</tbody>
     	<?php } ?>
        	
        	</table>

        </div>
       
         </div>
        
        </div>
      </div>
    </div>
  </div>
</div>