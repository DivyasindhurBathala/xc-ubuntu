 <div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      <?php echo lang('Add Subscription'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
      <?php echo lang('Please choose the type below'); ?>:
        </h5>
        <div class="table-responsive">
        	<div class="row">
          <div class="col-md-6">
        <a class="btn btn-lg btn-block btn-md btn-secondary" href="<?php echo base_url(); ?>admin/subscription/add_internet"><i class="fa fa-laptop fa-5x"></i><br/><?php echo lang('Internet'); ?></a>
      </div>
        <div class="col-md-6">
        <a class="btn btn-lg btn-block btn-md btn-primary" href="<?php echo base_url(); ?>admin/subscription/add_mobile"><i class="fa fa-mobile fa-5x"></i><br/><?php echo lang('Mobile'); ?></a>
      </div>

        </div>
    </div>
</div>
</div>
</div>
</div>