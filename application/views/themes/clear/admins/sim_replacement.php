<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Sim Replacement Requests
      </h6>
      <div class="element-box">
        <h5 class="form-header">
       <?php echo lang('List sim needs replacement'); ?>
        </h5>
        <div class="table-responsive">
            <table class="table table-striped table-lightfont" id="services">
              <thead>
                <tr>
                  <th><?php echo lang('Service ID#'); ?></th>
                  <th><?php echo lang('Customername'); ?></th>
                  <th><?php echo lang('Number'); ?></th>
                  <th><?php echo lang('Serial'); ?></th>
                  <th><?php echo lang('Recurring'); ?></th>
                  <th><?php echo lang('Action'); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="SwapModal">
<div class="modal-dialog">
  <div class="modal-content">
    <form method="post" action="<?php echo base_url(); ?>admin/subscription/swap_simcard">
      <input type="hidden" id="msisdn" name="msisdn" value="">
      <input type="hidden" id="SN"  name="SN" value="">
      <input type="hidden" id="userid"  name="userid" value="">
      <input type="hidden" id="serviceid"  name="serviceid" value="">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Do you wish to swap SIMcard?'); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <fieldset>
                <label class="control-label" for="msisdn"><?php echo lang('CLI(Number)'); ?></label>
                <input  id="CLI" name="CLI" class="form-control"  type="text" value="" readonly>
              </fieldset>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <fieldset>
                <label class="control-label" for="msisdn"><?php echo lang('Current SIMCARD'); ?></label>
                <input  id ="current_simnumber" name="current_simnumber" class="form-control"  type="text" value="" readonly>
              </fieldset>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <fieldset>
                <label class="control-label" for="msisdn"><?php echo lang('New SIMCARD'); ?></label>
                <input  name="new_simnumber" class="form-control"  type="text" value="" required>
                <span class="text-help"><?php echo lang('This will revoke old simcard, please make sure the simcard has been received by customer before executing this'); ?></span>
              </fieldset>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-md btn-primary"><i class="fa fa-thumbs-up"></i> <?php echo lang('Yes Swap Simcard'); ?></button>
      </div>
    </form>
  </div>
</div>
</div>
<script>
	function replace(id){

		$.ajax({
url: '<?php echo base_url(); ?>admin/subscription/getSim/',
type: 'post',
dataType: 'json',
success: function (data) {
var sn = data.sn;
var service =data.service;
var mobile =data.mobile;
console.log(mobile);
if(sn){
$('#msisdn').val(data.sn.MSISDNNr);
$('#CLI').val(data.sn.MSISDNNr);
$('#SN').val(data.sn.SN.trim());
$('#userid').val(data.service.userid);
$('#serviceid').val(id);
$('#current_simnumber').val(mobile.SIMCardNbr);
}
$('#SwapModal').modal('toggle');
},
data: {'id': id}
});

	}
</script>
