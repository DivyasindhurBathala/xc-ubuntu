<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
       <?php echo lang('Client List'); ?>
       <div class="float-right">
        <?php if ($this->session->cid == 2) {
    ?>
          <button class="btn btn-rounded btn-danger" type="button" onclick="importing()"><?php echo lang('Import Customer From magebo'); ?></button>
        <?php
} ?>
       <a class="btn btn-rounded btn-success" id="newinvoice" href="<?php echo base_url(); ?>admin/client/export_clients"><i class="fa fa-file-excel"></i> <?php echo lang('Export Clients'); ?></a>

          <a class="btn btn-rounded btn-success" id="newinvoice" href="<?php echo base_url(); ?>admin/client/add"><i class="fa fa-plus-circle"></i><?php echo lang('Add New Client'); ?></a>
        </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
           <!-- <?php echo lang('List Clients'); ?> -->
        </h5>

          <div id="columnchart_material" style="width: 100%; height: 200px;"></div>

         <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="clients">
            <thead>
              <tr>
                <th><?php echo lang('#ClientID'); ?></th>
                <th><?php echo lang('ArtiliumID'); ?></th>
                <th><?php echo lang('Customer'); ?></th>
                <th><?php echo lang('Email'); ?></th>

                <th><?php echo lang('Acive/Pending Subscriptions'); ?></th>
                <th><?php echo lang('Reseller'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
       <?php if ($stats) {
        ?>
      <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([<?php echo implode(',', $stats); ?>]);

        var options = {
          chart: {
            title: '<?php echo lang('Client Registration'); ?>'
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
  <?php
    } ?>
<script>
  $(document).ready(function()
{
/*$.ajax({
           url: '<?php echo base_url(); ?>admin/subscription/get_pending_port_out_pending/',
           dataType: 'json',
           success: function (data) {
              $(".portout").html(data.total);

              if(data.total >= 1){
  $('#portoutpending').modal({backdrop: 'static', keyboard: false});
  $('#portoutx').html('<h3 class="text-danger">Do you know processing portout must be done as soon as it arrived?, if they aren\'t allowed yet, please rejected them. At this moment you have '+data.total+' pending request(s)</h3>');
              }
           },

       });
*/
function deleteRecord(mech_id,row_index) {
$.ajax({
        url:"{{action('MechanicController@destroy')}}",
        type: 'get',
        data: {
              "id": mech_id,
              "_token": token,
              },
        success: function ()
             {
              var i = row_index.parentNode.parentNode.rowIndex;
              document.getElementById("table1").deleteRow(i);
            }
     });
}
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
var x = new Date();
var y = x.getFullYear().toString();
var m = (x.getMonth() + 1).toString();
var d = x.getDate().toString();
(d.length == 1) && (d = '0' + d);
(m.length == 1) && (m = '0' + m);
var date = y +'-'+ m +'-'+ d;
 $('#clients').DataTable({
"autoWidth": false,
"processing": true,
"orderCellsTop": true,
"ordering": true,
"serverSide": true,
"colReorder": true,
"pageLength": 10,
"ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getclients',
"aaSorting": [[1, 'desc']],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
 /*
  if(aData[4] == 'directdebit' ){

  $('td:eq(4)', nRow).html('<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/DD.png" height="16">');
}else if(aData[4] == 'banktransfer') {
  $('td:eq(4)', nRow).html('<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/BT.png" height="16">');
  }else{
  $('td:eq(4)', nRow).html('<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/others.png" height="16">');
  }
  */
if(aData[7] == date ){
$('td:eq(0)', nRow).html(aData[0] + '<img src="'+window.location.protocol + '//' + window.location.host + '/assets/img/new-sticker.png" height="16">');
}
$('td:eq(0)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '">'+aData[0]+'</a>');
$('td:eq(4)', nRow).html(aData[4]+'/'+aData[5]);
$('td:eq(5)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/agent/detail/' + aData[9] + '">'+aData[7]+'</a>');
$('td:eq(1)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '">'+aData[1]+'</a>');
$('td:eq(2)', nRow).html('<a  href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><i class="fa fa-user"></i> '+aData[2]+'</a>');
$('td:eq(3)', nRow).html('<a href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><i class="fa fa-envelope"></i> '+aData[3]+'</a>');
<?php if ($this->session->master) {
        ?>
$('td:eq(6)', nRow).html('<a class="btn btn-sm btn-success" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><i class="fa fa-search"></i> Details</a><?php if($this->session->email == "simson.parlindungan@united-telecom.be"){ ?><button class="btn btn-sm btn-danger" id="'+aData[6]+'"><i class="fa fa-trash"></i></button><?php } ?>');
<?php
    } else {
        ?>
$('td:eq(6)', nRow).html('<a class="btn btn-sm btn-success" href="'+window.location.protocol + '//' + window.location.host + '/admin/client/detail/' + aData[6] + '"><i class="fa fa-search"></i> Details</a>');
<?php
    } ?>
return nRow;
},
});
});
});
//$("#clients").on('click', '.btn-danger', function () {functio
  <?php if ($this->session->master) {
        ?>
    $(document).delegate('.btn-danger', 'click', function() {

          var t = confirm('<?php echo lang('Are you sure?'); ?>');
          if(t){
            var id = this.id; //getting the ID of the pressed button
            var row = $(this).closest("tr").get(0);
            /****AJAX Function*********/
            $.post('/admin/client/delete', {id:id}, function(data) {
              console.log(data);
             var obj = JSON.parse(data);
                if(obj.result == "1"){ //In case if data is deleted
                    var oTable = $('#clients').dataTable(); // JQuery dataTable function to delete the row from the table
                    oTable.fnDeleteRow(oTable.fnGetPosition(row));// JQuery dataTable function to delete the row from the table
                  //  oTable.fnDraw(true);
               }
                else
                    alert(obj.message);
            });
          }
        });
  <?php
    } ?>
//    });
</script>
<div class="modal fade" id="portoutpending">
  <div class="modal-dialog">
    <div class="modal-content">

        <input type="hidden" name="serviceid" id="serviceid">
        <input type="hidden" name="action" value="reject">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Portout request need to be process'); ?>
          </h4>

        </div>

        <!-- Modal body -->
        <div class="modal-body" id="portoutx">

        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <a href="<?php echo base_url(); ?>admin/subscription/pending_porting_out" class="btn btn-primary">
            <?php echo lang('Process Portout request Now'); ?></a>

        </div>

    </div>
  </div>
</div>


<div class="modal fade" id="importclient">
  <div class="modal-dialog">
    <div class="modal-content">

        <form method="post" action="<?php echo base_url(); ?>admin/client/import_client_from_magebo">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Portout request need to be process'); ?>
          </h4>

        </div>

        <!-- Modal body -->
        <div class="modal-body" id="portoutx">

            <input type="number" name="mageboid" class="form-control">

        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><?php echo lang('Import Now'); ?></button>

        </div>
      </form>

    </div>
  </div>
</div>
<script>
  function importing(){
    $('#importclient').modal('toggle');
  }
  </script>