<section class="tables">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <form method="post" action="<?php echo base_url(); ?>admin/warehouse/inventory_add/" enctype="multipart/form-data">
              <?php if (!empty($this->uri->segment(4))) {?>
              <input type="hidden" name="userid" value="<?php echo $this->uri->segment(3); ?>" id="userid" required>
              <?php } else {?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="firstname">Client Name</label>
                      <input  name="userid" class="form-control ui-autocomplete-input ui-autocomplete-loading clientinventory" autocomplete="off"  id="userid" type="text" placeholder="Search client Name" required>
                    </fieldset>
                  </div>
                </div>
              </div>
              <?php }?>
              <div class="form-group">
                <label for="exampleSelect2">Is This credential related?</label>
                <select class="form-control" id="cred">
                  <option value="yes">Yes</option>
                  <option value="no">NO</option>
                </select>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="firstname">Inventory Name</label>
                      <input  name="name" class="form-control" id="name" type="text" placeholder="Manufacture Name"  required>
                    </fieldset>
                  </div>
                </div>
              </div>
              <div id="cre">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <fieldset>
                        <label class="control-label" for="firstname">IP/URL/Host</label>
                        <input  name="ip_host" class="form-control" id="ip_host" type="text" placeholder="Manufacture Name"  required>
                      </fieldset>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <fieldset>
                        <label class="control-label" for="firstname">Username</label>
                        <input  name="username" class="form-control" id="username" type="text" placeholder="Manufacture Name"  required>
                      </fieldset>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <fieldset>
                        <label class="control-label" for="firstname">Password</label>
                        <input  name="password" class="form-control" id="password" type="text" placeholder="Manufacture Name"  required>
                      </fieldset>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="exampleTextarea">Description</label>
                    <textarea class="form-control" placeholder="Enter description of the product will appear on the invoice and quotes" rows="5" name="description"></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-3">
                  <div class="form-group">
                    <fieldset>
                      <label class="control-label" for="picture">Pictures</label>
                      <input name="picture"  class="form-control" type="file">
                    </fieldset>
                  </div>
                </div>
                <div class="col-sm-3">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> Save Manufacture</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
$('#cred').on('change', function (e) {
var optionSelected = $("option:selected", this);
var valueSelected = this.value;
if(valueSelected == "no"){
$('#cre').html('');
}else{
$('#cre').html('<div class="row"> <div class="col-sm-12"> <div class="form-group"> <fieldset> <label class="control-label" for="firstname">IP/URL/Host</label> <input name="ip_host" class="form-control" id="ip_host" type="text" placeholder="Manufacture Name" required> </fieldset> </div> </div> </div> <div class="row"> <div class="col-sm-12"> <div class="form-group"> <fieldset> <label class="control-label" for="firstname">Inventory Name</label> <input name="username" class="form-control" id="username" type="text" placeholder="Manufacture Name" required> </fieldset> </div> </div> </div> <div class="row"> <div class="col-sm-12"> <div class="form-group"> <fieldset> <label class="control-label" for="firstname">Inventory Name</label> <input name="password" class="form-control" id="password" type="text" placeholder="Manufacture Name" required> </fieldset> </div> </div> </div');
}
});
</script>