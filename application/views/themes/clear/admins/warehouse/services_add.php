<section class="tables">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<form method="post" action="<?php echo base_url(); ?>admin/warehouse/services_add/" enctype="multipart/form-data">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Service Name</label>
											<input  name="name" class="form-control" id="name" type="text" placeholder="Service Name"  required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Pricing Monthly (1)</label>
											<input  name="monthly" class="form-control" id="monthly" type="number" step="any" value="0.00"  required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Pricing Quaterly (3)</label>
											<input  name="quarterly" class="form-control" id="quarterly" type="number" step="any" value="0.00"  required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Pricing Semi-Annually(6)</label>
											<input  name="semiannually" class="form-control" id="semiannually" type="number" step="any" value="0.00"  required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Pricing Annually (12)</label>
											<input  name="annually" class="form-control" id="annually" type="number" step="any" value="0.00"  required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Pricing Bienially(24)</label>
											<input  name="biennially" class="form-control" id="biennially" type="number" step="any" value="0.00"  required>
										</fieldset>
									</div>
								</div>

								<div class="col-sm-2">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Pricing Trienially(36)</label>
											<input  name="triennially" class="form-control" id="triennially" type="number" step="any" value="0.00"  required>
										</fieldset>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> Save Services</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
</script>