<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
     Warehouse<div class="close">
           <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/warehouse/product_add"><i class="fa fa-plus-circle"></i> Add Product</a>
          </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
       <?php echo lang('Product List'); ?>
        </h5>
        <div class="table-responsive">
           <table class="table table-striped table-lightfont" id="clients">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Supplierid</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function confirm_delete(id) {
var answer = confirm("<?php echo lang('Do you wish to delete this product?'); ?>?")
if (answer){
window.location.replace('<?php echo base_url(); ?>admin/warehouse/product_delete/'+id);
}
else{
console.log("hello ");
$("#sendinvoice").prop('disabled', false);
}
}
function openModal() {
document.getElementById('modal').style.display = 'block';
document.getElementById('fade').style.display = 'block';
}
function closeModal() {
document.getElementById('modal').style.display = 'none';
document.getElementById('fade').style.display = 'none';
}
</script>