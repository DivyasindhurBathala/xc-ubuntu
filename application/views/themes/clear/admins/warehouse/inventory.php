<section class="tables">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-close">
            <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/warehouse/inventory_add"><i class="fa fa-plus-circle"></i> Add Data to Client</a>
          </div>
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Clients Inventory</h3>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-striped table-lightfont" id="clients">
                <thead>
                  <tr>
                    <th>CustomerName</th>
                    <th>Name</th>
                    <th>Tools</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>