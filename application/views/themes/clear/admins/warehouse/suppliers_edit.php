<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
     Warehouse
      </h6>
      <div class="element-box">
        <h5 class="form-header">
       <?php echo lang('Supplier Edit'); ?>
        </h5>
        <div class="table-responsive">
        		<form method="post" action="<?php echo base_url(); ?>admin/warehouse/supplier_edit/" enctype="multipart/form-data">
							<input type="hidden" name="id" value="<?php echo $supplier->id; ?>">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Supplier Name</label>
											<input  name="name" class="form-control" id="name" type="text" placeholder="Supplier Name" value="<?php echo $supplier->name; ?>" required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Address</label>
											<input name="address1"  class="form-control" type="text" value="<?php echo $supplier->address1; ?>">
										</fieldset>
									</div>
								</div>
									<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Postcode</label>
											<input name="postcode"  class="form-control" type="text" value="<?php echo $supplier->postcode; ?>">
										</fieldset>
									</div>
								</div>
									<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">City</label>
											<input name="city"  class="form-control" type="text" value="<?php echo $supplier->city; ?>">
										</fieldset>
									</div>
								</div>

							</div>

							<div class='row'>
									<div class="col-sm-6">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Phonenumber</label>
											<input name="phonenumber"  class="form-control" type="number" value="<?php echo $supplier->phonenumber; ?>">
										</fieldset>
									</div>
								</div>
									<div class="col-sm-6">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Email</label>
											<input name="email"  class="form-control" type="email" value="<?php echo $supplier->email; ?>">
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Logo</label>
											<input name="picture"  class="form-control" type="file">
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<?php if (file_exists(FCPATH . 'assets/img/suppliers/' . $supplier->logo)) {?>
									<div class="avatar"><a href="<?php echo base_url(); ?>assets/img/suppliers/<?php echo $supplier->logo; ?>" data-toggle="lightbox"><img src="<?php echo base_url(); ?>assets/img/suppliers/<?php echo $supplier->logo; ?>" height="70"></a></div>
									<?php }?>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> Save Supplier</button>
									</div>
								</div>
							</div>
						</form>
        </div>
    </div>
</div>
</div>
</div>

<script>
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
</script>