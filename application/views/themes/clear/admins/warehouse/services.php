<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
     Warehouse <div class="close">
            <a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>admin/warehouse/services_add"><i class="fa fa-plus-circle"></i> Add New Service</a>
          </div>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
       <?php echo lang('Services List'); ?>
        </h5>
        <div class="table-responsive">
           <table class="table table-striped table-lightfont" id="services">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Monthly</th>
                    <th>Quarterly</th>
                     <th>Semi-Annually</th>
                    <th>Annually</th>
                     <th>Bienially</th>
                    <th>Trienially</th>
                     <th></th>

                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function confirm_delete(id) {
var answer = confirm("<?php echo lang('Do you wish to delete this services?'); ?>?")
if (answer){
console.log(id);
window.location.replace('<?php echo base_url(); ?>admin/warehouse/services_delete/'+id);
}
}
</script>