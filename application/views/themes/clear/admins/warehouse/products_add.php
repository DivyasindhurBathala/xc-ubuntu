<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
     Warehouse
      </h6>
      <div class="element-box">
        <h5 class="form-header">
       <?php echo lang('Product Add'); ?>
        </h5>
        <div class="table-responsive">
        	<form method="post" action="<?php echo base_url(); ?>admin/warehouse/product_add/" enctype="multipart/form-data">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname">Product Name</label>
											<input  name="name" class="form-control" id="name" type="text" placeholder="Product Name"  required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="exampleTextarea">Product Description</label>
										<textarea class="form-control" placeholder="Enter description of the product will appear on the invoice and quotes" rows="5" name="description"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<fieldset>
											<label for="exampleSelect1">Supplier</label>
											<select class="form-control" id="supplierid"  name="supplierid" >
												<?php foreach (getSuppliers() as $supplier) {?>
												<option value="<?php echo $supplier->id; ?>"><?php echo $supplier->name; ?></option>
												<?php }?>
											</select>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<fieldset>
											<label for="exampleSelect1">Manufacture</label>
											<select class="form-control" id="manufactureid"  name="manufactureid" >
												<?php foreach (getManufacture() as $man) {?>
												<option value="<?php echo $man->id; ?>"><?php echo $man->name; ?></option>
												<?php }?>
											</select>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="stock">Stock</label>
											<input  name="stock" class="form-control" id="firstname" type="number" value="1" required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="picture">Picture</label>
											<input name="picture"  class="form-control" type="file">
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> Save Product</button>
									</div>
								</div>
							</div>
						</form>
        </div>
    </div>
</div>
</div>
</div>

<script>
	$(document).on('click', '[data-toggle="lightbox"]', function(event) {
event.preventDefault();
$(this).ekkoLightbox();
});
</script>