<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
      Setting
      </h6>
      <div class="element-box">
        <h5 class="form-header">
       <?php echo lang('Edit Staf'); ?>
        </h5>
        <div class="table-responsive">
        	<form method="post" action="<?php echo base_url(); ?>admin/setting/staf_edit/<?php echo $staf->id; ?>" enctype="multipart/form-data">
							<input type="hidden" name="id" value="<?php echo $staf->id; ?>">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="firstname"><?php echo lang('Firstname'); ?></label>
											<input  name="firstname" class="form-control" id="firstname" type="text" placeholder="Firstname"  value="<?php echo $staf->firstname; ?>" required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="lastname"><?php echo lang('Lastname'); ?></label>
											<input  name="lastname" class="form-control" id="lastname" type="text" placeholder="Lastname"  value="<?php echo $staf->lastname; ?>" required>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="address1"><?php echo lang('Address'); ?></label>
											<input  name="address1" class="form-control" id="address1" type="text" placeholder="Address"  value="<?php echo $staf->address1; ?>" required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-2">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="Postcode"><?php echo lang('Postcode'); ?></label>
											<input  name="postcode" class="form-control" id="postcode" type="text" placeholder="Postcode"  value="<?php echo $staf->postcode; ?>" required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="City"><?php echo lang('City'); ?></label>
											<input  name="city" class="form-control" id="city" type="text" placeholder="City"  value="<?php echo $staf->city; ?>" required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label for="exampleSelect1"><?php echo lang('Country'); ?></label>
											<select class="form-control" id="country"  name="country" >
												<?php foreach (getCountries() as $key => $country) {?>
												<option value="<?php echo $key; ?>"<?php if ($key == "BE") {?> selected<?php }?>><?php echo $country; ?></option>
												<?php }?>
											</select>
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label for="exampleSelect1"><?php echo lang('Languages'); ?></label>
											<select class="form-control" id="language" name="language" >
												<?php foreach (getLanguages() as $key => $lang) {?>
												<option value="<?php echo $key; ?>"<?php if ($key == "dutch") {?> selected<?php }?>><?php echo $lang; ?></option>
												<?php }?>
											</select>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="Phonenumber"><?php echo lang('Phonenumber'); ?></label>
											<input name="phonenumber" class="form-control" id="phonenumber" type="number" placeholder="Phonenumber"  value="<?php echo $staf->phonenumber; ?>" required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="Email"><?php echo lang('Email'); ?></label>
											<input name="email"  class="form-control" id="email" type="email" placeholder="Email address"  value="<?php echo $staf->email; ?>" required>
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="NationalNr"><?php echo lang('NationalNr'); ?>.</label>
											<input name="nationalnr"  class="form-control" id="NationalNr" type="number" placeholder="NationalNr" value="<?php echo $staf->nationalnr; ?>">
										</fieldset>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group">
										<fieldset>
											<label class="control-label" for="NationalNr">Picture</label>
											<input name="picture"  class="form-control" type="file">
										</fieldset>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="avatar"><img src="<?php echo base_url(); ?>assets/img/staf/<?php echo $staf->picture; ?>" height="70"></div>
								</div>
							</div>
							<?php if ($staf->role == "superadmin") {?>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<fieldset class="form-group">
											<label for="exampleSelect1">Role/Function</label>
											<select class="form-control" id="role" name="role" >
												<option value="superadmin">Administrator</option>
												<option value="sales">Sales</option>
												<option value="administratie">Administratie medewerker</option>
												<option value="finance">Finance</option>
											</select>
										</fieldset>
									</div>
								</div>
							</div>
							<?php }?>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<button type="submit" class="btn btn-md btn-primary btn-block"><i class="fa fa-save"></i> Save Administrator</button>
									</div>
								</div>
							</div>
						</form>
        </div>
    </div>
</div>
</div>
</div>