

<section class="tables">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6">
        <div class="card">
          <div class="card-header d-flex align-items-center">
            <h3 class="h4">Create/Modify Invoice Customer : <?php echo $client->firstname . ' ' . $client->lastname . ' - ' . $client->companyname; ?></h3>
          </div>
          <div class="card-body">
<form method="post" action="<?php echo base_url(); ?>admin/warehouse/setupinvoice/<?php echo $this->uri->segment(4); ?>">

  <fieldset>
    <legend>Legend</legend>

    <div class="form-group">
      <label for="exampleInputEmail1">Invoice Total</label>
        <select class="form-control" id="total" name="total">
       <?php foreach ($pdf as $p) {?>
 <option value="<?php echo $p; ?>"><?php echo $p; ?></option>
       <?php }?>


      </select>
    </div>

    <div class="form-group">
      <label for="invoicenum">Invoicenum</label>
      <select class="form-control" id="invoicenum" name="invoicenumber">
       <?php foreach ($pdf as $p) {?>
 <option value="<?php echo $p; ?>"><?php echo $p; ?></option>
       <?php }?>


      </select>
    </div>

    <div class="form-group">
      <label for="subtotal">Subtotal</label>
      <select class="form-control" id="subtotal" name="subtotal">
       <?php foreach ($pdf as $p) {?>
 <option value="<?php echo $p; ?>"><?php echo $p; ?></option>
       <?php }?>


      </select>
    </div>


    <div class="form-group">
      <label for="subtotal">Tax</label>
      <select class="form-control" id="tax" name="tax">
       <?php foreach ($pdf as $p) {?>
 <option value="<?php echo $p; ?>"><?php echo $p; ?></option>
       <?php }?>


      </select>
    </div>

      <div class="form-group">
      <label for="subtotal">Date</label>
      <select class="form-control" id="date" name="date">
       <?php foreach ($pdf as $p) {?>
 <option value="<?php echo $p; ?>"><?php echo $p; ?></option>
       <?php }?>


      </select>
    </div>


      <div class="form-group">
      <label for="subtotal">Duedate</label>
      <select class="form-control" id="duedate" name="duedate">
       <?php foreach ($pdf as $p) {?>
 <option value="<?php echo $p; ?>"><?php echo $p; ?></option>
       <?php }?>


      </select>
    </div>


    <div class="form-group">
      <label for="exampleTextarea">Example textarea</label>
      <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
    </div>



    </fieldset>
    <button type="submit" class="btn btn-primary">Submit</button>
  </fieldset>
</form>
</div>
</div>
</div>
<div class="col-lg-6">
 <object data="<?php echo base_url(); ?>assets/purchases/<?php echo $invoice->filename; ?>" type="application/pdf" width="100%" height="100%">
   <p><b>Example fallback content</b>: This browser does not support PDFs. Please download the PDF to view it: <a href="/pdf/sample-3pp.pdf">Download PDF</a>.</p>
</object>

</div>

</div>
</div>
</section>
<script>
	$('#invoicenum').change(function() {
		var newval = $(this).val();
		var newval = newval.replace(/\s/g, '')
	$('#invoicenum').replaceWith('<input class="form-control" type="text" name="invoicenumber" value="'+newval+'">');
	});

	$('#total').change(function() {
		var newval = $(this).val();
		var newval = newval.replace(/\s/g, '')
	$('#total').replaceWith('<input class="form-control" type="text" name="total" value="'+newval+'">');
	});


	$('#subtotal').change(function() {
		var newval = $(this).val();
		var newval = newval.replace(/\s/g, '')
	$('#subtotal').replaceWith('<input class="form-control" type="text" name="subtotal" value="'+newval+'">');
	});


	$('#tax').change(function() {
		var newval = $(this).val();
		var newval = newval.replace(/\s/g, '')
	$('#tax').replaceWith('<input class="form-control" type="text" name="tax" value="'+newval+'">');
	});
	</script>