<section class="tables">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header d-flex align-items-center">
						<h3 class="h4"><?php echo lang('Add Billableitems'); ?></h3>
					</div>
					<div class="card-body">
						<form method="post"  action="<?php echo base_url(); ?>admin/subscription/addfuturebill">
							<fieldset>
								<legend>Add new Billable items</legend>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label for="exampleInputEmail1">Customer</label>
											<input name="userid" type="text" class="form-control ui-autocomplete-input ui-autocomplete-loading clientquote" id="userid" aria-describedby="search" placeholder="Serach..." <?php if (!empty($this->uri->segment(4))) {?> value="<?php echo $this->uri->segment(4); ?>" <?php }?>required>
											<small id="emailHelp" class="form-text text-muted">Please search customer by firstname, lastname, companyname or id</small>
										</div>
										<div class="col-sm-6">
											<label for="exampleInputEmail1">Customer Name</label>
											<input type="text" class="form-control" id="customername" aria-describedby="search">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label for="exampleSelect1">Enter Description</label>
											<input name="description" type="text" class="form-control" id="customername" aria-describedby="search" required>
										</div>
										<div class="col-sm-3">
											<label for="exampleInputEmail1">Override Pricing</label>
											<input name="recurring" type="number" step="any" class="form-control" id="recurring" aria-describedby="search" placeholder="10.2" required>
											<small id="emailHelp" class="form-text text-muted">This will be the pricing recurred to customer</small>
										</div>
										<div class="col-sm-3">
											<label for="exampleInputEmail1">Taxrate</label>
											<select class="form-control" id="exampleSelect1" name="taxrate">
												<option value="0">0%</option>
												<option value="6">6%</option>
												<option value="21">21%</option>
											</select>
										</div>
									</div>
								</div>
								<legend>Invoice Type</legend>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<div class="form-check">
												<label class="form-check-label">
													<input type="radio" class="form-check-input" name="invoicetype" id="no" value="onetime" checked="">
													Invoice On the duedate
												</label>
											</div>
											<div class="form-check">
												<label class="form-check-label">
													<input type="radio" class="form-check-input" name="invoicetype" id="new" value="recurring">
													Invoice Recurring every
												</label>
											</div>
										</div>
										<div id="shownew"  style="display:none;" class="col-md-6">
											<div class="row">
												<div class="col-md-6">
													<label for="exampleSelect1">Choose Billing Cycle</label>
													<select class="form-control" id="exampleSelect1" name="cycle">
														<option value="monthly">Monthly</option>
														<option value="quarterly">Quarterly</option>
														<option value="semiannually">Semi-Annually</option>
														<option value="annually">Annually</option>
														<option value="biennially">Bienually</option>
													</select>
												</div>
												<div class="col-md-6">
													<label for="exampleSelect1">Date to Invoice</label>
													<input name="invoicedaterecurring" type="text" id="pickdate3" class="form-control">
												</div>
											</div>
										</div>
										<div class="col-md-6" id="invoicerecurring">
											<label for="exampleSelect1">Date to Invoice</label>
											<input name="invoicedate" type="text" id="pickdate2" class="form-control" required>
										</div>
									</div>
								</div>
							</fieldset>
							<button type="submit" class="btn btn-block btn-primary">Submit</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<script>
	$(document).ready(function() {
$('input[type=radio][name=invoicetype]').change(function() {
if (this.value == 'onetime') {
	$("#pickdate2").prop('required', true);
	$("#pickdate3").prop('required', false);
	$("#invoicerecurring").show();
	$("#shownew").hide();
	$("#pickdate3").val('0000-00-00');
}else if (this.value == 'recurring') {
	$("#pickdate3").prop('required', true);
	$("#pickdate2").prop('required', false);
$("#invoicerecurring").hide();
$("#pickdate2").val('0000-00-00');
$("#shownew").show();
}
});
});
</script>
<script>
$(document).ready(function() {
$( "#checkdomain" ).click(function() {
	var domain =$('#domain').val();
$.ajax({
url: window.location.protocol + '//' + window.location.host + '/whois/check',
type: 'post',
dataType: 'json',
data: {
domain:domain
},
success: function (data) {
console.log(data);
if(data.result){
$("#showok").show();
}else{
$("#shownok").show();
}
},
error: function(XMLHttpRequest, textStatus, errorThrown) {
//alert("Error  accour while sending your email");
console.log(errorThrown);
}
});
});
});
</script>