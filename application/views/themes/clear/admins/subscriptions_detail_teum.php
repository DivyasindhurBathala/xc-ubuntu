<?php if (in_array($service->status, array( "Terminated","Cancelled"))) {
    ?>
<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Services Summary'); ?> [
        <a
          href="<?php echo base_url(); ?>admin/client/detail/<?php echo $service->userid; ?>">
          <?php echo lang('Back to Client Details'); ?>
        </a>]
      </h6>
      <div class="element-box">
        <h5 class="form-header">
        </h5>
        <div class="table-responsive">
          <div class="row">
            <div class="col-lg-4">
              <div class="card bg-default mb-3">
                <div class="card-header bg-primary text-white"
                  style="pading: 5px; padding-bottom:5px; padding-left:5px;">
                  <h4 class="card-title text-light">
                    <i class="fa fa-cubes">
                    </i>
                    <?php echo $service->packagename; ?>
                  </h4>
                </div>
                <div class="card-body">
                  <table class="table table-striped table-hover" width="100%">
                    <tr>
                      <td>
                        <?php echo lang('Product Brand'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $service->product_brand; ?>
                        </strong>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Agent'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $service->agent; ?>
                        </strong>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Service ID'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $service->id; ?>
                        </strong>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Date Reg'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $service->date_created; ?>
                        </strong>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Product Name'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $service->packagename; ?>
                        </strong>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('Status'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo lang($service_info->{'card-network-status'}); ?>
                        </strong>
                      </td>
                    </tr>
                    <?php if ($service->promocode) {
        ?>
                    <tr>
                      <td>
                        <?php echo lang('Promocode'); ?>T
                        <a href="#" onclick="removePromotion();">
                          <i class="fa fa-trash text-danger">
                          </i>
                        </a>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo getPromoname($service->promocode); ?>
                        </strong>
                      </td>
                    </tr>
                    <?php
    } ?>
                    <tr>
                      <td>
                        <?php echo lang('Contract date'); ?>
                        <small>DD-MM-YYYY
                        </small>
                      </td>
                      <td align="right">
                        <strong>
                          <?php if ($setting->mage_invoicing) {
        ?>
                          <?php echo convert_contract($service->date_contract); ?>
                          ( <?php echo $service->contract_terms; ?>)
                        </strong>
                      </td>
                      <?php
    } ?>
                    </tr>
                    <tr>
                      <td>
                        <?php echo lang('BillingID'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $client->mageboid; ?>
                        </strong>
                      </td>
                    </tr>
                    <?php if ($service->details->msisdn_type == "porting" && $service->details->date_wish != "0000-00-00") {
        ?>
                    <tr>
                      <td>
                        <?php echo lang('Porting Date Wish'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $service->details->date_wish; ?>
                        </strong>
                      </td>
                    </tr>
                    <?php
    } ?>
                    <?php if ($setting->mage_invoicing) {
        ?>
                    <tr>
                      <td>
                        <?php echo lang('iGeneralPricing'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $service->iGeneralPricingIndex; ?>
                        </strong>
                      </td>
                    </tr>
                    <?php
    } ?>
                    <tr>
                      <td>
                        <?php echo lang('Last Changes'); ?>
                      </td>
                      <td align="right"><strong>
                          <?php echo $service->details->date_modified; ?></strong></td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="row">
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-body">

                      <table class="table table-striper table-bordered">
                        <thead>
                          <tr class="bg-success  text-white">
                            <th>
                              <?php echo lang('Client ID'); ?>
                            </th>
                            <th>
                              <?php echo lang('Contact'); ?>
                            </th>
                            <th>
                              <?php echo lang('Address'); ?>
                            </th>
                            <th>
                              <?php echo lang('Email'); ?>
                            </th>
                            <th>
                              <?php echo lang('Contact'); ?>
                            </th>
                          </tr>
                        </thead>
                        <tr>
                          <td>
                            <a
                              href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>">
                              <?php echo trim($client->id); ?>
                            </a>
                          </td>
                          <td>
                            <a
                              href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>">
                              <?php echo $client->firstname . ' ' . $client->lastname; ?>
                            </a>
                          </td>
                          <td>
                            <?php echo $client->address1 . ' ' . $client->postcode . ' ' . $client->city; ?>
                          </td>
                          <td>
                            <?php echo $client->email; ?>
                          </td>
                          <td>
                            <?php echo $client->phonenumber; ?>
                          </td>
                        </tr>
                      </table>
                      <hr />

                      <h1 class="text-center text-danger">MSISDN:
                        <?php echo $service->details->msisdn; ?>
                        <br />
                        SN:
                        <?php echo $service->details->msisdn_sn; ?>
                        <br />
                      </h1>
                      <h1 class="text-center text-danger">Date End:
                        <?php echo $service->date_terminate; ?>
                      </h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
} else {
        ?>
  <?php //print_r($service);?>
  <div class="content-i">
    <div class="content-box">
      <div class="element-wrapper">
        <h6 class="element-header">
          <?php echo lang('Services Summary'); ?>
          [
          <a
            href="<?php echo base_url(); ?>admin/client/detail/<?php echo $service->userid; ?>">
            <?php echo lang('Back to Client Details'); ?>
          </a>]
          <div class="float-right">
            <!--
            <button class="btn btn-sm btn-primary" onclick="MoveServicesNow()">Move Subscription to another Customer
            </button>
            -->
          </div>
        </h6>
        <div class="element-box">
          <h5 class="form-header">
          </h5>
          <div class="table-responsive">
            <?php if ($service->orderstatus != "Active"  && $service->details->msisdn_type == "porting") {
            ?>
            <div class="row">
              <div class="col-sm-12">
                <div class="alert alert-warning" role="alert">
                  <h6 class="text-danger">
                    <i><?php echo lang('Attention: If you recevied notification that status of portability'); ?> <?php echo $service->details->donor_msisdn; ?> <?php echo lang('has been completed please'); ?> <button type="button"  id="portindone"><?php echo lang('Finalize Portin'); ?></button>
                    </i>
                  </h6>
                </div>
              </div>
            </div>
            <?php
        } ?>
            <?php if ($service->status == "Suspended") {
            ?>
            <div class="row">
              <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                  <strong>
                    <?php echo lang('This service is currently suspended, it does not mean billing would stop'); ?>
                  </strong>
                </div>
              </div>
            </div>
            <?php
        } ?>
            <div class="row">
              <div class="col-lg-4">
                <div class="card bg-default mb-3">
                  <div class="card-header bg-primary text-white"
                    style="pading: 5px; padding-bottom:5px; padding-left:5px;">
                    <h4 class="card-title text-light">
                      <i class="fa fa-cubes">
                      </i>
                      <?php echo $service->packagename; ?>
                    </h4>
                  </div>
                  <div class="card-body">
                    <table class="table table-striped">
                      <tr>
                        <td>
                          <?php echo lang('Product Brand'); ?>
                        </td>
                        <td align="right">
                          <strong>
                            <?php echo $service->product_brand; ?>
                          </strong>
                        </td>
                      </tr>
                      <tr>
                      <td>
                        <?php echo lang('Agent'); ?>
                      </td>
                      <td align="right">
                        <strong>
                          <?php echo $service->agent; ?>
                        </strong>
                      </td>
                    </tr>
                      <tr>
                        <td>
                          <?php echo lang('Service ID'); ?>
                        </td>
                        <td align="right">
                          <strong>
                            <?php echo $service->id; ?>
                          </strong>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('Product Name'); ?>
                        </td>
                        <td align="right">
                          <strong>
                            <?php echo $service->packagename; ?>
                          </strong>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('TEUM Accountid'); ?>
                        </td>
                        <td align="right">
                          <strong>
                            <?php echo $service->details->teum_accountid; ?>
                          </strong>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('Status'); ?>
                        </td>
                        <td align="right">
                          <strong>
                            <?php echo lang($service_info->{'card-network-status'}); ?>
                          </strong>
                        </td>
                      </tr>
                      <?php if ($service->promocode) {
            ?>
                      <tr>
                        <td>
                          <?php echo lang('Promocode'); ?>
                        </td>
                        <td align="right">
                          <strong>
                            <?php echo getPromoname($service->promocode); ?>
                          </strong>
                        </td>
                      </tr>
                      <?php
        } ?>
                    </table>
                    <table class="table table-striped">
                      <?php if ($setting->mvno_type == "prepaid") {
            ?>
                      <tr>
                        <td>
                          <?php echo lang('Credit Balance'); ?>
                        </td>
                        <td align="right">
                          <span class="float-left text-success">
                            <?php echo number_format($credit, 2); ?>
                          </span>
                        </td>
                      </tr>
                      <?php
        } ?>
                      <tr>
                        <td>
                          <?php echo lang('Msisdn'); ?>
                        </td>
                        <td align="right">
                          <?php echo $mobile->msisdn; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('SIMCARD NO'); ?>.
                        </td>
                        <td align="right">
                          <?php echo $mobile->msisdn_sim; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('PIN'); ?>
                        </td>
                        <td align="right">
                          <?php echo $setting->default_pin; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('PUK1'); ?>
                        </td>
                        <td align="right">
                          <?php echo $mobile->msisdn_puk1; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('PUK2'); ?>
                        </td>
                        <td align="right">
                          <?php echo $mobile->msisdn_puk2; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('SIMCARD Type'); ?>.
                        </td>
                        <td align="right">
                          <?php echo 'TRIO'; ?>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <?php echo lang('Last Updated'); ?>.
                        </td>
                        <td align="right">
                          <?php echo $service->details->date_modified; ?>
                        </td>
                      </tr>


                      <?php if ($mobile->msisdn_type == "porting") {
            ?>
                      <tr>
                        <td>
                          <?php echo lang('TYPE Porting'); ?>
                        </td>
                        <td align="right">
                          <?php echo lang($mobile->donor_type); ?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('DonorMSISDN'); ?>
                        </td>
                        <td align="right">
                          <?php echo $mobile->donor_msisdn; ?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <?php echo lang('DonorSimCardNbr'); ?>
                        </td>
                        <td>
                          <?php echo $mobile->donor_sim; ?>
                        </td>
                      </tr>
                      <?php
        } ?>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card">
                      <div class="card-body">
                        <table class="table table-striper table-bordered">
                          <thead>
                            <tr class="bg-success  text-white">
                                                               <th><?php echo lang('Client ID'); ?></th>
                                                               <th><?php echo lang('Contact'); ?></th>
                                                               <th><?php echo lang('Address'); ?></th>
                                                               <th><?php echo lang('Email'); ?></th>
                                                               <th><?php echo lang('Phonenumber'); ?></th>
                                                               </tr>
                                                               </thead>
                                                               <tr>
                                                               <td><a href=" <?php echo base_url(); ?>admin/client/detail/
                              <?php echo $client->id; ?>">
                              <?php echo $client->id; ?>
                              </a>
                              </td>
                              <td>
                                <a
                                  href="<?php echo base_url(); ?>admin/client/detail/<?php echo $client->id; ?>">
                                  <?php echo $client->firstname . ' ' . $client->lastname; ?>
                                </a>
                              </td>
                              <td>
                                <?php echo $client->address1 . ' ' . $client->postcode . ' ' . $client->city; ?>
                              </td>
                              <td>
                                <?php echo $client->email; ?>
                              </td>
                              <td>
                                <?php echo $client->phonenumber; ?>
                              </td>
                            </tr>
                        </table>
                        <hr />
                        <?php //print_r($bundles);?>
                        <?php if ($bundles) {
            ?>
                        <table class="table table-striper table-bordered">
                          <thead>
                            <tr class="bg-primary  text-white">
                              <th>
                                <?php echo lang('BundleID'); ?> <?php echo lang('Addonid'); ?>
                              </th>
                              <th>
                                <?php echo lang('Name'); ?>
                              </th>
                              <th>
                                <?php echo lang('Cycle'); ?>
                              </th>
                              <th>
                                <?php echo lang('Renew Left'); ?> <?php echo lang('month'); ?>
                              </th>
                              <th class="text-right">
                                <?php echo lang('Next Renewal'); ?>
                              </th>
                              <th class="text-right">
                                <?php echo lang('Action'); ?>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($bundles as $bundle) {
                ?>
                            <tr>
                              <td>
                                <?php echo $bundle->id; ?>
                              </td>
                              <td>
                                <?php echo $bundle->name; ?>
                              </td>
                              <td>

                                <?php echo $bundle->terms.' '.$bundle->cycle; ?>


                              </td>
                              <td class="text-center">

                                <?php if ($bundle->teum_autoRenew >= 500) {
                    ?>
                                <?php echo lang('Indefinite'); ?>
                              <?php
                } else {
                    ?>
                                <?php echo $bundle->teum_autoRenew; ?>
                            <?php
                } ?>
                              </td>
                              <td class="text-right">
                                <?php echo convert_date_to_uk_from_dash($bundle->teum_NextRenewal); ?>
                                00:00:00
                              </td>
                              <td class='text-right'>
                                <?php if ($bundle->teum_NextRenewal < date('Y-m-d')) {
                    ?>
                                <button type="button"
                                  onclick="orderthisbundle(<?php echo $bundle->addonid; ?>, '<?php echo $bundle->name; ?>')"
                                  class="btn btn-sm btn-warning"><?php echo lang('ReOrder this Bundle'); ?></button>
                                <?php
                } else {
                    ?>
                                <?php if ($bundle->teum_autoRenew > 0 && $bundle->base_bundle != 1) {
                        ?>
                                <button type="button"
                                  onclick="enable_disable_autorenew(<?php echo $bundle->id; ?>, '<?php echo $bundle->name; ?>', 0)"
                                  class="btn btn-sm btn-warning"><?php echo lang('Disable auto Renew'); ?></button>
                                <?php
                    } else {
                        ?>
                                <button type="button"
                                  onclick="enable_disable_autorenew(<?php echo $bundle->id; ?>, '<?php echo $bundle->name; ?>', 1)"
                                  class="btn btn-sm btn-primary"><?php echo lang('Enable auto Renew'); ?></button>
                                <?php
                    } ?>
                                <?php
                } ?>


                              </td>
                            </tr>

                            <?php
            } ?>
                          </tbody>
                        </table>

                        <hr />
                        <?php
        } ?>
                        <table class="table" id="bundlesx_table">
                          <thead>
                            <tr class="bg-primary  text-white">
                              <th width="30%">
                                <?php echo lang('Allowance'); ?>
                              </th>
                              <th width="20%">
                                <?php echo lang('Value'); ?>
                              </th>
                              <th width="25%">
                                <?php echo lang('Unit'); ?>
                              </th>
                              <th width="25%" class="text-right">
                                <?php echo lang('Expiration date'); ?>
                              </th>
                            </tr>
                          </thead>
                          <tbody id="tbody_bundlesx">
                            <?php if ($service_info->resultCode == "0") {
            ?>
                            <?php if (is_array($service_info->resources)) {
                ?>
                            <?php foreach ($service_info->resources as $row) {
                    ?>
                            <tr>
                              <td>
                                <?php echo $row->resourceName; ?>
                              </td>
                              <?php if ($row->resourceUnit == "SECOND") {
                        ?>
                              <td>
                                <?php echo s2m($row->resourceValue); ?>
                              </td>
                              <?php
                    } elseif ($row->resourceUnit == "CENT") {
                        ?>
                              <td>
                                <?php echo number_format(($row->resourceValue/100), 2); ?>
                              </td>
                              <?php
                    } elseif ($row->resourceUnit == "KB") {
                        ?>
                              <td>
                                <?php echo convertToReadableSize($row->resourceValue*1024); ?>
                              </td>
                              <?php
                    } elseif ($row->resourceUnit == "SMS") {
                        ?>
                              <td>
                                <?php echo $row->resourceValue; ?>
                              </td>
                              <?php
                    } else {
                        ?>
                              <td>
                                <?php echo $row->resourceValue; ?>
                              </td>
                              <?php
                    } ?>
                              <?php if ($row->resourceUnit == "SECOND") {
                        ?>
                              <td>Minutes
                              </td>
                              <?php
                    } elseif ($row->resourceUnit == "CENT") {
                        ?>
                              <td>
                                <?php echo $service_info->currency; ?>
                              </td>
                              <?php
                    } elseif ($row->resourceUnit == "KB") {
                        ?>
                              <td>
                              </td>
                              <?php
                    } elseif ($row->resourceUnit == "SMS") {
                        ?>
                              <td>
                                <?php echo $row->resourceUnit; ?>
                              </td>
                              <?php
                    } else {
                        ?>
                              <td>
                                <?php echo $row->resourceUnit; ?>
                              </td>
                              <?php
                    } ?>
                              <td class="text-right">
                                <?php echo convert_date_to_uk_from_slash($row->resourceExpirationDate); ?>
                              </td>
                            </tr>
                            <?php
                } ?>
                            <?php
            } else {
                ?>
                          <?php $row = $service_info->resources; ?>
                              <tr>
                              <td>
                                <?php echo $row->resourceName; ?>
                              </td>
                              <?php if ($row->resourceUnit == "SECOND") {
                    ?>
                              <td>
                                <?php echo s2m($row->resourceValue); ?>
                              </td>
                              <?php
                } elseif ($row->resourceUnit == "CENT") {
                    ?>
                              <td>
                                <?php echo number_format(($row->resourceValue/100), 2); ?>
                              </td>
                              <?php
                } elseif ($row->resourceUnit == "KB") {
                    ?>
                              <td>
                                <?php echo convertToReadableSize($row->resourceValue*1024); ?>
                              </td>
                              <?php
                } elseif ($row->resourceUnit == "SMS") {
                    ?>
                              <td>
                                <?php echo $row->resourceValue; ?>
                              </td>
                              <?php
                } else {
                    ?>
                              <td>
                                <?php echo $row->resourceValue; ?>
                              </td>
                              <?php
                } ?>
                              <?php if ($row->resourceUnit == "SECOND") {
                    ?>
                              <td>Minutes
                              </td>
                              <?php
                } elseif ($row->resourceUnit == "CENT") {
                    ?>
                              <td>
                                <?php echo $service_info->currency; ?>
                              </td>
                              <?php
                } elseif ($row->resourceUnit == "KB") {
                    ?>
                              <td>
                              </td>
                              <?php
                } elseif ($row->resourceUnit == "SMS") {
                    ?>
                              <td>
                                <?php echo $row->resourceUnit; ?>
                              </td>
                              <?php
                } else {
                    ?>
                              <td>
                                <?php echo $row->resourceUnit; ?>
                              </td>
                              <?php
                } ?>
                              <td class="text-right">
                                <?php echo convert_date_to_uk_from_slash($row->resourceExpirationDate); ?>
                              </td>
                            </tr>

                              <?php
            } ?>
                            <?php
        } ?>
                          </tbody>
                        </table>
                        <div id="loadingx1" style="display:none;">
                          <center>
                            <img
                              src="<?php echo base_url(); ?>assets/img/loader1.gif"
                              height="100" class="text-center">
                          </center>
                        </div>
                        <center>

                          <button class="btn btn-success" id="addbundle" type="button" <?php if (empty($service->details->teum_accountid)) {
            ?> disabled<?php
        } ?>>
                            <i class="fa fa-plus-circle">
                            </i>
                            <?php echo lang('Add 30 days Bundle'); ?>
                          </button>
                          <button class="btn btn-success" id="topup" type="button" <?php if (empty($service->details->teum_accountid)) {
            ?> disabled<?php
        } ?>>
                            <i class="fa fa-plus-circle">
                            </i>
                            <?php echo lang('Toup Credit'); ?>
                          </button>
                          <button class="btn btn-primary" id="open_usage" type="button" <?php if (empty($service->details->teum_accountid)) {
            ?> disabled<?php
        } ?>>
                            <i class="fa fa-chart-bar">
                            </i>
                            <?php echo lang('Usages'); ?>
                          </button>

                          <?php if ($this->session->email == "simson.parlindungan@united-telecom.be") {
            ?>

                          <button class="btn btn-danger" id="delete_subscription" onclick="ds()" type="button">
                            <i class="fa fa-trash">
                            </i>
                            <?php echo lang('Delete Subscription'); ?>
                          </button>
                          <?php
        } ?>

               


                        <?php if (trim($service_info->{'card-network-status'}) == "ACTIVATED") {
            ?>
                          <button data-toggle="modal" data-target="#SuspendModal" class="btn btn-danger" id="freeze"
                            type="button" <?php if (empty($service->details->teum_accountid)) {
                ?> disabled<?php
            } ?>>
                            <i class="fa fa-pause">
                            </i>
                            <?php echo lang('Freeze'); ?>
                          </button>
                          <?php
        } else {
            ?>
                          <button data-toggle="modal" data-target="#UnsuspendModal" class="btn btn-success"
                            id="unfreeze" type="button" <?php if (empty($service->details->teum_accountid)) {
                ?> disabled<?php
            } ?>>
                            <i class="fa fa-play">
                            </i>
                            <?php echo lang('Unfreeze'); ?>
                          </button>
                          <?php
        } ?>

                           <?php if (trim($service_info->{'card-network-status'}) == "ACTIVATED" && $service->details->msisdn_type == "new") {
            ?>
                          <button data-toggle="modal" data-target="#PortinNewNumber" class="btn btn-danger" id="portin" type="button">
                            <i class="fa fa-fire">
                            </i>
                            <?php echo lang('Port-In'); ?>
                          </button>
                <?php
        } ?>


                        </center>
                        <hr />


                      </div>
                    </div>
                  </div>
                </div>
                <!-- module import -->
                <div class="row usage" style="display:none;">
                  <form id="getusage">
                    <div class="row">
                      <div class="col-4">
                        <input type="text" id="pickdate2" name="from" placeholder="FROM" class="form-control"
                          value="<?php echo date('Y-m-d', strtotime('-30 days')); ?>"
                          readonly>
                      </div>
                      <div class="col-4">
                        <input type="text" id="pickdate3" name="to" placeholder="TO" class="form-control"
                          value="<?php echo date('Y-m-d'); ?> "
                          readonly>
                      </div>
                      <div class="col-4">
                        <button class='btn btn-primary' type="button" onclick="getUsageNow()"> <i
                            class="fas fa-cog fa-spin" style="display:none;"></i> Get usage
                        </button>
                      </div>
                    </div>
                  </form>
                  <br />
                  <br />
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>From
                        </th>
                        <th>To
                        </th>
                        <th>Date
                        </th>
                        <th>Duration
                        </th>
                        <th>Amount
                        </th>
                        <th>Type
                        </th>
                      </tr>
                    </thead>
                    <tbody id="usages">
                    </tbody>
                  </table>
                </div>
                <?php if ($setting->whmcs_ticket) {
            ?>
                <hr />
                <table class="table table-striped table-bordered tik" id="ticket_tables">
                  <thead>
                    <tr class="bg-primary  text-white">
                      <th width="15%">
                        <?php echo lang('Ticket ID'); ?>
                      </th>
                      <th width="15%">
                        <?php echo lang('Date'); ?>
                      </th>
                      <th width="55%">
                        <?php echo lang('Subject'); ?>
                      </th>
                      <th width="15%">
                        <?php echo lang('Department'); ?>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <?php
        } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <form>
    <input type="hidden" id="msisdn"
      value="<?php echo trim($service->domain); ?>">
  </form>
  <script>


    function IgnorePlan(id) {
      $('#SumPlanIdActionIgnore').modal('toggle');
      $('#SUMAssignmentIdIgnore').val(id);
    }

    function ActivatePlan(id) {
      $('#SumPlanIdActionActivate').modal('toggle');
      $('#SUMAssignmentIdActivate').val(id);
    }

    function RemovePlan(id) {
      $('#SumPlanIdActionRemove').modal('toggle');
      $('#SUMAssignmentIdRemove').val(id);
    }

    function OpenPorting() {
      $('#PortingExist').modal('toggle');
    }
  </script>
  <div class="modal fade" id="PortingExist">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="portingdata" method="post"
          action="<?php echo base_url(); ?>admin/subscription/addPorting">
          <input type="hidden" name="serviceid"
            value="<?php echo $service->id; ?>">
          <input type="hidden" name="userid"
            value="<?php echo $service->userid; ?>">
          <input type="hidden" name="orderid"
            value="<?php echo $service->id; ?>">
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Add Porting'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;
            </button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row" id="portingbody">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Mobile Number'); ?>
                    :31XXXXXXXXXX
                  </label>
                  <input class="form-control" autocomplete="new-username"
                    placeholder="<?php echo lang('REQUIRED'); ?>"
                    type="number" name="MsisdnNumber" id="msisdn" value="31" required>
                </div>
                <div class="form-group">
                  <label for="">Type
                  </label>
                  <select class="form-control" id="type_number" name="PortingType">
                    <option value="0" selected>
                      <?php echo lang('Prepaid'); ?>
                    </option>
                    <option value="1">
                      <?php echo lang('Postpaid'); ?>
                    </option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Current Provider'); ?>
                  </label>
                  <select class="form-control" id="provider" name="provider">
                    <?php foreach (getDonors('NL') as $row) {
            ?>
                    <option
                      value="<?php echo $row->operator_code; ?>"
                      selected>
                      <?php echo $row->operator_name; ?>
                    </option>
                    <?php
        } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('SIMcard Number'); ?>
                  </label>
                  <input class="form-control pengkor" autocomplete="new-username" placeholder="" type="text"
                    name="simnumber" id="simnumberx" required>
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('Customer Type'); ?>
                  </label>
                  <select class="form-control" id="customertype1" name="customertype1">
                    <option value="0" selected>
                      <?php echo lang('Residential'); ?>
                    </option>
                    <option value="1">
                      <?php echo lang('Bussiness'); ?>
                    </option>
                  </select>
                </div>
                <div class="form-group" style="display:none;" id="acct">
                  <label for="">
                    <?php echo lang('Client Number from other provider'); ?>
                  </label>
                  <input class="form-control" autocomplete="new-username" placeholder="" type="text"
                    name="AccountNumber" id="AccountNumber">
                </div>
                <div class="form-group">
                  <label for="">
                    <?php echo lang('PortIN Date Wish'); ?>:
                  </label>
                  <input class="form-control" autocomplete="new-username"
                    placeholder="<?php echo date('Y-m-d'); ?>"
                    type="text" name="PortInWishDate" id="pickdate8" required>
                </div>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" id="portingbutton" class="btn btn-primary">
              <?php echo lang('Submit'); ?>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- The Bundle List Modal -->
  <div class="modal fade" id="BundleModalTarif">
    <div class="modal-dialog">
      <div class="modal-content">
        <form method="post"
          action="<?php echo base_url(); ?>admin/subscription/addBundleTarif">
          <input type="hidden" name="SN"
            value="<?php echo $mobile->msisdn_sn; ?>">
          <input type="hidden" name="userid"
            value="<?php echo $service->userid; ?>">
          <input type="hidden" name="msisdn"
            value="<?php echo $service->domain; ?>">
          <input type="hidden" name="serviceid"
            value="<?php echo $service->id; ?>">
          <input type="hidden" name="typebar" value="2">
          <!-- Modal Header SimCardNbr -->
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Add Extra Bundle'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;
            </button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group bundle_select2" style="display:none;">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('The bundle you are ordering will be auto renew for next month until the date you specified below  and it will be auto prorata for the current month if aplicable:'); ?>
                    </label>
                    <br />
                    <select class="form-control" id="bundleid2" name="bundleid">
                    </select>
                  </fieldset>
                </div>
                <div class="form-group bundle_loading2">
                  <center>
                    <img
                      src="<?php echo base_url(); ?>assets/img/loader1.gif">
                  </center>
                </div>
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="charge">
                      <?php echo lang('Charge Customer?'); ?>
                      (€)
                      <?php echo lang('each Month until Valid Until'); ?>
                    </label>
                    <select class="form-control" id="bundlechargeid2" name="charge">
                      <option value="1" selected>Yes
                      </option>
                      <option value="0">No
                      </option>
                    </select>
                  </fieldset>
                </div>
                <div class="form-group chargex">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('Price'); ?>
                    </label>
                    <input type="text" class="form-control" id="pricex" name="price" value="">
                  </fieldset>
                </div>
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('VALID FROM'); ?>
                    </label>
                    <input type="text" class="form-control" id="pickdate13a" name="from"
                      value="<?php echo date('Y-m-d\TH:i:s'); ?>"
                      required readonly>
                  </fieldset>
                </div>
                <div class="form-group">
                  <fieldset>
                    <label class="control-label" for="type">
                      <?php echo lang('VALID UNTIL'); ?>
                    </label>
                    <input type="text" class="form-control" id="pickdate15" name="to" value="" readonly>
                  </fieldset>
                </div>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">
              <?php echo lang('Add Bundle'); ?>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="modal fade" id="BlockOutgoingModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="blockout">
          <input type="hidden" name="SN"
            value="<?php echo $mobile->msisdn_sn; ?>">
          <input type="hidden" name="userid"
            value="<?php echo $service->userid; ?>">
          <input type="hidden" name="msisdn"
            value="<?php echo $service->domain; ?>">
          <input type="hidden" name="serviceid"
            value="<?php echo $service->id; ?>">
          <input type="hidden" name="typebar" value="2">
          <!-- Modal Header SimCardNbr -->
          <div class="modal-header">
            <h4 class="modal-title">
              <?php echo lang('Block Originating/Outbound Traffic'); ?>
            </h4>
            <button type="button" class="close" data-dismiss="modal">&times;
            </button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <center>
                  <?php echo lang("Do you wish to proceed"); ?>?
                </center>
                <table class="table table-striped">
                  <div id="loadingx40" style="display:none;">
                    <img
                      src="<?php echo base_url(); ?>assets/img/loader1.gif"
                      height="100">
                  </div>
                </table>
                <button type="button" id="blockoriginating" class="btn btn-block btn-primary btn-md">Submit
                </button>
              </div>
            </div>
        </form>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>
<div class="modal fade" id="UnBlockOutgoingModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="unblockout">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Block Originating/Outbound Traffic'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <center>
                <?php echo lang("Do you wish to proceed"); ?>?
              </center>
              <table class="table table-striped">
                <div id="loadingx40" style="display:none;">
                  <img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif"
                    height="100">
                </div>
              </table>
              <button type="button" id="unblockoriginating" class="btn btn-block btn-primary btn-md">
                <?php echo lang('Save Changes'); ?>
              </button>
            </div>
          </div>
      </form>
    </div>
    <!-- Modal footer -->
  </div>
</div>
</div>


<div class="modal" id="PortinNewNumber">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="portindata">
        <input type="hidden" id="reload_userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" id="reload_serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Portability'); ?> to <?php echo $service->details->msisdn; ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <h6><?php echo lang('Do you wish to port a number to current active msisdn?'); ?></h6>
          

          <div class="row" id="loadingreload" style="display:none;">
            <div class="col-sm-12">
              <table class="table table-striped">
                <div id="loadingx401" class="text-center">
                  <img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif"
                    height="100">
                </div>
              </table>
            </div>
          </div>
          <div class="form-group" id="datareload">
            <fieldset>
              <label class="control-label" for="type">
                <?php echo lang('Number To Port'); ?>
              </label>
              <input type="text" class="form-control" id="msisdnx" name="msisdnx" placeholder="44XXXXXXXXXX" required>
            </fieldset>
             <fieldset>
              <label class="control-label" for="type">
                <?php echo lang('PAC Code'); ?>
              </label>
              <input type="text" class="form-control" id="pac" name="pac" placeholder="123456" required>
            </fieldset>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button type="button" id="PortNow" class="btn btn-block btn-primary btn-md">
                <?php echo lang('Submit Portability'); ?>
              </button>
            </div>
          </div>
        </div>
      </form>
      <!-- Modal footer -->
    </div>
  </div>
</div>


<div class="modal" id="addReload">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="reload">
        <input type="hidden" id="reload_userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" id="reload_serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Reload Credit'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row" id="loadingreload" style="display:none;">
            <div class="col-sm-12">
              <table class="table table-striped">
                <div id="loadingx40" class="text-center">
                  <img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif"
                    height="100">
                </div>
              </table>
            </div>
          </div>
          <div class="form-group" id="datareload">
            <fieldset>
              <label class="control-label" for="type">
                <?php echo lang('Credit to add'); ?>
              </label>
              <input type="text" class="form-control" id="reload_credit" name="credit" value="10" required>
            </fieldset>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button type="button" id="ReloadNow" class="btn btn-block btn-primary btn-md">
                <?php echo lang('Reload'); ?>
              </button>
            </div>
          </div>
        </div>
      </form>
      <!-- Modal footer -->
    </div>
  </div>
</div>
<div class="modal fade" id="AdvanceSetting">
  <div class="modal-dialog">
    <div class="modal-content">
      <input type="hidden" name="SN"
        value="<?php echo $mobile->msisdn_sn; ?>">
      <input type="hidden" name="userid"
        value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn"
        value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid"
        value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Advance Setting'); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <table class="table table-striped" id="showAdvance">
              <div id="loadingx40" style="display:none;">
                <img
                  src="<?php echo base_url(); ?>assets/img/loader1.gif"
                  height="100">
              </div>
            </table>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
    </div>
  </div>
</div>
<div class="modal fade" id="EnableAllSetting">
  <div class="modal-dialog">
    <div class="modal-content">
      <input type="hidden" name="SN"
        value="<?php echo $mobile->msisdn_sn; ?>">
      <input type="hidden" name="userid"
        value="<?php echo $service->userid; ?>">
      <input type="hidden" name="msisdn"
        value="<?php echo $service->domain; ?>">
      <input type="hidden" name="serviceid"
        value="<?php echo $service->id; ?>">
      <input type="hidden" name="typebar" value="2">
      <!-- Modal Header SimCardNbr -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Enable All Services'); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <center>
              <?php echo lang('Do you wish to enable all services including Roaming,Premium,Data,International & ETC'); ?>?
            </center>
          </div>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" id="enableallsetting" class="btn btn-primary">
          <?php echo lang('Submit'); ?>
        </button>
      </div>
    </div>
  </div>
</div>
<!-- The Stolen Modal -->
<div class="modal fade" id="StolenModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/mobile_stolen">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="SimCardNbr"
          value="<?php echo $mobile->msisdn_sim; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Phone Stolen?'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('Create Ticket SIM Replacement'); ?>
                  </label>
                  <select class="form-control" id="ticket" name="ticket">
                    <option value="1">
                      <?php echo lang('NO'); ?>
                    </option>
                    <option value="2">
                      <?php echo lang('YES'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>
                  </label>
                  <input name="msisdn" class="form-control" type="text"
                    value="<?php echo $service->domain; ?>" readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">
            <?php echo lang('Report Stolen'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="unStolenModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/mobile_unstolen">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="SimCardNbr"
          value="<?php echo $mobile->msisdn_sim; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Phone Found or Simcard Replaced?'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>
                  </label>
                  <input name="msisdn" class="form-control" type="text"
                    value="<?php echo $service->domain; ?>" readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">
            <?php echo lang('Remove Stolen Record'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The Suspend Modal -->
<div class="modal fade" id="LangModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/update_sim_language">
        <input type="hidden" name="msisdn"
          value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to change Platform Language'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('Languages'); ?> (
                    <?php echo lang('VoiceMail'); ?>)
                  </label>
                  <select class="form-control" id="exampleSelect1" name="language">
                    <option value="1" <?php if ($client->language == "english") {
            ?> selected
                      <?php
        } ?>>
                      <?php echo lang('English'); ?>
                    </option>
                    <option value="2" <?php if ($client->language == "french") {
            ?> selected
                      <?php
        } ?>>
                      <?php echo lang('French'); ?>
                    </option>
                    <option value="3" <?php if ($client->language == "dutch") {
            ?> selected
                      <?php
        } ?>>
                      <?php echo lang('Dutch'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>
                  </label>
                  <input name="msisdn" class="form-control" id="msisdn" type="text"
                    value="<?php echo $service->domain; ?>" readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-secondary" data-dismiss="modal">
            <i class="fa fa-thumbs-down">
            </i>
            <?php echo lang('No'); ?>
          </button>
          <button type="submit" class="btn btn-md btn-primary">
            <i class="fa fa-thumbs-up">
            </i>
            <?php echo lang('Yes'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The Unsuspend Modal -->
<!-- The Swap Modal -->
<!-- The Unsuspend Modal -->
<div class="modal fade" id="ChangeContractTerm">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/change_contract_terms">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to change the contract terms?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Contract Terms'); ?>
                  </label>
                  <input type="text" name="msisdn"
                    value="<?php echo $service->details->msisdn; ?>"
                    class="form-control" readonly>
                </fieldset>
              </div>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Contract Terms'); ?>
                  </label>
                  <select name="contract_terms" class="form-control">
                    <?php foreach (array(1,3,6,12,24) as $row) {
            ?>
                    <option value="<?php echo $row; ?>" <?php if ($service->contract_terms == $row) {
                ?> selected
                      <?php
            } ?>>
                      <?php echo $row; ?> Month(s)
                    </option>
                    <?php
        } ?>
                  </select>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary">
            <i class="fa fa-thumbs-up">
            </i>
            <?php echo lang('Yes'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The Swap Modal -->
<div class="modal fade" id="SwapModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/swap_simcard">
        <input type="hidden" name="msisdn"
          value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to swap SIMcard?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>
                  </label>
                  <input name="new_simnumber" class="form-control" type="text"
                    value="<?php echo trim($mobile->msisdn); ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Current SIMCARD'); ?>
                  </label>
                  <input name="new_simnumber" class="form-control" type="text"
                    value="<?php echo $mobile->msisdn_sim; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('New SIMCARD'); ?>
                  </label>
                  <input name="new_simnumber" class="form-control" type="text" value="" required>
                  <span class="text-help">
                    <?php echo lang('This will revoke old simcard, please make sure the simcard has been received by customer before executing this'); ?>
                  </span>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <fieldset>
                  <label for="exampleSelect1">
                    <?php echo lang('Charge Customer for SWAP'); ?>
                  </label>
                  <select class="form-control" id="swapcharge" name="charge">
                    <option value="YES" <?php if ($this->session->cid == "54") {
            ?> selected
                      <?php
        } ?>>Yes
                    </option>
                    <option value="NO" <?php if ($this->session->cid != "54") {
            ?> selected
                      <?php
        } ?>>No
                    </option>
                  </select>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-6" style="display:none;" id="swapamount">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="amount">
                    <?php echo lang('Amount'); ?>
                    (€)
                  </label>
                  <input name="amount" id="chargeamount" class="form-control" type="number"
                    value="<?php echo round($service->mobile_swapcost, 2); ?>">
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary">
            <i class="fa fa-thumbs-up">
            </i>
            <?php echo lang('Yes Swap Simcard'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The ChangeService Modal -->
<div class="modal fade" id="ChangeService">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/change_service">
        <input type="hidden" name="msisdn"
          value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="sn"
          value="<?php echo trim($mobile->msisdn_sn); ?>">
        <input type="hidden" name="requestor"
          value="<?php echo $this->session->firstname . ' ' . $this->session->lastname; ?>">
        <input type="hidden" name="old_pid"
          value="<?php echo $service->packageid; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to Change the Package?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="msisdn">
                  <?php echo lang('CLI(Number)'); ?>
                </label>
                <input class="form-control" type="text"
                  value="<?php echo trim($service->domain); ?>"
                  readonly>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Product'); ?>:
                </label>
                <select class="form-control" id="addonproduct" name="new_pid">
                  <?php foreach (getProductSell($this->session->cid) as $row) {
            ?>
                  <option value="<?php echo $row->id; ?>">
                    <?php echo $row->name; ?>
                    <?php if ($row->setup > 0) {
                echo 'Setup fee: &euro;' . number_format($row->setup, 2);
            } ?>
                  </option>
                  <?php
        } ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Contract'); ?>
                  (
                  <?php echo lang('Month'); ?>)
                </label>
                <select class="form-control" id="contractduration" name="ContractDuration">
                  <option value="3">
                    <?php echo lang('Quarterly'); ?> (3
                    Months)
                  </option>
                  <option value="6">
                    <?php echo lang('Semi Annually'); ?>
                    (6 Months)
                  </option>
                  <option value="12" selected>
                    <?php echo lang('Annually'); ?> (12
                    Months)
                  </option>
                  <option value="24">
                    <?php echo lang('Bienially'); ?> (24
                    Months)
                  </option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Price Recurring'); ?>
                </label>
                <input name="new_price" type="text" class="form-control" id="harga" value="">
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="msisdn">
                  <?php echo lang('Date Start'); ?>
                </label>
                <input name="date_commit" class="form-control" id="firstdate" type="text"
                  value="<?php echo next_month_date(); ?>" required
                  readonly>
                <span class="text-help">
                  <?php echo lang('Please choose the date this package to be activated'); ?>
                </span>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="exampleSelect1">
                  <?php echo lang('Charge Customer?'); ?>
                </label>
                <select class="form-control" id="changecharge" name="charge">
                  <option value="YES" <?php if ($this->session->cid == "54") {
            ?> selected
                    <?php
        } ?>>Yes
                  </option>
                  <option value="NO" <?php if ($this->session->cid != "54") {
            ?> selected
                    <?php
        } ?>>No
                  </option>
                </select>
              </div>
            </div>
            <div class="col-sm-6" style="display:none;" id="changeamount">
              <div class="form-group">
                <label class="control-label" for="amount">
                  <?php echo lang('Amount'); ?>
                  (€)
                </label>
                <input name="amount" id="charechangeamount" class="form-control" type="number"
                  value="<?php echo round($service->mobile_changecost, 2); ?>">
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary">
            <i class="fa fa-thumbs-up">
            </i>
            <?php echo lang('Yes Change Package'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="TerminateModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/terminate_sim">
        <input type="hidden" name="msisdn"
          value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to Terminate This SIM?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>
                  </label>
                  <input name="new_simnumber" class="form-control" type="text"
                    value="<?php echo $service->domain; ?>" readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Current SIMCARD'); ?>
                  </label>
                  <input name="new_simnumber" class="form-control" type="text"
                    value="<?php echo $mobile->msisdn_sim; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <div class="form-desc">
                  <?php echo lang('Date Cancellation'); ?>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input cancel" name="date_cancellation" id="date_cancellation1"
                      value="now" checked="">
                    <?php echo lang('Now'); ?>
                  </label>
                </div>
                <div class="form-check">
                  <label class="form-check-label">
                    <input type="radio" class="form-check-input cancel" name="date_cancellation" id="date_cancellation2"
                      value="future">
                    <?php echo lang('Future'); ?>
                  </label>
                </div>
              </div>
            </div>
            <div class="col-sm-12 datefuture" style="display:none;">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Date Termination'); ?>
                  </label>
                  <input name="date" class="form-control" type="text" id="pickdate66"
                    value="<?php echo date(" Y-m-d", strtotime("tomorrow")); ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary">
            <i class="fa fa-thumbs-up">
            </i>
            <?php echo lang('Yes Cancel Service'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="CancelTerminateModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/cancel_terminate_sim">
        <input type="hidden" name="msisdn"
          value="<?php echo trim($service->domain); ?>">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish to Cancel this Cancellation?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>
                  </label>
                  <input name="new_simnumber" class="form-control" type="text"
                    value="<?php echo $service->domain; ?>" readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('Current SIMCARD'); ?>
                  </label>
                  <input name="new_simnumber" class="form-control" type="text"
                    value="<?php echo $mobile->msisdn_sim; ?>"
                    readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary">
            <i class="fa fa-thumbs-up">
            </i>
            <?php echo lang('Yes Cancel The Cancellation'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The CDR Modal -->
<div class="modal fade" id="CdrModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Call Detail records'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="cdr">
            <thead>
              <tr>
                <th>
                  <?php echo lang('Date'); ?>
                </th>
                <th>
                  <?php echo lang('Number'); ?>
                </th>
                <th>
                  <?php echo lang('Description'); ?>
                </th>
                <th>
                  <?php echo lang('Duration'); ?>
                </th>
                <th>
                  <?php echo lang('Type'); ?>
                </th>
                <th>
                  <?php echo lang('Cost'); ?>
                </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <?php echo lang('Close'); ?>
        </button>
        <a class="btn btn-md btn-primary"
          href="<?php echo base_url(); ?>admin/subscription/export_livecdr/<?php echo trim($service->domain); ?>/<?php echo $service->id; ?>/<?php echo trim($mobile->msisdn_sn); ?>">
          <i class="fa fa-file-excel">
          </i>
          <?php echo lang('Export Excel'); ?>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="SumPlanIdActionIgnore">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/setSumPlanAction">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="orderid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="sn"
          value="<?php echo $service->details->msisdn_sn; ?>">
        <input type="hidden" name="State" value="Ignore">
        <input type="hidden" name="SUMAssignmentId" id="SUMAssignmentIdIgnore" value="">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Do you wish to disable SumPlan for current month'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Mobile Number'); ?>
                </label>
                <input class="form-control" autocomplete="new-username"
                  placeholder="<?php echo lang('REQUIRED'); ?>"
                  type="number" name="MsisdnNumber" id="msisdn"
                  value="<?php echo $service->details->msisdn; ?>"
                  required readonly>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Process'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="SumPlanIdActionActivate">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/setSumPlanAction">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="orderid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="sn"
          value="<?php echo $service->details->msisdn_sn; ?>">
        <input type="hidden" name="State" value="Reactivate">
        <input type="hidden" name="SUMAssignmentId" id="SUMAssignmentIdActivate" value="">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Do you wish to disable SumPlan for current month'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Mobile Number'); ?>
                </label>
                <input class="form-control" autocomplete="new-username"
                  placeholder="<?php echo lang('REQUIRED'); ?>"
                  type="number" name="MsisdnNumber" id="msisdn"
                  value="<?php echo $service->details->msisdn; ?>"
                  required readonly>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Process'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="SumPlanIdActionRemove">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/setSumPlanAction">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="orderid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="sn"
          value="<?php echo $service->details->msisdn_sn; ?>">
        <input type="hidden" name="SUMAssignmentId" id="SUMAssignmentIdRemove" value="">
        <input type="hidden" name="State" value="Disable">
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Do you wish to remove this sumPlan forerver?'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label for="">
                  <?php echo lang('Mobile Number'); ?>
                </label>
                <input class="form-control" autocomplete="new-username"
                  placeholder="<?php echo lang('REQUIRED'); ?>"
                  type="number" name="MsisdnNumber" id="msisdn"
                  value="<?php echo $service->details->msisdn; ?>"
                  required readonly>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">
            <?php echo lang('Process'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The CDR Modal -->
<div class="modal fade" id="CdrInModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Incoming Call Detail records'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="cdrin">
            <thead>
              <tr>
                <th>
                  <?php echo lang('Date'); ?>
                </th>
                <th>
                  <?php echo lang('Number'); ?>
                </th>
                <th>
                  <?php echo lang('Description'); ?>
                </th>
                <th>
                  <?php echo lang('Duration'); ?>
                </th>
                <th>
                  <?php echo lang('Type'); ?>
                </th>
                <th>
                  <?php echo lang('Cost'); ?>
                </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">
    <?php echo lang('Close'); ?></button>
        <a class="btn btn-md btn-primary"
          href="<?php echo base_url(); ?>admin/subscription/export_livecdr/<?php echo trim($service->domain); ?>/<?php echo $service->id; ?>/<?php echo trim($mobile->msisdn_sn); ?>"><i
            class="fa fa-file-excel"></i>
          <?php echo lang('Export Excel'); ?></a>
        -->
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="MoveService">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Do you wish to move this subscription to another customer'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <!-- Modal body -->
      <form
        action="<?php echo base_url(); ?>admin/subscription/move_subscription"
        method="post">
        <input type="hidden" name="serviceid"
          value="<?php echo $mobile->serviceid; ?>">
        <input type="hidden" name="olduserid"
          value="<?php echo $service->userid; ?>">
        <div class="modal-body">
          <div class="table-responsive">
            <div class="form-group">
              <fieldset>
                <label class="control-label" for="msisdn">
                  <?php echo lang('Artilium ID'); ?>
                </label>
                <input name="userid" class="form-control ui-autocomplete-input customerid" type="text" id="customerid"
                  required>
              </fieldset>
            </div>
            <div class="form-group" id="hidecustomer1">
              <fieldset>
                <label class="control-label" for="msisdn">
                  <?php echo lang('CustomerName'); ?>
                </label>
                <input name="name" class="form-control" type="text" id="customername" readonly="">
              </fieldset>
            </div>
            <!--<div class="form-group" id="hidecustomer2">
<fieldset>
<label class="control-label" for="msisdn">
    <?php echo lang('Relationid'); ?></label>
            <input name="relationid" class="form-control" type="text" id="relationidx" readonly="">
            </fieldset>
          </div>
          -->
        </div>
    </div>
    <!-- Modal footer -->
    <div class="modal-footer">
      <!--<button type="submit" id="movecustomer" class="btn btn-primary">
            <?php echo lang('Move Subscription'); ?>
      </button>
      -->
    </div>
    </form>
  </div>
</div>
</div>
<div class="modal fade" id="ExpireBundleModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Change the Expired date of the Bundle'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <!-- Modal body -->
      <form id="expBundle">
        <input type="hidden" name="BundleAssignId" value="" id="assignid">
        <input type="hidden" name="sn"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $this->uri->segment(4); ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <div class="modal-body">
          <div class="table-responsive">
            <div class="form-group">
              <fieldset>
                <label class="control-label" for="msisdn">
                  <?php echo lang('Date Valid Until'); ?>
                </label>
                <input name="ValidUntil" class="form-control" type="text" id="pickdate15d"
                  value="<?php echo date("Y-m-t\T23:59:59"); ?>"
                  readonly>
              </fieldset>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" id="changeValidBundle" class="btn btn-primary">
            <?php echo lang('Save Changes'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- The CDR Modal -->
<div class="modal fade" id="Plt">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          <?php echo lang('Platform Logs records'); ?>:
          <?php echo $service->domain; ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;
        </button>
      </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="table-responsive">
          <table class="table table-striped table-lightfont" id="platformlog">
            <thead>
              <tr>
                <th>
                  <?php echo lang('Date'); ?>
                </th>
                <th>
                  <?php echo lang('Command'); ?>
                </th>
                <th>
                  <?php echo lang('Value'); ?>
                </th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
          <?php echo lang('Close'); ?>
        </button>
        <a class="btn btn-md btn-primary"
          href="<?php echo base_url(); ?>admin/subscription/export_livecdr/<?php echo trim($service->domain); ?>/<?php echo $service->id; ?>/<?php echo trim($mobile->msisdn_sn); ?>">
          <i class="fa fa-file-excel">
          </i>
          <?php echo lang('Export Excel'); ?>
        </a>
      </div>
    </div>
  </div>
</div>
<?php //print_r($service);?>
<?php
    }?>
<script>

    $('#PortNow').click(function() {
     var msisdn = $('#msisdnx').val();
     var pac = $('#pac').val();
     if(!msisdn){
      alert('Please enter msisdn');
      return;
     }else if(!pac){
      alert('Please enter PAC Code');
        return;
     }
      $('#loadingreload').show();
      $('#PortNow').prop('disabled', true);
    $.ajax({
      url: '<?php echo base_url(); ?>admin/subscription/Teum_addPortin',
      type: 'post',
      dataType: 'json',
      success: function(data) {
        $('#loadingreload').hide();
          window.location.replace("<?php echo base_url();?>admin/subscription/detail/<?php echo $this->uri->segment(4); ?>");
      },
      data: {
        'msisdn': msisdn,
        'pac': pac,
        'serviceid': '<?php echo $this->uri->segment(4); ?>'
      }
  
    });
    
  });


  $('#addbundle').click(function() {
    var msisdn = '<?php echo trim($service->domain); ?>';
    $('#BundleModal').modal('toggle');
    $.ajax({
      url: '<?php echo base_url(); ?>admin/complete/getBundleList/<?php echo $this->session->cid; ?>',
      type: 'post',
      dataType: 'json',
      success: function(data) {
        $('.bundle_loading1').hide();
        $('#bundleid1').html(data.html);
        $('.bundle_select1').show();
      },
      <?php if ($this->session->cid == 55) {
        ?>
      data: {
        'msisdn': msisdn,
        'type': 'option',
        'agid': '<?php echo $service->gid; ?>',
        'serviceid': '<?php echo $service->id; ?>'
      }
      <?php
    } else {
        ?>
      data: {
        'msisdn': msisdn,
        'type': 'option',
        'serviceid': '<?php echo $this->uri->segment(4); ?>'
      }
      <?php
    } ?>
    });
    //$('#BundleModal').modal('toggle');
  });
  $('#topup').click(function() {
    $('.bundle_loading1').hide();
    var msisdn = '<?php echo trim($service->domain); ?>';
    $('#TopupModal').modal('toggle');
    $.ajax({
      url: '<?php echo base_url(); ?>admin/complete/getBundleList/<?php echo $this->session->cid; ?>',
      type: 'post',
      dataType: 'json',
      success: function(data) {

        $('#bundleid1').html(data.html);
        $('.bundle_select1').show();
      },
      <?php if ($this->session->cid == 55) {
        ?>
      data: {
        'msisdn': msisdn,
        'type': 'option',
        'agid': '<?php echo $service->gid; ?>'
      }
      <?php
    } else {
        ?>
      data: {
        'msisdn': msisdn,
        'type': 'option'
      }
      <?php
    }
?>
    });
  });
  $('#open_usage').click(function() {
    $('.usage').show('slow');
  });
</script>
<div class="modal fade" id="BundleModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="bundleform" method="post"
        action="<?php echo base_url(); ?>admin/subscription/Teum_addBundle">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Add 30 days bundle'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group bundle_select1" style="display:none;">
                <fieldset>
                  <label class="control-label" for="type">
                    <?php echo lang('This will activate Bundle for 30 days and auto renew depend on the amount month you have chosen'); ?>
                  </label>
                  <br />
                  <select class="form-control" id="bundleid1" name="bundleid">
                  </select>
                </fieldset>
              </div>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Auto Renew for how many months, 1month = 30 days'); ?>
                  </label>
                  <select class="form-control" name="month">
                    <option value="1"><?php echo lang('One Time'); ?>
                    </option>
                    <option value="2">1 <?php echo lang('Months'); ?>
                    </option>
                    <option value="3">3 <?php echo lang('Months'); ?>
                    </option>
                    <option value="6">6 <?php echo lang('Months'); ?>
                    </option>
                    <option value="12">12 <?php echo lang('Months'); ?>
                    </option>
                    <option value="24">24 <?php echo lang('Months'); ?>
                    </option>
                    <option value="36">36 <?php echo lang('Months'); ?>
                    </option>
                    <option value="600"> <?php echo lang('Indefinite'); ?>
                    </option>

                  </select>
                </fieldset>
              </div>
              <?php //print_r($service);?>
          <?php if ($service->reseller_type == "Prepaid") {
    ?>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Deduct Current Reseller Balance?'); ?>
                  </label>
                  <select class="form-control" name="reseller_charge">
                    <option value="No"><?php echo lang('No'); ?>
                    </option>
                    <option value="Yes"><?php echo lang('Yes'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            <?php
} ?>
              <div class="form-group bundle_loading1">
                <center>
                  <img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="disable_buttn('bundle')">
            <?php echo lang('Add Bundle'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="ReorderBundleModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="reorderform" method="post"
        action="<?php echo base_url(); ?>admin/subscription/Teum_addBundle">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="bundleid" id="bundleidvalue" value="">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Add 30 days bundle'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">

              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Auto Renew for how many months, 1month = 30 days'); ?>
                  </label>
                  <select class="form-control" name="month">
                    <option value="1"><?php echo lang('One Time'); ?>
                    </option>
                    <option value="2">1 <?php echo lang('Months'); ?>
                    </option>
                    <option value="3">3 <?php echo lang('Months'); ?>
                    </option>
                    <option value="6">6 <?php echo lang('Months'); ?>
                    </option>
                    <option value="12">12 <?php echo lang('Months'); ?>
                    </option>
                    <option value="24">24 <?php echo lang('Months'); ?>
                    </option>
                    <option value="36">36 <?php echo lang('Months'); ?>
                    </option>
                    <option value="600"><?php echo lang('Indefinite'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
              <?php if ($service->reseller_type == "Prepaid") {
        ?>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Deduct Current Reseller Balance?'); ?>
                  </label>
                  <select class="form-control" name="reseller_charge">
                    <option value="No"><?php echo lang('No'); ?>
                    </option>
                    <option value="Yes"><?php echo lang('Yes'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>
            <?php
    } ?>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="disable_buttn('reorder')">
            <?php echo lang('Add Bundle'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>


<div class="modal fade" id="TopupModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="topupform" method="post"
        action="<?php echo base_url(); ?>admin/subscription/Teum_Topup">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typebar" value="2">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <?php echo lang('Topup Credit'); ?>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Please add Amount to topup'); ?>
                    (<?php echo $setting-currency; ?>)
                  </label>
                  <input type="number" name="amount" step="any" id="amount" placeholder="10.00" class="form-control"
                    required>
                </fieldset>
              </div>
              <?php if ($service->reseller_type == "Prepaid") {
        ?>
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="charge">
                    <?php echo lang('Deduct Current Reseller Balance?'); ?>
                  </label>
                  <select class="form-control" name="reseller_charge">
                    <option value="No"><?php echo lang('No'); ?>
                    </option>
                    <option value="Yes"><?php echo lang('Yes'); ?>
                    </option>
                  </select>
                </fieldset>
              </div>

            <?php
    } ?>
              <div class="form-group bundle_loading1">
                <center>
                  <img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="disable_buttn('topup')">
            <?php echo lang('Topup Credit'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>

<script>
  function orderthisbundle(id, name) {
    $('#bundleidvalue').val(id);
    $('#ReorderBundleModal').modal('toggle');
  }

  function getUsageNow() {
    $('.fa-cog').show();
    $.ajax({
      url: '<?php echo base_url(); ?>admin/subscription/Teum_Usage/<?php echo $this->session->cid; ?>',
      type: 'post',
      dataType: 'json',
      success: function(data) {
        if (data.statusCode == "0") {
          $('#usages').html('');
          if (data.usages.length > 0) {
            data.usages.forEach(function(el) {
              $('.fa-cog').hide('slow');
              $('#usages').append('<tr><td>' + el.from + '</td><td>' + el.to + '</td><td>' + el.date +
                '</td><td>' + el.duration + '</td><td>' + el.amount + '</td><td>' + el.serviceType +
                '</td></tr>');
            });
          }
        }
      },
      data: {
        'from': $('#pickdate2').val(),
        'to': $('#pickdate3').val(),
        'serviceid': '<?php echo $service->id; ?>'
      }
    });
  }

  function disable_buttn(type) {
    $('.btn').prop('disabled', true);
    if (type == "topup") {
      if ($('#amount').val() == '') {
        $('.btn').prop('disabled', false);
        alert('Please enter the value of the amount');
        $('#amount').focus();
      } else {
        $('#topupform').submit();
      }
    } else if (type == 'renew') {
      $('#renewform').submit();

    } else if (type == 'reorder') {
      $('#reorderform').submit();
    } else {
      $('#bundleform').submit();
    }
  }

  function ds() {
    $('#delete_subscription').prop('disabled', true);
    var msisdn = '<?php echo trim($service->domain); ?>';

    var t = confirm('are you sure?');
    if (t) {
      $.ajax({
        url: '<?php echo base_url(); ?>admin/subscription/Teum_deleteSubscription/',
        type: 'post',
        dataType: 'json',
        success: function(data) {
          alert('Subscription has been deleted');
        },
        data: {
          'serviceid': '<?php echo $service->id; ?>',
          'msisdn': msisdn,
          'subscriptionid': '<?php echo getSubscriptionId($service->details->msisdn); ?>'

        }
      });
    }
  }

  function enable_disable_autorenew(id, name, type) {
    $('#RenewModal').modal('toggle');
    $('#addonid').val(id);
    $('#typeaction').val(type);
    if (type == 1) {
      $('.autorenewmonth').show('slow');
      $('.disaling').html('');
      $('.paketname').html("Enable Auto renew :" + name);
    } else {
      $('.autorenewmonth').hide();
      $('.disaling').html('This will disable auto renew, if would not disable current active allowance');
      $('.paketname').html("Disable Auto renew :" + name);

    }
    //$('#delete_subscription').prop('disabled', true);
  }
</script>

<div class="modal fade" id="RenewModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="renewform" method="post"
        action="<?php echo base_url(); ?>admin/subscription/Teum_Enable_Disable_Autorenew">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="msisdn"
          value="<?php echo $service->domain; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="typeaction" id="typeaction" value="0">
        <input type="hidden" name="addonid" id="addonid" value="0">
        <!-- Modal Header SimCardNbr -->
        <div class="modal-header">
          <h4 class="modal-title">
            <span class="paketname text-danger"></span>
          </h4>
          <button type="button" class="close" data-dismiss="modal">&times;
          </button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="disaling">
              </div>
              <div class="form-group autorenewmonth"> <label for="">
                  <?php echo lang('Auto renew for '); ?>
                  (<?php echo lang('Month'); ?> 1
                  month = 30 days) from it current next renewal</label>
                <select class="form-control" id="contractduration" name="ContractDuration">
                  <option value="1" selected><?php echo lang("One time"); ?>
                  </option>
                  <option value="3">3 <?php echo lang("Months"); ?>
                  </option>
                  <option value="6">6 <?php echo lang("Months"); ?>
                  </option>
                  <option value="12">12 <?php echo lang("Months"); ?>
                  </option>
                  <option value="24">24 <?php echo lang("Months"); ?>
                  </option>
                  <option value="36">36 <?php echo lang("Months"); ?>
                  </option>

                  <option value="600"> <?php echo lang("Indefinite"); ?>
                  </option>
                </select>
              </div>
              <div class="form-group bundle_loading3" style="display:none;">
                <center>
                  <img
                    src="<?php echo base_url(); ?>assets/img/loader1.gif">
                </center>
              </div>
            </div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="disable_buttn('renew')">
            <?php echo lang('Submit'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="SuspendModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/Teum_Freeze">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <input type="hidden" name="status" value="0">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish do suspend?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>?
                  </label>
                  <input name="msisdn" class="form-control" type="text"
                    value="<?php echo $service->domain; ?>" readonly>
                </fieldset>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label for="Reason">Reason
                </label>
                <textarea class="form-control" id="Reason" name="suspend_reason" rows="4" required></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-secondary" data-dismiss="modal">
            <i class="fa fa-thumbs-down">
            </i>
            <?php echo lang('No'); ?>
          </button>
          <button type="submit" class="btn btn-md btn-primary">
            <i class="fa fa-thumbs-up">
            </i>
            <?php echo lang('Yes'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="UnsuspendModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post"
        action="<?php echo base_url(); ?>admin/subscription/Teum_Unfreeze">
        <input type="hidden" name="SN"
          value="<?php echo $mobile->msisdn_sn; ?>">
        <input type="hidden" name="userid"
          value="<?php echo $service->userid; ?>">
        <input type="hidden" name="serviceid"
          value="<?php echo $service->id; ?>">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <?php echo lang('Do you wish do unsuspend?'); ?>
          </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;
            </span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <fieldset>
                  <label class="control-label" for="msisdn">
                    <?php echo lang('CLI(Number)'); ?>
                  </label>
                  <input name="msisdn" class="form-control" type="text"
                    value="<?php echo $service->domain; ?>" readonly>
                </fieldset>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-md btn-primary">
            <i class="fa fa-thumbs-up">
            </i>
            <?php echo lang('Yes'); ?>
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
 $('#portindone').click(function(){
    $('#portindone').prop('disabled', true);
      var t = confirm('<?php echo lang('Are you sure the portin has been completed?'); ?>');
      if(t){
      $('#portindone').prop('disabled', false);
      $.ajax({
      url: '<?php echo base_url(); ?>admin/subscription/Teum_CompletePortin',
      type: 'post',
      dataType: 'json',
      success: function(data) {

       alert('Msisdn has now set to active');
       window.location.replace("<?php echo base_url();?>admin/subscription/detail/<?php echo $this->uri->segment(4); ?>");
      },
      data: {
        'serviceid': '<?php echo $service->id; ?>'
      }
    });


      }else{

        $('#portindone').prop('disabled', false);
      }
  });
  </script>