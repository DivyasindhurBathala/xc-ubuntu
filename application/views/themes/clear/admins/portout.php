<div class="content-i">
  <div class="content-box">
    <div class="element-wrapper">
      <h6 class="element-header">
        <?php echo lang('Mobile Subscription Requests'); ?>
      </h6>
      <div class="element-box">
        <h5 class="form-header">
            <?php echo lang('Pending Port Out Requests'); ?>
        </h5>
        <div class="table-responsive">
          <div class="row">
            <div class="col-md-12">
              <table class="table table-striped table-bordered" id="cdr">
                <thead>
                  <tr class="active">
                    <th>
                        <?php echo lang('AccountNumber'); ?>
                    </th>
                    <th>
                        <?php echo lang('RequestType'); ?>
                    </th>
                    <th>
                        <?php echo lang('DonorOperator'); ?>
                    </th>
                    <th>
                        <?php echo lang('MSISDN'); ?>
                    </th>
                    <th>
                        <?php echo lang('Date Requested'); ?>
                    </th>
                    <th>
                        <?php echo lang('CustomerName'); ?>
                    </th>
                    <th>
                        <?php echo lang('Status'); ?>
                    </th>
                    <th>
                        <?php echo lang('button'); ?>
                    </th>
                  </tr>
                </thead>

              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#cdr').DataTable({
"autoWidth": false,
"ajax": {
"url": window.location.protocol + '//' + window.location.host + '/admin/subscription/get_pending_port_out',
"dataSrc": ""
},
 "language": {
    "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
"aaSorting": [[0, 'desc']],
"columns": [
  { mData: 'AccountNumber' } ,
              { mData: 'RequestType' },
              { mData: 'DonorOperator' },
              { mData: 'MSISDN' },
              { mData: 'ActionDate' },
              { mData: 'CustomerName' },
              { mData: 'Status' },
              { mData: 'button' }
],
"language": {
"url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
},
"fnRowCallback": function( nRow, aData, iDisplayIndex ) {
if(aData["TrafficTypeId"] == "0"){
$('td:eq(3)', nRow).html('-');
}
if(aData["TrafficTypeId"] == 1){
$('td:eq(4)', nRow).html('<strong>Voice</strong>');
}else if(aData["TrafficTypeId"] == 2){
$('td:eq(4)', nRow).html('<strong>Data</strong>');
}else if(aData["TrafficTypeId"] == 5){
$('td:eq(4)', nRow).html('<strong>SMS</strong>');
}
return nRow;
},
});
});
});
</script>
<script>
  function OpenAccept(sn){
$('#Accept').modal('toggle');
$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getParameterCLI/'+sn,
dataType: 'json',
success: function(data) {
  console.log(data);
  $('#Sn').val(sn);
  $('#portingid').val(data.PortinID);
  $('#pickdate6').val(data.RequestDate);
  $('.help').html('<span class="text-danger">New Provider request date is :'+data.RequestDate+',  if the date is less than 120 days of your requirement, you must accepted then set your first possible date above</span>');
  $('#Msisdn').val(data.Msisdn);
},
error: function(errorThrown) {
console.log(errorThrown);
}
});
}

  function OpenReject(sn){


$.ajax({
url: '<?php echo base_url(); ?>admin/complete/getParameterCLI/'+sn,
dataType: 'json',
success: function(data) {
   $('#RejectMe').modal('toggle');
  $('#xSn').val(sn);
  $('#xportingid').val(data.PortinID);
  $('#pickdate7').val(data.RequestDate);
  $('.xhelp').html('<span class="text-danger">New Provider request date is :'+data.RequestDate+',  if the date is less than 120 days of your requirement, you must accepted then set your first possible date above</span>');
  $('#xMsisdn').val(data.Msisdn);
},
error: function(errorThrown) {
console.log(errorThrown);
}
});

}

function Accept(){
  $('.tolx').prop('disabled', true);
  var PortingId = $('#portingid').val();
  var FirstPossibleDate = $('#pickdate6').val();
  var Sn = $('#Sn').val();
$.ajax({
url: '<?php echo base_url(); ?>admin/subscription/process_porting/'+Sn,
dataType: 'json',
type: 'POST',
data: {
  PortingId: PortingId,
  FirstPossibleDate: FirstPossibleDate,
  Type: 'Accept'
},
success: function(data) {
  console.log(data);
  $('.acceptmodal').modal('toggle');
  if(data.result){
window.location.href = window.location.protocol + '//' + window.location.host +"/admin/subscription/pending_porting_out";
}else{
  alert(data.message);
}
},
error: function(errorThrown) {
console.log(errorThrown);
}
});

}

function Reject(){
  //$('.tolx').prop('disabled', true);
  var PortingId = $('#xportingid').val();
  var FirstPossibleDate = $('#pickdate7').val();
  var Sn = $('#xSn').val();
  var RejectId = $('#xRejectId').find(":selected").val();
  var Reason = $('#xReason').val();

  if(RejectId == 0){
    alert('You must choose the Reject reason');
  }else{
    if(RejectId == 99){
      if(Reason == ""){
        alert("If you choose Other then please specify the reason on description box");
  return;

      }
    }else if(RejectId == 63){
if(FirstPossibleDate == ""){
  alert("You must pick first possible date");
  return;
}
}
    $.ajax({
url: '<?php echo base_url(); ?>admin/subscription/process_porting/'+Sn,
dataType: 'json',
type: 'POST',
data: {
  PortingId: PortingId,
  RejectId: RejectId,
  FirstPossibleDate: FirstPossibleDate,
  Type: 'Reject',
  Sn: Sn

},
success: function(data) {
  console.log(data);
  $('.rejectmodal').modal('toggle');
if(data.result){
  window.location.href = window.location.protocol + '//' + window.location.host +"/admin/subscription/pending_porting_out";
}else{
  alert(data.message);
}
},
error: function(errorThrown) {
console.log(errorThrown);
}
});




  }

}
</script>
<script>
$( document ).ready(function() {
  console.log('ready');
  var RejectId = $('#RejectId').find(":selected").val();
  console.log(RejectId);
  if(RejectId == 99){
 $('.reason').show('2000');

  }
  $('#RejectId').change(function() {
  var RejectId = $('#RejectId').find(":selected").val();
  console.log(RejectId);
  if(RejectId == 99){
  $('.reason').show('2000');

  }
});
});
</script>

<div class="modal" id="RejectMe">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Send Rejection for this request</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
<form id="rejectform">
      <!-- Modal body -->
      <div class="modal-body">
          <div class="form-group">
            <label class="label-control">
                <?php echo lang('MSISDN'); ?></label>
            <input type="text" value="" class="form-control" id="xMsisdn" name="Msisdn" readonly>
          </div>
          <div class="form-group">
            <label class="label-control">
                <?php echo lang('Porting ID'); ?></label>
            <input type="text" value="" class="form-control" id="xportingid" name="PortingId" readonly>
          </div>
          <div class="form-group">
            <label class="label-control">
                <?php echo lang('Reject Reason'); ?></label>
            <select name="RejectId" id="xRejectId" class="form-control">
              <option value="0">
                <?php echo lang('Select Reason If you wish to reject '); ?>
              </option>
              <option value="1">
                <?php echo lang('Porting ID is not valid '); ?>
              </option>
              <option value="2">
                <?php echo lang('Porting request for this/these number(s) is already in progress '); ?>
              </option>
              <option value="27">
                <?php echo lang('Telephone number and customer ID do not correspond, or Customer ID is required but not provided '); ?>
              </option>
              <option value="41">
                <?php echo lang('Multiple number types are not allowed '); ?>
              </option>
              <option value="44">
                <?php echo lang('Multiple DNOs or DSPs not allowed '); ?>
              </option>
              <option value="45">
                <?php echo lang('Telephone number is not portable '); ?>
              </option>
              <option value="63">
                <?php echo lang('Porting not possible within porting window '); ?>
              </option>
              <option value="99">
                <?php echo lang('Other '); ?>
              </option>
            </select>
          </div>

          <div class="form-group reason" style="display:none;">
            <label class="label-control">
                <?php echo lang('Description'); ?></label>
            <input type="text" value="" class="form-control" id="xReason" name="Reason">

          </div>


          <div class="form-group">
            <label class="label-control">
                <?php echo lang('First Possible Date'); ?></label>
            <input type="text" value="" class="form-control" id="pickdate7" name="FirstPossibleDate" readonly>
            <h6 class="xhelp"></h6>
          </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" onclick="Reject()">Reject</button>
      </div>
       <input type="hidden" value="" class="form-control" id="xSn" name="Sn">
      <input type="hidden" value="" class="form-control" id="xserviceid" name="serviceid">
<form>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="Accept" class="modal fade acceptmodal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <form id="portingport">
        <div class="modal-header">

          <h4 class="modal-title">
            <?php echo lang('Accept this portout'); ?>
          </h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="label-control">
                <?php echo lang('MSISDN'); ?></label>
            <input type="text" value="" class="form-control" id="Msisdn" name="Msisdn" readonly>
          </div>
          <div class="form-group">
            <label class="label-control">
                <?php echo lang('Porting ID'); ?></label>
            <input type="text" value="" class="form-control" id="portingid" name="PortingId" readonly>
          </div>
          <!--
          <div class="form-group">
            <label class="label-control">
                <?php echo lang('Reject Reason'); ?></label>
            <select name="RejectId" id="RejectId" class="form-control">
              <option value="0">
                <?php echo lang('Select Reason If you wish to reject '); ?>
              </option>
              <option value="1">
                <?php echo lang('Porting ID is not valid '); ?>
              </option>
              <option value="2">
                <?php echo lang('Porting request for this/these number(s) is already in progress '); ?>
              </option>
              <option value="27">
                <?php echo lang('Telephone number and customer ID do not correspond, or Customer ID is required but not provided '); ?>
              </option>
              <option value="41">
                <?php echo lang('Multiple number types are not allowed '); ?>
              </option>
              <option value="44">
                <?php echo lang('Multiple DNOs or DSPs not allowed '); ?>
              </option>
              <option value="45">
                <?php echo lang('Telephone number is not portable '); ?>
              </option>
              <option value="63">
                <?php echo lang('Porting not possible within porting window '); ?>
              </option>
              <option value="99">
                <?php echo lang('Other '); ?>
              </option>
            </select>
          </div>

        -->

          <div class="form-group reason" style="display:none;">
            <label class="label-control">
                <?php echo lang('Description'); ?></label>
            <input type="text" value="" class="form-control" id="Reason" name="Reason">

          </div>


          <div class="form-group">
            <label class="label-control">
                <?php echo lang('First Possible Date'); ?></label>
            <input type="text" value="" class="form-control" id="pickdate6" name="FirstPossibleDate" readonly>
            <h6 class="help"></h6>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-md btn-primary tolx" onclick="Accept()">
                <?php echo lang('Accept'); ?></button>
           <!-- <button type="button" class="btn btn-md btn-danger tolx" onclick="Reject()">
                <?php echo lang('Reject'); ?></button>
              -->
          </div>
          <input type="hidden" value="" class="form-control" id="Sn" name="Sn">
          <input type="hidden" value="" class="form-control" id="serviceid" name="serviceid">
      </form>
    </div>

  </div>
</div>



<div id="RejectMex" class="modal fade rejectmodal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form id="portingport">
        <div class="modal-header">

          <h4 class="modal-title">
            <?php echo lang('Process this Portout'); ?>
          </h4>
        </div>
        <div class="modal-body">
        
          <div class="modal-footer">
          <!--  <button type="button" class="btn btn-md btn-primary tolx" onclick="Accept()">
                <?php echo lang('Accept'); ?></button> -->
            <button type="button" class="btn btn-md btn-danger tolx" onclick="Reject()">
                <?php echo lang('Reject'); ?></button>
          </div>
          <input type="hidden" value="" class="form-control" id="xSn" name="Sn">
          <input type="hidden" value="" class="form-control" id="xserviceid" name="serviceid">
      </form>
    </div>

  </div>
</div>



