<div class="content-i">
  <div class="content-box">
    <div class="support-index">
      <div class="support-tickets">
        <div class="support-tickets-header">
          <div class="tickets-control">
            <h5>
            Tickets
            </h5>
            <div class="element-search">
              <input placeholder="Type to filter tickets..." type="text">
            </div>
          </div>
        </div>
        <div id="ticketblock">
          <?php if (!empty($tickets)) {?>
          <?php foreach ($tickets as $row) {?>
          <div id="<?php echo $row->id; ?>" class="support-ticket<?php if ($row->id == $this->uri->segment(4)) {?> active<?php }?>">
            <div class="st-meta">
              <div class="badge badge-warning-inverted">
                <?php echo $row->deptname; ?>
              </div>
              <i class="os-icon os-icon-ui-09"></i>
              <div class="status-pill yellow"></div>
            </div>
            <div class="st-body">
              <div class="avatar">
                <img alt="" src="<?php echo base_url(); ?>assets/clear/client/client.png">
              </div>
              <div class="ticket-content">
                <h6 class="ticket-title">
                <?php echo custom_echo($row->subject, 39); ?>
                </h6>
                <div class="ticket-description">
                  <?php echo custom_echo($row->message, 59); ?>
                </div>
              </div>
            </div>
            <div class="st-foot">
              <span class="label">Customer:</span><span class="value"> <?php echo ucfirst($row->fullname); ?></span> <span class="label">Created on:</span><span class="value"> <?php echo $row->date; ?></span>
            </div>
          </div>
          <?php }?>
          <?php }?>
        </div>
        <div class="load-more-tickets" id="loading">
          <a href="#" onclick="load_more('7');"><span>Load More Tickets...</span></a>
        </div>
      </div>
      <div class="support-ticket-content-w folded-info">
        <div class="support-ticket-content">
          <div class="support-ticket-content-header">
            <h3 class="ticket-header">
            <?php echo $ticket['subject']; ?>
            </h3>
            <a class="back-to-index" href="#"><i class="os-icon os-icon-arrow-left5"></i><span>Back</span></a><a class="show-ticket-info" href="#"><span>Show Details</span><i class="os-icon os-icon-documents-03"></i></a>
          </div>
          <div class="ticket-thread">
            <!-- start replies -->
            <?php if (!empty($ticket['adminid'])) {?>
            <div class="ticket-reply highlight">
              <div class="ticket-reply-info">
                <a class="author with-avatar" href="#"><img alt="" src="<?php echo base_url(); ?>assets/img/staf/<?php echo $this->session->userdata('picture'); ?>"><span><?php echo getAdminName($ticket['adminid']); ?></span></a><span class="info-data"><span class="label">poseted on</span><span class="value"><?php echo date("d F Y H:i:s", changetime($ticket['date'])); ?></span></span><span class="badge badge-success">support agent</span>
                <div class="actions" href="#">
                  <i class="os-icon os-icon-ui-46"></i>
                  <div class="actions-list">
                    <a href="#"><i class="os-icon os-icon-ui-49"></i><span>Edit</span></a><a href="#"><i class="os-icon os-icon-ui-09"></i><span>Mark Private</span></a><a href="#"><i class="os-icon os-icon-ui-03"></i><span>Favorite</span></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i><span>Delete</span></a>
                  </div>
                </div>
              </div>
              <div class="ticket-reply-content">
               <?php echo nl2br($ticket['message']); ?>
              </div>
            </div>
            <?php } else {?>
            <div class="ticket-reply">
              <div class="ticket-reply-info">
                <a class="author with-avatar" href="#"><img alt="" src="<?php echo base_url(); ?>assets/clear/client/client.png"><span><?php echo $ticket['fullname']; ?></span></a><span class="info-data"><span class="label">replied on</span><span class="value"><?php echo date("d F Y H:i:s", changetime($ticket['date'])); ?></span></span>
                <div class="actions" href="#">
                  <i class="os-icon os-icon-ui-46"></i>
                  <div class="actions-list">
                    <a href="#"><i class="os-icon os-icon-ui-49"></i><span>Edit</span></a><a href="#"><i class="os-icon os-icon-ui-09"></i><span>Mark Private</span></a><a href="#"><i class="os-icon os-icon-ui-03"></i><span>Favorite</span></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i><span>Delete</span></a>
                  </div>
                </div>
              </div>
              <div class="ticket-reply-content">
               <?php echo nl2br($ticket['message']); ?>
              </div>
              <div class="ticket-attachments">
                <a class="attachment" href="#"><i class="os-icon os-icon-ui-51"></i><span>Bug Report.xml</span></a><a class="attachment" href="#"><i class="os-icon os-icon-documents-07"></i><span>Sytem Information.txt</span></a>
              </div>
            </div>
            <?php }?>
            <?php if (!empty($ticket['replies'])) {?>
            <?php foreach ($ticket['replies'] as $row) {?>
            <?php if (!empty($row['admin_name'])) {?>
            <div class="ticket-reply highlight">
              <div class="ticket-reply-info">
                <a class="author with-avatar" href="#"><img alt="" src="<?php echo base_url(); ?>assets/img/staf/<?php echo $this->session->userdata('picture'); ?>"><span>Phil Collins</span></a><span class="info-data"><span class="label">replied on</span><span class="value">May 12th, 2017 at 11:23am</span></span><span class="badge badge-success">support agent</span>
                <div class="actions" href="#">
                  <i class="os-icon os-icon-ui-46"></i>
                  <div class="actions-list">
                    <a href="#"><i class="os-icon os-icon-ui-49"></i><span>Edit</span></a><a href="#"><i class="os-icon os-icon-ui-09"></i><span>Mark Private</span></a><a href="#"><i class="os-icon os-icon-ui-03"></i><span>Favorite</span></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i><span>Delete</span></a>
                  </div>
                </div>
              </div>
              <div class="ticket-reply-content">
              <?php echo $row['fullname']; ?>
              </div>
            </div>
            <?php } else {?>
            <div class="ticket-reply">
              <div class="ticket-reply-info">
                <a class="author with-avatar" href="#"><img alt="" src="<?php echo base_url(); ?>assets/clear/client/client.png"><span><?php echo $ticket['fullname']; ?></span></a><span class="info-data"><span class="label">replied on</span><span class="value">May 27th, 2017 at 7:42am</span></span>
                <div class="actions" href="#">
                  <i class="os-icon os-icon-ui-46"></i>
                  <div class="actions-list">
                    <a href="#"><i class="os-icon os-icon-ui-49"></i><span>Edit</span></a><a href="#"><i class="os-icon os-icon-ui-09"></i><span>Mark Private</span></a><a href="#"><i class="os-icon os-icon-ui-03"></i><span>Favorite</span></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i><span>Delete</span></a>
                  </div>
                </div>
              </div>
              <div class="ticket-reply-content">
               <?php echo $row['fullname']; ?>
              </div>
              <div class="ticket-attachments">
                <a class="attachment" href="#"><i class="os-icon os-icon-ui-51"></i><span>Bug Report.xml</span></a><a class="attachment" href="#"><i class="os-icon os-icon-documents-07"></i><span>Sytem Information.txt</span></a>
              </div>
            </div>
            <?php }?>
            <?php }?>
            <?php }?>
            <!-- end replies -->
          </div>
        </div>
        <div class="support-ticket-info">
          <a class="close-ticket-info" href="#"><i class="os-icon os-icon-ui-23"></i></a>
          <div class="customer">
            <div class="avatar">
              <img alt="" src="img/avatar1.jpg">
            </div>
            <h4 class="customer-name">
            <?php echo ucfirst($ticket['fullname']); ?>
            </h4>
            <div class="customer-tickets">
              <?php echo getTotalTickets($ticket['userid']); ?>
            </div>
          </div>
          <h5 class="info-header">
          Ticket Details
          </h5>
          <div class="info-section text-center">
            <div class="label">
              Created:
            </div>
            <div class="value">
              <?php echo $ticket['date']; ?>
            </div>
            <div class="label">
              Department
            </div>
            <div class="value">
              <div class="badge badge-primary">
                <?php echo $ticket['deptname']; ?>
              </div>
            </div>
          </div>
          <h5 class="info-header">
          Agents Assigned
          </h5>
          <div class="info-section">
            <ul class="users-list as-tiles">
              <?php if ($ticket['adminid'] > 0) {?>
              <li>
                <a class="author with-avatar" href="#">
                  <div class="avatar" style="background-image: url(<?php echo base_url() . 'assets/img/staf/' . $_SESSION['picture']; ?>)"></div>
                  <span><?php echo $ticket['staf'] ?></span></a>
                </li>
                <?php }?>
                <li>
                  <a class="add-agent-btn" href="<?php echo base_url(); ?>admin/helpdesk/assigntome/<?php echo $ticket['id']; ?>">
                    <div class="avatar" style="background-image: url(<?php echo base_url() . 'assets/clear/client/staf.png'; ?>)"></div>
                    <span>Assign to me</span></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
    $('document').ready(function() {
    $(".support-ticket").click(function(event) {
    var id =  $(this).attr('id');
    window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/helpdesk/detail/'+id);
    });
    });
    function load_more(ofset){
    //ticketblock
    $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/getLastTickets/',
    type: 'POST',
    data: 'ofset='+ofset,
    success: function(response){
    console.log(response);
    $('#ticketblock').html(response);
    }
    });
    }
    </script>
    <!--
    <div class="content-i">
      <div class="content-box">
        <div class="support-index">
          <div class="support-tickets">
            <div class="support-tickets-header">
              <div class="tickets-control">
                <div class="support-ticket-content-w">
                  <div class="support-ticket-content">
                    <div class="support-ticket-content-header">
                      <h3 class="ticket-header">
                      School Orders
                      </h3>
                      <a class="back-to-index" href="#"><i class="os-icon os-icon-arrow-left5"></i><span>Back</span></a><a class="show-ticket-info" href="#"><span>Show Details</span><i class="os-icon os-icon-documents-03"></i></a>
                    </div>
                    <div class="ticket-thread">
                      <?php if (!empty($ticket['replies'])) {?>
                      <?php foreach ($ticket['replies'] as $row) {?>
                      <?php if (!empty($row['admin_name'])) {?>
                      <div class="ticket-reply highlight">
                        <div class="ticket-reply-info">
                          <a class="author with-avatar" href="#"><img alt="" src="<?php echo base_url(); ?>assets/clear/img/avatar3.jpg"><span><?php echo $row['admin_name']; ?></span></a><span class="info-data"><span class="label">replied on</span><span class="value"><?php echo $row['date']; ?></span></span><span class="badge badge-success">support agent</span>
                          <div class="actions" href="#">
                            <i class="os-icon os-icon-ui-46"></i>
                            <div class="actions-list">
                              <a href="#"><i class="os-icon os-icon-ui-49"></i><span>Edit</span></a><a href="#"><i class="os-icon os-icon-ui-09"></i><span>Mark Private</span></a><a href="#"><i class="os-icon os-icon-ui-03"></i><span>Favorite</span></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i><span>Delete</span></a>
                            </div>
                          </div>
                        </div>
                        <div class="ticket-reply-content">
                          <?php echo nl2br($row['message']); ?>
                        </div>
                      </div>
                      <?php } else {?>
                      <div class="ticket-reply">
                        <div class="ticket-reply-info">
                          <a class="author with-avatar" href="#"><img alt="" src="<?php echo base_url(); ?>assets/clear/img/avatar1.jpg"><span><?php if ($row['userid'] > 0) {?><a href="<?php echo base_url(); ?>admin/client/detail/<?php echo $row['userid']; ?>"><?php }?> <?php echo $row['fullname']; ?><?php if ($row['userid'] > 0) {?></a> <?php }?></span></a><span class="info-data"><span class="label">replied on</span><span class="value"><?php echo $row['date']; ?></span></span>
                          <div class="actions" href="#">
                            <i class="os-icon os-icon-ui-46"></i>
                            <div class="actions-list">
                              <a href="#"><i class="os-icon os-icon-ui-49"></i><span>Edit</span></a><a href="#"><i class="os-icon os-icon-ui-09"></i><span>Mark Private</span></a><a href="#"><i class="os-icon os-icon-ui-03"></i><span>Favorite</span></a><a class="danger" href="#"><i class="os-icon os-icon-ui-15"></i><span>Delete</span></a>
                            </div>
                          </div>
                        </div>
                        <div class="ticket-reply-content">
                          <?php echo nl2br($row['message']); ?>
                        </div>
                        <div class="ticket-attachments">
                          <a class="attachment" href="#"><i class="os-icon os-icon-ui-51"></i><span>Bug Report.xml</span></a><a class="attachment" href="#"><i class="os-icon os-icon-documents-07"></i><span>Sytem Information.txt</span></a>
                        </div>
                      </div>
                      <?php }?>
                      <?php }?>
                      <?php }?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="content-i">
      <div class="content-box">
        <div class="element-wrapper">
          <div class="element-box">
            <div class="table-responsive">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-lg-12">
                    <div class="card">
                      <div class="card-header d-flex align-items-center">
                        <h3 class="h4"><i class="fa fa-life-buoy"></i>Respond ticket</h3>
                      </div>
                      <div class="card-body">
                        <form enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>admin/helpdesk/reply/<?php echo $this->uri->segment(4); ?>">
                          <input type="hidden" name="relid" value="<?php echo $this->uri->segment(4); ?>">
                          <input type="hidden" name="old_deptid" value="<?php echo $ticket['deptid']; ?>">
                          <input type="hidden" name="old_assigne" value="<?php echo $ticket['adminid']; ?>">
                          <fieldset>
                            <div class="form-group">
                              <label for="exampleSelect1">Department</label>
                              <select class="form-control" id="exampleSelect1" name="deptid">
                                <?php foreach (getDepartments() as $dep) {?>
                                <option value="<?php echo $dep['id']; ?>"<?php if ($dep["id"] == $ticket['deptid']) {?> selected<?php }?>><?php echo $dep['name']; ?></option>
                                <?php }?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="exampleSelect1">Change Status</label>
                              <select class="form-control" id="exampleSelect1" name="status">
                                <?php foreach (getStatuses() as $status) {?>
                                <option value="<?php echo $status; ?>"<?php if ($status == "Answered") {?> selected<?php }?>><?php echo $status; ?></option>
                                <?php }?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="exampleSelect1">Assign to</label>
                              <select class="form-control" id="exampleSelect1" name="assigne">
                                <?php foreach (getStaf() as $staf) {?>
                                <option value="<?php echo $staf['id']; ?>"<?php if ($staf['id'] == $ticket['adminid']) {?> selected<?php }?>><?php echo $staf['firstname'] . ' ' . $staf['lastname']; ?></option>
                                <?php }?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="exampleTextarea">Example textarea</label>
                              <textarea class="form-control" id="exampleTextarea" rows="10" name="message" required></textarea>
                            </div>
                            <div class="form-group">
                              <label for="exampleSelect1">Attachment</label>
                              <input type="file" name="attachments[]" class="form-control" multiple="multiple">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                          </fieldset>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    -->
