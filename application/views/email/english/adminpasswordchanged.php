<p></p>
<p>Beste <?php echo $info->name; ?>,</p>
<p>Uw wachtwoord voor <a href="<?php echo base_url(); ?>/client">MijnDelta Mobiel</a> is aangepast.</p>
<p>Heeft u nog vragen of opmerkingen? Kijk op onze website of neem contact op met onze Klantenservice via 0118 225505 of mobiel@delta.nl. Wij helpen u graag verder.</p>
<p></p>
<p><?php echo lang('Do you have any question? look up our'); ?> <a href="http://www.delta.nl/mobile" target="_blank"><?php echo lang('website'); ?></a> <?php echo lang('get in touch with us'); ?></p>
<p><?php echo lang('We are glad to help you'); ?>.</p>
<p><?php echo lang('Kind Regards'); ?>,</p>
<p></p>
<p style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; LINE-HEIGHT: 16px" align="left">
Ludolf Rasterhoff <br><br>Directeur Telecom</p>
