
                                        <tr>
                                            <td>
                                                <p></p>
                                                <p><?php echo lang('Dear'); ?> <?php echo $info->name; ?>,</p>
                                                <p><?php echo lang('Thank you for choosing Delta Mobile'); ?>.</p>
                                                <p><?php echo lang('You can login to our portal at mijnmobiel.delta.nl'); ?></p>
                                                <p><?php echo lang('Please find your login information below'); ?></p>
                                                <p></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                 <table border="0" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr>
                                                            <td align="left">
                                                                <table border="1" cellpadding="5" cellspacing="2">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td width="50%" align="right"><?php echo lang('Username'); ?> </td>
                                                                            <td witdh="50%" align="left"> <?php echo strtolower($info->email); ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="50%" align="right"><?php echo lang('Password'); ?> </td>
                                                                            <td witdh="50%" align="left"> <?php echo strtolower($info->password); ?></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <table border="0" cellpadding="0" cellspacing="0" class="btn btn-success">
                                                                                <tr>
                                                                                    <td> <a href="<?php echo base_url(); ?>client/auth" target="_blank"><?php echo lang('Login'); ?></a></td>
                                                                                </tr>
                                                                            </table>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <p></p>
                                                <p><?php echo lang('Kind Regards'); ?>,</p>
                                                <p><?php echo lang('Delta Customer Service'); ?></p>
                                                <p></p>
                                            </td>
                                        </tr>

