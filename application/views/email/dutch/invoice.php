<p>Beste <?php echo $name; ?>,</p>
<p></p>
<p>Uw factuur over de maand <b><?php echo $month['PERIOD']; ?></b> is beschikbaar op MijnDelta Mobiel.</p>
<p>Het bedrag van </b>€<?php echo number_format($amount, 2); ?></b> zal binnen circa 4 werkdagen van uw rekening nummer worden afgeschreven.<br />
<p></p>
<a class="btn btn-primary" href="<?php echo base_url(); ?>client">naar MijnDelta Mobiel</a>
<p></p>
<p>U heeft PDF software, zoals Adobe Reader, nodig om deze factuur te kunnen openen.</p>
<p>Wij hopen dat alles duidelijk is.</p>
<p>Met vriendelijke groet,</p>
<p></p>
<p style="FONT-SIZE: 14px; FONT-FAMILY: Arial, Helvetica, sans-serif; COLOR: #000000; LINE-HEIGHT: 16px" align="left">
Ludolf Rasterhoff <br><br>Directeur Telecom</p>


