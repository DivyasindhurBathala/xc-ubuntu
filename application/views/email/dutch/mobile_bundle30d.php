<p></p>
<p style="color: #111;">
Beste <?php echo $client->lastname; ?>
</p>
<p></p>
<p style="color: #111;">Geregeld! Uw extra <?php echo $productname; ?> bundel gaat per direct in en is 30 dagen geldig. Hierna stopt de extra bundel automatisch. U hoeft verder dus niets te doen.
</p>
<p style="color: #111;">
Veel plezier met uw extra bundel!</p>
<p></p>
<p style="color: #111;">
Vragen? We helpen u graag. De meeste antwoorden vindt u hier.
</p>
<p></p>
<p style="color: #111;">Vriendelijke groet,<br />
DELTA-klantenservice</p>
