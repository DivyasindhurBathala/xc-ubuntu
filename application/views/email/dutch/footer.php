  </P>


            </TD>
            <TD class=margin_small bgColor=#ffffff
                width=1></TD>
          </TR>
          </TBODY>
        </TABLE>
      </TD>
    </TR>
    </TBODY>
  </TABLE>
</DIV>






<style>@media only screen{html{min-height:100%;background:#ffffff}}@media only screen and (max-width:692px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width:692px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px!important;padding-right:16px!important}table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .columns{padding-left:0!important;padding-right:0!important}th.small-2{display:inline-block!important;width:16.66667%!important}th.small-4{display:inline-block!important;width:33.33333%!important}th.small-6{display:inline-block!important;width:50%!important}th.small-12{display:inline-block!important;width:100%!important}.columns th.small-12{display:block!important;width:100%!important}}</style>
<DIV style="BOX-SIZING: border-box; FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; WIDTH: 100% !important; BACKGROUND: #ffffff; MIN-WIDTH: 100%; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; -MS-TEXT-SIZE-ADJUST: 100%; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%"><SPAN class=preheader style="FONT-SIZE: 1px; OVERFLOW: hidden; MAX-WIDTH: 0px; COLOR: #ffffff; DISPLAY: none !important; LINE-HEIGHT: 1px; MAX-HEIGHT: 0px; VISIBILITY: hidden; mso-hide: all; opacity: 0"></SPAN>
<TABLE class=body style="FONT-SIZE: 16px; HEIGHT: 100%; FONT-FAMILY: Helvetica,Arial,sans-serif; WIDTH: 100%; VERTICAL-ALIGN: top; BACKGROUND: #ffffff; BORDER-COLLAPSE: collapse; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; MARGIN: 0px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px">
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TD class=center style="FONT-SIZE: 16px; WORD-WRAP: break-word; FONT-FAMILY: Helvetica,Arial,sans-serif; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse !important; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px; -moz-hyphens: auto; -webkit-hyphens: auto; hyphens: auto" vAlign=top align=center>
<CENTER style="WIDTH: 100%; MIN-WIDTH: 676px" data-parsed="">
<TABLE class="spacer float-center" style="WIDTH: 100%; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse; FLOAT: none; PADDING-BOTTOM: 0px; TEXT-ALIGN: center; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; MARGIN: 0px auto; PADDING-RIGHT: 0px">
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TD style="FONT-SIZE: 40px; WORD-WRAP: break-word; FONT-FAMILY: Helvetica,Arial,sans-serif; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse !important; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 40px; PADDING-RIGHT: 0px; -moz-hyphens: auto; -webkit-hyphens: auto; hyphens: auto; mso-line-height-rule: exactly" height=40>&nbsp;</TD></TR></TBODY></TABLE>
<TABLE class="wrapper footer_clean float-center" style="WIDTH: 100%; VERTICAL-ALIGN: top; BACKGROUND: 0px 0px; BORDER-COLLAPSE: collapse; FLOAT: none; PADDING-BOTTOM: 0px; TEXT-ALIGN: center; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; MARGIN: 0px auto; PADDING-RIGHT: 0px" align=center>
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TD class=wrapper-inner style="FONT-SIZE: 16px; WORD-WRAP: break-word; FONT-FAMILY: Helvetica,Arial,sans-serif; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse !important; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px; -moz-hyphens: auto; -webkit-hyphens: auto; hyphens: auto">
<TABLE class=spacer style="WIDTH: 100%; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; PADDING-RIGHT: 0px">
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TD style="FONT-SIZE: 40px; WORD-WRAP: break-word; FONT-FAMILY: Helvetica,Arial,sans-serif; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse !important; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 40px; PADDING-RIGHT: 0px; -moz-hyphens: auto; -webkit-hyphens: auto; hyphens: auto; mso-line-height-rule: exactly" height=40>&nbsp;</TD></TR></TBODY></TABLE>
<TABLE class=container style="WIDTH: 676px; VERTICAL-ALIGN: top; BACKGROUND: 0px 0px; BORDER-COLLAPSE: collapse; COLOR: #a8aaa7; PADDING-BOTTOM: 0px; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; MARGIN: 0px auto; PADDING-RIGHT: 0px" align=center>
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TD style="FONT-SIZE: 16px; WORD-WRAP: break-word; FONT-FAMILY: Helvetica,Arial,sans-serif; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse !important; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px; -moz-hyphens: auto; -webkit-hyphens: auto; hyphens: auto">
<HR>

<TABLE class=spacer style="WIDTH: 100%; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; PADDING-RIGHT: 0px">
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TD style="FONT-SIZE: 15px; WORD-WRAP: break-word; FONT-FAMILY: Helvetica,Arial,sans-serif; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse !important; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 15px; PADDING-RIGHT: 0px; -moz-hyphens: auto; -webkit-hyphens: auto; hyphens: auto; mso-line-height-rule: exactly" height=15>&nbsp;</TD></TR></TBODY></TABLE>
<TABLE class=row style="WIDTH: 100%; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse; POSITION: relative; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; DISPLAY: table; PADDING-RIGHT: 0px">
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TH class="small-4 large-4 columns first" style="FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; WIDTH: 209px; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 16px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 16px; MARGIN: 0px auto; LINE-HEIGHT: 1.3; PADDING-RIGHT: 8px">
<TABLE style="WIDTH: 100%; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; PADDING-RIGHT: 0px">
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TH style="FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px">
<P style="FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px 0px 10px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px"><SMALL style="FONT-SIZE: 60%; COLOR: #a8aaa7">© 2018 DELTA Fiber Nederland B.V.</SMALL></P></TH></TR></TBODY></TABLE></TH>
<TH class="small-4 large-4 columns" style="FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; WIDTH: 209px; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 16px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 8px; MARGIN: 0px auto; LINE-HEIGHT: 1.3; PADDING-RIGHT: 8px">
<TABLE style="WIDTH: 100%; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; PADDING-RIGHT: 0px">
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TH style="FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px">
<P class=text-center style="FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: center; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px 0px 10px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px"><SMALL style="FONT-SIZE: 60%; COLOR: #a8aaa7">&nbsp;</SMALL></P></TH></TR></TBODY></TABLE></TH>
<TH class="small-4 large-4 columns last" style="FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; WIDTH: 209px; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 16px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 8px; MARGIN: 0px auto; LINE-HEIGHT: 1.3; PADDING-RIGHT: 16px">
<TABLE style="WIDTH: 100%; VERTICAL-ALIGN: top; BORDER-COLLAPSE: collapse; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; BORDER-SPACING: 0; PADDING-RIGHT: 0px">
<TBODY>
<TR style="VERTICAL-ALIGN: top; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px">
<TH style="FONT-SIZE: 16px; FONT-FAMILY: Helvetica,Arial,sans-serif; FONT-WEIGHT: 400; COLOR: #fefefe; PADDING-BOTTOM: 0px; TEXT-ALIGN: left; PADDING-TOP: 0px; PADDING-LEFT: 0px; MARGIN: 0px; LINE-HEIGHT: 1.3; PADDING-RIGHT: 0px"><IMG class=float-right style="TEXT-DECORATION: none; MAX-WIDTH: 100%; WIDTH: auto; FLOAT: right; OUTLINE-WIDTH: 0px; TEXT-ALIGN: right; OUTLINE-STYLE: none; CLEAR: both; DISPLAY: block; OUTLINE-COLOR: invert; -MS-INTERPOLATION-MODE: bicubic" alt="DELTA logo" src="http://files.delta.nl/email/logo-small.png"></TH></TR></TBODY></TABLE></TH></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></CENTER></TD></TR></TBODY></TABLE><!-- prevent Gmail on iOS font size manipulation -->
<DIV style="WHITE-SPACE: nowrap; FONT: 15px/0 courier; DISPLAY: none">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</DIV></DIV>


</BODY>
</HTML>
