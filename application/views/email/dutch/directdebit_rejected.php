<p></p>
<p style="color: #111;">
Beste <?php echo $client->lastname; ?>
</p>
<p></p>
<p style="color: #111;">U heeft ons gemachtigd de abonnementskosten voor DELTA Mobiel af te schrijven van rekeningnummer <b>NL76 INGB 0008 5195 20</b>. De laatste afschrijving is helaas niet gelukt. Misschien komt dit door onvoldoende saldo op uw rekening. Op dit moment staat er een bedrag open van €  <?php echo $invoice->mInvoiceAmount; ?>,–, betreffende factuurnummer <?php echo $invoice->iInvoiceNbr; ?>.
</p>
<p></p>
<p style="color: #111;">
Blijf bereikbaar door te betalen<br />
Bereikbaar blijven? Betaal uw factuur voor DELTA Mobiel dan op tijd. Zo voorkomt u dat uw abonnement wordt geblokkeerd. Wij doen per factuur 1 incasso-poging. U moet het bedrag nu dus zelf aan ons overmaken.
</p>
<p></p>
<p style="color: #111;">Maak het bedrag binnen 14 dagen over op rekeningnummer <b>NL76 INGB 0008 5195 20</b> onder vermelding van “DELTA Mobiel en uw relatienummer <?php echo $client->mvno_id; ?>”.</p>
<p></p>
<p style="color: #111;">Heeft u al betaald? Fijn! U kunt deze herinnering dan als niet verzonden beschouwen.</p>
<p style="color: #111;">
Controleer uw gegevens<br/>
<p style="color: #111;">In MijnDELTA Mobiel ziet u van welk rekeningnummer wij incasseren. Heeft u daar geen toegang toe of wilt u het rekeningnummer wijzigen? Neem dan contact op met de DELTA-klantenservice. Klopt het rekeningnummer en had u voldoende saldo? Neem dan contact op met uw bank.</p>
<p></p>
<p style="color: #111;">
Vriendelijke groet,<br />
DELTA-klantenservice</p>
