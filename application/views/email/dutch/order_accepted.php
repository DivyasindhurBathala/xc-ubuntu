<p></p>
<p style="color: #111;">Beste <?php echo $client->lastname; ?>,
<p></p>
<p style="color: #111;">
Welkom bij DELTA Mobiel!
</p>
<p></p>
<?php if ($combi == "1") {?>
<p style="color: #111;">U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. U ontvangt daarom elke maand de volgende gratis extra’s:
<ul>
<li>Dubbele data en Onbeperkt bellen met je mobiel</li>
<li>€ 5,– korting op uw DELTA Internet factuur</li>
<li>25 Mbps extra downloadsnelheid (tot maximaal 400Mbps).</li>
<li>Een extra TV-pakket boordevol sport-, muziek-, entertainment-, en jeugdzenders</li>
</ul>
</p>
<p></p>
<?php } elseif ($combi == "2") {?>
<p style="color: #111;">
U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. Vanaf nu ontvangt u daarom thuis 25 Mbps extra downloadsnelheid (tot maximaal 400 Mbps). Nog sneller streamen, downloaden en gamen dus!</p>
<?php } elseif ($combi == "0") {?>
<p style="color: #111;">We zijn erg blij met u als klant. Daarom krijgt u als welkomstcadeau een halfjaar € 5,– maandelijkse korting op uw mobiele abonnement. U hoeft daar verder niets voor te doen.</p>
<?php }?>
<p></p>
<p style="color: #111;">MijnDELTA Mobiel<br />
Op MijnDELTA Mobiel beheert u eenvoudig uw abonnement. Ook vindt u hier facturen en informatie over uw huidige verbruik. Inloggen doet u met onderstaande knop en inloggegevens.
</p><p></p>
<p style="color: #111;">Let op: U moet na de eerste keer inloggen het onderstaande wachtwoord veranderen in een zelfgekozen wachtwoord. Wel zo veilig.</p>
<p></p>
<p style="color: #111;">
Login: 		<?php echo $client->email; ?><br />
Wachtwoord:	<?php echo $password; ?>
</p>
<table border="0.5" cellspacing="1" cellpadding="2" >
<tbody>
<tr bgcolor="#eaa309">
<td> </td>
<td><p><b><a style="text-decoration: none; color: #fff;" href="https://mijnmobiel.delta.nl/client/auth">naar MijnDelta Mobiel</a></b></p></td>
<td> </td>
</tr>
</tbody>
</table>
<p></p>
<p style="color: #111;">
Vragen? We helpen u graag. De meeste antwoorden vindt u hier.
</p>
<p></p>
<p style="color: #111;">Veel plezier met DELTA Mobiel!</p>
<p></p>
<p style="color: #111;">
Vriendelijke groet,<br />
DELTA-klantenservice</p>
