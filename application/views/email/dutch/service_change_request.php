<p></p>
<p style="color: #111;">Beste <?php echo $client->lastname; ?>,
<p></p>
<?php if (in_array($combi_changes, array(2, 1))) {?>
<p style="color: #111;">Uw DELTA Internet abonnement is gewijzigd. Dit betekent dat uw abonnement op DELTA Mobiel ook veranderd. Vanaf de eerste van de volgende maand wordt uw abonnement aangepast naar <?php echo $new_bundle; ?></p>
<?php } else {?>
<p style="color: #111;">We hebben de aanvraag voor uw abonnementswijziging naar <?php echo $new_bundle; ?> verwerkt. Uw nieuwe abonnement gaat in op de eerste dag van de volgende maand.</p>
<?php }?>

<p></p>
<p style="color: #111;">Vragen? We helpen u graag. De meeste antwoorden vindt u hier.</p>
<p></p>
<p style="color: #111;">Vriendelijke groet,</p>
<p style="color: #111;">DELTA-klantenservice</p>
