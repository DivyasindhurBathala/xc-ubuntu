<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
</head>
<body>

 <h2>Create child company for #<?php echo $this->session->cid ?></h2> 

<form method="post">
  
  <table width="600" border="1" cellspacing="5" cellpadding="5">
  <tr>
    <td width="230">Company Id</td>
    <td width="329"><input type="text" name="company_id"/></td>
  </tr>
  <tr>
    <td>Company Name </td>
    <td><input type="text" name="company_name"/></td>
  </tr>
  <tr>
    <td>Parent Company Id</td>
    <td><input type="text" name="parent_companyid"/></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input type="submit" name="save" value="Create Company"/></td>
  </tr>
</table>
 
</form>

<a href='<?php echo base_url()."admin/Company/showcompany"; ?>'><button>Back</button></a>  

</body>
</html>