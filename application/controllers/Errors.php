<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ErrorController extends CI_Controller {

	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		if (!empty($this->session->language)) {
			$this->config->set_item('language', $this->session->language);
		} else {
			$this->config->set_item('language', 'dutch');
		}
		$this->lang->load('admin');
		$this->setProperty();
	}

	public function setProperty() {
		$this->data['setting'] = globo();
	}

}
class Errors extends ErrorController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *     http://example.com/index.php/welcome
	 *  - or -
	 *     http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
		//$this->load->library('user_agent');
		$this->companyid = get_companyidby_url(base_url());

		$this->data['setting'] = globofix($this->companyid);
		$this->data['url'] = $this->data['setting']->companyname;
		
	}

	public function access_denied() {
		$this->data['title'] = "Access Denied";
		//$this->data['main_content'] =  'errors/html/error_general';
		$this->load->view('errors/html/error_general', $this->data);
		
	}

	public function not_found() {
		$this->data['title'] = "Page Not Found";
		//$this->data['main_content'] = 'errors/html/error_404';
		$this->load->view('errors/html/error_404', $this->data);
	}

	public function exceptions() {
		$this->data['title'] = "SERVER Internal Error";
		//$this->data['main_content'] = 'errors/html/error_php';
		$this->load->view('errors/html/error_exception', $this->data);
	}

}