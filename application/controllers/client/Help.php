<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HelpController extends CI_Controller {

	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		$this->config->set_item('language', $_SESSION['client']['language']);
		$this->lang->load('client');
		$this->companyid = get_companyidby_url(base_url());

		$this->data['setting'] = globofix($this->companyid);
	}

}
class Help extends HelpController {

	public function __construct() {
		parent::__construct();
		$this->load->model('Auth_model');

	}
	public function important_numbers() {

		$this->data['title'] = "Important Numbers";
		$this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/help_important_numbers';
		$this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/content', $this->data);

	}

}