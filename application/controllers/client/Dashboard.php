<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ClientDashboardController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', $this->session->client['language']);
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }
}
class Dashboard extends ClientDashboardController
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Admin_model');
        $this->load->model('Client_model');
        $this->load->model('Teams_model');
         if($this->session->email == "simson.parlindungan@united-telecom.be"){
           // $this->output->enable_profiler(TRUE);
        }
    }

    public function index()
    {

        if ($this->session->email == "simson.parlindungan@united-telecom.be") {
        }


        $this->data['consumption'] = '0.00';
        $this->load->library('xml');
       // $usage = array('0.00');
        $this->data['activemobiles'] = getMobileActive($this->companyid, $_SESSION['client']['id']);
        //print_r($this->data);
        if (empty($this->data['activemobiles'])) {
            $this->data['showusage'] = false;
        } else {
        //$service = $this->Admin_model->getService($_POST['serviceid']);
        if($this->data['setting']->mobile_platform =="ARTA"){
             $this->load->library('artilium', array('companyid' => $this->companyid));
             $f = array();


            foreach ($this->data['activemobiles'] as $IN => $order) {
                if ($order['domainstatus'] == 'Active') {
                    $this->data["service" . $IN] = $this->Admin_model->getService($order['id']);

                    $mobile = $this->data["service" . $IN]->details;

                    $f[] = array('domain' => $mobile->msisdn, 'sn' => $mobile->msisdn_sn, 'mobile' => $mobile);
                    $sns[] = $order['sn'];


                }
            }
            if($sns){
                $this->data['consumption'] =  $this->artilium->getOutofBundleusagePostpaid(array_filter($sns));
            }
            $this->data['bundles'] = $this->artilium->GetBundleAssignList1($f['0']['sn']);
        }

            $this->data['showusage'] = true;

                }
        $this->data['proforma'] = $this->Client_model->getProforma($_SESSION['client']['id']);
        if ($this->session->master) {
            //print_r($this->data['proforma']);
        }
        $this->data['stats'] = $this->Client_model->getStats($_SESSION['client']['id']);
        $this->data['title'] = $this->data['setting']->companyname;
        $this->data['dtt'] = true;
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/dashboard';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/content', $this->data);
    }

    public function getusage($id)
    {

        $this->load->model('Admin_model');
        $this->data['service'] = $this->Admin_model->getService($id);
        //echo $this->data['service']->domain;
        $this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . trim($this->data['service']->domain));

        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
        $this->data['sn'] = $this->Admin_model->artiliumGet('GetSn/' . trim($this->data['service']->domain), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);
        $cdr = $this->get_cdr($this->data['sn']->SN, $this->data['service']->domain, $this->data['mobile']);

        return array('voice' => second2hms($cdr['voice']), 'data' => round($cdr['data'] / 1048576, 2), 'sms' => $cdr['sms']);
    }

    public function getusagev2()
    {
        $id = $_POST['id'];

        $service = $this->Admin_model->getService($id);
        $this->load->library('artilium', array('companyid' => $this->companyid));
        $bundles = $this->artilium->GetBundleAssignList1($service->details->msisdn_sn);

        if ($bundles) {
            $html = '';
            foreach ($bundles as $index => $row) {
                $html .= '<div class=" col-md-4">
            <div class="card bg-default">
               <div class="card-header text-center text-light bg-primary">' . $row->szBundle . ' </div>
              <div class="card-body text-dark text-center"><h5>
              ' . str_replace('.', ',', $row->UsedValue) . ' / ' . str_replace('.', ',', $row->AssignedValue) . '</h5>';
                if (strpos($row->szBundle, '30 dagen') || strpos($row->szBundle, '30 days')) {
                    $html .= lang('ValidUntil') . ' ' . $row->ValidUntil . '</h6>';
                }

                $html .= '</div>
            </div>
          </div>';
                if ($index == 3) {
                    $html .= ' <hr />';
                }
            }
        }

        echo json_encode(array('html' => $html));
    }
    public function link2delta()
    {
        //print_r($_POST);
        if (validate_deltausername(strtolower($_POST['username']))) {
            $this->Client_model->insertDeltaUsername($_POST);
            header('Location: ' . base_url() . 'sso.php?login=true');
            exit;
        } else {
            $this->session->set_flashdata('error', lang('Sorry there is already customer assigned with this delta username : ' . $_POST['deltausername']));
            redirect('client');
        }
    }

    public function stopasking()
    {
        $res = $this->Client_model->doNotAskSSO($_POST['userid']);
        $_SESSION['client']['sso'] = 1;
        echo json_encode(array('result' => $res));
    }
    public function pull_usage()
    {

        echo json_encode($this->getusage($_POST['id']));
    }
    public function pull_usagev2()
    {

        echo json_encode($this->getusagev2($_POST['id']));
    }
    public function get_lang()
    {

        echo json_encode(array('result' => ucfirst($_SESSION['client']['language'])));
    }

    public function chart()
    {

        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . 'chart', $this->data);
    }

    public function switchlang()
    {
        $lang = $this->uri->segment(4);
        $_SESSION['client']['language'] = $lang;
        $ref = $this->agent->referrer();
        $this->config->set_item('language', $lang);
        $this->lang->load('client');
        $this->session->set_flashdata('success', lang('Your Language has been changed to') . ' ' . lang(ucfirst($lang)));
        header('Location: ' . $ref);
    }
    public function myaccount()
    {
        if (isPost()) {
            $post = $_POST;
            $this->db = $this->load->database('default', true);
            $this->db->where('id', $_SESSION['client']['id']);
            $this->db->update('a_clients', $post);

            if ($this->db->affected_rows() > 0) {
                $this->load->library('magebo', array('companyid' => $this->session->cid));
                $cc = $this->db->query("select * from a_clients where id=?", array($_SESSION['client']['id']));
                $this->magebo->updateClient($d->mageboid, $cc->row_array(), $this->session->cid);
                $_SESSION['client'] = $post;
                $_SESSION['client']['islogged'] = true;
                $this->session->set_flashdata('success', lang('Your information has been saved'));
            }
        }

        $this->data['title'] = $this->data['setting']->companyname;
        $this->data['client'] = $this->Admin_model->getClient($_SESSION['client']['id']);
        $this->data['title'] = "Caiway Dasboard";
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/myaccount';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/content', $this->data);
    }
    public function get_cdr($sn, $msisdn, $mobile)
    {
        $total = 0;
        $bytes = array();
        $this->load->model('Admin_model');

        $cdr = array();

        //print_r($this->data['mobile']);
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
        //echo $msisdn . "<br />";
        //print_r($mobile);
        //$result = $this->artilium->GetCDRList($sn);
        $params = array('companyid' => $this->companyid,
            'action' => 'GetCDRList',
            'type' => 'POST PAID',
            'SN' => trim($sn),
            'From' => date('Y-m-01\T00:00:00+02:00'),
            'Till' => date('Y-m-d\TH:i:s'),
            'PageIndex' => 0,
            'PageSize' => 2500,
            'SortBy' => 0,
            'SortOrder' => 0);

        $sms = array();
        $voice = array();
        $bytes = array();

        $result = $this->Admin_model->artiliumPost($params);

        foreach ($result->ListInfo as $row) {
            if ($row->Cause != "1000") {
                if ($row->TypeCallId != 5) {
                    if ($row->MaskDestination != "72436") {
                        if (!empty($row->DestinationCountry)) {
                            $country = $row->DestinationCountry;
                        } else {
                            $country = "";
                        }

                        if ($row->TrafficTypeId == 2) {
                            $total = $total + includevat('21', $row->CurNumUnitsUsed);

                        } else {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                            } else {

                                    $total = $total + includevat('21', $row->CurNumUnitsUsed);

                            }
                        }
                    }
                }
            }

            if ($row->TypeCallId != 5) {
                if (!in_array($row->TrafficTypeId, array(2, 3))) {
                    if (strlen($row->MaskDestination) > 9) {
                        if ($row->TrafficTypeId) {
                            if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {
                            } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {
                            } else {
                                if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
                                    $voice[] = $row->DurationConnection;
                                } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
                                    $sms[] = $row->DurationConnection;
                                }
                            }
                        }
                    }
                } elseif ($row->TrafficTypeId == 2) {
                    $bytes[] = $row->DurationConnection;
                }
            }
        }

        if (!$sms) {
            $smsi = '0';
        } else {
            $smsi = array_sum($sms);
        }
        return array('data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => $smsi, 'total' => $total);

        //return array('cdr' => $cdr, 'data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => array_sum($sms));
    }

    public function changepassword()
    {
        $this->data['dtt'] = 'clientarea_services.js?version=1.0';
        if (isPost()) {
            $client = $_SESSION['client'];
            $this->Teams_model->send_msg($this->uri->segment(4), "<br /><br /> CLIENT:<br />" . json_encode($client));
            $this->Teams_model->send_msg($this->uri->segment(4), "<br /><br /> POST:<br />" . $this->Client_model->getPassword($client['id']));
            if (password_verify(trim($_POST['currentpassword']), $this->Client_model->getPassword($client['id']))) {
                if (trim($_POST['password1']) == trim($_POST['password2'])) {
                    //$this->load->database('default', true);
                    $this->Client_model->ChangePassword($client['id'], $_POST);
                    $this->session->set_flashdata('success', lang('Your new password has been set successfully, please') . ' <a href="javascript:voice(0)" onclick="confirmation_logout();">' . lang('logout') . '</a> ' . lang('and try your new password'));

                    if (!empty($_POST['first'])) {
                        logClient(array('companyid' => $this->companyid,
                            'description' => 'Customer change password',
                            'user' => $_SESSION['client']['firstname'] . ' ' . $_SESSION['client']['lastname'],
                            'userid' => $_SESSION['client']['id'],
                            'serviceid' => null,
                            'ip' => $_SERVER['REMOTE_ADDR']));


                    }
                     $this->Client_model->RemoveFirstlogin($client['id']);
                    redirect('client');
                } else {
                    $this->session->set_flashdata('error', lang('Your new password does not match with confirmation password'));
                }
            } else {
                $this->session->set_flashdata('error', lang('Your current password is incorrect'));
            }
        }
        $this->data['client'] = (object) $_SESSION['client'];
        $this->data['title'] = "Change Password";
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/changepassword';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/' . $this->session->cid . '/content', $this->data);
    }

    public function export_livecdr($sn, $msisdn)
    {
        $total = 0;
        set_time_limit(0);

/*
<th><?php echo lang('Date'); ?></th>
<th><?php echo lang('Destination'); ?></th>
<th><?php echo lang('Number'); ?></th>
<th><?php echo lang('Duration'); ?></th>
<th><?php echo lang('Type'); ?></th>
<th><?php echo lang('Cost'); ?></th>

 */
        $bytes = array();
        $this->load->model('Admin_model');

        $cdr = array();
        $this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . $msisdn);

        $sms = array();
        $voice = array();
        $bytes = array();
        $req = array('action' => 'GetCDRList',
            'type' => 'POST PAID',
            'From' => date('Y-m-01\TH:i:s+02:00'),
            'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
            'SN' => $sn,
            'PageIndex' => 0,
            'PageSize' => 5000,
            'SortBy' => 0,
            'SortOrder' => 0,
            'companyid' => $this->data['setting']->companyid);
        $result = $this->Admin_model->artiliumPost($req);
        $header = array(
            lang('Begintime') => 'string',
            lang('Type') => 'string',
            lang('Destination') => 'string',
            lang('Duration') => 'string',

            lang('Cost') => 'string',

        );
        foreach ($result->ListInfo as $row) {
            if ($row->Cause != "1000") {
                if ($row->TypeCallId != 5) {
                    if ($row->MaskDestination != "72436") {
                        if (!empty($row->DestinationCountry)) {
                            $country = $row->DestinationCountry;
                        } else {
                            $country = "";
                        }

                        if ($row->TrafficTypeId == 2) {
                            $total = $total + includevat('21', $row->CurNumUnitsUsed);
                            $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => str_replace('.', ',', round($row->DurationConnection / 1048576, 2)) . 'MB', 'CurNumUnitsUsed' => $this->data['setting']->currency.' ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                        } else {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                            } else {
                                if ($row->TypeCallId != "3") {
                                    $total = $total + includevat('21', $row->CurNumUnitsUsed);
                                    $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'CurNumUnitsUsed' => $this->data['setting']->currency.' ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                                }
                            }
                        }
                    }
                }
            }
        }

        return $total;
    }

    public function get_cdr_amount($sn, $msisdn, $mobile)
    {
        $total = 0;
        $bytes = array();
        $this->load->model('Admin_model');

        $cdr = array();
        $this->data['mobile'] = $mobile;
        //print_r($this->data['mobile']);
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));

        //$result = $this->artilium->GetCDRList($sn);
        $sms = array();
        $voice = array();
        $bytes = array();
        /*
        $req = array('action' => 'GetCDRList',
        'type' => $this->data['mobile']->PaymentType,
        'From' => date('Y-m-01\T00:00:00'),
        'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
        'SN' => $sn,
        'PageIndex' => 0,
        'PageSize' => 2500,
        'SortBy' => 0,
        'SortOrder' => 0,
        'companyid' => $this->data['setting']->companyid);
        $result = $this->Admin_model->artiliumPost($req);
         */
        $params = array('companyid' => $this->data['setting']->companyid,
            'action' => 'GetCDRList',
            'type' => $mobile->PaymentType,
            'SN' => $sn,
            'From' => date('Y-m-01\T00:00:00+02:00'),
            'Till' => date('Y-m-d\TH:i:s'),
            'PageIndex' => 0,
            'PageSize' => 2500,
            'SortBy' => 0,
            'SortOrder' => 0);

        $sms = array();
        $voice = array();
        $bytes = array();

        $result = $this->Admin_model->artiliumPost($params);

        //print_r($result);
        //exit;
        foreach ($result->ListInfo as $row) {
            if (empty($row->DestinationCountry)) {
                $country = "";
            } else {
                $country = $row->DestinationCountry;
            }
            if ($row->Cause != "1000") {
                if ($row->TypeCallId != "5") {
                    if ($row->MaskDestination != "72436") {
                        if ($row->TrafficTypeId == "2") {
                            $total = $total + includevat('21', $row->CurNumUnitsUsed);
                            $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $country, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => round($row->DurationConnection / 1048576, 2) . 'MB', 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => $this->data['setting']->currency.' ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                        } else {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                            } else {
                                if ($row->TypeCallId != "3") {
                                    $total = $total + includevat('21', $row->CurNumUnitsUsed);
                                    $cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $country, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => $this->data['setting']->currency.' ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
                                }
                            }
                        }
                    }
                }
            }

            if ($row->TypeCallId != 5) {
                if (!in_array($row->TrafficTypeId, array(2, 3))) {
                    if (strlen($row->MaskDestination) > 9) {
                        if ($row->TrafficTypeId) {
                            if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {
                            } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {
                            } else {
                                if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
                                    $voice[] = $row->DurationConnection;
                                } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
                                    $sms[] = $row->DurationConnection;
                                }
                            }
                        }
                    }
                } elseif ($row->TrafficTypeId == 2) {
                    $bytes[] = $row->DurationConnection;
                }
            }
        }

        if (!$sms) {
            $smsi = '0';
        } else {
            $smsi = array_sum($sms);
        }
        return array('cdr' => $cdr, 'data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => $smsi, 'usage' => $total);
        //return array('cdr' => $cdr, 'data' => array_sum($bytes));
    }
}
