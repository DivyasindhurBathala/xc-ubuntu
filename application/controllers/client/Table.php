<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Table extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *     http://example.com/index.php/welcome
     *  - or -
     *     http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ssp');
        $this->sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname,
        );
        $this->magebo_details = array(
            'user' => 'root',
            'pass' => 'un!t3d',
            'db' => 'mvno_dev',
            'host' => 'localhost',
        );

        $this->local_details = array(
            'user' => 'root',
            'pass' => 'un!t3d',
            'db' => 'mvno_delta',
            'host' => 'localhost',

        );

        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
        error_reporting(0);
    }

    public function getclient_invoices()
    {
        $userid = $_SESSION['client']['mageboid'];
        $date = date("Y-m-d", strtotime("-6 months"));
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT a.iInvoiceType,a.iInvoiceNbr,a.iAddressNbr,a.dInvoiceDate,a.dInvoiceDueDate,a.iInvoiceStatus,b.cName,a.mInvoiceAmount
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE b.iCompanyNbr =?
    and a.iAddressNbr = ?
    ORDER BY a.iInvoiceNbr DESC", array($this->companyid, $_SESSION['client']['mageboid']));

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $row['mInvoiceAmount'] = number_format($row['mInvoiceAmount'], 2);
                $result[] = $row;
            }
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }

      public function getclient_creditnotes()
    {
        $userid = $_SESSION['client']['mageboid'];
        $date = date("Y-m-d", strtotime("-6 months"));
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT a.iInvoiceNbr,a.iAddressNbr,a.dInvoiceDate,a.dInvoiceDueDate,a.iInvoiceStatus,b.cName,a.mInvoiceAmount
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE b.iCompanyNbr =?
    and a.iAddressNbr = ?
    and a.iInvoiceType = ?
    ORDER BY a.iInvoiceNbr DESC", array($this->companyid, $_SESSION['client']['mageboid'], '41'));

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $row['mInvoiceAmount'] = number_format($row['mInvoiceAmount'], 2);
                $result[] = $row;
            }
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }
    public function getemails()
    {

        $userid = $_SESSION['client']['id'];
        $year = date('Y') . '%';
        $table = <<<EOT
 (
    SELECT a.invoicenum, a.userid,DATE_FORMAT(a.date, '%d/%m/%Y') as date, DATE_FORMAT(a.duedate, '%d/%m/%Y') as duedate,a.total,a.id,a.status
    FROM invoices a
    WHERE a.userid = '$userid'
    AND a.date > curdate() - interval (dayofmonth(curdate()) - 1) day - interval 12 month
    and a.status in ('Paid','Unpaid')
    ORDER BY a.id DESC


 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'invoicenum',
                'dt' => 0,
            ),
            array(
                'db' => 'date',
                'dt' => 1,
            ),
            array(
                'db' => 'duedate',
                'dt' => 2,
            ),
            array(
                'db' => 'total',
                'dt' => 3,
            ),
            array(
                'db' => 'status',
                'dt' => 4,
            ),
            array(
                'db' => 'id',
                'dt' => 5,
            ),
            array(
                'db' => 'userid',
                'dt' => 6,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function gettickets()
    {

        $userid = $_SESSION['client']['id'];
        $year = date('Y') . '%';
        $table = <<<EOT
 (
    SELECT *
    FROM a_helpdesk_tickets a
    WHERE a.userid = '$userid'
    and a.adminonly =(0)
    ORDER BY a.id DESC


 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'date',
                'dt' => 0,
            ),
            array(
                'db' => 'subject',
                'dt' => 1,
            ),
            array(
                'db' => 'status',
                'dt' => 2,
            ),
            array(
                'db' => 'admin',
                'dt' => 3,
            ),
            
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getclient_services()
    {

        $userid = $_SESSION['client']['id'];
        $year = date('Y') . '%';
        $table = <<<EOT
 (
    select a.id,a.status,a.userid, ROUND(a.recurring, 2) as recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.userid = '$userid'
    group by a.id

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'id',
                'dt' => 0,
            ),
            array(
                'db' => 'packagename',
                'dt' => 1,
            ),
            array(
                'db' => 'domain',
                'dt' => 2,
            ),
            array(
                'db' => 'billingcycle',
                'dt' => 3,
            ),

            array(
                'db' => 'status',
                'dt' => 4,
            ),
            array(
                'db' => 'recurring',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 6,
            ),
            array(
                'db' => 'userid',
                'dt' => 7,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_mytickets()
    {
        $userid = $_SESSION['client']['id'];
        $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
where a.userid='$userid'
and a.companyid='$this->companyid'
and a.adminonly= '0'
and a.status not in ('Closed')
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'subject',
                'dt' => 1,
            ),
            array(
                'db' => 'status',
                'dt' => 2,
            ),
            array(
                'db' => 'id',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 6,
            ),
            array(
                'db' => 'userid',
                'dt' => 7,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_emailaccounts()
    {
        $userid = $_SESSION['client']['id'];
        $table = <<<EOT
 (
  SELECT *
  FROM users
  WHERE userid='$userid'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'id',
                'dt' => 0,
            ),
            array(
                'db' => 'email',
                'dt' => 1,
            ),
            array(
                'db' => 'size',
                'dt' => 2,
            ),
            array(
                'db' => 'userid',
                'dt' => 3,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_email, $table, $primaryKey, $columns));
    }
}
