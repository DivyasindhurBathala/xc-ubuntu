<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ServiceController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', $_SESSION['client']['language']);

        //$this->companyid = get_companyidby_url(base_url());

        $this->companyid = get_companyidby_url(base_url());
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_client_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('client');
        }



        $this->data['setting'] = globofix($this->companyid);
    }
}
class Service extends ServiceController
{
    public function __construct()
    {
        parent::__construct();
        if (empty($_SESSION['theme'])) {
            $_SESSION['theme'] = "exocom";
        }
        $this->load->model('Client_model');
    }

    public function index()
    {
        $this->load->model('Admin_model');

        $f = array();
        $cdr = array();
        $stats = $this->Client_model->getStats($_SESSION['client']['id']);
        if ($stats) {
            $this->data['title'] = $this->data['setting']->companyname;
        }

        $this->data['dtt'] = 'clientarea_services.js?version=1.0';
        $this->data['title'] = "United Dasboard";
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/client_services';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/content', $this->data);
    }

    public function send_bundleorder_email($id, $bundle, $validfrom, $validuntil, $template)
    {
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);

        $body = getMailContent($template, $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$bundle_validfrom}', $validfrom, $body);
        $body = str_replace('{$bundle_validuntil}', $validuntil, $body);
        $body = str_replace('{$bundle_name}', $bundle->name, $body);
        $body = str_replace('{$bundle_price}', $bundle->recurring_total, $body);
        $body = str_replace('{$msisdn_sim}', $mobile->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->msisdn_puk2, $body);
        $body = str_replace('{$msisdn', $mobile->msisdn, $body);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to(array('mail@simson.one'));
        $subject  =getSubject('mobile_bundle30d', $client->language, $client->companyid);

        $this->email->subject($subject);
        if (empty($subject)) {
            return array('result'=>'success');
        }
        if (empty($body)) {
            return array('result'=>'success');
        }
        $this->email->message($body);
        if ($this->email->send()) {
            logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => $subject, 'message' => $body));
            return array('result' => 'success');
        } else {
            logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => $subject, 'message' => $body, 'status' => 'error', 'error_message' => $this->email->print_debugger()));
            return array('result' => 'error', 'message' => $this->email->print_debugger());
        }
    }



    public function buybundle()
    {
        $this->load->model('Admin_model');
        //Array ( [bundleid] => 159 [from] => 2019-06-21 [serviceid] => 700630 )
        $bundle = $this->Admin_model->getBundleOption($_POST['bundleid']);
        if ($bundle->bundle_type == 'option') {
            $this->load->library('magebo', array(
            'companyid' => $this->companyid,
            ));

            $service = $this->Admin_model->getService($_POST['serviceid']);
            if ($service->details->platform =="ARTA") {
                $this->load->library('artilium', array('companyid' => $this->companyid));
            }



            $bundleid = $_POST['bundleid'];
            $serviceid = $_POST['serviceid'];
            $date = explode('T', $_POST['from']);
            $s = explode('-', $date[0]);
            $contract = explode('-', $_POST['from']);
            if ($bundle) {
                $future = getFuturedate($date[0], $bundle->bundle_duration_type, $bundle->bundle_duration_value);
            } //$bundle
            if (!empty($_POST['to'])) {
                $future = $_POST['to'];
                $toto = explode('T', $_POST['to']);
            }

            if ($toto == date('Y-m-t')) {
                $months = 1;
            } else {
                $months = getMonthDif($date[0], $toto[0]);
            }

            $recurring = false;


            $addon_id = $this->Admin_model->insert_option_bundle(array(
            'name' => $bundle->name,
            'companyid' => $this->companyid,
            'terms' => $bundle->bundle_duration_value,
            'cycle' => $bundle->bundle_duration_type,
            'serviceid' => $_POST['serviceid'],
            'addonid' => $_POST['bundleid'],
            'recurring_total' => $bundle->recurring_total,
            'addon_type' => $bundle->bundle_type,
            'arta_bundleid' => $bundle->bundleid,
            'iGeneralPricingIndex' => null,
            ));


            if ($addon_id > 0) {
                $service = $this->Admin_model->getService($_POST['serviceid']);
                $client = $this->Admin_model->getClient($service->userid);

                $result = $this->artilium->AddBundleAssign($service->details->msisdn_sn, $_POST['bundleid'], $_POST['from'], $future, $recurring);
                //print_r($result);
                if ($result->result == "success") {
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'user' =>   $_SESSION['client']['initial']." ".$_SESSION['client']['firstname'].' '.$client['client']['lastname'],
                        'serviceid' => $_POST['serviceid'],
                        'userid' => $service->userid,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Purchase Bundle: ' . $bundle->name.' via selfcare'
                        ));
                    $magebo = $this->magebo->ProcessPricing($service, $client->mageboid, $bundleid, 1, $months, $contract[1].'-'.$contract[2].'-'.$contract[0]);

                    if ($magebo) {
                        $this->db->query("update a_services_addons set iGeneralPricingIndex =? where id=?", array(trim($magebo), $addon_id));
                    }

                    $this->send_bundleorder_email($_POST['serviceid'], $bundle, $_POST['from'], $future, 'mobile_bundle30d');
                    $this->session->set_flashdata('success', lang('Addon has been added as requested please do not add again otherwise it will be double'));
                } //$result->result == "success"
                else {
                    $this->session->set_flashdata('success', lang('There was error on updating addong please contract +32484889888 for urgent support'));
                }
            } //$addon_id > 0
            else {
                $this->session->set_flashdata('error', lang('Addon order failed, please contact our support'));
            }
        } elseif ($bundle->bundle_type == 'tarif') {
            $this->addBundleTarief($_POST);
        }

        redirect('client/service/detail/' . $_POST['serviceid']);
    }


    public function addBundleTarief($dd)
    {
        $service = $this->Admin_model->getService($dd['serviceid']);
        if ($service->details->platform =="ARTA") {
            $this->load->library('artilium', array('companyid' => $this->companyid));
        }

        $this->load->library('magebo', array(
            'companyid' => $this->companyid,
        ));
        $bundle = $this->Admin_model->getBundleOption($dd['bundleid']);
        $bundleid = $dd['bundleid'];
        $serviceid = $dd['serviceid'];
        $date = explode('T', $dd['from']);
        $s = explode('-', $date[0]);

        if (!empty($dd['to'])) {
            $toto = explode('T', $dd['to']);
            $to = $toto[0] . 'T23:59:59';
            $date2 = $toto[0];
        } else {
            $to = '2099-12-31T23:59:59';
            $date2 = "2099-12-31";
        }

        $months = getMonthDif($date[0], $date2);

        if ($bundle) {
            $future = getFuturedate($date[0], $bundle->bundle_duration_type, $bundle->bundle_duration_value);
        } //$bundle

        $recurring = 1;
        $price = $bundle->recurring_total;


        $addon_id = $this->Admin_model->insert_option_bundle(array(
            'name' => $bundle->name,
            'companyid' => $this->companyid,
            'terms' => $bundle->bundle_duration_value,
            'cycle' => $bundle->bundle_duration_type,
            'serviceid' => $dd['serviceid'],
            'addonid' => $dd['bundleid'],
            'recurring_total' => $price,
            'addon_type' => $bundle->bundle_type,
            'arta_bundleid' => $bundle->bundleid,
            'iGeneralPricingIndex' => null,
        ));
        if ($addon_id > 0) {
            $service = $this->Admin_model->getService($dd['serviceid']);
            $client = $this->Admin_model->getClient($service->userid);

            $result = $this->artilium->AddBundleAssign($service->details->msisdn_sn, $dd['bundleid'], $dd['from'], $to);
            if ($result->result == "success") {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'user' =>   $_SESSION['client']['initial']." ".$_SESSION['client']['firstname'].' '.$client['client']['lastname'],
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $service->userid,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Purchase Bundle: ' . $bundle->name.' via selfcare'
                    ));
                $this->send_bundleorder_email($dd['serviceid'], $bundle, $dd['from'], $future, 'mobile_bundle_recurring');
                if ($this->data['setting']->create_magebo_bundle) {
                    $create = 1;
                } //$this->data['setting']->create_magebo_bundle
                else {
                    $create = 0;
                }

                if ($recurring) {
                    mail('mail@simson.one', 'post', print_r($dd, true).' '.$create.' '.print_r($service, true));
                    $this->magebo->ProcessPricingExtraBundle($service, $client->mageboid, $dd['bundleid'], $create, $price, $s, $months+1);
                } else {
                    $mageboid = $this->magebo->ProcessPricing($service, $client->mageboid, $dd['bundleid'], $create, 1, $s[1].'-'.$s[2].'-'.$s[0]);
                    if ($magebo) {
                        $this->db->query("update a_services_addons set iGeneralPricingIndex =? where id=?", array(trim($magebo), $addon_id));
                    }
                }
                // $this->magebo->ProcessPricingExtraBundle($service, $client->mageboid, $_POST['bundleid'], $create, $price, $s);
                $this->session->set_flashdata('success', lang('Addon has been added as requested please do not add again otherwise it will be double'));
            } //$result->result == "success"
            else {
                $this->session->set_flashdata('success', lang('There was error on updating addong please contact our agent'));
            }
        } //$addon_id > 0
        else {
            $this->session->set_flashdata('error', lant('Addon has been added as requested'));
        }

        redirect('client/service/detail/' . $_POST['serviceid']);
    }

    public function detail()
    {
        $this->data['pending'] = false;
        $this->data['productchange'] = hasChangeProductPlanned($this->uri->segment(4));
        $this->data['showusage'] = false;
        $f = $this->db->query("SELECT * FROM `a_logs` WHERE `serviceid` = 1814543 ORDER BY `a_logs`.`id` desc");
        if ($f->num_rows()>0) {
            $this->data['logs'] = $f->result();
            $this->data['logs'][] = (object)array('date' => '', 'description' => '');
        } else {
            $this->data['logs'] = false;
        }
        $this->load->model('Admin_model');
        $res = array();
        $excludes = array('MOBILE', 'Block All', 'MNP Beep');
        $id = $this->uri->segment(4);

        $this->data['service'] = $this->Admin_model->getService($id);

        if ($this->data['service']->userid != $this->session->userdata('client')['id']) {
            $this->session->set_flashdata('error', lang('Access denied'));
            redirect('client/dashboard');
            exit;
        }

        if (in_array($this->data['service']->status, array('Suspended','Terminated','Cancelled'))) {
            $this->session->set_flashdata('error', lang('This service has been suspended or Terminated Please contact our support'));
            redirect('client/dashboard');
            exit;
        }
        $this->data['client'] = $this->Admin_model->getClient($this->data['service']->userid);
        $this->data['title'] = "Service Information";
        //$service = $this->Admin_model->getService($_POST['serviceid']);




        if ($this->data['service']->details->platform =="ARTA") {
            $this->load->library('artilium', array('companyid' => $this->companyid));
            if($this->data['service']->product_sub_type == "Prepaid"){
                $mm = $this->artilium->GetSpecificCliInfo($this->data['service']->details->msisdn_sn);
                //print_r($mm);
                $this->data['credit'] = number_format($mm->Balance,2);
            } else {
                $this->data['credit'] = "0.00";
            }
        } else {
            $this->data['credit'] = "0.00";
        }




        if ($this->data['service']->status == 'Active') {
            if (in_array($this->data['service']->type, array("mobile"))) {
                $this->data['mobile'] = $this->data['service']->details;
                if ($this->data['service']->details->platform == "ARTA") {
                    //echo $service->platform;
                    $ss = $this->artilium->GetSn($this->data['mobile']->msisdn_sim);
                    $this->data['sn'] = (object) $ss->data;
                    $this->data['showusage'] = true;

                    $this->data['bundles'] = $this->artilium->GetBundleAssignList1(trim($this->data['sn']->SN));

                    $this->data['cdr'] = $this->artilium->get_cdr($this->data['service']);
                } else {
                    $this->data['sn']="";
                    $this->data['showusage'] = false;
                    $this->data['bundles']=array();
                    $this->data['cdr'] =array();
                }


                if ($this->session->email == "simson.parlindungan@united-telecom.be") {
                    // print_r($this->data['cdr']);
                }
            } else {
                redirect('client/service/activate_mobile/' . $this->uri->segment('4'));
                exit;
            }
        } else {
            $this->data['pending'] = true;
        }
        if (!$this->data['pending']) {
            $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/service_mobile_details';
        } else {
            if ($this->data['mobile']->msisdn_status == "PortinAccepted") {
                $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/service_mobile_pending_active';
            } else {
                $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/service_mobile_activation';
            }
        }

        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/content', $this->data);
    }
    public function getAddonid()
    {
        $this->db = $this->load->database('default', true);
        $addon = $this->db->query("select a.* from a_products_mobile_bundles a where a.id=?", array($_POST["id"]));
        $result = $addon->row_array();
        $result['recurring_total'] = number_format($result['recurring_total'], 2);
        echo json_encode($result);
    }
    public function getusage()
    {
        $id = $this->uri->segment(4);
        $this->data['service'] = $this->Admin_model->getService($id);

        $cdr = $this->artilium->get_cdr($this->data['service']);
        echo json_encode(array('voice' => second2hms($cdr['voice']), 'data' => round($cdr['data'] / 1048576, 2), 'sms' => $cdr['sms']));
    }
    public function activate_mobile()
    {
        if (isPost()) {
            $this->session->set_flashdata('success', lang('Your request has been received, your number will be activate shortly, if your are porting your number from other provider, wait until the  signal droped from your current provider'));
        }

        $this->load->model('Admin_model');
        $id = $this->uri->segment(4);
        $this->data['service'] = $this->Admin_model->getService($id);
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/service_mobile_activation';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/content', $this->data);
    }
    public function mobile_stolen()
    {
        if (!empty($_POST['msisdn']) && !empty($_POST['typebar']) && isset($_POST['SN'])) {
            $this->data['mobile'] = $this->Admin_model->getService($_POST['serviceid']);

            $result = $this->artilium->UpdateCLI($sn->SN, 0);
            if ($result->result == "success") {
                $this->db->where('id', $_POST["serviceid"]);
                $this->db->update('a_services_mobile', array('status' => 'SimBlocked'));
                if ($_POST['ticket'] == 2) {
                    $this->load->model('Helpdesk_model');
                    $ticket['subject'] = "Simcard replacement for " . $_POST['msisdn'];
                    $ticket['userid'] = $_POST['userid'];
                    $ticket['name'] = $_SESSION['firstname'] . " " . $_SESSION["lastname"];
                    $ticket['email'] = $_SESSION["email"];
                    $ticket['deptid'] = getDefaultDepartment($this->companyid);
                    $ticket['categoryid'] = getCategoryidSimReplacement($this->companyid);
                    $ticket['message'] = "Customer request simcard replacement for :" . $_POST['SimCardNbr'];
                    $ticket['date'] = date('Y-m-d H:i:s');
                    $ticket['date'] = date('Y-m-d H:i:s');
                    $ticket['status'] = 'Open';
                    $ticket['tid'] = random_str('alphacaps', 3) . '-' . random_str('num', 6) . '-' . date('Y');

                    $id = $this->Helpdesk_model->insert_ticket($ticket);
                    $this->send_simblock_email($_POST['serviceid']);
                    $this->session->set_flashdata('success', lang('Service has been blocked, our agent  has been notified for your simcard replacement'));
                } else {
                    $this->session->set_flashdata('success', lang('Service has been blocked, if you wish to unblock please click button Unblock Sim'));
                }
            } else {
                $this->session->set_flashdata('error', lang('Failed to change Service Status'));
            }
        } else {
            $this->session->set_flashdata('error', lang('there has been an error when requesting service block, please contact our support'));
        }

        redirect('client/service/detail/' . $_POST['serviceid']);
    }
    public function send_simblock_email($id)
    {
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);

        $body = getMailContent('simcard_blocking', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$msisdn}', $mobile->msisdn, $body);
        $body = str_replace('{$msisdn_sim}', $mobile->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->msisdn_puk2, $body);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('simcard_blocking', $client->language, $client->companyid));
        if (empty(getSubject('simcard_blocking', $client->language, $client->companyid))) {
            return array('result' => 'error');
        }
        if (empty(getMailContent('simcard_blocking', $client->language, $client->companyid))) {
            return array('result' => 'error');
        }
        $this->email->message($body);
        if ($this->email->send()) {
            logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('simcard_blocking', $client->language, $client->companyid), 'message' => $body));
            return array('result' => 'success');
        } else {
            logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('simcard_blocking', $client->language, $client->companyid), 'message' => $body, 'status' => 'error', 'error_message' => $this->email->print_debugger()));
            return array('result' => 'error', 'message' => $this->email->print_debugger());
        }
    }
    public function update_sim_language()
    {
        $this->load->model('Admin_model');

        if ($this->data['setting']->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->data['setting']->companyid;
        }

        if ($_POST['language'] == "3") {
            $l = "Dutch";
        } elseif ($_POST['language'] == "2") {
            $l = "French";
        } elseif ($_POST['language'] == "1") {
            $l = "English";
        }

        if ($this->Admin_model->IsAllowedMobile($_POST['msisdn'], $companyid)) {
            $result = $this->artilium->UpdateLanguage($_POST['sn'], $_POST['language']);
            if ($result->result == "success") {
                $this->db->where('serviceid', $_POST['serviceid']);
                $this->db->update('a_services_mobile', array('msisdn_languageid' => $_POST['language']));

                $res = array('result' => "success", 'message' => lang('Language has been changed to: ' . $l));
            } else {
                $res = array('result' => "error", 'message' => lang('There were an error while changing your voice mail language.'), 'extra' => $result);
            }
        } else {
            $res = array('result', 'Access denied');
        }

        echo json_encode($res);
    }


    public function export_livecdr()
    {
        set_time_limit(0);
        $this->load->model('Admin_model');
        $this->load->library('xlswriter');
        $serviceid = $this->uri->segment(5);
        $mobile    = $this->Admin_model->getService($serviceid);

        if ($mobile->userid != $_SESSION['client']['id']) {
            die('Access denied');
        }
        $this->load->library('artilium', array('companyid' =>  $mobile->companyid));
        $c         = $this->artilium->get_cdr($mobile);
        $header    = array(
            lang('Begintime') => 'string',
            lang('Type') => 'string',
            lang('Destination') => 'string',
            lang('Description') => 'string',
            lang('Duration') => 'string',
            lang('Cost') => 'string'
        );
        if (count($c['cdr']) > 0) {
            foreach ($c['cdr'] as $x) {
                if ($x['TrafficTypeId'] == 2) {
                    $x['DurationConnection'] = convertToReadableSize($x['DurationConnection']);
                }
                if ($x['DurationConnection'] != "0 bytes") {
                    $cdr[] = array(
                            'Begintime' => $x['Begintime'],
                            'TrafficTypeId' => getTraficName($x['TrafficTypeId']),
                            'MaskDestination' => $x['MaskDestination'],
                            'Description' => $x['DestinationCountry'],
                            'DurationConnection' => $x['DurationConnection'],
                            'CurNumUnitsUsed' => $x['CurNumUnitsUsed']
                    );
                }
            } //$c['cdr'] as $x
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->xlswriter->writeSheet($cdr, 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('LIVE CDR Export');
            $this->xlswriter->setCompany('United');
            echo $this->xlswriter->writeToString();
        } //count($c['cdr']) > 0
        else {
            $this->session->set_flashdata('error', 'no data to be exported');
            redirect('client/service/detail/' . $serviceid);
        }
    }


    public function export_livecdr_byMonth()
    {
        $month = $this->uri->segment(7);



        $this->load->model('Admin_model');
        $this->load->library('xlswriter');
        $serviceid = $this->uri->segment(5);
        $mobile    = $this->Admin_model->getService($serviceid);

        if ($mobile->userid != $_SESSION['client']['id']) {
            die('Access denied');
        }
        if($mobile->details->platform == "ARTA"){
            $this->load->library('artilium', array('companyid' => $this->companyid));
            $c         = $this->artilium->get_cdr($mobile, $month);
        }

        /* $header    = array(
            lang('Begintime') => 'string',
            lang('Type') => 'string',
            lang('Destination') => 'string',
            lang('Description') => 'string',
            lang('Duration') => 'string',
            lang('Cost') => 'string'
         );
         if (count($c['cdr']) > 0) {
            foreach ($c['cdr'] as $x) {
                if ($x['TrafficTypeId'] == 2) {
                    $x['DurationConnection'] = convertToReadableSize($x['DurationConnection']);
                }

                if($x['DurationConnection'] != "0 bytes"){
                      $cdr[] = array(
                            'Begintime' => $x['Begintime'],
                            'TrafficTypeId' => getTraficName($x['TrafficTypeId']),
                            'MaskDestination' => $x['MaskDestination'],
                            'Description' => $x['DestinationCountry'],
                            'DurationConnection' => $x['DurationConnection'],
                            'CurNumUnitsUsed' => $x['CurNumUnitsUsed']
                    );

                }

            } //$c['cdr'] as $x
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->xlswriter->writeSheet($cdr, 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('LIVE CDR Export');
            $this->xlswriter->setCompany('United');
            echo $this->xlswriter->writeToString();

*/


        if (count($c['cdr']) > 0) {
            $this->load->library('spdf');
            $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Simson Lai');
            $pdf->SetTitle('CDR');
            $pdf->SetSubject('CDR ' . $mobile->details->msisdn.' '.$month);
            $pdf->SetKeywords('TCPDF, Mobile, ' . $mobile->details->msisdn);
            $pdf->AddPage('P', 'A4');

            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            /*// set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
            */
            // ---------------------------------------------------------

            // set font
            $pdf->SetFont('helvetica', 'B', 20);

            // add a page
            //$pdf->Write(0, 'Example of HTML tables', '', 0, 'L', true, 0, false, false, 0);


            $tbl = '
<table cellspacing="0" cellpadding="1" border="0">
<thead>
    <tr>
        <th><strong>'.lang('Date').'</strong></th>
        <th>'.lang('Type').'</th>
        <th>'.lang('Destination').'</th>
        <th>'.lang('Description').'</th>
        <th>'.lang('Duration').'</th>
         <th>'.lang('Cost').'</th>
    </tr>
    </thead>
    <tbody>';
            $pdf->SetFont('helvetica', '', 8);
            // -----------------------------------------------------------------------------

            foreach ($c['cdr'] as $x) {
                if ($x['TrafficTypeId'] == 2) {
                    $x['DurationConnection'] = convertToReadableSize($x['DurationConnection']);
                }

                if ($x['DurationConnection'] != "0 bytes") {
                    $tbl .='   <tr>
        <td>'.$x['Begintime'].'</td>
        <td>'.getTraficName($x['TrafficTypeId']).'</td>
            <td>'.$x['MaskDestination'].'</td>
        <td>'.$x['DestinationCountry'].'</td>
            <td>'.$x['DurationConnection'].'</td>
        <td>'.$x['CurNumUnitsUsed'].'</td>
    </tr>
    ';
                }
            }

            $tbl .='</tbody>
</table>';

            $pdf->writeHTML($tbl, true, false, false, false, '');



            $pdf->Output($mobile->details->msisdn.$month.'.pdf', 'I');
        } //count($c['cdr']) > 0
        else {
            $this->session->set_flashdata('error', 'no data to be exported');
            redirect('client/service/detail/' . $serviceid);
        }
    }

    public function change_packagesetting()
    {
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if($service->details->platform == "ARTA"){
            $this->load->library('artilium', array('companyid' => $this->companyid));
            if (!empty($_POST['sn']) && !empty($_POST['id']) && isset($_POST['val'])) {
                if ($_POST['id'] == 100000) {
                    $result = $this->artilium->SwitchVoiceMail($_POST['msisdn'], $_POST['sn'], $_POST['val']);
                } else {
                    $result = $this->artilium->UpdatePackageOptionsForSN(trim($_POST['sn']), $_POST['id'], $_POST['val']);
                    logAdmin(array(
                            'companyid' => $this->companyid,
                            'user' => $this->session->firstname . ' ' . $this->session->lastname,
                            'serviceid' => $_POST['serviceid'],
                            'userid' => $_POST['userid'],
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'UpdatePackageOptionsForSn for PackageDefinitionId ' . $_POST['id'] . ' to ' . $_POST['val']
                    ));
                }

                echo json_encode($result);
            } //!empty($_POST['sn']) && !empty($_POST['id']) && isset($_POST['val'])
            else {
                echo json_encode(array(
                            'result' => 'error',
                            'message' => 'Not enough data: sn,id,val'
                    ));
            }
        }

    }
}
