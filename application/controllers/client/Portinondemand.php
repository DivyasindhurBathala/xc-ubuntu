<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PodController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }
}
class Portinondemand extends PodController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
    }
    public function index()
    {
        $this->data['listen'] = false;
        $this->load->database('default', true);
        if (!empty($_SESSION['pod'])) {
            $q = $this->db->query("select * from a_services_mobile where msisdn like ? and msisdn_status = ?", array($_SESSION['pod']['msisdn'], 'PortinPending'));
            if ($q->num_rows()>0) {
                $this->data['pod'] = $q->row();
                $this->data['listen'] = true;
            } else {
                $this->data['listen'] = false;
            }
        }
        if ($_SESSION['pod']['msisdn']) {
            $this->data['number'] = "0". substr(trim($_SESSION['pod']['msisdn']), 2);
        } else {
            $this->data['number'] = "06";
        }
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/pod1', $this->data);
    }
    public function request_new()
    {
        if ($_SESSION['pod']['msisdn']) {
            $this->data['number'] = "0". substr(trim($_SESSION['pod']['msisdn']), 2);
        } else {
            $this->data['number'] = "06";
        }
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/pod1_new', $this->data);
    }
    public function request_code()
    {
        if (substr(trim($_POST["msisdn"]), 0, 1) == "0") {
            $_POST['msisdn'] = "31".substr(trim($_POST["msisdn"]), 1);
        } else {
            echo json_encode(array("result" => "error", "message" => lang('Sorry your Number can not be found in portinpending status')));
            exit;
        }
        $this->load->model('Admin_model');
        $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where donor_msisdn like ? and msisdn_status !=  ?", array(trim($_POST['msisdn']), 'Cancelled'));
        if ($q->row()->msisdn_status == "Active") {
            echo json_encode(array("result" => "error", "message" => lang('Sorry your Number can not be found in portinpending status')));
            exit;
        }

        if ($q->row()->donor_customertype == "1") {
            echo json_encode(array("result" => "error", "message" => lang('You have chosen to use Normal portin way so portin on demand is disabled, please wait until current provider approve your portin if you wish to change to portin on demand please contact our agent')));
            exit;
        }

        $this->load->library('artilium', array('companyid' => $q->row()->companyid));
        if ($q->num_rows()>0) {
            if ($q->row()->pod_counter >= 3) {
                $counter = $q->row()->pod_counter+1;
                $this->db->query("update a_services_mobile set pod_counter = ?, pod_lastrequest=? where msisdn like ? and msisdn_status = ?", array($counter, time(), $_POST['msisdn'], 'PortinPending' ));
                // insertAPILog(array('request' => $cancel->request, 'result' => $cancel->response, 'ip' => '127.0.0.1'));
                echo json_encode(array("result" => "error", "message" => lang('Sorry you have reach your maximum threshold of requesting code, please contact our support')));
                exit;
            }

            if (in_array($q->row()->msisdn_status, array("PortInValidateVerificationCodeAccepted","PortinAccepted"))) {
                echo json_encode(array("result" => "error", "message" => lang('This Portin Request has been accepted or validated, you may not request new code')));
                //insertAPILog(array('request' => $cancel->request, 'result' => $cancel->response, 'ip' => '127.0.0.1'));
                //mail('mail@simson.one', 'Portin request attempt but already validated', print_r($q->row(), true) ."\n ".print_r($_SESSION, true)."\n ". print_r($_SERVER, true));
                exit;
            }
            $client = $this->Admin_model->getClient($q->row()->userid);
            $cancel = $this->artilium->CancelPortIn($q->row()->msisdn_sn);
            //mail('mail@simson.one', 'Cancel Portin', print_r($cancel, true));
            // insertAPILog(array('request' => $cancel->request, 'result' => $cancel->response, 'ip' => '127.0.0.1'));
            insertAPILog(array('api_call' => 'CancelPortIn', 'api_env' => '906_302', 'request' => $cancel->request, 'result' => $cancel->response, 'ip' => '127.0.0.1', 'companyid' => $q->row()->companyid));

            sleep(35);
            $resportin = $this->artilium->PortingNewSIM(format_name($client), $q->row());

            log_message('error', print_r($resportin, true));
            //$resportin = $this->artilium->ActiveNewSIM($client->firstname . ' ' . $client->lastname, $q->row());
            if ($resportin->result == "success") {
                $counter = $q->row()->pod_counter+1;
                $this->db->query("update a_services_mobile set pod_counter = ?, pod_lastrequest=? where msisdn like ? and msisdn_status = ?", array($counter, time(), $_POST['msisdn'], 'PortinPending' ));
                $s = $this->db->query("select * from a_services_mobile where msisdn like ? and msisdn_status = ?", array($_POST['msisdn'], 'PortinPending'));
                $_SESSION['pod'] = $_POST;

                $this->session->set_flashdata('success', lang('Your request has been processed, please enter the verification code below'));
                logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' =>  $q->row()->serviceid,
                            'userid' => $client->id,
                            'user' => $client->firstname . ' ' . $client->lastname,
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' . $q->row()->serviceid .' '. lang('requested for new code portin demand successfully for ').$q->row()->msisdn
                    ));
                send_growl(
                        array(
                        'message' => $client->firstname . ' ' . $client->lastname.' requested for new code portin demand: '. $q->row()->msisdn,
                        'companyid' =>  $this->companyid)
                    );
                echo json_encode(array("result" => "success"));
            } else {
                send_growl(array(
                'message' => $client->firstname . ' ' . $client->lastname.' Failed to request new code for Number: '. $q->row()->msisdn,
                'companyid' =>  $this->companyid));
                logAdmin(array(
                   'companyid' => $this->companyid,
                   'serviceid' => $q->row()->serviceid,
                   'userid' => $client->id,
                   'user' => $client->firstname . ' ' . $client->lastname,
                   'ip' => $_SERVER['REMOTE_ADDR'],
                   'description' => 'Service : ' . $q->row()->serviceid .' '. lang('requested for new code portin demand failed ').print_r($result, true)
                ));
            }
        } else {
            //insertAPILog(array('request' => $cancel->request, 'result' => $cancel->response, 'ip' => '127.0.0.1'));
            // mail('mail@simson.one', 'Portin On demand request code failed', print_r($_POST, true));
            echo json_encode(array("result" => "error", "message" => lang('Sorry your Number can not be found in portinpending status').' '.$_POST['msisdn']));
            exit;
        }
        // redirect('client/portinondemand');
    }

    public function confirm_code()
    {
        if (substr(trim($_POST["msisdn"]), 0, 1) == "0") {
            $_POST['msisdn'] = "31".substr(trim($_POST["msisdn"]), 1);
        } else {
            echo json_encode(array("result" => "error", "message" => lang('You must enter number with leading 0')));
            exit;
        }
        $_SESSION['pod'] = $_POST;
        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.* from a_services_mobile a left join a_services b on b.id=a.serviceid where a.msisdn like ? and b.status != ? order by a.id desc", array('%'.trim($_POST['msisdn']).'%', 'Terminated'));
        log_message('error', $this->db->last_query());
        if ($q->num_rows() > 0) {
            if (in_array($q->row()->msisdn_status, array("PortInValidateVerificationCodeAccepted","PortinAccepted"))) {
                echo json_encode(array("result" => "error", "message" => lang('This Portin Request has been accepted or validated, you do not need to confirm it anymore')));
                //mail('mail@simson.one', 'Portin request attempt but already validated',  print_r($q->row(), true) ."\n ".print_r($_SESSION, true)."\n ". print_r($_SERVER, true));
                exit;
            }
            if ($q->row()->pod_counter > 3) {
                echo json_encode(array("result" => "error", "message" => lang('Sorry you have reach your maximum threshold of requesting code, please contact our support')));
                exit;
            }
            $this->load->library('artilium', array('companyid' => $q->row()->companyid));
            if (!empty($q->row()->msisdn_sn)) {
                $request = $this->artilium->SaveVerificationCode($q->row()->msisdn_sn, trim($_POST['code']));

                //mail('mail@simson.one', 'porting save array', print_r($request, true));
                insertAPILog(array('api_call' => 'SaveVerificationCode', 'api_env' => '906_302', 'request' => $request->request, 'result' => $request->response, 'ip' => '127.0.0.1', 'companyid' => $q->row()->companyid));
                // insertAPILog(array('api_call' => 'PortInRequest', 'api_env' => $this->params->arta_version, 'request' => $this->ServicePorting->__getLastRequest(), 'result' => $this->ServicePorting->__getLastResponse(), 'ip' => '127.0.0.1'));



                $client = $this->Admin_model->getClient($q->row()->userid);
                if ($request->result == "success") {
                    //$this->db->query("update a_services_mobile set msisdn_status =? where serviceid=?", array('PortinAccepted', $q->row()->serviceid));
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $q->row()->serviceid,
                        'userid' => $client->id,
                        'user' => $client->firstname . ' ' . $client->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' . $q->row()->serviceid .' '. lang('requested for confirmation successfull with code: ').$_POST['code']
                    ));
                    send_growl(array(
                        'message' => $client->firstname . ' ' . $client->lastname.' requested for confirmation successfull with code: '.$_POST['code'],
                        'companyid' =>  $this->companyid));
                    $this->session->set_flashdata('success', lang('Your request has been processed'));
                    $this->session->set_flashdata('pending', 'yes');
                    echo json_encode(array("result" => "success"));
                    exit;
                } else {
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $q->row()->serviceid,
                        'userid' => $client->id,
                        'user' => $client->firstname . ' ' . $client->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' . $q->row()->serviceid . ' '.lang('requested for confirmation failed with code').': '.$_POST['code']
                    ));
                    //$this->session->set_flashdata('error', lang('ErrorCode: ').': '. $request->result);
                    echo json_encode(array("result" => "error", "message" => $request->message));
                    exit;
                }
            } else {
                mail('mail@simson.one', 'Portin On demand request confirm failed', print_r($_POST, true));
                echo json_encode(array("result" => "error", "message" => lang('We are unable to process your request at the moment, our agent has been notified')));
                exit;
                //$this->session->set_flashdata('error', lang('We are unable to process your request at the moment, our agent has been notified'));
            }
        } else {
            logAdmin(array(
                        'companyid' => $client->companyid,
                        'serviceid' => null,
                        'userid' => $client->id,
                        'user' => $client->firstname . ' ' . $client->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' . $q->row()->serviceid .' '. lang('requested for confirmation Failed because number not found: ').$_POST['msisdn'].' Code : '.$_POST['code']
                    ));

            //$this->session->set_flashdata('error', lang('Sorry your Number can not be found'));
            echo json_encode(array("result" => "error", "message" => lang('Your number status is in terminated, we can not process your request, please contact our agent')));
            exit;
        }
        // redirect('client/portinondemand');
    }

    public function send_PortinInitiation($id)
    {
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->data['setting']->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);


        $body = getMailContent('portin_initiation', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->etails->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->etails->msisdn_puk2, $body);
        $body = str_replace('{$msisdn', $mobile->etails->msisdn, $body);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('portin_initiation', $client->language, $client->companyid));
        //$this->email->subject(lang("Your New Password"));
        $this->email->message($body);
        if ($this->email->send()) {
            logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => getSubject('portin_initiation', $client->language, $client->companyid), 'message' => $body));
        }
    }
    public function send_PortinAccepted($id)
    {
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->data['setting']->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);


        $body = getMailContent('portin_accepted', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->etails->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->etails->msisdn_puk2, $body);
        $body = str_replace('{$msisdn', $mobile->etails->msisdn, $body);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('portin_accepted', $client->language, $client->companyid));
        //$this->email->subject(lang("Your New Password"));
        $this->email->message($body);
        if ($this->email->send()) {
            logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => getSubject('portin_accepted', $client->language, $client->companyid), 'message' => $body));
        }
    }
}
