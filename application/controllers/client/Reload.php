<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ReloadController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', $_SESSION['client']['language']);
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Reload extends ReloadController
{

    public function __construct()
    {
        parent::__construct();
        if (empty($_SESSION['theme'])) {
            $_SESSION['theme'] = "exocom";
        }
        $this->load->model('Client_model');
        $this->load->library('artilium', array('companyid' => $this->companyid));
    }

    public function index()
    {

        $this->load->model('Admin_model');

        $f = array();
        $cdr = array();
        $stats = $this->Client_model->getStats($_SESSION['client']['id']);
        if ($stats) {
            $this->data['title'] = $this->data['setting']->companyname;
        }

        $this->data['title'] = "Reload Services";
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/client_reload';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/clients/'.$this->session->cid.'/content', $this->data);

    }

}