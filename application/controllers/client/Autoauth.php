<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AutoauthController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('client');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }
}
class Autoauth extends AutoauthController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('user_agent');
        $this->load->model('Api_model');
    }
    public function index()
    {
        $key = $_GET['key'];
        if (!empty($key)) {
            $data = encrypt_decrypt('decrypt', $key);
            if ($this->agent->is_referral()) {
                $referal = parse_url($this->agent->referrer());
                if ($this->Api_model->getHost($referal['host'])) {
                    $user = explode("#", $data);
                    $now = time();
                    if ($now - $user[0] <= 3600) {
                        $client = $this->Api_model->get_clientinfo($user[1]);
                        $client['islogged'] = true;
                        $this->session->set_userdata('client', $client);
                        //print_r($_SESSION);
                        redirect('client');
                    } else {
                        die('TOKEN EXPIRED');
                    }
                } else {
                    die('The Host: <font color="red">' . $referal['host'] . '</font> referrer is not on the ACL');
                }
            } else {
                die('direct access disallowed');
            }
        } else {
            die('Key Required');
        }
    }
    function namorambe()
    {
        $this->db = $this->load->database('default', true);
        $ip = $_SERVER['REMOTE_ADDR'];
        if (isPost()) {
            if (in_array($ip, array('35.187.167.30','51.255.146.43', '10.10.10.101'))) {
                $key = encrypt_decrypt('encrypt', $_POST['id']);
                $this->db->where('id', strtolower(trim($_POST['id'])));
                $this->db->update('a_clients', array('authdata' => $key));
                //$this->db->query("update a_clients where id=? and authdata=?", array($_POST['id'], $key));
                echo json_encode(array('result' => true, 'key' => $key, 'time' => time(), 'id' => $_POST['id'], 'update' => $this->db->affected_rows()));
            } else {
                echo json_encode(array('result' => false));
            }
        } else {
        }
    }
    function showsess()
    {
        print_r($_SESSION);
    }
}
