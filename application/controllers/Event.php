<?php
defined('BASEPATH') or exit('No direct script access allowed');
class EventController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
    }
}
class Event extends EventController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('mq');
        $this->companyid = get_companyidby_url(base_url());
    }
    public function index()
    {
        error_reporting(1);
        try {
            $companyid = 2;
            $this->load->model('Admin_model');
            $this->db = $this->load->database('default', true);

            if ($_POST['event'] == 'Arta.Events.Reseller.Subscription.Provisioning.JobEndEvent.') {
                if ($_GET['Resellerid'] == "104") {
                    //  mail('mail@simson.one', 'Iriscall Events', print_r($_POST, true));
                }
                $pl      = json_decode($_POST['body']);
                $payload = $pl[0];
                $array   = (array) $payload;
                $cid     = $this->Admin_model->getCompanyid($_GET['Resellerid']);
                if ($cid > 0) {
                    $companyid = $cid;
                }
                if (in_array($cid, get_enabled_events())) {
                    log_message('error', 'Received events from EventHandler '.$companyid .', SN:'.trim($payload->SubscriptionId)." Action: ".$array['Action']);
                    $setting            = globofix($companyid);
                    // mail('mail@simson.one', 'Event company found '.$companyid, print_r($array, true));
                    $array['companyid'] = $companyid;
                    $service            = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.msisdn_sn LIKE ? and b.status not in ('Terminated','Cancelled')", array(
                        '%' . trim($payload->SubscriptionId) . '%'
                    ));
                    if ($service->num_rows() > 0) {
                        // mail('mail@simson.one', 'Event company found '.$companyid, print_r($service->row(), true));
                        $array['serviceid']      = $service->row()->serviceid;
                        $array['SubscriptionId'] = trim($array['SubscriptionId']);
                        if (!in_array($array['JobName'], array(
                            'ChangedService',
                            'ProvisionApn4G',
                            'ChangeQosProfile',
                            'ProvisionApn3G'
                        ))) {
                            if ((trim($array['Action']) == 'PortInRequested') && (trim($array['JobName']) == 'PortInValidateVerificationCodeStep2')) {
                                $array['status'] = 1;
                                $this->db->insert('a_event_log', $array);
                                //echo date('Y-m-d H:i:s')." ==> ".implode(' ', $ca)."\n";
                                $s = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                                    $array['serviceid']
                                ));
                                if ($s->num_rows() > 0) {
                                    $client = getCompanydataByUserID($s->row()->userid);
                                    $this->Admin_model->update_services_data('mobile', $array['serviceid'], array(
                                        'msisdn_status' => 'PortInValidateVerificationCodeAccepted',
                                                'date_modified' => date('Y-m-d H:i:s')
                                    ));
                                    // $this->sendEmailNotification($s->row(), "Portin Code Request on ".date('Y-m-d H:i:s')." Number:" . $s->row()->msisdn." has been validated", "CodeAccepted", $client);
                                    unset($s);
                                    unset($client);
                                }
                            } elseif ((trim($array['Action']) == 'PortInAccepted') && (trim($array['JobName']) == 'Request')) {
                                $array['status'] = 0;
                                $this->db->insert('a_event_log', $array);
                                logAdmin(array(
                                    'companyid' => $companyid,
                                    'userid' => $service->row()->userid,
                                    'serviceid' => $service->row()->serviceid,
                                    'user' => 'System',
                                    'ip' => $_SERVER['REMOTE_ADDR'],
                                    'description' => 'Portin Request has been accepted for  ' . $service->row()->msisdn
                                ));
                                send_growl(array(
                                    'message' => 'Portin has been accepted for  ' . $service->row()->msisdn,
                                    'companyid' => $companyid
                                ));
                            } elseif ((trim($array['Action']) == 'PortInRejected') && (trim($array['JobName']) == 'PortInInitiate')) {
                                $array['status'] = 1;
                                $this->db->insert('a_event_log', $array);
                                logAdmin(array(
                                    'companyid' => $companyid,
                                    'userid' => $service->row()->userid,
                                    'serviceid' => $service->row()->serviceid,
                                    'user' => 'System',
                                    'ip' => $_SERVER['REMOTE_ADDR'],
                                    'description' => 'Portin Request has been rejected for  ' . $service->row()->msisdn
                                ));
                                send_growl(array(
                                    'message' => 'Portin Completed for  ' . $service->row()->msisdn,
                                    'companyid' => $companyid
                                ));
                            } elseif ((trim($array['Action']) == 'PortInReady') && (trim($array['JobName']) == 'PortInChangeMsisdn')) {
                                $array['status'] = 1;
                                $this->db->insert('a_event_log', $array);
                                $ss = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                                    $array['serviceid']
                                ));
                                if ($ss->num_rows() > 0) {
                                    $this->load->libarary('artilium', array(
                                        'companyid' => $array['companyid']
                                    ));
                                    $this->artilium->PartialFullBar($order->details, '0');
                                    if ($setting->mage_invoicing) {
                                        $this->load->libarary('magebo', array(
                                            'companyid' => $array['companyid']
                                        ));
                                        $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
                                    }
                                    $client = getCompanydataByUserID($ss->row()->userid);
                                    $this->Admin_model->update_services_data('mobile', $array['serviceid'], array(
                                        'msisdn_status' => 'Active',
                                        'date_modified' => date('Y-m-d H:i:s')
                                    ));
                                    $this->Admin_model->update_services($array['serviceid'], array(
                                        'status' => 'Active'
                                    ));
                                    logAdmin(array(
                                        'companyid' => $companyid,
                                        'userid' => $service->row()->userid,
                                        'serviceid' => $service->row()->serviceid,
                                        'user' => 'System',
                                        'ip' => $_SERVER['REMOTE_ADDR'],
                                        'description' => 'Portin Completed for  ' . $service->row()->msisdn
                                    ));

                                    unset($s);
                                    unset($client);
                                }
                            } elseif ((trim($array['Action']) == 'PortOutManual') && (trim($array['JobName']) == 'Request')) {
                                $array['status'] = 0;
                                $this->db->insert('a_event_log', $array);
                                logAdmin(array(
                                    'companyid' => $companyid,
                                    'userid' => $service->row()->userid,
                                    'serviceid' => $service->row()->serviceid,
                                    'user' => 'System',
                                    'ip' => $_SERVER['REMOTE_ADDR'],
                                    'description' => 'Portout Request Received for ' . $service->row()->msisdn
                                ));
                                $client = getCompanydataByUserID($service->row()->userid);
                                $this->SendPortoutNotification($service);
                            } elseif ((trim($array['Action']) == 'PortedOut') && (trim($array['JobName']) == 'PortOutRepatriate')) {
                                $array['status'] = 0;
                                $this->db->insert('a_event_log', $array);
                                if ($array['serviceid']) {
                                    $this->Admin_model->update_services_data('mobile', $array['serviceid'], array(
                                        'msisdn_status' => 'PortedOut',
                                        'datacon_ftp' => 0,
                                        'date_modified' => date('Y-m-d H:i:s')
                                    ));
                                    $this->Admin_model->update_services($array['serviceid'], array(
                                        'status' => 'Terminated',
                                        'date_terminate' => date('Y-m-d')
                                    ));
                                    /*  $this->send_TicketUnited($array['serviceid']);
                                      logAdmin(array(
                                          'companyid' => $companyid,
                                          'userid' => $service->row()->userid,
                                          'serviceid' => $service->row()->serviceid,
                                          'user' => 'System',
                                          'ip' => $_SERVER['REMOTE_ADDR'],
                                          'description' => 'Portout Completed for  ' . $service->row()->msisdn
                                      ));
                                      */
                                }
                            } elseif ((trim($array['Action']) == 'PortingFailed') && (trim($array['JobName']) == 'Request')) {
                                $array['status'] = 0;
                                //$this->db->insert('a_event_log', $array);
                                if ($array['serviceid']) {
                                    $this->db->insert('a_event_log', $array);
                                }
                            } else {
                                $this->db->insert('a_event_log', $array);
                            }
                            $id = $this->db->insert_id();
                        }
                    } else {
                        $array['SubscriptionId'] = trim($array['SubscriptionId']);
                        //  mail('mail@simson.one', $companyid . " SERVICE NOT FOUND " . $_POST['event'], print_r($payload, true));
                    }
                } else {
                    if (!in_array($array['JobName'], array(
                        'ChangedService',
                        'ProvisionApn4G',
                        'ChangeQosProfile',
                        'ProvisionApn3G'
                    ))) {
                        log_message('error', 'Event Handler has been hit for United :'.$_POST['body']);

                        $this->db->insert('a_event_log_united', $array);
                    }
                }
            } elseif ($_GET['routing'] == 'Arta.Events.Reseller.Subscription.Bundle.BundleThresholdEvent.') {
                $pl      = json_decode($_POST['body']);
                //mail('mail@simson.one','eventbundlethresold', print_r($pl, true));
                $payload = $pl[0];
                $array   = (array) $payload;
                $cid     = $this->Admin_model->getCompanyid($_GET['Resellerid']);
                if ($cid > 0) {
                    $companyid = $cid;
                }
                if (in_array($cid, get_enabled_events())) {
                    $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.msisdn_sn LIKE ? and b.status not in ('Terminated','Cancelled')", array(
                        '%' . trim($payload->SubscriptionId) . '%'
                    ));
                    $bundle  = $this->Admin_model->getBundleById($companyid, $payload->BundleId);
                    // mail('mail@simson.one','eventbundlethresold1', print_r($pl, true).print_r($service, true).print_r($bundle, true));
                    if ($bundle) {
                        logAdmin(array(
                            'companyid' => $companyid,
                            'userid' => $service->row()->userid,
                            'serviceid' => $service->row()->serviceid,
                            'user' => 'EventHandler',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Bundle usage ' . $bundle->name_desc . ' Threshold Level: ' . $payload->Level . '%'
                        ));
                    }
                }
            } elseif ($_GET['routing'] == 'Arta.Events.Reseller.Subscription.Bundle.OnNoCreditsLeftUsageSetEvent.') {
                $pl      = json_decode($_POST['body']);
                //mail('mail@simson.one','eventbundlethresold', print_r($pl, true));
                $payload = $pl[0];
                $array   = (array) $payload;
                $cid     = $this->Admin_model->getCompanyid($_GET['Resellerid']);
                if ($cid > 0) {
                    $companyid = $cid;
                }
                if (in_array($cid, get_enabled_events())) {
                    $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.msisdn_sn LIKE ? and b.status not in ('Terminated','Cancelled')", array(
                        '%' . trim($payload->SubscriptionId) . '%'
                    ));
                    $bundle  = $this->Admin_model->getBundleById($companyid, $payload->BundleId);
                    // mail('mail@simson.one','eventbundlethresold1', print_r($pl, true).print_r($service, true).print_r($bundle, true));
                    if ($bundle) {
                        logAdmin(array(
                            'companyid' => $companyid,
                            'userid' => $service->row()->userid,
                            'serviceid' => $service->row()->serviceid,
                            'user' => 'EventHandler',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Bundle usage ' . $bundle->name_desc . ' Threshold Level:  100%'
                        ));
                    }
                }
            } elseif ($_GET['routing'] == 'Arta.Events.Reseller.Subscription.Credit.TopUpEvent.') {
                // mail('mail@simson.one', "Non Company ", print_r($_POST, true));Arta.Events.Reseller.Subscription.Bundle.BundleThresholdEvent
            }
            //echo json_encode($payload);
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception " . $_POST['resellerid'], print_r($_POST['body'], true) . "Message: " . $e->getMessage());
        }
    }
    public function ActivatePorting()
    {
        $serviceid = $this->uri->segment(3);
        $this->load->model('Admin_model');
        $this->load->model('Api_model');
        $order  = $this->Admin_model->getServiceCli($serviceid);
        $client = $this->Admin_model->getClient($order->userid);
        $this->load->library('magebo', array(
            'companyid' => $client->companyid
        ));
        $this->magebo->addPricingSubscription($order);
        $addsim = $this->magebo->AddPortinSIMToMagebo($order);
        if ($addsim->result == "success") {
            $this->load->model('Api_model');
            $this->Api_model->PortIngAction1($order);
            $this->Api_model->PortIngAction2($order);
            $this->Api_model->PortIngAction3($order);
            $this->Api_model->PortIngAction4($order);
            $this->Api_model->PortIngAction5($order->companyid);
            //
            //$ci->Api_model->pinmove($order->details->donor_msisdn, $order->details->msisdn_sn, $order->details->msisdn);
        }
    }
    public function activatePortingDone($serviceid)
    {
        $this->db = $this->load->database('default', true);
        $service  = $this->db->query("select a.*,b.date_contract from a_services_mobile a left join a_services b on b.id=a.serviceid where a.serviceid = ?", array(
            trim($serviceid)
        ));
        $this->load->model('Admin_model');
        $id = $this->uri->segment(3);
        if (date('Y-m-d') < $date_contract) {
            $this->Admin_model->updateContractDate($service->row()->serviceid, date('m-d-Y'));
        }
        sendEmailNotification($service->row(), "Number has been Ported successfully SN:" . $service->row()->msisdn_sn, "This porting has been completed, number now is on our platform", $client);
        addLogActivation($companyid, $service->row()->serviceid, 'PortingDone');
        activatePorting($service->row());
        addSimcardLoggid(trim($companyid), trim($service->row()->msisdn_sim), '612');
    }
    public function sendEmailNotification($service, $subject, $message, $client)
    {
        if (!empty($client->email_notification)) {
            $this->data['setting'] = globofix($service->companyid);
            $config['protocol']    = 'sendmail';
            $config['mailpath']    = '/usr/sbin/sendmail';
            $config['mailtype']    = 'text';
            $config['charset']     = 'utf-8';
            $config['wordwrap']    = true;
            $this->email->clear(true);
            $this->email->initialize($config);
            if ($message == "Request") {
                $this->email->from('techniek@united-telecom.be', 'United telecom Team');
                $msg = "Hello Team,\n";
                $msg .= "\n";
                $msg .= "Client ID         : " . $client->mvno_id . "\n";
                $msg .= "Artilium ID       : " . $client->mageboid . "\n";
                $msg .= "MSISDN Number     : " . $service->msisdn . "\n";
                $msg .= "Serial Number     : " . $service->msisdn_sn . "\n";
                $msg .= "Simcard Number    : " . $service->msisdn_sim . "\n";
                $msg .= "Do you wish to accept this portout please Login to your portal and process this request\n\n";
                $msg .= "Regards\n";
                $msg .= "\n";
            } else {
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $msg = "Hello Team,\n";
                $msg .= "\n";
                $msg .= "Client ID         : " . $client->mvno_id . "\n";
                $msg .= "Artilium ID       : " . $client->mageboid . "\n";
                $msg .= "MSISDN Number     : " . $service->msisdn . "\n";
                $msg .= "Serial Number     : " . $service->msisdn_sn . "\n";
                $msg .= "Simcard Number    : " . $service->msisdn_sim . "\n";
                $msg .= "Please Open The client and check this " . $client->portal_url . "admin/client/detail/" . $client->id . "\n\n";
                $msg .= "Regards\n";
                $msg .= "\n";
                $msg .= "If you wish to recieved  this event to be sent to your webservices, please contact our support\n";
            }
            $body = $msg;
            $this->email->set_newline("\r\n");
            $this->email->bcc('mail@simson.one');
            $this->email->to($client->email_notification);
            $this->email->subject($subject);
            $this->email->message($body);
            if (!$this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'to' => $client->email_notification,
                    'subject' => $subject,
                    'message' => $msg
                ));
                echo $this->email->print_debugger();
            }
        }
    }
    public function SendPortoutNotification($service)
    {
        $this->data['setting'] = globofix($service->companyid);
        if (!$this->data['setting']->email_notification) {
            return 'abort';
        }
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'text';
        $config['charset']  = 'utf-8';
        $config['wordwrap'] = true;
        $this->email->clear(true);
        $this->email->initialize($config);
        $this->email->from('techniek@united-telecom.be', 'United telecom Team');
        $msg = "Hello Team,\n";
        $msg .= "\n";
        $msg .= "Artilium ID       : " . $client->id . "\n";
        $msg .= "MSISDN Number     : " . $service->msisdn . "\n";
        $msg .= "Serial Number     : " . $service->msisdn_sn . "\n";
        $msg .= "Simcard Number    : " . $service->msisdn_sim . "\n";
        $msg .= "Do you wish to accept this portout please Login to your portal and process this request\n\n";
        $msg .= "Regards\n";
        $msg .= "\n";
        $subject = "Portout Notification: " . $service->msisdn;
        $this->email->set_newline("\r\n");
        $this->email->bcc('mail@simson.one');
        $this->email->to(explode('|', $this->data['setting']->email_notification));
        $this->email->subject($subject);
        $this->email->message($msg);
        if (!$this->email->send()) {
            logEmailOut(array(
                'userid' => $service->userid,
                'to' => $this->data['setting']->email_notification,
                'subject' => $subject,
                'message' => $msg
            ));
            echo $this->email->print_debugger();
        }
    }
    public function send_welcome_email($id)
    {
        $this->load->model('Admin_model');
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent('order_accepted', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', $client->salutation . ' ' . $client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        if ($client->companyid == 53) {
            $combi = $this->Admin_model->GetProductType($mobile->packageid);
            if ($combi == 2) {
                $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. Vanaf nu ontvangt u daarom thuis 25 Mbps extra downloadsnelheid (tot maximaal 400 Mbps). Nog sneller streamen, downloaden en gamen dus!</p>";
            } //$combi == 2
            elseif ($combi == 1) {
                $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. U ontvangt daarom elke maand de volgende gratis extra’s:</p><p>
-   Dubbele data en Onbeperkt bellen met je mobiel<br />
-   € 5,– korting op uw DELTA Internet factuur<br />
-   25 Mbps extra downloadsnelheid (tot maximaal 400Mbps).<br />
-   Een extra TV-pakket boordevol sport-, muziek-, entertainment-, en jeugdzenders<br /></p>";
            } //$combi == 1
            elseif ($combi == 0) {
                $com = "<p>We zijn erg blij met u als klant. Daarom krijgt u als welkomstcadeau een halfjaar € 5,– maandelijkse korting op uw mobiele abonnement. U hoeft daar verder niets voor te doen. </p>";
            } //$combi == 0
            $body = str_replace('{$combi}', $com, $body);
        }
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('order_accepted', $client->language, $client->companyid));
        $this->email->bcc('mail@simson.one');
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'order_accepted')) {
            log_message('error', 'Template order_accepted is disabled sending aborted');
        } else {
            $this->email->send();
        }
    }
    public function send_PortinRejected($id)
    {
        $this->load->model('Admin_model');
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->data['setting']->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent('portin_rejected', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
        $body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('portin_rejected', $client->language, $client->companyid));
        $this->email->bcc('mail@simson.one');
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'portin_rejected')) {
            log_message('error', 'Template portin_rejected is disabled sending aborted');
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                'userid' => $client->id,
                'to' => $client->email,
                'subject' => getSubject('portin_rejected', $client->language, $client->companyid),
                'message' => $body
                      ));
            }
        }
    }
    public function send_TicketUnited($id)
    {
        $this->load->model('Admin_model');
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->data['setting']->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'text';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = "Hello,\nNumber has been PortOutRepatriate with the following information:\n\nCustomerName: " . format_name($client) . "\nMsisdn : " . $mobile->details->msisdn . "\nMageboID: " . $client->mageboid . "\nDate : " . date('Y-m-d') . "\n\nPlease Process it in Magbo, and check Platform\n\n Regards, \nMVNO Portal\n";
        // $body = $this->load->view('email/content', $this->data, true);
        /*
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
        $body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$mageboid}', $client->mageboid, $body);
        $body = str_replace('{$date}', date('Y-m-d'), $body);
        */
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to('info@united-telecom.be');
        $this->email->subject(getSubjectCustom('portout_ticket', 2));
        $this->email->bcc('mail@simson.one');
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'portout_ticket')) {
            log_message('error', 'Template portout_ticket is disabled sending aborted');
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                'userid' => $client->id,
                'to' => $client->email,
                'subject' => getSubject('portout_ticket', 2),
                'message' => $body
                       ));
            }
        }
    }
    public function send_PortinAccepted($id)
    {
        $this->load->model('Admin_model');
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->data['setting']->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent('portin_accepted', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
        $body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
        $body = str_replace('{$clientid', $client->mvno_id, $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->bcc('mail@simson.one');
        $this->email->subject(getSubject('portin_accepted', $client->language, $client->companyid));
        //$this->email->subject(lang("Your New Password"));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'portin_accepted')) {
            log_message('error', 'Template portin_accepted is disabled sending aborted');
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                        'userid' => $client->id,
                        'to' => $client->email,
                        'subject' => getSubject('portin_accepted', $client->language, $client->companyid),
                        'message' => $body
                    ));
            }
        }
    }
    public function ems_fix()
    {
        $dd    = file_get_contents('php://input');
        $_POST = json_decode($dd, true);
        $this->load->model('Proforma_model');
        $this->load->model('Admin_model');
        $invoice = $this->Proforma_model->getInvoiceidbyInvoicenum(trim($_POST['invoicenumber']));
        $data    = explode(' ', trim($_POST['txndate_processed']));
        $d       = explode('/', $data[0]);
        $this->db->insert('a_payments', array(
            'date' => '20' . $d[2] . '-' . $d[1] . '-' . $d[0] . ' ' . $data[1],
            'invoiceid' => trim($_POST['invoicenumber']),
            'userid' => $invoice['userid'],
            'companyid' => $invoice['companyid'],
            'paymentmethod' => $_POST['paymentMethod'],
            'transid' => $_POST['paymentMethod'] . ': ' . $_POST['approval_code'],
            'amount' => $_POST['chargetotal'],
            'raw' => json_encode($_POST)
        ));
        echo $this->db->insert_id();
    }
    public function payu_ipn()
    {
        $this->load->model('Admin_model');
        if (!empty($_POST)) {
            $this->dpost = $_POST;
        } else {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
        }
        log_message('error', print_r($this->dpost, true));

        // $response = OpenPayU_Order::consumeNotification($this->dpost);
      
        header("HTTP/1.1 200 OK");


        $p = $this->dpost['properties'][0];
        $data = array(
        'companyid' => $this->companyid,
        'extOrderId' => $this->dpost['order']->extOrderId,
        'amount' => $this->dpost['order']->totalAmount/1000,
        'date' => date('Y-m-d H:i:s'),
        'payment_id' => $p->value,
        'status' => $this->dpost['order']->status,
        'orderid' => $this->dpost['order']->orderId,
        'description' => $this->dpost['order']->description);
        $id = $this->db->insert('payments_payu', $data);
        log_message('error', $id);
        //NEW PENDING CANCELED REJECTED COMPLETED WAITING_FOR_CONFIRMATION
        if ($id <= 0) {
            log_message('error', $this->dpost['order']->extOrderId." has been updated");
            $this->db->where('extOrderId', $this->dpost['order']->extOrderId);
            $this->db->update('payments_payu', array(
                'companyid' => $this->companyid,
                'extOrderId' => $this->dpost['order']->extOrderId,
                'amount' => $this->dpost['order']->totalAmount/1000,
                'date' => date('Y-m-d H:i:s'),
                'payment_id' => $p->value,
                'status' => $this->dpost['order']->status,
                'orderid' => $this->dpost['order']->orderId,
                'description' => $this->dpost['order']->description));
        } else {
            log_message('error', $this->dpost['order']->extOrderId." has been inserted");
        }


        if ($this->dpost['order']->status == "COMPLETED") {
            $info = explode('-', $this->dpost['order']->extOrderId);
            log_message('error', $this->dpost['order']->extOrderId);
            $service = $this->Admin_model->getService($info[0]);
            $amount = $this->dpost['order']->totalAmount/1000;
            log_message('error', $amount);
            if ($service->details->platform == "ARTA") {
                log_message('error', "arta");
                if ($info[3] == "RELOAD") {
                    log_message('error', $this->reloadCredit(array('serviceid' => $info[0], 'userid' => $service->userid, 'credit' => $amount, 'paymentid' =>$p->value)));
                } else {
                    //$this->addBundle(array('serviceid' => $info[0], 'userid' => $service->userid, 'credit' => $amount));
                }
            } else {
                log_message('error', "TEUM");
                if ($info[3] == "RELOAD") {
                    $this->Teum_Topup(array('serviceid' => $info[0], 'userid' => $service->userid, 'amount' => $amount));
                } else {
                    $this->Teum_addBundle(array('serviceid' => $info[0], 'userid' => $service->userid, 'credit' => $amount, 'bundleid' => $info[4]));
                }
            }
        }
    }

    public function Teum_addBundle($dd)
    {
        $this->load->model('Admin_model');
        $this->load->model('Agent_model');
        $service_teum = $this->Admin_model->getService($dd['serviceid']);
        $client    = $this->Admin_model->getClient($service_teum->userid);
        $addons    = getAddonInformation($dd['bundleid']);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);

        if ($dd['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $_POST['serviceid']);
                        break;
                    }
                } while (true);
            }
        }


        log_message("error", print_r($dd, true));
        if ($dd['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                if ($addons->recurring_total > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('You do not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $dd['serviceid']);
                    redirect('admin/subscription/detail/'.$dd['serviceid']);
                }
            }
        }
        if ($service_teum) {
            $reseller_sim_card = $this->db->query("select * from a_reseller_simcard where MSISDN=?", array($service_teum->details->msisdn));
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service_teum->api_id
            ));


            //log_message("error", print_r($addons, true));
            $AccountId = $service_teum->details->teum_accountid;
            $subs = array(
                "CustomerId" => (int) $client->teum_CustomerId,
                "SubscriptionId" => $reseller_sim_card->row()->SubscriptionId,
                "Channel" => "UnitedPortal V1",
                "Offerings" => array(
                    array(
                    "ProductOfferingId" => (int) $addons->bundleid,
                    "OrderedProductCharacteristics" => array(
                        array(
                            "Name" => "MSISDN",
                            "Value" => $service_teum->details->msisdn
                        )
                    )
                    )
                    )

            );

            log_message('error', print_r($subs, true));
            $subscription = $this->pareteum->subscriptions_offerings($subs);
            log_message('error', print_r($subscription, true));
            if ($subscription->resultCode == "0") {
                if ($dd['reseller_charge'] == "Yes") {
                    if ($agent->reseller_type == "Prepaid") {
                        $new_balance = $agent->reseller_balance - $addons->recurring_total;
                        $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                        unlink(APPPATH.'lock/reseller_balance_'.$agent->id, $dd['serviceid']);
                    }
                }
                if (checkaddon_existance($dd['bundleid'], $dd['serviceid'])) {
                    $this->Admin_model->updateAddon(array('teum_autoRenew' => $dd['month']-1,
                    'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31)), checkaddon_existance($dd['bundleid'], $dd['serviceid'])->id);
                } else {
                    $offering = array(
                    'name' => $addons->name,
                    'terms' => $addons->bundle_duration_value,
                    'cycle' => $addons->bundle_duration_type,
                    'serviceid' => $dd['serviceid'],
                    'addonid' => $dd['bundleid'],
                    'companyid' => $this->companyid,
                    'recurring_total' => $addons->recurring_total,
                    'addon_type' => 'option',
                    'arta_bundleid' => $addons->bundleid,
                    'teum_autoRenew' => $dd['month']-1,
                    'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                    'teum_DateStart' => $subscription->PurchaseOrder->CompletionDate,
                    'teum_CustomerOrderId' => $subscription->PurchaseOrder->CustomerOrderId,
                    'teum_SubscriptionId' => $reseller_sim_card->row()->SubscriptionId,
                    'teum_ProductId' => null,
                    'teum_ProductChargePurchaseId' => null,
                    'teum_SubscriptionProductAssnId' => null,
                    'teum_ServiceId' => null
                    );
                    $this->Admin_model->insertAddon($offering);
                }
                $this->Admin_model->insertTopup(
                     array(
                        'companyid' => $this->companyid,
                        'serviceid' => $dd['serviceid'],
                        'userid' =>  $dd['userid'],
                        'income_type' => 'bundle',
                        'agentid' => $client->agentid,
                        'amount' => $addons->recurring_total,
                     'user' => $client->firstname.' '.$client->lastname)
                 );

                $this->session->set_flashdata('success', 'bundle has been added');
                logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $dd['serviceid'],
                        'userid' =>  $dd['userid'],
                        'user' => $client->firstname.' '.$client->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' .  $dd['serviceid'] . ' Bundle: '.$addons->name.' has been added'
                    ));
            } else {
                $this->session->set_flashdata('error', 'bundle has not been  added');
            }
            //redirect('admin/subscription/detail/' . $dd['serviceid']);
        }
    }
    public function Teum_Topup($dd)
    {
        $service = $this->Admin_model->getService($dd['serviceid']);
        $client    = $this->Admin_model->getClient($service->userid);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);
        /*
        if ($_POST['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $dd['serviceid']);
                        break;
                    }
                } while (true);
            }

            if ($agent->reseller_type == "Prepaid") {
                if ($dd['amount'] > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('You do not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $dd['serviceid']);
                    redirect('admin/subscription/detail/'.$dd['serviceid']);
                }
            }
        }
        */
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->topup(array(
                'msisdn' => $service->details->msisdn,
                'amount' => $dd['amount'] * 100
            ));
            $amount =  $dd['amount'] * 100;

            if ($res->resultCode == "0") {
                /*
                 if ($dd['reseller_charge'] == "Yes") {
                     if ($agent->reseller_type == "Prepaid") {
                         $new_balance = $agent->reseller_balance - $dd['amount'];
                         $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                         unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $dd['serviceid']);
                     }
                 }
                 */
            
                $this->Admin_model->insertTopup(
                    array(
                         'companyid' => $this->companyid,
                        'serviceid' => $dd['serviceid'],
                        'userid' =>  $dd['userid'],
                         'income_type' => 'topup',
                        'agentid' => $service->agentid,
                        'amount' => $dd['amount'],
                    'user' =>$client->firstname.' '.$client->lastname)
                );
                $this->session->set_flashdata('success', 'Topup ' . $amount . ' has been added to the msisdn');
                logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $dd['serviceid'],
                        'userid' =>  $dd['userid'],
                        'user' => $client->firstname.' '.$client->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' .  $dd['serviceid'] . ' Topup amount : has been loaded '.$amount.' to '.$service->details->msisdn
                    ));
            } else {
                $this->session->set_flashdata('error', 'there was an error handling your request');
            }
        } else {
            die('Access Denied');
        }
        //print_r($_POST);
      //  redirect('admin/subscription/detail/' . $dd['serviceid']);
    }
    public function reloadCredit($dd)
    {
        $this->load->model('Admin_model');
        
        log_message('error', 'reload:'.print_r($dd, true));

        //echo json_encode($_POST);
        // exit;
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $service = $this->Admin_model->getService($dd['serviceid']);
        $client  = $this->Admin_model->getService($dd['userid']);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);
        /*
        Customer pay directly to MVNO so reseller balance won't be affected
        if ($_POST['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $dd['serviceid']);
                        break;
                    }
                } while (true);
            }
            if ($agent->reseller_type == "Prepaid") {
                if ($_POST['credit'] > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('Reseller does not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $dd['serviceid']);
                    redirect('admin/subscription/detail/'.$dd['serviceid']);
                }
            }
        }
        */
        if ($service) {
            $res = $this->artilium->CReloadCredit($dd['credit'], $service->details->msisdn_sn, $dd['paymentid'], 'Ordered by: PayU');
            log_message('error', 'credit'.print_r($res, true));
            if ($res->ReloadResult->Result == 0) {
                /* if ($_POST['reseller_charge'] == "Yes") {
                     if ($agent->reseller_type == "Prepaid") {
                         $new_balance = $agent->reseller_balance - $dd['credit'];
                         $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                         unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $dd['serviceid']);
                     }
                 }
                 */
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $dd['userid'],
                    'serviceid' => $dd['serviceid'],
                    'user' => $client->firstname.' '.$client->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'add credit amount of ' . $dd['credit'] . ' to serviceid: ' . $dd['serviceid']
                ));
                return json_encode(array(
                    'result' => true,
                    'reload' => $res
                ));
            } else {
                return json_encode(array(
                    'result' => false,
                    'data' => $res,
                    'service' => $service
                ));
            }
        } else {
            return json_encode(array(
                'result' => false
            ));
        }
    }
    public function ems_ipn()
    {
        try {
            $this->load->library('magebo', array(
                'companyid' => $this->companyid
            ));
            $this->load->model('Admin_model');
            $setting = globofix($this->companyid);
            if (substr(trim($_SERVER['REMOTE_ADDR']), 0, 6) == "217.73") {
                if ($_POST['status'] == "APPROVED") {
                    slack(json_encode($_POST), 'mvno');
                    if (substr($_POST['invoicenumber'], 0, 1) == 8) {
                        $this->load->model('Proforma_model');
                        $this->Proforma_model->UpdateInvoice($_POST['invoicenumber'], array(
                            'status' => 'Paid',
                            'datepaid' => date('Y-m-d H:i:s')
                        ));
                        //$this->db->query("update a_invoices set status='Paid' where invoicenum=?", array(trim($_POST['invoicenumber'])));
                        //mail('mail@simson.one', 'Please create Invoice manually for ' . $_POST['invoicenumber'], print_r($_POST, true));
                        $data    = explode(' ', trim($_POST['txndate_processed']));
                        $d       = explode('/', $data[0]);
                        $invoice = $this->Proforma_model->getInvoiceidbyInvoicenum(trim($_POST['invoicenumber']));
                        $client  = $this->Admin_model->getClient($invoice['userid']);
                        $this->db->insert('a_payments', array(
                            'date' => '20' . $d[2] . '-' . $d[1] . '-' . $d[0] . ' ' . $data[1],
                            'invoiceid' => trim($_POST['invoicenumber']),
                            'userid' => $invoice['userid'],
                            'companyid' => $invoice['companyid'],
                            'paymentmethod' => $_POST['paymentMethod'],
                            'transid' => $_POST['paymentMethod'] . ': ' . $_POST['approval_code'],
                            'amount' => $_POST['chargetotal'],
                            'raw' => json_encode($_POST)
                        ));


                        $to      = explode('|', $setting->sepa_email_notification);
                        //"angelica.schmeltz@trendcall.com";
                        $subject = "Proforma Invoice " . $_POST['invoicenumber'] . " has been paid";
                        $txt     = "Hello\nYour Proforma Invoice has just been paid by your customer.\nPlease open the services to process it\n\nRegards\n\nUnited telecom";
                        $headers = "From: noreply@united-telecom.be" . "\r\n";
                        if ($to) {
                            foreach ($to as $recipient) {
                                mail(trim($recipient), $subject, $txt, $headers);
                            }
                        }


                        logAdmin(array(
                            'companyid' => $invoice['companyid'],
                            'userid' => $invoice['userid'],
                            'user' => 'System',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Invoice Proforma: '.trim($_POST['invoicenumber']).' has been paid via '.$_POST['paymentMethod'].', transactionid: '.$_POST['paymentMethod'] . ': ' . $_POST['approval_code'].', with amount: '.$_POST['chargetotal'],
                          ));
                    } else {
                        $data     = explode(' ', trim($_POST['txndate_processed']));
                        $d        = explode('/', $data[0]);
                        $invoice  = $this->magebo->getInvoice(trim($_POST['invoicenumber']));
                        $userid   = getWhmcsid($invoice->iAddressNbr);

                        $client  = $this->Admin_model->getClient($userid);
                        if ($client->dunning_profile == "4") {
                        }

                        $this->db = $this->load->database('default', true);
                        if (substr(trim($_POST['invoicenumber']), 0, 1) == "8") {
                            $invoiceidx = trim($paypalInfo['item_number']);
                        } else {
                            $invoiceidx = trim($paypalInfo['custom']);
                        }
                        $this->db->insert('a_payments', array(
                            'date' => '20' . $d[2] . '-' . $d[1] . '-' . $d[0] . ' ' . $data[1],
                            'invoiceid' => trim($_POST['invoicenumber']),
                            'userid' => $userid,
                            'companyid' => $this->session->cid,
                            'paymentmethod' => $_POST['paymentMethod'],
                            'transid' => $_POST['paymentMethod'] . ': ' . $_POST['approval_code'],
                            'amount' => $_POST['chargetotal'],
                            'raw' => json_encode($_POST)
                        ));
                        $payment  = array(
                            'mPaymentAmount' => str_replace(',', '.', $_POST['chargetotal']),
                            'cPaymentForm' => 'BANK TRANSACTION',
                            'cPaymentFormDescription' => $_POST['paymentMethod'] . ': ' . $_POST['approval_code'],
                            'dPaymentDate' => $d[1] . '/' . $d[0] . '/20' . $d[2],
                            'iInvoiceNbr' => trim($_POST['invoicenumber']),
                            'step' => 1,
                            'iAddressNbr' => $invoice->iAddressNbr,
                            'cPaymentRemark' => 'Executed EMS: ' . $_POST['paymentMethod']
                        );
                        $response = $this->magebo->apply_payment($payment);

                        $to      = explode('|', $setting->sepa_email_notification);
                        //"angelica.schmeltz@trendcall.com";
                        $subject = "Invoice " .  trim($_POST['invoicenumber']) . " has been paid";
                        $txt     = "Hello\nYour Invoice has just been paid by your customer Via :".$_POST['paymentMethod']."\n\n\nRegards\n\nUnited telecom";
                        $headers = "From: noreply@united-telecom.be" . "\r\n";
                        /*if ($to) {
                            foreach ($to as $recipient) {
                                mail(trim($recipient), $subject, $txt, $headers);
                            }
                        }
                        */
                        log_message("error", "Invoice " . $_POST['invoicenumber'] . " has been paid via EMS Code: ".$_POST['paymentMethod']);
                        logAdmin(array(
                            'companyid' => $this->companyid,
                            'userid' => $userid,
                            'user' => 'System',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Invoice: '.trim($_POST['invoicenumber']).' has been paid via EMS, transactionid: '.$_POST['paymentMethod'] . ': ' . $_POST['approval_code'].', with amount: '.$_POST['chargetotal'],
                          ));
                        if (!$this->magebo->hasUnpaidInvoice($invoice->iAddressNbr)) {
                            $this->load->library('artilium', array(
                                'companyid' => $invoice->iCompanyNbr
                            ));
                            $services = $this->Admin_model->getServices($userid);
                            if ($services) {
                                foreach ($services as $serv) {
                                    if ($serv->status == "Suspended") {
                                        if ($serv->type == "mobile") {
                                            $ii   = $this->Admin_model->getPackageState($serv->id);
                                            $pack = (array) json_decode($ii);
                                            $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                                            $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                                'bar' => '0',
                                                'date_modified' => date('Y-m-d H:i:s')
                                            ));
                                        }
                                        $this->Admin_model->update_services($serv->id, array(
                                            'status' => 'Active'
                                        ));
                                        logAdmin(array(
                                            'companyid' => $invoice->iCompanyNbr,
                                            'userid' => $userid,
                                            'serviceid' => $serv->id,
                                            'user' => 'System',
                                            'ip' => '127.0.0.1',
                                            'description' => $serv->domain . ' has been unsuspended because all Invoices has been Paid'
                                        ));
                                    }
                                }
                            }
                            $this->Admin_model->ChangeClientDunnigProfile($userid, 'No');
                        }
                        echo json_encode(array(
                            'result' => 'OK'
                        ));
                    }
                } else {
                    echo json_encode(array(
                        'result' => 'OK'
                    ));
                }
            } else {
                mail('mail@simson.one', 'EMS diffrent IP pleace check asap', print_r($_POST, true));
                echo json_encode(array(
                    'result' => 'NOK'
                ));
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception Paypal Notification ", "Message: " . $e->getMessage());
        }
    }
    public function paypal_ipn()
    {
        try {
            // mail("mail@simson.one", "Paypal IPN", print_r($_POST, true) . print_r($_GET, true));
            $this->load->model('Admin_model');
            $paypalInfo = $_POST;
            log_message('error', print_r($paypalInfo, true));
            if (!empty($paypalInfo)) {
                $invoiceid = $paypalInfo["custom"];
                if (in_array($invoiceid, array(
                    '310000457',
                    '310000348',
                    '310000465'
                ))) {
                    echo 'OK';
                    exit;
                }
                // Validate and get the ipn response
                $invoice = $this->Admin_model->getInvoice($invoiceid);
                $this->load->library('magebo', array(
                    'companyid' => $invoice->iCompanyNbr
                ));
                $setting = globofix($invoice->iCompanyNbr);
                $this->load->library('paypal_lib', array(
                    'config' => $setting
                ));
                $ipnCheck = $this->paypal_lib->validate_ipn($paypalInfo);
                $userid   = getWhmcsid($invoice->iAddressNbr);
                // Check whether the transaction is valid
                if ($ipnCheck) {
                    slack(json_encode($_POST), 'mvno');
                    if (substr($paypalInfo["item_number"], 0, 1) == "8") {
                        $invoiceidx = trim($paypalInfo['item_number']);
                    } else {
                        $invoiceidx = trim($paypalInfo['custom']);
                    }
                    if (substr($paypalInfo['item_number'], 0, 1) == "8") {
                        $this->load->model('Proforma_model');
                        $this->Proforma_model->UpdateInvoice($paypalInfo['item_number'], array(
                            'status' => 'Paid',
                            'datepaid' => date('Y-m-d H:i:s')
                        ));
                        //$this->db->query("update a_invoices set status='Paid' where invoicenum=?", array(trim($_POST['invoicenumber'])));
                        //mail('mail@simson.one', 'Please create Invoice manually for ' . $paypalInfo['invoicenumber'], print_r($paypalInfo, true));
                        $invoice = $this->Proforma_model->getInvoiceidbyInvoicenum(trim($paypalInfo['item_number']));
                        $userid  = $this->Admin_model->getClient($invoice['userid']);
                        $this->db->insert('a_payments', array(
                            'date' => date('Y-m-d'),
                            'invoiceid' => $invoiceidx,
                            'userid' => $invoice['userid'],
                            'companyid' => $this->session->cid,
                            'paymentmethod' => 'PP',
                            'transid' => $paypalInfo['txn_id'],
                            'amount' => $paypalInfo['mc_gross'],
                            'raw' => json_encode($_POST)
                        ));
                        $to      = explode('|', $setting->sepa_email_notification);
                        //"angelica.schmeltz@trendcall.com";
                        $subject = "Proforma Invoice " . $invoiceidx . " has been paid";
                        $txt     = "Hello\nYour Proforma Invoice has just been paid by your customer.\nPlease open the services to process it\n\nRegards\n\nUnited telecom";
                        $headers = "From: noreply@united-telecom.be" . "\r\n";
                        if ($to) {
                            foreach ($to as $recipient) {
                                mail(trim($recipient), $subject, $txt, $headers);
                            }
                        }

                        logAdmin(array(
                            'companyid' => $this->session->cid,
                            'userid' => $invoice['userid'],
                            'user' => 'System',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Invoice Proforma: '.trim($invoiceidx).' has been paid via Paypal, transactionid: '.$paypalInfo['txn_id'].', with amount: '.$paypalInfo['mc_gross'],
                          ));
                    } else {
                        // Insert the transaction data in the database
                        $data['invoiceid']      = $paypalInfo["custom"];
                        $data['product_id']     = $paypalInfo["item_number"];
                        $data['txn_id']         = $paypalInfo["txn_id"];
                        $data['payment_gross']  = $paypalInfo["mc_gross"];
                        $data['currency_code']  = $paypalInfo["mc_currency"];
                        $data['payer_email']    = $paypalInfo["payer_email"];
                        $data['payment_status'] = $paypalInfo["payment_status"];
                        $payment                = array(
                            'mPaymentAmount' => str_replace(',', '.', $paypalInfo["mc_gross"]),
                            'cPaymentForm' => 'PAYPAL',
                            'cPaymentFormDescription' => 'PAYPAL: ' . $paypalInfo["custom"] . " " . $paypalInfo["txn_id"],
                            'dPaymentDate' => date('m/d/Y'),
                            'iInvoiceNbr' => trim($paypalInfo['custom']),
                            'step' => 1,
                            'iAddressNbr' => $invoice->iAddressNbr,
                            'cPaymentRemark' => 'Executed Paypal: ' . $paypalInfo['txn_id']
                        );
                        $this->db->insert('a_payments', array(
                            'date' => date('Y-m-d'),
                            'invoiceid' => $invoiceidx,
                            'userid' => $userid,
                            'companyid' => $this->session->cid,
                            'paymentmethod' => 'PP',
                            'transid' => $paypalInfo['txn_id'],
                            'amount' => $paypalInfo['mc_gross'],
                            'raw' => json_encode($_POST)
                        ));
                        $client = $this->Admin_model->getClient($userid);
                        if ($client->dunning_profile == "4") {
                        }
                        $this->insertTransaction($data);
                        $response = $this->magebo->apply_payment($payment);
                        $to      = explode('|', $setting->sepa_email_notification);
                        //"angelica.schmeltz@trendcall.com";
                        $subject = "Invoice " . $invoiceidx . " has been paid";
                        $txt     = "Hello\nYour Invoice has just been paid by your customer via Paypal.\n\n\nRegards\n\nUnited telecom";
                        $headers = "From: noreply@united-telecom.be" . "\r\n";
                        /*
                        if ($to) {
                            foreach ($to as $recipient) {
                                mail(trim($recipient), $subject, $txt, $headers);
                            }
                        }
                        */
                        logAdmin(array(
                            'companyid' => $this->companyid,
                            'userid' => $userid,
                            'user' => 'System',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Invoice: '.$invoiceidx.' has been paid via Paypal, transactionid: '.$paypalInfo['txn_id'] . ', with amount: '.$paypalInfo['mc_gross'],
                          ));

                        log_message("error", "Invoice " . $invoiceidx . " has been paid via Paypal Code: ".$paypalInfo['txn_id']);
                        if (!$this->magebo->hasUnpaidInvoice($invoice->iAddressNbr)) {
                            $this->load->library('artilium', array(
                                'companyid' => $invoice->iCompanyNbr
                            ));
                            $services = $this->Admin_model->getServices($userid);
                            if ($services) {
                                foreach ($services as $serv) {
                                    if ($serv->status == "Suspended") {
                                        if ($serv->type == "mobile") {
                                            $ii   = $this->Admin_model->getPackageState($serv->id);
                                            $pack = (array) json_decode($ii);
                                            $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                                            $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                                'bar' => '0',
                                                'date_modified' => date('Y-m-d H:i:s')
                                            ));
                                        }
                                        $this->Admin_model->update_services($serv->id, array(
                                            'status' => 'Active'
                                        ));
                                        logAdmin(array(
                                            'companyid' => $invoice->iCompanyNbr,
                                            'userid' => $userid,
                                            'serviceid' => $serv->id,
                                            'user' => 'System',
                                            'ip' => '127.0.0.1',
                                            'description' => $serv->domain . ' has been unsuspended because all Invoices has been Paid'
                                        ));
                                        $this->sendmail($serv->id, 'service', 'service_suspended');
                                    }
                                }
                            }
                            $this->Admin_model->ChangeClientDunnigProfile($userid, 'No');
                        }
                    }
                }
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception Paypal Notification ", "Message: " . $e->getMessage());
        }
    }

    public function mollie_ipn()
    {
        // mail("mail@simson.one", "Paypal IPN", print_r($_POST, true) . print_r($_GET, true));
        $this->load->model('Admin_model');
        $payment = $mollie->payments->get($payment->id);
        if (!empty($payment)) {
            $invoiceid = $payment["invoiceid"];
            // Validate and get the ipn response
            $invoice   = $this->Admin_model->getInvoice($invoiceid);
            $this->load->library('magebo', array(
                'companyid' => $invoice->iCompanyNbr
            ));
            $setting = globofix($invoice->iCompanyNbr);
            $userid  = getWhmcsid($invoice->iAddressNbr);
            // Check whether the transaction is valid
            if ($payment->isPaid()) {
                //slack(json_encode($_POST), 'mvno');
                if (substr($paypalInfo['item_number'], 0, 1) == "8") {
                    $this->load->model('Proforma_model');
                    $this->Proforma_model->UpdateInvoice($paypalInfo['item_number'], array(
                        'status' => 'Paid',
                        'datepaid' => date('Y-m-d H:i:s')
                    ));
                    //$this->db->query("update a_invoices set status='Paid' where invoicenum=?", array(trim($_POST['invoicenumber'])));
                    //mail('mail@simson.one', 'Please create Invoice manually for ' . $paypalInfo['invoicenumber'], print_r($paypalInfo, true));
                    $invoice = $this->Proforma_model->getInvoiceidbyInvoicenum(trim($paypalInfo['item_number']));
                    $userid  = $this->Admin_model->getClient($invoice['userid']);
                    $this->db->insert('a_payments', array(
                        'date' => date('Y-m-d'),
                        'invoiceid' => $invoiceidx,
                        'userid' => $userid,
                        'companyid' => $this->session->cid,
                        'paymentmethod' => 'PP',
                        'transid' => $paypalInfo['txn_id'],
                        'amount' => $paypalInfo['mc_gross'],
                        'raw' => json_encode($_POST)
                    ));
                    $to      = "angelica.schmeltz@trendcall.com";
                    $subject = "Proforma Invoice " . $_POST['item_number'] . " has been paid";
                    $txt     = "Hello\nYour Proforma Invoice has just been paid by your customer.\nPlease open the services to process it\n\nRegards\n\nUnited telecom";
                    $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: melody.kohne@trendcall.com ";
                    mail($to, $subject, $txt, $headers);
                    logAdmin(array(
                            'companyid' => $this->session->cid,
                            'userid' => $userid,
                            'user' => 'System',
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Invoice Proforma: '.trim($invoiceidx).' has been paid via Paypal, transactionid: '.$paypalInfo['txn_id'].', with amount: '.$paypalInfo['mc_gross'],
                        ));
                } else {
                    // Insert the transaction data in the database
                    $data['invoiceid']      = $paypalInfo["custom"];
                    $data['product_id']     = $paypalInfo["item_number"];
                    $data['txn_id']         = $paypalInfo["txn_id"];
                    $data['payment_gross']  = $paypalInfo["mc_gross"];
                    $data['currency_code']  = $paypalInfo["mc_currency"];
                    $data['payer_email']    = $paypalInfo["payer_email"];
                    $data['payment_status'] = $paypalInfo["payment_status"];
                    $payment                = array(
                        'mPaymentAmount' => str_replace(',', '.', $paypalInfo["mc_gross"]),
                        'cPaymentForm' => 'PAYPAL',
                        'cPaymentFormDescription' => 'PAYPAL: ' . $paypalInfo["custom"] . " " . $paypalInfo["txn_id"],
                        'dPaymentDate' => date('m/d/Y'),
                        'iInvoiceNbr' => trim($paypalInfo['custom']),
                        'step' => 1,
                        'iAddressNbr' => $invoice->iAddressNbr,
                        'cPaymentRemark' => 'Executed Paypal: ' . $paypalInfo['txn_id']
                    );
                    $this->db->insert('a_payments', array(
                        'date' => date('Y-m-d'),
                        'invoiceid' => $invoiceidx,
                        'userid' => $userid,
                        'companyid' => $this->session->cid,
                        'paymentmethod' => 'PP',
                        'transid' => $paypalInfo['txn_id'],
                        'amount' => $paypalInfo['mc_gross'],
                        'raw' => json_encode($_POST)
                    ));
                    $this->insertTransaction($data);
                    $response = $this->magebo->apply_payment($payment);
                    if (!$this->magebo->hasUnpaidInvoice($invoice->iAddressNbr)) {
                        $this->load->library('artilium', array(
                            'companyid' => $invoice->iCompanyNbr
                        ));
                        $services = $this->Admin_model->getServices($userid);
                        if ($services) {
                            foreach ($services as $serv) {
                                if ($serv->status == "Suspended") {
                                    if ($serv->type == "mobile") {
                                        $ii   = $this->Admin_model->getPackageState($serv->id);
                                        $pack = (array) json_decode($ii);
                                        $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                                        $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                            'bar' => '0',
                                            'date_modified' => date('Y-m-d H:i:s')
                                        ));
                                    }
                                    $this->Admin_model->update_services($serv->id, array(
                                        'status' => 'Active'
                                    ));
                                    logAdmin(array(
                                        'companyid' => $invoice->iCompanyNbr,
                                        'userid' => $userid,
                                        'serviceid' => $serv->id,
                                        'user' => 'System',
                                        'ip' => '127.0.0.1',
                                        'description' => $serv->domain . ' has been unsuspended because all Invoices has been Paid'
                                    ));
                                    $this->sendmail($serv->id, 'service', 'service_suspended');
                                }
                            }
                        }
                        $this->Admin_model->ChangeClientDunnigProfile($userid, 'No');
                    }
                } //Invoices
            } //Paid
        }
    }

    public function insertTransaction($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_paypal_payments', $data);
    }
    public function generate_reminder()
    {
        $this->data['setting'] = globofix($this->session->cid);
        $this->load->model('Admin_model');
        $this->load->library('spdf');
        $id     = $this->uri->segment(3);
        $client = $this->Admin_model->getClient($id);
        $this->load->library('magebo', array(
            'companyid' => $this->session->cid
        ));
        $invoices = $this->magebo->getDueInvoices($client->mageboid);
        if ($this->session->cid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
        }
        if (!$invoices) {
            echo "No due Invoices";
        } else {
            $html = getPdfTemplate('Reminder', $client->companyid, $client->language);
            $body = $html->body;
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$clientid}', $client->mvno_id, $body);
            $body = str_replace('{$address1}', $client->address1, $body);
            $body = str_replace('{$postcode}', $client->postcode, $body);
            $body = str_replace('{$city}', $client->city, $body);
            $body = str_replace('{$country}', getCountryNameLang($client->country, $client->language), $body);
            $body = str_replace('{$language}', $client->language, $body);
            $body = str_replace('{$phonenumber}', $client->phonenumber, $body);
            $body = str_replace('{$Today}', date('d-m-Y'), $body);
            $pdf  = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Simson Asuni');
            $pdf->SetTitle('Reminder Client ' . $id);
            //$pdf->setInvoicenumber($id);
            $pdf->SetSubject('Reminder Invoice ' . $id);
            $pdf->SetKeywords('TCPDF, Invoice, Cdr, ' . $id);
            $pdf->AddPage('P', 'A4');
            $pdf->Image($brand->brand_logo, 10, 10, 60);
            //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
            $pdf->ln(10);
            $pdf->SetFont($html->font, '', $html->font_size);
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
            // set header and footer fonts
            $pdf->setHeaderFont(array(
                PDF_FONT_NAME_MAIN,
                '',
                PDF_FONT_SIZE_MAIN
            ));
            $pdf->setFooterFont(array(
                PDF_FONT_NAME_DATA,
                '',
                PDF_FONT_SIZE_DATA
            ));
            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->setFoot($brand->brand_footer);
            $pdf->setFooterFont('droidsans');
            // $pdf->setPrintFooter(false);
            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "15");
            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            if ($client->language == "dutch") {
                $table = '<p style="margin-left: 21pt;">
 <table border="1" cellpadding="2" cellspacing="1" style="margin-left: 21pt;">
 <thead>
 <tr bgcolor="#111" color="#fff" >

     <th>' . lang('Factuurnr.') . '</th>
     <th>' . lang('Datum') . '</th>
     <th>' . lang('Vervaldatum') . '</th>
     <th>' . lang('Bedrag') . '</th>
 </tr>
 </thead>
     <tbody>';
            } else {
                $table = '<p style="margin-left: 21pt;">
 <table border="1" cellpadding="2" cellspacing="1" style="margin-left: 21pt;">
 <thead>
 <tr bgcolor="#111" color="#fff" >

     <th>' . lang('Invoice.') . '</th>
     <th>' . lang('Date') . '</th>
     <th>' . lang('Duedate') . '</th>
     <th>' . lang('Amount') . '</th>
 </tr>
 </thead>
     <tbody>';
            }
            // ---------------------------------------------------------
            foreach ($invoices as $invoice) {
                $table .= ' <tr>

        <td> ' . $invoice->iInvoiceNbr . ' </td>
        <td> ' . date("d-m-Y", strtotime(str_replace(' 00:00:00.000', '', $invoice->dInvoiceDate))) . ' </td>
        <td> ' . date("d-m-Y", strtotime(str_replace(' 00:00:00.000', '', $invoice->dInvoiceDueDate))) . '</td>
        <td> €' . number_format($invoice->mInvoiceAmount, 2) . '</td>
    </tr>';
            }
            $table .= '
     </tbody>
 </table></p>
';
            if ($html) {
                $body = str_replace('{$table}', $table, $body);
                $pdf->writeHTML($body, true, false, true, false, 'L');
                $pdf->Output($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/' . $id . '.pdf', 'I');
            } else {
                echo "Template not found";
            }
        }
    }
    public function sendmail($cid, $type, $template)
    {
        $this->load->model('Admin_model');
        if ($type == "service") {
            $mobile                = $this->Admin_model->getServiceCli($cid);
            $client                = $this->Admin_model->getClient($mobile->userid);
            $brand                 = $this->Admin_model->getBrandPdfFooter($mobile->gid);
            $this->data['setting'] = globofix($client->companyid);
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent($template, $client->language, $client->companyid);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
            $body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
            $body = str_replace('{$address1}', $client->address1, $body);
            $body = str_replace('{$city}', $client->city, $body);
            $body = str_replace('{$postcode}', $client->postcode, $body);
            $body = str_replace('{$country}', getCountryName($client->country), $body);
            $body = str_replace('{$client_companyname}', $client->companyname, $body);
            $body = str_replace('{$Companyname}', $brand->name, $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            $this->email->bcc('mail@simson.one');
            $this->email->subject(getSubject($template, $client->language, $client->companyid));
            $this->email->message($body);
            if (empty($body)) {
                return false;
            }
            if (empty($subject)) {
                return false;
            }
            if (!isTemplateActive($client->companyid, $template)) {
                log_message('error', 'Template '.$template.' is disabled sending aborted');
                return true;
            }
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject($template, $client->language, $client->companyid),
                    'message' => $body
                ));
                return true;
            // redirect('admin/subscription/detail/'.$cid);
                //$this->session->set_flashdata('success','Email has been sent');
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject($template, $client->language, $client->companyid),
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
                return false;
                //$this->session->set_flashdata('error','Email was not sent');
            }
        }
    }
}
