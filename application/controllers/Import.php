<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Genkgo\Camt\Config;
use Genkgo\Camt\Reader;
use \Mod11\Mod11;
use Codelicious\Coda\Parser;
use Codelicious\Coda\Statements\Statement;
use Codelicious\Coda\Statements\Transaction;
use Codelicious\BelgianBankStatement;

class ImportController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->load->model('Import_model');
        if (php_sapi_name() != "cli") {
            // die('Not allowed');
        }
    }
    public function setProperty()
    {
        $this->data['setting'] = globo();
    }
}
class Import extends ImportController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model');
    }
    public function master_delete_company($companyid)
    {
        $this->db->query("delete from a_admin where companyid=?", array($companyid));
        $this->db->query("delete from a_mvno where companyid=?", array($companyid));
        $this->db->query("delete from a_api_data where companyid=?", array($companyid));
        $this->db->query("delete from a_configuration where companyid=?", array($companyid));
        $this->db->query("delete from a_admin where companyid=?", array($companyid));
        $this->db->query("delete from a_helpdesk_category where companyid=?", array($companyid));
        $this->db->query("delete from a_helpdesk_department where companyid=?", array($companyid));
        $this->db->query("delete from a_role where companyid=?", array($companyid));
        $this->db->query("delete from a_products_group where companyid=?", array($companyid));
        $this->db->query("delete from a_products where companyid=?", array($companyid));
        $this->db->query("delete from a_email_templates where companyid=?", array($companyid));
        $this->db->query("delete from a_products_pdf where companyid=?", array($companyid));
    }
    public function test_lang($lang)
    {
        $this->config->set_item('language', 'dutch');

        $this->lang->load('admin');
        echo lang('Invoice');
    }

    public function generate_accountteum($companyid)
    {
        $services = $this->db->query("select * from a_services where companyid = ? and id > '1817128' order by id", array($companyid));
        $this->load->model('Admin_model');
        $this->load->library('pareteum', array('companyid' => $companyid));
        foreach ($services->result_array() as $row) {
            $rr['serviceid'] = $row['id'];
            $service_teum = $this->Admin_model->getServiceCli($row['id']);
            $client = $this->Admin_model->getClient($row['userid']);
            if (empty($service_teum->details->teum_accountid)) {
                $account = array(
                "AccountInfo" => array(
                    "AccountType" => "Prepaid",
                    "CustomerId" => $client->teum_CustomerId,
                    "ExternalAccountId" => (string)$rr['serviceid'].''.rand(10000000, 99999999),
                    "AccountStatus" => "Active",
                    "Names" => array(
                        array(
                            "LanguageCode" => "eng",
                            "Text" => "Account".$row['userid']
                        )
                    ),
                    "Descriptions" => array(
                        array(
                            "LanguageCode" => "eng",
                            "Text" => "Account".$row['userid']
                        )
                    ),
                    "AccountCurrency" => "GBP",
                    "Balance" => 0,
                    "CreditLimit" => 1
                )
                );
                $acct    = $this->pareteum->CreateAccount($account);
                print_r($account);
                log_message('error', print_r($acct, true));
                if ($acct->resultCode == "0") {
                    $AccountId = $acct->AccountId;
                } else {
                    die('Error when creating Account');
                }
            }

            if (!$AccountId) {
                die('Account id empty');
            }
            $addons = getaddons_teum_base($rr['serviceid'], $service_teum->details->msisdn_sn);
            $subs   = array(
            "CustomerId" => (int) $client->teum_CustomerId,
            "Items" => array(
                array(
                    "AccountId" => (string) $AccountId,
                    "ProductOfferings" => $addons,
                    "ServiceAddress" => array(
                        "Address" => $client->address1,
                        "HouseNo" => $client->housenumber,
                        "City" => $client->city,
                        "ZipCode" => $client->postcode,
                        "State" => "unknown",
                        "CountryId" => "76"
                    )
                )
            ),
            "channel" => "UnitedPortal V1 Import"
            );

            log_message('error', print_r($subs, true));
            $subscription = $this->pareteum->AddSubscription($subs);
            log_message('error', 'AddSubscription'.print_r($subscription, true));
            if ($subscription->resultCode == "0") {
                $this->db->query(
                    "update a_reseller_simcard set SubscriptionId=?, AccountId=?,CustomerOrderId=? where serviceid=?",
                    array($subscription->Subscription->SubscriptionId,
                    $AccountId,
                    $subscription->CustomerOrderId,
                    $rr['serviceid'])
                );
                //mme
                $this->Admin_model->update_services_data('mobile', $rr['serviceid'], array(
                'teum_customerid' => $client->teum_CustomerId,
                'teum_subscriptionid' => $subscription->Subscription->SubscriptionId,
                'teum_accountid' => $AccountId));
            } else {
                print_r($row);
                die('Error accour');
            }
        }
    }
    public function generate_customerid()
    {
        /*
        $q = $this->db->query("select * from a_clients where companyid=? and id >= ? order by id asc", array(111, 949445));
        $this->load->library('pareteum', array('companyid' => 111));
        //Serviceid 1817046 Clientid.949445
        foreach($q->result_array() as $row){

           if(!$row['teum_CustomerId'] ){

            print_r($row['id']);


            $array = array(
            "CustomerData" => array(
                "ExternalCustomerId" => (string)$row['mvno_id'],
                "FirstName" => $row['firstname'],
                "LastName" => $row['lastname'],
                "LastName2" => 'NA',
                "CustomerDocumentType" => $row['id_type'],
                "DocumentNumber" => $row['idcard_number'],
                "Telephone" => $row['phonenumber'],
                "Email" => $row['email'],
                "LanguageName" => "eng",
                "FiscalAddress" => array(
                    "Address" => $row['address1'],
                    "City" =>$row['city'],
                    "CountryId" => "76",
                    "HouseNo" => $row['housenumber'],
                    "State" => "Unknown",
                    "ZipCode" => $row['postcode'],
                ),
                "CustomerAddress" => array(
                    "Address" => $row['address1'],
                    "City" => $row['city'],
                    "CountryId" => "76",
                    "HouseNo" => $row['housenumber'],
                    "State" => "Unknown",
                    "ZipCode" => $row['postcode']
                ),
                "Nationality" => "GB"
            ),
            "comments" => "Customer added via UnitedPortal V1 by Import script"
            );
            log_message('error', json_encode($array));
            $teum = $this->pareteum->AddCustomers($array);
            // echo json_encode($teum);

            log_message('error', json_encode($teum));
            if ($teum->resultCode == "0") {
                $this->db->query("update a_clients set teum_CustomerId=?, teum_OrderCode=? where id=?", array($teum->customerId, $teum->orderCode, $row['id']));
            }else{

                die(print_r($teum));
            }


           }
        }
*/
    }

    public function import_notes()
    {
        $this->load->model('Admin_model');
        $filename = '/home/mvno_dev/public_html/assets/datas/justsims';
        $contents = file($filename);


        foreach ($contents as $line) {
            $arr = explode(';', $line);
            echo $arr[0]." ".$arr[7]."\n";
            $this->db->insert('a_clients_notes', array(
                'companyid' => 111,
                'userid' => $this->getuseridbyMsisdn($arr[0]),
                'admin' => 'Awal Junanto',
                'created' => date('Y-m-d H:i:s'),
                'note' => $arr[7],
                'sticky' => 1
            ));
        }
    }

    public function getseviceidbyMsisdn($msisdn)
    {
        $q = $this->db->query("select * from a_services_mobile where msisdn like ? and companyid=111", array(trim($msisdn)));
        if ($q->num_rows()>0) {
            return $q->row()->serviceid;
        } else {
            die('error '.$msisdn);
        }
    }

    public function update_expire_justsim()
    {
        $filename = '/home/mvno_dev/public_html/assets/datas/expire.txt';
        $contents = file($filename);
        foreach ($contents as $line) {
            $array = explode(';', $line);
            $serviceid = $this->getseviceidbyMsisdn(trim($array[0]));

            $addons = $this->getServiceAddons($serviceid);
            if ($addons) {
                print_r($addons);
            }

            if (!$serviceid) {
                die('error');
            }
        }
    }

    public function gen_uuid()
    {
        $q = $this->db->query("select * from a_admin where uuid is NULL");

        foreach ($q->result() as $r) {
            $this->db->query("update a_admin set uuid=? where id=?", array(gen_uuid(), $r->id));
            echo "Generating :".$r->id."\n";
        }
    }
    public function add_reseller_permission()
    {
        $q = $this->db->query("select * from a_clients_agents");

        foreach ($q->result() as $row) {
            $this->db->insert('a_clients_agents_perms', array('agentid' => $row->id, 'companyid' => $row->companyid));
        }
    }
    public function getServiceAddons($serviceid)
    {
        $q = $this->db->query("select * from a_services_addons where serviceid=?", array($serviceid));

        if ($q->num_rows() == "1") {
            return false;
        } elseif ($q->num_rows()>1) {
            return $q->result();
        } else {
            return false;
        }
    }
    public function import_justsim_new_account($companyid, $agentid)
    {
        //8944125646604431719
        //8944125646604431222
        /*
        $this->load->model('Admin_model');
        $filename = '/home/mvno_dev/public_html/assets/datas/justsims';
        $contents = file($filename);
        $this->load->library('pareteum', array(
            'companyid' => $companyid,
        ));

        foreach ($contents as $line) {
           $array = explode(';', $line);

           if(count($array) == 18){

            if(strtolower($array[0]) != "number"){
                $this->db->insert('a_clients', $this->gen_anonymous($companyid , $array[2], $array[3], $agentid));
                $bundles = explode(',', $array[5]);

                $bundle_start_date = $array[6];
                $userid = $this->db->insert_id();
                if(!$userid){
                    print_r($array);
                    die('error creating user');
                }
                if($array[8] == "-1"){
                    $terms = 600;
                }else{

                    $terms = $array[8];
                }
               $serviceid =  $this->addservice(array(
                'userid' => $userid,
                'companyid'=> $companyid,
                'identity'=> $array[0],
                'type' => 'mobile',
                'status' => 'Active',
                'packageid' => 188,
                'date_created' => $array[4],
                'date_terminate' => NULL,
                'termination_status' => '0',
                'billingcycle' => 'Monthly',
                'date_start' => NULL,
                'date_contract' =>  convert_to_contract($array[4]),
                'date_contract_formated' => $array[4],
                'recurring' => '0.00',
                'notes' => 'Initial Import',
                'promocode' => '',
                'iGeneralPricingIndex' => NULL,
                'contract_terms' => 1,
                'suspend_reason' => NULL,
                'reject_reason' =>  NULL,
                'future_activation' =>  NULL,
                'xml_order' => NULL,
                'autodelete' => '0'));
                if(!$serviceid){
                    print_r($array);
                    die('error creating service');
                }
                    echo "Serviceid ".$serviceid." Clientid.".$userid."\n";
                if($bundles){

                    foreach($bundles as $bundle){
                        $addonid = $this->getBundleidx($bundle);

                        if(!$addonid){

                            die('addon id not found');

                        }
                     $this->db->insert('a_services_addons', array(
                        'serviceid' => $serviceid,
                        'companyid' => $companyid,
                        'name' => $bundle,
                        'terms' => $terms,
                        'cycle' => 'month',
                        'addonid' =>  $addonid,
                        'recurring_total' => getBundlePricebyId($addonid),
                        'addon_type' => 'option',
                        'arta_bundleid' => getBundleId($addonid),
                        'iGeneralPricingIndex' => NULL,
                        'import' => 0,
                        'proformaid' => NULL,
                        'date_order' => $array[6],
                        'teum_autoRenew' => $terms,
                        'teum_DateStart' => $array[6],
                        'teum_DateEnd' => getFuturedate($array[6], 'day', 31),
                        'teum_NextRenewal' => getFuturedate($array[6], 'day', 31),
                        'teum_CustomerOrderId' => NULL,
                        'teum_SubscriptionId' => NULL,
                        'teum_ProductId' => NULL,
                        'teum_ProductChargePurchaseId' => NULL,
                        'teum_SubscriptionProductAssnId' => NULL,
                        'teum_ServiceId' => NULL));
                        unset($addonid);

                    }


                }
                $this->db->insert('a_reseller_simcard', array(
                    'companyid' => $companyid,
                    'serviceid' => $serviceid,
                    'resellerid' => $agentid,
                    'simcard' => $array[10],
                    'MSISDN' => $array[0],
                    'IMSI' => $array[9],
                    'PIN1' => $array[11],
                    'PIN2' => $array[13],
                    'PUK1' => $array[12],
                    'PUK2' => $array[14],
                    'AccountId' => NULL,
                    'CustomerOrderId' => NULL,
                    'SubscriptionId' => NULL,
                    'TeumServiceId' => NULL,
                    'date_created' => NULL,
                    'import_via' => 'manual',
                    'created_by' => 'Import Script'));


if(strtolower($array[1]) == "n/a"){

    $type = "new";
    $donor = "";
}else{

    $type = "porting";
    $donor = $array[1];
}

$this->db->insert('a_services_mobile', array(
'companyid' => $companyid,
'userid' => $userid,
'serviceid' => $serviceid,
'msisdn' => $array[0],
'teum_accountid' => NULL,
'teum_customerid' => NULL,
'teum_subscriptionid' => NULL,
'msisdn_imsi' => $array[9],
'msisdn_sn' => $array[0],
'msisdn_sim' => $array[10],
'msisdn_pin' => $array[11],
'msisdn_puk1' => $array[12],
'msisdn_puk2' => $array[14],
'msisdn_status' => 'Active',
'msisdn_type' => $type,
'ptype' => 'PRE PAID',
'platform' => 'TEUM',
'donor_msisdn' => $donor));
                unset($userid);
                unset($serviceid);
                unset($bundles);
            }


           }

        }
*/
    }

    public function getBundleidx($name)
    {
        if (trim($name) == '500MB') {
            $id = 1187;
        } elseif (trim($name) == '2GB') {
            $id = 1188;
        } elseif (trim($name) == '5GB') {
            $id = 1189;
        } elseif (trim($name) == 'ROW') {
            $id = 1194;
        } elseif (trim($name) == 'TurkeyEU') {
            $id = 1231;
        } elseif (trim($name) == '10GB') {
            $id = 1190;
        } elseif (trim($name) == '15GB') {
            $id = 1191;
        } elseif (trim($name) == '25GB') {
            $id = 1192;
        } elseif (trim($name) == '50GB') {
            $id = 1193;
        }

        return $id;
    }
    public function addservice($data)
    {
        $this->db->insert('a_services', $data);
        return $this->db->insert_id();
    }
    public function gen_anonymous($companyid, $firstname, $lastname, $agentid)
    {
        $this->load->helper('string');
        $rand = rand(100000000, 9999999999);
        $data = array(
                'agentid' => $agentid,
                'companyid' => $companyid,
                'uuid' => gen_uuid(),
                'password' => password_hash($rand, PASSWORD_DEFAULT),
                'mvno_id' => $rand,
                'email' => random_string('alnum', 10).'@'.random_string('alnum', 10).'.com',
                'phonenumber' => rand(1000000000, 9999999999),
                'vat' => random_string('alnum', 10),
                'companyname' => random_string('alnum', 10),
                'initial' => 'AN',
                'salutation' => 'Mr.',
                'firstname' => $firstname,
                'lastname' => $lastname,
                'address1' => random_string('alnum', 10),
                'housenumber' => rand(10, 120),
                'postcode' => rand(1000, 9999),
                'city' => random_string('alnum', 10),
                'country' => 'UK',
                'language' => 'english',
                'paymentmethod' => 'banktransfer',
                'gender' => 'male',
                'sso_id' => '0',
                'nationalnr' => random_string('alnum', 10),
                'date_created' => date('Y-m-d'),
                'invoice_email' => 1,
                'date_birth' => '1970-01-01',
                'vat_exempt' => 0,
                'idcard_number' => rand(10000000000, 99999999999));

        return $data;
    }
    public function growl()
    {
        $r =  send_growl(array('message' => 'Updating system...', 'companyid' => 53));
        print_r($r);
    }
    public function testme($companyid)
    {
        $this->load->library('artilium', array('companyid'=> $companyid));
        $date = date('Y-m-d');
        echo $date."\n";
        $q = $this->db->query("select * from a_subscription_changes where status = '0' and date_commit = ? and companyid=?", array($date, $companyid));
        echo $this->db->last_query();
        foreach ($q->result() as $row) {
            $bundles = $this->artilium->GetBundleAssignList($row->sn);
            print_r($bundles);
        }
    }

    public function AddBundleAssignassign($row, $new_bundles)
    {
        $this->load->model('Admin_model');
        foreach ($new_bundles as $bundleid) {
            $r = $this->artilium->AddBundleAssign($row->sn, $bundleid, $row->date_commit . 'T00:00:00', '2099-12-31T23:59:59');
            if ($r->result == "success") {
                $setting = globofix($row->companyid);
                if ($setting->create_magebo_bundle) {
                    $create = 1;
                    $this->magebo->InsertBundlePincode($mobile->details->msisdn, $this->Admin_model->getBundleQuery($bundleid), $row->date_commit);
                } //$this->data['setting']->create_magebo_bundle
                else {
                    $create = 0;
                }
                unset($setting);
                // $this->magebo->ProcessPricing($mobile, $client->mageboid, $bundleid, $create);
                $IDS[] = $r->id;
            } //$r->result == "success"
            else {
                $IDS[] = $r;
            }
        } //$bundles as $bundleid
    }
    public function isemailsend()
    {
        echo isTemplateActive(54, 'mobile_bundle_recurring');
    }

    public function Teum_getSubscription()
    {
        $this->load->library('pareteum', array(
            'companyid' => 35
        ));
        $res = $this->pareteum->getsubscription(array(
                'CustomerId' => 1065076740,
                'IncludeInactiveSubscriptions' => 1,
                'ActiveFrom' => '2018-07-01',
                'ActiveTo' => '2019-07-26',
                'MSISDN' => '447421966432',
                'IncludeHistoricInformation' => 1,
                'channel' => 'MVNO portal V1'
            ));
        print_r($res);
    }
    public function getfk()
    {
        $this->load->library('artilium', array('companyid' => 53));

        //31628318463

        $s = $this->artilium->GetSUMAssignmentList1('31628318463');
        $r = $this->artilium->getOutofBundleusagePostpaid(array('31628324862'));

        print_r($r);
    }

    public function validate()
    {
        $this->load->model('Api_model');
        echo $this->Api_model->validate();
    }
    public function getPincode($pin)
    {
        $this->load->library('magebo', array(
            'companyid' => 55,
        ));
        $s = $this->magebo->getPincodeRemote($pin);
        print_r($s);
    }
    public function send_appologize()
    {
        //Laatste Herinnering Factuur mobiele telefonie
        $this->load->model('Admin_model');
        $q = $this->db->query("SELECT * FROM `a_email_log` where subject like 'Laatste Herinnering Factuur mobiele telefonie%' and date like '2019-02-18%' order by id asc");

        foreach ($q->result() as $row) {
            echo $row->to . "\n";
            $client = $this->Admin_model->getClient($row->userid);
            print_r($client);
            echo $this->sendcustom('54', 'custom', $client);
        }
    }

    public function getIbanMagebo()
    {
        $this->load->model('Admin_model');
        $q = $this->db->query("select id as userid,mageboid,companyid  from a_clients where companyid=54  and paymentmethod='directdebit'");

        foreach ($q->result() as $row) {
            $mandate = $this->Admin_model->getMandateIdImport($row->mageboid, 54);

            unset($row->mageboid);


            $this->db->insert('a_clients_mandates', array_merge((array)$row, (array) $mandate));

            echo implode(';', array_merge((array)$row, (array) $mandate))."\n";
        }
    }
    public function coda_parser()
    {
        $parser = new Parser();
        $statements = $parser->parseFile('/tmp/dd.cod');
        foreach ($statements as $statement) {
            print_r($statement);
            //echo $statement->getDate()->format('Y-m-d') . "\n";
            foreach ($statement->getTransactions() as $transaction) {
                $m =$transaction->getMessage();
                $t = str_replace(' ', '#', $transaction->getMessage());
                if (strpos($t, '#####################')) {
                    // $m = str_replace('#####################', ';', $t);
                    //$m = str_replace('##', ';', $m);
                    //$m = str_replace('#', PHP_EOL, $m);
                    $m =$t;
                }

                print_r(array('Date' => $transaction->getTransactionDate()->format('Y-m-d'),
                'Name' => $transaction->getAccount()->getName(),
                'IBAN' => $transaction->getAccount()->getNumber(),
                'Structured' => $transaction->getStructuredMessage(),
                'isDirectdebit' => $transaction->getSepaDirectDebit(),
                'Amount' => $transaction->getAmount(),
                'Message' => $m));
            }

            echo $statement->getNewBalance() . "\n";
        }
    }
    public function camt53()
    {
        $reader = new Reader(Config::getDefault());
        $message = $reader->readFile('/home/mvno/documents/camt/done/s9017918.ING.190207.CAMT05300102.DPA01.S.345756.P.NL76INGB0008519520.xml');
        $statements = $message->getRecords();
        foreach ($statements as $statement) {
            $entries = $statement->getEntries();
            print_r($entries);
        }
    }
    public function fix_bar()
    {
        $this->load->model('Admin_model');
        $q = $this->db->query("select a.* from a_services_mobile a left join a_services b on b.id=a.serviceid where a.bar =1 and a.companyid=54 and b.status = 'Suspended' ");
        foreach ($q->result() as $row) {
            $this->Admin_model->update_services($row->serviceid, array('status' => 'Suspended', 'suspend_reason' => 'Payment overdue'));
            //$this->Admin_model->update_services_data('mobile', $row->serviceid, array('status' => 'Suspended'));
        }

        echo $q->num_rows();
    }

    public function getPortins($cid)
    {
        $this->load->library('artilium', array('companyid' => $cid));
        $list = $this->artilium->GetPendingPortIns();
        //$res = $this->artilium->getFucthPortinpending();

        print_r($list);
    }

    public function import_digiweb()
    {
        $this->load->model('Admin_model');
        $filename = '/home/mvno_dev/public_html/assets/datas/digiweb.txt';
        $contents = file($filename);
        $this->load->library('magebo', array(
            'companyid' => 2
        ));

        foreach ($contents as $line) {
            $r = explode(';', $line);
            print_r($r);
            $client = getClientbyMageboid($r[0]);
            if ($client) {
                $userid = $client->id;
            } else {
                $client = $this->magebo->getiAddress($r[0]);
                $client['companyid'] = 2;
                $this->db = $this->load->database('default', true);
                $this->db->insert('a_clients', $client);
                $userid =  $this->db->insert_id();
            }

            echo $userid."\n";

            $this->db->insert("a_services", array(
            'companyid' => 2,
            'identity' => $r[8],
            'type' => 'xdsl',
            'status' => 'Active',
            'packageid' => 111,
            'userid' => $userid,
            'date_created' => date('Y-m-d'),
            'billingcycle' => 'Monthly',
            'date_start' => date('Y-m-d'),
            'date_contract' => date('m-d-Y'),
            'date_contract_formated' => date('Y-m-d'),
            'recurring' => 20,
            'notes' => 'Imported by gert'));
            $serviceid = $this->db->insert_id();
            if ($serviceid) {
                echo "Done ".$serviceid."\n";
                $this->db->insert('a_services_xdsl', array(
                'requestid' => null,
                'serviceid' => $serviceid,
                'companyid' => 2,
                'proximus_orderid' => null,
                'userid' => $userid,
                'circuitid' => $r[8],
                'status' => 'Active',
                'street' => $r[2],
                'number' =>$r[3],
                'alpha' => $r[4],
                'bus' => null,
                'floor' => null,
                'block' =>null,
                'city' => $r[6],
                'postcode' => $r[5],
                'country' => 'BE',
                'phonenumber' => $r[9],
                'dialnumber' => null,
                'proximus_status' => 'Active',
                'realm' => 'UNITED',
                'pppoe_username' => null,
                'pppoe_password' => null,
                'profile' =>null,
                'lex' => $r[11],
                'proformaid' => null,
                'created' => date('Y-m-d H:i:s'),
                'last_updated' =>  date('Y-m-d H:i:s'),
                'product_name' => 'ADSL',
                'product_id' => null,
                'bussiness_pack' => 0,
                'fixed_ip' => 0,
                'address_verified' => 1,
                'introductionPairUsed' => null,
                'feedback_description' => null,
                'address_lang' => $r[7],
                'MDU_SDU' => null,
                'appointment_date_requested' =>null,
                'appointment_date_confirmed' => null,
                'date_activation' => null,
                'remarks' => null,
                'remark_installation' => null,
                'workorderid' => null,
                'type_install' => 'Unknown',
                'type_order' => 'PROVIDE',
                'SNA' => 0));
            } else {
                die('ie');
            }
        }
    }
    public function fix_Discount_delta()
    {
        $this->load->library('magebo', array('companyid' => 53));
        $q = $this->db->query("SELECT * FROM `a_debug_sql` WHERE `funct` LIKE '%addPricingRemote Result: ' and description like '%Discount Next%' and date_created > '2019-10-01' ORDER BY `a_debug_sql`.`id` ASC");
        foreach ($q->result() as $row) {
            $ex  = explode(",", $row->description);
            print_r($ex);
            if (count($ex) == "16") {
                $ex[15] = str_replace("'", "", $ex[15]);

                $iStartMonth = $ex[9];
                $iStartYear =  $ex[10];
                $prorata_disc = $ex[15];
                $vat = $ex[5];
            } elseif (count($ex) == "18") {
                $ex[17] = str_replace("'", "", $ex[17]);
                $iStartMonth = $ex[9];
                $iStartYear =  $ex[10];
                $prorata_disc = $ex[17];
                $vat = $ex[5];
            } else {
                // print_r($ex);
            }
            echo implode(', ', $ex)."\n";
            $promoIndex = $this->magebo->ExecuteQuery(implode(', ', $ex));
            if ($promoIndex) {
                $this->db->query('update a_debug_sql set funct=? where id=?', array('addPricingRemote Result: '.$promoIndex, $row->id));
                echo "exec spInsertPricedItems " . $promoIndex . ", " . $iStartMonth . ", " . $iStartYear . ", 1, " . $prorata_disc . ", " . $vat . ", 1\n";
                $this->magebo->ExecuteQuery2("exec spInsertPricedItems " . $promoIndex . ", " . $iStartMonth . ", " . $iStartYear . ", 1, " . $prorata_disc . ", " . $vat . ", 1");
            }
        }
    }
    public function checkContractDate()
    {
        $this->load->library('magebo', array('companyid' => $companyid));
        $subs = $this->magebo->getSubscriptions();

        foreach ($subs as $row) {
            $p = explode('-', $row->cRemark);
            $contract_date = str_replace(' 00:00:00', '', $row->dContractDate);
            $msisdn = "316" . str_replace(' ', '', end($p));
            $oo = getSubscriptionbyMsisdn($msisdn);
            if (!companredate($oo, $contract_date)) {
                echo convert_to_contract($contract_date) . ";" . $oo->date_contract . ";" . $oo->msisdn . ";" . $oo->mvno_id . ";" . $oo->mageboid . "\n";
            }
            unset($oo);
            unset($p);
            unset($msisdn);
        }
    }
    public function fix_grace()
    {
        $this->load->model('Admin_model');
        $filename = '/home/mvno/public_html/assets/datas/grace_trendcall';
        $contents = file($filename);
        $this->load->library('artilium', array(
            'companyid' => 54,
        ));
        foreach ($contents as $line) {
            $serv = $this->Admin_model->getSN(trim($line));
            if (count($serv) > 1) {
                print_r($serv);
            } elseif (count($serv) == 1) {
                print_r($this->artilium->UpdateCLI($serv[0]->msisdn_sn, 1));
            } else {
                die("not found");
            }
            echo $line;
        }
    }
    public function move_cli($companyid, $sn)
    {
        $this->load->library('artilium', array(
            'companyid' => $companyid,
          ));
        $res = $this->artilium->MoveCLI($sn, 7, 0);

        print_r($res);
    }
    public function upload_ciot()
    {
        $this->load->model('Admin_model');
        $filename = '/home/mvno/public_html/assets/ciot.csv';
        $contents = file($filename);
        foreach ($contents as $line) {
            $data = explode(';', $line);
            $this->db->query("update a_clients_ciot set address1=?, housenumber=?, alphabet=?,ciot=1 where mvno_id=?", array($data[2],$data[3],$data[4],$data[0]));
            // $this->db->insert('a_sms_notifications', array('userid' => getIdMvno_id(trim($data[0]), 54), 'msisdn' => trim($data[5])));
            unset($data);
        }
    }
    public function trendcall_updatecontract_date()
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select id,date_created,date_contract from a_services where companyid=? and notes like ?", array(54, 'Imported from KPN'));


        foreach ($q->result() as $row) {
            print_r($row);

            $this->db->query("update a_services set date_contract =? where id=?", array(convert_to_contract($row->date_created), $row->id));
        }
    }
    public function checkInvoice()
    {
        $this->load->model('Admin_model');
        $filename = '/home/mvno/public_html/assets/datas/ems';
        $contents = file($filename);
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));

        foreach ($contents as $line) {
            if ($this->getReminder(trim($line))) {
                echo "toro" . $line;
            }
        }
    }
    public function getReminder($id)
    {
        $this->db = $this->load->database('default', true);
        echo "select * from a_reminder where invoicenum=" . $id . "  and type='Reminder3'";

        $q = $this->db->query("select * from a_reminder where invoicenum=?  and type=?", array($id, 'Reminder3'));

        if ($q->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function ems_get()
    {
        $body = '<SOAP-ENV:Envelope ...>
        <ns4:IPGApiActionRequest
xmlns:ns4="http://ipg-online.com/ipgapi/schemas/ipgapi"
xmlns:ns2="http://ipg-online.com/ipgapi/schemas/a1"
xmlns:ns3="http://ipg-online.com/ipgapi/schemas/v1">
<ns2:Action>
<ns2:InquiryOrder>
 <ns2:OrderId>
 b5b7fb49-3310-4212-9103-5da8bd026600
 </ns2:OrderId>
 </ns2:InquiryOrder>
</ns2:Action>
</ns4:IPGApiActionRequest>
</SOAP-ENV:Envelope>';
        // initializing cURL with the IPG API URL:
        $ch = curl_init("https://test.ipg-online.com/ipgapi/services");
        // setting the request type to POST:
        curl_setopt($ch, CURLOPT_POST, 1);
        // setting the content type:
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
        // setting the authorization method to BASIC:
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // supplying your credentials:
        curl_setopt($ch, CURLOPT_USERPWD, "WS101._.007:myPW");
        // filling the request body with your SOAP message:
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        $response = curl_exec($ch);
    }

    public function reminder_portin_code($companyid)
    {
        echo "Starting Cron reminder portin code\n";
        //portin_reminder_verification
        $this->load->library('artilium', array('companyid' => $companyid));
        $this->load->model('Admin_model');
        //$this->load->library('artilium', array(COMPANYID, PLATFORM));
        $dd = array();
        $list = $this->artilium->GetPendingPortIns();
        //print_r($list);
        //exit;
        if (!empty($list)) {
            // mail('mail@simson.one', 'status',print_r($list, true)); 2019/03/01
            foreach ($list as $key => $row) {
                if (!in_array(trim($row['Status']), array("CANCELED", "Port in Canceled"))) {
                    if (in_array($row["Status"], array("Port in Wait for verificationcode", "Port in Not yet complete"))) {
                        if (!strpos($row['EnteredDate'], date('Y/m/d'))) {
                            print_r($row);
                            echo $this->sendcustom($companyid, 'portin_reminder_verification', $this->Admin_model->getClientBySN($row['SN'], $companyid));
                            sleep(9);
                        }
                    }
                }
            }
        } else {
            echo "List Empty\n";
        }
    }
    public function import_missing_sepa()
    {
        $this->load->model('Admin_model');
        $filename = '/home/mvno/public_html/assets/datas/missing_data';
        $contents = file($filename);
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        foreach ($contents as $line) {
            $s = explode(';', $line);
            //$client['mageboid'] = getIAddressByMvno_id($s[0]);
            $client['userid'] = getIdMvno_id($s[0], 54);
            $client['iban'] = $s[2];
            $client['type'] = 'NEW';
            $client['mandateid'] = $s[7];
            print_r($client);
            $this->sepa_amend($client);
        }
    }
    public function sepa_amend($dd)
    {
        $this->load->model('Admin_model');
        $this->load->model('Api_model');
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        if (!empty($dd['iban']) && !empty($dd['userid'])) {
            $client = $this->Admin_model->getClient($dd['userid']);
            $bic = $this->Api_model->getBic(trim($dd['iban']));
            if (empty($bic->bankData->bic)) {
                print_r($bic);
            //die('Your IBAN is invalid');
            } else {
                $dd['bic'] = $bic->bankData->bic;
                $this->Admin_model->update_sepa($dd);
                if ($client) {
                    if ($dd['type'] == "NONE") {
                        die('You need to choose AMEND or NEW');
                    } else {
                        if ($dd['type'] == "NEW") {
                            $this->magebo->insertIban($client->mageboid, array(
                                'iban' => strtoupper(str_replace(' ', '', trim($dd['iban']))),
                                'bic' => trim($dd['bic']),
                            ));
                            $this->magebo->addMageboSEPA(strtoupper(str_replace(' ', '', trim($dd['iban']))), trim($dd['bic']), $client->mageboid, $dd['mandateid']);
                        } else {
                            //    $this->magebo->amendMageboSEPA(strtoupper(str_replace(' ', '', trim($dd['iban']))),trim($dd['bic']),$client->mageboid);
                        }
                        //    $this->Admin_model->update_sepa($dd);
                        //logAdmin(array('companyid' => $this->companyid, 'userid' => $client->id, 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Execute Sepa Amendement for  ' . $client->mageboid.' IBAN: '.$_POST['iban']));
                    }
                    //$this->session->set_flashdata('success','Sepa has been amended');
                } else {
                    //$this->session->set_flashdata('error','Billing Id cant be found');
                }
            }
        } else {
            //$this->session->set_flashdata('success','Please check your data');
        }
        //redirect('admin/client/detail/'.$_POST["userid"]);
    }
    /*
    public function fixoldorder()
    {
    $this->load->model('Admin_model');
    $this->db = $this->load->database('default', true);
    $q        = $this->db->query("select a.*,b.date_contract,b.packageid,b.userid,c.firstname,c.lastname,c.address1,c.postcode,c.mageboid,b.status as servicestatus,b.id as hostingid from a_services_mobile a left join a_services b on b.id=a.serviceid left join a_clients c on c.id=b.userid where a.msisdn_status ='ActivationRequested'");
    foreach ($q->result() as $row)
    {
    //
    $sn     = $this->artilium->getSn(trim($row->msisdn_sim));
    //echo trim($sn->data->SN) . "\n";
    $bundle = $this->artilium->GetBundleAssignList(trim($sn->data->SN));
    if ($bundle->GetBundleAssignListV2Result->TotalItems > 0)
    {
    // print_r($bundle->GetBundleAssignListV2Result);
    if ("Active" == $row->servicestatus)
    {
    //$this->db->query("update a_services_mobile set msisdn_status =? where serviceid=?", array('Active', $row->serviceid));
    //print_r($row);
    //$this->Admin_model->ChangeStatusOrderID($row->serviceid, 'Active', 'mobile');
    //echo $bundle->GetBundleAssignListV2Result->TotalItems."\n";
    //$this->db->query("update a_services_mobile set msisdn_status = ?")
    }
    }
    else
    {
    if ($row->mageboid > 0)
    {
    $sn         = $this->artilium->getSn($row->msisdn_sim);
    $contract   = explode('-', $row->date_contract);
    $ValidFrom  = $contract[2] . '-' . $contract[0] . '-' . $contract[1] . 'T00:00:00';
    $ValidUntil = "2099-12-31T23:59:59";
    print_r($row);
    $mobile = $this->Admin_model->getServiceCli($row->hostingid);
    $client = $this->Admin_model->getClient($row->userid);
    $addsim = $this->magebo->AddSIMToMagebo($mobile);
    if ("success" == $addsim->result)
    {
    $this->magebo->addPricingSubscription($mobile);
    // Add Simcardlog into Magebo
    $this->magebo->addSimcardLog(trim($mobile->details->msisdn_sim), '599');
    }
    //assign Bundles when sim is activated
    $bundles = $this->Admin_model->getBundlebyProduct($mobile->packageid);
    if ($bundles)
    {
    $IDS = array();
    // activate tarief per packages
    foreach ($bundles as $bundleid)
    {
    $r = $this->artilium->AddBundleAssign($mobile->details->msisdn_sn, $bundleid, $ValidFrom, $ValidUntil);
    print_r($r);
    if ("success" == $r->result)
    {
    $create = 1;
    $this->magebo->ProcessPricing($mobile, $client->mageboid, $bundleid, $create);
    $IDS[] = $r->id;
    }
    else
    {
    $IDS[] = $r;
    }
    }
    if ($IDS)
    {
    $this->Admin_model->updateBundleID($mobile->id, implode(',', $IDS));
    }
    }
    //Check if contract date is today otherwise disable all services
    if (date('Y-m-d') <= $contract[2] . '-' . $contract[0] . '-' . $contract[1])
    {
    $msisdn = $this->artilium->getSn($mobile->details->msisdn_sim);
    $pack   = $this->artilium->GetListPackageOptionsForSn($msisdn->data->SN);
    print_r($pack);
    $this->artilium->UpdateServices($msisdn->data->SN, $pack->NewDataSet->PackageOptions, '0');
    }
    }
    //print_r($bundle);
    }
    unset($sn);
    unset($bundle);
    }
    }
    public function updateNew()
    {
    $this->db = $this->load->database('default', true);
    $q        = $this->db->query("select * from a_services_mobile where msisdn_status ='OrderWaiting'");
    foreach ($q->result() as $row)
    {
    $this->db->query("update a_services set status='New' where id=?", array(
    $row->serviceid
    ));
    echo $this->db->affected_rows();
    }
    echo $q->num_rows();
    }

    public function importClients($companyid)
    {
    echo "Getting clients....\n";
    $clients = $this->Import_model->getClients();
    if (!empty($clients))
    {
    echo "Found " . count($clients) . "\n";
    echo "Removing Old Clients ...\n";
    //$this->Import_model->DeleteClients($companyid);
    foreach ($clients as $client)
    {
    print_r($client);
    $this->db->insert('a_clients', $client);
    }
    }
    else
    {
    echo "No Client Found\n";
    }
    }
    public function importServices($companyid)
    {
    echo "Getting services....\n";
    $services = $this->Import_model->getServices($companyid);
    echo "Found " . count($services) . "\n";
    if (!empty($services))
    {
    echo "Removing Old Services ...\n";
    //$this->Import_model->DeleteServices1($companyid);
    //$this->Import_model->DeleteServices2($companyid);
    foreach ($services as $service)
    {
    if ("700627" == $service['id'])
    {
    print_r($services);
    }
    $custom           = $service["services"];
    $custom['userid'] = $service['userid'];
    unset($service["services"]);
    $this->db->insert('a_services', $service);
    $this->db->insert('a_services_mobile', $custom);
    }
    }
    }
    public function importProducts()
    {
    $services = $this->Import_model->getProducts();
    foreach ($services as $service)
    {
    print_r($services);
    $this->db->insert('a_products', $service);
    }
    }
    public function importTarif()
    {
    $services = $this->Import_model->getTarif();
    foreach ($services as $service)
    {
    print_r($services);
    $this->db->insert('a_products_mobile_bundles', $service);
    }
    }
    public function importAddon()
    {
    $services = $this->Import_model->getAddon();
    foreach ($services as $service)
    {
    print_r($services);
    $this->db->insert('a_products_mobile_bundles', $service);
    }
    }
     */
    public function import_trendcall_customer_tomagebo($companyid)
    {
        $this->load->model('Admin_model');
        $client = $this->db->query("select * from a_clients where companyid=? and mageboid <= 0 order by id asc", array(
            $companyid,
        ));
        foreach ($client->result() as $row) {
            if (!$row->mageboid) {
                //print_r($row);
                echo "Customer created: " . $this->addMageboClientT($row) . " from customerid :" . $row->id . "\n";
            }
        }
    }
    public function updatecName()
    {
        $this->load->library('magebo', array(
            'companyid' => 53,
        ));
        $q = $this->db->query("select mageboid,initial,firstname,lastname,salutation from a_clients where companyid=53");
        foreach ($q->result() as $client) {
            print_r($client);
            $this->magebo->update_name($client->mageboid, format_name_address_no_salutation($client));
        }
    }
    public function addMageboClientT($data)
    {
        $this->load->model('Admin_model');
        $this->load->model('Api_model');
        $this->load->library('magebo', array(
            'companyid' => '54',
        ));
        if ($data->mageboid > 0) {
            return false;
            exit;
        }
        $client = (array) $data;
        unset($client['stats']);
        $client['mvno_id'] = strtoupper(trim($client['mvno_id']));
        /*
        if ($client['country'] == "NL") {
        $pcode = explode(' ', trim($client["postcode"]));
        if (count($pcode) != 2) {
        // $this->session->set_userdata('registration', $client);
        return false;

        exit;
        } else {

        if (strlen($pcode[0]) != 4) {

        //echo json_encode(array('result' => 'error', 'message' => lang('Postcode must: NNNN XX formaat (102) ')));
        return false;
        exit;
        } elseif (strlen($pcode[1]) != 2) {
        return false;

        exit;
        }

        }
        } elseif ($client['country'] == "BE") {
        $pcode = $client["postcode"];
        } else {
        $pcode = trim($client["postcode"]);
        }
         */
        if (!empty($client['iban'])) {
            $bic = $this->Api_model->getBic(trim($client['iban']));
            if (!$bic->valid) {
            } else {
                $client['bic'] = $bic->bankData->bic;
            }
        }
        $client['date_created'] = date('Y-m-d');
        $client['email'] = strtolower(trim($client['email']));
        $magebo = $this->magebo->AddClient($client);
        if ($magebo->result == "success") {
            $this->db->query("update a_clients set mageboid=? where id=?", array(
                $magebo->iAddressNbr,
                $data->id,
            ));
            return $magebo->iAddressNbr;
        } else {
            return false;
        }
    }
    public function GetCustomerProductList($companyid)
    {
        $this->load->library('artilium', array('companyid' => $companyid));
        $msisdn = $this->artilium->GetCustomerProductList();

        print_r($msisdn);
    }
    public function getDefaultAgentid($cid)
    {
        echo  getDefaultAgentid($cid);
    }

    public function AddCustomerProduct($companyid)
    {
        $this->load->library('artilium', array('companyid' => $companyid));
        $res =  $this->artilium->AddCustomerProduct(array('ContactType2Id' => '16', 'PackageId' => '9', 'SN'=> '32485347495'));

        print_r($res);
    }
    public function GetPackageList($companyid)
    {
        $this->load->library('artilium', array('companyid' => $companyid));
        $msisdn = $this->artilium->GetPackageList();

        print_r($msisdn);
    }


    public function addPackageContact($companyid)
    {
        $this->load->library('artilium', array('companyid' => $companyid));
        $msisdn = $this->artilium->ChangePackageForContactType(array(
            'ContactType' => 17,
            'CheckSelfCare' => true,
            'CheckSalesRep' => true,
            'PackageId' => 10));

        print_r($msisdn);
    }


    public function getSN($sim)
    {
        $this->load->library('artilium', array('companyid' => 53));
        $msisdn = $this->artilium->getSn($sim);
        $s      = $this->artilium->GetSpecificCliInfo('31639807513');
        $options = $this->artilium->GetListPackageOptionsForSn('31639807513');
        $bundle = $this->artilium->GetBundleAssignList1('31639807513');
        $sum = $this->artilium->GetSUMAssignmentList('31639807513');

        //$res = $this->artilium->UpdateCLITest();
        print_r($s);
        //print_r($msisdn);
        print_r($s);
        print_r($bundle);
        print_r($sum);
        print_r($options);
        exit;
        foreach ($s->NewDataSet->BundleAssign as $rr) {
            $r['main'] = $rr;
            $r['usage'] = $this->artilium->GetBundleUsageList1($msisdn->data->SN, $rr->BundleAssignId);
            $r['def'] = $this->artilium->GetBundleDefinition($r['usage']->NewDataSet->BundleUsage->BundleDefinitionId);
            print_r($r);
            if (in_array($r['def'], array(
                "1",
                "3",
                "2",
            ))) {
                print_r($r);
            }
            unset($r);
        }
        exit;
        //$pack = $this->artilium->GetListPackageOptionsForSn($msisdn->data->SN);
        foreach ($pack->NewDataSet->PackageOptions as $row) {
            print_r($row);
            print_r($this->artilium->GetBundleAssignList($msisdn->data->SN));
            //print_r($this->artilium->UpdatePackageOptionsForSN($msisdn->data->SN, $row->PackageDefinitionId, '1'));
        }
    }
    public function GetParametersCLI($sn)
    {
        $this->load->library('artilium', array(
            'companyid' => '53',
        ));
        $res = $this->artilium->GetParametersCLI($sn);
        print_r($res);
    }
    
    public function insertportalidtomagebo()
    {
        $this->load->library('magebo', array(
                'companyid' => '2',
            ));
    
        $q = $this->db->query("select mageboid,id from a_clients where companyid in (2) and mageboid >0 order by id asc");
    
        foreach ($q->result() as $row) {
            print_r($row);
            $this->magebo->insertAddressData($row);
        }
    }


    public function updatecli()
    {
        $this->load->library('artilium', array(
            'companyid' => '53',
        ));
        $this->artilium->UpdateCLI('31628325397', 1);
    }

    public function fix_generalPricing($companyid)
    {
        $this->load->model('Admin_model');
        $this->load->library('magebo', array(
            'companyid' => $companyid,
        ));
        $q = $this->db->query("select  a.* from a_services_mobile a left join a_services b on b.id=a.serviceid where b.iGeneralPricingIndex <= 0 and a.companyid=? and b.status='Active'", array($companyid));
        echo $q->num_rows() . " found\n";
        foreach ($q->result() as $row) {
            $pi = $this->magebo->getGeneralPricingIndexDetail(trim($row->msisdn));
            if ($pi) {
                $this->Admin_model->update_services($row->serviceid, array('iGeneralPricingIndex' => $pi));
                echo $row->serviceid . ' ' . $row->msisdn . " " . $pi . "\n";
            } else {
                echo $row->serviceid . ' ' . $row->msisdn . " Not found\n";
            }
        }
    }
    public function getCountryNameLang()
    {
        echo getCountryNameLang('NL', 'dutch');
    }
    public function getCountryIndex()
    {
        print_r($this->magebo->getCountryIndex('NL'));
    }
    public function getMageboClient($id)
    {
        print_r($this->magebo->getClientId($id));
    }
    public function getCdr($number)
    {
        $this->load->model('Admin_model');
        $order = $this->Admin_model->getServiceCli($number);
        $this->load->library('artilium', array(
            'companyid' => '55',
        ));
        $cdr = $this->artilium->get_cdr($order);
        print_r($cdr);
    }
    public function test()
    {
        $this->load->library('magebo', array(
            'companyid' => '54',
        ));
        $this->load->library('artilium', array(
            'companyid' => '54',
        ));
        //$this->load->model('Admin_model');
        //$mobile = $this->Admin_model->getService($id);
        $res = $this->artilium->GetBundleAssignList('31648925121');
        if (is_array($res->NewDataSet->BundleAssign)) {
            foreach ($res->NewDataSet->BundleAssign as $row) {
                $res = $this->artilium->GetBundleUsageList(31648925121, $row->BundleAssignId);
                if (is_array($res->NewDataSet->BundleUsage)) {
                    foreach ($res->NewDataSet->BundleUsage as $bu) {
                        if (isset($bu->OverallBundleUsageId)) {
                            print_r($this->artilium->GetBundleUsage($bu->OverallBundleUsageId));
                        }
                    }
                    //print_r($this->artilium->GetBundleUsageList(31648925121, $res->NewDataSet->BundleUsage[1]->OverallBundleUsageId));
                } else {
                    print_r($this->artilium->GetBundleUsage($res->NewDataSet->BundleUsage->BundleUsageId));
                }
            }
        } else {
            print_r($res);
        }
    }
    public function getCompanyName($id)
    {
        print_r($this->magebo->getCompanyName());
    }
    public function updateContractdate()
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.domain,b.username from tblcustomfieldsvalues a left join tblhosting b on b.id=a.relid where a.fieldid=42");
        foreach ($q->result() as $row) {
            print_r($row);
        }
    }
    public function updateFromfile()
    {
        $this->load->model('Admin_model');
        $file = fopen("/tmp/list.csv", "r") or exit("Unable to open file!");
        //Output a line of the file until the end is reached
        while (!feof($file)) {
            $array = explode(';', fgets($file));
            if ($array[6] > '2018-01-01') {
                echo "-- " . $array[6] . "\n";
                if (!empty($array[5])) {
                    if ($array[5] < $array[6]) {
                        $cdate = $array[5];
                    } else {
                        $cdate = $array[6];
                    }
                } else {
                    $cdate = $array[6];
                }
                //echo $array[4] . " " . $array[3] . " (" . $array[5] . ") " . $array[6] . " **" . $cdate . "**\n";
                $service = $this->Import_model->getHostingId($array);
                if (!empty($service->id)) {
                    //$this->Admin_model->whmcs_api(array('action' => 'UpdateClientProduct', 'serviceid' => $service->id, ''))
                    //echo $service->id . ';' . $cdate . "\n";
                    //$this->Import_model->delcontract($service->id);
                    echo $this->Import_model->replacecontractid(array(
                        'serviceid' => $service->id,
                        'contractdate' => date("m-d-Y", strtotime($cdate)),
                    )) . "\n";
                }
                //echo $array[4] . " " . $array[3] . " (" . $array[5] . ") " . $array[6] . " **" . $cdate . "**\n";
                unset($cdate);
                unset($array);
                //print_r($service);
                unset($service);
            } elseif ($array[5] > '2018-01-01') {
                $service = $this->Import_model->getHostingId($array);
                if (!empty($service->id)) {
                    //echo $service->id . ';' . $cdate . "\n";
                    //$this->Import_model->delcontract($service->id);
                    echo $this->Import_model->replacecontractid(array(
                        'serviceid' => $service->id,
                        'contractdate' => date("m-d-Y", strtotime($array[5])),
                    )) . "\n";
                }
                //echo $array[4] . " " . $array[3] . " (" . $array[5] . ") " . $array[6] . " **" . $cdate . "**\n";
                unset($array);
                //print_r($service);
                unset($service);
            }
        }
        fclose($file);
    }
    public function fixbundlepricingmagebo($id, $create)
    {
        // $id == serviceid
        // $create == if create pricing ex: delta
        $this->load->model('Admin_model');
        $order = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($order->userid);
        $this->load->library('magebo', array(
            'companyid' => $client->companyid,
        ));
        $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
        if ($bundles) {
            // activate tarief per packages
            foreach ($bundles as $bundleid) {
                // $r = $this->artilium->AddBundleAssign($order->details->msisdn_sn, $bundleid, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
                $this->magebo->ProcessPricing($order, $client->mageboid, $bundleid, $create);
            } //$bundles as $bundleid
        } //$bundles
    }
    public function update_agent_trendcall()
    {
        $filename = FCPATH . 'assets/datas/update_agent';
        $contents = file($filename);
        foreach ($contents as $line) {
            $d = explode(';', $line);
            $this->update_agent(array(
                'mvno_id' => trim($d[1]),
                'agentid' => trim($d[0]),
            ));
            unset($d);
        }
    }
    public function checkName($id)
    {
        $this->load->model('Admin_model');
        $this->load->library('artilium', array(
            'companyname' => 55,
        ));
        $client = $this->Admin_model->getClient($id);
        $c = $this->artilium->nameme($client);
        echo $c;
    }
    public function update_agent($data)
    {
        $this->db->where('mvno_id', $data['mvno_id']);
        $this->db->where('companyid', '54');
        $this->db->update('a_clients', array(
            'agentid' => $data['agentid'],
        ));
    }
    public function update_email_trendcall()
    {
        $this->load->model('Admin_model');
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        $filename = FCPATH . 'assets/datas/update_email_trendcall';
        $contents = file($filename);
        foreach ($contents as $line) {
            $d = explode(';', $line);
            $client = $this->Admin_model->getClientData($d[0]);
            if ($client) {
                if (!empty($d[6])) {
                    $this->Admin_model->deleteMandate($d[0]);
                    $this->Admin_model->addMandate(array(
                        'userid' => $d[0],
                        'companyid' => $client['companyid'],
                        'mandate_status' => 'SEPA FIRST',
                        'mandate_id' => $d[7],
                        'mandate_date' => date('Y-m-d'),
                    ));
                    $client['payment_duedays'] = $d[5];
                    $client['iban'] = $d[6];
                    $client['email'] = $d[4];
                } else {
                    $this->Admin_model->deleteMandate($d[0]);
                    $client['payment_duedays'] = $d[5];
                    $client['email'] = $d[4];
                }
                if ($this->Admin_model->UpdateClient($client['id'], $client)) {
                    echo "Processing " . $client['id'] . "\n";
                    print_r($this->magebo->updateclient($client['mageboid'], $client));
                } else {
                    die('Not working');
                }
            }
            //$this->update_agent(array('mvno_id' => trim($d[1]), 'agentid' => trim($d[0])));
            unset($d);
        }
    }
    public function productChangesBatchSim()
    {
        $bundles = array(
            39,
            32,
            30,
        );
        $filename = FCPATH . 'assets/datas/product_changes_6GB.csv';
        $contents = file($filename);
        foreach ($contents as $line) {
            // echo trim($line) . "\n";
            $sn = $this->artilium->getSn(trim($line));
            //print_r($sn);
            /*

            $lists = $this->artilium->GetBundleAssignList(trim($sn->data->SN));
            if (!in_array(trim($sn->data->SN), array("31628325502", "31628325491", "31628325512", "31628325499"))) {
            foreach ($lists->NewDataSet->BundleAssign as $list) {

            $res = $this->artilium->UpdateBundleAssignV2(array('SN' => trim($sn->data->SN), 'BundleAssignId' => $list->BundleAssignId, 'ValidUntil' => '2018-11-01T00:00:00'));
            print_r($list);
            print_r($res);

            }

            foreach ($bundles as $bun) {
            echo $bun;
            print_r($this->artilium->AddBundleAssign(trim($sn->data->SN), $bun, '2018-11-01T00:00:00'));
            }

            }
             */
            echo $this->Admin_model->ChangeProduct(trim($sn->data->SN)) . "\n";
            echo trim($sn->data->SN) . "\n";
        }
    }
    public function getLogging()
    {
        $sn = $this->artilium->ServiceLogging('GetLoggingBySn', array(
            'SN' => '31648925110',
            'From' => '2018-08-31T00:00:00',
            'Till' => '2018-10-31T00:00:00',
            'MaxResults' => '30',
        ));
        print_r($sn);
    }
    public function fixApiData()
    {
        $q  = $this->db->query("select * from a_products");

        foreach ($q->result() as $row) {
            $this->db->query("update a_products set api_id =? where id=?", array($this->getApiId($row->companyid), $row->id));
            echo $this->db->last_query();
        }
    }

    public function getApiId($id)
    {
        $q = $this->db->query("select * from a_api_data where companyid=?", array($id));

        return $q->row()->id;
    }
    public function GetSpecificCliInfo($sn)
    {
        $this->load->library('artilium', array('companyid' => 53));
        $s = $this->artilium->GetParametersCLI($sn);
        print_r($s);
    }
    public function GetSUMAssignmentList($sn)
    {
        $plan = $this->artilium->GetSUMAssignmentList($sn);
        print_r($plan);
    }
    public function download_invoice($companyid, $id, $cdrdownload = false)
    {
        error_reporting(1);
        $this->load->model('Admin_model');
        $this->data['setting'] = globofix($companyid);
        print_r($this->data['setting']);
        if (!file_exists($this->data['setting']->DOC_PATH . $companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . $companyid . '/invoices/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . $companyid . '/invoices/';
        $invoice = $this->Admin_model->getInvoicePdf($id);
        $file = $invoice_path . $id . '.pdf';
        file_put_contents($file, $invoice->PDF);
        chmod($file, 0755);
        chown($file, 'mvno');
        if (file_exists($file)) {
            if ($cdrdownload) {
                $cdr = $this->downloadcdrpdf($id, $companyid);
                if ($cdr) {
                    shell_exec('pdftk ' . $file . ' ' . $cdr . ' cat output ' . $this->data['setting']->DOC_PATH . $companyid . '/invoices/withcdr/' . $id . '.pdf');
                    return $this->data['setting']->DOC_PATH . $companyid . '/invoices/withcdr/' . $id . '.pdf';
                    chmod($this->data['setting']->DOC_PATH . $companyid . '/invoices/withcdr/' . $id . '.pdf', 0755);
                    chown($this->data['setting']->DOC_PATH . $companyid . '/invoices/withcdr/' . $id . '.pdf', 'mvno');
                } else {
                    return $file;
                }
            } else {
                return $file;
            }
        } else {
            return false;
        }
    }
    public function getDataCdr($id)
    {
        $this->load->library('magebo', array(
            'companyid',
            '54',
        ));
        $data = $this->magebo->getDataCdr($id);
        print_r($data);
    }
    public function downloadcdrpdf($id, $companyid)
    {
        $setting = globofix($companyid);
        //$mageboid = $this->uri->segment(5);
        $this->load->library('magebo', array(
            'companyid',
            $companyid,
        ));
        $cdrs = $this->magebo->getInvoiceCdrFileSplit($id);
        $gprs = $this->magebo->getDataCdr($id);
        // print_r($cdrs);
        //$cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);
        // exit;
        if (!$cdrs) {
            if ($gprs) {
                $gp = "";
            } else {
                return false;
            }
        } else {
            $table = '';
            foreach ($cdrs as $pin => $cdr) {
                //print_r($cdrs);
                // exit;
                $table .= '<h2>CDR Voice & SMS: ' . $pin . '</h2><br />
            <table><thead>
            <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
            <th width="12%">' . lang('Date') . '</th>
            <th width="10%">' . lang('Number') . '</th>
            <th width="15%">' . lang('Destination') . '</th>
            <th width="42%">' . lang('Description') . '</th>
            <th width="10%">' . lang('Duration') . '</th>
            <th width="10%">' . lang('Cost') . ' (incl. VAT)</th>
            </tr>
            </thead>
            <tbody>
            ';
                foreach ($cdr as $array) {
                    $table .= '
               <tr>
                <td width="12%">' . substr($array->dCallDate, 0, -3) . '</td>
                <td width="10%">' . $array->iPincode . '</td>
                <td width="15%">' . $array->cDialedNumber . '</td>
                <td width="42%">' . $array->cInvoiceGroupDescription.' - ' .$array->cCallTranslation. '</td>
                <td width="10%">' . gmdate("H:i:s", $array->iInvoiceDuration) . '</td>
                <td width="10%">€' . str_replace('.', ',', includevat('21', $array->mInvoiceSale)) . '</td>
                </tr>
                ';
                }
                $table .= '</tbody></table>';
            }
        }
        if ($gprs) {
            foreach ($gprs as $pincode => $cdr) {
                $gp .= '<h2>CDR DATA: ' . $pincode . '</h2><br />
				<table><thead>
				<tr height="10" bgcolor="#111" color="#fff" height="20px">
				<th width="15%">' . lang('Date') . '</th>
				<th width="13%">' . lang('Number') . '</th>
				<th width="20%">' . lang('Type') . '</th>
				<th width="35%">' . lang('Zone') . '</th>
				<th width="10%">' . lang('Bytes') . '</th>


				</tr>
				</thead>
				';
                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp .= '
					<tr>
					<td width="15%">' . substr($array->SessionDate, 0, -3) . '</td>
					<td width="13%">' . $array->iPincode . '</td>
					<td width="20%">' . $array->Type . '</td>
					<td width="35%">' . $array->Zone . '</td>
					<td width="10%">' . convertToReadableSize($array->Bytes) . '</td>

					</tr>
					';
                }
                $gp .= '</table><br /><br /><strong>Total Data usage  ' . $pincode . ' : ' . convertToReadableSize(array_sum($bytes)) . '</strong>';
                unset($bytes);
            }
        }
        $this->load->library('spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->setPrintFooter(false);
        $pdf->SetAuthor('Simson Lai');
        $pdf->SetTitle('CDR');
        $pdf->SetSubject('CDR Invoice ' . $id);
        $pdf->setInvoicenumber($id);
        $pdf->SetKeywords('Trendcall, Invoice, Cdr, ' . $id);
        $pdf->AddPage('L', 'A4');

        $pdf->ln(10);
        $pdf->SetFont('droidsans', '', '9');
        // set default header data
        $pdf->setInvoicenumber($id);
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(array(
            PDF_FONT_NAME_MAIN,
            '',
            PDF_FONT_SIZE_MAIN,
        ));
        $pdf->setFooterFont(array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA,
        ));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(true, "10");
        $pdf->SetPrintFooter(false);
        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }
        // ---------------------------------------------------------
        // print TEXT
        //  $pdf->PrintChapter(1, lang('Call Detail Records'), $table, true);
        $pdf->writeHTML($table, true, false, true, false, 'J');
        if ($gprs) {
            $pdf->AddPage('L', 'A4');
            $pdf->ln(10);
            $pdf->SetFont('droidsans', '', '9');
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
            // set header and footer fonts
            $pdf->setHeaderFont(array(
                PDF_FONT_NAME_MAIN,
                '',
                PDF_FONT_SIZE_MAIN,
            ));
            $pdf->setFooterFont(array(
                PDF_FONT_NAME_DATA,
                '',
                PDF_FONT_SIZE_DATA,
            ));
            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "10");
            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            $pdf->SetFont('helvetica', '', '9');
            $pdf->writeHTML($gp, true, false, true, false, 'J');
        }
        // print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', FCPATH.'data/chapter_demo_2.txt', true);
        // ---------------------------------------------------------
        //Close and output PDF document
        $pdf->Output($setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf', 'F');
        return $setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf';
    }
    public function tendcallVoicemail()
    {
        $list = $this->db->query("SELECT b.mageboid,a.msisdn_sim,a.msisdn,a.msisdn_sn,b.language,b.firstname,b.lastname FROM `a_services_mobile` a left join a_clients b on b.id=a.userid where a.companyid=54 and a.msisdn_sn is not null");
        //$this->load->library('magebo', array('companyid' => 54));
        $this->load->library('artilium', array(
            'companyid' => 54,
        ));
        //        $this->artilium->UpdateLanguage($sn, $msisdn_languageid);
        foreach ($list->result() as $l) {
            if ($l->language == "dutch") {
                print_r($this->artilium->UpdateLanguage($l->msisdn_sn, 3));
            //print_r($this->magebo->ChangeSimLanguage($msisdn, 3));
            } else {
                print_r($this->artilium->UpdateLanguage($l->msisdn_sn, 1));
                //print_r($this->magebo->ChangeSimLanguage($msisdn, 1));
            }
            print_r($l);
        }
    }

    public function one($id)
    {
        $this->load->library('artilium', array(
            'companyid' => 33,
        ));
        $res1 = $this->artilium->GetSimListbyContact($id);
        foreach ($res1 as $sim) {
            $sn = $this->artilium->getSN($rr['SIMNr']);
            $this->db->insert('a_services', array(
                        'companyid' => 33,
                        'userid' => $row->id,
                        'type' => 'mobile',
                        'status' => $Status,
                        'packageid' => 63,
                        'notes' => $name,
                        'recurring' => '0.00',
                        'date_terminate' => $date_terminated,
                        'date_created' => $dc[0],
                    ));
            $this->Admin_model->insertServicemobile(array(
                        'companyid' => 33,
                        'msisdn' => $sn->data->MSISDNNr,
                        'serviceid' => $this->db->insert_id(),
                        'userid' => $row->id,
                        'msisdn_status' => $Status1,
                        'msisdn_sn' => trim($sn->data->SN),
                        'msisdn_puk1' => $rr['PUK1'],
                        'msisdn_type' => $ntype,
                        'msisdn_puk2' => $rr['PUK2'],
                        'msisdn_status' => 'Active',
                        'msisdn_sim' => $rr['SIMNr'],
                        'msisdn_contactid' => $rr['ContactType2Id'],
                    ));
        }
    }
    public function GetFilteredListOfNumbers()
    {
        $this->load->model('Admin_model');
        $this->load->library('artilium', array(
            'companyid' => 33,
        ));
        $res = $this->db->query("select * from a_clients where companyid=33 order by id desc");
        foreach ($res->result() as $row) {
            // print_r($row);
            //$res1 = $this->artilium->GetContactType2List($row->ContactType2Id);
            //echo $row->ContactType2Id."\n";
            $res1 = $this->artilium->GetSimListbyContact($row->ContactType2Id);
            //print_r($res1);

            foreach ($res1 as $rr) {
                if (isset($rr['SIMNr'])) {
                    $sn = $this->artilium->getSN($rr['SIMNr']);
                    if ($sn) {
                        $cli = $this->artilium->getCLI($sn->data->SN);
                        print_r($cli);
                        if (!empty($sn->CustomerName)) {
                            $name = $sn->CustomerName;
                        } else {
                            $name ="";
                        }
                        echo "Inserting ".$sn->SN." ".$sn->CLI."\n";

                        if (preg_replace('/\D/', '', $sn->SN) ==  preg_replace('/\D/', '', $sn->CLI)) {
                            $ntype = "new";
                        } else {
                            $ntype = "porting";
                        }


                        if ($sn->Status == "1") {
                            $Status = "Active";
                            $Status1 = "Active";
                            $date_terminated = null;
                        } elseif ($sn->Status == "0") {
                            $Status = "Suspended";
                            $Status1 = "Active";
                            $date_terminated = null;
                        } else {
                            $Status = "Terminated";
                            $Status1 = "Terminated";
                            $do = explode('T', $sn->data->DateStatusChanged);
                            $date_terminated = $do[0];
                        }



                        if (!empty($cli->FirstTimeUsed)) {
                            $dc= explode('T', $cli->FirstTimeUsed);
                        } else {
                            $dc= explode('T', $cli->LastUpdate);
                        }
                        //$dc= explode('T',$cli->FirstTimeUsed);RecordAdded
                        $this->db->insert('a_services', array(
                        'companyid' => 33,
                        'userid' => $row->id,
                        'type' => 'mobile',
                        'status' => $Status,
                        'packageid' => 63,
                        'notes' => $name,
                        'recurring' => '0.00',
                        'date_terminate' => $date_terminated,
                        'date_created' => $dc[0],
                        ));
                        $this->Admin_model->insertServicemobile(array(
                        'companyid' => 33,
                        'msisdn' => $sn->data->MSISDNNr,
                        'serviceid' => $this->db->insert_id(),
                        'userid' => $row->id,
                        'msisdn_status' => $Status1,
                        'msisdn_sn' => trim($sn->data->SN),
                        'msisdn_puk1' => $rr['PUK1'],
                        'msisdn_type' => $ntype,
                        'msisdn_puk2' => $rr['PUK2'],
                        'msisdn_status' => 'Active',
                        'msisdn_sim' => $rr['SIMNr'],
                        'msisdn_contactid' => $rr['ContactType2Id'],
                        ));
                    }
                }
            }
            //$sn = $this->artilium->ge
            //
            //
        }
    }
    /*
    $res1 = $this->artilium->GetFilteredListOfNumbers($row->ContactType2Id);
    print_r($res1);
    if(isset($res1->NewDataSet->Numbers->SN)){

    $client = getClientByContactId(33,$res1->NewDataSet->ContactType2Id);
    if($client){

    print_r($res1->NewDataSet->Numbers);

    }

    unset($client);

     */

    public function GetMasterProfiles()
    {
        $this->load->library('artilium', array(
            'companyid' => 33,
          ));
        $res = $this->artilium->GetMasterProfiles();
        print_r($res);
    }

    public function ff()
    {
        $q = $this->db->query("SELECT * FROM `a_sepa_items` WHERE `mandateid` LIKE 'SEPA%' ORDER BY `a_sepa_items`.`iAddressNbr` ASC");

        foreach ($q->result() as $row) {
            $this->db->query("update a_sepa_items set iAddressNbr=? where id=?", array(str_replace('SEPA00', '', $row->mandateid), $row->id));
        }
    }
    public function o_getcontact()
    {
        $this->load->library('artilium', array(
            'companyid' => 33,
        ));
        $res = $this->artilium->GetContactType2List("-1");
        print_r($res);
        foreach ($res as $row) {
            if ($row->ContactType2Id != 50) {
                $client = $this->artilium->GetContactType2List($row->ContactType2Id);
                if ($client) {
                    if (is_array($client)) {
                        foreach ($client as $c) {
                            $info = $this->artilium->GetContactType2($c->ContactType2Id);
                            $dc = explode('T', $info->RecordCreated);
                            $this->db->insert('a_clients', array(
                            'ContactType2Id' => $c->ContactType2Id,
                            'companyid' => 33,
                            'firstname' => $info->Name,
                            'email' => $info->Reference.'@onecentral.net',
                            'password' => password_hash($info->Reference, PASSWORD_DEFAULT),
                            'agentid' => getAgenIdbyContactid('33', $c->ParentId),
                            'mvno_id' => $info->Reference,
                            'country' => 'NL',
                            'date_created' => $dc[0]
                            ));

                            print_r($dc);
                            unset($info);
                        }
                    } elseif (!empty($client->ContactType2Id)) {
                        $info = $this->artilium->GetContactType2($client->ContactType2Id);
                        $dc = explode('T', $info->RecordCreated);

                        $this->db->insert('a_clients', array(
                        'ContactType2Id' => $client->NewDataSet->ContactType2->ContactType2Id,
                        'companyid' => 33,
                        'email' => $info->Reference.'@onecentral.net',
                        'firstname' => $info->Name,
                        'password' => password_hash($info->Reference, PASSWORD_DEFAULT),
                        'agentid' => getAgenIdbyContactid('33', $client->NewDataSet->ContactType2->ParentId),
                        'mvno_id' => $info->Reference,
                        'country' => 'NL',
                        'date_created' => $dc[0]
                        ));

                        print_r($dc);
                        unset($info);
                    }
                }
            }
        }
    }
    public function getInvoiceBalance($ia, $in)
    {
        $this->load->library('magebo', array('companyid' => 54));

        echo $this->magebo->getInvoiceBalance($in, $ia);
    }
    public function GetInstanceList($companyid, $sn)
    {
        $this->load->library('artilium', array(
            'companyid' => $companyid,
        ));
        $s = $this->artilium->GetInstanceList($sn);
        print_r($s);
    }

    public function sendInvoiceEmailTest($companyid, $emailaddress, $invoiceid)
    {
        $this->load->model('Admin_model');
        error_reporting(1);
        echo $invoiceid . " Test\n";
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("select * from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where b.iCompanyNbr=? and iInvoiceNbr=?  order by a.iInvoiceNbr ASC and a.iInvoiceNbr > 310002876", array(
            $companyid,

            $invoiceid
        ));
        echo $q->num_rows();
        //sleep(4);
        foreach ($q->result() as $row) {
            $invoice = $this->Admin_model->getInvoicedetail($row->iInvoiceNbr);
            $clientid = getClientidbyMagebo($invoice->iAddressNbr);
            if ($invoice) {
                //print_r($invoice);
                if ($clientid) {
                    unset($client);
                    $client = $this->Admin_model->getClient($clientid);
                    $this->sendInvoiceTest($companyid, $client, $invoice, $emailaddress);
                }
                unset($invoice);
                unset($client);
                sleep(4);
            } else {
                echo "Invoice not found\n";
            }
        }
    }



    public function sendInvoiceEmail($companyid, $date, $frominvoice = false, $month = false)
    {
        try {
            $this->load->model('Admin_model');
            error_reporting(1);
            echo $companyid . " " . $date . "\n";
            $this->db = $this->load->database('magebo', true);
            if ($frominvoice > 0) {
                $q = $this->db->query("select * from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where b.iCompanyNbr=? and a.dInvoiceDate=? and a.iInvoiceNbr > ? and a.iInvoiceType in (40,41) order by a.iInvoiceNbr ASC", array(
                $companyid,
                $date,
                trim($frominvoice)
                ));
            } else {
                $q = $this->db->query("select * from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where b.iCompanyNbr=? and a.dInvoiceDate=? and a.iInvoiceType in (40,41)  order by a.iInvoiceNbr ASC", array(
                $companyid,
                $date,
                ));
            }

            echo $q->num_rows();
            //sleep(4);
            foreach ($q->result() as $row) {
                $invoice = $this->Admin_model->getInvoicedetail($row->iInvoiceNbr);
                $clientid = getClientidbyMagebo($invoice->iAddressNbr);
                if ($invoice) {
                    //print_r($invoice);
                    if ($clientid) {
                        unset($client);
                        $client = $this->Admin_model->getClient($clientid);
                        //checkSend()
                        $this->sendInvoice($companyid, $client, $invoice);
                    }
                    unset($invoice);
                    unset($client);
                    sleep(4);
                } else {
                    echo "Invoice not found\n";
                }
            }
        } catch (Exception $e) {
            mail('it@united-telecom.be', "Exception Sending Invoice ", "Hello\nInvoice Email has been stop with an exception, please contact simson to check the error and to restart the email sending\n\nPlease do not restart sending Invoice before contacting simson\n\nMessage: " . $e->getMessage());
        }
    }
    public function sendTestteam()
    {
        $this->load->model('Teams_model', 'teams');
        echo $this->teams->send_msg('Test', 'foo bar');
    }
    public function send_invoice_cron($companyid)
    {
        $this->load->model('Admin_model');
        foreach (array('310000531', '310000532', '310000533', '310000534', '310000535') as $inv) {
            $invoice = $this->Admin_model->getInvoicedetail($inv);
            $clientid = getClientidbyMagebo($invoice->iAddressNbr);
            if ($invoice) {
                //print_r($invoice);
                if ($clientid) {
                    unset($client);
                    $client = $this->Admin_model->getClient($clientid);
                    $this->sendInvoice($companyid, $client, $invoice);
                }
                unset($invoice);
                unset($client);
                sleep(4);
            } else {
                echo "Invoice not found\n";
            }
        }
    }

    public function ticket_statistic_report()
    {
        $this->db = $this->load->database('import', true);
        $q = $this->db->query("select * from tbltickets where status =?", array('Open'));

        print_r($q->num_rows());

        foreach ($q->result() as $row) {
        }
    }
    public function sendInvoice($companyid, $client, $invoice)
    {
        error_reporting(1);
        $setting = globofix($companyid);
        //print_r($this->data['setting']);
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = true;
        $this->email->clear(true);
        $this->email->initialize($config);
        if ($client->companyid == 54) {
            if ($invoice->iInvoiceStatus == "54") {
                $body = getMailContent('invoice_email_notification_paid', $client->language, $client->companyid);
            } else {
                $body = getMailContent('invoice_email_notification', $client->language, $client->companyid);
            }
        } else {
            $body = getMailContent('invoice_email_notification', $client->language, $client->companyid);
        }


        //$body = getMailContent('invoice_email_notification', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$month}', 'august', $body);
        $this->load->library('magebo', array('companyid' => $client->companyid));
        $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
        //$body = str_replace('{$mInvoiceAmount}', number_format($invoice->mInvoiceAmount, 2), $body);
        $payment = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
        $amount = $invoice->mInvoiceAmount - $payment;

        $body = str_replace('{$mInvoiceAmount}', str_replace(',', '', number_format($amount, 2)), $body);
        $body = str_replace('{$mTotalInvoiceAmount}', str_replace(',', '', number_format($invoice->mInvoiceAmount, 2)), $body);
        //$body = str_replace('{$mInvoiceAmount}', str_replace('.', ',', number_format($amount, 2)), $body);
        $body = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$dInvoiceDate}', $invoice->dInvoiceDate, $body);
        $body = str_replace('{$dInvoiceDueDate}', $invoice->dInvoiceDueDate, $body);
        $body = str_replace('{$Companyname}', $setting->companyname, $body);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$cInvoiceReference}', $invoice_ref, $body);
        $this->email->set_newline("\r\n");
        $this->email->from($setting->smtp_sender, $setting->smtp_name);
        $client_email = getInvoiceEmail($client->id);

        //$this->email->to('mail@simson.one');
        $this->email->to($client_email);
        /* Disable for testing

        $cc = getContactInvoiceEmail($client->id);
        if ($cc) {
            $this->email->cc($cc);
        }
        */
        if ($client->companyid == 54) {
            if ($invoice->iInvoiceStatus == "54") {
                $subject = getSubject('invoice_email_notification_paid', $client->language, $client->companyid);
            } else {
                $subject = getSubject('invoice_email_notification', $client->language, $client->companyid);
            }
        } else {
            $subject = getSubject('invoice_email_notification', $client->language, $client->companyid);
        }


        $subject = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $subject);
        $subject = str_replace('{$Companyname}', $setting->companyname, $subject);
        $this->email->subject($subject);
        $this->email->message($body);

        if (in_array($companyid, array("54", "55"))) {
            $cdr = true;
        } else {
            $cdr = true;
        }
        if (!in_array($companyid, array(53, 56))) {
            $file = $this->download_invoice($companyid, $invoice->iInvoiceNbr, $cdr);

            if ($file) {
                $this->email->attach($file);
            }
        }

        /*
        $attachments = getAttachmentEmail('invoice_email_notification', $client->language, $client->companyid);
        if($attachments){
        foreach($attachments as $att){
        $this->email->attach($setting->DOC_PATH . '/' . $client->companyid . '/invoices/'.trim($att));
        }

        }
         */

        if ($this->email->send()) {
            foreach ($client_email as $emm) {
                log_message('error', 'Inserting Log for sending email invoice for '.$emm);
                logEmailOut(array(
                    'userid' => $client->id,
                    'to' => $emm,
                    'subject' => $subject,
                    'message' => $body,
                    'companyid' => $companyid,
                ));
            }


            /*if ($cc) {
                foreach ($cc as $em) {
                    logEmailOut(array(
                    'userid' => $client->id,
                    'to' => $em,
                    'subject' => $subject,
                    'message' => $body,
                    'companyid' => $companyid,
                    ));
                }
            }
            */
            echo json_encode(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'result' => true,
                'email' => $client_email
            ));
            echo "\n";
        } else {
            logEmailOut(array(
                'userid' => $client->id,
                'to' => json_encode($client_email),
                'subject' => $subject,
                'message' => $body,
                'companyid' => $companyid,
                'status' => 'error',
                'error_message' => $this->email->print_debugger(),
            ));
            echo json_encode(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'result' => false,
                'email' => $client_email,
                'error' => $this->email->print_debugger(),
            ));
        }
    }


    public function sendInvoiceTest($companyid, $client, $invoice, $emailaddress)
    {
        error_reporting(1);
        $setting = globofix($companyid);
        print_r($this->data['setting']);
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = true;
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent('invoice_email_notification', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$month}', 'februari', $body);
        $this->load->library('magebo', array('companyid' => $client->companyid));
        $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
        //$body = str_replace('{$mInvoiceAmount}', number_format($invoice->mInvoiceAmount, 2), $body);
        $payment = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
        $amount = $invoice->mInvoiceAmount - $payment;

        $body = str_replace('{$mInvoiceAmount}', str_replace(',', '', number_format($amount, 2)), $body);
        //$body = str_replace('{$mInvoiceAmount}', str_replace('.', ',', number_format($amount, 2)), $body);
        $body = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$dInvoiceDate}', $invoice->dInvoiceDate, $body);
        $body = str_replace('{$dInvoiceDueDate}', $invoice->dInvoiceDueDate, $body);
        $body = str_replace('{$Companyname}', $setting->companyname, $body);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$cInvoiceReference}', $invoice_ref, $body);
        $this->email->set_newline("\r\n");
        $this->email->from($setting->smtp_sender, $setting->smtp_name);
        $this->email->to($emailaddress);
        $this->email->bcc('mail@simson.one');
        $subject = getSubject('invoice_email_notification', $client->language, $companyid);
        $subject = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $subject);
        $subject = str_replace('{$Companyname}', $setting->companyname, $subject);
        $this->email->subject($subject);
        $this->email->message($body);

        if (in_array($companyid, array("54", "55"))) {
            $cdr = true;
        } else {
            $cdr = false;
        }
        if (!in_array($companyid, array(53, 56))) {
            $file = $this->download_invoice($companyid, $invoice->iInvoiceNbr, $cdr);
            //echo "file " . $file;
            if ($file) {
                $this->email->attach($file);
            }
        }

        /*
        $attachments = getAttachmentEmail('invoice_email_notification', $client->language, $client->companyid);
        if($attachments){
        foreach($attachments as $att){
        $this->email->attach($setting->DOC_PATH . '/' . $client->companyid . '/invoices/'.trim($att));
        }

        }
         */
        if ($this->email->send()) {
            logEmailOut(array(
                'userid' => $client->id,
                'to' => $client->email,
                'subject' => $subject,
                'message' => $body,
                'companyid' => $companyid,
            ));
            echo json_encode(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'result' => true,
            ));
        } else {
            echo json_encode(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'result' => false,
                'error' => $this->email->print_debugger(),
            ));
        }
    }

    public function sendcustom($companyid, $template, $client)
    {
        if (!$client) {
            return "client not found\n";
        }
        error_reporting(1);
        $setting = globofix($companyid);

        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = true;
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent($template, $client->language, $client->companyid);
        $body = str_replace('{$name}', format_name($client), $body);
        $this->email->set_newline("\r\n");
        $this->email->from($setting->smtp_sender, $setting->smtp_name);
        $this->email->to($client->email);
        // $this->email->bcc(mobiel@d);
        $subject = getSubject($template, $client->language, $companyid);

        $this->email->subject($subject);
        $this->email->message($body);

        logEmailOut(array(
            'userid' => $client->id,
            'to' => $client->email,
            'subject' => $subject,
            'message' => $body,
            'companyid' => $companyid,
        ));
    }
    public function import_ref($filename)
    {
        $this->db = $this->load->database('default', true);

        $filename = FCPATH . 'assets/datas/' . $filename;
        $contents = file($filename);
        echo $filename."\n";
        foreach ($contents as $r) {
            $d = explode(';', $r);
            if (!empty($d[0])) {
                print_r($d);
                //echo trim($d[1])." ".$d[0]."\n";
                $this->db->query("update a_clients set refferal=?, location=? where companyid=? and id=?", array(trim($d[2]), trim($d[1]), 54, trim($d[0])));
                echo $this->db->affected_rows()."\n";
            }
        }
    }
    public function import_phone($filename)
    {
        $this->db = $this->load->database('default', true);
        // $this->db->query("delete from a_clients where companyid=54");
        // $this->db->query("delete from a_clients_contacts where companyid=54");
        $filename = FCPATH . 'assets/datas/' . $filename;
        echo $filename;
        $contents = file($filename);
        foreach ($contents as $key => $r) {
            print_r($r);
        }
    }

    public function sms_test($cid, $userid, $iInvoiceNbr)
    {
        $this->load->model('Admin_model');
        $this->load->library('magebo', array('companyid' => $cid));
        $inv = $this->magebo->getInvoice($iInvoiceNbr);
        $client = $this->Admin_model->getClient($userid);

        $this->data['setting'] = globofix($cid);
        $this->load->library('sms', array(
                          'username' => $this->data['setting']->sms_username,
                          'password' => $this->data['setting']->sms_password,
                          'companyid' => $cid
                        ));
        $sms_res = $this->sms->send_message_bulksms(array(
                          array(
                            'from' => $this->data['setting']->sms_senderid,
                            'to' => '+' . preg_replace('/\D/', '', $client->phonenumber),
                            'body' => replace_body_sms($this->data['setting']->sms_content_reminder2, array(
                              'iInvoiceNbr' => $inv->iInvoiceNbr,
                              'mInvoiceAmount' => $inv->mInvoiceAmount,
                              'name' => format_name($client)
                            ))
                          )
                        ));

        print_r($sms_res);
    }
    public function importTrendcall($filename)
    {
        $this->db = $this->load->database('default', true);
        // $this->db->query("delete from a_clients where companyid=54");
        // $this->db->query("delete from a_clients_contacts where companyid=54");
        $filename = FCPATH . 'assets/datas/' . $filename;
        $contents = file($filename);
        foreach ($contents as $key => $r) {
            $line = explode(';', $r);
            if ("EN" == $line['16']) {
                $line['16'] = 'english';
            } elseif ("NL" == $line['16']) {
                $line['16'] = 'dutch';
            } elseif ("JP" == $line['16']) {
                $line['16'] = 'japan';
            }
            if (trim($line[40]) == "Other") {
                $line[40] = "banktransfer";
            }
            echo $key . "\n";
            $data = array(
                'mvno_id' => trim($line[0]),
                'vat' => trim($line[21]),
                'kvk' => trim($line[20]),
                'vat_rate' => trim($line[22]),
                'companyid' => 54,
                'companyname' => trim($line[2]),
                'email' => trim($line[33]),
                'phonenumber' => preg_replace('/\D/', '', trim($line[32])),
                'firstname' => '',
                'lastname' => trim($line[9]),
                'address1' => trim($line[28]),
                'language' => $line[16],
                'city' => trim($line[30]),
                'postcode' => trim($line[29]),
                'country' => 'NL',
                'date_birth' => trim($line[26]),
                'initial' => trim($line[6]),
                'salutation' => trim($line[5]),
                'language' => $line[16],
                'invoice_email' => $line[17],
                'paymentmethod' => strtolower($line[40]),
                'refferal' => trim($line[46]),
            );
            print_r($data);
            $check = isMvno_Exist(trim($line[0]), '54');
            echo trim($line[0]);
            print_r($check);
            if ($check > 0) {
                //$this->db->insert('a_clients', $data);
                /*   $this->db->insert('a_clients_contacts', array(
            'companyid'       => 54,
            'userid'          => $check,
            'firstname'       => trim($line[6]),
            'lastname'        => trim($line[9]),
            'companyname'     => trim($line[2]),
            'email'           => trim($line[15]),
            'address1'        => trim($line[28]),
            'city'            => trim($line[30]),
            'state'           => '',
            'postcode'        => trim($line[29]),
            'country'         => 'NL',
            'phonenumber'     => trim($line[14]),
            'subaccount'      => 1,
            'password'        => password_hash(rand(10000000, 900000000), PASSWORD_DEFAULT),
            'permissions'     => '',
            'domainemails'    => 0,
            'generalemails'   => 1,
            'invoiceemails'   => 1,
            'productemails'   => 1,
            'supportemails'   => 1,
            'affiliateemails' => 1,
            'created_at'      => date('Y-m-d H:i:s'),
            'updated_at'      => date('Y-m-d H:i:s')));
             */
            } else {
                /* $this->db->insert('a_clients', $data);
            $this->db->insert('a_clients_contacts', array(
            'companyid'       => 54,
            'userid'          => $this->db->insert_id(),
            'firstname'       => trim($line[6]),
            'lastname'        => trim($line[9]),
            'companyname'     => trim($line[2]),
            'email'           => trim($line[15]),
            'address1'        => trim($line[28]),
            'city'            => trim($line[30]),
            'state'           => '',
            'postcode'        => trim($line[29]),
            'country'         => 'NL',
            'phonenumber'     => trim($line[14]),
            'subaccount'      => 1,
            'password'        => password_hash(rand(10000000, 900000000), PASSWORD_DEFAULT),
            'permissions'     => '',
            'domainemails'    => 0,
            'generalemails'   => 1,
            'invoiceemails'   => 1,
            'productemails'   => 1,
            'supportemails'   => 1,
            'affiliateemails' => 1,
            'created_at'      => date('Y-m-d H:i:s'),
            'updated_at'      => date('Y-m-d H:i:s')));
             */
            }
            //print_r($data);
            unset($data);
            unset($line);
        }
    }
    public function importTrendcallTickets($file)
    {
        $csvFile = FCPATH . 'assets/datas/' . $file;
        $a = explode('|', file_get_contents($csvFile));
        foreach ($a as $r) {
            $s = explode(';', $r);
            print_r($s);
            if (count($s) == 10) {
                $this->db->insert('import_trendcall_notes', array(
                    'customerid' => $s[1],
                    'datenote' => $s[2],
                    'entered' => $s[3],
                    'notes' => $s[4],
                    'status' => $s[5],
                    'priority' => $s[6],
                    'assigned' => $s[7],
                    'category' => $s[8],
                    'closed' => $s[9],
                ));
            }
            unset($s);
            unset($r);
        }
    }
    public function getTs()
    {
        $whmcsUrl = 'https://probile.united-telecom.be/api.php?step=addPricing';
        $postfields['query'] = 'select * from database';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $whmcsUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_returnTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
        $response = curl_exec($ch);
        if (curl_error($ch)) {
            die('Unable to connect: WHMCS ' . curl_errno($ch) . ' - ' . curl_error($ch));
        }
        curl_close($ch);
        $jsondata = json_decode($response);
        print_r($jsondata);
    }
    public function GetBundleList()
    {
        $r = $this->artilium->GetBundleList();
        foreach ($r->NewDataSet->Bundle as $bundle) {
            echo implode(';', (array) $bundle) . "\n";
        }
    }
    public function dump_baring()
    {
        error_reporting(0);
        $this->load->library('artilium', array(
            'companyid' => '54',
        ));
        $q = $this->db->query("select c.mvno_id,a.*,b.msisdn_sim,msisdn_sn,b.msisdn from a_services a left join a_services_mobile b on b.serviceid=a.id left join a_clients c on c.id=a.userid where a.companyid=?", array(
            '54',
        ));
        foreach ($q->result() as $row) {
            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($row->msisdn_sn));
            foreach ($pack as $r) {
                $setting['mvno_id'] = $row->mvno_id;
                $setting['cli'] = $row->msisdn;
                if ($r->Customizable) {
                    //print_r($r);
                    $setting[str_replace(' ', '_', $r->CallModeDescription)] = $r->Available;
                }
            }
            //echo $row->mvno_id.";".$row->msisdn.";".
            print_r(implode(';', $setting));
            $this->db->insert('a_packageoptions', $setting);
            //$this->artilium->UpdateServices(trim($row['msisdn_sn']), $pack, '0');
            unset($setting);
            unset($pack);
        }
    }

    public function unblock_state($serviceid, $sn)
    {
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getPackageState($serviceid);
        $this->load->library('artilium', array('companyid' => 54));
        $pack = (array) json_decode($service);
        print_r($pack);
        $this->artilium->UnBlockOriginating(trim($sn), $pack, '1');
    }
    public function dump_bundle()
    {
        error_reporting(1);
        $this->load->library('artilium', array(
            'companyid' => '53',
        ));
        $q = $this->db->query("select g.name,c.mvno_id,a.*,b.msisdn_sim,msisdn_sn,b.msisdn from a_services a left join a_services_mobile b on b.serviceid=a.id left join a_clients c on c.id=a.userid left join a_products g on g.id=a.packageid where a.companyid=? and a.status IN ('Pending','Active')", array(
            '53',
        ));
        foreach ($q->result() as $row) {
            $bundle = $this->artilium->GetBundleAssignList1($row->msisdn_sn);
            $d = array();
            foreach ($bundle as $n) {
                $d[] = $n->szBundle;
            }
            $b['packagename'] = $row->name;
            $b['mvno_id'] = $row->mvno_id;
            $b['cli'] = $row->msisdn;
            if ($d) {
                echo implode(';', array_merge($b, $d)) . "\n";
            } else {
                echo implode(';', $b) . "\n";
            }
            unset($d);
            unset($b);
            unset($bundle);
        }
    }
    public function enable_data()
    {
        $this->db = $this->load->database('default', true);
        $this->load->library('artilium', array(
            'companyid' => 54,
        ));
        $q = $this->db->query("select c.mvno_id,a.*,b.msisdn_sim,msisdn_sn,b.msisdn from a_services a left join a_services_mobile b on b.serviceid=a.id left join a_clients c on c.id=a.userid where a.companyid=? and a.notes like ?", array(
            '54',
            'Imported from KPN',
        ));
        foreach ($q->result() as $row) {
            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($row->msisdn_sn));
            foreach ($pack as $r) {
                $setting['mvno_id'] = $row->mvno_id;
                $setting['cli'] = $row->msisdn;
                if ($r->Customizable) {
                    print_r($r);
                    $setting[str_replace(' ', '_', $r->CallModeDescription)] = $r->Available;
                }
            }
            //echo $row->mvno_id.";".$row->msisdn.";".
            print_r($setting);
            $this->db->insert('a_packageoptions', $setting);
            //$this->artilium->UpdateServices(trim($row['msisdn_sn']), $pack, '0');
            unset($setting);
            unset($pack);
        }
    }
    public function tex($serviceid)
    {
        //echo "tratata";
        $this->load->model('Admin_model');
        $order = $this->Admin_model->getService($serviceid);
        print_r($order);
        $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
        print_r($bundles);
    }
    public function reminder()
    {
        $d = explode('-', date('m-d-Y'));
        $datequery = "SELECT SD.InvoiceStartDate,
                                    CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)) ContractDate,
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) CtrDate
                                    ,LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) Lday,
                                    DATEDIFF(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )),
                                    STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ) )diff,
                                    DAY(LAST_DAY(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' ))) DaysInMonth,
                                    MONTH(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartMonth,
                                    YEAR(STR_TO_DATE(CONCAT(SUBSTR(SD.InvoiceStartDate,6,2),'/',SUBSTR(SD.InvoiceStartDate,9,2),'/',SUBSTR(SD.InvoiceStartDate,1,4)),'%m/%d/%Y' )) StartYear
                                    FROM
                                    (SELECT '" . $d[2] . '-' . $d[0] . '-' . $d[1] . "' InvoiceStartDate )SD ";
        $this->db = $this->load->database('default', true);
        $resultnew = $this->db->query($datequery);
        $row = $resultnew->row_array();
        $PricingAmount = 20;
        $prorata = round($PricingAmount * (($row['diff'] + 1) / $row['DaysInMonth']), 4);
        print_r($row);
        echo $row['diff'] + 1;
        echo $prorata;
    }
    public function send_sepa_notification()
    {
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = true;
        $this->email->clear(true);
        $this->email->initialize($config);
        $this->data['language'] = "dutch";
        $body = "Hello,<br /> Your sepa file has been uploaded on your FTP server<br /><br />Regards, <br>United Telecom";
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@united-telecom.be', 'United Team');
        //$this->email->to('betaalopdrachten@deltafibernederland.nl');
        //$this->email->cc('BBrouwer@deltafibernederland.nl');
        //$this->email->bcc('it@united-telecom.be');
        $this->email->subject(lang("Sepa File Uploaded"));
        $this->email->message($body);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
    public function send_attachment($file, $dt = false)
    {
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = true;
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = "Hello,<br /> Your Import file has been rejected, please modify and upload it to /logs folder<br /><br />Regards, <br>United Telecom";

        if ($dt) {
            $body .="<br><br><br>".nl2br($dt);
        }
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@united-telecom.be', 'United Team');
        // $this->email->to('angelica.schmeltz@trendcall.com');
        // $this->email->cc('lex.v.d.hondel@trendcall.com');
        $this->email->to('mail@simson.one');
        $this->email->subject(lang("File Import clients"));
        $this->email->attach($file);
        $this->email->message($body);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
    public function addMageboClient($id)
    {
        $this->load->model('Admin_model');
        $this->load->model('Api_model');
        $this->load->library('magebo', array(
            'companyid' => '53',
        ));
        $data = $this->Admin_model->getClient($id);
        if ($data->mageboid > 0) {
            return false;
            exit;
        }
        $client = (array) $data;
        unset($client['stats']);
        $client['mvno_id'] = strtoupper(trim($client['mvno_id']));
        if ("NL" == $client['country']) {
            $pcode = explode(' ', trim($client["postcode"]));
            if (count($pcode) != 2) {
                // $this->session->set_userdata('registration', $client);
                return false;
                exit;
            } else {
                if (strlen($pcode[0]) != 4) {
                    $this->session->set_userdata('registration', $client);
                    //echo json_encode(array('result' => 'error', 'message' => lang('Postcode must: NNNN XX formaat (102) ')));
                    return false;
                    exit;
                } elseif (strlen($pcode[1]) != 2) {
                    return false;
                    exit;
                }
            }
        } elseif ("BE" == $client['country']) {
            $pcode = explode(' ', 'BE ', trim($client["postcode"]));
        } else {
            $pcode = trim($client["postcode"]);
        }
        if (!empty($client['iban'])) {
            $bic = $this->Api_model->getBic(trim($client['iban']));
            if (!$bic->valid) {
            } else {
                $client['bic'] = $bic->bankData->bic;
            }
        }
        $client['date_created'] = date('Y-m-d');
        $client['email'] = strtolower(trim($client['email']));
        $magebo = $this->magebo->AddClient($client);
        if ("success" == $magebo->result) {
            $this->db->query("update a_clients set mageboid=? where id=?", array(
                $magebo->iAddressNbr,
                $id,
            ));
            return $magebo->iAddressNbr;
        } else {
            return false;
        }
    }
    public function importTickets()
    {
        $this->load->model('Admin_model');
        $q = $this->db->query("select *  from import_trendcall_notes");
        foreach ($q->result() as $row) {
            $date = substr(trim($row->datenote), 0, 4);
            $client = getClientbyMvnoid($row->customerid);
            if ($client) {
                $email = $this->Admin_model->getClient($client);
                if ($email) {
                    $e = $email->email;
                } else {
                    $e = '';
                }
            } else {
                $e = '';
            }
            if ($client) {
                print_r($row);
                $name = getTrendcatName($row->entered);
                if (11 == $row->assigned) {
                    $assigne = "41";
                } elseif (13 == $row->assigned) {
                    $assigne = "44";
                } elseif (28 == $row->assigned) {
                    $assigne = "42";
                } elseif (35 == $row->assigned) {
                    $assigne = "48";
                } elseif (37 == $row->assigned) {
                    $assigne = "49";
                } elseif (32 == $row->assigned) {
                    $assigne = "70";
                } elseif (14 == $row->assigned) {
                    $assigne = "53";
                } elseif (18 == $row->assigned) {
                    $assigne = "18";
                } elseif (22 == $row->assigned) {
                    $assigne = "61";
                } elseif (26 == $row->assigned) {
                    $assigne = "65";
                } elseif (20 == $row->assigned) {
                    $assigne = "59";
                } elseif (17 == $row->assigned) {
                    $assigne = "56";
                } elseif (34 == $row->assigned) {
                    $assigne = "72";
                } elseif (10 == $row->assigned) {
                    $assigne = "50";
                } elseif (12 == $row->assigned) {
                    $assigne = "52";
                } elseif (15 == $row->assigned) {
                    $assigne = "54";
                } elseif (30 == $row->assigned) {
                    $assigne = "68";
                } elseif (24 == $row->assigned) {
                    $assigne = "63";
                } elseif (16 == $row->assigned) {
                    $assigne = "55";
                } elseif (23 == $row->assigned) {
                    $assigne = "62";
                } elseif (31 == $row->assigned) {
                    $assigne = "69";
                } elseif (100 == $row->assigned) {
                    $assigne = "49";
                } elseif (29 == $row->assigned) {
                    $assigne = "67";
                } elseif (21 == $row->assigned) {
                    $assigne = "60";
                } elseif (25 == $row->assigned) {
                    $assigne = "64";
                } elseif (33 == $row->assigned) {
                    $assigne = "71";
                } elseif (19 == $row->assigned) {
                    $assigne = "58";
                } elseif (36 == $row->assigned) {
                    $assigne = "73";
                } else {
                    $assigne = "41";
                }
                $this->db->insert('a_helpdesk_tickets', array(
                    'companyid' => '54',
                    'tid' => random_str('alphacaps', 3) . '-' . random_str('num', 6) . '-' . $date,
                    'userid' => $client,
                    'email' => $e,
                    'name' => $name,
                    'subject' => substr($row->notes, 0, 100),
                    'message' => $row->notes,
                    'date' => $row->datenote,
                    'deptid' => 4,
                    'status' => ucfirst(trim($row->status)),
                    'admin' => $name,
                    'assigne' => $assigne,
                    'priority' => $row->priority,
                    'categoryid' => getDepartmentid($row->category),
                    'adminonly' => 1,
                ));
                $id = $this->db->insert_id();
                echo $id . "\n";
                $this->db->insert('a_helpdesk_ticket_replies', array(
                    'name' => $name,
                    'ticketid' => $id,
                    'message' => $row->notes,
                    'date' => $row->datenote,
                    'attachments' => '',
                    'isadmin' => 1,
                ));
            }
            unset($name);
            unset($assigne);
            unset($date);
            unset($id);
        }
    }
    public function getLIST($SN)
    {
        $final = array();
        $this->load->library('artilium', array(
            'companyid' => '53',
        ));
        $list = $this->artilium->GetBundleAssignList($SN);
        //$array = simplexml_load_string($list->GetBundleAssignListV2Result->ListInfo->any);
        print_r($list);
        exit;
        foreach ($array->NewDataSet->BundleAssign as $r) {
            $res = $this->artilium->GetBundleUsageList($SN, $r->BundleAssignId);
            $m = (array) $res->NewDataSet->BundleUsage;
            $m['szBundle'] = getMobilebundleName($r->BundleId, 53);
            $m['Defenition'] = $this->artilium->GetBundleDefinition($m['BundleDefinitionId']);
            if (!empty($m['ValidFrom'])) {
                $m['ValidFrom'] = substr($m['ValidFrom'], 0, 10);
            }
            if (!empty($m['ValidUntil'])) {
                $m['ValidUntil'] = substr($m['ValidUntil'], 0, 10);
            }
            if (1 == $m['Defenition']) {
                if (120000 == $m['AssignedValue']) {
                    $m['AssignedValue'] = 'onbeperkt';
                    $m['Percentage'] = '0';
                } else {
                    $s = $m['UsedValue'] / $m['AssignedValue'];
                    $m['Percentage'] = $s * 100;
                    $m['AssignedValue'] = second2hms(round($m['AssignedValue'], 2));
                }
                $m['UsedValue'] = second2hms(round($m['UsedValue'], 2));
            } else {
                $s = $m['UsedValue'] / $m['AssignedValue'];
                $m['Percentage'] = $s * 100;
                $m['AssignedValue'] = round($m['AssignedValue'], 2);
                $m['UsedValue'] = round($m['UsedValue'], 2);
            }
            $m['icon'] = ratingtype($m['Defenition']);
            $final[] = $m;
            unset($m);
            //echo implode(';', $m)."\n";
        }
        print_r($final);
    }
    public function lt()
    {
        foreach (range(1, 600) as $p) {
            $i[] = rand(1, 50);
            foreach (range(1, 4) as $row) {
                if (!in_array($row, $i)) {
                    $i[] = rand(1, 50);
                } else {
                    $i[] = rand(1, 50);
                }
            }
            $star[] = array();
            foreach (range(1, 2) as $rr) {
                if (!in_array($rr, $star)) {
                    $star[] = rand(1, 12);
                } else {
                    $star[] = rand(1, 12);
                }
            }
            echo implode(" ", $i) . " -- " . implode(' ', array_filter($star)) . "\n";
            unset($i);
            unset($star);
            // echo $1." ".$2." ".$3." ".$4." ".$5." -- ".$star1." ".$star2."\n";
        }
    }
    public function update_salutation()
    {
        $filename = FCPATH . 'assets/datas/delta_sal';
        $contents = file($filename);
        foreach ($contents as $line) {
            $gg = explode(';', $line);
            if ($gg[0] > 0) {
                print_r($gg);
                echo $this->update_sal($gg[0], array(
                    'salutation' => $gg[1],
                    'gender' => $gg[2],
                )) . "\n";
                unset($gg);
            }
        }
    }
    public function update_Iban()
    {
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        $filename = FCPATH . 'assets/datas/trendcall_date';
        $contents = file($filename);
        foreach ($contents as $line) {
            $gg = explode(';', $line);
            if ($gg[0] > 0) {
                if ($gg[1]) {
                    if (getIAddressByMvno_id($gg[2])) {
                        $this->magebo->update_Mandate_Date(getIAddressByMvno_id($gg[2]), $gg[1]);
                        echo $gg[0] . " " . $gg[1] . " " . getIAddressByMvno_id($gg[2]) . "\n";
                        //$this->magebo->insertIban(getIAddressByMvno_id($gg[0]), array('iban' => strtoupper(str_replace(' ', '', trim($gg[3]))), 'bic' => trim($gg[4])));
                        //$this->magebo->addMageboSEPA(strtoupper(trim(str_replace(' ', '', $gg[3]))), $gg[4], getIAddressByMvno_id($gg[0]), $gg[8]);
                    }
                }
                unset($gg);
            }
        }
    }
    public function import_subscription($companyid, $filename)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        //1802408
        $this->load->model('Admin_model');
        $filename = FCPATH . 'assets/datas/' . $filename;
        $contents = file($filename);
        foreach ($contents as $line) {
            $bundle = array();
            $gg = explode(';', $line);
            if (trim($gg[7]) == "Domestic Very Small (100 min/SMS)") {
                $bundle[] = 45;
            } elseif (trim($gg[7]) == "Domestic Small (200 min/SMS)") {
                $bundle[] = 46;
            } elseif (trim($gg[7]) == "Domestic Medium (300 min/SMS)") {
                $bundle[] = 47;
            } elseif (trim($gg[7]) == "Domestic Large (750 min/SMS)") {
                $bundle[] = 49;
            } elseif (trim($gg[7]) == "Domestic Unlimited (1500 min/SMS)") {
                $bundle[] = 50;
            }
            if (trim($gg[8]) == "Domestic Unlimited (1500 min/SMS)") {
                $bundle[] = 50;
            } elseif (trim($gg[8]) == "International Small (100 min/SMS)") {
                $bundle[] = 51;
            } elseif (trim($gg[8]) == "International Large (750 min/SMS)") {
                $bundle[] = 53;
            } elseif (trim($gg[8]) == "International Medium (300 min/SMS)") {
                $bundle[] = 52;
            }
            if (trim($gg[9]) == "Data NL+EU 100 MB") {
                $bundle[] = 55;
            } elseif (trim($gg[9]) == "Data NL+EU 400 MB") {
                $bundle[] = 56;
            } elseif (trim($gg[9]) == "Data NL+EU 1 GB") {
                $bundle[] = 57;
            } elseif (trim($gg[9]) == "Data NL+EU 1,2 GB") {
                $bundle[] = 58;
            } elseif (trim($gg[9]) == "Data NL+EU 2 GB") {
                $bundle[] = 59;
            } elseif (trim($gg[9]) == "Data NL+EU 3 GB") {
                $bundle[] = 61;
            }
            if (is_numeric($gg[0])) {
                if (count($gg) == 27) {
                    $gg[3] = str_replace(',', '.', preg_replace("/[^0-9,.]/", "", $gg[3]));
                    $d = explode('-', $gg[11]);
                    if (count($d) != 3) {
                        $gg[11] = date('Y-m-d');
                        $gg[40] = date('Y-12-01');
                    } else {
                        $gg[40] = date('Y-12-01');
                        $gg[11] = $d[2] . '-' . sprintf("%02d", $d[1]) . '-' . sprintf("%02d", $d[0]);
                    }
                    echo $gg[0] . " " . $gg[40] . "\n";
                    if (isMvno_Exist($gg[0], $companyid)) {
                        $serviceid = $this->Import_model->insert_service(array(
                            'companyid' => $companyid,
                            'type' => 'mobile',
                            'status' => 'Pending',
                            'packageid' => $gg[4],
                            'userid' => isMvno_Exist($gg[0], $companyid),
                            'date_created' => $gg[11],
                            'date_terminate' => null,
                            'termination_status' => 0,
                            'billingcycle' => 'Monthly',
                            'date_start' => $gg[11],
                            'date_contract' => $gg[40],
                            'recurring' => $gg[3],
                            'username' => $gg[1],
                            'password' => 'import',
                            'notes' => 'Imported from KPN',
                            'promocode' => null,
                            'iGeneralPricingIndex' => null,
                            'contract_terms' => $gg[13],
                            'suspend_reason' => null,
                            'reject_reason' => null,
                        ));
                        if ($serviceid) {
                            if ($bundle) {
                                foreach ($bundle as $b) {
                                    if ($b) {
                                        $this->Admin_model->InsertAddon(array(
                                            'name' => getOptionName($b),
                                            'terms' => 1,
                                            'cycle' => 'month',
                                            'addonid' => $b,
                                            'serviceid' => $serviceid,
                                            'recurring_total' => 0,
                                            'addon_type' => 'option',
                                            'arta_bundleid' => getBundleId($b),
                                            'iGeneralPricingIndex' => null,
                                            'import' => 1,
                                        ));
                                    }
                                }
                            }
                        }
                        if (trim(strtolower($gg[20])) == "on") {
                            $bar = 1;
                        } else {
                            $bar = 0;
                        }
                        if (trim(strtolower($gg[21])) == "on") {
                            $internet = 1;
                        } else {
                            $internet = 0;
                        }
                        if (trim(strtolower($gg[22])) == "on") {
                            $premium = 0;
                        } else {
                            $premium = 1;
                        }
                        if (trim(strtolower($gg[24])) == "on") {
                            $roaming = 0;
                        } else {
                            $roaming = 1;
                        }
                        if (trim(strtolower($gg[26])) == "on") {
                            $international = 0;
                        } else {
                            $international = 1;
                        }
                        $this->Import_model->insert_mobile(array(
                            'serviceid' => $serviceid,
                            'msisdn' => $gg[1],
                            'userid' => isMvno_Exist($gg[0], $companyid),
                            'companyid' => $companyid,
                            'msisdn_sn' => null,
                            'msisdn_sim' => $gg[17],
                            'msisdn_pin' => 1111,
                            'msisdn_puk1' => $gg[18],
                            'msisdn_puk2' => null,
                            'msisdn_status' => 'OrderWaiting',
                            'msisdn_type' => 'porting',
                            'msisdn_languageid' => 1,
                            'msisdn_contactid' => $this->Admin_model->getContactIdArta($gg[4]),
                            'msisdn_swap' => 0,
                            'donor_type' => 0,
                            'donor_sim' => 1111111111111111111,
                            'donor_msisdn' => $gg[1],
                            'donor_provider' => 'GSM1-YEST',
                            'donor_customertype' => 0,
                            'donor_accountnumber' => $gg[0],
                            'date_ported' => null,
                            'date_accepted' => null,
                            'date_wish' => '2018-12-03',
                            'ptype' => 'GSM1-YEST',
                            'bar' => $bar,
                            'GPRS' => $internet,
                            'MMS' => 1,
                            'LTE' => $internet,
                            'Roaming' => $roaming,
                            'BarInternationalCalls' => $international,
                            'BarPremium' => $premium,
                            'BarPremiumVoice' => $premium,
                            'bundleids' => null,
                        ));
                        echo "service: " . $serviceid . " inserted\n";
                        unset($gg);
                        unset($serviceid);
                        unset($d);
                    } else {
                        echo $gg[0] . " customer does not exist\n";
                        exit;
                    }
                } else {
                    die('error');
                }
                echo "going to process " . count($contents) . "\n";
            }
        }
    }
    public function update_sal($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('a_clients', $data);
        return $this->db->affected_rows();
    }
    public function update_Bank($id, $data)
    {
        $this->db->where('mvno_id', $id);
        $this->db->where('companyid', 54);
        $this->db->update('a_clients', $data);
        return $this->db->affected_rows();
    }
    public function update_mandates($companyid)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid,
        ));
        $q = $this->db->query("select id,paymentmethod,mageboid from a_clients where paymentmethod=? and companyid=?", array(
            'directdebit',
            $companyid,
        ));
        foreach ($q->result() as $row) {
            if ($row->mageboid) {
                $fin = $this->magebo->getMandateId($row->mageboid);
                $this->db->insert('a_clients_mandates', array(
                    'userid' => $row->id,
                    'companyid' => $companyid,
                    'mandate_id' => $fin->SEPA_MANDATE,
                    'mandate_date' => $fin->SEPA_MANDATE_SIGNATURE_DATE,
                    'mandate_status' => $fin->SEPA_STATUS,
                ));
            }
        }
    }
    public function update_name($companyid)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid,
        ));
        $q = $this->db->query("select * from a_clients where companyid=?order by id asc", array(
            $companyid
        ));
        foreach ($q->result() as $row) {
            echo $this->magebo->update_name($row->mageboid, $row->firstname . ' ' . $row->lastname);
        }
    }
    public function import_united_clients($companyid)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid,
        ));
        $limit = 4000;
        $a = $this->db->query("SELECT * FROM `a_clients` where companyid = 2 and length(mageboid) = 6 and mageboid > 150000 and mageboid <= 160000 ORDER BY `mageboid` DESC limit 1", array(
            $companyid,
        ));
        if ($a->num_rows() > 0) {
            echo $a->row()->mageboid;
            $address = $this->Import_model->getMageboClients($companyid, $limit, $a->row()->mageboid);
        } else {
            $address = $this->Import_model->getMageboClients($companyid, $limit, 0);
        }
        print_r($address);
        foreach ($address as $add) {
            $this->db->insert('a_clients', $this->magebo->getiAddress($add->iAddressNbr));
        }
    }
    public function import_product_united()
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query('SELECT  a.* FROM GDC_ERP.dbo.tblGeneralPricing a
			left join GDC_ERP.dbo.tblAddress b on b.iAddressNbr=a.iAddressNbr
			where b.iCompanyNbr=2
			and bEnabled=1
			and iStartYear is NOT NULL
			AND iTotalMonths > 1
			ORDER BY a.iGeneralPricingIndex asc');
        foreach ($q->result() as $p) {
            //echo "Check ".$p->iGeneralPricingIndex." ".$p->iStartMonth." ".$p->iStartYear." ".$p->iTotalMonths."\n";
            if (isActiveSubscription($p)) {
                echo implode('|', (array) $p) . "\n";
            } else {
                //echo "Expired ".$p->iGeneralPricingIndex." ".$p->iStartMonth." ".$p->iStartYear." ".$p->iTotalMonths."\n";
            }
        }
    }
    public function process_porting($companyid)
    {
        echo "Staring...\n";
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
        //SELECT *  FROM `a_services_mobile` WHERE `companyid` = 54 AND `msisdn_status` = 'OrderWaiting' AND `date_wish` = '2018-12-03'
        $q = $this->db->query("select a.*,b.date_contract FROM a_services_mobile a left join a_services b on b.id=a.serviceid WHERE a.companyid = ? AND a.donor_provider LIKE 'GSM1-YEST' and a.msisdn_status like 'OrderWaiting' and b.packageid in(65,66,67,68,69,70) order by a.id asc", array(
            $companyid,
        ));
        //print_r($q->result());
        foreach ($q->result_array() as $row) {
            $client = $this->Admin_model->getClient($row['userid']);
            //this->Import_model->updateDonor($row['id'], $client->mvno_id);
            if (!empty($client->mvno_id)) {
                $this->process_porting_platform_flex($row);
                echo $row['serviceid'] . " " . $row['userid'] . " " . $client->mvno_id . "\n";
            } else {
                echo "Failed" . $row['userid'] . " " . $client->mvno_id . "\n";
                exit;
                //$this->Import_model->updateDonor($row->id, $client->mvno_id);
            }
        }
    }
    public function getSumlist($companyid)
    {
        $this->db = $this->load->database('default', true);
        //$this->load->library('artilium', array('companyid' => $companyid));
        $q = $this->db->query("select  companyid,serviceid,msisdn_sn from a_services_mobile where companyid=? order by serviceid desc", array(
            $companyid,
        ));
        foreach ($q->result() as $row) {
            if ($row->msisdn_sn) {
                echo $row->msisdn_sn . " " . $row->serviceid . "\n";
                $sum = $this->artilium->GetSUMAssignmentList($row->msisdn_sn);
                if (count($sum) < 2) {
                    $res = $this->artilium->createSUMassignment($row->msisdn_sn);
                    print_r($res);
                    unset($res);
                }
            }
            unset($sum);
        }
    }
    public function testf($filename)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        //1802408
        $this->load->model('Admin_model');
        $filename = FCPATH . 'assets/datas/' . $filename;
        $contents = file($filename);
        foreach ($contents as $line) {
            $gg = explode(';', $line);
            if (trim(strtolower($gg[20])) == "on") {
                $bar = 1;
            } else {
                $bar = 0;
            }
            if (trim(strtolower($gg[21])) == "on") {
                $internet = 1;
            } else {
                $internet = 0;
            }
            if (trim(strtolower($gg[22])) == "on") {
                $premium = 0;
            } else {
                $premium = 1;
            }
            if (trim(strtolower($gg[24])) == "on") {
                $roaming = 1;
            } else {
                $roaming = 0;
            }
            if (trim(strtolower($gg[26])) == "on") {
                $international = 0;
            } else {
                $international = 1;
            }
            $r = array(
                'bar' => $bar,
                'GPRS' => $internet,
                'MMS' => 1,
                'LTE' => $internet,
                'Roaming' => $roaming,
                'BarInternationalCalls' => $international,
                'BarPremium' => $premium,
                'BarPremiumVoice' => $premium,
            );
            //print_r($r);
            echo $gg[1] . " " . $this->up($gg[1], $r);
            unset($gg);
        }
        /*
    $pack   = $this->artilium->GetListPackageOptionsForSnAdvance(trim($sn));

    foreach($pack as $p){
    echo $p->CallModeDescription."\n";
    }
     */
    }
    public function up($msisdn, $data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->like('msisdn', trim($msisdn));
        $this->db->update('a_services_mobile', $data);
        echo $this->db->affected_rows() . "\n";
    }
    public function block_services($companyid)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 0);
        $this->db = $this->load->database('default', true);
        //SELECT *  FROM `a_services_mobile` WHERE `companyid` = 54 AND `msisdn_status` = 'OrderWaiting' AND `date_wish` = '2018-12-03'
        $q = $this->db->query("select a.*,b.date_contract,c.name as productname FROM a_services_mobile a left join a_services b on b.id=a.serviceid left join a_products c on c.id=b.packageid left join a_products_group d on d.id=c.gid WHERE a.companyid = ? AND a.donor_provider LIKE 'GSM1-YEST' AND a.msisdn_status ='PortinPending'  order by a.id asc", array(
            $companyid,
        ));
        foreach ($q->result_array() as $row) {
            echo $row['msisdn'] . "\n";
            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($row['msisdn_sn']));
            $this->artilium->UpdateServices(trim($row['msisdn_sn']), $pack, '0');
            unset($pack);
        }
    }
    public function unblock_services($companyid)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 0);
        $this->db = $this->load->database('default', true);
        //SELECT *  FROM `a_services_mobile` WHERE `companyid` = 54 AND `msisdn_status` = 'OrderWaiting' AND `date_wish` = '2018-12-03'
        $q = $this->db->query("select a.*,b.date_contract,c.name as productname FROM a_services_mobile a left join a_services b on b.id=a.serviceid left join a_products c on c.id=b.packageid left join a_products_group d on d.id=c.gid WHERE a.companyid = ? AND a.donor_provider LIKE 'GSM1-YEST' AND a.date_wish ='2018-12-03' and a.msisdn_status != 'Active' group by a.id order by a.id asc", array(
            $companyid,
        ));
        foreach ($q->result_array() as $row) {
            echo $row['msisdn'] . "\n";
            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($row['msisdn_sn']));
            $this->artilium->UpdateServicesDone(trim($row['msisdn_sn']), $pack, '1', $row);
            unset($pack);
        }
    }
    public function insert_subscriptions_magebo()
    {
        $this->load->model('Api_model');
        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT a.id,b.msisdn_type,b.msisdn,b.msisdn_sn,b.msisdn_status,c.mageboid FROM a_services a left join a_services_mobile b on b.serviceid=a.id left join a_clients c on c.id=a.userid WHERE a.companyid = 54 AND a.status = 'Active' and a.notes = 'Imported from KPN' order by a.id asc");
        foreach ($q->result() as $row) {
            $mobile = $this->Admin_model->getServiceCli($row->id);
            echo $this->magebo->addPricingSubscription($mobile) . "\n";
            if ($row->msisdn_type == "porting") {
                $this->Api_model->PortIngAction1($mobile);
                $this->Api_model->PortIngAction2($mobile);
                $this->Api_model->PortIngAction3($mobile);
                $this->Api_model->PortIngAction4($mobile);
                $this->Api_model->PortIngAction5($mobile->companyid);
            }
            /*
            if(!$this->magebo->getRef($row->msisdn_sn)){
            $mobile = $this->Admin_model->getServiceCli($row->serviceid);
            $this->magebo->AddSIMToMagebo($mobile);
            }
             */
            echo $row->mageboid . " " . $row->msisdn . " " . $row->id . " " . $this->magebo->getRef($row->msisdn) . "\n";
            //echo $row->mageboid." ".$row->msisdn_sn." ".$row->serviceid." ".$this->magebo->hasPricingList($row->mageboid, $row->msisdn_sn)."\n";
        }
    }
    public function terminateCli()
    {
        $pr = $this->artilium->TerminateCLI("31648924532", date('Y-m-d') . 'T' . date('H:i:s'));
        print_r($pr);
    }
    public function addcli($userid)
    {
        $this->load->model('Api_model');
        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("SELECT a.userid,a.id,b.msisdn,b.msisdn_sn from a_services a left join a_services_mobile b on b.serviceid=a.id where a.userid=? order by a.id asc", array(
            $userid,
        ));
        foreach ($q->result() as $row) {
            print_r($row);
            $mobile = $this->Admin_model->getServiceCli($row->id);
            $addsim = $this->magebo->AddPortinSIMToMagebo($mobile);
            $this->Api_model->PortIngAction1($mobile);
            $this->Api_model->PortIngAction2($mobile);
            $this->Api_model->PortIngAction3($mobile);
            $this->Api_model->PortIngAction4($mobile);
            $this->Api_model->PortIngAction5($mobile->companyid);
            unset($mobile);
        }
        //1801996
    }
    public function checkPricing($mageboid, $number)
    {
        echo $this->magebo->hasPricingList($mageboid, $number);
    }
    public function process_porting_platform($data)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 0);
        $this->load->model('Admin_model');
        $next = true;
        $id = $data['serviceid'];
        $order = $this->Admin_model->getServiceCli($id);
        $noAddSim = false;
        $client = $this->Admin_model->getClient($order->userid);
        if (empty($data['msisdn_sn'])) {
            $test = $this->artilium->getSn(trim($data['msisdn_sim']));
            $this->Admin_model->update_services_data('mobile', $data['serviceid'], array(
                'msisdn_sn' => trim($test->data->SN),
                'msisdn_puk1' => $test->data->PUK1,
                'msisdn_puk2' => $test->data->PUK2,
            ));
            //$this->session->set_flashdata('success', lang('Serial Number has been updated..'));
            $order = $this->Admin_model->getServiceCli($data['serviceid']);
        } //empty($order->details->msisdn_sn)
        //$this->Teams_model->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " By" . $_SESSION['firstname'] . ' ' . $_SESSION['firstname']);
        $serviceid = $data["serviceid"];
        $date_contract = $data["date_contract"];
        //unset($data["serviceid"]);
        //unset($data["date_contract"]);
        $sn = $this->artilium->getSn($data['msisdn_sim']);
        if ($sn->result != "success") {
            echo "SN noy Found\n";
            //$this->session->set_flashdata('error', 'Simcard you provided :' . $data["msisdn_sim"] . ' is not provisioned, please check the number');
            //redirect('admin/subscription/activate_mobile/' . $id);
            exit;
        } //$sn->result != "success"
        $data["msisdn_sn"] = trim($sn->data->SN);
        $data["msisdn_puk1"] = $sn->data->PUK1;
        $data["msisdn_puk2"] = $sn->data->PUK2;
        // Start Get fresh parameters
        //$order  = $this->Admin_model->getService($serviceid);
        $nc = explode("-", $order->date_contract);
        $result = $this->artilium->ActiveNewSIM($client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $order->details);
        if ($result->result == "success") {
            logAdmin(array(
                'companyid' => $data['companyid'],
                'serviceid' => $serviceid,
                'userid' => $client->id,
                'user' => 'System',
                'ip' => '127.0.0.1',
                'description' => 'Service : ' . $serviceid . ' requested for Activation',
            ));
            if ($order->details->msisdn_type == "porting") {
                $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
            } //$order->details->msisdn_type == "porting"
            else {
                if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                    $this->Admin_model->ChangeStatusService($serviceid, 'Active');
                    $this->Admin_model->ChangeStatusOrderID($serviceid, 'Active', 'mobile');
                } else {
                    $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                }
            }
            $mobile = $this->Admin_model->getServiceCli($serviceid);
            if (!$noAddSim) {
                $addsim = $this->magebo->AddSIMToMagebo($mobile);
                if ($addsim->result == "success") {
                    if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                        $this->magebo->addPricingSubscription($mobile);
                    }
                    // Add Simcardlog into Magebo
                } //$addsim->result == "success"
                //assign Bundles when sim is activated
                $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
                if ($bundles) {
                    $IDS = array();
                    // activate tarief per packages
                    foreach ($bundles as $bundleid) {
                        $r = $this->artilium->AddBundleAssign($order->details->msisdn_sn, $bundleid, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
                        if ($r->result == "success") {
                            $create = 0;
                            $this->magebo->ProcessPricing($order, $client->mageboid, $bundleid, $create);
                            $IDS[] = $r->id;
                        } //$r->result == "success"
                        else {
                            $IDS[] = $r;
                        }
                    } //$bundles as $bundleid
                    if ($IDS) {
                        logAdmin(array(
                            'companyid' => $data['companyid'],
                            'serviceid' => $serviceid,
                            'userid' => $client->id,
                            'user' => 'System ',
                            'ip' => '127.0.0.1',
                            'description' => 'Service : ' . $data['serviceid'] . ' added bundle id ' . implode(',', $IDS),
                        ));
                        $this->Admin_model->updateBundleID($order->id, implode(',', $IDS));
                    } //$IDS
                } //$bundles
            } //!$noAddSim
            //Check if contract date is today otherwise disable all services
            if (date('Y-m-d') <= $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                $pack = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                $this->artilium->UpdateServices($msisdn->data->SN, $pack, '0');
            } //date('Y-m-d') <= $nc[2] . '-' . $nc[0] . '-' . $nc[1]
            else {
                $this->artilium->UpdateServices($msisdn->data->SN, $pack, '1');
                $this->artilium->UpdateCLI($msisdn->data->SN, 1);
            }
            if ($data["msisdn_type"] == "porting") {
                //sleep(2);
                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
            } //$_POST["msisdn_type"] == "porting"
            else {
                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                //sleep(2);
                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
            }
            print_r(array(
                'result' => 'Activation has been Requested',
            ));
        } //$result->result == "success"
        else {
            print_r(array(
                'result' => $result->message,
            ));
            //$this->session->set_flashdata('error', $result->message);
        }
        //redirect('admin/client/detail/' . $client->id);
        // End activating simcard
    }
    public function process_porting_platform_flex($data)
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 0);
        $this->load->model('Admin_model');
        $next = true;
        $id = $data['serviceid'];
        $order = $this->Admin_model->getServiceCli($id);
        $noAddSim = false;
        $client = $this->Admin_model->getClient($order->userid);
        if (empty($data['msisdn_sn'])) {
            $test = $this->artilium->getSn(trim($data['msisdn_sim']));
            $this->Admin_model->update_services_data('mobile', $data['serviceid'], array(
                'msisdn_sn' => trim($test->data->SN),
                'msisdn_puk1' => $test->data->PUK1,
                'msisdn_puk2' => $test->data->PUK2,
            ));
            //$this->session->set_flashdata('success', lang('Serial Number has been updated..'));
            $order = $this->Admin_model->getServiceCli($data['serviceid']);
        } //empty($order->details->msisdn_sn)
        //$this->Teams_model->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " By" . $_SESSION['firstname'] . ' ' . $_SESSION['firstname']);
        $serviceid = $data["serviceid"];
        $date_contract = $data["date_contract"];
        //unset($data["serviceid"]);
        //unset($data["date_contract"]);
        $sn = $this->artilium->getSn($data['msisdn_sim']);
        if ($sn->result != "success") {
            echo "SN noy Found\n";
            //$this->session->set_flashdata('error', 'Simcard you provided :' . $data["msisdn_sim"] . ' is not provisioned, please check the number');
            //redirect('admin/subscription/activate_mobile/' . $id);
            exit;
        } //$sn->result != "success"
        $data["msisdn_sn"] = trim($sn->data->SN);
        $data["msisdn_puk1"] = $sn->data->PUK1;
        $data["msisdn_puk2"] = $sn->data->PUK2;
        // Start Get fresh parameters
        //$order  = $this->Admin_model->getService($serviceid);
        $nc = explode("-", $order->date_contract);
        $result = $this->artilium->ActiveNewSIM($client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $order->details);
        if ($result->result == "success") {
            logAdmin(array(
                'companyid' => $data['companyid'],
                'serviceid' => $serviceid,
                'userid' => $client->id,
                'user' => 'System',
                'ip' => '127.0.0.1',
                'description' => 'Service : ' . $serviceid . ' requested for Activation',
            ));
            if ($order->details->msisdn_type == "porting") {
                $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
            } //$order->details->msisdn_type == "porting"
            else {
                if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                    $this->Admin_model->ChangeStatusService($serviceid, 'Active');
                    $this->Admin_model->ChangeStatusOrderID($serviceid, 'Active', 'mobile');
                } else {
                    $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                }
            }
            $mobile = $this->Admin_model->getServiceCli($serviceid);
            if (!$noAddSim) {
                $addsim = $this->magebo->AddSIMToMagebo($mobile);
                if ($addsim->result == "success") {
                    if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                        $this->magebo->addPricingSubscription($mobile);
                    }
                    // Add Simcardlog into Magebo
                } //$addsim->result == "success"
                //assign Bundles when sim is activated
                $bundles = $this->Admin_model->getBundleList($serviceid);
                if ($bundles) {
                    $IDS = array();
                    // activate tarief per packages
                    foreach ($bundles as $bundleid) {
                        $r = $this->artilium->AddBundleAssign($order->details->msisdn_sn, $bundleid, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
                        if ($r->result == "success") {
                            $create = 0;
                            $this->magebo->ProcessPricing($order, $client->mageboid, $bundleid, $create);
                            $IDS[] = $r->id;
                        } //$r->result == "success"
                        else {
                            $IDS[] = $r;
                        }
                    } //$bundles as $bundleid
                    if ($IDS) {
                        logAdmin(array(
                            'companyid' => $data['companyid'],
                            'serviceid' => $serviceid,
                            'userid' => $client->id,
                            'user' => 'System ',
                            'ip' => '127.0.0.1',
                            'description' => 'Service : ' . $data['serviceid'] . ' added bundle id ' . implode(',', $IDS),
                        ));
                        $this->Admin_model->updateBundleID($order->id, implode(',', $IDS));
                    } //$IDS
                } //$bundles
            } //!$noAddSim
            //Check if contract date is today otherwise disable all services
            if (date('Y-m-d') <= $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                $pack = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                $this->artilium->UpdateServices($msisdn->data->SN, $pack, '0');
            } //date('Y-m-d') <= $nc[2] . '-' . $nc[0] . '-' . $nc[1]
            else {
                $this->artilium->UpdateServices($msisdn->data->SN, $pack, '1');
                $this->artilium->UpdateCLI($msisdn->data->SN, 1);
            }
            if ($data["msisdn_type"] == "porting") {
                //sleep(2);
                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
            } //$_POST["msisdn_type"] == "porting"
            else {
                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                //sleep(2);
                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
            }
            print_r(array(
                'result' => 'Activation has been Requested',
            ));
        } //$result->result == "success"
        else {
            print_r(array(
                'result' => $result->message,
            ));
            //$this->session->set_flashdata('error', $result->message);
        }
        //redirect('admin/client/detail/' . $client->id);
        // End activating simcard
    }
    public function update_even_companyid()
    {
        $q = $this->db->query("select a.*,b.companyid as cid from a_event_status a left join a_services b on b.id=a.serviceid");
        foreach ($q->result() as $row) {
            $this->update_cid($row);
        }
    }
    public function tttt($serviceid, $newdate, $old_pid, $companyid)
    {
        $this->load->model('Admin_model');
        $InvoiceGroupNbr = $this->Admin_model->getiInvoiceGroupNbr($old_pid);
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        $service = $this->Admin_model->getServiceCli($serviceid);
        $date_contract = convert_to_contract($newdate);
        $this->data['setting'] = globofix($service->companyid);
        echo $date_contract . "\n";
        if ($service->iGeneralPricingIndex) {
            $GP = explode(',', $service->iGeneralPricingIndex);
            if (count($GP) == 1) {
                print_r($GP);
                $date_contract = convert_to_contract($newdate);
                echo $this->magebo->UpgradeDowngrade($service->iGeneralPricingIndex, str_replace('-', '', $newdate));
                $this->Admin_model->update_services($serviceid, array(
                    'iGeneralPricingIndex' => null,
                    'date_contract' => $date_contract,
                ));
                $this->process_pricing($service->userid, $serviceid, $service->companyid);
                $this->magebo->migrateCallPricing($service->iGeneralPricingIndex, str_replace('-', '', $newdate));
            } elseif (count($GP) > 1) {
                foreach ($GP as $G) {
                    print_r($G) . "\n";
                    echo $this->magebo->CheckSubscriptionPricing($InvoiceGroupNbr, $G) . "\n";
                    if ($this->magebo->CheckSubscriptionPricing($InvoiceGroupNbr, $G)) {
                        echo $this->magebo->UpgradeDowngrade($G, str_replace('-', '', $newdate));
                        $this->Admin_model->update_services($serviceid, array(
                            'iGeneralPricingIndex' => null,
                            'date_contract' => $date_contract,
                        ));
                        $this->process_pricing($service->userid, $serviceid, $service->companyid);
                        $this->magebo->migrateCallPricing($G, str_replace('-', '', $newdate));
                    }
                }
            }
            if ($service->companyid == "54") {
                $invoice_condition = $service->PriceList;
                $PriceListTarif = $service->PriceListSubType;
                $client = $this->Admin_model->getClient($service->userid);
                $priority = $this->magebo->getLatestPriority($service->details->msisdn);
                $this->magebo->InsertPricelist($client->mageboid, $invoice_condition, $service->details->msisdn, $PriceListTarif, $client->vat_exempt, $priority);
            }
        }
    }

    public function process_pricing($userid, $serviceid, $companyid)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid,
        ));
        $service = $this->db->query("select * from a_services where userid=? and status=? and id=?", array(
            $userid,
            'Active',
            $serviceid,
        ));
        if ($service->num_rows() > 0) {
            foreach ($service->result() as $row) {
                $mobile = $this->Admin_model->getServiceCli($row->id);
                if (!$row->iGeneralPricingIndex) {
                    echo "Executing..\n";
                    echo $this->magebo->addPricingSubscription($mobile, true) . "\n";
                    if (($row->msisdn_type == "porting") && (substr($mobile->details->msisdn, 0, 2) == "31")) {
                        $this->Api_model->PortIngAction1($mobile);
                        $this->Api_model->PortIngAction2($mobile);
                        $this->Api_model->PortIngAction3($mobile);
                        $this->Api_model->PortIngAction4($mobile);
                        $this->Api_model->PortIngAction5($mobile->companyid);
                    }
                    unset($mobile);
                }
            }
        }
    }
    public function fix_change_service()
    {
        $datetime = new DateTime('tomorrow');
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->load->model('Admin_model');
        $q = $this->db->query("select * from a_subscription_changes where  date_commit like ?", array(
            '2019-02-01',
        ));
        //  $q =  $this->db->query("select * from a_subscription_changes where cron_status =? and serviceid = ?", array('0', '1800687'));
        foreach ($q->result() as $row) {
            $killdate = new DateTime($row->date_commit);
            $killdate->modify('-1 day');
            $price = $this->Admin_model->getPriceProduct($row->new_pid, $row->ContractDuration);
            $this->db->query("update a_subscription_changes set new_price=? where id=?", array(
                $price,
                $row->id,
            ));
            echo $price . "\n";
            print_r(array(
                'packageid' => $row->new_pid,
                'recurring' => $price,
            ));
            echo "result: " . $this->Admin_model->update_services($row->serviceid, array(
                'packageid' => $row->new_pid,
                'recurring' => $price,
            ));
        }
    }

    public function test_magebo($id)
    {
        $this->load->library('magebo', array('companyid' => 54));
        echo $this->magebo->Mod11($id);
    }

    public function update_cid($row)
    {
        $this->db->where('id', $row->id);
        $this->db->update('a_event_status', array(
            'companyid' => $row->cid,
        ));
    }
    public function getCli($sn)
    {
        $ss = $this->artilium->getCli($sn);
        print_r($ss);
    }
    public function enable4G()
    {
        $filename = FCPATH . 'assets/datas/enable_roaming';
        $contents = file($filename);
        foreach ($contents as $line) {
            $this->db = $this->load->database('default', true);
            $q = $this->db->query("select msisdn_status,msisdn_sn as sn from a_services_mobile where msisdn=?", array(
                trim($line),
            ));
            foreach ($q->result() as $row) {
                if ($row->msisdn_status == "Active") {
                    $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($row->sn));
                    foreach ($pack as $s) {
                        print_r($s);
                        if ($s->Customizable == 1) {
                            if (trim($s->CallModeDescription) == "Data Roaming Originating") {
                                echo $row->sn . " - " . $s->PackageDefinitionId . "\n";
                                $this->artilium->UpdatePackageOptionsForSN($row->sn, $s->PackageDefinitionId, 1);
                            } elseif (trim($s->CallModeDescription) == "Roaming") {
                                echo $row->sn . " - " . $s->PackageDefinitionId . "\n";
                                $this->artilium->UpdatePackageOptionsForSN($row->sn, $s->PackageDefinitionId, 1);
                            }
                        }
                    }
                }
            }
        }
    }
    public function enable_setting()
    {
        $this->load->library('artilium', array(
            'companyid' => 54,
        ));
        $this->db = $this->load->database('default', true);
        $filename = FCPATH . 'assets/datas/barring2.csv';
        $contents = file($filename);
        foreach ($contents as $l) {
            $line = explode(';', $l);
            //print_r($line);
            $q = $this->db->query("select msisdn_status,msisdn_sn as sn from a_services_mobile where msisdn=?", array(
                trim($line[2]),
            ));
            if ($q->row()->sn) {
                $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($q->row()->sn));
                foreach ($pack as $s) {
                    if ($s->Customizable == 1) {
                        if (trim($s->CallModeDescription) == "Data Roaming Originating") {
                            echo $q->row()->sn . " - " . $s->PackageDefinitionId . " " . $line[6] . "\n";
                            print_r($this->artilium->UpdatePackageOptionsForSN($q->row()->sn, $s->PackageDefinitionId, $line[6]));
                        }
                        if (trim($s->CallModeDescription) == "Voice phone calls Roaming Originating") {
                            echo $q->row()->sn . " - " . $s->PackageDefinitionId . " " . $line[5] . "\n";
                            print_r($this->artilium->UpdatePackageOptionsForSN($q->row()->sn, $s->PackageDefinitionId, $line[5]));
                        }
                    }
                }
            }
        }
    }
    public function disable4G()
    {
        $this->load->library('artilium', array(
            'companyid' => 54,
        ));
        $filename = FCPATH . 'assets/datas/disable_data.csv';
        $contents = file($filename);
        foreach ($contents as $line) {
            $this->db = $this->load->database('default', true);
            $q = $this->db->query("select msisdn_status,msisdn_sn as sn from a_services_mobile where msisdn=?", array(
                trim($line),
            ));
            foreach ($q->result() as $row) {
                if ($row->msisdn_status == "Active") {
                    $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($row->sn));
                    foreach ($pack as $s) {
                        if ($s->Customizable == 1) {
                            if (trim($s->CallModeDescription) == "Data Roaming Originating") {
                                echo $s->CallModeDescription . " " . $row->sn . " - " . $s->PackageDefinitionId . "\n";
                                $this->artilium->UpdatePackageOptionsForSN($row->sn, $s->PackageDefinitionId, 0);
                            } elseif (trim($s->CallModeDescription) == "4G") {
                                echo $s->CallModeDescription . " " . $row->sn . " - " . $s->PackageDefinitionId . "\n";
                                $this->artilium->UpdatePackageOptionsForSN($row->sn, $s->PackageDefinitionId, 0);
                            } elseif (trim($s->CallModeDescription) == "Data Originating") {
                                echo $s->CallModeDescription . " " . $row->sn . " - " . $s->PackageDefinitionId . "\n";
                                $this->artilium->UpdatePackageOptionsForSN($row->sn, $s->PackageDefinitionId, 0);
                            }
                        }
                    }
                }
            }
        }
    }
    public function change_status()
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.serviceid,a.JobId,b.msisdn_sn from a_event_log a left join a_services_mobile b on b.serviceid=a.serviceid ");
        foreach ($q->result() as $row) {
            print_r($row);
            $this->db->query("update a_event_log set SubscriptionId=? where JobId=?", array(
                trim($row->msisdn_sn),
                $row->JobId,
            ));
        }
    }
    public function monthme($invoiceid)
    {
        print_r(getInvoiceMonth($invoiceid));
    }
    public function change_password()
    {
        $this->load->model('Admin_model');
        $q = $this->db->query("select id,password from a_clients where length(password) <= 12");
        foreach ($q->result() as $row) {
            $this->Admin_model->changePasswordClientV2($row);
            print_r($row);
        }
    }
    public function move_deltaInvoices()
    {
        $this->load->model('Admin_model');
        //$filename = FCPATH . 'assets/datas/move_invoice_delta';
        $contents = file($filename);
        foreach ($contents as $line) {
            $whmcsid = getWhmcsid(trim($line));
            $services = getActiveServices($whmcsid);
            $invoices = $this->magebo->getInvoices(trim($line));
            if ($services) {
                foreach ($services as $serv) {
                    print_r($serv);
                    /*
                $mobile = $this->Admin_model->getServiceCli($serv['id']);
                $terminate_now = true;
                $time          = date('H:i:s');

                $client = $this->Admin_model->getClient($mobile->userid);

                $date          = date('Y-m-d') . 'T' . $time;
                $this->db->where('id',$serv['id']);
                $this->db->where('companyid', $client->companyid);
                $this->db->update('a_services', array(
                'date_terminate' => date('Y-m-d'),
                'status' =>    'Terminated'));

                echo $this->db->affected_rows();
                $this->magebo->TerminateRequest($mobile, date('Y-m-d') . ' ' . $time);

                if ($mobile)
                {
                $pack   = $this->artilium->GetListPackageOptionsForSnAdvance(trim($mobile->details->msisdn_sn));
                $this->artilium->UpdateServices(trim($mobile->details->msisdn_sn), $pack, '0');
                $terminate = $this->artilium->TerminateCLI(trim($mobile->details->msisdn_sn), $date);

                if ($terminate->result == "success")
                {
                logAdmin(array(
                'companyid' => $client->companyid,
                'userid' => $client->id,
                'serviceid' => $serv['id'],
                'user' => 'System',
                'ip' => '127.0.0.1',
                'description' => 'SIMCARD  for serviceid ' . $serv['id'] . ' has been requested to be Terminated'
                ));

                } //$terminate->result == "success"
                else
                {
                $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
                $body    = "Termination request has just been reqested by : Bob Brower \n\nPlease terminate and create prorata for this customer\n\nMsisdn: " . $mobile->details->msisdn . "\nTermination Date: " . $date . "\niAddressNbr: " . $client->mageboid . "\n\nTo Get the list of Termination requests, go to: https://minmobiel.delta.nl/admin/subscription/termination_service_list";
                mail('mail@simson.one', 'Manual temrinate not working', $body, $headers);

                }
                } //$mobile

                 */
                }
            }
            if ($invoices) {
                foreach ($invoices as $inv) {
                    echo "Move Invoice " . $inv->iInvoiceNbr . " to Customer 200000000 from Customer " . trim($line) . "\n";
                }
            }
        }
    }
    public function moveService()
    {
        $filename = FCPATH . 'assets/datas/move_subscription';
        $contents = file($filename);
        foreach ($contents as $line) {
            $whmcsid = getWhmcsid(trim($line));
            $services = getActiveServices($whmcsid);
            if ($services) {
                foreach ($services as $se) {
                    echo trim($line) . ";" . $se['id'] . "\n";
                    $this->db->query("update a_services set userid=? where id=?", array(
                        '800064',
                        $se['id'],
                    ));
                    $this->db->query("update a_services_mobile set userid=? where serviceid=?", array(
                        '800064',
                        $se['id'],
                    ));
                }
            }
        }
    }
    public function update_ceiling()
    {
        $filename = FCPATH . 'assets/datas/ceiling.txt';
        $contents = file($filename);
        foreach ($contents as $line) {
            $s = explode(';', trim($line));
            if ($s[2] == 80) {
                $setid = 4;
            } elseif ($s[2] == 250) {
                $setid = 6;
            }
            $sn = $this->getSNNya($s[1]);
            echo $s[1] . " " . $sn . " " . $setid . "\n";
            if ($sn != "NOTFOUND") {
                echo $this->ceil($sn, $setid);
            }
            //
        }
    }
    public function test_smtp()
    {
        $this->data['setting'] = globofix('53');
        print_r($this->data['setting']);
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => $this->data['setting']->smtp_host,
            'smtp_port' => $this->data['setting']->smtp_port,
            'smtp_user' => $this->data['setting']->smtp_user,
            'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'starttls' => true,
            'wordwrap' => true,
        );
        print_r($config);
        $this->email->clear();
        $this->email->initialize($config);
        $body = "test";
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to("mail@simson.one");
        $this->email->subject(lang("Your New Password"));
        $this->email->message($body);
        if ($this->email->send()) {
            //$this->session->set_flashdata('success', 'Password has been sent!');
            echo json_encode(array(
                'result' => true,
            ));
        } else {
            echo $this->email->print_debugger();
        }
    }
    public function update_contractdate()
    {
        $filename = FCPATH . 'assets/datas/delta_contract_date_final';
        $contents = file($filename);
        foreach ($contents as $line) {
            $s = explode(';', trim($line));
            echo $s[0] . " " . $this->update_con($s[0], trim($s[1])) . "\n";
        }
    }
    public function update_con($id, $date)
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $id);
        $this->db->update('a_services', array(
            'date_contract' => trim($date),
        ));
        return $this->db->affected_rows();
    }
    public function GetOperatorList()
    {
        $this->load->library('artilium', array(
            'companyid' => 55,
        ));
        $q = $this->artilium->GetOperatorList();
        print_r($q);
    }
    public function createpaymentList($BatchPaymentId)
    {
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        $invoices = $this->magebo->getBankDetails($BatchPaymentId);
        foreach ($invoices as $invoice) {
            $this->db->insert('a_payments', array(
                'date' => '2019-02-04',
                'invoiceid' => trim($invoice->iInvoiceNbr),
                'userid' => getWhmcsid($invoice->iAddressNbr),
                'companyid' => 54,
                'paymentmethod' => "DD",
                'transid' => "Batch ID: " . $BatchPaymentId,
                'amount' => $invoice->mInvoiceAmount,
                'raw' => json_encode($invoice)));

            print_r($invoice);
        }
    }

    public function getpaymentlist($i)
    {
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        $is = $this->magebo->getPaymentListInvoice($i);

        print_r($is);
    }
    public function update_mageboid()
    {
        $this->load->model('Admin_model');
        $q = $this->db->query("select * from a_sepa_items where mandateid like 'SEPA%'");
        foreach ($q->result() as $row) {
            $iAddressNbr = $this->Admin_model->GetiAddressNbr($row->invoicenumber);
            print_r($iAddressNbr);
            //echo $iAddressNbr."\n";
            if ($iAddressNbr) {
                $this->db->query('update a_sepa_items set iAddressNbr=? where id=?', array($iAddressNbr->iAddressNbr, $row->id));
            }

            unset($iInvoiceNbr);
        }
    }
    public function cancelportin($sn)
    {
        $this->load->library('artilium', array('companyid' => 53));
        $res = $this->artilium->CancelPortIn($sn);
        print_r($res);
    }
    public function reload_fix()
    {
        $this->load->library('artilium', array(
            'companyid' => 55,
        ));
        $q = $this->db->query("SELECT * FROM `a_services_mobile` WHERE `companyid` = 55  AND msisdn_status in('Active','PortinPending')ORDER BY `initial_reload_amount` ASC");
        foreach ($q->result() as $row) {
            $this->load->model('Admin_model');
            $m = $this->Admin_model->GetServiceCli($row->serviceid);
            if ($m->details->msisdn != "32496172438") {
                print_r($m);
                $r = $this->artilium->reloadCredit($m);
                print_r($r);
                unset($r);
                unset($m);
            }
            //print_r($m);
        }
    }
    public function getSNNya($number)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where msisdn like ?", array(
            trim($number),
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->msisdn_sn;
        } else {
            return "NOTFOUND";
        }
    }
    public function ceil($sn, $setid)
    {
        echo json_encode($this->artilium->CreateAssignment(array(
            'SN' => $sn,
            'ActionSetId' => $setid,
            'AssignmentId' => 3,
        )));
    }
    public function getSum($companyid)
    {
        $this->load->model('Admin_model');
        $h = $this->Admin_model->getInvoiceSum($companyid, 'Unpaid');
        print_r($h);
    }
    public function update_bundle($sn, $date)
    {
        $this->load->library('artilium', array(
            'companyid' => 54,
        ));
        $bundles = $this->artilium->GetBundleAssignList($sn);
        foreach ($bundles->NewDataSet->BundleAssign as $bundle) {
            $res = $this->artilium->UpdateBundleAssignV2(array(
                'SN' => trim($sn),
                'BundleAssignId' => $bundle->BundleAssignId,
                'ValidUntil' => $date . 'T23:59:59',
            ));
            if ($res->UpdateBundleAssignV2Result->Result == "0") {
                $t = $this->artilium->GetBundleUsageList($sn, $bundle->BundleAssignId);
                if (is_array($t->NewDataSet->BundleUsage)) {
                    //because artilium stupidity about array and object
                    foreach ($t->NewDataSet->BundleUsage as $usage) {
                        print_r($this->artilium->UpdateBundleUsage(array(
                            "Sn" => $sn,
                            "BundleUsageId" => $usage->BundleUsageId,
                            "BundleUsageSetId" => $usage->BundleUsageSetId,
                            "ValidUntil" => $date . "T23:59:59",
                            "UpdateBundleAssignment" => false,
                        )));
                    }
                } else {
                    print_r($this->artilium->UpdateBundleUsage(array(
                        "Sn" => $sn,
                        "BundleUsageId" => $t->NewDataSet->BundleUsage->BundleUsageId,
                        "BundleUsageSetId" => $t->NewDataSet->BundleUsage->BundleUsageSetId,
                        "ValidUntil" => $date . "T23:59:59",
                        "UpdateBundleAssignment" => false,
                    )));
                }
                unset($t);
            }
        }
    }
    public function trendcall_price_error()
    {
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        $q = $this->db->query("select a.*,b.mageboid as iAddressNbr from a_services a left join a_clients b on b.id=a.userid where a.notes=? and a.id != ?", array(
            'Imported from KPN',
            1802290,
        ));
        foreach ($q->result() as $row) {
            if ($row->iGeneralPricingIndex) {
                echo $this->magebo->UpdatePricing((object) array(
                    'mUnitPrice' => exvat4('21', $row->recurring),
                    'iGeneralPricingIndex' => $row->iGeneralPricingIndex,
                    'iAddressNbr' => $row->iAddressNbr,
                ));
            }
            //print_r(array(includevat4('21',$row->recurring), $row->id));
            //$this->db->query("update a_services set recurring=? where id=?", array(includevat4('21',$row->recurring), $row->id));
        }
    }
    public function trendcall_priceitem_error()
    {
        $this->load->library('magebo', array(
            'companyid' => 54,
        ));
        $q = $this->db->query("select a.iGeneralPricingIndex,b.mageboid as iAddressNbr from a_services a left join a_clients b on b.id=a.userid where a.notes=?", array(
            'Imported from KPN',
        ));
        foreach ($q->result_array() as $row) {
            if ($row['iGeneralPricingIndex']) {
                $row['mUnitPrice'] = $this->magebo->GetPricingIndexPrice($row['iGeneralPricingIndex']);
                $this->magebo->updatePricingItem($row['iGeneralPricingIndex'], $this->magebo->GetPricingIndexPrice($row['iGeneralPricingIndex']));
                print_r($row);
                unset($row);
                //echo $this->magebo->UpdatePricing((object) array('mUnitPrice' => exvat4('21',$row->recurring), 'iGeneralPricingIndex' => $row->iGeneralPricingIndex, 'iAddressNbr' => $row->iAddressNbr));
            }
            //print_r(array(includevat4('21',$row->recurring), $row->id));
            //$this->db->query("update a_services set recurring=? where id=?", array(includevat4('21',$row->recurring), $row->id));
        }
    }
    public function fix_large_trendcall()
    {
        //1802847 1801941
        $this->load->library('artilium', array(
            'companyid' => 54,
        ));
        $large = $this->db->query("select a.*,b.msisdn_sn from a_services a left join a_services_mobile b on b.serviceid=a.id where a.packageid in(47,54,58,62) and a.companyid=54 and a.status ='Active' and a.id NOT  in(1801943,1801948,1801941,1800689,1801949) and a.id < 1802981");
        foreach ($large->result() as $l) {
            $this->artilium->AddBundleAssign($l->msisdn_sn, 50, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
            print_r($l);
            /*
            $bundles = $this->artilium->GetBundleAssignList($l->msisdn_sn);
            //$new = $this->artilium->AddBundleAssign($l->msisdn_sn, 50, '2019-01-01T00:00:00', '20199-12-31T23:59:59');
            //print_r($new);
            foreach($bundles->NewDataSet->BundleAssign as $bundle){
            if($bundle->BundleId == 11){

            print_r($temp);

            print_r($bundle);

            $res = $this->artilium->UpdateBundleAssignV2(array('SN' => trim($l->msisdn_sn), 'BundleAssignId' => $bundle->BundleAssignId, 'ValidUntil' => '2018-12-31T23:59:59'));
            if($res->UpdateBundleAssignV2Result->Result == "0"){
            $t = $this->artilium->GetBundleUsageList($l->msisdn_sn, $bundle->BundleAssignId);
            if(is_array($t->NewDataSet->BundleUsage)){ //because artilium stupidity about array and object
            foreach($t->NewDataSet->BundleUsage as $usage){
            print_r($this->artilium->UpdateBundleUsage(array("Sn" => $l->msisdn_sn,
            "BundleUsageId"  => $usage->BundleUsageId,
            "BundleUsageSetId"  => $usage->BundleUsageSetId,
            "ValidUntil" => "2018-12-31T23:59:59",
            "UpdateBundleAssignment" => FALSE)));
            }
            }else{
            print_r($this->artilium->UpdateBundleUsage(array("Sn" => $l->msisdn_sn,
            "BundleUsageId"  => $t->NewDataSet->BundleUsage->BundleUsageId,
            "BundleUsageSetId"  => $t->NewDataSet->BundleUsage->BundleUsageSetId,
            "ValidUntil" => "2018-12-31T23:59:59",
            "UpdateBundleAssignment" => FALSE)));
            }
            unset($t);
            }

            unset($res);

            }

            }

            unset($bundle);
             */
            //exit;
        }
    }
    public function fix_medium_trendcall()
    {
        //1802847 1801941
        $this->load->library('artilium', array(
            'companyid' => 54,
        ));
        $large = $this->db->query("select a.*,b.msisdn_sn from a_services a left join a_services_mobile b on b.serviceid=a.id where a.packageid in(45,52,56,62) and a.companyid=54 and a.id != 1802104 and a.id <= 1802912");
        foreach ($large->result() as $l) {
            $this->artilium->AddBundleAssign($l->msisdn_sn, 50, '2019-01-01T00:00:00', '2099-12-31T23:59:59');
            /*
        $temp = $this->artilium->AddBundleAssign($l->msisdn_sn, 48, date('Y-m-d').'T'.date('H:i:s'), '2018-12-31T23:59:59');
        $temp = $this->artilium->AddBundleAssign($l->msisdn_sn, 48, date('Y-m-d').'T'.date('H:i:s'), '2018-12-31T23:59:59');
        $bundles = $this->artilium->GetBundleAssignList($l->msisdn_sn);

        foreach($bundles->NewDataSet->BundleAssign as $bundle){
        if(in_array($bundle->BundleId, array('9','48'))){

        print_r($bundle);

        $res = $this->artilium->UpdateBundleAssignV2(array('SN' => trim($l->msisdn_sn), 'BundleAssignId' => $bundle->BundleAssignId, 'ValidUntil' => '2018-12-31T23:59:59'));
        if($res->UpdateBundleAssignV2Result->Result == "0"){
        $t = $this->artilium->GetBundleUsageList($l->msisdn_sn, $bundle->BundleAssignId);
        if(is_array($t->NewDataSet->BundleUsage)){ //because artilium stupidity about array and object
        foreach($t->NewDataSet->BundleUsage as $usage){
        print_r($this->artilium->UpdateBundleUsage(array("Sn" => $l->msisdn_sn,
        "BundleUsageId"  => $usage->BundleUsageId,
        "BundleUsageSetId"  => $usage->BundleUsageSetId,
        "ValidUntil" => "2018-12-31T23:59:59",
        "UpdateBundleAssignment" => FALSE)));
        }
        }else{
        print_r($this->artilium->UpdateBundleUsage(array("Sn" => $l->msisdn_sn,
        "BundleUsageId"  => $t->NewDataSet->BundleUsage->BundleUsageId,
        "BundleUsageSetId"  => $t->NewDataSet->BundleUsage->BundleUsageSetId,
        "ValidUntil" => "2018-12-31T23:59:59",
        "UpdateBundleAssignment" => FALSE)));
        }
        unset($t);
        }

        unset($res);

        }

        }
        unset($bundle);
         */
        }
    }
    public function fix_recurring()
    {
        $q = $this->db->query("SELECT id,recurring  FROM `a_services` WHERE `notes` LIKE '%Import%'");
        foreach ($q->result() as $row) {
            print_r($row);
        }
    }
    public function check_status_service()
    {
        $q = $this->db->query("select id,status from a_services where status = 'Terminated'");
        foreach ($q->result() as $row) {
            $this->db->query("update a_services_mobile set msisdn_status=? where serviceid=?", array(
                'Terminated',
                $row->id,
            ));
        }
    }
    public function service_changes_cron()
    {
        $this->load->model('Admin_model');
        //1802847 1801941
        $large = $this->db->query("select c.*,b.msisdn_sn from a_subscription_changes c left join a_services a on a.id=c.serviceid left join a_services_mobile b on b.serviceid=a.id where  and c.cron_status = '0'");
        foreach ($large->result() as $l) {
            $this->load->library('artilium', array(
                'companyid' => $l->companyid,
            ));
            $this->db->query("update a_subscription_changes set cron_status=? where id=?", array(
                1,
                $l->id,
            ));
            //$temp = $this->artilium->AddBundleAssign($l->msisdn_sn, 49, date('Y-m-d').'T'.date('H:i:s'), '2018-12-31T23:59:59');
            $bundles = $this->artilium->GetBundleAssignList($l->msisdn_sn);
            //$new = $this->artilium->AddBundleAssign($l->msisdn_sn, 50, '2019-01-01T00:00:00', '20199-12-31T23:59:59');
            //print_r($new);
            foreach ($bundles->NewDataSet->BundleAssign as $bundle) {
                print_r($bundle);
                $res = $this->artilium->UpdateBundleAssignV2(array(
                    'SN' => trim($l->msisdn_sn),
                    'BundleAssignId' => $bundle->BundleAssignId,
                    'ValidUntil' => date('Y-m-d', strtotime('-1 day', strtotime($l->date_commit))) . "T23:59:59",
                ));
                if ($res->UpdateBundleAssignV2Result->Result == "0") {
                    $t = $this->artilium->GetBundleUsageList($l->msisdn_sn, $bundle->BundleAssignId);
                    if (is_array($t->NewDataSet->BundleUsage)) {
                        //because artilium stupidity about array and object
                        foreach ($t->NewDataSet->BundleUsage as $usage) {
                            print_r($this->artilium->UpdateBundleUsage(array(
                                "Sn" => $l->msisdn_sn,
                                "BundleUsageId" => $usage->BundleUsageId,
                                "BundleUsageSetId" => $usage->BundleUsageSetId,
                                "ValidUntil" => date('Y-m-d', strtotime('-1 day', strtotime($l->date_commit))) . "T23:59:59",
                                "UpdateBundleAssignment" => false,
                            )));
                        }
                    } else {
                        print_r($this->artilium->UpdateBundleUsage(array(
                            "Sn" => $l->msisdn_sn,
                            "BundleUsageId" => $t->NewDataSet->BundleUsage->BundleUsageId,
                            "BundleUsageSetId" => $t->NewDataSet->BundleUsage->BundleUsageSetId,
                            "ValidUntil" => date('Y-m-d', strtotime('-1 day', strtotime($l->date_commit))) . "T23:59:59",
                            "UpdateBundleAssignment" => false,
                        )));
                    }
                    unset($t);
                }
                unset($res);
            }
            $newbundles = $this->Admin_model->getBundlebyProduct($l->new_pid);
            if ($newbundles) {
                $IDS = array();
                // activate tarief per packages
                foreach ($newbundles as $bundleid) {
                    $r = $this->artilium->AddBundleAssign($l->msisdn_sn, $bundleid, trim($l->date_commit) . 'T00:00:00', '2099-12-31T23:59:59');
                    unset($r);
                } //$bundles as $bundleid
            } //$bundles
            unset($newbundles);
            unset($bundle);
            unset($this->artilium);
        }
    }
    public function getKey()
    {
        $this->load->model('Admin_model');
        $result = $this->Admin_model->billi_api_auth(array(
            'username' => 'administration@united-telecom.be',
            'password' => 'cm3j39n40a',
        ));
        print_r($result);
    }
    public function process_camt($companyid, $id)
    {
        $setting = globofix($companyid);
        $file = "s8124505.ING.190107.CAMT05300102.DPA01.S.151499.P.NL76INGB0008519520.xml";
        $this->load->model('Cron_model');
        $this->companyid = $companyid;
        /*$connection = ssh2_connect( $setting->sftp_host,  $setting->sftp_port, array('hostkey'=>'ssh-rsa'));

        if (ssh2_auth_pubkey_file($connection,  $setting->sftp_user,
        $setting->sftp_pub,
        $setting->sftp_key)) {
        echo "Public Key Authentication Successful\n";

        $handle = opendir("ssh2.sftp://$sftp_host/path/to/directory");
        echo "Directory handle: $handle\n";
        echo "Entries:\n";
        while (false != ($entry = readdir($handle))){
        echo "$entry\n";
        }

        } else {
        die('Public Key Authentication Failed');
         */
        $id = 163;
        if ($id) {
            echo "\nProcessing " . $file . "\n";
            $reader = new Reader(Config::getDefault());
            $message = $reader->readFile($setting->DOC_PATH . '53/camt/' . $file);
            $statements = $message->getRecords();
            foreach ($statements as $statement) {
                foreach ($statement->getEntries() as $entry) {
                    $dddd = $entry->getBookingDate();
                    $date = $dddd->format('Y-m-d');
                    $BatchPaymentId = null;
                    $mandateid = "";
                    $endtoend = "";
                    $name = "";
                    $transactioncode = $entry->getBankTransactionCode()->getProprietary()->getCode();
                    $transactionissuer = $entry->getBankTransactionCode()->getProprietary()->getIssuer();
                    $amount = $entry->getAmount();
                    foreach ($entry->getTransactionDetails() as $r) {
                        $retinfo = $r->getReturnInformation();
                        if ($retinfo != null) {
                            $ret_code = $retinfo->getCode();
                            $ret_extra = $retinfo->getAdditionalInformation();
                        } else {
                            $ret_code = null;
                            $ret_extra = null;
                        }
                        //print_r( $entry->getBookingDate());
                        $reference = $r->getreferences();
                        foreach ($reference as $o) {
                            $mandateid = $o->getMandateId();
                            $endtoend = $o->GetendToEndId();
                        }
                        if ($mandateid) {
                            $remit = $r->getRemittanceInformation()->getMessage();
                        } else {
                            $remit = "";
                        }
                        $bank = $r->getRelatedParties();
                        foreach ($bank as $party) {
                            $name = $party->getAccount()->getIban();
                        }
                        $am = $amount->getAmount() * 0.01;
                        echo $am . "\n";
                        if ($am < 0) {
                            $creddeb = 'D';
                        } else {
                            $creddeb = 'C';
                        }
                        unset($am);
                        /*

                     */
                    }
                    $BatchPaymentId = $entry->getBatchPaymentId();
                    $this->db->insert('a_sepa_items', array(
                        'fileid' => "163",
                        'date_transaction' => $date,
                        'companyid' => $companyid,
                        'amount' => $amount->getAmount() * 0.01,
                        'iban' => $name,
                        'end2endid' => $endtoend,
                        'mandateid' => $mandateid,
                        'message' => $remit,
                        'status' => 'Pending',
                        'BatchPaymentId' => $BatchPaymentId,
                        'debitcredit' => $creddeb,
                        'transactionissuer' => $transactionissuer,
                        'transactioncode' => $transactioncode,
                        'return_code' => $ret_code,
                        'return_extra' => $ret_extra,
                    ));
                    echo $endtoend . " " . $name . " " . $mandateid . " " . $date . " " . $amount->getAmount() * 0.01 . " " . $remit . "\n";
                }
            }
        }
    }
    public function updateMvno($companyid)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid,
        ));
        $q = $this->db->query("select mageboid as id,mvno_id from a_clients where companyid=?", array(
            $companyid,
        ));
        foreach ($q->result_array() as $row) {
            $this->magebo->updateMvnoId($row);
            print_r($row);
        }
    }
    public function creditnote()
    {
        $this->load->model('Admin_model');
        $this->load->library('magebo', array(
            "companyid" => $companyid,
        ));
        $filename = FCPATH . 'assets/datas/delta_cn';
        $contents = file($filename);
        foreach ($contents as $line) {
            $i = explode(';', $line);
            /*
        $clientid = getClientidbyMagebo($c['iAddressNbr']);
        $client = $this->Admin_model->getClient($clientid);
        $date = new DateTime(date('Y-m-d'));
        $date->modify("+".$client->payment_duedays." day");

        if($c['amount'] > $c['mInvoiceAmount']){
        print_r(array('result' => 'error', 'Your Amount of Ceditnote seem to be larger than invoice amount'));

        }else{
        print_r( $this->magebo->CreateCn(array(
        'invoicenum' => $c['invoicenum'],
        'payment_duedays' =>  $c->payment_duedays,
        'date' => date('Y-m-d'),
        'duedate' => $date->format('Y-m-d'),
        'amount' => $c['amount'],
        'vat_rate' => $client->vat_rate,
        'message' => $c['description']." - ".$c['invoicenum'],
        'companyid' => $this->session->cid,
        'mageboid' => $c['iAddressNbr'])));

        }
         */
        }
    }
    public function createcn($c)
    {
    }
    public function logemail()
    {
        logEmailOut(array(
            'userid' => '11111',
            'to' => 'laiard@gmail.com',
            'subject' => 'test',
            'message' => 'test',
        ));
    }
    public function getSumCsv($companyid)
    {
        try {
            $setting = globofix($companyid);
            if (!file_exists($setting->DOC_PATH.$companyid.'/SUM')) {
                mkdir($setting->DOC_PATH.$companyid.'/SUM', 0755, true);
            }
            $this->load->model('Api_model');
            $this->load->model('Admin_model');
            $conn = ssh2_connect('10.17.2.51', 22);
            ssh2_auth_pubkey_file(
                $conn,
                'arta',
                '/home/mvno/.ssh/id_rsa.pub',
                '/home/mvno/.ssh/id_rsa'
            );

            echo "connected to 10.17.2.51\n";
            $sftp = ssh2_sftp($conn);
            $sftp_fd = intval($sftp);
            $handle = opendir("ssh2.sftp://$sftp_fd/home/arta/SUM/");
            echo "Directory handle: $handle\n";
            echo "Entries:\n";
            while (false != ($entry = readdir($handle))) {
                if (strpos($entry, '.csv')) {
                    echo "Downloading " .$entry."\n";
                    ssh2_scp_recv($conn, '/home/arta/SUM/'.$entry, $setting->DOC_PATH.$companyid.'/SUM/'.$entry);
                    if (file_exists($setting->DOC_PATH.$companyid.'/SUM/'.$entry)) {
                        ssh2_sftp_unlink($sftp, '/home/arta/SUM/'.$entry);
                    }
                    $this->Api_model->tc_table('a_sum_report');
                    echo "Table a_sum_report has been truncated";
                    $contents = file($setting->DOC_PATH.$companyid.'/SUM/'.$entry);
                    foreach ($contents as $index => $line) {
                        if ($index > 0) {
                            $d = explode(';', trim($line));
                            $date = explode('/', $d[5]);
                            $j = date('j');

                            $d[1] = str_replace('.', '', $d[1]);
                            $d1 = str_replace(',', '.', $d[1]);
                            $d1 =  preg_replace("/[^0-9,.]/", "", $d1);

                            $d[2] = str_replace('.', '', $d[2]);
                            $d2 = str_replace(',', '.', $d[2]);
                            $d2 =  preg_replace("/[^0-9,.]/", "", $d2);

                            $d3 = str_replace(',', '.', $d[3]);
                            $d3 =  preg_replace("/[^0-9,.]/", "", $d3);


                            $usage = $d2 - $d3;
                            $kontol = $d3/$j*30;
                            $memek = $d2-$kontol;
                            $meko = trim($date[2]).'-'.trim($date[1]).'-'.trim($date[0]);
                            echo $meko."\n";
                            if (trim($date[2]).'-'.trim($date[1]).'-'.trim($date[0]) == date('Y-m-').'01') {
                                $this->db->insert(
                                    'a_sum_report',
                                    array('msisdn'=> trim($d[0]),
                                    'amount' => $d1,
                                    'target' =>   $d2,
                                    'used' => $d3,
                                    'extra' =>  $d[4],
                                    'date' => trim($date[2]).'-'.trim($date[1]).'-'.trim($date[0]),
                                    'current_slack_percent' =>  $usage/$d2,
                                    'current_slack_amount' => $usage,
                                    'expectation_slack_percent' => $memek/$d2,
                                    'expectation_slack_amount' => $kontol
                                    )
                                );
                            }


                            unset($d);
                            unset($d1);
                            unset($d2);
                            unset($d3);
                            unset($date);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception moveCliContactid ", "Message: " . $e->getMessage());
        }
    }


    public function getFTP($companyid)
    {
        echo $companyid;
        $setting = globofix($companyid);
        error_reporting(1);
        // FTP server details
        $ftpHost = 'ftp.expatmobile.nl';
        $ftpUsername = 'expatmobile.nl';
        $ftpPassword = 'call4less';
        // open an FTP connection
        $connId = ftp_connect($ftpHost) or die("Couldn't connect to $ftpHost");
        // try to login ftp
        if (@ftp_login($connId, $ftpUsername, $ftpPassword)) {
            $this->load->model('Admin_model');
            echo "Connected as expatmobile.nl\n";
            $contents = ftp_nlist($connId, "/logs/Art*");
            foreach ($contents as $file) {
                $new_file = str_replace('/logs/', '/logs/done/', $file);
                $filename = $setting->DOC_PATH . $companyid . '/ftp/' . str_replace('/logs/', '', $file);


                if (ftp_get($connId, $filename, $file, FTP_ASCII)) {
                    echo "Successfully written to $filename\n";
                    $ccx = file($filename);
                    foreach ($ccx as $key => $line) {
                        if ($key > 0) {
                            $d = explode('|', $line);
                            echo "we found " . count($d) . "\n";
                            print_r($d);
                            log_message("error", print_r($d, 1));
                            if (count($d) == 42) {
                                $client = validate_email(strtolower(trim($d[9])), $companyid);
                                if ($client) {
                                    $cc = array(
                                        'result' => true,
                                        'id' => $client->id,
                                    );
                                /* $this->Admin_model->AddNotes(array(
                                     'companyid' => $companyid,
                                     'userid' => $client->id,
                                     'admin' => 'FTP Files',
                                     'note' => $d['31'],
                                     'sticky' => 1,
                                 ));
                                 */
                                } else {
                                    $password = random_str('alphacaps', 3) . random_str('num', 3);
                                    $client = array(
                                        'uuid' => gen_uuid(),
                                        'mvno_id' => null,
                                        'companyid' => $companyid,
                                        'email' => strtolower(str_replace(' ', '', $d[11])),
                                        'password' => password_hash($password, PASSWORD_DEFAULT),
                                        'companyname' => $d[17],
                                        'firstname' => $d[3],
                                        'lastname' => $d[2],
                                        'address1' => $d[5],
                                        'housenumber' => $d[6],
                                        'alphabet' => $d[7],
                                        'postcode' => strtoupper($d[8]),
                                        'city' => $d[9],
                                        'country' => $d[10],
                                        'language' => $d[27],
                                        'status' => 'Active',
                                        'phonenumber' => $d[21],
                                        'paymentmethod' => $d[34],
                                        'payment_duedays' => '14',
                                        'gender' => $d[13],
                                        'iban' => str_replace(' ', '', strtoupper($d[12])),
                                        'iban_holder' => $d[3] . ' ' . $d[2],
                                        'date_birth' => $d[18],
                                        'date_created' => date('Y-m-d'),
                                        'invoice_email' => 'yes',
                                        'refferal' => $d[20],
                                        'agentid' => $d[35],
                                        'authdata' => null,
                                        'location' => $d[15],
                                    );

                                    if (strtolower(trim($d[13])) == "male") {
                                        $client['salutation'] = "Mr.";
                                    } else {
                                        $client['salutation'] = "Mrs.";
                                    }
                                    $client['mvno_id'] = genMvnoId(54);
                                    if (empty($client['paymentmethod']) == "banktransfer") {
                                        $client['paymentmethod'] = "banktransfer";
                                    }

                                    if ($d[27] == "EN") {
                                        $client['language'] = "english";
                                    } elseif ($d[27] == "NL") {
                                        $client['language'] = "dutch";
                                    } elseif ($d[27] == "JP") {
                                        $client['language'] = "Japanese";
                                    } elseif ($d[27] == "FR") {
                                        $client['language'] = "french";
                                    }
                                    $this->load->library('magebo', array(
                                        'companyid' => $companyid,
                                    ));
                                    $this->load->model('Api_model');
                                    $this->load->model('Teams_model');
                                    $this->Teams_model->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($client, true));

                                    /* if (!empty($client['iban'])) {
                                         $bic = $this->Api_model->getBic(trim(str_replace(' ', '', $client['iban'])));
                                         if (empty($bic->bankData->bic)) {
                                             mail('mail@simson.one', 'Error Importing data IBAN not valid', print_r($client, true));
                                             echo json_encode(array(
                                                 'result' => false,
                                                 'message' => 'Your IBAN: ' . $client['iban'] . ' is not valid',
                                             ));
                                             $new_file = str_replace('/logs/', '/logs/rejected/', $file);
                                             if (ftp_rename($connId, $file, $new_file)) {
                                                 $this->send_attachment($new_file);
                                                 echo "successfully renamed rejected $file to $new_file .ERROR: 101\n";
                                             } else {
                                                 $this->send_attachment($file);
                                                 echo "There was a problem while renaming $file to $new_file\n";
                                             }
                                             $client['bic'] = '';
                                         } else {
                                             if (empty($client['paymentmethod'])) {
                                                 $client['paymentmethod'] = "banktransfer";
                                             }
                                             $client['bic'] = $bic->bankData->bic;
                                         }
                                     }
                                     */

                                    //print_r($client);



                                    $client['paymentmethod'] = "banktransfer";
                                    $magebo = $this->magebo->AddClient($client);
                                    if ($magebo->result == "success") {
                                        $client['mageboid'] = $magebo->iAddressNbr;
                                    } else {
                                        $new_file = str_replace('/logs/', '/logs/rejected/', $file);
                                        if (ftp_rename($connId, $file, $new_file)) {
                                            $this->send_attachment($filename, file_get_contents($filename));
                                            log_message("error", "successfully renamed rejected ".$file." to ".$new_file ."ERROR: 102");
                                        } else {
                                            $this->send_attachment($filename, file_get_contents($filename));
                                            log_message("error", "There was a problem while renaming ".$file." to ".$new_file);
                                        }
                                        log_message("error", "Adding to Magebo Failed");
                                        mail('mail@simson.one', 'adding to magebo failed', print_r($client, true));
                                        exit;
                                    }
                                    $cc = $this->Admin_model->insertCustomer($client);
                                    if ($cc['result']) {
                                        $this->Admin_model->AddNotes(array(
                                            'companyid' => $companyid,
                                            'userid' => $cc['id'],
                                            'admin' => 'FTP Files',
                                            'note' => $d['33'],
                                            'sticky' => 1,
                                        ));

                                        //  $this->Admin_model->addSMs($cd);
                                        logAdmin(array(
                                            'companyid' => $companyid,
                                            'userid' => $cc['id'],
                                            'user' => 'FTP Import',
                                            'ip' => '127.0.0.1',
                                            'description' => 'Customer id ' . $cc['id'].' created',
                                        ));
                                        $config['protocol'] = 'sendmail';
                                        $config['mailpath'] = '/usr/sbin/sendmail';
                                        $config['mailtype'] = 'html';
                                        $config['charset'] = 'utf-8';
                                        $config['wordwrap'] = true;
                                        $this->email->clear(true);
                                        $this->email->initialize($config);
                                        $client = $this->Admin_model->getClient($cc['id']);
                                        $body = getMailContent('signup_email', $client->language, $client->companyid);
                                        $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
                                        $body = str_replace('{$password}', $password, $body);
                                        $body = str_replace('{$email}', trim(strtolower($client->email)), $body);
                                        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                                        $this->email->set_newline("\r\n");
                                        $this->email->from($setting->smtp_sender, $setting->smtp_name);
                                        $this->email->to(strtolower(trim($client->email)));
                                        $this->email->subject(getSubject('signup_email', $client->language, $client->companyid));
                                        $this->email->message($body);
                                        if ($this->email->send()) {
                                            logEmailOut(array(
                                                'userid' => $client->id,
                                                'to' => $client->email,
                                                'subject' => getSubject('signup_email', $client->language, $companyid),
                                                'message' => $body,
                                                'companyid' => $companyid
                                            ));
                                        }

                                        if ($client->paymentmethod == "directdebit") {
                                            $sepa = $this->magebo->addMageboSEPA($client->iban, $client->bic, $magebo->iAddressNbr);
                                            $this->Admin_model->addMandate(array(
                                                'userid' => $cc['id'],
                                                'companyid' => $companyid,
                                                'mandate_id' => $sepa->SEPA_MANDATE,
                                                'mandate_date' => $sepa->SEPA_MANDATE_SIGNATURE_DATE,
                                                'mandate_status' => $sepa->SEPA_STATUS,
                                            ));
                                        }
                                    }
                                }
                                //adding Product
                                if ($cc['result']) {
                                    $ss = explode('-', $d['23']);
                                    $orderdata = array(
                                        'clientid' => $cc['id'],
                                        'brand' => 1,
                                        'pid' => $d['25'],
                                        'addons' => '',
                                        'contractduration' => $d['16'],
                                        'porting' => $d['24'],
                                        'startdate' => $ss['1'] . '-' . $ss['2'] . '-' . $ss['0'],
                                    );
                                    if ($orderdata['porting']) {
                                        $orderdata['porting_msisdn'] = $d[36];
                                        $orderdata['porting_date'] = $d[41];
                                        $orderdata['porting_customer_type'] = $d[37];
                                        $orderdata['porting_type'] = $d[38];
                                        $orderdata['porting_simnumber'] = "";
                                        $orderdata['porting_accountnumber'] = $d[39];
                                    }
                                    $result = $this->addOrder($orderdata, $companyid);
                                    log_message("error", print_r($result, true));
                                    if ($result['result'] == "success") {
                                        logAdmin(array(
                                            'companyid' => $companyid,
                                            'userid' => $cc['id'],
                                            'user' => 'System',
                                            'ip' => '127.0.0.1',
                                            'description' => 'Order Received ID ' . $result['serviceid'],
                                         ));

                                        if ($result['proformaid'] >0) {
                                            $resmail = $this->sendProforma(array('invoiceid' => $result['proformaid'], 'invoicenum' => $result['invoicenum'], 'companyid' => $companyid));
                                            log_message("error", print_r($resmail, true));
                                        }
                                    }
                                }
                                unset($client);
                            } else {
                                log_message('error', 'Checksum failed, total colum must be 42 but we have '.count($d));
                                $new_file = str_replace('/logs/', '/logs/rejected/', $file);
                                if (ftp_rename($connId, $file, $new_file."<br><br>".$file)) {
                                    $this->send_attachment($new_file);
                                    log_message("error", "successfully renamed rejected $file to $new_file .ERROR: 104");
                                } else {
                                    $this->send_attachment($file);
                                    log_message("error", "There was a problem while renaming $file to $new_file");
                                }
                                $mails = explode($client->email_notification);
                                $headers = "From: noreply@united-telecom.be";
                                if ($mails) {
                                    foreach ($mails as $email) {
                                        mail($client->email_notification, 'Error Importing data is not valid and has been rejected', print_r($client, true), $headers);
                                    }
                                }

                                die('Data Invalid Stop Process until it fixed');
                            }
                        }
                        unset($d);
                    }
                    if (ftp_rename($connId, $file, $new_file)) {
                        log_message("error", "successfully renamed ".$file." to ".$new_file);
                    } else {
                        log_message("error", "There was a problem while renaming ".$file." to ".$new_file);
                    }
                } else {
                    log_message("error", "Error downloading ".$filename);
                }
            }
        } else {
            log_message("error", "Couldn't connect as ". $ftpUsername);
        }
        // close the connection
        ftp_close($connId);
    }

    public function CreateProforma($companyid, $userid, $pid, $serviceid)
    {
        $this->load->model('Admin_model');

        $this->data['setting'] = globofix($companyid);
        $client = $this->Admin_model->getClient($userid);
        $killdate = new DateTime(date('Y-m-d'));
        $killdate->modify('+' . $client->payment_duedays . ' day');
        $setupfee = getSetupFee($pid, $client->vat_rate, $userid);
        if ($this->data['setting']->country_base == "NL") {
            $inum = getNewInvoicenum($companyid);
            echo $inum;

            $this->load->library('magebo', array('companyid' => $companyid));
            $ogm = $this->magebo->Mod11($inum);
        } else {
            $inum = getNewInvoicenum($companyid);
            $ogm = ogm($inum);
        }

        $proforma = array(
                    'companyid' => $companyid,
                    'invoicenum' => $inum,
                    'userid' => $userid,
                    'subtotal' => $setupfee->subtotal,
                    'tax' => $setupfee->tax,
                    'total' => $setupfee->total,
                    'date' => date('Y-m-d'),
                    'duedate' => $killdate->format('Y-m-d'),
                    'ogm' => $ogm,
                    'notes' => str_replace(' ', '', $ogm),
                    'paymentmethod' => 'banktransfer',
                    'status' => 'Unpaid');

        $proformaid = $this->Admin_model->CreateProforma($proforma);
        $result['proformaid'] = $proformaid;


        $result['invoicenum'] = $inum;
        if ($proformaid) {
            //$pin = "0" . substr($iPincode, 2, 1) . "-" . substr($iPincode, 3, 2) . " " . substr($iPincode, 5, 2) . " " . substr($iPincode, 7, 2) . " " . substr($iPincode, 9, 2);

            $this->Admin_model->CreateProformaItems(array(
            'companyid' => $companyid,
            'invoiceid' => $proformaid,
            'userid' => $userid,
            'description' => 'Subscription Activation Fee (one time)',
            'price' => $setupfee->total,
            'qty' => 1,
            'amount' => $setupfee->total,
            'taxrate' => $client->vat_rate,
            'serviceid' => $serviceid,
            'invoicetype' => 'Service',
                        ));
        }
    }
    public function resendProforma($companyid, $proformaid, $invoicenum)
    {
        print_r($this->sendProforma(array('invoiceid' => $proformaid, 'invoicenum' => $invoicenum, 'companyid' => $companyid)));
    }
    public function sendProforma($dd)
    {
        error_reporting(1);
        log_message("error", "Sending Proforma");
        $this->data['setting'] = globofix($dd['companyid']);
        $this->data['setting'] = globofix($dd['companyid']);
        $this->load->model('Admin_model');
        $this->load->model('Proforma_model');
        print_r($dd);
        //echo json_encode(array('result' => false, 'data' => $_POST)); dcoppieters@delta.nl
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/';
        $invoiceid = $dd['invoiceid'];
        // print_r($dd);
        $invoice = $this->Proforma_model->getInvoice($invoiceid);
        $client = $this->Admin_model->getClient($invoice['userid']);

        $this->load->library('magebo', array('companyid' => $client->companyid));
        if ($dd['invoicenum'] <= 0) {
            die('Invoice not found 11');
        }

        $invoice_ref = $this->magebo->Mod11($dd['invoicenum']);
        //$invoice_ref = "11111111111";
        log_message("error", print_r($invoice_ref, true));
        $res = $this->download_id($invoiceid, $dd['companyid'], $this->data['setting']);

        if ($res) {
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true,
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent('proforma', $client->language, $client->companyid);
            //$attachments = getAttachmentEmail('invoice', $client->language, $client->companyid);

            $amount = $invoice['total'];
            $body = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
            $body = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $body);
            $body = str_replace('{$invoiceid}', $invoice['id'], $body);
            $body = str_replace('{$clientid}', $client->mvno_id, $body);
            $body = str_replace('{$dInvoiceDate}', $invoice['date'], $body);
            $body = str_replace('{$dInvoiceDueDate}', $invoice['duedate'], $body);
            $body = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$cInvoiceReference}', $invoice_ref, $body);

            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            //$this->email->to(array('mail@simson.one'));
            $subject = getSubject('proforma', $client->language, $client->companyid);
            $subject = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $subject);
            $subject = str_replace('{$Companyname}', $this->data['setting']->companyname, $subject);
            //$this->email->bcc('mail@simson.one');
            $this->email->subject($subject);
            $this->email->attach($res);
            $this->email->message($body);
            if ($this->email->send()) {
                logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => $subject, 'message' => $body));

                return array('result' => true);
            } else {
                return array('result' => false, 'error' => $this->email->print_debugger());
            }
        } else {
            return array('result' => false);
        }
    }

    public function addOrder($dpost, $companyid)
    {
        $this->load->model('Api_model');
        $this->load->model('Teams_model');
        $this->load->model('Admin_model');
        $this->data['setting'] = globofix($companyid);
        $req = $this->Api_model->checkRequired(array(
            'clientid',
            'brand',
            'pid',
            'addons',
            'contractduration',
            'porting',
            'startdate',
        ), $dpost);
        $result = array(
            'result' => 'error',
        );
        $activate = false;
        $this->load->library('artilium', array(
            'companyaid' => $companyid,
        ));
        $this->Teams_model->send_msg($this->uri->segment(4), "<br /><br /> REQUEST:<br />" . json_encode($dpost));
        $recurring = "0.00";
        $update_handset = false;
        $pass = random_str('alphanum', 8);
        $extra = false;
        $dpost['companyid'] = $companyid;
        $gid = $dpost['pid'];
        /*
        if(!$gid){
        echo json_encode(array('result' => 'error', 'message' => 'Productid ' . $dpost['pid'] . ' does not have Group Assigned, Please contact +32484889888 for Help'));
        exit;
        }
         */
        if (!validate_cd(trim($dpost['startdate']))) {
            return array(
                'result' => 'error',
                'message' => 'startdate is invalid MM-DD-YYYY.',
            );
            exit;
        }
        if (!isAllowed_Clientid($companyid, $dpost['clientid'])) {
            return array(
                'result' => 'error',
                'message' => 'Clientid ' . $dpost['clientid'] . ' does not not found on companyid ' . $companyid,
            );
            exit;
        }
        if (!in_array($dpost['contractduration'], array(
            '1',
            '3',
            '6',
            '12',
            '24',
            '36',
        ))) {
            return array(
                'result' => 'error',
                'message' => 'contractduration supported is 1, 3, 6, 12, 24, 36 months ',
            );
            exit;
        }
        if (!empty($dpost['promocode'])) {
            $promo = strtoupper(trim($dpost['promocode']));
        } else {
            $promo = "";
        }
        $userid = $dpost['clientid'];
        $client = $this->Admin_model->getClient($dpost['clientid']);
        if (!empty($dpost['delta_sim'])) {
            if (trim($dpost['delta_sim']) != "1111111111111111111") {
                if (isSimcardFree(trim($dpost['delta_sim']))) {
                    return array(
                        'result' => 'error',
                        'message' => 'Your delta_sim has been used for previously order, please use free delta_sim',
                    );
                    exit;
                }
                if (strlen(trim($dpost['delta_sim'])) != 19) {
                    return array(
                        'result' => 'error',
                        'message' => 'Your delta_sim is Invalid, Format: 89XXXXXXXXXXXXXXXXX, 19 Digit, you provide: ' . $dpost['delta_sim'],
                    );
                    exit;
                } else {
                    $sim_ok = $this->Admin_model->getsimcard(trim($dpost['delta_sim']), $companyid);
                    if (!$sim_ok) {
                        return array(
                            'result' => 'error',
                            'message' => 'The Simcard: ' . $dpost['delta_sim'] . ' is not registered yet in database, please contact United telecom MVNO IT Support',
                        );
                        exit;
                    } else {
                        if (!empty($sim_ok->iPincode)) {
                            return array(
                                'result' => 'error',
                                'message' => 'The Simcard: ' . $dpost['delta_sim'] . ' has been assigned to MSISDN: ' . $sim_ok->iPincode . ' please use a free simcard',
                            );
                            exit;
                        }
                    }
                }
                $dpost['msisdn_sim'] = $dpost['delta_sim'];
                $sim = $this->artilium->getSn($dpost['delta_sim']);
                $dpost['msisdn_sn'] = trim($sim->data->SN);
                $dpost['msisdn'] = trim($sim->data->MSISDNNr);
                $dpost['msisdn_puk1'] = $sim->data->PUK1;
                $dpost['msisdn_puk2'] = $sim->data->PUK2;
            } else {
                $mobiledata['msisdn_sim'] = $dpost['delta_sim'];
            }
        }
        if ($dpost['porting']) {
            if (!$this->Admin_model->checkMsisdn(trim($dpost['porting_msisdn']))) {
                return array(
                    'result' => 'error',
                    'message' => 'The number ' . $dpost['porting_msisdn'] . ' already exists',
                );
                exit;
            }
            if (strlen(trim($dpost['porting_msisdn'])) != "11") {
                return array(
                    'result' => 'error',
                    'message' => 'You are requesting for PortIn, but your porting_msisdn is invalid FORMAT: (countrycode)(number without leading 0) ex: 31612345788',
                );
                exit;
            }
            if (DateTime::createFromFormat('Y-m-d', $dpost['porting_date']) !== false) {
                /* Weekend checks is enabled

            if (isWeekend($dpost['porting_date'])) {
            return array('result' => 'error', 'message' => 'Your porting_date is must not be in the weekend'));
            exit;
            }
             */
            } else {
                return array(
                    'result' => 'error',
                    'message' => 'Your porting_date is invalid YYYY-MM-DD',
                );
                exit;
            }
        }

        $needproforma = NeedProforma($userid);
        //If every parameter has been check, lets start inserting order
        $serviceid = $this->Api_model->insertOrder(array(
            'companyid' => $companyid,
            'type' => 'mobile',
            'status' => 'New',
            'packageid' => $dpost['pid'],
            'userid' => $userid,
            'date_created' => date('Y-m-d'),
            'billingcycle' => 'Monthly',
            'promocode' => $promo,
            'date_contract' => $dpost['startdate'],
            'contract_terms' => $dpost['contractduration'],
            'recurring' => getPriceRecurring($dpost['pid'], $dpost['contractduration']),
            'notes' => 'Order via file :',
        ));
        if (!$serviceid) {
            return array(
                'result' => 'error',
                'message' => 'Order failed please contact Support +32 484889888',
            );
            exit;
        } else {
            if ($this->data['setting']->mobile_provider == "TMNL") {
                $req1 = $this->Api_model->checkRequired(array(
                    'porting_msisdn',
                    'porting_provider',
                    'porting_type',
                    'porting_customer_type',
                    'porting_date',
                ), $dpost);
            } else {
                $req1 = $this->Api_model->checkRequired(array(
                    'porting_msisdn',
                    'porting_provider',
                    'porting_simnumber',
                    'porting_type',
                    'porting_customer_type',
                ), $dpost);
            }
            $result['result'] = "success";
            $result['serviceid'] = $serviceid;
            $result['orderid'] = $serviceid;
            if ($dpost['porting']) {
                if ($req1['result'] != "success") {
                    return $req1;
                    exit;
                }
                $mobiledata = array(
                    'msisdn_type' => 'porting',
                    'donor_msisdn' => preg_replace('/\D/', '', $dpost['porting_msisdn']),
                    'donor_provider' => $dpost['porting_provider'],
                    'donor_sim' => $dpost['porting_simnumber'],
                    'donor_type' => $dpost['porting_type'],
                    'donor_customertype' => $dpost['porting_customer_type'],
                    'msisdn_status' => 'OrderWaiting',
                    'date_wish' => $dpost['porting_date'],
                    'serviceid' => $serviceid,
                    'companyid' => $companyid,
                    'userid' => $userid,
                    'msisdn' => preg_replace('/\D/', '', $dpost['porting_msisdn']),
                    'msisdn_pin' => $this->data['setting']->default_pin,
                    'msisdn_sim' => $dpost['delta_sim'],
                    'ptype' => getSimType($companyid),
                );
                if ($this->data['setting']->mobile_provider != "TMNL") {
                    if ($dpost['porting_customer_type'] == "1") {
                        $req2 = $this->Api_model->checkRequired(array(
                            'porting_accountnumber',
                        ), $dpost);
                        if ($req2['result'] != "success") {
                            return $req2;
                            exit;
                        }
                        $mobiledata['donor_accountnumber'] = $dpost['porting_accountnumber'];
                    }
                } else {
                    if (!empty($dpost['porting_accountnumber'])) {
                        $mobiledata['donor_accountnumber'] = $dpost['porting_accountnumber'];
                    }
                }
            } else {
                $mobiledata = array(
                    'msisdn_type' => 'new',
                    'msisdn_status' => 'OrderWaiting',
                    'serviceid' => $serviceid,
                    'companyid' => $companyid,
                    'userid' => $userid,
                    'msisdn_pin' => $this->data['setting']->default_pin,
                    'msisdn_sim' => $dpost['delta_sim'],
                    'ptype' => 'POST PAID',
                );
            }

            unset($mobiledata['delta_sim']);
            $mobiledata['msisdn_contactid'] = $this->Admin_model->getContactIdArta($gid);
            if (!empty($dpost['handset_name'])) {
                $mobiledata_addons['serviceid'] = $serviceid;
                $mobiledata_addons['cycle'] = 'month';
                $mobiledata_addons['name'] = $dpost['handset_name'];
                $mobiledata_addons['terms'] = $dpost['handset_terms'];
                $dpost['handset_price'] = $dpost['handset_price'] * 1.21;
                $tot = round($dpost['handset_price'], 4) / $dpost['handset_terms'];
                //sending price exclusif vat;
                $mobiledata_addons['recurring_total'] = $tot;
                $mobiledata_addons['addon_type'] = 'others';
                $mobiledata_addons['addonid'] = '18';
                $this->db->insert('a_services_addons', $mobiledata_addons);
            }
            // mail('mail@simson.one','pppp',print_r($mobiledata_addons, true));
            $service_mobile = $this->Api_model->insertMobileData($mobiledata);
            if ($service_mobile > 0) {
                $email = getNotificationOrderEmail($companyid);
                if ($email) {
                    $product = getProduct($dpost['pid']);
                    $headers = "From: noreply@united-telecom.be";
                    log_message("error", "New Order has just been arrived, Please process this order as soon as possible:\n\nServiceid: " . $serviceid . "\nProduct: " . $product->name . "\n\nRegards");
                    mail($email, "New order notification", "Dear Team,\n\nNew Order has just been arrived, Please process this order as soon as possible:\n\nServiceid: " . $serviceid . "\nProduct: " . $product->name . "\n\nRegards", $headers);
                }
                $result['result'] = "success";

                if ($needproforma) {
                    $killdate = new DateTime(date('Y-m-d'));
                    $killdate->modify('+' . $client->payment_duedays . ' day');
                    $setupfee = getSetupFee($dpost['pid'], $client->vat_rate, $userid);
                    if ($this->data['setting']->country_base == "NL") {
                        $inum = getNewInvoicenum($companyid);
                        $this->load->library('magebo', array('companyid' => $companyid));
                        $ogm = $this->magebo->Mod11($inum);
                    } else {
                        $inum = getNewInvoicenum($companyid);
                        $ogm = ogm($inum);
                    }

                    $proforma = array(
                    'companyid' => $companyid,
                    'invoicenum' => $inum,
                    'userid' => $userid,
                    'subtotal' => $setupfee->subtotal,
                    'tax' => $setupfee->tax,
                    'total' => $setupfee->total,
                    'date' => date('Y-m-d'),
                    'duedate' => $killdate->format('Y-m-d'),
                    'ogm' => $ogm,
                    'notes' => str_replace(' ', '', $ogm),
                    'paymentmethod' => 'banktransfer',
                    'status' => 'Unpaid');

                    $proformaid = $this->Admin_model->CreateProforma($proforma);
                    $result['proformaid'] = $proformaid;


                    $result['invoicenum'] = $inum;
                    if ($proformaid) {
                        //$pin = "0" . substr($iPincode, 2, 1) . "-" . substr($iPincode, 3, 2) . " " . substr($iPincode, 5, 2) . " " . substr($iPincode, 7, 2) . " " . substr($iPincode, 9, 2);

                        $this->Admin_model->CreateProformaItems(array(
                        'companyid' => $companyid,
                        'invoiceid' => $proformaid,
                        'userid' => $userid,
                        'description' => 'Subscription Activation Fee (one time)',
                        'price' => $setupfee->total,
                        'qty' => 1,
                        'amount' => $setupfee->total,
                        'taxrate' => $client->vat_rate,
                        'serviceid' => $serviceid,
                        'invoicetype' => 'Service',
                        ));
                    }
                } else {
                    $result['proformaid']  ="0";
                }
            } else {
                $result['result'] = "error";
                $result['message'] = "Service mobile was not created";
            }
        }
        //mail("mail@simson.one", "Result addorder", print_r($result, true));
        return $result;
    }

    public function test_moxx()
    {
        echo "test\n";
        $this->load->library('artilium', array(
            'companyid' => 23,
        ));
        $sn = $this->artilium->GetSnMoxx('8932030000065527996');
        $active = $this->artilium->ActiveNewSIMMoxx('Sim Test', '32487147393', '8932030000065527996');
        print_r($active);
        print_r($sn);
    }
    public function oki()
    {
        $d = getSetupFee(51, 21);
        print_r($d);

        $inum = getNewInvoicenum(54);
        $this->load->library('magebo', array('companyid' => 54));
        $ogm = $this->magebo->Mod11($inum);
        echo $ogm;
    }
    public function updateLanguageVoice($companyid)
    {
        $this->load->model('Admin_model');
        $services = $this->db->query("select a.id,b.language from a_services a left join a_clients b on b.id=a.userid where a.companyid=?", array(
            $companyid,
        ));
        foreach ($services->result() as $row) {
            print_r($row);
            $this->Admin_model->update_services_data('mobile', $row->id, array(
                'msisdn_languageid' => setVoiceMailLanguageByClientLang($row->language),
            ));
        }
    }

    public function addPay($id)
    {
        $this->load->library("magebo", array('companyid' => 54));
        $invoices = $this->magebo->getBankDetails($id);

        if ($invoices) {
            $text .= date('Y-m-d H:i:s') . "- Processing Directdebit payments " . $row->BatchPaymentId . "\n";
            foreach ($invoices as $invoice) {
                $text .= date('Y-m-d H:i:s') . "- " . $invoice->iInvoiceNbr . "\n";
                if (trim($invoice->cTypeDescription) == "SEPA FIRST") {
                    $paymentform = 'SEPA FIRST';
                } else {
                    $paymentform = 'SEPA REOCCURRING';
                }

                echo "Processing :" . $paymentform . " " . $invoice->mBankFileAmount . " " . $invoice->iInvoiceNbr . " " . $invoice->iAddressNbr . "\n";
                $client = getClientDetailidbyMagebo($invoice->iAddressNbr);
                print_r(array(
                    'date' => date('Y-m-d'),
                    'invoiceid' => trim($invoice->iInvoiceNbr),
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'paymentmethod' => 'DD',
                    'transid' => 'BatchID: ' . $id,
                    'amount' => $invoice->mBankFileAmount,
                    'adminid' => 1,
                    'raw' => 'Executed by: Simson Parlindungan'));

                $this->db->insert('a_payments', array(
                    'date' => date('Y-m-d'),
                    'invoiceid' => trim($invoice->iInvoiceNbr),
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'paymentmethod' => 'DD',
                    'transid' => 'BatchID: ' . $id,
                    'amount' => $invoice->mBankFileAmount,
                    'adminid' => 1,
                    'raw' => 'Executed by: Simson Parlindungan'));

                // unset($res);
            }
        }
    }

    public function download_id($id, $companyid, $setting)
    {
        $this->data['setting'] = $setting;
        set_time_limit(0);
        $this->load->model('Admin_model');

        if ($companyid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
            //print_r($brand);
            //exit;
            $this->lang->load('admin');
        }
        $this->config->set_item('language', 'english');
        $this->lang->load('admin');
        // $this->lang->load('admin');
        $invoice = $this->Proforma_model->getInvoice($id);

        $customerku = $this->Proforma_model->getClient($invoice['userid']);
        $this->load->library('magebo', array('companyid' => $companyid));
        $this->load->library('Spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        //$pdf->setData($invoice['notes']);
        //$pdf->setBank($this->data['setting']->bank_acc);
        //$pdf->settax($invoice['tax']);
        //$pdf->SetTitle($this->data['setting']->companyname . ' ' . $invoice['invoicenum']);
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        //$pdf->droidsansogo($this->data['setting']->proforma_footer);
        $pdf->SetAutoPageBreak(true, 30);
        $pdf->SetAuthor('Simson Parlindungan & Tim Claesen');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->setFoot($brand->brand_footer);
        $pdf->setFooterFont('droidsans');
        //$pdf->disableExtrabox(true);

        /*
        $this->getPageNumGroupAlias() to get current page number
        and
        $this->getPageGroupAlias() to get total page count
         */

        $tax = round($invoice['tax']) . '%';

        $pdf->AddPage('P', 'A4');

        $pdf->Image($brand->brand_logo, 10, 10, 60);
        $style = array('position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false,
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6,
        );

        $style1 = array('position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(64, 64, 64),
            'bgcolor' => false,
            'color' => array(255, 255, 255),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8, 'stretchtext' => 6,
        );
        $pdf->setY(13);
        $pdf->setX(130);
        // $pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');

        $pdf->GetPage();

        $pdf->Image($brand->brand_logo, 10, 10, 90);
        $pdf->SetFont('droidsans', '', 10);
        if (!empty($customerku->companyname)) {
            $company = $customerku->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            $contact = $customerku->firstname . " " . $customerku->lastname . "\n";
        }
        $address1 = str_replace("&#039;", "'", $customerku->address1) . "\n";
        $pdf->setY(70);
        $city = $customerku->postcode . ", " . $customerku->city . "\n";
        $country = $customerku->country . "\n";
        $pdf->SetFont('droidsans', 'B', 12);
        $pdf->setCellPaddings(0, 0, 0, 0);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $a = '';
        $pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 30, 'T');
        $pdf->MultiCell(90, 30, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryNameLang($customerku->country, $customerku->language), 0, 'L', 1, 1, '', '', true, 0, false, true, 30, 'M');

        if ($this->data['setting']->country_base == "BE") {
            $this->data['invoice_ref'] = ogm($this->uri->segment(4));
        } else {
            $this->data['invoice_ref'] = $this->magebo->Mod11($invoice['invoicenum']);
            // $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
        }

        $pdf->SetFont('droidsans', 'B', 14);
        //$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 153, 0)));
        $pdf->cell(70, 4, lang("Proforma Invoice") . ": " . $invoice['invoicenum'], 0, 1, 'L');
        $pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(64, 64, 64);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(35, 7, lang('Client Number'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(40, 7, lang('VAT'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(50, 7, lang('Reference'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Date'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Duedate'), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->setCellPaddings(1, 1, 1, 1);
        $pdf->setCellMargins(1, 1, 1, 1);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('droidsans', '', 9);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(35, 5, trim($invoice['mvno_id']), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(40, 5, $customerku->vat, 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(50, 5, $this->data['invoice_ref'], 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['date'])), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['duedate'])), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->Ln(15);

        # Invoice Items
        $tblhtml = '<table width="550" bgcolor="#404040" cellspacing="1" cellpadding="1" border="0">
    <tr height="40" bgcolor="#404040" style="font-weight:bold;text-align:center;">
    <td align="left" width="7%"  color="#fff" >' . lang('Qty') . '</td>
        <td align="left" width="58%"  color="#fff">' . lang('Description') . '</td>
         <td align="right" width="18%" color="#fff">' . lang('Price/pcs') . '</td>
        <td align="right" width="18%" color="#fff">' . lang('Amount') . '</td>
    </tr>';
        foreach ($invoice['items'] as $item) {
            $tblhtml .= '
    <tr bgcolor="#fff" border="1">
         <td align="left">' . $item['qty'] . '<br /></td>
        <td align="left">' . nl2br($item['description']) . '<br /></td>
        <td align="right">' . '&euro;' . $item['price'] . '</td>
        <td align="right">' . '&euro;' . exvat($item['taxrate'], $item['amount']) . '</td>
    </tr>';
        }
        $tblhtml .= '

</table>';

        $pdf->writeHTML($tblhtml, true, false, false, false, '');

        $pdf->Ln(5);

        $styles = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64));

        //$pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));

        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, 'Subtotal' . " :", 1, 'R', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, '€' . $invoice['subtotal'], 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, "VAT " . floor($invoice['items'][0]['taxrate']) . '%:', 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, '€' . $invoice['tax'], 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');

        $pdf->SetFillColor(64, 64, 64);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->MultiCell(30, 5, "Total :", 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, '€' . $invoice['total'], 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $taxrate = 1;
        $lebar = '38';
        $ypos = '100';
        $xpos = '161';
        /*
        Gelieve het totale bedrag te storten voor 13/03/2019
        op rekeningnummer NL10RABO0147382548
        met vermelding van de referentie 5310 0005 5200 0000
         */
        $pdf->setY(-120);
        $pdf->SetLeftMargin(-10);
        $pdf->SetRightMargin(0);
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('droidsansb', 'B', 7);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setImageScale(1.53);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(5, 240, 200, 40, 3.50, '1111');
        $pdf->setXY(10, 242);
        $pdf->SetFont('droidsansb', '', 12);
        $pdf->Cell(140, 0, lang("Please pay amount of ") . " €" . $invoice['total'] . " " . lang("to be transfered before ") . date("d-m-Y", strtotime($invoice['duedate'])), 0, 0, 'R', 0, '0', 4);
        $pdf->setXY(10, 248);
        $pdf->Cell(100, 0, lang("On account number") . " : NL10RABO0147382548", 0, 0, 'R', 0, '0', 4);
        if ($invoice['items'][0]['taxrate'] > 0) {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        } else {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(30, 0, lang("Vat Exempt"), 0, 0, 'R', 0, '0', 4);
            $pdf->ln(4);
            $pdf->setXY(10, 270);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        }
        //$this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/'

        $pdf->Output($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf', 'F');
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf')) {
            return $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf';
        } else {
            return false;
        }
    }

    public function createInvoice()
    {
        $this->load->model('Admin_model');
        $ss = $this->db->query("select a.*,b.date as paiddate,b.transid,c.mageboid as iAddressNbr from a_invoices a left join a_payments b on b.invoiceid=a.invoicenum left join a_clients c on c.id=a.userid where a.invoicenum > 1 and a.status='Paid' and a.iInvoiceNbr is NULL and a.companyid=54");

        foreach ($ss->result() as $s) {
            $clientid = getClientidbyMagebo($s->iAddressNbr);
            $client = $this->Admin_model->getClient($clientid);

            $dd = array('iAddressNbr' => $s->iAddressNbr,
            'amount' =>  $s->total,
            'date_paid' => $s->paiddate,
            'invoicenum' => $s->invoicenum,
            'description' => 'Subscription Activation Fee (one time)',
            'trxid' => $s->transid);
            if (empty($dd['date_paid'])) {
                $okra =  explode(' ', $s->datepaid);

                $dd['date_paid'] = $okra[0];
            }
            $this->load->library('magebo', array("companyid" => $client->companyid));

            $date = new DateTime(date('Y-m-d'));
            $date->modify("+" . $client->payment_duedays . " day");

            if ($dd['amount'] <= 0) {
                echo json_encode(array('result' => 'error', 'Your Amount of Invoice seem to 0 amount'));
                exit;
            }

            $result = $this->magebo->CreateInvoice(array(
            'invoicenum' => $dd['invoicenum'],
            'payment_duedays' => $client->payment_duedays,
            'date' => date('Y-m-d'),
            'duedate' => $date->format('Y-m-d'),
            'amount' => $dd['amount'],
            'vat_rate' => $client->vat_rate,
            'message' => $dd['description'] . " - " . $dd['invoicenum'],
            'companyid' => $client->companyid,
            'transactionid' => $dd['trxid'],
            'mageboid' => $dd['iAddressNbr']));
            if ($result['result'] == "success") {
                $payment = array('mPaymentAmount' => $dd['amount'],
                                            'cPaymentForm' => 'BANK TRANSACTION',
                                            'cPaymentFormDescription' => $dd['trxid'],
                                            'dPaymentDate' => convert_invoice_date($dd['date_paid']),
                                            'iInvoiceNbr' => trim($result['iInvoiceNbr']),
                                            'step' => 1,
                                            'iAddressNbr' => $dd['iAddressNbr'],
                                            'cPaymentRemark' =>  'Payment Proforma: '.$s->invoicenum);
                $response = $this->magebo->apply_payment($payment);
                print_r($response);
                //if($response->id >0){
                $this->Admin_model->SetiInvoiceNbrProforma($s->invoicenum, $result['iInvoiceNbr']);
                //}
            }
            print_r($result);
            //exit;
        }
    }

    public function import_internet()
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("select L.iAddressNbr, I.cInvoiceGroup,

 CASE when G.iTotalMonths <= P.CNT

       then 'Stopped'

       else CASE G.bEnabled when 1 then 'Active' else 'Disabled' end end as Status,

CONVERT(VARCHAR, A.dActivationDate, 103 ) as ActivationDate,  A.iMonth, A.iYear,

G.iTotalMonths, CASE when P.CNT is null then 0 else P.CNT end As PricingCounter,

 G.iGeneralPricingIndex, G.mUnitPrice as Price,

 CASE G.bEnabled when 0 then '' else

CASE WHEN G.iTotalMonths > P.CNT OR CASE when P.CNT is null then 0 else P.CNT end = 0

       THEN 'x'

       ELSE '' END END as Billing,

DATEADD(month, -1, CAST(CAST(YEAR(A.dActivationDate) AS VARCHAR(4)) + '/' +

 CAST(MONTH(A.dActivationDate) AS VARCHAR(2)) + '/01' AS DATETIME)) as START,

 dateadd(second,-1, CAST(CAST(YEAR(A.dActivationDate) AS VARCHAR(4)) + '/' +

 CAST(MONTH(A.dActivationDate) AS VARCHAR(2)) + '/01' AS DATETIME)) as 'END', AG.iAgentNbr, AGA.cName,

CASE when CASE when G.iInvoiceGroupNbr in ( 225,226 ) then right(G.cRemark,8) else '' end

       IN ( select cast(iPincode as varchar) from tblC_Pincode )

      then right(G.cRemark,8)

      else '0' end as Pincode

from dbo.tblADSL_Activations A

left join tblGeneralPricing G ON G.iGeneralPricingIndex = A.iGeneralPricingIndex

left join ( select iGeneralPricingIndex, count(*) as CNT from tblPricedItems

       where iGeneralPricingIndex in ( select iGeneralPricingIndex from tblADSL_Activations )

       group by iGeneralPricingIndex ) P ON P.iGeneralPricingIndex = G.iGeneralPricingIndex

left join tblInvoiceGroup I ON I.iInvoiceGroupNbr = G.iInvoiceGroupNbr

left join tblAllinOne AL on AL.iAllinOneIndex = A.iAllinOneIndex

left join tblLocation L ON L.iLocationIndex = AL.iLocationIndex

left join ( select iAddressNbr, min(iAgentIndex) as iAgentIndex

       from tblAgent

       where NOT iAgentNbr in ( 0, 96851 )

       group by iAddressNbr ) AGI ON AGI.iAddressNbr = L.iAddressNbr

left join tblAgent AG ON AG.iAgentIndex = AGI.iAgentIndex

left join tblAddress AGA ON AGA.iAddressNbr = AG.iAgentNbr

where cPricingType = 'Monthly'

  AND dActivationDate is not null

order by A.dActivationDate");
        echo "There are ".$q->num_rows()."\n";
        sleep(5);
        $count = 0;
        foreach ($q->result() as $row) {
            if ($row->Status == "Active") {
                // print_r($row);
                $product[] = $row->cInvoiceGroup;
                $d = explode('/', $row->ActivationDate);
                $userid = getIdMvno_id($row->iAddressNbr, 2);

                if (!$userid) {
                    echo "Creating Customer by Importing from Magebo\n";
                    $userid =   $this->import_united_clientfu($row->iAddressNbr, 2);
                }
                echo    $this->insert_service(array('companyid' => 2,
                'type' => 'xdsl',
                'status' => 'Active',
                'packageid' => getProductidbyName(trim($row->cInvoiceGroup)),
                'userid' => $userid,
                'date_created' => $d[2]."-".$d[1]."-".$d[0],
                'date_terminate' => null,
                'termination_status' => null,
                'billingcycle' => 'Monthly',
                'date_start' => $d[2]."-".$d[1]."-".$d[0],
                'date_contract' => $d[2]."-".$d[1]."-".$d[0],
                'recurring' => includevat4(21, $row->Price),
                'username' => null,
                'password' => null,
                'notes' => null,
                'promocode' => null,
                'iGeneralPricingIndex' => $row->iGeneralPricingIndex,
                'contract_terms' => '12',
                'suspend_reason' => null,
                'reject_reason' => null,
                'future_activation' => null));

                $count++;
            }
        }
        echo "There are ".$count." Active\n";
        /*$keke = array_unique($product);
        foreach($keke as $p){
           echo "insettinf ".$p."\n";
           //print_r(array('companyid' => 2, 'name' => trim($p), 'type' => 'xdsl', 'recurring_subtotal' => '10', 'setup' => '15'));
           $da= array('companyid' => 2, 'gid' => 16, 'name' => trim($p), 'product_type' => 'xdsl', 'recurring_subtotal' => '10', 'setup' => '15');
           $this->lele($da);
        }
        */
    }
    public function addproducts($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_products', $data);

        if ($this->db->insert_id()) {
            return true;
        } else {
            return 'kontol';
        }
    }

    public function insert_service($data)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_services', $data);

        return $this->db->insert_id();
    }

    public function import_united_clientfu($clientid, $companyid)
    {
        $this->load->library('magebo', array(
          'companyid' => $companyid,
        ));

        $this->db->insert('a_clients', $this->magebo->getiAddress($clientid));
        return  $this->db->insert_id()." Created\n";
    }

    public function import_whmcs()
    {
        try {
            $this->load->model('Admin_model');
            $q = $this->db->query("select concat(c.firstname,' ',c.lastname) as customername, a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
         case when a.type  = 'mobile' THEN d.msisdn
         when a.type = 'xdsl' then e.circuitid
         when a.type = 'voip' then f.cli
         else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
         when a.type = 'xdsl' then e.status
         when a.type = 'voip' then f.status
         else 'Unknown' end as orderstatus
      from a_services a
      left join a_products b on b.id=a.packageid
      left join a_clients c on c.id=a.userid
      left join a_services_mobile d on d.serviceid=a.id
      left join a_services_xdsl e on e.serviceid=a.id
      left join a_services_voip f on f.serviceid=a.id
      where a.status in ('Pending','Active')
      and a.companyid = '2'
      group by a.id");

            foreach ($q->result() as $row) {
                if ($row->domain <= 0) {
                    echo "Let's Check customer ".$row->customername." ".$row->id."\n ";
                    echo
                       $ff = $this->getWhmcsOrder($row->customername, $row->id);
                    if ($ff->result == "success") {
                        if ($ff->xdslCircuitID >0) {
                            $this->processImport(array(
                               'city' => $ff->oaCity,
                                'whmcsid' => $ff->serviceid,
                                 'street' => $ff->oaStreet,
                                  'number' => $ff->oaHouseNumber,
                                   'postcode' => $ff->oaZipCode,
                                    'alpha' => $ff->oaHouseNumberAlfa,
                                     'serviceid' => $row->id,
                                      'realm' => $ff->realm,
                                       'circuitid' => $ff->xdslCircuitID,
                                       'userid' => $row->userid,
                                       'username' => $ff->dsllogin,
                                       'password' => $ff->dslpass
                            ));
                        }
                    }
                }
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception Import ", "Message: " . $e->getMessage());
        }
    }

    public function getWhmcsOrder($name, $serviceid)
    {
        $this->load->model('Admin_model');
        $this->db = $this->load->database('import', true);
        $client = $this->db->query("select id,firstname,lastname,state from tblclients WHERE concat(firstname,' ',lastname) LIKE ?", array('%'.$name.'%'));

        if ($client->num_rows() == 1) {
            //echo "Found Client ".$client->row()->id." With the following Data\n";
            //print_r($client->row());
            $xservice = $this->db->query("select a.id,a.packageid,a.domainstatus, b.name from tblhosting a left join tblproducts b on b.id=a.packageid where a.userid=?", array($client->row()->id));
            if ($xservice->num_rows() > 0) {
                // echo "We found ".$xservice->num_rows()." services for customer ".$name."\n";
                $ok = 0;
                foreach ($xservice->result() as $r) {
                    //  print_r($r);
                    if (in_array(array('Active','Pending'), $r->domainstatus)) {
                        //  echo "Yes!!! We found Active Subscription.. lets check if this is XDSL\n";
                        if (in_array(array('DSL','DSL_B','Sel','Super Snel','Surf@home','Surf&Talk ADSL','Surf&Talk VDSL','Surf@Home ADSL','Surf@Home VDSL','Surf@Work'), $r->name)) {
                            // echo "This is  the one\n";
                            // print_r($r);
                            $fin = $r;
                            $ok++;
                        } else {
                            // echo "This is not the one\n";
                            //print_r($r);
                        }
                    }
                }

                if ($ok == "1") {
                    $service = $this->Admin_model->getServiceCli($serviceid);
                    $params['userid'] = $service->userid;

                    $params['result'] = 'success';
                    $this->db = $this->load->database('import', true);
                    $res = $this->Admin_model->united_api(array('action' => 'GetClientsProducts', 'serviceid' => $r->id));
                    // $params['res']= $res;
                    if ($res->result == "success") {
                        $params['serviceid'] = $r->id;
                        $product = $res->products->product[0];

                        foreach ($product->customfields->customfield as $row) {
                            $params[str_replace('.', '', $row->name)] = $row->value;
                        }
                    } else {
                        $params['result'] = 'error';
                    }
                    return (object) $params;
                }
                /*

                */
            }
        } elseif ($client->num_rows() == 0) {
            if (count(explode(' ', trim($name))) == 2) {
                $mm=explode(' ', trim($name));

                $this->db = $this->load->database('import', true);
                $client = $this->db->query("select id,firstname,lastname,state from tblclients WHERE concat(firstname,' ',lastname) LIKE ?", array('%'.$mm[1].' '.$mm[0].'%'));

                if ($client->num_rows() == 1) {
                    //echo "Found Client ".$client->row()->id." With the following Data\n";
                    //print_r($client->row());
                    $xservice = $this->db->query("select a.id,a.packageid,a.domainstatus, b.name from tblhosting a left join tblproducts b on b.id=a.packageid where a.userid=? order by a.id asc", array($client->row()->id));
                    if ($xservice->num_rows() > 0) {
                        // echo "We found ".$xservice->num_rows()." services for customer ".$name."\n";
                        $ok = 0;
                        foreach ($xservice->result() as $r) {
                            // print_r($r);
                            if (in_array($r->domainstatus, array('Active','Pending'))) {
                                //echo "Yes!!! We found Active Subscription.. lets check if this is XDSL\n";
                                if (in_array($r->name, array('DSL','DSL_B','Sel','Super Snel','Surf@home','Surf&Talk ADSL','Surf&Talk VDSL','Surf@Home ADSL','Surf@Home VDSL','Surf@Work'))) {
                                    //echo "This is  the one\n";
                                    //print_r($r);
                                    $ok++;
                                    $fin = $r;
                                } else {
                                    //echo "This is not the one\n";
                      //  print_r($r);
                                }
                            }
                        }
                        if ($ok == "1") {
                            $service = $this->Admin_model->getServiceCli($serviceid);
                            $params['userid'] = $service->userid;

                            $params['result'] = 'success';
                            $this->db = $this->load->database('import', true);
                            $res = $this->Admin_model->united_api(array('action' => 'GetClientsProducts', 'serviceid' => $r->id));
                            // $params['res']= $res;
                            if ($res->result == "success") {
                                $product = $res->products->product[0];
                                $params['serviceid'] = $r->id;
                                foreach ($product->customfields->customfield as $row) {
                                    $params[str_replace('.', '', $row->name)] = $row->value;
                                }
                            } else {
                                $params['result'] = 'error';
                            }
                            return (object) $params;
                        }
                    }
                }
            }
        }
    }

    public function processImport($s)
    {
        $this->load->library('encrypt');
        $this->load->model('Admin_model');
        $username = $s['username'];
        $password = $s['password'];
        $this->Admin_model->update_services($s['serviceid'], array(
           'username' => $username,
           'password' => $this->encryption->encrypt($password)));
        /*
        alpha: "A"
circuitid: ""
city: "Kelmis"
number: "13"
password: "e9syah46"
postcode: "4721 "
realm: "@unitedadsl"
serviceid: "1809785"
street: "Kehrweg"
username: "ut6354203"
whmcsid: "73063"

*/
        $this->db = $this->load->database('default', true);

        $this->db->insert(
            'a_services_xdsl',
            array('requestid' => null,
            'companyid' => 2,
            'userid' => $s['userid'],
            'serviceid' => $s['serviceid'],
            'circuitid' =>$s['circuitid'],
            'status' => 'Active',
            'street' => $s['street'],
            'number' => $s['number'],
            'alpha' => $s['alpha'],
            'bus' => null,
            'floor' => null,
            'block' => null,
            'city' => $s['city'],
            'postcode' => $s['postcode'],
            'country' => 'BEL',
            'phonenumber' => null,
            'dialnumber' => null,
            'proximus_status' => 'Active',
            'profile' => null,
            'lex' => null,
            'proformaid' => null,
            'created' => null,
            'last_updated' => date('Y-m-d H:i:s'),
            'proximus_orderid' => null,
            'product_name' => null,
            'product_id' =>  null,
            'address_verified' => 1,
            'feedback_description' => null,
            'address_lang' => null,
            'MDU_SDU ' => null,
            'appointment_date_requested' => null,
            'appointment_date_confirmed' => null,
            'remarks' => 'Imported from Whmcs: Serviceid: '.$s['whmcsid'],
            'workorderid' => null,
            'InvoiceGroupMaster' => null,
            'ArticleIdVOIP' => null,
            'realm' =>  $s['realm'])
        );

        if ($this->db->insert_id()>0) {
            // $this->session->set_flashdata('success','Order has been imported');
            echo json_encode(array('result' => 'success'));
        } else {
            $this->session->set_flashdata('success', 'Order fail to be imported');
            echo json_encode(array('result' => 'error'));
        }
    }

    public function getsump($companyid, $plan = false)
    {
        $this->load->library('artilium', array('companyid' => $companyid));
        print_r($this->artilium->getsum());
        if ($plan) {
            print_r($this->artilium->getsumplanid($plan));
        }
    }


    public function add_reminder_invoice_charge($companyid, $date = false)
    {
        $this->load->model('Admin_model');
        $this->load->library('magebo', array('companyid' => $companyid));
        $this->load->library('artilium', array('companyid' => $companyid));


        if ($date) {
            $q = $this->db->query("select a.*,b.mvno_id,b.mageboid from a_reminder a left join a_clients b on b.id=a.userid where a.companyid=? and a.type='Reminder3' and a.date_sent like ?", array($companyid, $date.'%'));
        } else {
            $q = $this->db->query("select a.*,b.mvno_id,b.mageboid from a_reminder a left join a_clients b on b.id=a.userid where a.companyid=? and a.type='Reminder3'", array($companyid));
        }
        echo "Invoicenumber;Customerid;AmountInvoice;DateReminder\n";
        foreach ($q->result() as $s) {
            if ($this->magebo->isInvoicePaid($s->invoicenum)) {
                echo $s->invoicenum.";".$s->mvno_id.";".$s->amount.";".$s->date_sent."; ".$this->getSubscription($s->userid)."\n";
            }
        }
    }

    public function getSubscription($userid)
    {
        $number = array();
        $q =  $this->db->query("select a.status, b.msisdn from a_services a left join a_services_mobile b on b.serviceid=a.id where a.userid=? and a.status != 'Cancelled'", array($userid));

        if ($q->num_rows()>0) {
            foreach ($q->result() as $o) {
                $number[] = $o->msisdn.' '.$o->status;
            }
        }

        if ($number) {
            return implode(';', $number);
        }
    }
    public function add_extra_nextinvoice_items($msisdn, $amount, $client)
    {
        $this->load->model('Admin_model');

        $this->load->library('magebo', array('companyid' => $client->companyid));

        $terms = 1;


        $option = (object) array('terms' => $terms, 'name' => 'Reactivation Fee '.$msisdn, 'recurring_total' => $amount);
        $contract = date('m-01-Y', strtotime('+1 month'));
        $id = $this->magebo->addExtraPricing($client, $option, $contract);
        if ($id > 0) {
            //  $this->session->set_flashdata('success', 'Invoice Item has been planned');
        } else {
            //   $this->session->set_flashdata('error', 'This action was blocked');
        }
        //redirect('admin/client/detail/' . $_POST['userid']);
    }


    public function getInvoiceReminder3()
    {
        $this->load->model('Admin_model');
        foreach ($this->Admin_model->getInvoicesList(54) as $invoice) {
            $client = $this->Admin_model->getClientByMageboid($invoice->iAddressNbr);
            $payments = $this->Admin_model->getInvoicesPayments($invoice->iInvoiceNbr, str_replace(' 00:00:00.000', '', $invoice->dInvoiceDueDate));
            echo $invoice->iInvoiceNbr.';'.$invoice->iAddressNbr.';'.str_replace(' 00:00:00.000', '', $invoice->dInvoiceDate).';'.str_replace(' 00:00:00.000', '', $invoice->dInvoiceDueDate).";". $payments.";".$this->getSubscription($client->id)."\n";
        }
    }

    public function GetFinancialCondition($id)
    {
        $this->load->library('magebo', array('companyid' => 54));

        print_R($this->magebo->GetFinancialCondition($id));
    }




    public function process_sepa_to_paid($companyid, $bankid, $fileid)
    {
        if (empty($companyid)) {
            die('companyid, bankid, fileid');
        } elseif (empty($bankid)) {
            die('companyid, bankid, fileid');
        } elseif (empty($fileid)) {
            die('companyid, bankid, fileid');
        }


        $text = "";
        $row = (object) array('BatchPaymentId' => $bankid,  'date_transaction' => date('Y-m-d'), 'fileid' => $fileid, 'companyid' => $companyid);

        $this->load->library('magebo', array('companyid' => $row->companyid));

        if (is_numeric($row->BatchPaymentId)) {
            $invoices = $this->magebo->getBankDetails($row->BatchPaymentId);

            if ($invoices) {
                $text .= date('Y-m-d H:i:s') . "- Processing Directdebit payments " . $row->BatchPaymentId . "\n";
                foreach ($invoices as $invoice) {
                    $text .= date('Y-m-d H:i:s') . "- " . $invoice->iInvoiceNbr . "\n";
                    if (trim($invoice->cTypeDescription) == "SEPA FIRST") {
                        $paymentform = 'SEPA FIRST';
                    } else {
                        $paymentform = 'SEPA REOCCURRING';
                    }

                    $res = $this->magebo->apply_payment(array('cPaymentForm' => $paymentform,
                        'mPaymentAmount' => $invoice->mBankFileAmount,
                        'cPaymentFormDescription' => "Sepa DD Payment " . $invoice->iInvoiceNbr,
                        'dPaymentDate' => date("m/d/Y", strtotime($row->date_transaction)),
                        'iInvoiceNbr' => $invoice->iInvoiceNbr,
                        'step' => 1,
                        'iAddressNbr' => $invoice->iAddressNbr,
                        'cPaymentRemark' => $row->fileid));

                    lof_message("error", "Processing :" . $paymentform . " " . $invoice->mBankFileAmount . " " . $invoice->iInvoiceNbr . " " . $invoice->iAddressNbr);

                    $client = getClientDetailidbyMagebo($invoice->iAddressNbr);
                    $this->db->insert('a_payments', array(
                        'date' => date('Y-m-d'),
                        'invoiceid' => trim($invoice->iInvoiceNbr),
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'paymentmethod' => 'DD',
                        'transid' => 'BatchId: ' . $row->BatchPaymentId,
                        'amount' => $invoice->mBankFileAmount,
                        'adminid' => $this->session->id,
                        'raw' => 'Executed by: ' . $this->session->firstname . ' ' . $this->session->lastname));
                    unset($res);
                }
            }
        }
    }
    public function set_reject_complete()
    {
        $id = $this->uri->segment(4);
        $this->db->where('id', $id);
        $this->db->update('a_sepa_items', array('extra_status' => 1));
        $this->session->set_flashdata('success', 'successfully set to completed');
        redirect('admin/sepa');
    }
    public function send_sepa_notification_manual($companyid)
    {
        $file = "/home/mvno/documents/54/directdebit/201905258672.XML";

        echo $file." ".$companyid."\n";
        $this->data['setting'] =  globofix($companyid);

        if (!empty($this->data['setting']->sepa_email_notification)) {
            $recipients = explode('|', $this->data['setting']->sepa_email_notification);
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;

            $this->email->clear(true);
            $this->email->initialize($config);
            $this->data['language'] = "dutch";
            $body = "Hello,<br /> Your sepa file has been uploaded on your FTP server if you opted for it otherwise here is your File<br /><br />Regards, <br>United Telecom";
            $this->email->set_newline("\r\n");
            $this->email->from('noreply@united-telecom.be', 'United Team');
            $this->email->to($recipients);
            // $this->email->cc('mail@simson.one');
            $this->email->bcc(array('it@united-telecom.be','mail@simson.one'));
            $this->email->subject(lang("Sepa File Directdebit"));
            $this->email->attach($file);
            $this->email->message($body);
            if ($this->email->send()) {
                return true;
            } else {
                return false;
            }
        }
    }


    public function update_reseller_earnings($companyid, $date = false)
    {
        if (!$date) {
            $date =  date('Y-m-d');
        }


        $res = array();
        $reseller = $this->db->query("select id,comission_value,comission_type from a_clients_agents where companyid=?", array($companyid));
        echo $this->db->last_query();
        $this->load->library('magebo', array('companyid' => $companyid));
        $this->db = $this->load->database('default', true);

        $invoices = $this->magebo->getLastMonthInvoicesbyDate($date);
        print_r($invoices);
        foreach ($invoices as $invoice) {
            if (getResellerid($invoice['iAddressNbr'])) {
                $res[getResellerid($invoice['iAddressNbr'])][] = $invoice['mInvoiceAmount'];
            }
        }
        print_r($res);
        foreach ($reseller->result() as $row) {
            if ($row->comission_type == "Percentage") {
                $this->db->query("update a_clients_agents set last_month_earning=? where id=?", array(array_sum($res[$row->id])*($row->comission_value/100), $row->id));
            } else {
                $this->db->query("update a_clients_agents set last_month_earning=? where id=?", array(array_sum($res[$row->id])*0.10, $row->id));
            }
        }
        mail('lex.v.d.hondel@trendcall.com', 'Reseller Commission Updated', "Hi,\nThis email is to inform you that your resellers earning has been updated for invoices of last month\n\nIf you have issue please create ticket, our team will check it for you\n\nNOTE: THIS FEATURE IS NOT IN OUR CORE SERVICE AND WE DO NOT MONITOR THIS PROCESS\n\nRegards.\n\nEU Portal");
    }

    public function updatetic()
    {
        $q = $this->db->query("SELECT * FROM `a_services_xdsl_callback` WHERE `rawdata` LIKE '%OLO-Tic%'");
        foreach ($q->result() as $row) {
            $array = json_decode($row->rawdata);
            if (!empty($array->referenceData->networkTerminationPoint->networkPhysicalResource->charVal->characteristicName)) {
                echo $array->referenceData->networkTerminationPoint->networkPhysicalResource->charVal->characteristicName."\n";
                if ($array->referenceData->networkTerminationPoint->networkPhysicalResource->charVal->characteristicName == "introductionPairUsed") {
                    $this->db->query("update a_services_xdsl set introductionPairUsed = ? where proximus_orderid = ?", array($array->referenceData->networkTerminationPoint->networkPhysicalResource->charVal->value, $array->customerOrder->customerOrderIdentifier->id));
                }
            }

            //print_r(json_decode($row->rawdata));
        }
    }

    public function updatedone()
    {
        $q = $this->db->query("SELECT a.*,b.serviceid FROM a_services_xdsl_callback a left join a_services_xdsl b on b.proximus_orderid=a.orderid WHERE a.rawdata LIKE '%The customer order is technically executed%'");
        foreach ($q->result() as $row) {
            $array = json_decode($row->rawdata);
            if ($row->serviceid) {
                $this->db->query("update a_services set status='Active' where id=?", array($row->serviceid));
                $this->db->query("update a_services_xdsl set status = ? where proximus_orderid = ?", array('Active', $array->customerOrder->customerOrderIdentifier->id));
            }
            // $this->db->query("update a_services set status='Active' where id=?", array())


            //print_r(json_decode($row->rawdata));
        }
    }

    public function import_invoices()
    {
        $limit = 100;
        $this->load->model('Radius_model');
        foreach (range(1, 200) as $num) {
            $next =  $this->getNextNumber();
            echo $next."\n";
            $res = $this->Radius_model->getInvoices($next);

            $this->insertBatch($res);
        }
    }

    public function getNextNumber()
    {
        $this->db = $this->load->database('default', true);

        $q = $this->db->query("select * from a_tblInvoice order by id desc limit 1");
        if ($q->num_rows()>0) {
            return $q->row()->iInvoiceNbr;
        } else {
            return 0;
        }
    }

    public function insertBatch($array)
    {
        $this->db = $this->load->database('default', true);

        $this->db->insert_batch('a_tblInvoice', $array);
    }

    public function removeSubscriptionActiveFromMagebo()
    {
        $this->load->model('Admin_model');
        $this->load->library('artilium', array(
            'companyid' => 53,
         ));
        $this->load->library('magebo', array(
            'companyid' => 53,
        ));
        $this->load->model('Admin_model');
        $filename = '/home/mvno_dev/public_html/remove.txt';
        $contents = file($filename);

        foreach ($contents as $line) {
            echo $line."\n";
            $mobile = $this->getServicebyMsisdn(trim($line));
            //print_r($mobile);
            if ($mobile) {
                if ($mobile->msisdn_status != "PortinAccepted") {
                    //$this->update_contract_date($mobile->id, 53, $this->convert_date_ported_todate_contract($mobile->date_ported));
                    $ko = (array)$mobile;
                    //$ko['date_ported_format'] = $this->convert_date_ported_todate_contract($mobile->date_ported);
                    $ko['formated'] = $this->magebo->DeleteMageboSubs($mobile->msisdn_sn);
                    $this->Admin_model->update_services($mobile->id, array('status' => 'Pending', 'iGeneralPricingIndex' => null, 'username' => 'OLD INDEX:'.$mobile->iGeneralPricingIndex));
                    print_r($ko);
                }
                // $serv = $this->Admin_model->getSN(trim($mobile->msisdn_sim));
            } else {
                die("error");
            }
        }
    }


    public function convert_date_ported_todate_contract($date)
    {
        $r =explode('-', str_replace(' 00:00:00', '', $date));

        return $r[1].'-'.$r[2].'-'.$r[0];
    }
    public function update_contract_date($serviceid, $companyid, $date_contract)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($serviceid);
        $date   = $date_contract;
        $this->db->where('id', $serviceid);
        $this->db->update('a_services', array(
            'date_contract' => $date
        ));
        $this->magebo->change_contract_date($mobile->details->msisdn, $date);
        $this->magebo->change_contract_date($mobile->details->msisdn_sn, $date);
        if (!empty($mobile->iGeneralPricingIndex)) {
            $dt = explode('-', $date);
            $this->magebo->updatePricingMonthbypin($mobile->details->msisdn, $dt[2], $dt[0], $dt[1]);
            $this->magebo->updatePricingMonthbypin($mobile->details->msisdn_sn, $dt[2], $dt[0], $dt[1]);
        }
        logAdmin(array(
            'companyid' => $companyid,
            'user' => 'System',
            'serviceid' => $serviceid,
            'userid' => $mobile->userid,
            'ip' => '127.0.0.1',
            'description' => 'Portin Accepted and system changes Contract Date from  ' . $mobile->date_contract . ' to ' . $date_contract
        ));
    }

    public function getServicebyMsisdn($msisdn)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select  a.id,a.iGeneralPricingIndex,a.date_contract,b.msisdn_sim,b.msisdn_sn,date_ported,msisdn_status from a_services_mobile b left join a_services a on a.id=b.serviceid where b.msisdn = ?", array($msisdn));

        return $q->row();
    }


    public function fixContractdate_caiway()
    {
        $counter = 0;
        $this->load->library('artilium', array(
            'companyid' => 56,
        ));
        $list = $this->artilium->GetPendingPortIns();
        foreach ($list as $key => $mobile) {
            if ($mobile['Status'] == "Port in Accepted") {
                //echo $counter++;
                $data = explode(' ', $mobile['ActionDate']);
                $cd = explode('/', $data[0]);

                $service  = $this->getServicebyMsisdn($mobile['MSISDN']);
                //print_r($service);
                echo $service->id." ".$service->date_contract."    ".$cd[1].'-'.$cd[2].'-'.$cd[0]."\n ";
                //$this->db->query("update a_services set date_contract=? where id=?", array($cd[1].'-'.$cd[2].'-'.$cd[0], $service->id));
            }
        }

        echo $counter;
    }


    public function getServiceByContractDate($companyid, $date)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select * from a_services where status='Pending' and date_contract like ? and companyid=?", array($date, $companyid));

        if ($q->num_rows()>0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function caiway_activate_billing_duesubs($companyid)
    {
        $period = new DatePeriod(
            new DateTime('2019-01-01'),
            new DateInterval('P1D'),
            new DateTime('2019-06-25')
        );

        foreach ($period as $key => $value) {
            $services = $this->getServiceByContractDate($companyid, $value->format('m-d-Y'));

            if ($services) {
                print_r($services);
            }
            echo $value->format('m-d-Y')."\n";
        }
        /*
          $counter = 0;
          $this->load->library('artilium', array(
             'companyid' => 56,
          ));
          */
        //$list = $this->artilium->GetPendingPortIns();

        /*foreach($list as $key => $mobile){
           if($mobile['Status'] == "Port in Accepted"){
           //echo $counter++;
           $data = explode(' ', $mobile['ActionDate']);
           $cd = explode('/', $data[0]);

           $service  = $this->getServicebyMsisdn($mobile['MSISDN']);
           //print_r($service);
            echo $service->id." ".$service->date_contract."    ".$cd[1].'-'.$cd[2].'-'.$cd[0]."\n ";
            //$this->db->query("update a_services set date_contract=? where id=?", array($cd[1].'-'.$cd[2].'-'.$cd[0], $service->id));
           }

        }

        echo $counter;
       */
    }
    public function testloop()
    {
        $this->load->library('artilium', array('companyid' => '54'));
        $y = $this->artilium->GetPendingPortIns();

        print_r($y);
    }
    public function testtemplate($companyid)
    {
        if (!isTemplateActive($companyid, 'simcard_blocking')) {
            log_message('error', 'Template simcard_blocking is disabled sending aborted');
            echo json_encode(array(
                'result' => true,
                'message' => 'Tempate simcard_blocking is disabled'
            ));
        }

        print_r(array('result' => 'success'));
    }

    public function get_pending()
    {
        $rep = "CLI;Clientid;EMAIL\n";

        $sts = "All";

        $tr = "";
        $this->load->library('artilium', array('companyid' => 54));
        //$this->load->library('artilium', array(COMPANYID, PLATFORM));
        $dd = array();
        $list = $this->artilium->GetPendingPortIns();
        $s = null;
        if (!empty($list)) {
            //mail('mail@simson.one', 'status',print_r($list, true));
            foreach ($list as $key => $row) {
                if (!in_array(trim($row['Status']), array("CANCELED", "Port in Canceled"))) {
                    //print_r($row);
                    $s['Id'] = $key;
                    $s['OperationType'] = $row['OperationType'];
                    $s['RequestType'] = $row['RequestType'];
                    $s['DonorOperator'] = $row['DonorOperator'];
                    $s['MSISDN'] = $row['MSISDN'];
                    $s['SN'] = $row['SN'];
                    if (!empty($row['Remark'])) {
                        $s['Remark'] = $row['Remark'];
                    } else {
                        $s['Remark'] = '';
                    }

                    $e = explode(' ', $row['EnteredDate']);
                    $s['EnteredDate'] = str_replace('/', '-', $e['0']);
                    $s['ActionDate'] = str_replace(' 00:00:00', '', str_replace('/', '-', $row['ActionDate']));
                    $s['CustomerName'] = getCustomernamebySerial(trim($row['SN']));
                    /*
                    // Send List Pending Verification
                    if(trim($row["Status"]) == "Port in Wait for verificationcode"){

                    $rep .= $row['MSISDN'].";".getCustomerEmailBySerial($row['MSISDN'], $row['SN']);
                    }

                     */

                    if ($row['Country'] == "NL") {
                        if ($row['Error.ID'] === "0") {
                            $s['Remark'] = $row['ErrorComment'];
                        } elseif ($row['Error.ID'] == 27) {
                            $s['Remark'] = lang("Telephone number and customer ID do not correspond");
                        } elseif ($row['Error.ID'] == 63) {
                            $s['Remark'] = lang("beyond notice terms / Porting not possible within porting window");
                        } elseif ($row['Error.ID'] == 99) {
                            $s['Remark'] = lang("Other");
                        } elseif ($row['Error.ID'] == 41) {
                            $s['Remark'] = lang("Multiple number types are not allowed.");
                        } elseif ($row['Error.ID'] == 44) {
                            $s['Remark'] = lang("Multiple DNOs or DSPs not allowed.");
                        } elseif ($row['Error.ID'] == 45) {
                            $s['Remark'] = lang("Telephone number is not portable");
                        } elseif ($row['Error.ID'] == "02") {
                            $s['Remark'] = lang("Porting request for this/these number(s) is already in progress");
                        } elseif ($row['Error.ID'] == "01") {
                            $s['Remark'] = lang("Other");
                        } else {
                            $s['Remark'] = "";
                        }
                    }

                    if ($row['Status'] == "Port in Rejected") {
                        $s['Status'] = '<a href="javascript:void(0)" class="data-toggle="tooltip" data-placement="top" title="">' . $row['Status'] . '</a>';
                    } //$row['Status'] == "Port in Rejected
                    elseif ($row['Status'] == "Port in Cancelled") {
                        $s['Status'] = "batal";
                    } //$row['Status'] == "Port in Rejected"
                    else {
                        $s['Status'] = $row['Status'];
                    }
                    if ($row['OperationType'] == "PORT_OUT") {
                        if ($row['Status'] != 'ACCEPTED') {
                            $s['button'] = ' <button type="button"class="btn btn-success btn-sm"  onclick="OpenAccept(\'' . $row['MSISDN'] . '\', \'' . $row['SN'] . '\');">ACCEPT</button>
                    <button type="button"class="btn btn-danger btn-sm"  onclick="OpenReject(\'' . $row['MSISDN'] . '\');">REJECT</button>';
                        } //$row['Status'] != 'ACCEPTED'
                    } //$row['OperationType'] == "PORT_OUT"
                    else {
                        if ($row['Status'] == 'Port in Rejected') {
                            $s['button'] = ' <button type="button" class="btn btn-primary btn-sm" onclick="OpenExecute(\'' . $row['MSISDN'] . '\');">Rexecute</button>';
                        } //$row['Status'] == 'Port in Rejected'
                        elseif ($row['Status'] == 'Port in Wait for verificationcode') {
                            $s['button'] = ' <button type="button" class="btn btn-primary btn-sm" onclick="OpenPod(\'' . $row['MSISDN'] . '\');">Porting on Demand</button>';
                        } elseif ($row['Status'] == 'Port in Failed') {
                            $s['button'] = '  <button type="button" class="btn btn-primary btn-sm" onclick="OpenExecute(\'' . $row['MSISDN'] . '\');">EXECUTE</button>';
                        } //$row['Status'] == 'Port in Accepted'
                        else {
                            $s['button'] = '';
                        }
                        $s['button'] .= '<button type="button" class="btn btn-primary btn-sm" onclick="OpenCancel(\'' . $row['SN'] . '\');">Cancel</button>';
                    }

                    //unset($row);
                } //$row['Status'] != "CANCELED"
            } //$list as $key => $row
            if ($s != null) {
                //echo $sts;
                if ($sts == "Rejected") {
                    if (trim($row["Status"]) == "Port in Rejected") {
                        $dd[] = $s;
                    }
                } elseif ($sts == "Pending") {
                    if (in_array($row["Status"], array("Port in Wait for verificationcode", "Port in Not yet complete", "Port in Pending"))) {
                        $dd[] = $s;
                    }
                } elseif ($sts == "Accepted") {
                    if (trim($row["Status"]) == "Port in Accepted") {
                        $dd[] = $s;
                    }
                } elseif ($sts == "Fails") {
                    if (trim($row["Status"]) == "Port in Failed") {
                        $dd[] = $s;
                    }
                } else {
                    $dd[] = $s;
                }
            }
        } //!empty($list)

        // mail('mail@simson.one','List Pending Verification', $rep);
        echo json_encode($dd);
    }
    public function test_reminder($companyid)
    {
        $date = "2019-06-24";
        $this->load->library('magebo', array('companyid' => $companyid));
        $reminder= $this->magebo->getUnpaidInvoicesByRejection($date);
        print_r($reminder);
    }

    public function teum_update_customer()
    {
        $this->load->library('pareteum', array('companyid' => 35));
        $json = '{"CustomerId":1065076582,"CustomerData":{"ExternalCustomerId":"1556511983","FirstName":"Simson","LastName":"Lais","LastName2":"NA","CustomerDocumentType":"Passport","DocumentNumber":"13245356","Telephone":"820018564097535","Email":"vq3elgkgr@ed8vobo6hn.com","LanguageName":"eng","FiscalAddress":{"Address":"Wingeparks","City":"Rotselaar","CountryId":"76","HouseNo":"5","State":"Unknown","ZipCode":"3110"},"CustomerAddress":{"Address":"Wingeparks","City":"Rotselaar","CountryId":"76","HouseNo":"5","State":"Unknown","ZipCode":"3110"},"Nationality":"GB"},"comments":"Customer added via UnitedPortal V1 bySimson Lai","channel":"UnitedPortal"}';
        $sim = $this->pareteum->UpdateCustomer(json_decode($json, true));

        print_r($sim);
    }

    public function teum_gitaccount()
    {
        $this->load->library('pareteum', array('companyid' => 35));
        $sim = $this->pareteum->accountInformation(array('msisdn' => '447421966433'));

        print_r($sim);
    }

    public function teum_activate($companyid)
    {
        $this->load->model('Admin_model');
        $simcard = "8944125646610154859";
        $sim = simcardExist(trim($simcard));
        $this->load->library('pareteum', array('companyid' => $companyid));
        $client = $this->Admin_model->getClient('946413');

        $account = array(
                    "AccountInfo" => array(
                     "AccountType" => "Prepaid",
                     "CustomerId" => $client->teum_CustomerId,
                     "ExternalAccountId" => '1813675',
                     "AccountStatus" => "Active",
                     "Names" => array(array( "LanguageCode"=> "eng", "Text"=> "Account" )),
                     "Descriptions"=> array(array(  "LanguageCode" => "eng", "Text" => "Account")),
                     "AccountCurrency"=> "GBP",
                     "Balance"=> 0,
                     "CreditLimit"=> 0));

        // $acct =  $this->pareteum->CreateAccount($account);
        // print_r($acct);


        // log_message('error',print_($acct, true));

        // if($acct->resultCode == "0"){
        $addons = getaddons(1813675, $sim->MSISDN);
        $subs = array(
                    "CustomerId" => (int) $client->teum_CustomerId,
                    "Items" => array(
                        array(
                            "AccountId" => "1050000000000000731",
                            "ProductOfferings" => $addons,
                            "ServiceAddress" => array(
                                "Address" => $client->address1,
                                "HouseNo" =>$client->housenumber,
                                "City" => $client->city,
                                "ZipCode" => $client->postcode,
                                "State" => "unknown",
                                "CountryId" => "76"
                            )
                        )
                    ),
                    "channel" => "UnitedPortal V1"
                    );
        log_message('error', print_r($subs, true));
        $subscription =  $this->pareteum->AddSubscription($subs);
        print_r($subscription);
        exit;

        //  }

        redirect('admin/subscription/detail/'.$_POST["serviceid"]);
        exit;
    }

    public function delta_ciot()
    {
        log_message('error', 'Running CIOT Export Delta & Caiway');
        //$this->update_imsi();
        $last_date= file_get_contents(APPPATH.'logs/app/last_changes.log');
        $date = date('Y-m-d H:i:s');
        $this->fix_contract_date();
        $this->load->dbutil();
        $time =date('Y-m-d');
        $this->db->cache_off();
        $this->load->model('Admin_model');
        //;Dhr.;Jeroen;;Stooker;800004;SOFTLAUNCHM-STOOKER;;;BILLING;Elstar 22;;;5056 DG;;;;;;;;;;;;jeroen.stooker@gmail.com;31619675822;;NL30INGB0007624165;DELTA;2018-11-09 14:46:58
        $q = $this->db->query("select initial,salutation,firstname,'' as middlename,lastname,id,mvno_id as deltaid,'' as caiwayid,address1,housenumber,alphabet,postcode,city,country,address1 as delta_address1,housenumber as delta_housenumber,alphabet as delta_alphabet,postcode as delta_postcode,city as delta_city,country as delta_country,'' as caiway_address1,'' as caiway_housenumber,'' as caiway_alphabet,'' as caiway_postcode,'' as caiway_city,'' as caiway_country,email,phonenumber,'' as phonenumber2,iban,'Delta' as brand,date_modified from a_clients where companyid=? and date_modified >= ? order by date_modified asc", array('53', trim($last_date)));
        $s = $this->db->query("select initial,salutation,firstname,'' as middlename,lastname,id,'' as deltaid,mvno_id as caiwayid,address1,housenumber,alphabet,postcode,city,country,'' as delta_address1,'' as delta_housenumber,'' as delta_alphabet,'' as delta_postcode,'' as delta_city,'' as delta_country,address1 as caiway_address1,housenumber as caiway_housenumber,alphabet as caiway_alphabet,postcode as caiway_postcode,city as caiway_city,country as caiway_country,email,phonenumber,'' as phonenumber2,iban,'Caiway' as brand,date_modified from a_clients   where companyid=?  and date_modified >= ? order by date_modified asc", array('56', trim($last_date)));
        log_message('error', 'Exporting Clients '.$q->num_rows().' for Delta');
        log_message('error', 'Exporting Clients '.$s->num_rows().' for Caiway');
        $delta_p =  $this->db->query("select id as ProductId, 'DELTA' as Brand,name as BundleName,round(data) as Data,round(voice) as Voice,round(sms) as Sms, case status when 1 then 'Active' else 'Inactive' end as Status,date_modified as MutationDate from a_products where companyid=53 and date_modified >=? ", array($last_date));
        $caiway_p =  $this->db->query("select id as ProductId, 'CAIWAY' as Brand,name as BundleName,round(data) as Data,round(voice) as Voice,round(sms) as Sms, case status when 1 then 'Active' else 'Inactive' end as Status,date_modified as MutationDate from a_products where companyid=56 and date_modified >=?", array($last_date));
        $c_service = $this->db->query("select a.userid as CustomerNumber,a.msisdn_imsi as IMSI, b.packageid as Productid, b.date_contract_formated as Start_date, date_terminate as End_Of_Contract,b.date_contract_prolong as Date_Contract_Prolong,b.recurring as MRR, c.promo_value as MRR_Discount, promo_duration as MRR_Discount_Duration,a.date_modified as Date_Mutation,d.companyname as Brand
        from a_services_mobile a
        left join a_services b on b.id=a.serviceid
        left join a_promotion c on c.promo_code=b.promocode
        left join a_mvno d on d.companyid=a.companyid
        where a.companyid in (53,56) and b.status in ('Active','Terminated') and a.date_modified >= ? ORDER BY `Brand` ASC", array(trim($last_date)));
        log_message('error', 'Exporting Subscriptions for delta and caiway records: '.$c_service->num_rows());
        $csv = $this->dbutil->csv_from_result($q);
        echo "Writting caiway..\n";

        $csv .= preg_replace('/^.+\n/', '', $this->dbutil->csv_from_result($s));

        $product = $this->dbutil->csv_from_result($delta_p);
        echo "Writting caiway..\n";

        $product .= preg_replace('/^.+\n/', '', $this->dbutil->csv_from_result($caiway_p));
        $service = $this->dbutil->csv_from_result($c_service);
        file_put_contents(FCPATH.'assets/ciot/'.$time.'_export_clients.csv', $csv);
        $this->moveCaiwayFile(FCPATH.'assets/ciot/'.$time.'_export_clients.csv', $time.'_export_clients.csv');
        file_put_contents(FCPATH.'assets/ciot/'.$time.'_export_products.csv', $product);
        $this->moveCaiwayFile(FCPATH.'assets/ciot/'.$time.'_export_products.csv', $time.'_export_products.csv');
        file_put_contents(FCPATH.'assets/ciot/'.$time.'_export_services.csv', $service);
        $this->moveCaiwayFile(FCPATH.'assets/ciot/'.$time.'_export_services.csv', $time.'_export_services.csv');
        file_put_contents(APPPATH.'logs/app/last_changes.log', $date);
    }

    public function fix_contract_date()
    {
        $last_date= file_get_contents(APPPATH.'logs/app/last_changes.log');

        $q = $this->db->query("select b.* from a_services_mobile a left join a_services b on b.id=a.serviceid where a.companyid in (53,54,55,56,57) and a.date_modified > ?", array(trim($last_date)));

        foreach ($q->result() as $row) {
            $data = array(convert_contract($row->date_contract),  calculate_contractend_date_formated($row->date_contract, $row->contract_terms), $row->id);
            $this->db->query("update a_services set date_contract_formated =?, date_contract_prolong= ? where id=?", $data);
            print_r($data);
        }
    }

    public function moveCaiwayFile($File, $filename)
    {
        echo "Uploading {$filename}\n";
        $connection = ssh2_connect('ftp-sftp1.caiw.net', 2222);
        ssh2_auth_password($connection, 'delta_cdr_prod_artilium', 'CwDg9z');
        $sftp = ssh2_sftp($connection);

        $resFile = fopen("ssh2.sftp://{$sftp}/CRM_EXPORT/".$filename, 'w');
        $srcFile = fopen($File, 'r');
        echo stream_copy_to_stream($srcFile, $resFile);
        fclose($resFile);
        fclose($srcFile);
    }
    public function update_imsi()
    {
        foreach (array('56','53','54','55','33','57','58','23') as $companyid) {
            echo "starting with {$companyid}.\n";
            $q = $this->db->query("select id,msisdn_sn,msisdn_sim from a_services_mobile where companyid =? and msisdn_imsi is null and msisdn_sim != '1111111111111111111'", $companyid);
            $this->load->library('artilium', array('companyid' => $companyid));
            foreach ($q->result() as $row) {
                print_r($row);
                $sim = $this->artilium->getSim($row->msisdn_sim);
                if ($sim) {
                    print_r($sim);
                    $this->db->query("update a_services_mobile set msisdn_imsi=? where id=?", array($sim, $row->id));
                }
            }

            unset($this->artilium);
        }
    }

    public function getsim($companyid, $sn)
    {
        $this->load->library('artilium', array('companyid' => $companyid));

        $list = $this->artilium->GetParametersCLI($sn);
        print_r($list);
    }

    public function update_address_ciot()
    {
        $q = $this->db->query("select id,address1,postcode,city,country,housenumber,alphabet from a_clients_ciot where companyid=54");
        foreach ($q->result() as $row) {
            $this->db->query("update a_clients set address1=?, postcode=?, housenumber=?, alphabet=?, city=?,country=? where id=?", array(
                $row->address1, $row->postcode, $row->housenumber,$row->alphabet,$row->city, $row->country, $row->id));
        }
    }



    public function delete111()
    {
        $q = $this->db->query("select id,serviceid from a_services_mobile where msisdn_status = 'Cancelled' and msisdn_sim is null");
        foreach ($q->result() as $row) {
            print_r($row);
            $this->db->query("delete from a_services where id=?", array($row->serviceid));
            $this->db->query("delete from a_services_mobile where id=?", array($row->id));
        }
    }


    public function street_fix($companyid)
    {
        $q = $this->db->query("select id,address1,housenumber,alphabet,country from a_clients where companyid=? and housenumber =''", $companyid);

        foreach ($q->result() as $row) {
            if (empty($row->housenumber)) {
                if ($row->country == "NL") {
                    $address = array_values(array_filter(explode(' ', trim($row->address1))));

                    if (count($address) == 2) {
                        print_r($address);
                        echo $address[0]." ". end($address)."\n";
                        $this->db->query("update a_clients set address1=?, housenumber=? where id=?", array(trim($address[0]),end($address),$row->id ));
                    } elseif (count($address) == 3) {
                        if (is_numeric($address[2]) && is_numeric($address[1])) {
                            print_r($address);
                            $this->db->query("update a_clients set address1=?, housenumber=?, alphabet=? where id=?", array(trim($address[0]),trim($address[1]),strtoupper(trim($address[2])),$row->id ));
                        } elseif (strlen($address[2]) == "1" && is_numeric($address[1])) {
                            print_r($address);
                            $this->db->query("update a_clients set address1=?, housenumber=?, alphabet=? where id=?", array(trim($address[0]),trim($address[1]),strtoupper(trim($address[2])),$row->id ));
                        } elseif (is_numeric($address[2]) && strlen($address[1]) > 1) {
                            print_r($address);
                            $this->db->query("update a_clients set address1=?, housenumber=? where id=?", array(trim($address[0])." ".trim($address[1]), trim($address[2]), $row->id ));
                        } else {
                            $this->db->query("update a_clients set address1=?, housenumber=? where id=?", array(trim($address[0])." ".trim($address[1]),end($address),$row->id ));
                        }
                        //print_r($address);
                    //

                  // print_r($address);
                 //echo $address[0]." ". end($address)."\n";
               // $this->db->query("update a_clients set address1=?, housenumber=? where id=?", array(trim($address[0]),end($address),$row->id ));
                    } elseif (count($address) == 4) {
                        if (end($address) > 1) {
                            $this->db->query("update a_clients set address1=?, housenumber=? where id=?", array(trim($address[0]) ." ".trim($address[1])." ".trim($address[2]),end($address),$row->id ));
                            print_r($address);
                        }
                    }
                } elseif (in_array($row->country, array("US","CA","FR"))) {
                } else {
                }
            }

            //print_r($row);
        }
        echo $q->num_rows();
    }
    public function ttt()
    {
        $_POST['serviceid'] = 1813681;
        $this->load->model('Admin_model');
        $service_teum = $this->Admin_model->getServiceCLI($_POST['serviceid']);

        print_r($service_teum);
        exit;
        $addons = getaddons($_POST['serviceid'], $service_teum->details->msisdn_sn);
        $client = $this->Admin_model->getClient($service_teum->userid);

        $subs   = array(
                        "CustomerId" => (int) $client->teum_CustomerId,
                        "Items" => array(
                            array(
                                "AccountId" =>  $service_teum->details->teum_accountid,
                                "ProductOfferings" => $addons,
                                "ServiceAddress" => array(
                                    "Address" => $client->address1,
                                    "HouseNo" => $client->housenumber,
                                    "City" => $client->city,
                                    "ZipCode" => $client->postcode,
                                    "State" => "unknown",
                                    "CountryId" => "76"
                                )
                            )
                        ),
                        "channel" => "UnitedPortal V1"
                    );

        echo json_encode($subs);
    }
    public function update_xdsl_pricing()
    {
        $q = $this->db->query("select * from a_services where packageid in (115,116,139,144,146)");

        foreach ($q->result() as $row) {
            if ($row->recurring == '0.00') {
                $res= getMageboProduct($row->packageid);
                $this->db->query('update a_services set recurring=? where id=?', array($res->recurring_total, $row->id));
                print_r($row);
            }
        }
    }


    public function FixcompanyidServiceaddons()
    {
        $q = $this->db->query("select a.id,b.companyid,a.serviceid,b.id as servid from a_services_addons a left join a_services b on b.id=a.serviceid where a.companyid is  null");

        foreach ($q->result() as $row) {
            print_r($row);
            //$this->db->query("update a_services_addons set companyid=? where id=?", array($row->companyid, $row->id));
        }
    }
    public function updateInvoiceDuedate()
    {
        $this->db = $this->load->database('magebo', true);
        $q = $this->db->query("SELECT a.iInvoiceNbr, a.iAddressNbr, a.iInvoiceType, a.dInvoiceDate, a.dInvoiceDueDate, a.mInvoiceAmount, a.tRemark, a.iInvoiceStatus, b.iCompanyNbr
    FROM GDC_ERP.dbo.tblInvoice a left join GDC_ERP.dbo.tblAddress b on b.iAddressNbr=a.iAddressNbr where b.iCompanyNbr = 54 and a.dInvoiceDate='2019-08-07' and a.dInvoiceDueDate='2019-09-06' and iInvoiceNbr >= '310004108' order by a.iInvoiceNbr desc;
    ");

        foreach ($q->result() as $row) {
            print_r($row);
            $this->db->query("update tblInvoice set dInvoiceDuedate=? where iInvoiceNbr=?", array('2019-09-15',$row->iInvoiceNbr));
            echo $this->db->last_query()."\n";
        }

        echo $q->num_rows();
    }
    public function existance()
    {
        echo checkaddon_existance(195, 1813752)->id;
    }


    public function syncronize_teum_justsim($cid)
    {
        $q = $this->db->query("select * from a_services_mobile where companyid=? and id >= ? order by id limit 5", array($cid, 1817046));

        $this->load->library('pareteum', array('companyid' => $cid));

        foreach ($q->result() as $row) {
            // print_r($row);

            $serv = $this->pareteum->GetSubscriptionDetail($row->teum_subscriptionid);

            if ($serv->resultCode == "0") {
                foreach ($serv->Subscription->Products as $prod) {
                    print_r($prod);
                    echo  "Getting ".$row->serviceid." ". $prod->ProductId."\n";
                    print_r($this->getAddonsTeum($row->serviceid, $prod->ProductId));
                }
            }
        }

        echo "\nFound ".$q->num_rows();
    }

    public function getAddonsTeum($serviceid, $bundleid)
    {
        $q = $this->db->query("select * from a_services_addons where serviceid=? and arta_bundleid=?", array($serviceid, $bundleid));
        if ($q->num_rows()>0) {
            return $q->row();
        } else {
            // die($serviceid ." ". $bundleid." Not found\n");
        }
    }
}
/*
Array
(
[0] => OrderDate
[1] => BrandName

[9] => ContactEmail
[10] => IBAN
[11] => Gender
[12] => Domestic Voice/SMS Bundle Price
[13] => Location =
[14] => Domestic Voice/SMS Bundle Duration
[15] => CompanyName
[16] => BirthDate
[17] => MaxUsage
[18] => Referral
[19] => ContactPhone
[20] => PhoneModel
[21] => Domestic Voice/SMS Bundle Startdate
[22] => Inport
[23] => PackageID
[24] => Source
[25] => CustomerLanguage
[26] => Operator
[27] => Domestic Voice/SMS Bundle
[28] => International Voice/SMS Bundle
[29] => Databundle NL/EU
[30] => International Databundle
[31] => Remarks Client

)
$ss = explode('-', $d['21']);
array('clientid' => $cc['id'], 'brand' => 1, 'pid' => $d['23'], 'addons' => '', 'contractduration' => $d['14'], 'porting' => $d['22], 'startdate' => $ss['1'].'-'.$ss['2'].'-'.$ss['0'])

[0] => 2019-01-10 08:35:35
[1] => ExpatMobile
[2] => Hagins
[3] => Zaki
[4] =>
[5] => Statensingel 201
[6] => 6211 pr
[7] => Maastricht
[8] => Netherlands
[9] => l.a.i.nard@gmail.com
[10] =>
[11] => male
[12] => 24.95
[13] => LongHere
[14] => 12
[15] =>
[16] => 1997-02-06
[17] => 80
[18] => Google
[19] => 12675719254
[20] =>
[21] => 2019-01-24
[22] => 0
[23] => 45
[24] => expatmobile.nl
[25] => EN
[26] => T-Mobile NL
[27] =>
[28] =>
[29] =>
[30] =>
[31] => 12 months - GoDutch - Medium - 24.95 - Portation: no -

 */
