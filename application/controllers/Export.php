<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ExportController extends CI_Controller
{
  
    public function __construct()
    {
       
        parent::__construct();
        $this->load->model('Export_model');
        if (php_sapi_name() != "cli") {
            die('Not allowed');
        }
    }
    public function setProperty()
    {
        $this->data['setting'] = globo();
    }
}
class Export extends ExportController
{

    public function __construct()
    {
        parent::__construct();
        
    }


  public function delta_datawarehouse(){
        //$this->update_imsi();
        $last_date= file_get_contents(APPPATH.'logs/app/last_changes.log');
        $date = date('Y-m-d H:i:s');
        $this->fix_contract_date();
        $this->load->dbutil();
        $time =date('Y-m-d');
        $this->db->cache_off();
        $this->load->model('Admin_model');
        //;Dhr.;Jeroen;;Stooker;800004;SOFTLAUNCHM-STOOKER;;;BILLING;Elstar 22;;;5056 DG;;;;;;;;;;;;jeroen.stooker@gmail.com;31619675822;;NL30INGB0007624165;DELTA;2018-11-09 14:46:58
        $q = $this->db->query("SELECT initial,salutation,firstname,'' as middlename,lastname,id,mvno_id as deltaid,'' as caiwayid,address1,housenumber,alphabet,postcode,city,country,address1 as delta_address1,housenumber as delta_housenumber,alphabet as delta_alphabet,postcode as delta_postcode,city as delta_city,country as delta_country,'' as caiway_address1,'' as caiway_housenumber,'' as caiway_alphabet,'' as caiway_postcode,'' as caiway_city,'' as caiway_country,email,phonenumber,'' as phonenumber2,iban,'Delta' as brand,date_modified FROM a_clients WHERE companyid=? and date_modified=? order by date_modified asc", array('53', trim($last_date)));
        
        $s = $this->db->query("SELECT initial,salutation,firstname,'' as middlename,lastname,id,'' as deltaid,mvno_id as caiwayid,address1,housenumber,alphabet,postcode,city,country,'' as delta_address1,'' as delta_housenumber,'' as delta_alphabet,'' as delta_postcode,'' as delta_city,'' as delta_country,address1 as caiway_address1,housenumber as caiway_housenumber,alphabet as caiway_alphabet,postcode as caiway_postcode,city as caiway_city,country as caiway_country,email,phonenumber,'' as phonenumber2,iban,'Caiway' as brand,date_modified FROM a_clients   WHERE companyid=?  and date_modified=? order by date_modified asc", array('56', trim($last_date)));
        echo "Writting delta..\n";


        $delta_p =  $this->db->query("SELECT id as ProductId, 'DELTA' as Brand,name as BundleName,round(data) as Data,round(voice) as Voice,round(sms) as Sms, case status when 1 then 'Active' else 'Inactive' end as Status,date_modified as MutationDate FROM a_products WHERE companyid=53");
        $caiway_p =  $this->db->query("SELECT id as ProductId, 'CAIWAY' as Brand,name as BundleName,round(data) as Data,round(voice) as Voice,round(sms) as Sms, case status when 1 then 'Active' else 'Inactive' end as Status,date_modified as MutationDate FROM a_products WHERE companyid=56");


      $c_service = $this->db->query("SELECT a.userid as CustomerNumber,a.msisdn_imsi as IMSI, b.packageid as Productid, b.date_contract_formated as Start_date, date_terminate as End_Of_Contract,b.date_contract_prolong as Date_Contract_Prolong,b.recurring as MRR, c.promo_value as MRR_Discount, promo_duration as MRR_Discount_Duration,a.date_modified as Date_Mutation,d.companyname as Brand 
        FROM a_services_mobile a 
        left join a_services b on b.id=a.serviceid 
        left join a_promotion c on c.promo_code=b.promocode 
        left join a_mvno d on d.companyid=a.companyid
        WHERE a.companyid in (53,56) and b.status in ('Active','Terminated') and a.date_modified >= ? ORDER BY `Brand` ASC", array(trim($last_date)));
      $csv = $this->dbutil->csv_from_result($q);
       echo "Writting caiway..\n";
       
      $csv .= preg_replace('/^.+\n/', '', $this->dbutil->csv_from_result($s));

       $product = $this->dbutil->csv_from_result($delta_p);
       echo "Writting caiway..\n";
       
      $product .= preg_replace('/^.+\n/', '', $this->dbutil->csv_from_result($caiway_p));
      $service = $this->dbutil->csv_from_result($c_service);
      file_put_contents(FCPATH.'assets/ciot/'.$time.'_export_clients.csv', $csv);
      $this->moveCaiwayFile(FCPATH.'assets/ciot/'.$time.'_export_clients.csv', $time.'_export_clients.csv');
      file_put_contents(FCPATH.'assets/ciot/'.$time.'_export_products.csv', $product);
      $this->moveCaiwayFile(FCPATH.'assets/ciot/'.$time.'_export_products.csv', $time.'_export_products.csv');
      file_put_contents(FCPATH.'assets/ciot/'.$time.'_export_services.csv', $service);
       $this->moveCaiwayFile(FCPATH.'assets/ciot/'.$time.'_export_services.csv', $time.'_export_services.csv');
    file_put_contents(APPPATH.'logs/app/last_changes.log', $date);

    }


    public function fix_contract_date(){
            $last_date= file_get_contents(APPPATH.'logs/app/last_changes.log');
        
          $q = $this->db->query("SELECT b.* FROM a_services_mobile a left join a_services b on b.id=a.serviceid WHERE a.companyid in (53,54,55,56,57) and a.date_modified > ?", array(trim($last_date)));

        foreach($q->result() as $row){         
            $data = array(convert_contract($row->date_contract),  calculate_contractend_date_formated($row->date_contract, $row->contract_terms), $row->id);
            $this->db->query("update a_services set date_contract_formated =?, date_contract_prolong= ? WHERE id=?", $data);
                print_r($data);
        }

    }

     public function moveCaiwayFile($File, $filename)
    {
        echo "Uploading {$filename}\n";
        $connection = ssh2_connect('ftp-sftp1.caiw.net', 2222);
        ssh2_auth_password($connection, 'delta_cdr_prod_artilium', 'CwDg9z');
        $sftp = ssh2_sftp($connection);
       
       $resFile = fopen("ssh2.sftp://{$sftp}/CRM_EXPORT/".$filename, 'w');
       $srcFile = fopen($File, 'r');
       echo stream_copy_to_stream($srcFile, $resFile);
       fclose($resFile);
       fclose($srcFile);

    }

        public function export_client()
    {
        try
        {
            $this->load->library('xlswriter');
            $this->load->model('Admin_model');
            $q = $this->db->query("SELECT * FROM a_request_export WHERE status=? and type_export=?", array(
                'created',
                'client'
            ));
            if ($q->num_rows() > 0)
            {
                foreach ($q->result() as $r)
                {
                    $setting = globofix($r->companyid);
                    if (!file_exists($setting->DOC_PATH . $r->companyid . '/exports'))
                    {
                        mkdir($setting->DOC_PATH . $r->companyid . '/exports', 0777, true);
                    }
                    $this->db->query('update a_request_export set status=? WHERE id=?', array(
                        'processing',
                        $r->id
                    ));
                    $fileName = 'ExportID_' . $r->id . '_Clients-Summary_by_Date_' . date('Y-m-d') . '.xlsx';
                    $header   = array(
                        'ArtiliumID' => 'string',
                        'BillingID' => 'string',
                        'MvnoID' => 'string',
                        'Salutation' => 'string',
                        'Initial' => 'string',
                        'Firstname' => 'string',
                        'Lastname' => 'string',
                        'CompanyName' => 'string',
                        'VAT' => 'string',
                        'Email' => 'string',
                        'Address' => 'string',
                        'Postcode' => 'string',
                        'City' => 'string',
                        'Country' => 'string',
                        'Language' => 'string',
                        'Phonenumber' => 'string',
                        'Paymentmethod' => 'string',
                        'Due days' => 'string',
                        'Date Birth' => 'string',
                        'Vat Rate' => 'string',
                        'AgentID' => 'string',
                        'Gender' => 'string',
                        'Referral' => 'string',
                        'Location' => 'string',
                        'IBAN' => 'string',
                        'BIC' => 'string',
                        'Madate ID' => 'string',
                        'Mandate Status' => 'string',
                        'Mandate Date' => 'string'
                    );
                    $this->db = $this->load->database('default', true);
                    $q        = $this->db->query("SELECT a.id,a.mageboid,a.mvno_id,a.salutation,a.initial,a.firstname,a.lastname,a.companyname,a.vat,a.email,a.address1,a.postcode,a.city,a.country,a.language,a.phonenumber,a.paymentmethod,a.payment_duedays, a.date_birth,a.vat_rate,a.agentid,a.gender,a.refferal,a.location FROM a_clients a WHERE a.companyid=?", array(
                        $r->companyid
                    ));
                    if ($q->num_rows() > 0)
                    {
                        $res = $q->result_array();
                        foreach ($res as $j)
                        {
                            if ($j['paymentmethod'] == "directdebit")
                            {
                                $mandate             = $this->Admin_model->getMandateId($j['mageboid']);
                                $j['iban']           = $mandate->IBAN;
                                $j['bic']            = $mandate->BIC;
                                $j['mandate_id']     = $mandate->SEPA_MANDATE;
                                $j['mandate_status'] = $mandate->SEPA_STATUS;
                                $j['mandate_date']   = $mandate->SEPA_MANDATE_SIGNATURE_DATE;
                                $rim[]               = $j;
                            }
                            else
                            {
                                $j['iban']           = "";
                                $j['bic']            = "";
                                $j['mandate_id']     = "";
                                $j['mandate_status'] = "";
                                $j['mandate_date']   = "";
                                $rim[]               = $j;
                            }
                            print_r($j);
                            unset($j);
                        }
                        /*
                        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition: attachment; filename="' . $fileName . '"');
                        */
                        $this->xlswriter->writeSheet($rim, 'Sheet1', $header);
                        $this->xlswriter->setAuthor('Simson Parlindungan');
                        $this->xlswriter->setTitle('Client Export');
                        $this->xlswriter->setCompany('United');
                        $this->xlswriter->writeToFile($setting->DOC_PATH . $r->companyid . '/exports/' . $fileName);
                        //$this->session->set_flashdata('success', 'Downloaded as requested');
                        $this->data['setting'] = globofix($r->companyid);
                        if ($this->data['setting']->smtp_type == 'smtp')
                        {
                            $config = array(
                                'protocol' => 'smtp',
                                'smtp_host' => $this->data['setting']->smtp_host,
                                'smtp_port' => $this->data['setting']->smtp_port,
                                'smtp_user' => $this->data['setting']->smtp_user,
                                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                                'mailtype' => 'html',
                                'charset' => 'utf-8',
                                'wordwrap' => true
                            );
                        }
                        else
                        {
                            $config['protocol'] = 'sendmail';
                            $config['mailpath'] = '/usr/sbin/sendmail';
                            $config['mailtype'] = 'html';
                            $config['charset']  = 'utf-8';
                            $config['wordwrap'] = true;
                        }
                        $body = "<p>Hello</p><p>Export ID:#" . $r->id . "</p>Filename: " . $fileName . "<br>RequestDate: " . $r->date . "<br>Type: " . ucfirst($r->type_export) . "<p>Has been created</p><p>Please find the file in this email attachment</p>Kind Regards<br /><br />United";
                        $this->email->clear(true);
                        $this->email->initialize($config);
                        $this->email->set_newline("\r\n");
                        $this->email->from($setting->smtp_sender, $setting->smtp_name);
                        $this->email->to($r->recipient);
                        $this->email->bcc('mail@simson.one');
                        $this->email->subject('United Portal' . " Backup Report");
                        $this->email->message($body);
                        // shell_exec('zip '.$setting->DOC_PATH . $r->companyid . '/exports/'.$fileName.'.zip '.$setting->DOC_PATH . $r->companyid . '/exports/' . $fileName);
                        $this->email->attach($setting->DOC_PATH . $r->companyid . '/exports/' . $fileName, 'attachment', $fileName);
                        if ($this->email->send())
                        {
                            $this->db->query('update a_request_export set filename=?, status=? WHERE id=?', array(
                                $fileName,
                                'sent',
                                $r->id
                            ));
                            echo "Export file " . $fileName . " has been sent successfully\n";
                        }
                        else
                        {
                            $this->db->query('update a_request_export set filename=?, status=? WHERE id=?', array(
                                $fileName,
                                'failed',
                                $r->id
                            ));
                            echo print_r($this->email->print_debugger());
                        }
                    }
                    else
                    {
                        //$this->session->set_flashdata('error', 'No data found');
                    }
                }
            }
        }
        catch (Exception $e)
        {
            mail('mail@simson.one', "Exception Sexport_client ", "Message: " . $e->getMessage());
        }
    }

      public function ciot_trendcall($companyid)
    {
        $myFile = $companyid . '_ciot_export_' . date('YmdHis') . '.csv';
        $q      = $this->db->query("SELECT a.companyname as Bedrijfsnaam, a.lastname as Achternaam, a.salutation as Voorvoegsels, a.initial as Voorletters, a.address1 as Straatnaam, a.housenumber as Huisnummer, a.alphabet as Huisnummertoevoeging, a.postcode as Postcode, '' as PostcodeBL, a.city as Woonplaats, '' as  LocatieOms, a.country as LAND, 'A' as soortNAW, 'GSM' as soortverbinding, 'ABON 'as DienstSoort, 'TRENDCALL' as DienstAanbieder,'TRENDCALL' as NetwerkAanbieder, a.date_created as Generatiedatum, c.date_contract as Ingangsdatumtijd, c.date_terminate as Einddatumtijd, b.msisdn as Telefoonnummer FROM a_services_mobile b left join a_clients a on a.id=b.userid left join a_services c on c.id=b.serviceid WHERE a.status in('Active','Terminated') and b.companyid=? and b.msisdn != '' order by a.id asc ", array(
            $companyid
        ));
        $fh = fopen('/home/mvno/documents/' . $companyid . '/ciot/' . $myFile, 'a') or die("can't open file");
        fwrite($fh, "Bedrijfsnaam,Achternaam,Voorvoegsels,Voorletters,Straatnaam,Huisnummer, Huisnummertoevoeging,Postcode,PostcodeBL,Woonplaats,LocatieOms,Land,soortNAW ,soortverbinding,DienstSoort,DienstAanbieder,NetwerkAanbieder,Generatiedatum,Ingangsdatumtijd,Einddatumtijd,Telefoonnummer\n");
        foreach ($q->result_array() as $fields)
        {
            if ($fields['LAND'] == "NL")
            {
                $fields['Postcode']   = $fields['Postcode'];
                $fields['PostcodeBL'] = '';
            }
            else
            {
                $fields['PostcodeBL'] = $fields['Postcode'];
                $fields['Postcode']   = '';
            }
            $p                          = explode('-', $fields['Ingangsdatumtijd']);
            $fields['Ingangsdatumtijd'] = $p[2] . '-' . $p[0] . '-' . $p[1];
            fputcsv($fh, $fields);
        }
        fclose($fh);
        echo '/home/mvno/documents/' . $companyid . '/ciot/' . $myFile;
        $this->data['setting'] = globofix($companyid);
        if ($this->data['setting']->smtp_type == 'smtp')
        {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'wordwrap' => true
            );
        }
        else
        {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $body = "<p>Hello</p><p>Export ID:#" . time() . "</p>Filename: " . $myFile . "<br>RequestDate: " . date('Y-m-d') . "<br>Type: CIOT Report<p>Has been created</p><p>Please find the file in this email attachment</p>Kind Regards<br /><br />United";
        $this->email->clear(true);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($setting->smtp_sender, $setting->smtp_name);
        $this->email->to('lex.v.d.hondel@trendcall.com');
        $this->email->bcc('mail@simson.one');
        $this->email->subject('United Portal' . " CIOT Daily Report");
        $this->email->message($body);
        // shell_exec('zip '.$setting->DOC_PATH . $r->companyid . '/exports/'.$fileName.'.zip '.$setting->DOC_PATH . $r->companyid . '/exports/' . $fileName);
        $this->email->attach('/home/mvno/documents/' . $companyid . '/ciot/' . $myFile, 'attachment', $myFile);
        if ($this->email->send())
        {
            echo date('Y-m-d H:i:s') . " CIOT Report email has been sent\n";
        }
    }


}