<?php
use Genkgo\Camt\Config;
use Genkgo\Camt\Reader;

class Cron extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');
        if (!is_cli()) {
            die("Access Denied\n");
        }
        $g = getMasterConfig();
        if ($g->disable_cron == 1) {
            die("Cronjob are currently disabled\n");
        }
        if ($g->maintenance == 1) {
            die("Frameworks are under maintenance\n");
        }
        $this->masterconfig = $g;
        $this->load->model('Teams_model', 'teams');
    }
    public function getPending()
    {
        $this->load->model('Admin_model');
        $this->load->library('artilium', array(
            'companyid' => '53'
        ));
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select a.msisdn,a.msisdn_sn,a.msisdn_sim,a.serviceid,b.date_contract from a_services_mobile a left join a_services b on b.id=a.serviceid where a.msisdn_status in ('PortinPending','ActivationRequested') and b.companyid='53'");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $d = explode('-', $row->date_contract);
                if (date('Y-m-d') < $d[2] . '-' . $d[0] . '-' . $d[1]) {
                    log_message("error", "Disable Services  ".$row->serviceid);
                    $order  = $this->Admin_model->getServiceCli($row->serviceid);
                    //print_r($order);
                    $msisdn = $this->artilium->getSn($order->details->msisdn_sim);

                    $pack = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                    $kk   = $this->artilium->UpdateServices($msisdn->data->SN, $pack, '0');
                } else {
                }
            }
        }
    }
    public function auto_unsuspend()
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from a_payments where created like ?  and auto_unsuspend = ? group by userid order by userid asc", array(
            date('Y-m-d') . '%',
            '0'
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $this->db->query('update a_payments set auto_unsuspend=? where id=?', array(
                    1,
                    $row->id
                ));
                $this->load->library('magebo', array(
                    'companyid' => $row->companyid
                ));
                $this->load->library('artilium', array(
                    'companyid' => $row->companyid
                ));
                $userid = getiAddressNbr($row->userid);
                if ($userid) {
                    if (!$this->magebo->hasUnpaidInvoice($userid)) {
                        log_message("error", $userid . " This customer has no Open Invoice\nReady to Unsuspend");
                        $services = $this->Admin_model->getServices($row->userid);
                        if ($services) {
                            foreach ($services as $serv) {
                                if ($serv->status == "Suspended") {
                                    $ii   = $this->Admin_model->getPackageState($serv->id);
                                    $pack = (array) json_decode($ii);
                                    $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                                    $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                        'bar' => 0,
                                        'date_modified' => date('Y-m-d H:i:s')
                                    ));
                                    $this->Admin_model->update_services($serv->id, array(
                                        'status' => 'Active'
                                    ));
                                    logAdmin(array(
                                        'companyid' => $row->companyid,
                                        'userid' => $serv->userid,
                                        'serviceid' => $serv->id,
                                        'user' => 'System',
                                        'ip' => '127.0.0.1',
                                        'description' => 'Subscriptions has been Unsuspended, because all invoices have been paid'
                                    ));
                                }
                            }
                        }
                        $this->Admin_model->ChangeClientDunnigProfile($row->userid, 'No');
                    }
                }
                unset($userid);
                unset($this->artilium);
                unset($this->magebo);
            }
        }
    }
    public function prune_order()
    {
        $this->load->model('Admin_model');
        $companies = getCompanies();
        $this->db  = $this->load->database('default', true);
        foreach ($companies as $cid) {
            //   $q = $this->db->query("select a.* from a_services a left join a_services_mobile b on b.serviceid=a.id where (b.msisdn_sim is NULL || msisdn_sim = '')");
        }
    }
    public function EmscheckInvoice()
    {
        $this->load->model('Admin_model');
        $filename = '/home/mvno/public_html/assets/datas/ems';
        $contents = file($filename);
        $this->load->library('magebo', array(
            'companyid' => 54
        ));
        foreach ($contents as $line) {
            if ($this->magebo->getInvoice(trim($line))->iInvoiceStatus != 54) {
                print_r($line);
            }
        }
    }
    public function ActivateSim($date = false)
    {
        try {
            $myDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime(date("Y-m-d"))) . "-1 month"));
            $this->db->query("delete from a_logs where description like  'Logged successfully' and date < ?", array(
                $myDate
            ));
            log_message("error", date('Y-m-d H:i:s') . "      Login logs has been cleared before: " . $myDate . "\n");
            $this->load->model('Admin_model');
            //SELECT b.msisdn_sn,a.companyid,a.id,b.msisdn,a.date_contract,a.status FROM a_services a left join a_services_mobile b on b.serviceid=a.id where a.date_contract like '%-2019' and a.status='Pending' ORDER BY `b`.`msisdn_sn` ASC
            if ($date) {
                $d = $date;
            } else {
                $d = date('m-d-Y');
            }
            $this->db = $this->load->database('default', true);
            $q        = $this->db->query("SELECT a.msisdn,a.msisdn_sn,a.msisdn_sim,a.serviceid,b.date_contract,a.companyid,a.msisdn_status,b.status,a.msisdn_type
        FROM a_services_mobile a
        LEFT JOIN a_services b ON b.id=a.serviceid
        WHERE b.companyid IN (53,54,55,56,57)
        AND b.type=?
        AND b.date_contract=?
        AND a.msisdn_type = ?
        AND a.msisdn_sn IS NOT NULL
        AND b.status ='Pending'
        AND a.msisdn_status =?", array(
                'mobile',
                $d,
                'new',
                'ActivationRequested'
            ));
            log_message("error", date('Y-m-d H:i:s') . "      Starting to Activate NewSIM Which contract date Due today e: " . date('Y-m-d') . "\n");
            log_message("error", date('Y-m-d H:i:s') . "      found " . $q->num_rows() . " \n");
            if ($q->num_rows() > 0) {
                foreach ($q->result() as $row) {
                    log_message("error", print_r($row, true));
                    $cid = $row->companyid;
                    $this->load->library('artilium', array(
                        'companyid' => $cid
                    ));
                    $this->load->library('magebo', array(
                        'companyid' => $cid
                    ));
                    $order = $this->Admin_model->getServiceCli($row->serviceid);
                    if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                        //print_r($order);
                        //print_r($row);
                        //echo $row->status." ".$row->msisdn_status."\n";
                        // $this->send_welcome_email($row->serviceid);
                        // $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                        if ($row->status == "Pending" && $row->msisdn_type == "new") {
                            log_message("error", date('Y-m-d H:i:s') . "      " . $cid . " " . $row->serviceid . "  " . $order->details->msisdn . " has been activated\n");
                            $pack = $this->artilium->GetListPackageOptionsForSnAdvance($order->details->msisdn_sn);
                            $this->artilium->UpdateServices($order->details->msisdn_sn, $pack, '1');
                            $this->artilium->UpdateCli($order->details->msisdn_sn, '1');
                            $this->magebo->addPricingSubscription($order);
                            $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                            $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                            logAdmin(array(
                                'companyid' => $cid,
                                'userid' => $order->userid,
                                'serviceid' => $row->serviceid,
                                'user' => 'System',
                                'ip' => '127.0.0.1',
                                'description' => $order->details->msisdn . " Contract date has been reached therefor subscription is now active"
                            ));
                        }
                        if (($row->status == "Active") && ($row->msisdn_status != "Active") && ($row->msisdn_type == "new")) {
                            log_message("error", date('Y-m-d H:i:s') . "      " . $cid . " " . $row->serviceid . "  " . $order->details->msisdn . " was syncronized\n");
                            $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                            $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                        }
                    } else {
                        if ($row->status == "Pending" && $row->msisdn_type == "new") {
                            $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                            $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                            $this->artilium->PartialFullBar($order->details, '0');
                            $this->send_welcome_email($row->serviceid);
                            logAdmin(array(
                                'companyid' => $cid,
                                'userid' => $order->userid,
                                'serviceid' => $row->serviceid,
                                'user' => 'System',
                                'ip' => '127.0.0.1',
                                'description' => $order->details->msisdn . " Contract date has been reached therefor subscription is now active"
                            ));
                        }
                    }
                    $this->send_welcome_email($row->serviceid);
                    unset($order);
                    unset($cid);
                    unset($this->magebo);
                    unset($this->artilium);
                }
            } //end of New Sim Activation
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception ActivateSim ", "Message: " . $e->getMessage());
        }
    }
    public function enableServices()
    {
        $this->load->model('Cron_model');
        $q = $this->Cron_model->getNeedProcess();
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $this->load->library('artilium', array(
                    'companyid' => $row->companyid
                ));
                $pack = $this->artilium->GetListPackageOptionsForSnAdvance($row->msisdn_sn);
                $this->artilium->UpdateCLI($row->msisdn_sn, 1);
                $this->artilium->UpdateServices($row->msisdn_sn, $pack->NewDataSet->PackageOptions, '1');
                unset($this->artilium);
            }
        }
    }
    public function activate_porting_accepted($date = false)
    {

        //Only for delta and trendcall
        error_reporting(0);
        $this->load->model('Admin_model');
        //SELECT b.msisdn_sn,a.companyid,a.id,b.msisdn,a.date_contract,a.status FROM a_services a left join a_services_mobile b on b.serviceid=a.id where a.date_contract like '%-2019' and a.status='Pending' ORDER BY `b`.`msisdn_sn` ASC
        if ($date) {
            $d = $date;
        } else {
            $d = date('m-d-Y');
        }
        // Start Activate the Due Portin contract Date

        log_message("error", "DELTA & TRENDCALL Starting to Activate Portin Sim Which contract date Due today: " . $d);
        $x = $this->db->query("SELECT a.msisdn,a.msisdn_sn,a.msisdn_sim,a.serviceid,b.date_contract,a.companyid,a.msisdn_status,b.status,a.msisdn_type,b.iGeneralPricingIndex
        FROM a_services_mobile a
        LEFT JOIN a_services b ON b.id=a.serviceid
        WHERE b.companyid IN (53,54)
        AND b.type=?
        AND b.date_contract=?
        AND a.msisdn_type = ?
        AND a.msisdn_sn IS NOT NULL
        AND b.status NOT IN('Suspended','Terminated','Cancelled','New','Active')
        and a.msisdn_status = ?
        Order by a.serviceid asc", array(
            'mobile',
            $d,
            'porting',
            'PortInAccepted'
        ));
        if ($x->num_rows() > 0) {
            //print_r($x->result());
            //exit;
            foreach ($x->result() as $row) {
                // if($row->msisdn != "31642325169"){
                $cid = $row->companyid;
                $this->load->library('artilium', array(
                    'companyid' => $cid
                ));
                $this->load->library('magebo', array(
                    'companyid' => $cid
                ));
                $order = $this->Admin_model->getServiceCli($row->serviceid);
                if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                    //print_r($order);
                    //print_r($row);
                    //echo $row->status." ".$row->msisdn_status."\n";
                    // $this->send_welcome_email($row->serviceid);
                    // $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                    if ($row->status == "Pending" && $row->msisdn_type == "porting") {
                        if (in_array($row->msisdn_status, array(
                            "PortinAccepted"
                        ))) {
                            if ($row->iGeneralPricingIndex <= 0) {
                                //print_r($row);
                                //echo date('Y-m-d H:i:s') . "      " . $cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " has been activated\n";
                                log_message("error", $cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " has been activated");
                                $pack = $this->artilium->GetListPackageOptionsForSnAdvance($order->details->msisdn_sn);
                                $this->artilium->UpdateServices($order->details->msisdn_sn, $pack, '1');
                                $this->artilium->UpdateCli($order->details->msisdn_sn, '1');
                                $this->magebo->AddSIMToMagebo($order);
                                $this->magebo->addPricingSubscription($order);
                                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
                                $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                                // do not activate the porting status
                                // $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                                logAdmin(array(
                                    'companyid' => $cid,
                                    'userid' => $order->userid,
                                    'serviceid' => $row->serviceid,
                                    'user' => 'System',
                                    'ip' => '127.0.0.1',
                                    'description' => $order->details->msisdn . " Contract date has been reached even if its a porting therefore subscription is now active"
                                ));
                            } else {
                                //   echo date('Y-m-d H:i:s') . "      " . $cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " has already Pricing Index Skipping...\n";
                                log_message("error", $cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " has already Pricing Index Skipping");
                            }
                        } else {
                            // echo date('Y-m-d H:i:s') . "      " . $cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " Has Cancelled or Rejected status Skipping...\n";
                            log_message("error", $cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " Has Cancelled or Rejected status Skipping");
                        }
                    }
                }
                // $this->send_welcome_email($row->serviceid);
                unset($order);
                unset($cid);
                unset($this->magebo);
                unset($this->artilium);
                //exit;
                //}
            }
        }
    }
    public function activate_porting_due($date = false)
    {
        // only for caiway
        error_reporting(0);
        $this->load->model('Admin_model');
        //SELECT b.msisdn_sn,a.companyid,a.id,b.msisdn,a.date_contract,a.status FROM a_services a left join a_services_mobile b on b.serviceid=a.id where a.date_contract like '%-2019' and a.status='Pending' ORDER BY `b`.`msisdn_sn` ASC
        if ($date) {
            $d = $date;
        } else {
            $d = date('m-d-Y');
        }
        // Start Activate the Due Portin contract Date
        log_message("error", "CAIWAY Starting to Activate Portin Sim Which contract date Due today: " . $d);
        $x = $this->db->query("SELECT a.msisdn,a.msisdn_sn,a.msisdn_sim,a.serviceid,b.date_contract,a.companyid,a.msisdn_status,b.status,a.msisdn_type,b.iGeneralPricingIndex
        FROM a_services_mobile a
        LEFT JOIN a_services b ON b.id=a.serviceid
        WHERE b.companyid IN (56)
        AND b.type=?
        AND b.date_contract=?
        AND a.msisdn_type = ?
        AND a.msisdn_sn IS NOT NULL
        AND b.status NOT IN('Suspended','Terminated','Cancelled','New','Active')
        Order by a.serviceid asc", array(
            'mobile',
            $d,
            'porting'
        ));
        if ($x->num_rows() > 0) {
            //print_r($x->result());
            //exit;
            foreach ($x->result() as $row) {
                // if($row->msisdn != "31642325169"){
                $cid = $row->companyid;
                $this->load->library('artilium', array(
                    'companyid' => $cid
                ));
                $this->load->library('magebo', array(
                    'companyid' => $cid
                ));
                $order = $this->Admin_model->getServiceCli($row->serviceid);
                if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                    //print_r($order);
                    //print_r($row);
                    //echo $row->status." ".$row->msisdn_status."\n";
                    // $this->send_welcome_email($row->serviceid);
                    // $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                    if ($row->status == "Pending" && $row->msisdn_type == "porting") {
                        if ($row->iGeneralPricingIndex <= 0) {
                            //print_r($row);
                            log_message("error", date('Y-m-d H:i:s') . "      " . $cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " has been activated\n");
                            $pack = $this->artilium->GetListPackageOptionsForSnAdvance($order->details->msisdn_sn);
                            $this->artilium->UpdateServices($order->details->msisdn_sn, $pack, '1');
                            $this->artilium->UpdateCli($order->details->msisdn_sn, '1');
                            $this->magebo->AddSIMToMagebo($order);
                            //$this->magebo->addPricingSubscription($order);
                            $this->magebo->addPricingSubscription($order, false, false, true);
                            $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
                            $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                            // do not activate the porting status
                            // $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                            logAdmin(array(
                                'companyid' => $cid,
                                'userid' => $order->userid,
                                'serviceid' => $row->serviceid,
                                'user' => 'System',
                                'ip' => '127.0.0.1',
                                'description' => $order->details->msisdn . " Contract date has been reached despite the status of portin, therefore subscription is now active request by Erwin & Bob"
                            ));
                        } else {
                            log_message("error", date('Y-m-d H:i:s') . "      " . $cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " has already Pricing Index Skipping...\n");
                        }
                    }
                }
                // $this->send_welcome_email($row->serviceid);
                unset($order);
                unset($cid);
                unset($this->magebo);
                unset($this->artilium);
                //exit;
                //}
            }
        }
    }
    public function activatesimToday()
    {
        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("SELECT a.* FROM `a_services_mobile` a LEFT JOIN a_services b on b.id=a.serviceid WHERE a.msisdn_sn IS NOT NULL AND a.msisdn_status = 'OrderWaiting' AND a.msisdn_type = 'new' and b.date_contract = ? and b.status= ?", array(
            'm-d-Y'
        ), 'Pending');
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $mobile = $this->Admin_model->getServiceCli($row->serviceid);
                $this->load->library('artilium', array(
                    'companyid' => $row->companyid
                ));
                $this->load->library('magebo', array(
                    'companyid' => $row->companyid
                ));
                $this->magebo->addPricingSubscription($mobile);
                //addSimcardLoggid(trim($companyid), trim($mobile->details->msisdn_sim), '612');
                $this->Admin_model->ChangeServiceStatus($mobile->details->serviceid, 'Active');
                $this->Admin_model->ChangeOrdertatus($mobile->details->serviceid, 'Active');
                // $this->send_welcome_email($mobile->details->serviceid);
                if (substr($mobile->details->msisdn, 0, 2) == "32") {
                    $this->artilium->PartialFullBar($order->details, 0);
                } else {
                    $pack = $this->artilium->GetListPackageOptionsForSnAdvance($mobile->details->msisdn_sn);
                    $this->artilium->UpdateCLI($mobile->details->msisdn_sn, 1);
                    $this->artilium->UpdateServices($mobile->details->msisdn_sn, $pack->NewDataSet->PackageOptions, '1');
                }
                unset($this->magebo);
                unset($this->artilium);
            }
        }
    }
    public function sendEmailNotification($s, $subject, $message, $client)
    {
        $fail                  = false;
        $service               = $this->Admin_model->getServiceCli($s->serviceid);
        $this->data['setting'] = globofix($service->companyid);
        if (!empty($client->email_notification)) {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'text';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
            $this->email->clear(true);
            $this->email->initialize($config);
            if ($message == "Request") {
                $this->email->from('techniek@united-telecom.be', 'United telecom Team');
                $msg = "Hello Team,\n";
                $msg .= "\n";
                $msg .= "Client ID         : " . $client->mvno_id . "\n";
                $msg .= "Artilium ID       : " . $client->mageboid . "\n";
                $msg .= "MSISDN Number     : " . $service->msisdn . "\n";
                $msg .= "Serial Number     : " . $service->msisdn_sn . "\n";
                $msg .= "Simcard Number    : " . $service->msisdn_sim . "\n";
                $msg .= "Do you wish to accept this portout please reply to this email with the date you wish to accept\n\n";
                $msg .= "Regards\n";
                $msg .= "\n";
            } elseif ($message == "PortingFailed") {
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $msg = "Hello Team,\n";
                $msg .= "\n";
                $msg .= "Client ID         : " . $client->mvno_id . "\n";
                $msg .= "Artilium ID       : " . $client->mageboid . "\n";
                $msg .= "MSISDN Number     : " . $service->msisdn . "\n";
                $msg .= "Serial Number     : " . $service->msisdn_sn . "\n";
                $msg .= "Simcard Number    : " . $service->msisdn_sim . "\n";
                $msg .= "Do you wish to accept this portout please reply to this email with the date you wish to accept\n\n";
                $msg .= "Regards\n";
                $msg .= "\n";
                $fail = true;
            } else {
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $msg = "Hello Team,\n";
                $msg .= "\n";
                $msg .= "Client ID         : " . $client->mvno_id . "\n";
                $msg .= "Artilium ID       : " . $client->mageboid . "\n";
                $msg .= "MSISDN Number     : " . $service->msisdn . "\n";
                $msg .= "Serial Number     : " . $service->msisdn_sn . "\n";
                $msg .= "Simcard Number    : " . $service->msisdn_sim . "\n";
                $msg .= "EVENT             : " . $message . "\n";
                $msg .= "Please Open The client and check this " . $client->portal_url . "admin/client/detail/" . $client->id . "\n\n";
                $msg .= "Regards\n";
                $msg .= "\n";
                $msg .= "If you wish to recieved  this event to be sent to your webservices, please contact our support\n";
            }
            $body = $msg;
            $this->email->set_newline("\r\n");
            if ($fail) {
                $email = array(
                    'simson.parlindungan@pareteum.com'
                );
                if (in_array($client->companyid, array(
                    53,
                    56
                ))) {
                    $this->email->bcc('mobiel@delta.nl');
                }
            } else {
                $email = explode("|", $client->email_notification);
            }
            if ($email) {
                $this->email->to($email);
                $this->email->subject($subject);
                $this->email->message($body);
                if (!$this->email->send()) {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email_notification,
                        'subject' => $subject,
                        'message' => $msg
                    ));
                    echo $this->email->print_debugger();
                    //$cid . " " . $row->serviceid . "  " . $order->details->msisdn_sn . " Has Cancelled or Rejected status Skipping
                }
            }
        }
    }
    public function BlockSim($companyid)
    {
        $this->load->library('artilium', array(
            'companyid' => $companyid
        ));
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("SELECT a.msisdn,a.msisdn_sn,a.msisdn_sim,a.serviceid,b.date_contract
        FROM a_services_mobile a
        LEFT JOIN a_services b ON b.id=a.serviceid
        WHERE b.companyid =?
        AND b.type=?
        AND b.date_contract=?
        AND LENGTH(a.msisdn_sn) >= 11
        AND b.status NOT IN('Suspended','Terminated','Cancelled')", array(
            $companyid,
            'mobile',
            date('m-d-Y')
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $order  = $this->Admin_model->getServiceCli($row->serviceid);
                $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                echo $row->serviceid . " " . date('Y-m-d H:i:s') . " " . $order->details->msisdn . "has been activated\n";
                $pack = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                $kk   = $this->artilium->UpdateServices($msisdn->data->SN, $pack, '1');
                $this->artilium->UpdateCli($msisdn->data->SN, '1');
                $this->magebo->addPricingSubscription($order);
            }
        }
    }
    public function termination($date = false)
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        if ($date) {
            $killdate = new DateTime($date);
            $killdate->modify('-1 day');
            $td = $killdate->format('Y-m-d') . 'T23:59:59';
        } else {
            $td = date('Y-m-d', strtotime("-1 days")) . "T23:59:59";
        }
        log_message("error", "Running Termination");
        echo "Running Termination\n";
        $q = $this->db->query("select c.companyname,c.portal_url,a.status,a.date_terminate,a.id,b.serviceid,b.msisdn,b.msisdn_sn,a.companyid from a_services a left join a_services_mobile b on b.serviceid=a.id left join a_mvno c on c.companyid=a.companyid where a.date_terminate = ? and a.status != ? and a.type=?", array(
            date('Y-m-d'),
            'Terminated',
            'mobile'
        ));
        if ($q->num_rows() > 0) {
            log_message("error", "Found " . $q->num_rows() . "  termination request(s)");
            echo "Found " . $q->num_rows() . "  termination request(s)\n";
            foreach ($q->result() as $row) {
                if ($row->status != 'Terminated') {
                    log_message("error", "Terminating Serviceid :"  .$row->msisdn_sn." Companyid :". $row->companyid);
                    $this->load->model('Admin_model');
                    $this->load->library('artilium', array(
                        'companyid' => $row->companyid
                    ));
                    $this->load->library('magebo', array(
                        'companyid' => $row->companyid
                    ));
                    $terminate = $this->artilium->TerminateCLI(trim($row->msisdn_sn), $td);
                    $mobile    = $this->Admin_model->getServiceCli($row->id);
                    $this->magebo->TerminateComplete($mobile, date('Y-m-d', strtotime("+1 days")));
                    unset($this->artilium);
                    $this->db->query("update a_services set status=? where id=?", array(
                        "Terminated",
                        $row->id
                    ));
                    $this->db->query("update a_services_mobile set msisdn_status=?, datacon_ftp = '0' where serviceid=?", array(
                        "Terminated",
                        $row->id
                    ));
                    $this->sendmail($row->id, 'service', 'subscription_mobile_terminated');
                }
                unset($service);
            }
        } else {
            log_message("error", "No termination request found");
            echo "No termination request found\n";
        }
    }
    public function auto_assign()
    {
        print_r(get_promotion_auto_assign());
    }
    public function update_client_magebo()
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $this->load->library('magebo', array(
            'companyid' => 53
        ));
        $this->db->where('companyid', 53);
        $q = $this->db->get('a_clients');
        foreach ($q->result_array() as $client) {
            print_r($client);
            if ($client['mageboid'] > 0) {
                if ($client['mageboid'] != "152773") {
                    $clientid = $client['mageboid'];
                    unset($client['mageboid']);
                    $result = $this->magebo->UpdateClient($clientid, $client);
                    print_r($result);
                }
            }
        }
    }
    public function testcrypt()
    {
        $plain_text = 'This is a plain-text message!';
        $ciphertext = $this->encryption->encrypt($plain_text);
        // Outputs: This is a plain-text message!
        echo $this->encryption->decrypt($ciphertext);
    }
    public function fix_porting_issue()
    {
        $this->load->helper('mq');
        $this->load->model('Admin_model');
        $this->load->model('Api_model');
        $date     = date('Y-m-d');
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_event_log v left join  a_services_mobile a on a.serviceid=v.serviceid left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where v.date_event LIKE ? and v.JobName ='PortInChangeMsisdn' and v.FinalJob='true' and v.Success='true' ", array(
            $date . '%'
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $service) {
                $this->load->library('artilium', array(
                    'companyid' => $service->companyid
                ));
                $this->load->library('magebo', array(
                    'companyid' => $service->companyid
                ));
                $dc            = explode('-', $service->date_contract);
                $date_contract = $dc['2'] . '-' . $dc['0'] . '-' . $dc['1'];
                $client        = getCompanydataByUser($service->userid);
                $companyid     = $service->companyid;
                ilogClient(array(
                    'companyid' => $companyid,
                    'description' => 'Number: ' . $service->msisdn . ' has been ported successfully',
                    'user' => 'System',
                    'userid' => $service->userid,
                    'serviceid' => $service->serviceid,
                    'ip' => '127.0.0.1'
                ));
                // sendEmailNotification($service, "Number has been Ported successfully SN:" . $service->msisdn_sn, "This porting has been completed, number now is on our platform", $client);
                addLogActivation($companyid, $service->serviceid, 'PortingDone');
                addSimcardLoggid(trim($companyid), trim($service->msisdn_sim), '612');
                $this->Admin_model->ChangeServiceStatus($service->serviceid, 'Active');
                $this->Admin_model->ChangeOrdertatus($service->serviceid, 'Active');
                $this->send_welcome_email($service->serviceid);
                unset($this->magebo);
                unset($this->artilium);
            }
        }
    }
    public function send_welcome_email($id, $portin = false)
    {
        $this->load->model('Admin_model');
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $brand                 = $this->Admin_model->getBrandPdfFooter($mobile->gid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                //////'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent('order_accepted', $client->language, $client->companyid);
        $body = str_replace('{$name}', $client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$base_url}', url_to_domain($this->data['setting']->base_url), $body);
        //$body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
        $body = str_replace('{$msisdn}', '0' . substr($mobile->details->msisdn, 2), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
        $body = str_replace('{$Companyname}', $brand->name, $body);
        if ($client->companyid == 53) {
            $combi = $this->Admin_model->GetProductType($mobile->packageid);
            if ($combi == 2) {
                $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. Vanaf nu ontvangt u daarom thuis 25 Mbps extra downloadsnelheid (tot maximaal 400 Mbps). Nog sneller streamen, downloaden en gamen dus!</p>";
            } //$combi == 2
            elseif ($combi == 1) {
                $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. U ontvangt daarom elke maand de volgende gratis extra’s:</p><p>
    -   Dubbele data en Onbeperkt bellen met je mobiel<br />
    -   € 5,– korting op uw DELTA Internet factuur<br />
    -   25 Mbps extra downloadsnelheid (tot maximaal 400Mbps).<br />
    -   Een extra TV-pakket boordevol sport-, muziek-, entertainment-, en jeugdzenders<br /></p>";
            } //$combi == 1
            elseif ($combi == 0) {
                $com = "<p>We zijn erg blij met u als klant. Daarom krijgt u als welkomstcadeau een halfjaar € 5,– maandelijkse korting op uw mobiele abonnement. U hoeft daar verder niets voor te doen. </p>";
            } //$combi == 0
            $body = str_replace('{$combi}', $com, $body);
        }
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('order_accepted', $client->language, $client->companyid));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'order_accepted')) {
            log_message('error', 'Template order_accepted is disabled sending aborted');
            return true;
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('order_accepted', $client->language, $client->companyid),
                    'message' => $body
                ));
            }
        }
    }
    public function changeServicesMonth()
    {
        $this->db->where('date_commit', date('Y-m-d'));
        $q = $this->db->get('a_subscription_changes');
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                print_r($row);
            }
        }
    }
    public function send_porting($id, $template)
    {
        if (!$id) {
            return false;
        }
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        log_message("error", print_r($mobile->details, true));
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->config->set_item('language', $client->language);
        $this->lang->load('admin');
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                ////'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent($template, $client->language, $client->companyid);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain($this->data['setting']->base_url), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
        $body = str_replace('{$msisdn}', '0' . substr($mobile->details->msisdn, 2), $body);
        $send = true;
        if ($template == "portin_accepted") {
            if ($mobile->details->date_ported) {
                if (strpos($mobile->details->date_ported, "0000-00-00")) {
                    log_message("error", "Ported date still 0000-00-00, notification aborted".$mobile->details->msisdn);
                    $this->Admin_model->queue_send_email($id);
                    $send = false;
                } else {
                    $this->Admin_model->remove_queue_send_email($id);
                    $send = true;
                }
            } else {
                log_message("error", "Ported date still empty, notification aborted for ".$mobile->details->msisdn);
                $this->Admin_model->queue_send_email($id);
                $send = false;
            }
            $body = str_replace('{$porting_date}', date("d-m-Y", strtotime(str_replace(' 00:00:00', '', $mobile->details->date_ported))), $body);
        } elseif ($template == "portin_rejected") {
            if ($mobile->details->porting_remark) {
                $send = true;
            } else {
                $this->Admin_model->queue_send_email($id);
                //  mail('mail@simson.one','Portin Rejected but portin porting_remark was empty',print_r($mobile->details, true));
                $send = false;
            }
            $body = str_replace('{$reject_reason}', lang($mobile->details->porting_remark), $body);
        }


        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        // $this->email->bcc('mail@simson.one');
        $this->email->subject(getSubject($template, $client->language, $client->companyid));
        $subject = str_replace('{$name}', format_name($client), $subject);
        $subject = str_replace('{$base_url}', url_to_domain($this->data['setting']->base_url), $subject);
        $subject = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $subject);
        $subject = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $subject);
        $subject = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $subject);
        $subject = str_replace('{$msisdn}', '0' . substr($mobile->details->msisdn, 2), $subject);
        //$this->email->subject(lang("Your New Password"));
        if ($send) {
            $this->email->message($body);
            if (!isTemplateActive($client->companyid, $template)) {
                log_message('error', 'Template ' . $template . ' is disabled sending aborted');
            } else {
                if ($this->email->send()) {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email,
                        'subject' => getSubject($template, $client->language, $client->companyid),
                        'message' => $body
                    ));
                }
            }
        }

        log_message('error', "Sending email: ".$template ." status => ".$send);
        return $send;
    }


    public function sendBackupdatabase($file, $bp)
    {
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'html';
        $config['charset']  = 'utf-8';
        $config['wordwrap'] = true;
        $body               = "<p>Hello</p><p>Database backup has been successfully executed:</p>Filename: " . $bp['name'] . "<br>Start: " . $bp['start'] . "<br>End: " . $bp['end'] . "<p>Please find the file in this email attachment</p>ExoCrm";
        $this->email->clear(true);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to('simson.parlindungan@artilium.com');
        $this->email->subject('Backup MVO Portal' . " Backup Report");
        $this->email->message($body);
        $this->email->attach($file, 'attachment', $bp['name']);
        if ($this->email->send()) {
            echo "Backupfile " . $bp['name'] . " has been sent successfully\n";
        } else {
            echo print_r($this->email->print_debugger());
        }
    }
    public function backup()
    {
        echo "Backup database \n";
        $this->load->dbutil();
        $time         = date('YmdHis');
        $bkp['start'] = date('d/m/Y H:i:s');
        $backup       = $this->dbutil->backup();
        write_file(DOC_PATH . 'backups/backup_' . $time . '.gz', $backup);
        $bkp['end']  = date('d/m/Y H:i:s');
        $bkp['name'] = 'backup_' . $time . '.gz';
        echo "Database has been Backuped to " . DOC_PATH . "backups/backup_" . $time . ".gz\n";
        if (file_exists(DOC_PATH . 'backups/backup_' . $time . '.gz')) {
            $this->sendBackupdatabase(DOC_PATH . 'backups/backup_' . $time . '.gz', $bkp);
        }
    }
    public function sepafix()
    {
        $this->load->library('magebo', array(
            'companyid' => '53'
        ));
        $this->db = $this->load->database('magebo', true);
        $q        = $this->db->query("select * from tblAddress where iCompanyNbr = 53 AND iAddressNbr not in ( select iAddressNbr from tblAddressData where iTypeNbr = 2102 )");
        foreach ($q->result() as $row) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $client = getClientDetailidbyMagebo($row->iAddressNbr);
            if ($client) {
                if (!empty($client->iban) && !empty($client->bic)) {
                    //$res = $this->magebo->addMageboSEPA($client->iban, $client->bic, $row->iAddressNbr);
                    //print_r($res);
                } else {
                    print_r($client);
                    echo "error: " . $client->id . "\n";
                }
                unset($client);
            }
        }
    }
    public function fix_promo()
    {
        $q = $this->db->query("SELECT * FROM `a_services` where status in ('Active','Pending','New')  and promocode is null");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
            }
        }
    }
    public function process_camt53_delta($companyid)
    {
        $setting = globofix($companyid);
        shell_exec('sh /usr/local/bin/download_delta_files');
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->load->model('Cron_model');
        $dir  = $setting->DOC_PATH . $companyid . '/camt/';
        $list = scandir($dir);
        foreach ($list as $file) {
            if (substr(trim($file), -4) == ".xml") {
                if (!file_exists($setting->DOC_PATH . $companyid . '/camt/done/' . $file)) {
                    $id = $this->Cron_model->InsertSepaFiles($companyid, array(
                        'filename' => trim($file),
                        'date_received' => date('Y-m-d H:i:s'),
                        'companyid' => $companyid,
                        'status' => 'Done'
                    ));
                    if ($id) {
                        echo "processing file " . $file . "\n";
                        $shel = "/usr/bin/xsltproc /home/mvno/documents/53/camt/camt053-xsl/camt2csv.xsl /home/mvno/documents/" . $companyid . "/camt/" . trim($file) . " >  /home/mvno/documents/" . $companyid . "/camt/done/" . trim($file) . ".csv";
                        echo $shel . "\n";
                        shell_exec($shel);
                        shell_exec("ssh -i /home/mvno/.ssh/delta.key sftpArtilium-P@sftp.delta.nl 'mv ~/VanDeltaNaarArta/CAMT053/" . $file . "  ~/VanDeltaNaarArta/CAMT053/Verwerkt/'");
                        echo rename($setting->DOC_PATH . $companyid . '/camt/' . $file, $setting->DOC_PATH . $companyid . '/camt/done/' . $file);
                        $contents = file($setting->DOC_PATH . $companyid . '/camt/done/' . $file . '.csv');
                        print_r($contents);
                        foreach ($contents as $csv) {
                            $data = explode('|', $csv);
                            if (substr(trim($data[12]), 0, 4) == "SEPA") {
                                $name_owner = $data[15];
                            } else {
                                $name_owner = $data[5];
                            }

                            $array = array(
                                'fileid' => $id,
                                'date_transaction' => $data[0],
                                'companyid' => $companyid,
                                'amount' => $data[9],
                                'iban' => $data[3],
                                'end2endid' => $data[4],
                                'name_owner' => $name_owner,
                                'mandateid' => $data[12],
                                'message' => $data[4],
                                'status' => 'Pending',
                                'debitcredit' => substr($data[10], 0, 1),
                                'transactionissuer' => $data['13'],
                                'transactioncode' => $data['14'],
                                'return_code' => $data['11'],
                                'return_extra' => null
                            );
                            if ($data[1]) {
                                $array['BatchPaymentId'] = $data[1];
                            }

                            $this->db->insert('a_sepa_items', $array);
                            echo date('Y-m-d H:i:s') . "        Processing " . $csv . "\n";
                        }
                    }
                } else {
                    echo "Skipping file: " . $file . " its already processed before\n";
                }
            }
        }
    }
    public function test_camt($companyid, $file)
    {
        echo $companyid . " " . $file . "\n";
        $setting  = globofix($companyid);
        $contents = file($setting->DOC_PATH . $companyid . '/camt/done/' . $file . '.csv');
        echo $contents;
        print_r($contents);
        foreach ($contents as $csv) {
            $data = explode('|', $csv);
            if (substr(trim($data[12]), 0, 4) == "SEPA") {
                $name_owner = $data[15];
            } else {
                $name_owner = $data[5];
            }
            if (!$data[1]) {
                $bp = 'NULL';
            } else {
                $bp = $data[1];
            }
            $array = array(
                'fileid' => 432,
                'date_transaction' => $data[0],
                'companyid' => $companyid,
                'amount' => $data[9],
                'iban' => $data[3],
                'end2endid' => $data[4],
                'name_owner' => $name_owner,
                'mandateid' => $data[12],
                'message' => $data[4],
                'status' => 'Pending',
                'BatchPaymentId' => $bp,
                'debitcredit' => substr($data[10], 0, 1),
                'transactionissuer' => $data['13'],
                'transactioncode' => $data['14'],
                'return_code' => $data['11'],
                'return_extra' => null
            );
            $this->db->insert('a_sepa_items', $array);
            echo date('Y-m-d H:i:s') . "        Processing " . $csv . "\n";
        }
    }
    public function process_camt53_caiway($companyid)
    {
        try {
            $setting = globofix($companyid);
            shell_exec('/usr/local/bin/download_caiway_files');
            $setting = globofix($companyid);
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            $this->load->model('Cron_model');
            $dir  = $setting->DOC_PATH . $companyid . '/camt/';
            $list = scandir($dir);
            foreach ($list as $file) {
                if (substr(trim($file), -4) == ".xml") {
                    if (!file_exists($setting->DOC_PATH . $companyid . '/camt/done/' . $file)) {
                        $id = $this->Cron_model->InsertSepaFiles($companyid, array(
                            'filename' => trim($file),
                            'date_received' => date('Y-m-d H:i:s'),
                            'companyid' => $companyid,
                            'status' => 'Done'
                        ));
                        if ($id) {
                            echo "processing file " . $file . "\n";
                            $shel = "/usr/bin/xsltproc /home/mvno/documents/53/camt/camt053-xsl/camt2csv.xsl /home/mvno/documents/" . $companyid . "/camt/" . trim($file) . " >  /home/mvno/documents/" . $companyid . "/camt/done/" . trim($file) . ".csv";
                            echo $shel . "\n";
                            shell_exec($shel);
                            rename($setting->DOC_PATH . $companyid . '/camt/' . $file, $setting->DOC_PATH . $companyid . '/camt/done/' . $file);
                            $contents = file($setting->DOC_PATH . $companyid . '/camt/done/' . $file . '.csv');
                            print_r($contents);
                            foreach ($contents as $csv) {
                                $data = explode('|', $csv);
                                if (substr(trim($data[12]), 0, 4) == "SEPA") {
                                    $name_owner = $data[15];
                                    $iban       = $data[16];
                                } else {
                                    $name_owner = $data[5];
                                    $iban       = $data[3];
                                }
                                $array = array(
                                    'fileid' => $id,
                                    'date_transaction' => $data[0],
                                    'companyid' => $companyid,
                                    'amount' => $data[9],
                                    'iban' => $iban,
                                    'end2endid' => $data[4],
                                    'name_owner' => $name_owner,
                                    'mandateid' => $data[12],
                                    'message' => $data[4],
                                    'status' => 'Pending',
                                    'debitcredit' => substr($data[10], 0, 1),
                                    'transactionissuer' => $data['13'],
                                    'transactioncode' => $data['14'],
                                    'return_code' => $data['11'],
                                    'return_extra' => null
                                );
                                if ($data[1]) {
                                    $array['BatchPaymentId'] = $data[1];
                                }
                                $this->db->insert('a_sepa_items', $array);
                                echo date('Y-m-d H:i:s') . "        Processing " . $csv . "\n";
                            }
                        }
                    } else {
                        echo "Skipping file: " . $file . " its already processed before\n";
                    }
                }
                $this->moveCaiwayFile($file);
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception process_camt53 ", "Message: " . $e->getMessage());
        }
    }
    public function moveCaiwayFile($filename)
    {
        $connection = ssh2_connect('ftp-sftp1.caiw.net', 2222);
        ssh2_auth_password($connection, 'Artilium', 'Hyli9E');
        $sftp = ssh2_sftp($connection);
        ssh2_sftp_rename($sftp, '/VanCaiwayNaarArta/CAMT053/' . $filename, '/VanCaiwayNaarArta/CAMT053/Verwerkt/' . $filename);
    }
    public function send_Reject_Report($companyid)
    {
        error_reporting(1);
        echo "starting.\n";
        $this->load->model('Admin_model');
        try {
            $q = $this->db->query("SELECT * FROM `a_sepa_items` where mandateid like 'SEPA%' and amount < '0' and return_extra != 'sent' order by date_transaction desc");
            if ($q->num_rows() > 0) {
                $table = "Dear Team,<br>
        <br />
        There are " . $q->num_rows() . " Directdebit rejection found in this report<br /><br />
        You can find the below report of directdebit rejection from your customer:
        <br />
        <table colspan='1' border='1'>
        <tr>
        <th>Date</th>
        <th>ClientID</th>
        <th>InvoiceNum</th>
        <th>Amount</th>
        </tr>
        <tbody>
        ";
                foreach ($q->result() as $row) {
                    $this->db->query("update a_sepa_items set return_extra='sent' where id=?", array(
                        $row->id
                    ));
                    $table .= "<tr>
                <td>" . $row->date_transaction . "</td>
                <td>" . $this->Admin_model->getMvnoIdbyInvoice($row->message) . "</td>
                <td>" . $row->message . "</td>
                <td>" . $row->amount . "</td>
                </tr>";
                }
                $this->data['setting'] = globofix($companyid);
                $sepa_email            = explode('|', $this->data['setting']->sepa_email_notification);
                $table .= "</table><br />Kind Regard<br/>United Telecom";
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        ////'starttls' => true,
                        'wordwrap' => true
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset']  = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($sepa_email);
                //$this->email->bcc('mail@simson.one');
                $this->email->subject("Directdebit Sepa Reject Report");
                //$this->email->subject(lang("Your New Password"));
                $this->email->message($table);
                if ($this->email->send()) {
                    //logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => "Directdebit Sepa Reject Report", $client->language, $client->companyid), 'message' => $body));
                }
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception RejectSepa ", "Message: " . $e->getMessage());
        }
    }
    public function IncassoBuro($companyid, $date = false)
    {
        error_reporting(1);
        try {
            $send_zip = false;
            $this->load->model('Admin_model');
            $this->load->library('magebo', array(
                'companyid' => $companyid
            ));
            if ($date) {
                $date_reminder3 = date('Y-m-d', strtotime($date . ' -14 days'));
            } else {
                $date_reminder3 = date('Y-m-d', strtotime('-14 days'));
            }
            $date = $date_reminder3;
            log_message("error", $date_reminder3 . "\n");

            $setting = globofix($companyid);
            if (!file_exists($setting->DOC_PATH . $companyid . '/reminders/incaso/' . $date)) {
                echo "Creating folder." . $setting->DOC_PATH . $companyid . '/reminders/incaso/' . $date . "\n";
                mkdir($setting->DOC_PATH . $companyid . '/reminders/incaso/' . $date);
                if (!file_exists($setting->DOC_PATH . $companyid . '/reminders/incaso/archive')) {
                    mkdir($setting->DOC_PATH . $companyid . '/reminders/incaso/archive');
                }
            }
            log_message("error", "Running for date: " . $date_reminder3 . "\n");
            $reminders = $this->Admin_model->getReminder3($companyid, $date_reminder3);
            if ($reminders) {
                foreach ($reminders as $rem) {
                    if (!skipreminder($rem->invoicenum)) {
                        $inv = $this->magebo->isInvoiceUnPaid($rem->invoicenum);
                        if ($inv) {
                            log_message("error", date('Y-m-d H:i:s') . " " . $inv->iAddressNbr . " " . $rem->invoicenum . " UNPAID\n");
                            $client = $this->Admin_model->getClient(getWhmcsid($inv->iAddressNbr));
                            $this->Print_Reminder($client->id, $companyid, $date, $rem->invoicenum);
                        } else {
                            log_message("error", date('Y-m-d H:i:s') . " " . $rem->invoicenum . " PAID\n");
                        }
                    }
                }
                $send_zip = true;
            }
            if ($send_zip) {
                log_message("error", "Zipping Files\n");
                $setting = globofix($companyid);
                $path    = $setting->DOC_PATH . $companyid . '/reminders/incaso/' . $date;
                log_message("error", $path . "\n");
                shell_exec('pdftk ' . $path . '/*.pdf cat output ' . $path . '/combined_reminder.pdf');
                shell_exec('zip -j  ' . $setting->DOC_PATH . $companyid . '/reminders/incaso_reminder_' . $date . '.zip ' . $path . '/*.pdf');
                echo $this->send_Reminder3Pdf($companyid, 'incaso_reminder_' . $date . '.zip ', 1);
                shell_exec('mv ' . $path . ' ' . $setting->DOC_PATH . $companyid . '/reminders/incaso/archive');
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception RejectSepa ", "Message: " . $e->getMessage());
        }
    }
    public function textme($companyid, $date)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $ss = $this->magebo->getUnpaidInvoicesByRejection($date);
        print_r($ss);
    }
    public function ReminderDelta($date = false)
    {
        try {
            foreach (array(
                '53',
                '56'
            ) as $companyid) {
                $r1 = 0;
                $r2 = 0;
                $r3 = 0;
                log_message("error", date('Y-m-d H:i:s') . "      Running reminders for" . $companyid . "\n");
                $send_zip              = false;
                $this->data['setting'] = globofix($companyid);
                $this->load->model('Admin_model');
                $this->load->library('magebo', array(
                    'companyid' => $companyid
                ));
                $this->load->library('artilium', array(
                    'companyid' => $companyid
                ));
                if (!$date) {
                    $date = date('Y-m-d');
                }
                $time      = date('His');
                $reminders = $this->magebo->getUnpaidInvoicesByRejection($date);
                log_message('error', print_r($reminders, 1));
                if ($reminders) {
                    foreach ($reminders as $key => $reminder) {
                        if (count($reminder) > 0) {
                            foreach ($reminder as $inv) {
                                if (!skipreminder($inv->iInvoiceNbr)) {
                                    log_message('error', print_r($inv, true));
                                    $client = $this->Admin_model->getClient(getWhmcsid($inv->iAddressNbr));

                                    switch ($inv->reminder_type) {

                                        case "reminder_1":
                                        $r1 = $r1+1;
                                        $this->sendInvoice_Reminder($companyid, $client, $inv, $inv->reminder_type);
                                        if ($this->data['setting']->enable_sms == "1" && $this->data['setting']->sms_content_reminder1 != "") {
                                            $this->load->library('sms', array(
                                                'username' => $this->data['setting']->sms_username,
                                                'password' => $this->data['setting']->sms_password,
                                                'companyid' => $companyid
                                            ));
                                            $sms_res = $this->sms->send_message_bulksms(array(
                                                array(
                                                    'from' => $this->data['setting']->sms_senderid,
                                                    'to' => '+' . preg_replace('/\D/', '', $client->phonenumber),
                                                    'body' => replace_body_sms($this->data['setting']->sms_content_reminder1, array(
                                                        'iInvoiceNbr' => $inv->iInvoiceNbr,
                                                        'mInvoiceAmount' => str_replace('.', ',', number_format($inv->mInvoiceAmount, 2)),
                                                        'name' => format_name($client)
                                                    ))
                                                )
                                            ));
                                        }
                                        break;
                                        case "reminder_2":
                                        $r2 = $r2+1;
                                        if ($this->data['setting']->enable_sms == "1" && $this->data['setting']->sms_content_reminder2 != "") {
                                            $this->load->library('sms', array(
                                                'username' => $this->data['setting']->sms_username,
                                                'password' => $this->data['setting']->sms_password,
                                                'companyid' => $companyid
                                            ));
                                            $sms_res = $this->sms->send_message_bulksms(array(
                                                array(
                                                    'from' => $this->data['setting']->sms_senderid,
                                                    'to' => '+' . preg_replace('/\D/', '', $client->phonenumber),
                                                    'body' => replace_body_sms($this->data['setting']->sms_content_reminder2, array(
                                                        'iInvoiceNbr' => $inv->iInvoiceNbr,
                                                        'mInvoiceAmount' => str_replace('.', ',', number_format($inv->mInvoiceAmount, 2)),
                                                        'name' => format_name($client)
                                                    ))
                                                )
                                            ));
                                        }

                                        $this->sendInvoice_Reminder($companyid, $client, $inv, $inv->reminder_type);
                                        break;
                                        case "reminder_3":
                                        $r3 = $r3+1;
                                        $send_zip = true;
                                        $this->Print_Reminder($client->id, $companyid, $date);
                                        $services = $this->Admin_model->getServices($client->id);
                                        if ($services) {
                                            foreach ($services as $serv) {
                                                if (!in_array($serv->status, array(
                                                    'Cancelled',
                                                    'Terminated',
                                                    'Pending'
                                                ))) {
                                                    /*
                                                    print_r($serv);
                                                    $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($serv->sn));
                                                    $this->Admin_model->save_state($serv->id, $pack);
                                                    $this->artilium->BlockOriginating(trim($serv->sn), $pack, '0');
                                                    $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                                    'bar' => 1
                                                    ));
                                                    $this->Admin_model->update_services($serv->id, array(
                                                    'status' => 'Suspended'
                                                    ));
                                                    */
                                                    /*logAdmin(array(
                                                    'companyid' => $client->companyid,
                                                    'userid' => $client->id,
                                                    'serviceid' => $serv->id,
                                                    'user' => 'System',
                                                    'ip' => '127.0.0.1',
                                                    'description' => 'Subscription has been suspended due overdue invoices'
                                                    ));
                                                    */
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                                unset($client);
                            }
                            log_message("error", "Found " . count($reminder) . " need to be reminded\n");
                        }
                    }
                }
                $setting = globofix($companyid);
                $em = explode('|', $setting->reminder_email_notification);
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= 'From: <noreply@united-telecom.be>' . "\r\n";
                $headers .= 'Cc: mail@simson.one' . "\r\n";

                if ($em) {
                    foreach ($em as $m) {
                        mail($m, "Reminder Reports:  ".$setting->companyname, "Hello, This is an automate email notification after reminder process has been executed\n\nDate Run: ".date('Y-m-d H:i:s')."\n\nReminder 1 : ".$r1."\n\nReminder 2 : ".$r2."\n\nReminder 3 : ".$r3."\n\nIf you do not received this on daily base please contact out support to check the process.\n", $headers);
                    }
                }

                $this->Admin_model->updateReminderLog($companyid, array('Reminder1' => $r1, 'Reminder2' =>  $r2, 'Reminder3' =>  $r3));
                $r1 = 0;
                $r2 = 0;
                $r3 = 0;

                if ($send_zip) {
                    $rand = time();
                    log_message("error", "Zipping Files\n");
                    //$setting = globofix($companyid);
                    $path    = $setting->DOC_PATH . $companyid . '/reminders/' . $date;
                    log_message("error", $path . "\n");
                    shell_exec('zip -j  ' . $setting->DOC_PATH . $companyid . '/reminders/reminder_' . $rand . '_' . $date . '.zip ' . $path . '/*.pdf');
                    shell_exec('pdftk ' . $path . '/*.pdf cat output ' . $path . '/combined_reminder.pdf');
                    if (!file_exists($setting->DOC_PATH . $companyid . '/reminders/archive')) {
                        mkdir($setting->DOC_PATH . $companyid . '/reminders/archive');
                    }
                    echo $this->send_Reminder3Pdf($companyid, 'reminder_' . $rand . '_' . $date . '.zip ');
                    shell_exec('mv ' . $setting->DOC_PATH . $companyid . '/reminders/' . $date . ' ' . $setting->DOC_PATH . $companyid . '/reminders/archive');
                }
                unset($this->artilium);
                unset($this->magebo);
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception Sending Reminder ", "Message: " . $e->getMessage());
        }
    }
    public function ReminderCentral($date = false)
    {
        $r1 = 0;
        $r2 = 0;
        $r3 = 0;
        $companyid = 54;
        log_message("error", date('Y-m-d H:i:s') . "      Running reminders for" . $companyid . "\n");
        $this->data['setting'] = globofix($companyid);
        $this->load->model('Admin_model');
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $this->load->library('artilium', array(
            'companyid' => $companyid
        ));
        if (!$date) {
            $date = date('Y-m-d');
        }
        $time      = date('His');
        $reminders = $this->magebo->getUnpaidInvoices($date);
        if ($reminders) {
            foreach ($reminders as $key => $reminder) {
                $send_zip = false;
                if (count($reminder) > 0) {
                    foreach ($reminder as $inv) {
                        if (!skipreminder($inv->iInvoiceNbr)) {
                            //print_r($this->magebo->getInvoiceDetail($inv->iInvoiceNbr));
                            log_message("error", print_r($inv, true));
                            $client      = $this->Admin_model->getClient(getWhmcsid($inv->iAddressNbr));
                            $sms_numbers = getSmsNumbers($client->id);
                            switch ($inv->reminder_type) {
                            case "reminder_1":
                                $r1 = $r1+1;
                                $this->sendInvoice_Reminder($companyid, $client, $inv, $inv->reminder_type);
                                if ($this->data['setting']->enable_sms == "1" && $this->data['setting']->sms_content_reminder1 != "") {
                                    if (getclient_sms_number($client->id)) {
                                        $this->load->library('sms', array(
                                            'username' => $this->data['setting']->sms_username,
                                            'password' => $this->data['setting']->sms_password,
                                            'companyid' => $companyid,
                                            'userid' => $client->id
                                        ));
                                        foreach (getclient_sms_number($client->id) as $num) {
                                            $payment         = $this->magebo->getInvoiceBalance($inv->iInvoiceNbr, $inv->iAddressNbr);
                                            $amount_toremind = $inv->mInvoiceAmount - $payment;
                                            $sms_res         = $this->sms->send_message_bulksms(array(
                                                array(
                                                    'from' => $this->data['setting']->sms_senderid,
                                                    'to' => '+' . preg_replace('/\D/', '', $num),
                                                    'body' => replace_body_sms($this->data['setting']->sms_content_reminder1, array(
                                                        'iInvoiceNbr' => $inv->iInvoiceNbr,
                                                        'mInvoiceAmount' => str_replace('.', ',', number_format($amount_toremind, 2)),
                                                        'name' => format_name($client)
                                                    ))
                                                )
                                            ));
                                        }
                                        unset($this->sms);
                                    }
                                }
                                break;
                                case "reminder_2":
                                if ($this->data['setting']->enable_sms == "1" && $this->data['setting']->sms_content_reminder2 != "") {
                                    if (getclient_sms_number($client->id)) {
                                        $this->load->library('sms', array(
                                            'username' => $this->data['setting']->sms_username,
                                            'password' => $this->data['setting']->sms_password,
                                            'companyid' => $companyid,
                                            'userid' => $client->id
                                        ));
                                        foreach (getclient_sms_number($client->id) as $num) {
                                            $payment         = $this->magebo->getInvoiceBalance($inv->iInvoiceNbr, $inv->iAddressNbr);
                                            $amount_toremind = $inv->mInvoiceAmount - $payment;
                                            $sms_res         = $this->sms->send_message_bulksms(array(
                                                array(
                                                    'from' => $this->data['setting']->sms_senderid,
                                                    'to' => '+' . preg_replace('/\D/', '', $num),
                                                    'body' => replace_body_sms($this->data['setting']->sms_content_reminder2, array(
                                                        'iInvoiceNbr' => $inv->iInvoiceNbr,
                                                        'mInvoiceAmount' => str_replace('.', ',', number_format($amount_toremind, 2)),
                                                        'name' => format_name($client)
                                                    ))
                                                )
                                            ));
                                        }
                                        unset($this->sms);
                                    }
                                }
                                $r2 = $r2+1;
                                $this->sendInvoice_Reminder($companyid, $client, $inv, $inv->reminder_type);
                                //Bar on second reminder
                                $services = $this->Admin_model->getServices($client->id);
                                if ($services) {
                                    foreach ($services as $serv) {
                                        if (!in_array($serv->status, array(
                                            'Cancelled',
                                            'Terminated',
                                            'Pending'
                                        ))) {
                                            log_message("error", print_r($serv, true));
                                            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($serv->sn));
                                            $this->Admin_model->save_state($serv->id, $pack);
                                            $this->artilium->BlockOriginating(trim($serv->sn), $pack, '0');
                                            $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                                'bar' => 1,
                                                'date_modified' => date('Y-m-d H:i:s')
                                            ));
                                            $this->Admin_model->update_services($serv->id, array(
                                                'status' => 'Suspended'
                                            ));
                                            $this->sendmail($serv->id, 'service', 'service_suspended');
                                            logAdmin(array(
                                                'companyid' => $client->companyid,
                                                'userid' => $client->id,
                                                'serviceid' => $serv->id,
                                                'user' => 'System',
                                                'ip' => '127.0.0.1',
                                                'description' => 'Subscription has been suspended due to overdue invoices'
                                            ));
                                        }
                                    }
                                }
                                break;
                                case "reminder_3":
                                $send_zip = true;
                                $r3 = $r3+1;
                                $this->Print_Reminder($client->id, $companyid, $date);
                                $services = $this->Admin_model->getServices($client->id);
                                $this->Admin_model->ChangeClientDunnigProfile($client->id, 'reminder_3');
                                if ($services) {
                                    foreach ($services as $serv) {
                                        if (!in_array($serv->status, array(
                                            'Cancelled',
                                            'Terminated',
                                            'Pending',
                                            'Suspended'
                                        ))) {
                                            log_message("error", print_r($serv, true));
                                            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($serv->sn));
                                            //$this->Admin_model->save_state($serv->id, $pack);
                                            foreach ($pack as $pp) {
                                                $this->artilium->UpdatePackageOptionsForSN(trim($serv->sn), $pp->PackageDefinitionId, 0);
                                            }
                                            $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                                'bar' => 1,
                                                'date_modified' => date('Y-m-d H:i:s')
                                            ));
                                            $this->Admin_model->update_services($serv->id, array(
                                                'status' => 'Suspended'
                                            ));
                                            logAdmin(array(
                                                'companyid' => $client->companyid,
                                                'userid' => $client->id,
                                                'serviceid' => $serv->id,
                                                'user' => 'System',
                                                'ip' => '127.0.0.1',
                                                'description' => 'Subscription has been suspended due to overdue invoices'
                                            ));
                                            // $this->artilium->BlockOriginating(trim($serv->sn), $pack, '0');
                                        }
                                    }
                                }
                                break;
                            }
                        }
                        unset($client);
                    }
                    log_message("error", "Company ".$companyid." = Found " . count($reminder) . " Reminders has been sent\n");
                }
            }
        }
        $setting = globofix($companyid);
        $em = explode('|', $setting->reminder_email_notification);
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= 'From: <noreply@united-telecom.be>' . "\r\n";
        $headers .= 'Bcc: mail@simson.one' . "\r\n";

        if ($em) {
            foreach ($em as $m) {
                mail($m, "Reminder Reports: Central Services", "Hello, This is an automate email notification after reminder process has been executed\n\nDate Run: ".date('Y-m-d H:i:s')."\n\nReminder 1 : ".$r1."\n\nReminder 2 : ".$r2."\n\nReminder 3 : ".$r3."\n\nIf you do not received this on daily base please contact out support to check the process.\n", $headers);
            }
        }
        $this->Admin_model->updateReminderLog($companyid, array('Reminder1' => $r1, 'Reminder2' =>  $r2, 'Reminder3' =>  $r3));
        if ($send_zip) {
            $rand = time();
            log_message("error", "Zipping Files\n");

            $path    = $setting->DOC_PATH . $companyid . '/reminders/' . $date;
            log_message("error", $path . "\n");
            shell_exec('zip -j  ' . $setting->DOC_PATH . $companyid . '/reminders/reminder_' . $rand . '_' . $date . '.zip ' . $path . '/*.pdf');
            shell_exec('pdftk ' . $path . '/*.pdf cat output ' . $path . '/combined_reminder.pdf');
            if (!file_exists($setting->DOC_PATH . $companyid . '/reminders/archive')) {
                mkdir($setting->DOC_PATH . $companyid . '/reminders/archive');
            }
            echo $this->send_Reminder3Pdf($companyid, 'reminder_' . $rand . '_' . $date . '.zip ');
            shell_exec('mv ' . $setting->DOC_PATH . $companyid . '/reminders/' . $date . ' ' . $setting->DOC_PATH . $companyid . '/reminders/archive');
        }
        unset($this->artilium);
        unset($this->magebo);
    }
    public function sendmail($cid, $type, $template, $amount = false)
    {
        $this->load->model('Admin_model');
        if ($type == "service") {
            $mobile                = $this->Admin_model->getServiceCli($cid);
            $client                = $this->Admin_model->getClient($mobile->userid);
            $brand                 = $this->Admin_model->getBrandPdfFooter($mobile->gid);
            $this->data['setting'] = globofix($client->companyid);
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    ////'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent($template, $client->language, $client->companyid);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
            $body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
            $body = str_replace('{$address1}', $client->address1, $body);
            $body = str_replace('{$housenumber}', $client->housenumber, $body);
            $body = str_replace('{$alphabet}', $client->alphabet, $body);
            $body = str_replace('{$city}', $client->city, $body);
            $body = str_replace('{$postcode}', $client->postcode, $body);
            $body = str_replace('{$country}', getCountryName($client->country), $body);
            $body = str_replace('{$client_companyname}', $client->companyname, $body);
            $body = str_replace('{$Companyname}', $brand->name, $body);
            if ($template == "service_suspended") {
            }
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            //$this->email->bcc('mail@simson.one');
            $this->email->subject(getSubject($template, $client->language, $client->companyid));
            $this->email->message($body);
            if (empty($body)) {
                return false;
            }
            if (empty($subject)) {
                return false;
            }
            if (!isTemplateActive($client->companyid, $template)) {
                log_message('error', 'Template ' . $template . ' is disabled sending aborted');
                return true;
            }
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject($template, $client->language, $client->companyid),
                    'message' => $body
                ));
                return true;
            // redirect('admin/subscription/detail/'.$cid);
                //$this->session->set_flashdata('success','Email has been sent');
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject($template, $client->language, $client->companyid),
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
                return false;
                //$this->session->set_flashdata('error','Email was not sent');
            }
        }
    }
    public function update_maps()
    {
        $this->load->model('Admin_model');
        $q = $this->db->query("select companyid,id,concat(address1,'+',city,'+',postcode,'+',country) as address from a_clients where lat is  null and companyid not in(2,33)  order by companyid,id desc");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                if (!empty($row->address1) || !empty($row->city)) {
                    echo $row->companyid . "     " . $row->id . "     processing " . str_replace(' ', '+', $row->address) . "\n";
                    $d = $this->Admin_model->geolocation(str_replace(' ', '+', $row->address));
                    print_r($d);
                    if ($d) {
                        $this->db->where('id', $row->id);
                        $this->db->update('a_clients', $d);
                    }
                    sleep(3);
                }
            }
        }
    }
    public function send_reminder3($companyid, $date)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $reminders = $this->magebo->getUnpaidInvoices($date);
        //print_r($reminders);
        if (!empty($reminders['reminder_3'])) {
            foreach ($reminders['reminder_3'] as $rem) {
                $client = $this->Admin_model->getClient(getWhmcsid($rem->iAddressNbr));
                print_r($client);
                // $client = $this->Admin_model->getClient($userid);
                $invoices = $this->magebo->getDueInvoicesReminder3($rem->iAddressNbr);
                print_r($invoices);
                $this->sendInvoice_Reminder3($companyid, $client, $invoices, 'reminder_3', false);
            }
        }
        // print_r($invoices);
    }
    public function sendInvoice_Reminder3($companyid, $client, $invoices, $type, $file = false)
    {
        log_message('error', "Sending ".$type." ".$companyid);
        $this->load->model('Admin_model');
        $this->load->library('magebo', array(
            'companyid' => $client->companyid
        ));
        error_reporting(1);

        $this->data['setting'] = globofix($companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                ////'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body     = getMailContent($type, $client->language, $client->companyid);
        $body     = str_replace('{$name}', format_name($client), $body);
        $body     = str_replace('{$clientid}', $client->mvno_id, $body);
        $body     = str_replace('{$address1}', $client->address1, $body);
        $body     = str_replace('{$housenumber}', $client->housenumber, $body);
        $body     = str_replace('{$alphabet}', $client->alphabet, $body);
        $body     = str_replace('{$postcode}', $client->postcode, $body);
        $body     = str_replace('{$email}', $client->email, $body);
        $body     = str_replace('{$city}', $client->city, $body);
        $body     = str_replace('{$country}', getCountryNameLang($client->country, $client->language), $body);
        $body     = str_replace('{$language}', $client->language, $body);
        $body     = str_replace('{$phonenumber}', $client->phonenumber, $body);
        $body     = str_replace('{$Today}', date('d-m-Y'), $body);
        $invoices = $this->magebo->getDueInvoicesReminder3($client->mageboid);
        if ($client->language == "dutch") {
            $table = '
 <table border="1" cellpadding="2" cellspacing="1" style="margin-left: 21pt;" width="90%">
 <thead>
 <tr bgcolor="#D4D4D4" color="#fff" >

     <th>' . lang('Factuurnr.') . '</th>
     <th>' . lang('Datum') . '</th>
     <th>' . lang('Vervaldatum') . '</th>
     <th>' . lang('Bedrag') . '</th>
 </tr>
 </thead>
     <tbody>';
        } else {
            $table = '<p style="margin-left: 21pt;">
 <table border="1" cellpadding="2" cellspacing="1" style="margin-left: 21pt;" width="90%">
 <thead>
 <tr bgcolor="#D4D4D4" color="#fff" >

     <th>' . lang('Invoice.') . '</th>
     <th>' . lang('Date') . '</th>
     <th>' . lang('Duedate') . '</th>
     <th>' . lang('Amount') . '</th>
 </tr>
 </thead>
     <tbody>';
        }
        // ---------------------------------------------------------
        foreach ($invoices as $inv) {
            $payment         = $this->magebo->getInvoiceBalance($inv->iInvoiceNbr, $inv->iAddressNbr);
            $amount_toremind = $inv->mInvoiceAmount - $payment;
            $table .= ' <tr>

        <td> ' . $inv->iInvoiceNbr . ' </td>
        <td> ' . date("d-m-Y", strtotime(str_replace(' 00:00:00.000', '', $inv->dInvoiceDate))) . ' </td>
        <td> ' . date("d-m-Y", strtotime(str_replace(' 00:00:00.000', '', $inv->dInvoiceDueDate))) . '</td>
        <td> €' . number_format($amount_toremind, 2) . '</td>
    </tr>';
        }
        $table .= '
     </tbody>
 </table>
';
        $body = str_replace('{$table}', $table, $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $client_email = getInvoiceEmail($client->id);
        $this->email->to($client_email);

        $subject = getSubject($type, $client->language, $companyid);
        $subject = str_replace('{$clientid}', $client->mvno_id, $subject);
        $this->email->subject($subject);
        $this->email->message($body);
        if ($companyid == "54") {
            $file = true;
            $cdr  = true;
        } else {
            $cdr  = false;
            $file = false;
        }
        if ($file) {
            $this->email->attach($file);
        }
        if (!isTemplateActive($client->companyid, $type)) {
            log_message('error', 'Template ' . $type . ' is disabled sending aborted');
        } else {
            if ($this->email->send()) {
                /*
                logEmailOut(array(
                'userid' => $client->id,
                'to' => $client->email,
                'companyid' => $client->companyid,
                'subject' => $subject,
                'message' => $body
                ));

                $this->Admin_model->insertReminderLog(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'userid' => $client->id,
                'type' => ucfirst(str_replace('_', '', $type)),
                'companyid' => $companyid,
                'amount' => $invoice->mInvoiceAmount
                ));
                */
                echo json_encode(array(
                    'result' => true
                ));
            } else {
                echo json_encode(array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                ));
            }
        }
    }
    public function Print_Reminder($id, $companyid, $date, $reminder4 = false, $invoiceid = false)
    {
        if ($reminder4) {
            $reminder_types = 'Reminder4';
        } else {
            $reminder_types = 'Reminder3';
        }
        log_message("error", $reminder_types . "\n");
        $this->data['setting'] = globofix($companyid);
        $this->load->model('Admin_model');
        $this->load->library('spdf');
        $client = $this->Admin_model->getClient($id);
        $this->config->set_item('language', $client->language);
        $this->lang->load('admin');
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        if ($invoiceid) {
            $invoices = $this->magebo->getDueInvoices($client->mageboid, $invoiceid);
        } else {
            $invoices = $this->magebo->getDueInvoicesReminder3($client->mageboid);
        }
        if ($companyid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
        }
        if (!$invoices) {
            log_message("error", "No due Invoices\n");
        } else {
            $html = getPdfTemplate('Reminder', $client->companyid, $client->language);
            $body = $html->body;
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$clientid}', $client->mvno_id, $body);
            $body = str_replace('{$address1}', $client->address1, $body);
            $body = str_replace('{$housenumber}', $client->housenumber, $body);
            $body = str_replace('{$alphabet}', $client->alphabet, $body);
            $body = str_replace('{$postcode}', $client->postcode, $body);
            $body = str_replace('{$email}', $client->email, $body);
            $body = str_replace('{$city}', $client->city, $body);
            $body = str_replace('{$country}', getCountryNameLang($client->country, $client->language), $body);
            $body = str_replace('{$language}', $client->language, $body);
            $body = str_replace('{$phonenumber}', $client->phonenumber, $body);
            $body = str_replace('{$Today}', date('d-m-Y'), $body);
            $pdf  = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Simson Lai');
            $pdf->SetTitle('Reminder Client ' . $id);
            //$pdf->setInvoicenumber($id);
            $pdf->SetSubject('Reminder Invoice ' . $id);
            $pdf->SetKeywords('TCPDF, Invoice, Cdr, ' . $id);
            $pdf->AddPage('P', 'A4');
            $pdf->Image($brand->brand_logo, 10, 10, 60);
            //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
            $pdf->ln(10);
            $pdf->SetFont($html->font, '', $html->font_size);
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
            // set header and footer fonts
            $pdf->setHeaderFont(array(
                PDF_FONT_NAME_MAIN,
                '',
                PDF_FONT_SIZE_MAIN
            ));
            $pdf->setFooterFont(array(
                PDF_FONT_NAME_DATA,
                '',
                PDF_FONT_SIZE_DATA
            ));
            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->setFoot($brand->brand_footer);
            $pdf->setFooterFont('droidsans');
            // $pdf->setPrintFooter(false);
            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "15");
            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            if ($client->language == "dutch") {
                $table = '
 <table border="1" cellpadding="2" cellspacing="1" style="margin-left: 21pt;">
 <thead>
 <tr bgcolor="#111" color="#fff" >

     <th>' . lang('Factuurnr.') . '</th>
     <th>' . lang('Datum') . '</th>
     <th>' . lang('Vervaldatum') . '</th>
     <th>' . lang('Bedrag') . '</th>
 </tr>
 </thead>
     <tbody>';
            } else {
                $table = '<p style="margin-left: 21pt;">
 <table border="1" cellpadding="2" cellspacing="1" style="margin-left: 21pt;">
 <thead>
 <tr bgcolor="#111" color="#fff" >

     <th>' . lang('Invoice.') . '</th>
     <th>' . lang('Date') . '</th>
     <th>' . lang('Duedate') . '</th>
     <th>' . lang('Amount') . '</th>
 </tr>
 </thead>
     <tbody>';
            }
            // ---------------------------------------------------------
            foreach ($invoices as $invoice) {
                $payment         = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
                $amount_toremind = $invoice->mInvoiceAmount - $payment;
                $this->Admin_model->insertReminderLog(array(
                    'invoicenum' => $invoice->iInvoiceNbr,
                    'userid' => $client->id,
                    'type' => $reminder_types,
                    'companyid' => $companyid,
                    'amount' => $amount_toremind
                ));
                $table .= ' <tr>

        <td> ' . $invoice->iInvoiceNbr . ' </td>
        <td> ' . date("d-m-Y", strtotime(str_replace(' 00:00:00.000', '', $invoice->dInvoiceDate))) . ' </td>
        <td> ' . date("d-m-Y", strtotime(str_replace(' 00:00:00.000', '', $invoice->dInvoiceDueDate))) . '</td>
        <td> €' . number_format($amount_toremind, 2) . '</td>
    </tr>';
            }
            $table .= '
     </tbody>
 </table>
';
            if ($html) {
                $body = str_replace('{$table}', $table, $body);
                $pdf->writeHTML($body, true, false, true, false, 'L');
                /*  $this->Admin_model->insertReminderLog(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'userid' => $client->id,
                'type' => $reminder_types,
                'companyid' => $companyid,
                'amount' => $invoice->mInvoiceAmount
                ));
                */
                if ($reminder4) {
                    if (!file_exists($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/incaso/')) {
                        mkdir($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/incaso');
                        if (!file_exists($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/incaso/' . $date)) {
                            mkdir($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/incaso/' . $date);
                        }
                    }
                    $pdf->Output($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/incaso/' . $date . '/' . $id . '_' . trim($client->mvno_id) . '.pdf', 'F');
                } else {
                    if (!file_exists($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/' . $date)) {
                        mkdir($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/' . $date);
                    }
                    $pdf->Output($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/' . $date . '/' . $id . '_' . trim($client->mvno_id) . '.pdf', 'F');
                    if ($this->data['setting']->send_reminder3_email == "1") {
                        if ($reminder_types == 'Reminder3') {
                            $this->sendInvoice_Reminder3($companyid, $client, $invoices, 'reminder_3', $this->data['setting']->DOC_PATH . $client->companyid . '/reminders/' . $date . '/' . $id . '_' . trim($client->mvno_id) . '.pdf');
                        }
                    }
                }
                log_message("error", $client->mvno_id . ".pdf Created\n");
            } else {
                log_message("error", "Template not found\n");
            }
        }
    }
    public function send_sms_me($companyid)
    {
        $this->data['setting'] = globofix($companyid);
        $this->load->library('sms', array(
            'username' => $this->data['setting']->sms_username,
            'password' => $this->data['setting']->sms_password,
            'companyid' => $companyid,
            'userid' => '1'
        ));
        $sms_res = $this->sms->send_message_bulksms(array(
            array(
                'from' => '32484889888',
                'to' => '+66617298273',
                'body' => 'test'
            )
        ));
        log_message("error", print_r($sms_res, true));
    }
    public function createInvoiceReminderItems($companyid, $d)
    {
        //print_r($_POST);
        $q = $this->db->query("select * from a_reminder where type=? and companyid=? and date_sent like ?", array(
            'Reminder4',
            $companyid,
            trim($d) . '%'
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $invoices[] = array(
                    'admin' => 'System',
                    'userid' => $row->userid,
                    'companyid' => $companyid,
                    'type' => 'ONETIME',
                    'terms' => 1,
                    'description' => 'Administrative Fee ' . $row->invoicenum,
                    'vat_rate' => '21',
                    'amount' => 25
                );
            }
        }
        foreach ($invoices as $data) {
            $this->load->model('Admin_model');
            $effectiveDate    = date('Y-m-01', strtotime('+1 month'));
            $t                = $data['terms'] - 1;
            $date             = date('Y-m-d', strtotime("+" . $t . " months", strtotime($effectiveDate)));
            $data['end_date'] = $date;
            if ($this->Admin_model->IsAllowedClient($data['userid'], $companyid)) {
                $this->load->library('magebo', array(
                    'companyid' => $companyid
                ));
                $client = $this->Admin_model->getClient($data['userid']);
                if ($data['type'] == "RECUR") {
                    $this->db->insert('a_subscriptions_manual', $data);
                    $terms = $data["terms"];
                    if ($terms <= 1) {
                        //$this->session->set_flashdata('error', 'You choose recurring but the terms is only 1 month, please choose One Time instead');
                        //redirect('admin/client/detail/' . $_POST['userid']);
                    }
                } else {
                    $terms = 1;
                }
                $option   = (object) array(
                    'terms' => $terms,
                    'name' => $data['description'],
                    'recurring_total' => str_replace(',', '.', $data['amount'])
                );
                $contract = date('m-01-Y', strtotime('+1 month'));
                $id       = $this->magebo->addExtraPricing($client, $option, $contract);
                if ($id > 0) {
                    $this->magebo->updatePricingMonth($id, date('n'));
                    log_message("error", $id . " has been added for " . $data['userid'] . "\n");
                } else {
                    die("error");
                }
                //redirect('admin/client/detail/' . $_POST['userid']);
            } else {
                log_message("error", "Action not allowed\n");
                //redirect('admin/client');
            }
        }
    }
    public function send_Reminder3Pdf($companyid, $file, $incaso = false)
    {
        log_message("error", $companyid);
        error_reporting(1);
        $this->load->model('Admin_model');
        $this->data['setting'] = globofix($companyid);
        $email                 = explode('|', $this->data['setting']->sepa_email_notification);
        if ($email) {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
            $this->email->from('noreplay@united-telecom.be');
            //$this->email->to('mail@simson.one');
            //print_r($email);
            $this->email->to($email);
            $subject = 'Reminder 3 PDF : ' . date('Y-m-d');
            if ($incaso) {
                $this->email->subject('Reminder 4 Incasso PDF : ' . date('Y-m-d'));
                $body = 'Beste,<br /><br />
        Hierbij de aanmaningen die wij exact twee weken geleden verstuurd hebben naar onze klanten (de datum in de pdf is dus niet correct, die staat op vandaag). <br />
        De betreffende klanten hebben nog steeds niet betaald, dus wij dragen de klanten nu aan jullie over.
        <br /><br />URL: <a href="' . $this->data['setting']->base_url . 'admin/dashboard/download_reminder/' . $file . '" target="_blank">' . $this->data['setting']->base_url . 'admin/dashboard/download_reminder/' . $file . '</a>
        <br /><br/>Regards,<br>United Telecom';
            } else {
                $this->email->subject('Reminder 3 PDF');
                $body = 'Hello,<br />
        Your Reminder 3 Pdf is in this email attachments, you will need to print and send it to your customer via Post<br /><br />URL: <a href="' . $this->data['setting']->base_url . 'admin/dashboard/download_reminder/' . $file . '" target="_blank">' . $this->data['setting']->base_url . 'admin/dashboard/download_reminder/' . $file . '</a><br /> <br/>Regards,<br>United Telecom';
            }
            //$this->email->body($body);
            $this->email->message($body);
            if ($this->email->send()) {
                log_message("error", "mail sent\n");
                return true;
            } else {
                mail('mail@simson.one', 'Failed report', 'Report reminder 3 failed');
                return false;
            }
        } else {
            mail('mail@simson.one', 'Failed report', 'Report reminder 3 failed because the sepa_email_notification not set for ' . $companyid . ' file :' . $file);
            return false;
        }
    }
    public function gen_webserver()
    {
        $q= $this->db->query("select * from a_mvno where apache=0");
        if ($q->num_rows()>0) {
            foreach ($q->result() as $row) {
                $this->db->query("update a_mvno set apache=1 where id=?", array($row->id));
                log_message('error', 'Found '.url_to_domain(trim($row->portal_url)));
                shell_exec("mv /home/mvno/public_html/application/server/".url_to_domain(trim($row->portal_url)).".conf /etc/apache2/sites-enabled/");
                shell_exec("/root/certbot/certbot-auto  --apache -d ".url_to_domain(trim($row->portal_url))."  -n  --no-redirect");
                shell_exec("service apache2 reload");
            }
        } else {
            log_message('error', 'No new Company need apache config');
        }
    }
    public function reprint_reminder($companyid)
    {
        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from a_reminder where type='Reminder3' and date_sent like '2019-06-19%' and companyid=?", array(
            $companyid
        ));
        foreach ($q->result() as $row) {
            log_message("error", print_r($row, true));
            $client = $this->Admin_model->getClient($row->userid);
            $this->Print_Reminder($client->id, $companyid, '2019-06-19');
        }
    }
    public function sendInvoice_Reminder($companyid, $client, $invoice, $type)
    {
        $this->load->model('Admin_model');
        $this->load->library('magebo', array(
            'companyid' => $client->companyid
        ));
        error_reporting(1);
        $this->Admin_model->ChangeClientDunnigProfile($client->id, $type);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                ////'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body        = getMailContent($type, $client->language, $client->companyid);
        $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
        $payment     = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
        $amount      = $invoice->mInvoiceAmount - $payment;
        $body        = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
        $body        = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
        $body        = str_replace('{$clientid}', $client->mvno_id, $body);
        $body        = str_replace('{$dInvoiceDate}', $invoice->dInvoiceDate, $body);
        $body        = str_replace('{$dInvoiceDueDate}', $invoice->dInvoiceDueDate, $body);
        $body        = str_replace('{$Companyname}', $setting->companyname, $body);
        $body        = str_replace('{$name}', format_name($client), $body);
        // $body = str_replace('{$base_url}', 'mijnmobiel.delta.nl', $body);
        $body        = str_replace('{$cInvoiceReference}', $invoice_ref, $body);
        $this->email->set_newline("\r\n");
        $this->email->from($setting->smtp_sender, $setting->smtp_name);
        $client_email = getInvoiceEmail($client->id);
        $this->email->to($client_email);
        $cc = getContactInvoiceEmail($client->id);
        if ($cc) {
            $this->email->cc($cc);
        }
        $subject = getSubject($type, $client->language, $companyid);
        $subject = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $subject);
        $subject = str_replace('{$Companyname}', $setting->companyname, $subject);
        $this->email->subject($subject);
        $this->email->message($body);


        if (!isTemplateActive($client->companyid, $type)) {
            log_message('error', 'Template ' . $type . ' is disabled sending aborted');
            $this->Admin_model->insertReminderLog(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'userid' => $client->id,
                'type' => ucfirst(str_replace('_', '', $type)),
                'companyid' => $companyid,
                'amount' => $invoice->mInvoiceAmount
            ));
        } else {
            if ($this->email->send()) {
                if ($cc) {
                    foreach ($cc as $em) {
                        logEmailOut(array(
                            'userid' => $client->id,
                            'to' => $em,
                            'subject' => $subject,
                            'message' => $body,
                            'companyid' => $companyid
                        ));
                    }
                }
                logEmailOut(array(
                    'userid' => $client->id,
                    'to' => $client->email,
                    'companyid' => $client->companyid,
                    'subject' => $subject,
                    'message' => $body
                ));
                $this->Admin_model->insertReminderLog(array(
                    'invoicenum' => $invoice->iInvoiceNbr,
                    'userid' => $client->id,
                    'type' => ucfirst(str_replace('_', '', $type)),
                    'companyid' => $companyid,
                    'amount' => $invoice->mInvoiceAmount
                ));
                log_message("error", json_encode(array(
                    'invoicenum' => $invoice->iInvoiceNbr,
                    'result' => true
                )));
            } else {
                log_message("error", json_encode(array(
                    'invoicenum' => $invoice->iInvoiceNbr,
                    'result' => false,
                    'error' => $this->email->print_debugger()
                )));
            }
        }
    }
    public function process_sepa()
    {
        $this->load->model('Admin_model');
        foreach (array(
            '53',
            '54',
            '56'
        ) as $companyid) {
            //echo date("Y-m-d H:i:s")."      Processing " . $companyid . "\n";
            if (!$companyid) {
                die('Company ID required');
            }
            $text = "Sepa Transaction Report:\n";
            log_message('error', 'Sepa Transaction Report companyid: '.$companyid);
            $this->load->library('magebo', array(
                'companyid' => $companyid
            ));
            $q = $this->db->query("select * from a_sepa_items where BatchPaymentId != '' and status =? and companyid = ? order by BatchPaymentId ASC", array(
                'Pending',
                $companyid
            ));
            if ($q->num_rows() > 0) {
                foreach ($q->result() as $row) {
                    // get invoices from the iBankFileIndex
                    if (is_numeric($row->BatchPaymentId)) {
                        log_message('error', 'Processing Directdebit ID: '.$row->BatchPaymentId.' companyid: '.$companyid);
                        $invoices = $this->magebo->getBankDetails($row->BatchPaymentId);
                        if ($invoices) {
                            $this->db->query("update a_sepa_items set status='Completed' where id =?", array(
                                $row->id
                            ));
                            //log_message("error", date('Y-m-d H:i:s') . "- Processing Directdebit payments " . $row->BatchPaymentId . "\n");
                            foreach ($invoices as $invoice) {
                                log_message("error", date('Y-m-d H:i:s') . "- Processing Directdebit payments " . $row->BatchPaymentId . "\n");
                                $text .= date('Y-m-d H:i:s') . "- " . $invoice->iInvoiceNbr . "\n";
                                if (trim($invoice->cTypeDescription) == "SEPA FIRST") {
                                    $paymentform = 'SEPA FIRST';
                                } else {
                                    $paymentform = 'SEPA REOCCURRING';
                                }
                                log_message('error', 'Apply Payment for '.$invoice->iInvoiceNbr.' Amount: '. $invoice->mBankFileAmount);
                                $res = $this->magebo->apply_payment(array(
                                    'cPaymentForm' => $paymentform,
                                    'mPaymentAmount' => $invoice->mBankFileAmount,
                                    'cPaymentFormDescription' => "Sepa DD Payment " . $invoice->iInvoiceNbr,
                                    'dPaymentDate' => date("m/d/Y", strtotime($row->date_transaction)),
                                    'iInvoiceNbr' => $invoice->iInvoiceNbr,
                                    'step' => 1,
                                    'iAddressNbr' => $invoice->iAddressNbr,
                                    'cPaymentRemark' => $row->fileid
                                ));
                                log_message("error", date("Y-m-d H:i:s") . "      Processing :" . $paymentform . " " . $invoice->mBankFileAmount . " " . $invoice->iInvoiceNbr . " " . $invoice->iAddressNbr . "\n");
                                unset($res);
                            }
                        }
                    }
                }
            }
            //Process Reject
            if ($companyid == 54) {
                $this->proces_bankfiles($companyid);
                $rejects = $this->db->query("select * from a_sepa_items where transactioncode='st' and BatchPaymentId = '' and status =? and companyid = ?  and amount < 0 order by id ASC", array(
                    'Pending',
                    $companyid
                ));
            } else {
                $rejects = $this->db->query("select * from a_sepa_items where BatchPaymentId is NULL and status =? and companyid = ?  and amount < 0 order by id ASC", array(
                    'Pending',
                    $companyid
                ));
            }
            if ($rejects->num_rows() > 0) {
                //echo "Found " . $rejects->num_rows() . " Rejection for companyid." . $companyid . "\n";
                foreach ($rejects->result() as $row) {
                    if ($companyid == 53) {
                        if (empty($row->BatchPaymentId) && !empty($row->mandateid)) {
                            if ($row->amount < 0) {
                                $invoices = $this->magebo->getPaymentInformation($row);
                                if ($invoices) {
                                    foreach ($invoices as $inv) {
                                        log_message("error", date('Y-m-d H:i:s') . "- Processing Rejection (stage1) pf payments " . $inv->iInvoiceNbr . " \n");
                                        //In general any rejected payment, we need to delete the payment and also the allocation.
                                        //Now we get the iPaymentNbr for the transaction that needs to be reversed
                                        //Normally there should ba a payment transaction - then this reverse
                                        //Delete assignment
                                        $OldPayment = $inv->iPaymentNbr;
                                        $this->magebo->deletePaymentAssignment($inv->iPaymentNbr);
                                        //If in CODA file, the reject is for a first only then do we go back to FIRST
                                        if ($inv->cTypeDescription == 'SEPA FIRST') {
                                            //UPDATE SEPA STATUS
                                            $this->magebo->updateSepaStatus($inv->iAddressNbr);
                                        }
                                        //Create a negative payment
                                        //Now we prepare for the Reject PaymentId
                                        //$SeqNumberPaperStatReject='KBC'.$row['SequenceNumberPaperstatement'];
                                        if (trim($inv->cTypeDescription) == "SEPA FIRST") {
                                            $paymentform = 'SEPA FIRST';
                                        } else {
                                            $paymentform = 'SEPA REOCCURRING';
                                        }
                                        $res = $this->magebo->apply_payment(array(
                                            'cPaymentForm' => $paymentform,
                                            'mPaymentAmount' => $row->amount,
                                            'cPaymentFormDescription' => "Sepa Reject - " . $inv->iInvoiceNbr,
                                            'dPaymentDate' => date("m/d/Y", strtotime($row->date_transaction)),
                                            'iInvoiceNbr' => $inv->iInvoiceNbr,
                                            'step' => 1,
                                            'iAddressNbr' => $inv->iAddressNbr,
                                            'cPaymentRemark' => $row->id
                                        ));
                                        log_message("error", print_r($res, true));
                                        // echo "Processing :".$paymentform." ".$row->amount." ".$invoice->iInvoiceNbr." ".$invoice->iAddressNbr."\n";
                                        $userid         = getClientidbyMagebo($inv->iAddressNbr);
                                        $invoicedetails = $this->magebo->getInvoice($inv->iInvoiceNbr);
                                        $this->magebo->SetRefusedSepa($row->end2endid);
                                        echo $this->sendDDRejected($invoicedetails, $userid, $row->return_code);
                                        if ($res->result) {
                                            $this->Admin_model->SepaItemsComplete($row->id, array(
                                                'status' => 'Completed',
                                                'iAddressNbr' => $inv->iAddressNbr,
                                                'invoicenumber' => $inv->iInvoiceNbr
                                            ));
                                            log_message("error", date('Y-m-d H:i:s') . "- Processing Rejection (stage1) pf payments " . $OldPayment . " \n");
                                            //$this->db->query("update a_sepa_items set status='Completed',iAddressNbr=? where id=?", array($row->id));
                                            log_message('error', 'New:' . $res->id . ', Old' . $OldPayment);
                                            $this->magebo->assignPaymentPayment($res->id, $OldPayment);
                                        }
                                    }
                                }
                            }
                        }
                    } elseif ($companyid == 56) {
                        if (empty($row->BatchPaymentId) && !empty($row->mandateid)) {
                            if ($row->amount < 0) {
                                $invoices = $this->magebo->getPaymentInformation($row);
                                if ($invoices) {
                                    foreach ($invoices as $inv) {
                                        log_message("error", date('Y-m-d H:i:s') . "- Processing Rejection stage(1) pf payments " . $inv->iInvoiceNbr . " \n");
                                        //In general any rejected payment, we need to delete the payment and also the allocation.
                                        //Now we get the iPaymentNbr for the transaction that needs to be reversed
                                        //Normally there should ba a payment transaction - then this reverse
                                        //Delete assignment
                                        $this->magebo->deletePaymentAssignment($inv->iPaymentNbr);
                                        //If in CODA file, the reject is for a first only then do we go back to FIRST
                                        if ($inv->cTypeDescription == 'SEPA FIRST') {
                                            //UPDATE SEPA STATUS
                                            $this->magebo->updateSepaStatus($inv->iAddressNbr);
                                        }
                                        //Create a negative payment
                                        //Now we prepare for the Reject PaymentId
                                        //$SeqNumberPaperStatReject='KBC'.$row['SequenceNumberPaperstatement'];
                                        if (trim($inv->cTypeDescription) == "SEPA FIRST") {
                                            $paymentform = 'SEPA FIRST';
                                        } else {
                                            $paymentform = 'SEPA REOCCURRING';
                                        }
                                        $res            = $this->magebo->apply_payment(array(
                                            'cPaymentForm' => $paymentform,
                                            'mPaymentAmount' => $row->amount,
                                            'cPaymentFormDescription' => "Sepa Reject - " . $inv->iInvoiceNbr,
                                            'dPaymentDate' => date("m/d/Y", strtotime($row->date_transaction)),
                                            'iInvoiceNbr' => $inv->iInvoiceNbr,
                                            'step' => 1,
                                            'iAddressNbr' => $inv->iAddressNbr,
                                            'cPaymentRemark' => $row->id
                                        ));
                                        // echo "Processing :".$paymentform." ".$row->amount." ".$invoice->iInvoiceNbr." ".$invoice->iAddressNbr."\n";
                                        $userid         = getClientidbyMagebo($inv->iAddressNbr);
                                        $invoicedetails = $this->magebo->getInvoice($inv->iInvoiceNbr);
                                        $this->magebo->SetRefusedSepa($row->end2endid);
                                        $this->sendDDRejected($invoicedetails, $userid, $row->return_code);
                                        if ($res->result) {
                                            $this->Admin_model->SepaItemsComplete($row->id, array(
                                                'status' => 'Completed',
                                                'iAddressNbr' => $inv->iAddressNbr,
                                                'invoicenumber' => $inv->iInvoiceNbr
                                            ));
                                            //$this->db->query("update a_sepa_items set status='Completed',iAddressNbr=? where id=?", array($row->id));
                                            $this->magebo->assignPaymentPayment($res->id, $inv->iPaymentNbr);
                                        }
                                    }
                                }
                            }
                        }
                    } elseif ($companyid == 54) {
                        if (empty($row->BatchPaymentId) && !empty($row->end2endid) && strlen($row->end2endid) < 10) {
                            if ($row->amount < 0) {
                                $inv         = $this->magebo->getPaymentInformationbyEnd2endId($row);
                                $iPaymentNbr = $this->magebo->getPaymentNumber($inv->iInvoiceNbr, $row->amount);
                                log_message("error", date('Y-m-d H:i:s') . "- Processing Rejection of stage(1) payments " . $inv->iInvoiceNbr . " \n");
                                //In general any rejected payment, we need to delete the payment and also the allocation.
                                //Now we get the iPaymentNbr for the transaction that needs to be reversed
                                //Normally there should ba a payment transaction - then this reverse
                                //Delete assignment
                                $this->magebo->deletePaymentAssignment($iPaymentNbr);
                                //If in CODA file, the reject is for a first only then do we go back to FIRST
                                if ($inv->cTypeDescription == 'SEPA FIRST') {
                                    //UPDATE SEPA STATUS
                                    $this->magebo->updateSepaStatus($inv->iAddressNbr);
                                }
                                //Create a negative payment
                                //Now we prepare for the Reject PaymentId
                                //$SeqNumberPaperStatReject='KBC'.$row['SequenceNumberPaperstatement'];
                                if (trim($inv->cTypeDescription) == "SEPA FIRST") {
                                    $paymentform = 'SEPA FIRST';
                                } else {
                                    $paymentform = 'SEPA REOCCURRING';
                                }
                                $res            = $this->magebo->apply_payment(array(
                                    'cPaymentForm' => $paymentform,
                                    'mPaymentAmount' => $row->amount,
                                    'cPaymentFormDescription' => "Sepa Reject - " . $inv->iInvoiceNbr,
                                    'dPaymentDate' => date("m/d/Y", strtotime($row->date_transaction)),
                                    'iInvoiceNbr' => $inv->iInvoiceNbr,
                                    'step' => 1,
                                    'iAddressNbr' => $inv->iAddressNbr,
                                    'cPaymentRemark' => $row->id
                                ));
                                // echo "Processing :".$paymentform." ".$row->amount." ".$invoice->iInvoiceNbr." ".$invoice->iAddressNbr."\n";
                                $userid         = getClientidbyMagebo($inv->iAddressNbr);
                                $invoicedetails = $this->magebo->getInvoice($inv->iInvoiceNbr);
                                $this->magebo->SetRefusedSepa($row->end2endid);
                                $this->sendDDRejected($invoicedetails, $userid, $row->return_code);
                                if ($res->result) {
                                    $this->Admin_model->SepaItemsComplete($row->id, array(
                                        'status' => 'Completed',
                                        'iAddressNbr' => $inv->iAddressNbr,
                                        'invoicenumber' => $inv->iInvoiceNbr
                                    ));
                                    $this->magebo->assignPaymentPayment($res->id, $iPaymentNbr);
                                }
                            }
                        }
                    }
                }
            }
            // Process The reset Of the Reject
            $rejects_rest = $this->db->query("select * from a_sepa_items where mandateid LIKE 'SEPA%' and status=?", array(
                'Pending'
            ));
            if ($rejects_rest->num_rows() > 0) {
                foreach ($rejects_rest->result() as $row) {
                    //print_r($row);
                    if (empty($row->BatchPaymentId) && !empty($row->mandateid)) {
                        //$text. = date('Y-m-d H:i:s')."- Processing Rejection pf payments ".$row->BatchPaymentId."\n";
                        if ($row->amount < 0) {
                            $invoices = $this->magebo->getSinglePaymentInformation($row);
                            if ($invoices) {
                                foreach ($invoices as $inv) {
                                    log_message("error", date('Y-m-d H:i:s') . "- Processing Rejection of stage(2) Invoice: " . $inv->iInvoiceNbr . " Amount:\n");
                                    //In general any rejected payment, we need to delete the payment and also the allocation.
                                    //Now we get the iPaymentNbr for the transaction that needs to be reversed
                                    //Normally there should ba a payment transaction - then this reverse
                                    //Delete assignment
                                    $this->magebo->deletePaymentAssignment($inv->iPaymentNbr);
                                    //If in CODA file, the reject is for a first only then do we go back to FIRST
                                    if ($inv->cTypeDescription == 'SEPA FIRST') {
                                        //UPDATE SEPA STATUS
                                        $this->magebo->updateSepaStatus($inv->iAddressNbr);
                                    }
                                    if (trim($inv->cTypeDescription) == "SEPA FIRST") {
                                        $paymentform = 'SEPA FIRST';
                                    } else {
                                        $paymentform = 'SEPA REOCCURRING';
                                    }
                                    //Create a negative payment
                                    //Now we prepare for the Reject PaymentId
                                    //$SeqNumberPaperStatReject='KBC'.$row['SequenceNumberPaperstatement'];
                                    $res            = $this->magebo->apply_payment(array(
                                    'cPaymentForm' => $paymentform,
                                    'mPaymentAmount' => $row->amount,
                                    'cPaymentFormDescription' => "Sepa Reject - " . $inv->iPaymentNbr,
                                    'dPaymentDate' => date("m/d/Y", strtotime($row->date_transaction)),
                                    'iInvoiceNbr' => $inv->iInvoiceNbr,
                                    'step' => 1,
                                    'iAddressNbr' => $inv->iAddressNbr,
                                    'cPaymentRemark' => $row->fileid
                                ));
                                    $userid         = getClientidbyMagebo($inv->iAddressNbr);
                                    $invoicedetails = $this->magebo->getInvoice($inv->iInvoiceNbr);
                                    $this->magebo->SetRefusedSepa($row->end2endid);
                                    $this->sendDDRejected($invoicedetails, $userid, $row->return_code);
                                    if ($res->result) {
                                        $this->Admin_model->SepaItemsComplete($row->id, array(
                                        'status' => 'Completed',
                                        'iAddressNbr' => $inv->iAddressNbr,
                                        'invoicenumber' => $inv->iInvoiceNbr
                                    ));
                                        $this->magebo->assignPaymentPayment($res->id, $inv->iPaymentNbr);
                                    }
                                }
                            } else {
                                log_message('error', 'No Invoice to be processed ');
                            }
                        }
                    }
                }
            }
            unset($this->magebo);
        }
        //mail('mail@simson.one','Report Sepa Batch', $text);
    }
    public function SendDataCon($companyid)
    {
        $this->load->model('Admin_model');
        $count    = 0;
        $filename = 'OnnetVoipMsisdn_' . date('YmdHis') . '_'.$companyid.'.csv';
        //$this->load->library('artilium', array(COMPANYID, PLATFORM));
        $res      = $this->db->query("select a.date_terminate,a.id,a.date_contract as date,a.status,b.msisdn as domain,b.msisdn_sn as username from a_services a left join a_services_mobile b on b.serviceid=a.id where a.status in ('Active','Terminated') and b.companyid=? and b.datacon_ftp=?", array(
            $companyid,
            '0'
        ));
        if ($res->num_rows() > 0) {
            $myfile = fopen("/tmp/" . $filename, "w") or die("Unable to open file!");
            log_message("error", "Found " . $res->num_rows() . " files to be uploaded\n");
            foreach ($res->result_array() as $number) {
                if (in_array($number['status'], array(
                    'Active'
                ))) {
                    $status = 'ACTIVE';
                    log_message("error", print_r($number, true));
                    $t = explode('-', $number['date']);
                    log_message("error", date('Y-m-d H:i:s') . " -- " . $number['domain'] . "," . $status . "," . $t[2] . "-" . $t[0] . "-" . $t[1] . " 00:00:00\n");
                    fwrite($myfile, $number['domain'] . "," . $status . "," . $t[2] . "-" . $t[0] . "-" . $t[1] . " 00:00:00\n");
                    $this->Admin_model->update_services_data('mobile', $number['id'], array(
                        'datacon_ftp' => 1,
                        'datacon_date' => date('Y-m-d'),
                        'date_modified' => date('Y-m-d H:i:s')
                    ));
                    unset($t);
                    unset($status);
                    unset($number);
                } elseif (in_array($number['status'], array(
                    'Terminated'
                ))) {
                    $status = 'DEACTIVE';
                    if (empty($number['date_terminate'])) {
                        $number['date_terminate'] = "2019-01-01";
                    }
                    log_message("error", date('Y-m-d H:i:s') . " -- " . $number['username'] . "," . $status . "," . $number['date_terminate'] . " 00:00:00\n");
                    fwrite($myfile, $number['username'] . "," . $status . "," . $number['date_terminate'] . " 00:00:00\n");
                    $this->Admin_model->update_services_data('mobile', $number['id'], array(
                        'datacon_ftp' => 1,
                        'datacon_date' => date('Y-m-d'),
                        'date_modified' => date('Y-m-d H:i:s')
                    ));
                    unset($status);
                    unset($number);
                }
            }
            fclose($myfile);
            $this->upload_file($filename);
        }
    }
    public function upload_file($filename)
    {
        $localFile  = '/tmp/' . $filename;
        $remoteFile = '/Onnet_Prod_import/' . $filename;
        /*
        $host = "ftp.datacon.nl";
        $port = 22;
        $user = "delta";
        $pass = "D3lt@S3cr3t!";
        */
        $host       = "ftp-sftp1.caiw.net";
        $port       = 2222;
        $user       = "artillium_onnet";
        $pass       = "uL0CFn";
        $connection = ssh2_connect($host, $port);
        ssh2_auth_password($connection, $user, $pass);
        $sftp = ssh2_sftp($connection);
        if ($sftp) {
            log_message("error", date('Y-m-d H:i:s') . " -- Uploading Files " . $filename . "\n" . $stream = fopen("ssh2.sftp://$sftp$remoteFile", 'w'));
            $file = file_get_contents($localFile);
            fwrite($stream, $file);
            unlink($localFile);
        }
        fclose($stream);
    }
    public function block_numbers()
    {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->load->library('magebo', array(
            'companyid' => '20'
        ));
        $this->load->library('artilium', array(
            'companyid' => '20'
        ));
        $INparams = array(
            'platform' => 'POSTPAID',
            'reselleruser' => 'STUnitedPostpaid',
            'resellerpass' => '$TUn1tedP0stpa1D'
        );
        foreach ($this->magebo->getPremier() as $row) {
            //$SN = $this->artilium->getSn()
            // $this->artilium->mvno_PartialFullBar($INparams, $SimNbr, $SN, 2, $vars);
            $sim = $this->magebo->getSim(substr($row->iPincode, 2));
            print_r($sim);
            print_R($this->artilium->getSn($sim->cSIMCardNbr));
        }
    }
    public function check_double_mageboid()
    {
        $q = $this->db->query("SELECT mageboid, COUNT(*) FROM a_clients where companyid in (53,54,55,56,57) GROUP BY mageboid HAVING COUNT(*) > 1");
        if ($q->num_rows() > 0) {
            $headers = 'From: noreply@united-telecom.be' . "\r\n" .
                        'Reply-To: noreply@united-telecom.be' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
            mail("simson.parlindungan@pareteum.com", "Double Mageboid Detected", print_r($q->result(), true), $headers);
        }
    }
    public function sync_porting_status()
    {
        $this->db = $this->load->database('default', true);
        $this->load->model('Admin_model');
        foreach (array(
            53,
            54,
            56,
            55
        ) as $companyid) {
            $this->load->library('artilium', array(
                'companyid' => $companyid
            ));
            $list = $this->artilium->GetPendingPortIns();
            echo "Found " . count($list) . "\n";
            foreach ($list as $l) {
                if ($l['Status'] == "Port in Exec pending") {
                    $service = $this->db->query("select id,msisdn,serviceid,msisdn_status from a_services_mobile where msisdn_sn like ?", array(
                        trim($l['SN'])
                    ));
                    if ($service->num_rows() > 0) {
                        if ($service->row()->msisdn_status != 'PortinPending') {
                            //$this->db->query("update a_services_mobile set msisdn_status='PortinPending' where id=?", array($service->row()->id));
                        }
                        //$this->db->query("update a_services_mobile set msisdn_status='PortinRejected' where id=?", array($service->row()->id));trim($row['MSISDN'])
                    }
                } elseif ($l['Status'] == "Port in Accepted") {
                    $service = $this->db->query("select id,msisdn,serviceid,msisdn_status from a_services_mobile where msisdn like ?", array(
                        trim($l['MSISDN'])
                    ));
                    if ($service->num_rows() > 0) {
                        log_message("error", flatme((object) $l) . "\n");
                        //  $this->Admin_model->update_services_data('mobile', $service->row()->serviceid, array('msisdn_status' => 'PortinAccepted', 'date_ported' => str_replace('/', '-', $l['ActionDate'])));
                        //if($service->row()->msisdn_status != 'PortinAccepted'){
                        //// $date_accepted= str_replace()
                        $this->Admin_model->update_services_data('mobile', $service->row()->serviceid, array(
                            'date_ported' => str_replace('/', '-', $l['ActionDate']),
                            'date_modified' => date('Y-m-d H:i:s')
                        ));
                        //$this->db->query("update a_services_mobile set date_ported=? where id=?", array($service->row()->id), str_replace('/', '-', $l['ActionDate']));
                        // }
                        //$this->db->query("update a_services_mobile set msisdn_status='PortinRejected' where id=?", array($service->row()->id));
                    }
                } elseif ($l['Status'] == "Port in Rejected") {
                    $service = $this->db->query("select id,msisdn,serviceid,msisdn_status from a_services_mobile where msisdn_sn like ?", array(
                        trim($l['SN'])
                    ));
                    if ($service->num_rows() > 0) {
                        //if($service->row()->msisdn_status != 'PortinRejected'){
                        if ($l['Error.ID'] === "0") {
                            $s['Remark'] = $l['ErrorComment'];
                        } elseif ($l['Error.ID'] == 27) {
                            $s['Remark'] = "Telephone number and customer ID do not correspond";
                        } elseif ($l['Error.ID'] == 63) {
                            $s['Remark'] = "beyond notice terms / Porting not possible within porting window";
                        } elseif ($l['Error.ID'] == 99) {
                            $s['Remark'] = "Other";
                        } elseif ($l['Error.ID'] == 41) {
                            $s['Remark'] = "Multiple number types are not allowed.";
                        } elseif ($l['Error.ID'] == 44) {
                            $s['Remark'] = "Multiple DNOs or DSPs not allowed.";
                        } elseif ($l['Error.ID'] == 45) {
                            $s['Remark'] = "Telephone number is not portable";
                        } elseif ($l['Error.ID'] == "02") {
                            $s['Remark'] = "Porting request for this/these number(s) is already in progress";
                        } elseif ($l['Error.ID'] == "01") {
                            $s['Remark'] = "Other";
                        } else {
                            $s['Remark'] = "";
                        }
                        //echo $s['Remark'];
                        //print_r($service->row());
                        $this->Admin_model->update_services_data('mobile', $service->row()->serviceid, array(
                            'msisdn_status' => 'PortinRejected',
                            'porting_remark' => $s['Remark'],
                            'date_modified' => date('Y-m-d H:i:s')
                        ));
                        //$this->db->query("update a_services_mobile set msisdn_status='PortinRejected',porting_remark=? where id=?", array($service->row()->id, $s['Remark']));
                        unset($s['Remark']);
                        // }
                    }
                } elseif ($l['Status'] == "Port in Canceled") {
                    $service = $this->db->query("select id,msisdn,serviceid,msisdn_status from a_services_mobile where msisdn_sn like ?", array(
                        trim($l['SN'])
                    ));
                    if ($service->num_rows() > 0) {
                        if ($service->row()->msisdn_status != 'PortinCancelled') {
                            $this->db->query("update a_services_mobile set msisdn_status='PortinCancelled' where id=?", array(
                                $service->row()->id
                            ));
                        }
                    }
                } elseif ($l['Status'] == "Port in Failed") {
                    $service = $this->db->query("select id,msisdn,serviceid,msisdn_status from a_services_mobile where msisdn_sn like ?", array(
                        trim($l['SN'])
                    ));
                    if ($service->num_rows() > 0) {
                        if ($service->row()->msisdn_status != 'Port in Failed') {
                            $this->db->query("update a_services_mobile set msisdn_status='PortinFailed' where id=?", array(
                                $service->row()->id
                            ));
                        }
                    }
                }
                unset($service);
            }
            unset($this->artilium);
        }
    }
    public function process_service_changes($date = false)
    {
        log_message("error", "Cronjob for Packages changes has been started");
        /*
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        */

        try {
            if ($date) {
                $dateme = $date;
            } else {
                $datetime = new DateTime('tomorrow');
                $dateme   = $datetime->format('Y-m-d');
            }

            log_message("error", "Processing Service Package Changes ".$dateme . "\n");
            $this->load->model('Admin_model');
            $q = $this->db->query("select a.*,b.gid as new_gid, c.gid as old_gid,b.name as new_product, c.name as old_product  from a_subscription_changes a left join a_products b on b.id=a.new_pid left join a_products c on c.id=a.old_pid where a.cron_status=? and a.date_commit like ? order by a.companyid,a.id asc", array(
                '0',
                $dateme
            ));
            log_message("error", $this->db->last_query());
            if ($q->num_rows()>0) {
                log_message("error", "Found ".$q->num_rows()." to be change for ".$dateme);
                foreach ($q->result() as $row) {
                    $changes[$row->companyid][] = $row->serviceid." ".$row->msisdn." From ".$row->old_product." To ".$row->new_product;
                    log_message('error', "Processing : ".$row->serviceid);
                    log_message('error', print_r($row, true));
                    $killdate = new DateTime($row->date_commit);
                    $killdate->modify('-1 day');
                    $price = $row->new_price;
                    if ($this->Admin_model->update_services($row->serviceid, array(
                    'packageid' => $row->new_pid,
                    'recurring' => $price
                ))) {
                        echo "Package changes et complete\n";
                        $this->Admin_model->set_service_change_done($row->id);
                        $this->load->library('artilium', array(
                        'companyid' => $row->companyid
                    ));
                        $this->load->library('magebo', array(
                        'companyid' => $row->companyid
                    ));
                        //add new Bundle before set old bundle expire
                        $mobile      = $this->Admin_model->getServiceCli($row->serviceid);
                        $client      = $this->Admin_model->getClient($mobile->userid);
                        $new_bundles = $this->Admin_model->getBundlebyProduct($mobile->packageid);
                        $this->magebo->updateBundleStopDate($mobile->details->msisdn, $killdate->format('Y-m-d') . ' 23:59:59'); //disable current bundle in Magebo
                        //set Old Bundle to expire
                        $bundles = $this->artilium->GetBundleAssignList($row->sn);
                        print_r($bundles);

                        if (empty(!$bundles->NewDataSet->BundleAssign->BundleAssignId)) {
                            $bundle = $bundles->NewDataSet->BundleAssign;
                            $res = $this->artilium->UpdateBundleAssignV2(array(
                            'SN' => trim($row->sn),
                            'BundleAssignId' => $bundle->BundleAssignId,
                            'ValidUntil' => $killdate->format('Y-m-d') . 'T23:59:59'
                            ));
                            if ($res->UpdateBundleAssignV2Result->Result == "0") {
                                $t = $this->artilium->GetBundleUsageList($row->sn, $bundle->BundleAssignId);
                                if (is_array($t->NewDataSet->BundleUsage)) {
                                    foreach ($t->NewDataSet->BundleUsage as $usage) {
                                        print_r($this->artilium->UpdateBundleUsage(array(
                                        "Sn" => $row->sn,
                                        "BundleUsageId" => $usage->BundleUsageId,
                                        "BundleUsageSetId" => $usage->BundleUsageSetId,
                                        "ValidUntil" => $killdate->format('Y-m-d') . "T23:59:59",
                                        "UpdateBundleAssignment" => false
                                        )));
                                    }
                                } else {
                                    print_r($this->artilium->UpdateBundleUsage(array(
                                    "Sn" => $row->sn,
                                    "BundleUsageId" => $t->NewDataSet->BundleUsage->BundleUsageId,
                                    "BundleUsageSetId" => $t->NewDataSet->BundleUsage->BundleUsageSetId,
                                    "ValidUntil" => $killdate->format('Y-m-d') . "T23:59:59",
                                    "UpdateBundleAssignment" => false
                                    )));
                                }
                                unset($t);
                            }

                            unset($bundle);
                        } else {
                            foreach ($bundles->NewDataSet->BundleAssign as $bundle) {
                                $res = $this->artilium->UpdateBundleAssignV2(array(
                        'SN' => trim($row->sn),
                        'BundleAssignId' => $bundle->BundleAssignId,
                        'ValidUntil' => $killdate->format('Y-m-d') . 'T23:59:59'
                        ));
                                if ($res->UpdateBundleAssignV2Result->Result == "0") {
                                    $t = $this->artilium->GetBundleUsageList($row->sn, $bundle->BundleAssignId);
                                    if (is_array($t->NewDataSet->BundleUsage)) {
                                        foreach ($t->NewDataSet->BundleUsage as $usage) {
                                            print_r($this->artilium->UpdateBundleUsage(array(
                                    "Sn" => $row->sn,
                                    "BundleUsageId" => $usage->BundleUsageId,
                                    "BundleUsageSetId" => $usage->BundleUsageSetId,
                                    "ValidUntil" => $killdate->format('Y-m-d') . "T23:59:59",
                                    "UpdateBundleAssignment" => false
                                    )));
                                        }
                                    } else {
                                        print_r($this->artilium->UpdateBundleUsage(array(
                                "Sn" => $row->sn,
                                "BundleUsageId" => $t->NewDataSet->BundleUsage->BundleUsageId,
                                "BundleUsageSetId" => $t->NewDataSet->BundleUsage->BundleUsageSetId,
                                "ValidUntil" => $killdate->format('Y-m-d') . "T23:59:59",
                                "UpdateBundleAssignment" => false
                                )));
                                    }
                                    unset($t);
                                }
                            }
                        }


                        if ($new_bundles) {
                            $IDS = array();
                            // activate tarief per packages pricing_query
                            foreach ($new_bundles as $bundleid) {
                                $r = $this->artilium->AddBundleAssign($row->sn, $bundleid, $row->date_commit . 'T00:00:00', '2099-12-31T23:59:59');
                                if ($r->result == "success") {
                                    $setting = globofix($row->companyid);
                                    if ($setting->create_magebo_bundle) {
                                        $create = 1;
                                        $this->magebo->InsertBundlePincode($mobile->details->msisdn, $this->Admin_model->getBundleQuery($bundleid), $row->date_commit);
                                    } //$this->data['setting']->create_magebo_bundle
                                    else {
                                        $create = 0;
                                    }
                                    unset($setting);
                                    $this->magebo->ProcessPricing($mobile, $client->mageboid, $bundleid, $create);
                                    $IDS[] = $r->id;
                                } //$r->result == "success"
                                else {
                                    $IDS[] = $r;
                                }
                            } //$bundles as $bundleid
                            if ($IDS) {
                                logAdmin(array(
                            'companyid' => $row->companyid,
                            'serviceid' => $row->serviceid,
                            'userid' => $client->id,
                            'user' => $row->requestor,
                            'ip' => '127.0.0.1',
                            'description' => 'Service : ' . $row->serviceid . ' added bundle id ' . implode(',', $IDS)
                            ));
                                $this->Admin_model->updateBundleID($mobile->id, implode(',', $IDS));
                            } //$IDS
                            unset($new_bundles);
                            unset($IDS);
                        } //$bundles


                        unset($this->magebo);
                        unset($this->artilium);
                    } else {
                        log_message("error", "Already processed\n");
                    }
                }
                print_r($changes);
                foreach ($changes as $cp => $data) {
                    if ($data) {
                        mail("mail@simson.one", "Product Change Companyid ".$cp, "Dear\n\nYour Product Changes Plane for ".$dateme."\n\n".implode("\n", $data), "\n\nRegards\nEurpean Portal V1");
                    }
                }
            } else {
                log_message('error', "There is no subscription that need package changes for ".$dateme);
            }
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
            //mail('mail@simson.one', "Exception Sending Reminder ", "Message: " . $e->getMessage());
        }
    }
    public function moveCliContactid()
    {
        try {
            $q = $this->db->query("select x.msisdn_sn as sn, a.*,b.gid as new_gid, c.gid as old_gid from a_subscription_changes a left join a_services_mobile x on x.id=a.serviceid left join a_products b on b.id=a.new_pid left join a_products c on c.id=a.old_pid where a.cron_status=?  and a.date_commit like ?", array(
                '1',
                date('Y-m-d')
            ));
            log_message('error', $this->db->last_query());
            //  $q =  $this->db->query("select * from a_subscription_changes where cron_status =? and serviceid = ?", array('0', '1800687'));
            if ($q->num_rows() > 0) {
                log_message('error', 'Found 2 contact id need to me moved ');

                foreach ($q->result() as $row) {
                    $this->load->library('artilium', array(
                        'companyid' => $row->companyid
                    ));
                    if (getContactId($row->new_gid) != getContactId($row->old_gid)) {
                        log_message('error', 'Moving '.$row->msisdn.' contact id from '.getContactId($row->old_gid).' to '.getContactId($row->new_gid));

                        $this->artilium->MoveCLI($row->sn, getContactId($row->new_gid), 1);
                    }
                    unset($this->artilium);
                }
            } else {
                log_message('error', 'No Contact ID need to be moved');
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception moveCliContactid ", "Message: " . $e->getMessage());
        }
    }
    public function change_now()
    {
        $q = $this->db->query("select * from a_subscription_changes where cron_status =? and date_commit like ?", array(
            '0',
            '2019-02-01'
        ));
        $this->load->model('Admin_model');
        foreach ($q->result() as $row) {
            $killdate = new DateTime($row->date_commit);
            $killdate->modify('-1 day');
            if ($this->Admin_model->update_services($row->serviceid, array(
                'packageid' => $row->new_pid
            ))) {
                $this->Admin_model->set_service_change_done($row->id);
            }
        }
    }
    public function soap2xml($data)
    {
        $doc       = new DOMDocument();
        $domstring = htmlspecialchars_decode($data);
        $doc->loadXML($domstring);
        $dtaxml = $doc->getElementsByTagNameNS('http://schemas.datacontract.org/2004/07/Artilium.ServiceBus.Api.Operations', 'ClrType');
        foreach ($dtaxml as $book) {
            $evtall = $book->nodeValue;
        }
        $ea                          = explode(",", $evtall);
        $event                       = $ea[0];
        $dtaxml2                     = $doc->getElementsByTagNameNS('http://schemas.datacontract.org/2004/07/Artilium.ServiceBus.Api.Operations', 'Data');
        $domnode                     = $dtaxml2->item(0)->childNodes->item(0);
        $eventxml                    = simplexml_load_string($doc->saveXML($domnode));
        $eventjson                   = (array) $eventxml;
        $s                           = $this->getServicebySn(trim($eventxml->SubscriptionId));
        $eventjson['SubscriptionId'] = trim($eventxml->SubscriptionId);
        if ($s) {
            return array_merge($eventjson, $s);
        } else {
            return $eventjson;
        }
    }
    public function importDone()
    {
        $this->db = $this->load->database('events', true);
        $q        = $this->db->query("SELECT * FROM artaevents.events_raw where rawdata like '%PortInChangeMsisdn%' and posttime like '2019-01-16%' order by id desc");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $array = $this->soap2xml($row->rawdata);
                //print_r($array);
                //$this->deleteEvents($array);
                //$this->insertEvent($array);
                unset($array);
            }
        }
    }
    public function deleteEvents($array)
    {
        print_r($array);
        $this->db = $this->load->database('default', true);
        $this->db->where('serviceid', $array['serviceid']);
        $this->db->delete('a_event_log');
    }
    public function insertEvent($array)
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_event_log', $array);
        $id = $this->db->insert_id();
    }
    public function getServicebySn($sn)
    {
        $this->db = $this->load->database('default', true);
        $service  = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.msisdn_sn LIKE ?", array(
            '%' . trim($sn) . '%'
        ));
        if ($service->num_rows() > 0) {
            $array['companyid'] = $service->row()->companyid;
            $array['serviceid'] = $service->row()->serviceid;
            return $array;
        } else {
            return false;
        }
    }
    public function enable_services()
    {
        $this->load->model('Api_model');
        $this->db = $this->load->database('default', true);
        $this->load->model('Admin_model');
        error_reporting(1);
        log_message('error', 'Running task for portin');

        $cancels            =  $this->db->query("SELECT *  FROM `a_event_log` WHERE `Action` LIKE 'PortInCanceled' and JobName = 'PortInCancel' and status = '0' ORDER BY `JobId`  DESC");
        log_message('error', 'Found '.$cancels->num_rows().' For Portin Cancelled');

        if ($cancels->num_rows() > 0) {
            foreach ($cancels->result() as $can) {
                log_message("error", "Portin Cancelled ==> " . flatme($can) . "\n");
                $this->db->query("update a_services_mobile set msisdn_status=? where serviceid=?", array("PortinCancelled",$can->serviceid));
                $this->Admin_model->setEventLogDone($can->JobId);
            }
        }
        sleep(4);

        $ported             = $this->db->query("SELECT * FROM `a_event_log` WHERE `Action` LIKE 'PortedIn' and status='0' and FinalJob='true' and JobName='PortInChangeMsisdn' and Success='true' ORDER BY `JobId` ASC");
        log_message('error', 'Found '.$ported->num_rows().' For Portin Done');
        $accepts            = $this->db->query("SELECT * FROM `a_event_log` WHERE `Action` LIKE '%PortInAccepted%' and status='0' and FinalJob='false' and JobName='Request' and Success='true' ORDER BY `JobId` ASC");
        log_message('error', 'Found '.$accepts->num_rows().' For Portin Accepted');
        $rejects            = $this->db->query("SELECT * FROM `a_event_log` WHERE `Action` LIKE '%PortInRejected%' and status='0' and FinalJob='false' and JobName='Request' and Success='true' ORDER BY `JobId` ASC");
        log_message('error', 'Found '.$rejects->num_rows().' For Portin Rejected');
        $fails              = $this->db->query("SELECT * FROM `a_event_log` WHERE `Action` LIKE '%PortingFailed%' and status='0' and FinalJob='false' and JobName='Request' and Success='false' ORDER BY `JobId` ASC");
        log_message('error', 'Found '.$fails->num_rows().' For Portin Failed');
        $portout            = $this->db->query("SELECT * FROM `a_event_log` WHERE `Action` LIKE '%PortOutManual%' and status='0' ORDER BY `JobId` ASC");
        log_message('error', 'Found '.$portout->num_rows().' For Portout Failed');
        //$portinBelgium = $this->db->query("SELECT *  FROM `a_event_log` WHERE `Action` LIKE 'PortInReady' and JobName LIKE 'PortInChangeMsisdn' and status = '0' ORDER BY `JobId`  DESC");
        $code_rejected      = $this->db->query("SELECT *  FROM `a_event_log` WHERE `Action` LIKE 'PortInRejected' and JobName LIKE 'PortInValidateVerificationCodeStep2' and status = '0' ORDER BY `JobId`  DESC");
        log_message('error', 'Found '.$code_rejected->num_rows().' For Portin code Rejected');
        $verificationFailed = $this->db->query("SELECT *  FROM `a_event_log` WHERE `Action` LIKE 'PortingFailed' and JobName in ('PortInValidateVerificationCode','PortInInitiate') and status = '0' ORDER BY `JobId`  DESC");
        log_message('error', 'Found '.$verificationFailed->num_rows().' For Portin verification Failed');
        $portinit           = $this->db->query("SELECT *  FROM `a_event_log` WHERE `Action` LIKE 'PortInRejected' and JobName in ('PortInInitiate') and status = '0' ORDER BY `JobId`  DESC");
        log_message('error', 'Found '.$portinit->num_rows().' For Portin Inititialation Rejected');



        echo "Processing results......\n";
        //$resend_porting     = $this->db->query("select * from a_event_log where status='1' and accepted_nodate='0' and Action='PortInAccepted'");
        //print_r($q->result()); PortingFailed PortInValidateVerificationCode $this->send_porting($ac->serviceid, 'portin_accepted');
        //echo shell_exec('/usr/bin/php /home/mvno/public_html/index.php cron sync_porting_status');
        /*disabled
        if ($resend_porting->num_rows() > 0)
        {

        foreach ($resend_porting->result() as $resend)
        {
        //send_growl(array('companyid' => $resend->companyid, 'message' => 'Sending Porting accepted email to your customer'));
        echo date('Y-m-d H:i:s') . " ==> " . flatme($resend) . "\n";
        $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
        $resend->serviceid
        ));
        $client  = getCompanydataByUserID($service->row()->userid);
        // $this->send_porting($resend->serviceid, 'portin_accepted');
        unset($client);
        unset($service);
        }
        }
        */



        if ($accepts->num_rows() > 0) {
            echo "\nRunning Acceptations";
            foreach ($accepts->result() as $ac) {
                echo date('Y-m-d H:i:s') . " ==> " . flatme($ac) . "\n";
                $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                    $ac->serviceid
                ));
                log_message("error", "Portin Accepted ==> " . flatme($ac) . "\n");
                $client  = getCompanydataByUserID($service->row()->userid);
                $setting = globofix($ac->companyid);
                if ($setting->sync_contractdate_with_porting == "1") {
                    log_message('error', $ac->companyid.' Trying to syncronize the contractdate with portin date for '.$ac->serviceid);
                    
                    echo file_get_contents($setting->base_url.'/admin/complete/get_porting_status/'.$ac->serviceid);
                    $this->get_porting_status($service->companyid, $ac->serviceid);
                    // print_r($ppp);
                }
                $this->Admin_model->update_services_data('mobile', $ac->serviceid, array(
                    'msisdn_status' => 'PortinAccepted',
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                //$this->db->query("update a_services_mobile set msisdn_status='PortinAccepted' where id=?", array($ac->serviceid));

                $this->Admin_model->setEventLogDone($ac->JobId);

                $send =  $this->send_porting($ac->serviceid, 'portin_accepted');
                if ($send) {
                    $this->sendEmailNotification($service->row(), "Number Porting has been Accepted on " . $ac->date_event . " SN:" . $service->row()->msisdn, "PortinAccepted", $client);
                }
                $this->teams->send_msg("Portin Accepted Events", "<br /><br /> Request:<br />" . print_r($ac, true));
                unset($service);
                unset($client);
                unset($setting);
            }
        }
        if ($fails->num_rows() > 0) {
            foreach ($fails->result() as $fail) {
                log_message("error", "Portin Failed ==> " . flatme($fail) . "\n");
                $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                    $fail->serviceid
                ));
                $client  = getCompanydataByUserID($service->row()->userid);
                $this->Admin_model->update_services_data('mobile', $fail->serviceid, array(
                    'msisdn_status' => 'PortinFailed',
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                // $this->db->query("update a_services_mobile set msisdn_status='PortOutRequest' where id=?", array($pot->serviceid));
                $this->sendEmailNotification($service->row(), "Number Porting Failed Request on " . $fail->date_event . " SN:" . $service->row()->msisdn, "PortinFailed", $client);
                $this->Admin_model->setEventLogDone($fail->JobId);
                unset($service);
                unset($client);
            }
        }
        if ($portout->num_rows() > 0) {
            foreach ($portout->result() as $pot) {
                log_message("error", "Portout request ==> " . flatme($pot) . "\n");
                $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                    $pot->serviceid
                ));
                $client  = getCompanydataByUserID($service->row()->userid);
                $this->Admin_model->update_services_data('mobile', $pot->serviceid, array(
                    'msisdn_status' => 'PortOutRequest',
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                // $this->db->query("update a_services_mobile set msisdn_status='PortOutRequest' where id=?", array($pot->serviceid));
                $this->sendEmailNotification($service->row(), "Number PortOut Request on " . $pot->date_event . " SN:" . $service->row()->msisdn, "Request", $client);
                $this->Admin_model->setEventLogDone($pot->JobId);
                unset($service);
                unset($client);
            }
        }
        if ($code_rejected->num_rows() > 0) {
            foreach ($code_rejected->result() as $cr) {
                log_message("error", "Porin code rejected ==> " . flatme($cr) . "\n");
                $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                    $cr->serviceid
                ));
                $client  = getCompanydataByUserID($service->row()->userid);
                $this->Admin_model->update_services_data('mobile', $cr->serviceid, array(
                    'msisdn_status' => 'PortInValidateVerificationCodeRejected',
                    'pod_counter' => '0',
                    'porting_remark' => lang('Portin Validation Code Request on') .' '. $cr->date_event . ' '.lang('Number').' '. $service->row()->msisdn . ' '.lang('was invalid'),
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                //$this->db->query("update a_services_mobile set msisdn_status='PortinRejected' where id=?", array($cr->serviceid));
                $this->Admin_model->setEventLogDone($cr->JobId);
                $this->send_porting($cr->serviceid, 'portin_code_rejected');
                if ($send) {
                    $this->sendEmailNotification($service->row(), "Portin Validation Code Request on " . $cr->date_event . " Number:" . $service->row()->msisdn . " was invalid", "CodeRejected", $client);
                }
                unset($service);
                unset($client);
            }
        }
        if ($verificationFailed->num_rows() > 0) {
            foreach ($verificationFailed->result() as $vf) {
                log_message("error", "Portin Verification failed ==> " . flatme($vf) . "\n");
                $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                    $vf->serviceid
                ));
                $client  = getCompanydataByUserID($service->row()->userid);
                $this->Admin_model->update_services_data('mobile', $vf->serviceid, array(
                    'msisdn_status' => 'PortinRejected',
                    'pod_counter' => '0',
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                //$this->db->query("update a_services_mobile set msisdn_status='PortinPending' where id=?", array($vf->serviceid));
                $this->sendEmailNotification($service->row(), "Number Portin Request on " . $vf->date_event . " Number:" . $service->row()->msisdn . " was Failed", "PortinRejected Code Invalid", $client);
                $this->Admin_model->setEventLogDone($vf->JobId);
                $this->send_porting($vf->serviceid, 'portin_code_rejected');
                unset($service);
                unset($client);
            }
        }
        if ($rejects->num_rows() > 0) {
            foreach ($rejects->result() as $reject) {
                //echo date('Y-m-d H:i:s')." ==> Reject \n";
                $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                    $reject->serviceid
                ));
                log_message("error", "Rejection : ==> " . flatme($reject) . "\n");
                //log_message("error", "Rejection : ==> " . flatme($reject) . "\n");
                $this->load->library('artilium', array('companyid' => $service->row()->companyid));
                //print_r($reject);
                $list = $this->artilium->GetParametersCLI($reject->SubscriptionId);
                //print_r($list);
                $remark =  false;
                //log_message("error", "Portin status : ==> " . print_r($list->ParameterResult->Parameters->Parameter, true));



                if ($list->ParameterResult->Result == "0") {
                    foreach ($list->ParameterResult->Parameters->Parameter as $rx) {
                        if ($rx->ParameterId == "20514") {
                            $remark = $rx->ParameterValue;
                        }
                    }
                }

                unset($this->artilium);


                $client  = getCompanydataByUserID($service->row()->userid);
                $updates  = array(
                    'msisdn_status' => 'PortinRejected',
                    'pod_counter' => '0',
                    'date_modified' => date('Y-m-d H:i:s')
                );

                if ($remark) {
                    $patch = explode(';', $remark);
                    if ($patch) {
                        $final_remark = explode(':', $patch[0]);
                        if (trim($final_remark[0]) == "DisapprovalReason") {
                            $remark = str_replace("'", "", $final_remark[1]);
                        }
                    }

                    $updates['porting_remark'] = $remark;
                }


                unset($remark);
                $this->Admin_model->update_services_data('mobile', $reject->serviceid, $updates);
                //  $this->db->query("update a_services_mobile set msisdn_status='PortinRejected' where id=?", array($reject->serviceid));
                $this->Admin_model->setEventLogDone($reject->JobId);
                $this->send_porting($reject->serviceid, 'portin_rejected');
                if ($send) {
                    $this->sendEmailNotification($service->row(), "Number Porting has been Rejected on " . $reject->date_event . " Number:" . $service->row()->msisdn, "PortinRejected", $client);
                }
                logAdmin(array(
                    'companyid' => $client->companyid,
                    'userid' => $service->row()->userid,
                    'serviceid' => $service->row()->serviceid,
                    'user' => 'System',
                    'ip' => '127.0.0.1',
                    'description' => 'Portability Msisdn : ' . $service->row()->msisdn . ' has been rejected.'
                ));
                unset($service);
                unset($client);
            }
        }
        if ($portinit->num_rows() > 0) {
            foreach ($portinit->result() as $init) {
                log_message("error", "Portin Initiation ==> " . flatme($init) . "\n");
                $service = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
                    $init->serviceid
                ));
                $client  = getCompanydataByUserID($service->row()->userid);
                $this->sendEmailNotification($service->row(), "Number Porting has been Rejected on " . $init->date_event . " Number:" . $service->row()->msisdn, "PortinRejected on Initiation", $client);
                $this->Admin_model->update_services_data('mobile', $init->serviceid, array(
                    'msisdn_status' => 'PortinRejected',
                    'pod_counter' => '0'
                ));
                //$this->db->query("update a_services_mobile set msisdn_status='PortinRejected' where id=?", array($init->serviceid));
                $this->Admin_model->setEventLogDone($init->JobId);
                $this->send_porting($init->serviceid, 'portin_rejected');
                unset($service);
                unset($client);
            }
        }
        if ($ported->num_rows() > 0) {
            foreach ($ported->result() as $port) {


                //print_r($port);
                log_message("error", "Ported In successfully ==> " . flatme($port) . "\n");
                $this->load->library('artilium', array(
                    'companyid' => $port->companyid
                ));
                $this->load->library('magebo', array(
                    'companyid' => $port->companyid
                ));
                $order = $this->Admin_model->GetServiceCli($port->serviceid);


                $cli     = $this->artilium->getCLI($order->details->msisdn_sn);
                if ($cli->ContactType2Id != $order->details->msisdn_contactid) {
                    $client = $this->Admin_model->getClient($order->userid);
                    $configuration = globofix($port->companyid);
                    $em = explode('|', $configuration->email_notification);
                    $this->Admin_model->createWHMCSticket(array(
                    'name' => $em->companyname,
                    'email' => $em[0],
                    'subject' => "SIM ".$order->details->msisdn_sim."is not configured correctly ",
                    'message' => "Hello\nThe CLI is still in the orphanage, it seem therewere error on moving this cli, could you move the cli and make sure all bundles and ceiling are configured?\n\nThis is an automate system generated by portal"));
                    unset($client);
                    unset($configuration);
                    unset($em);
                }
                if (substr(trim($port->SubscriptionId), 0, 2) == '32') {
                    $this->artilium->PartialFullBar($order->details, '0');
                    $this->Admin_model->ChangeServiceStatus($order->id, 'Active');
                    $this->Admin_model->ChangeOrdertatus($order->id, 'Active');
                } else {
                    $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($port->SubscriptionId));
                    $this->artilium->UpdateServices(trim($port->SubscriptionId), $pack, '1');
                }


                if ($order->companyid != 56) {
                    if ($order->date_contract > date('m-d-Y')) {
                        $this->Admin_model->update_services($port->serviceid, array(
                        'date_contract' => date('m-d-Y')
                    ));
                        $order = $this->Admin_model->GetServiceCli($port->serviceid);
                        $this->magebo->change_contract_date($order->details->msisdn, date('m-d-Y'));
                        $this->magebo->change_contract_date($order->details->msisdn_sn, date('m-d-Y'));
                    }
                } else {
                    if ($order->status != 'Active') {
                        if ($order->date_contract > date('m-d-Y')) {
                            $this->Admin_model->update_services($port->serviceid, array(
                            'date_contract' => date('m-d-Y')
                        ));
                            $order = $this->Admin_model->GetServiceCli($port->serviceid);
                            $this->magebo->change_contract_date($order->details->msisdn, date('m-d-Y'));
                            $this->magebo->change_contract_date($order->details->msisdn_sn, date('m-d-Y'));
                        }
                    }
                }

                if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                    $addsim = $this->magebo->AddPortinSIMToMagebo($order);
                }
                $this->magebo->addPricingSubscription($order, false, true);
                if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                    $dt = explode('-', $order->date_contract);
                    $this->Api_model->PortIngAction1($order);
                    $this->Api_model->PortIngAction2($order);
                    $this->Api_model->PortIngAction3($order);
                    $this->Api_model->PortIngAction4($order);
                    $this->Api_model->PortIngAction5($order->companyid);
                    $this->magebo->updatePricingMonthbypin($mobile->details->msisdn, $dt[2], $dt[0], $dt[1]);
                    $this->magebo->updatePricingMonthbypin($mobile->details->msisdn_sn, $dt[2], $dt[0], $dt[1]);
                }
                /*
                if ($order->date_contract > date('m-d-Y'))
                {

                $this->magebo->updatePricingMonthbypin($mobile->details->msisdn, $dt[2],  $dt[0], $dt[1]);
                $this->magebo->updatePricingMonthbypin($mobile->details->msisdn_sn, $dt[2],  $dt[0], $dt[1]);
                }
                */
                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
                $this->Admin_model->ChangeServiceStatus($port->serviceid, 'Active');
                $this->Admin_model->ChangeOrdertatus($port->serviceid, 'Active');
                $this->send_welcome_email($port->serviceid);
                $this->db->query("update a_event_log set status='1',accepted_nodate='1' where JobId=?", array(
                    $port->JobId
                ));
                logAdmin(array(
                    'companyid' => $port->companyid,
                    'userid' => $order->userid,
                    'serviceid' => $port->serviceid,
                    'user' => 'System',
                    'ip' => '127.0.0.1',
                    'description' => 'Msisdn : ' . $order->details->msisdn . ' has been successfully ported in to the system'
                ));
                send_growl(array(
                    'message' => 'Msisdn : ' . $order->details->msisdn . ' has been successfully ported in to the system',
                    'companyid' => $port->companyid
                ));
                unset($port);
                unset($order);
                unset($pack);
                unset($this->artilium);
                unset($this->magebo);
            }
        }
    }
    public function DownloadInvoices()
    {
        $this->load->model('Import_model');
        $limit     = 20000;
        $companies = getCompanies();
        foreach ($companies as $cid) {
            if ($cid == 2) {
                foreach (range(1, 70) as $r) {
                    $lastinvoice = $this->Import_model->getLASTinvoices($cid);
                    echo "Query " . $cid . " Last InvoiceNumber: " . $lastinvoice . "\n";
                    $invoices = $this->Import_model->getInvoices($cid, $lastinvoice, $limit);
                    if ($invoices) {
                        $this->Import_model->BatchInsert($invoices);
                    }
                    unset($invoices);
                    unset($lastinvoice);
                }
            } else {
                $lastinvoice = $this->Import_model->getLASTinvoices($cid);
                echo "Query " . $cid . " Last InvoiceNumber: " . $lastinvoice . "\n";
                $invoices = $this->Import_model->getInvoices($cid, $lastinvoice, $limit);
                if ($invoices) {
                    $this->Import_model->BatchInsert($invoices);
                }
                unset($invoices);
                unset($lastinvoice);
            }
        }
    }
    public function sendDDRejected($invoice, $id, $reason = false)
    {
        //getInvoice
        try {
            $this->load->model('Admin_model');
            $client                = $this->Admin_model->getClient($id);
            $this->data['setting'] = globofix($client->companyid);
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    //'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $this->load->library('magebo', array(
                'companyid' => $client->companyid
            ));
            $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
            $body        = getMailContent('directdebit_rejected', $client->language, $client->companyid);
            // $body = $this->load->view('email/content', $this->data, true);
            $body        = str_replace('{$name}', format_name($client), $body);
            $body        = str_replace('{$base_url}', url_to_domain($this->data['setting']->base_url), $body);
            $body        = str_replace('{$mInvoiceAmount}', number_format($invoice->mInvoiceAmount, 2), $body);
            $body        = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
            $body        = str_replace('{$userid}', $client->mvno_id, $body);
            $body        = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
            $body        = str_replace('{$clientid}', $client->mvno_id, $body);
            $body        = str_replace('{$cInvoiceReference}', $invoice_ref, $body);
            $body        = str_replace('{$cRejectReason', $reason, $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            // $this->email->bcc('mail@simson.one');
            $subject = getSubject('directdebit_rejected', $client->language, $client->companyid);
            $subject = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $subject);
            $subject = str_replace('{$Companyname}', $invoice->iInvoiceNbr, $subject);
            $this->email->subject($subject);
            //$this->email->subject(lang("Your New Password"));
            $this->email->message($body);
            if (!isTemplateActive($client->companyid, 'directdebit_rejected')) {
                log_message('error', 'Template directdebit_rejected is disabled sending aborted');
            } else {
                if ($this->email->send()) {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email,
                        'subject' => $subject,
                        'message' => $body
                    ));
                    return "Reminder 1 rejection has been sent to " . $client->email . "\n";
                } else {
                    return "Reminder 1 rejection has not been  sent to " . $client->email . "\n";
                }
            }
        } catch (Exception $e) {
            return "Remnder 1 rejection has not been  sent to " . $client->email . "\n";
            mail('mail@simson.one', "Exception RejectSepa ", "Message: " . $e->getMessage());
        }
    }
    public function TicketsReport()
    {
        $this->db = $this->load->database('whmcs', true);
        $q        = $this->db->query("select * from tbltickets where status not in ('Closed', 'Resolved','Answered')");
        echo "We Have " . $q->num_rows() . " Open Ticket\n";
        sleep(10);
        foreach ($q->result() as $row) {
            print_r($row);
        }
    }
    public function send_portinlist()
    {
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from a_admin where request_export_portinlist = 1");
        $this->load->library('xlswriter');
        $header = array(
            'MVNO ID' => 'string',
            'SN' => 'string',
            'AccountNumber' => 'string',
            'MSISDN' => 'string',
            'Status' => 'string',
            'EnteredDate' => 'string',
            'ActionDate' => 'string',
            'Error.ID' => 'string',
            'ErrorValue' => 'string',
            'ErrorComment' => 'string'
        );
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $this->db->query("update a_admin set request_export_portinlist=? where id=?", array(
                    '0',
                    $row->id
                ));
                $this->load->library('artilium', array(
                    'companyid' => $row->companyid
                ));
                $list = $this->artilium->GetPendingPortIns();
                foreach ($list as $l) {
                    $lst[] = array(
                        'MVNOID' => getMvnoidbySN(trim($l['SN']), $l['MSISDN']),
                        'SN' => trim($l['SN']),
                        'AccountNumber' => $l['AccountNumber'],
                        'MSISDN' => $l['MSISDN'],
                        'Status' => $l['Status'],
                        'EnteredDate' => $l['EnteredDate'],
                        'ActionDate' => $l['EnteredDate'],
                        'Error.ID' => $l['Error.ID'],
                        'ErrorValue' => $l['ErrorValue'],
                        'ErrorComment' => $l['ErrorComment']
                    );
                }
                $this->xlswriter->setAuthor('Simson Parlindungan');
                $this->xlswriter->setTitle('Cdr Export');
                $this->xlswriter->setCompany('United Telecom');
                $this->xlswriter->writeSheet($lst, 'Sheet1', $header);
                $file = '/tmp/' . $row->id . '_' . time() . '_porting_list.xlsx';
                $this->xlswriter->writeToFile($file);
                $this->send_xls($file, $row);
                // mail($row->email, 'Portin Pending List', print_r($list, true));
                unset($this->artilium);
            }
        }
    }
    public function send_xls($file, $admin)
    {
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'html';
        $config['charset']  = 'utf-8';
        $config['wordwrap'] = true;
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = "Hello " . $admin->firstname . ",<br>Your File is Ready on this email attachment<br><br>Please use export wisely otherwise your account might be blocked temporary<br>Regards<br>United Telecom";
        $this->email->set_newline("\r\n");
        $this->email->from("noreply@united-telecom.be", "United Backend");
        $this->email->to($admin->email);
        $subject = "Export Portin Pending List";
        //$this->email->bcc($_SESSION['email']);
        $this->email->subject($subject);
        $this->email->attach($file);
        $this->email->message($body);
        if ($this->email->send()) {
        }
    }

    public function get_porting_status_x($companyid, $id)
    {
        $cnt     = 0;
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getServiceCli($id);

        $this->load->library('artilium', array(
            'companyid' => $service->companyid
        ));
        if (!empty($service->details->date_ported)) {
            echo json_encode(array(
                'result' => true,
                'date' => $service->details->date_ported
            ));
        } else {
            $list = $this->artilium->GetParametersCLI($service->details->msisdn_sn);
            echo "printing List parameters";
            print_r($list);
            if ($list->ParameterResult->Result == "0") {
                foreach ($list->ParameterResult->Parameters->Parameter as $row) {
                    if ($row->ParameterId == 20526) {
                        if (!empty($row->ParameterValue)) {
                            log_message('error', $companyid.' Cron portindate found '.$row->ParameterValue. " for ".$service->details->msisdn_sn);
                            $ActionDate = $row->ParameterValue;
                            $cnt        = $cnt + 1;
                        }
                    }
                }
            }
            echo "counter ".$cnt;
            if ($cnt > 0) {
                $this->Admin_model->update_services_data('mobile', $id, array(
                    'date_ported' => str_replace('/', '-', $ActionDate)
                ));
                echo json_encode(array(
                    'result' => true,
                    'date' => str_replace('/', '-', $ActionDate)
                ));
                $cnt = 0;
            } else {
                echo json_encode(array(
                    'result' => false
                ));
            }
        }
        unset($this->artilium);
    }
    public function get_porting_status($companyid, $id)
    {
        $this->load->model('Admin_model');
        $cnt     = 0;
        $service = $this->Admin_model->getServiceCli($id);
        //print_r($service);
        $this->load->library('artilium', array(
            'companyid' => $service->companyid
        ));

        if (!empty($service->details->date_ported)) {
            $datex = explode(' ', $service->details->date_ported);
            $dd    = explode('-', $datex[0]);
            //print_r($dd);
            //New Implementation requested by Michiel @ 2019-05-23 only delta no caiway
            if ($service->companyid != 56) {
                $this->update_contract_date($id, $service->companyid, $dd[1] . '-' . $dd[2] . '-' . $dd[0]);
            } else {
                if ($service->status != "Active") {
                    $this->update_contract_date($id, $service->companyid, $dd[1] . '-' . $dd[2] . '-' . $dd[0]);
                }
            }
           
            // $this->magebo->change_contract_date($service->details->msisdn, $dd[1].'-'.$dd[2].'-'.$dd[0]);
            // $this->magebo->change_contract_date($service->details->msisdn_sn, $dd[1].'-'.$dd[2].'-'.$dd[0]);
            unset($order);
        } else {
            $list = $this->artilium->GetParametersCLI($service->details->msisdn_sn);
            if ($list->ParameterResult->Result == "0") {
                foreach ($list->ParameterResult->Parameters->Parameter as $row) {
                    if ($row->ParameterId == 20526) {
                        print_r($row);
                        if (!empty($row->ParameterValue)) {
                            log_message('error', 'Cron portindate found '.$row->ParameterValue. " for ".$service->details->msisdn_sn);
                            // sleep(4);
                            $ActionDate = $row->ParameterValue;
                            $cnt        = $cnt + 1;
                        }
                    }
                }
            }
            if ($cnt > 0) {
                $this->Admin_model->update_services_data('mobile', $id, array(
                    'date_ported' => str_replace('/', '-', $ActionDate)
                ));
                $datex = explode(' ', str_replace('/', '-', $ActionDate));
                $dd    = explode('-', $datex[0]);
                //New Implementation requested by Michiel @ 2019-05-23 only delta no caiway
                if ($service->companyid != 56) {
                    $this->update_contract_date($id, $service->companyid, $dd[1] . '-' . $dd[2] . '-' . $dd[0]);
                } else {
                    if ($service->status != "Active") {
                        $this->update_contract_date($id, $service->companyid, $dd[1] . '-' . $dd[2] . '-' . $dd[0]);
                    }
                }
                return array(
                    'result' => true,
                    'date' => str_replace('/', '-', $ActionDate)
                );
            } else {
                return array(
                    'result' => false
                );
            }
        }
    }
    public function reminder_portin_code($companyid)
    {
        try {
            echo date('Y-m-d H:i:s') . "      Starting Cron reminder portin code\n";
            //portin_reminder_verification
            $this->load->library('artilium', array(
                'companyid' => $companyid
            ));
            $this->load->model('Admin_model');
            //$this->load->library('artilium', array(COMPANYID, PLATFORM));
            $dd   = array();
            $list = $this->artilium->GetPendingPortIns();
            //print_r($list);
            //exit;
            if (!empty($list)) {
                // mail('mail@simson.one', 'status',print_r($list, true)); 2019/03/01
                foreach ($list as $key => $row) {
                    if (!in_array(trim($row['Status']), array(
                        "CANCELED",
                        "Port in Canceled"
                    ))) {
                        if (in_array($row["Status"], array(
                            "Port in Wait for verificationcode",
                            "Port in Not yet complete"
                        ))) {
                            if (!strpos($row['EnteredDate'], date('Y/m/d'))) {
                                print_r($row);
                                $mobile = $this->Admin_model->getServiceBySNDetail(trim($row['SN']));
                                if ($mobile->portin_reminder < 3) {
                                    $this->Admin_model->updateReminderCounter($mobile->id, $mobile->portin_reminder + 1);
                                    echo $this->sendcustom($companyid, 'portin_reminder_verification', $this->Admin_model->getClientBySN($row['SN'], $companyid), $mobile);
                                }
                                sleep(5);
                            }
                        }
                    }
                }
            } else {
                echo "List Empty\n";
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception  reminder_portin_code ", "Message: " . $e->getMessage());
        }
    }
    public function update_contract_date($serviceid, $companyid, $date_contract)
    {
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($serviceid);
        $date   = $date_contract;

        $fmt = explode('-', $date);
        $this->db->where('id', $serviceid);
        $this->db->update('a_services', array(
            'date_contract' => $date,
            'date_contract_formated'=> $fmt[2].'-'.$fmt[0].'-'.$fmt[1]
        ));
        $this->magebo->change_contract_date($mobile->details->msisdn, $date);
        $this->magebo->change_contract_date($mobile->details->msisdn_sn, $date);
        if (!empty($mobile->iGeneralPricingIndex)) {
            $dt = explode('-', $date);
            $this->magebo->updatePricingMonthbypin($mobile->details->msisdn, $dt[2], $dt[0], $dt[1]);
            $this->magebo->updatePricingMonthbypin($mobile->details->msisdn_sn, $dt[2], $dt[0], $dt[1]);
        }
        logAdmin(array(
            'companyid' => $companyid,
            'user' => 'System',
            'serviceid' => $serviceid,
            'userid' => $mobile->userid,
            'ip' => '127.0.0.1',
            'description' => 'Portin Accepted and system changes Contract Date from  ' . $mobile->date_contract . ' to ' . $date_contract
        ));
    }

    public function report_error_address()
    {
        foreach (array(53,54,56) as  $companyid) {
            $q = $this->db->query("select * from a_clients where housenumber is null and companyid=?", array($companyid));
            if ($q->num_rows()>0) {
                log_message('error', 'found '.$q->num_rows().' customer with empty housenumber for companyid: '.$companyid);
                $this->send_report_address_error($companyid, $q->result());
            }
        }
    }

    public function report_package_changes($date=false)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }
        foreach (array(53,54,56) as  $companyid) {
            $q = $this->db->query("select a.date_request,a.executor,a.requestor,a.serviceid,a.id,a.date_commit,a.new_pid, a.old_pid,concat(c.firstname,' ',c.lastname) as customername,c.id as userid, c.mvno_id,(select concat(name, ' (', round(a.new_price,2),')') as price from a_products c where c.id=a.new_pid) as newname, (select name from a_products c where c.id=a.old_pid) as oldname, case when g.type = 'mobile' THEN d.msisdn when g.type = 'xdsl' then e.circuitid when g.type = 'voip' then f.cli else 'Unknown' end as domain from a_subscription_changes a left join a_services g on g.id=a.serviceid left join a_clients c on c.id=g.userid left join a_services_mobile d on d.serviceid=a.serviceid left join a_services_xdsl e on d.serviceid=a.serviceid left join a_services_voip f on d.serviceid=a.serviceid where a.status = '0' and g.companyid=? and a.date_commit=? group by a.id order by a.date_commit", array($companyid, $date));
            if ($q->num_rows()>0) {
                log_message('error', 'found '.$q->num_rows().' customer That has been processed for package changes: '.$companyid);
                $this->send_report_package_changes($companyid, $q->result());
            } else {
                log_message('error', 'No Packages changes today');
            }
        }
    }
    public function send_report_package_changes($companyid, $data)
    {
        $this->data['setting'] = globofix($companyid);
        $url = getCompanyUrl($companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    //'starttls' => true,
                    'wordwrap' => true
                );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $content = "Hello,<br><br>You have <font color='red'><b>".count($data)."</b></font> Downgrade/Upgrade Subscription that has been processed<br><br>Please verify if their package and bundles reflected to the new one<br /><br>";
        $content .= "<table border='1' colspan='1'>";
        $content .= "<thead><tr><th width='20%'>MSISDN</th><th width='20%'>Customer</th><th width='30%'>New Packages</th><th width='30%'>Old Packages</th></tr>";

        foreach ($data as $row) {
            print_r($row);
            $content .="<tr><td><b>".$row->domain."</b></td><td><a href='".$url."admin/subscription/detail/".$row->serviceid."'>".$row->customername."</a></td><td><a href='".$url."admin/subscription/detail/".$row->serviceid."'>".$row->newname."</a></td><td>".$row->oldname."</td></tr>";
        }
        $content .= "</table><br><br>This is an automate system, please do not reply<br><br>Thank you<br><br>United Telecom<br>European Portal V1";
        $body =  getMailContentx($content, 'english', $companyid);
        print_r($body);
        $subject = "Information: Package Changes Report";
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $email = explode("|", $this->data['setting']->email_notification);
        $this->email->to($email);
        $this->email->bcc("it@united-telecom.be");
        if ($this->email->send()) {
        } else {
            log_message('error', 'Failed to send daily incorrect housenumber customer '.$companyid);
        }
    }

    public function send_report_address_error($companyid, $data)
    {
        $this->data['setting'] = globofix($companyid);
        $url = getCompanyUrl($companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                //'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $content = "Hello,<br><br>You have <font color='red'><b>".count($data)."</b></font> customer(s) which has incorrect address information<br><br>Please Edit the following client because the house number is not separated from street name<br /><br>";
        $content .= "<table border='1' colspan='1'>";
        $content .= "<thead><tr><th width='10%'>MVNOID</th><th width='15%'>ARTILIUM ID</th><th width='35%'>Customer</th><th width='40%'>Address</th></tr>";

        foreach ($data as $row) {
            print_r($row);
            $content .="<tr><td>".$row->mvno_id."</td><td>".$row->id."</td><td><a href='".$url."admin/client/edit/".$row->id."'>".$row->initial." ".$row->firstname." ".$row->lastname."</a></td><td>".$row->address1." ".$row->alphabet."</td></tr>";
        }
        $content .= "</table><br><br>You can simply click on the name to edit them on the spot.<br><br>This Report will be sent every day as long as you still have incorrect information in our system<br><br>Thank you<br><br>United Telecom<br>European Portal V1";
        $body =  getMailContentx($content, 'english', $companyid);
        print_r($body);
        $subject = "Action Required: Customer Data incorrect";
        $this->email->subject($subject);
        $this->email->message($body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $email = explode("|", $this->data['setting']->email_notification);
        $this->email->to($email);
        if ($this->email->send()) {
        } else {
            log_message('error', 'Failed to send daily incorrect housenumber customer '.$companyid);
        }
    }


    public function sendcustom($companyid, $template, $client, $mobile = false)
    {
        if (!$client) {
            return "client not found\n";
        }
        error_reporting(1);
        $this->data['setting'] = globofix($companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                //'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent($template, $client->language, $client->companyid);
        if (!$body) {
            return json_encode(array(
                'result' => false,
                'Body is empty'
            ));
        }
        $body = str_replace('{$name}', format_name($client), $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        //$this->email->bcc('mail@simson.one');
        $subject = getSubject($template, $client->language, $companyid);
        if (!$subject) {
            return json_encode(array(
                'result' => false,
                'subject is empty'
            ));
        }
        if ($mobile) {
            //$body = str_replace('{$name}', $client->salutation . ' ' . $client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $body);
            $body    = str_replace('{$base_url}', url_to_domain($this->data['setting']->base_url), $body);
            //$body    = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
            $body    = str_replace('{$msisdn}', '0' . substr($mobile->msisdn, 2), $body);
            $body    = str_replace('{$msisdn_sim}', $mobile->msisdn_sim, $body);
            $body    = str_replace('{$msisdn_puk1}', $mobile->msisdn_puk1, $body);
            $body    = str_replace('{$msisdn_puk2}', $mobile->msisdn_puk2, $body);
            $subject = str_replace('{$base_url}', url_to_domain($this->data['setting']->base_url), $subject);
            $subject = str_replace('{$msisdn}', $mobile->msisdn, $subject);
            $subject = str_replace('{$msisdn_sim}', $mobile->msisdn_sim, $subject);
            $subject = str_replace('{$msisdn_puk1}', $mobile->msisdn_puk1, $subject);
            $subject = str_replace('{$msisdn_puk2}', $mobile->msisdn_puk2, $subject);
        }
        $this->email->subject($subject);
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, $template)) {
            log_message('error', 'Template ' . $template . ' is disabled sending aborted');
            return json_encode(array(
                'result' => true
            ));
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'to' => $client->email,
                    'subject' => $subject,
                    'message' => $body,
                    'companyid' => $companyid
                ));
                return json_encode(array(
                    'result' => true
                ));
            } else {
                return json_encode(array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                ));
            }
        }
    }
    public function export_ciot($companyid)
    {
        if (!file_exists('/home/mvno/documents/' . $companyid . '/ciot/')) {
            mkdir('/home/mvno/documents/' . $companyid . '/ciot/');
        }
        foreach (array('TrendCall','HayUnited') as $ciot) {
            $myFile = $ciot.'_'.$companyid . '_ciot_export_' . date('YmdHis') . '.csv';
            if ($ciot  == "TrendCall") {
                $q      = $this->db->query("select a.companyname as Bedrijfsnaam, a.lastname as Achternaam, a.salutation as Voorvoegsels, a.initial as Voorletters, a.address1 as Straatnaam, a.housenumber as Huisnummer, a.alphabet as Huisnummertoevoeging, a.postcode as Postcode, '' as PostcodeBL, a.city as Woonplaats, '' as  LocatieOms, a.country as LAND, 'A' as soortNAW, 'GSM' as soortverbinding, 'ABON' as DienstSoort, 'TrendCall' as DienstAanbieder,'TRENDCALL' as NetwerkAanbieder, a.date_created as Generatiedatum, c.date_contract as Ingangsdatumtijd, b.msisdn as Telefoonnummer from a_services_mobile b left join a_clients a on a.id=b.userid left join a_services c on c.id=b.serviceid where a.status in('Active') and b.companyid=? and b.msisdn != '' and a.agentid = 1000 and c.status = 'Active' order by b.msisdn asc ", array(
                    $companyid
                ));
            } else {
                $q      = $this->db->query("select a.companyname as Bedrijfsnaam, a.lastname as Achternaam, a.salutation as Voorvoegsels, a.initial as Voorletters, a.address1 as Straatnaam, a.housenumber as Huisnummer, a.alphabet as Huisnummertoevoeging, a.postcode as Postcode, '' as PostcodeBL, a.city as Woonplaats, '' as  LocatieOms, a.country as LAND, 'A' as soortNAW, 'GSM' as soortverbinding, 'ABON' as DienstSoort, 'HayUnited' as DienstAanbieder,'TRENDCALL' as NetwerkAanbieder, a.date_created as Generatiedatum, c.date_contract as Ingangsdatumtijd, b.msisdn as Telefoonnummer from a_services_mobile b left join a_clients a on a.id=b.userid left join a_services c on c.id=b.serviceid where a.status in('Active') and b.companyid=? and b.msisdn != '' and a.agentid != 1000 and c.status = 'Active' order by b.msisdn asc ", array(
                    $companyid
                ));
            }


            $fh = fopen('/home/mvno/documents/' . $companyid . '/ciot/' . $myFile, 'a') or die("can't open file");

            fputcsv($fh, array("Bedrijfsnaam","Achternaam","Voorvoegsels","Voorletters","Straatnaam","Huisnummer"," Huisnummertoevoeging","Postcode","PostcodeBL","Woonplaats","LocatieOms","Land","soortNAW ","soortverbinding","DienstSoort","DienstAanbieder","NetwerkAanbieder","Generatiedatum","Ingangsdatumtijd","Telefoonnummer"), "\t");


            foreach ($q->result_array() as $fields) {
                if ($fields['LAND'] == "NL") {
                    $fields['Postcode']   = str_replace(' ', '', $fields['Postcode']);
                    $fields['PostcodeBL'] = '';
                } else {
                    $fields['PostcodeBL'] = $fields['Postcode'];
                    $fields['Postcode']   = '';
                }
                $p                          = explode('-', $fields['Ingangsdatumtijd']);
                //$fields['Ingangsdatumtijd'] = $p[2] . '-' . $p[0] . '-' . $p[1];
                $fields['Generatiedatum'] = date('Y-m-d').'T'.date('H:i:s');
                if ($fields['Einddatumtijd']) {
                    $fields['Einddatumtijd'] = $fields['Einddatumtijd'].'T00:00:00';
                }

                if ($p) {
                    $fields['Ingangsdatumtijd'] = $p[2] . '-' . $p[0] . '-' . $p[1].'T00:00:00';
                } else {
                    $fields['Ingangsdatumtijd'] = "";
                }

                fputcsv($fh, $fields, "\t");
            }
            unset($fields);
            fclose($fh);
            $file[$myFile] = '/home/mvno/documents/' . $companyid . '/ciot/' . $myFile;
        }

        /*$this->data['setting'] = globofix($companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $body = "<p>Hello</p><p>Export ID:#" . time() . "</p>Filenames: ";
        foreach ($file as $key => $n) {
            $body .= "<br>".$key."<br />";
        }
        $body .= "<br>RequestDate: " . date('Y-m-d') . "<br>Type: CIOT Report<p>Have been created</p><p>Please find the file in this email attachments</p>Kind Regards<br /><br />United";
        $this->email->clear(true);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@united-telecom.be');
        $this->email->to('lex.v.d.hondel@trendcall.com');
        $this->email->bcc('mail@simson.one');
        $this->email->subject('United Portal' . " CIOT Daily Report");
        $this->email->message($body);
        */
        // shell_exec('zip '.$setting->DOC_PATH . $r->companyid . '/exports/'.$fileName.'.zip '.$setting->DOC_PATH . $r->companyid . '/exports/' . $fileName);

        foreach ($file as $key => $f) {
            echo $f." ".$key."\n";
            $this->send_ciot($companyid, $f, $key);
            $this->teams->send_msg("CIOT reports has been sent", "<br /><br /> File:<br />" .$key);
            //$this->email->attach($f, 'attachment', $key);
        }

        /*
        if ($this->email->send()) {
            echo date('Y-m-d H:i:s') . " CIOT Report email has been sent\n";
        }
        */
    }

    public function export_ciot_delta()
    {
        foreach (array('Delta','Caiway') as $ciot) {
            if ($ciot  == "Caiway") {
                $companyid =  56;
                $myFile = $ciot.'_'.$companyid . '_ciot_export_' . date('YmdHis') . '.csv';
                $q      = $this->db->query("select a.companyname as Bedrijfsnaam, a.lastname as Achternaam, a.salutation as Voorvoegsels, a.initial as Voorletters, a.address1 as Straatnaam, a.housenumber as Huisnummer, a.alphabet as Huisnummertoevoeging, a.postcode as Postcode, '' as PostcodeBL, a.city as Woonplaats, '' as  LocatieOms, a.country as LAND, 'A' as soortNAW, 'GSM' as soortverbinding, 'ABON' as DienstSoort, 'Caiway' as DienstAanbieder,'CAIWAY' as NetwerkAanbieder, a.date_created as Generatiedatum, c.date_contract as Ingangsdatumtijd, b.msisdn as Telefoonnummer from a_services_mobile b left join a_clients a on a.id=b.userid left join a_services c on c.id=b.serviceid where a.status in('Active') and b.companyid=? and b.msisdn != ''  and c.status = 'Active' order by b.msisdn asc ", array(
                    $companyid
                ));
            } else {
                $companyid =  53;
                $myFile = $ciot.'_'.$companyid . '_ciot_export_' . date('YmdHis') . '.csv';
                $q      = $this->db->query("select a.companyname as Bedrijfsnaam, a.lastname as Achternaam, a.salutation as Voorvoegsels, a.initial as Voorletters, a.address1 as Straatnaam, a.housenumber as Huisnummer, a.alphabet as Huisnummertoevoeging, a.postcode as Postcode, '' as PostcodeBL, a.city as Woonplaats, '' as  LocatieOms, a.country as LAND, 'A' as soortNAW, 'GSM' as soortverbinding, 'ABON' as DienstSoort, 'DELTA' as DienstAanbieder,'DELTA' as NetwerkAanbieder, a.date_created as Generatiedatum, c.date_contract as Ingangsdatumtijd, b.msisdn as Telefoonnummer from a_services_mobile b left join a_clients a on a.id=b.userid left join a_services c on c.id=b.serviceid where a.status in('Active') and b.companyid=? and b.msisdn != '' and c.status = 'Active' order by b.msisdn asc ", array(
                    $companyid
                ));
            }
            if (!file_exists('/home/mvno/documents/' . $companyid . '/ciot/')) {
                mkdir('/home/mvno/documents/' . $companyid . '/ciot/');
            }

            $fh = fopen('/home/mvno/documents/' . $companyid . '/ciot/' . $myFile, 'a') or die("can't open file");

            fputcsv($fh, array("Bedrijfsnaam","Achternaam","Voorvoegsels","Voorletters","Straatnaam","Huisnummer"," Huisnummertoevoeging","Postcode","PostcodeBL","Woonplaats","LocatieOms","Land","soortNAW ","soortverbinding","DienstSoort","DienstAanbieder","NetwerkAanbieder","Generatiedatum","Ingangsdatumtijd","Telefoonnummer"), "\t");


            foreach ($q->result_array() as $fields) {
                if ($fields['LAND'] == "NL") {
                    $fields['Postcode']   = str_replace(' ', '', $fields['Postcode']);
                    $fields['PostcodeBL'] = '';
                } else {
                    $fields['PostcodeBL'] = $fields['Postcode'];
                    $fields['Postcode']   = '';
                }
                $p                          = explode('-', $fields['Ingangsdatumtijd']);
                //$fields['Ingangsdatumtijd'] = $p[2] . '-' . $p[0] . '-' . $p[1];
                $fields['Generatiedatum'] = date('Y-m-d').'T'.date('H:i:s');
                if ($fields['Einddatumtijd']) {
                    $fields['Einddatumtijd'] = $fields['Einddatumtijd'].'T00:00:00';
                }

                if ($p) {
                    $fields['Ingangsdatumtijd'] = $p[2] . '-' . $p[0] . '-' . $p[1].'T00:00:00';
                } else {
                    $fields['Ingangsdatumtijd'] = "";
                }

                fputcsv($fh, $fields, "\t");
            }
            unset($fields);
            fclose($fh);
            $file[$myFile] = '/home/mvno/documents/' . $companyid . '/ciot/' . $myFile;
        }

        /*$this->data['setting'] = globofix($companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $body = "<p>Hello</p><p>Export ID:#" . time() . "</p>Filenames: ";
        foreach ($file as $key => $n) {
            $body .= "<br>".$key."<br />";
        }
        $body .= "<br>RequestDate: " . date('Y-m-d') . "<br>Type: CIOT Report<p>Have been created</p><p>Please find the file in this email attachments</p>Kind Regards<br /><br />United";
        $this->email->clear(true);
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@united-telecom.be');
        $this->email->to('lex.v.d.hondel@trendcall.com');
        $this->email->bcc('mail@simson.one');
        $this->email->subject('United Portal' . " CIOT Daily Report");
        $this->email->message($body);
        */
        // shell_exec('zip '.$setting->DOC_PATH . $r->companyid . '/exports/'.$fileName.'.zip '.$setting->DOC_PATH . $r->companyid . '/exports/' . $fileName);

        foreach ($file as $key => $f) {
            echo $f." ".$key."\n";
            // $this->send_ciot($companyid, $f, $key);
            $this->teams->send_msg("CIOT reports has been sent", "<br /><br /> File:<br />" .$key);
            //$this->email->attach($f, 'attachment', $key);
        }

        /*
        if ($this->email->send()) {
            echo date('Y-m-d H:i:s') . " CIOT Report email has been sent\n";
        }
        */
    }

    public function send_ciot($companyid, $file, $filename)
    {
        $setting = globofix($companyid);
        $conn_id = ftp_connect($setting->ftp_host, $setting->ftp_port);
        // login with username and password
        $login_result = ftp_login($conn_id, $setting->ftp_user, $setting->ftp_password);

        // upload a file
        if (ftp_put($conn_id, $filename, $file, FTP_ASCII)) {
            echo "successfully uploaded $file\n";
            unlink($file);
        } else {
            echo "There was a problem while uploading $file\n";
        }

        // close the connection
        ftp_close($conn_id);
    }
    public function export_client()
    {
        try {
            $this->load->library('xlswriter');
            $this->load->model('Admin_model');
            $q = $this->db->query("select * from a_request_export where status=? and type_export=? limit 1", array(
                'created',
                'client'
            ));
            if ($q->num_rows() > 0) {
                foreach ($q->result() as $r) {
                    $setting = globofix($r->companyid);
                    if (!file_exists($setting->DOC_PATH . $r->companyid . '/exports')) {
                        mkdir($setting->DOC_PATH . $r->companyid . '/exports', 0777, true);
                    }
                    $this->db->query('update a_request_export set status=? where id=?', array(
                        'processing',
                        $r->id
                    ));
                    $fileName = 'ExportID_' . $r->id . '_Clients-Summary_by_Date_' . date('Y-m-d') . '.xlsx';
                    $header   = array(
                        'ArtiliumID' => 'string',
                        'BillingID' => 'string',
                        'MvnoID' => 'string',
                        'Salutation' => 'string',
                        'Initial' => 'string',
                        'Firstname' => 'string',
                        'Lastname' => 'string',
                        'CompanyName' => 'string',
                        'VAT' => 'string',
                        'Email' => 'string',
                        'Street' => 'string',
                        'Housenumber' => 'string',
                        'Alpahbet' => 'string',
                        'Postcode' => 'string',
                        'City' => 'string',
                        'Country' => 'string',
                        'Language' => 'string',
                        'Phonenumber' => 'string',
                        'Paymentmethod' => 'string',
                        'Due days' => 'string',
                        'Date Birth' => 'string',
                        'Vat Rate' => 'string',
                        'AgentID' => 'string',
                        'Gender' => 'string',
                        'Referral' => 'string',
                        'Location' => 'string',
                        'IBAN' => 'string',
                        'BIC' => 'string',
                        'Madate ID' => 'string',
                        'Mandate Status' => 'string',
                        'Mandate Date' => 'string'
                    );
                    $this->db = $this->load->database('default', true);
                    $q        = $this->db->query("select a.id,a.mageboid,a.mvno_id,a.salutation,a.initial,a.firstname,a.lastname,a.companyname,a.vat,a.email,a.address1,a.housenumber,a.alphabet,a.postcode,a.city,a.country,a.language,a.phonenumber,a.paymentmethod,a.payment_duedays,
    a.date_birth,a.vat_rate,a.agentid,a.gender,a.refferal,a.location from a_clients a where a.companyid=?", array(
                        $r->companyid
                    ));
                    if ($q->num_rows() > 0) {
                        $res = $q->result_array();
                        if ($this->data['setting']->mobile_platform == "ARTA") {
                            foreach ($res as $j) {
                                if ($j['paymentmethod'] == "directdebit") {
                                    $mandate             = $this->Admin_model->getMandateId($j['mageboid']);
                                    $j['iban']           = $mandate->IBAN;
                                    $j['bic']            = $mandate->BIC;
                                    $j['mandate_id']     = $mandate->SEPA_MANDATE;
                                    $j['mandate_status'] = $mandate->SEPA_STATUS;
                                    $j['mandate_date']   = $mandate->SEPA_MANDATE_SIGNATURE_DATE;
                                    $rim[]               = $j;
                                } else {
                                    $j['iban']           = "";
                                    $j['bic']            = "";
                                    $j['mandate_id']     = "";
                                    $j['mandate_status'] = "";
                                    $j['mandate_date']   = "";
                                    $rim[]               = $j;
                                }
                                print_r($j);
                                unset($j);
                            }
                        } else {
                            $rim = $res;
                        }

                        /*
                        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-Disposition: attachment; filename="' . $fileName . '"');
                        */
                        $this->xlswriter->writeSheet($rim, 'Sheet1', $header);
                        unset($rim);
                        $this->xlswriter->setAuthor('Simson Parlindungan');
                        $this->xlswriter->setTitle('Client Export');
                        $this->xlswriter->setCompany('United');
                        $this->xlswriter->writeToFile($setting->DOC_PATH . $r->companyid . '/exports/' . $fileName);
                        //$this->session->set_flashdata('success', 'Downloaded as requested');
                        $this->data['setting'] = globofix($r->companyid);
                        if ($this->data['setting']->smtp_type == 'smtp') {
                            $config = array(
                                'protocol' => 'smtp',
                                'smtp_host' => $this->data['setting']->smtp_host,
                                'smtp_port' => $this->data['setting']->smtp_port,
                                'smtp_user' => $this->data['setting']->smtp_user,
                                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                                'mailtype' => 'html',
                                'charset' => 'utf-8',
                                'wordwrap' => true
                            );
                        } else {
                            $config['protocol'] = 'sendmail';
                            $config['mailpath'] = '/usr/sbin/sendmail';
                            $config['mailtype'] = 'html';
                            $config['charset']  = 'utf-8';
                            $config['wordwrap'] = true;
                        }
                        $body = "<p>Hello</p><p>Export ID:#" . $r->id . "</p>Filename: " . $fileName . "<br>RequestDate: " . $r->date . "<br>Type: " . ucfirst($r->type_export) . "<p>Has been created</p><p>Please find the file in this email attachment</p>Kind Regards<br /><br />United";
                        $this->email->clear(true);
                        $this->email->initialize($config);
                        $this->email->set_newline("\r\n");
                        $this->email->from($setting->smtp_sender, $setting->smtp_name);
                        $this->email->to($r->recipient);
                        //$this->email->to('mail@simson.one');
                        $this->email->subject('United Portal' . " Backup Report");
                        $this->email->message($body);
                        // shell_exec('zip '.$setting->DOC_PATH . $r->companyid . '/exports/'.$fileName.'.zip '.$setting->DOC_PATH . $r->companyid . '/exports/' . $fileName);
                        $this->email->attach($setting->DOC_PATH . $r->companyid . '/exports/' . $fileName, 'attachment', $fileName);
                        if ($this->email->send()) {
                            $this->db->query('update a_request_export set filename=?, status=? where id=?', array(
                                $fileName,
                                'sent',
                                $r->id
                            ));
                            echo "Export file " . $fileName . " has been sent successfully\n";
                            $this->teams->send_msg("Export client reports has been sent", "<br /><br /> File:<br />" .$filename);
                        } else {
                            $this->db->query('update a_request_export set filename=?, status=? where id=?', array(
                                $fileName,
                                'failed',
                                $r->id
                            ));
                            echo print_r($this->email->print_debugger());
                        }
                    } else {
                        //$this->session->set_flashdata('error', 'No data found');
                    }
                    unset($r);
                }
            }
        } catch (Exception $e) {
            mail('mail@simson.one', "Exception Sexport_client ", "Message: " . $e->getMessage());
        }
    }

    public function process_sms_queue()
    {
        $now = time();
        $q = $this->db->query("select * from a_sms_log where status=? LIMIT 10", array('Queued'));
        echo $this->db->last_query();
        if ($q->num_rows()>0) {
            foreach ($q->result() as $row) {
                $d = new DateTime($row->date, new DateTimeZone('Europe/Brussels'));

                if ($now-$d->getTimestamp() >= 7200) {
                    echo "Sending To: ".$row->number." ".$row->message."\n";
                }
            }
        }
    }

    public function subscribe_event($cid)
    {
        echo "Are you sure you want to do this?  Type 'Y' to continue: ";
        $handle = fopen("php://stdin", "r");
        $line = fgets($handle);
        if (trim($line) != 'Y') {
            echo "ABORTING!\n";
            exit;
        }
        $q = $this->db->query("select * from a_mvno where companyid=?", array($cid));
        if ($q->num_rows()>0) {
            $this->load->library('artilium', array('companyid' => $cid));
            print_r($this->artilium->EventSubscribe($q->row()->abbr));
            $this->db->query("update a_configuration set val=1 where companyid=? and name=?", array($cid, 'event_enable'));
        }
    }
    public function terminate_events()
    {
        $this->load->model('Admin_model');
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("SELECT b.status,b.date_terminate,a.* FROM `a_event_log` a left join a_services b on b.id=a.serviceid WHERE a.JobName LIKE 'PortOutRepatriate' and b.date_terminate is NULL and a.Action='PortedOut'");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $dd = explode(' ', $row->date_event);
                $this->Admin_model->update_services($row->serviceid, array(
                    'date_terminate' => $dd[0],
                    'status' => 'Terminated'
                ));
                $this->Admin_model->update_services_data('mobile', $row->serviceid, array(
                    'datacon_ftp' => 0,
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                $mobile = $this->Admin_model->getService($row->serviceid);
                if ($row->status != 'Terminated') {
                    $time = date('H:i:s');
                    $this->load->library('magebo', array(
                        'companyid' => $row->companyid
                    ));
                    $this->load->library('artilium', array(
                        'companyid' => $row->companyid
                    ));
                    $terminate_now = true;
                    $date          = date('Y-m-d') . 'T' . $time;
                    $this->magebo->TerminateRequest($mobile, date('Y-m-d', strtotime("+1 days")) . "T00:00:00");
                    $this->magebo->TerminateComplete($mobile, date('Y-m-d', strtotime("+1 days")));
                    $tcli   = date('Y-m-d') . "T" . $time;
                    $client = $this->Admin_model->getClient($mobile->userid);
                    if ($mobile) {
                        $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($row->SubscriptionId));
                        $this->artilium->UpdateServices(trim($row->SubscriptionId), $pack, '0');
                        $terminate = $this->artilium->TerminateCLI(trim($row->SubscriptionId), $tcli);
                    }
                    unset($dd);
                    unset($this->magebo);
                    unset($this->artilium);
                }
            }
        }
    }
    public function send_sms($message)
    {
    }
    /*public function reset_iris()
    {
        $this->load->model('Cron_model');
        $this->Cron_model->RC_Iriscall(8);
        $this->Cron_model->RC_Iriscall(9);
    }
    public function reset_united()
    {
        $this->load->model('Cron_model');
        foreach (array(
            94,
            95,
            96,
            97
        ) as $row) {
            $this->Cron_model->RC($row);
        }
    }
    */
    public function ResetCredit()
    {
        $this->load->model('Cron_model');
        foreach (array(
            94,
            95,
            96,
            97
        ) as $row) {
            $this->Cron_model->RC($row);
        }
        $this->Cron_model->RCiReachM(1);
        $this->Cron_model->RCEyeMobile(1);
        $this->Cron_model->RC_Iriscall(8);
        $this->Cron_model->RC_Iriscall(9);
    }
    public function add_extra_nextinvoice_titems($msisdn, $amount)
    {
        $this->load->model('Admin_model');
        $effectiveDate     = date('Y-m-01', strtotime('+1 month'));
        $t                 = $_POST['terms'] - 1;
        $date              = date('Y-m-d', strtotime("+" . $t . " months", strtotime($effectiveDate)));
        $_POST['end_date'] = $date;
        $this->load->library('magebo', array(
            'companyid' => $this->session->cid
        ));
        $client   = $this->Admin_model->getClient($_POST['userid']);
        $terms    = 1;
        $option   = (object) array(
            'terms' => $terms,
            'name' => 'Reactivation Fee ' . $msisdn,
            'recurring_total' => $amount
        );
        $contract = date('m-01-Y', strtotime('+1 month'));
        $id       = $this->magebo->addExtraPricing($client, $option, $contract);
        if ($id > 0) {
            //  $this->session->set_flashdata('success', 'Invoice Item has been planned');
        } else {
            //   $this->session->set_flashdata('error', 'This action was blocked');
        }
        //redirect('admin/client/detail/' . $_POST['userid']);
    }
    public function updategeneraLPricing()
    {
        foreach (array(
            54,
            53,
            56
        ) as $companyid) {
            $this->load->library('magebo', array(
                'companyid' => $companyid
            ));
            $q = $this->db->query("select a.id, b.msisdn, a.iGeneralPricingIndex from a_services a left join a_services_mobile b on b.serviceid=a.id where a.status ='Active' and a.companyid=? order by a.id asc", array(
                $companyid
            ));
            foreach ($q->result() as $row) {
                if (substr($row->msisdn, 0, 2) == "32") {
                    $row->msisdn = substr($row->msisdn, 2);
                    echo $row->msisdn . "\n";
                }
                $check = $this->magebo->updateGeneraPricingIndex(trim($row->msisdn), $row->iGeneralPricingIndex);
                if ($check) {
                    echo $row->msisdn . "    " . $row->serviceid . "    " . $row->iGeneralPricingIndex . "  " . $check->iGeneralPricingIndex . "\n";
                    //print_r($check);
                    $this->db->query("update a_services set iGeneralPricingIndex=? where id=?", array(
                        $check->iGeneralPricingIndex,
                        $row->id
                    ));
                }
            }
            unset($this->magebo);
        }
    }
    public function addPayment($filename, $assign = 1)
    {
        if (file_exists(APPPATH . 'queue/' . $filename)) {
            $json = file_get_contents(APPPATH . 'queue/' . $filename);
            $j    = json_decode($json);
            $this->load->library('magebo', array(
                'companyid' => 54
            ));
            $invoices = $this->magebo->getBankDetails($j->BatchPaymentId);
            //print_r($j);
            //echo date("m/d/Y", strtotime($j->date_transaction));
            //exit;
            if ($invoices) {
                echo date('Y-m-d H:i:s') . "- Processing Directdebit payments " . $j->BatchPaymentId . "\n";
                foreach ($invoices as $invoice) {
                    $text .= date('Y-m-d H:i:s') . "- " . $invoice->iInvoiceNbr . "\n";
                    if (trim($invoice->cTypeDescription) == "SEPA FIRST") {
                        $paymentform = 'SEPA FIRST';
                    } else {
                        $paymentform = 'SEPA REOCCURRING';
                    }
                    $res = $this->magebo->apply_payment(array(
                        'cPaymentForm' => $paymentform,
                        'mPaymentAmount' => $invoice->mBankFileAmount,
                        'cPaymentFormDescription' => "Sepa DD Payment " . $invoice->iInvoiceNbr,
                        'dPaymentDate' => date("m/d/Y", strtotime($j->date_transaction)),
                        'iInvoiceNbr' => $invoice->iInvoiceNbr,
                        'step' => 1,
                        'iAddressNbr' => $invoice->iAddressNbr,
                        'cPaymentRemark' => $j->fileid
                    ));
                    echo date("Y-m-d H:i:s") . "      Processing :" . $paymentform . " " . $invoice->mBankFileAmount . " " . $invoice->iInvoiceNbr . " " . $invoice->iAddressNbr . "\n";
                    unset($res);
                }
            }
            /*

            if (empty($postfields['iAddressNbr']) && empty($postfields['dPaymentDate']) && empty($postfields['mPaymentAmount']) && empty($postfields['cPaymentForm']) && empty($postfields['cPaymentFormDescription']) && empty($postfields['cPaymentRemark']) && empty($postfields['iInvoiceNbr'])) {
            echo json_encode(array('result' => false, 'message' => 'Not complete'));

            }

            $queryInsertPayment = "execute spInsertPayment '" . $postfields['iAddressNbr'] . "', '" . $postfields['dPaymentDate'] . "', '" . $postfields['mPaymentAmount'] . "', '" . $postfields['cPaymentForm'] . "', '" . $postfields['cPaymentFormDescription'] . "', '" . $postfields['cPaymentRemark'] . "', '0'";
            $this->db = $this->load->database('magebov5', true);
            $result = $this->db->query($query);
            $lineInsertPayment =  $result->row_array();
            $NewPayment = $lineInsertPayment['iNewPaymentNbr'];


            if ($NewPayment > 0) {
            $queryInsertPayment2 = "execute spAssignInvoicePayment '" . $postfields['iAddressNbr'] . "', '" . $NewPayment . "', '" . $postfields['iInvoiceNbr'] . "', ''";
            $res = $this->db->query($queryInsertPayment2);

            echo json_encode(array('result' => true, 'id' => $NewPayment, 'overpayment' => $assign));
            } else {
            echo json_encode(array('result' => false, 'message' => 'Not complete'));
            }
            */
        } else {
            echo json_encode(array(
                'result' => false,
                'message' => 'file not found'
            ));
        }
    }
    public function suspend_service_reminder3($date = false)
    {
        $this->load->model('Admin_model');
        if (!$date) {
            $date = date('Y-m-d');
        }
        $days_ago = date('Y-m-d', strtotime('-5 days', strtotime($date)));
        foreach (array(
            53,
            56
        ) as $companyid) {
            echo "Checking Reminder3 sent 5 days ago for " . $companyid . "\n";
            send_growl(array(
                'companyid' => $companyid,
                'message' => date('Y-m-d H:i:s') . "  Task of Running reminder3 suspension is now running...\n"
            ));
            $reminders = $this->Admin_model->getReminder3Delta($companyid, $days_ago);
            echo "Found " . $reminders->count . "\n";
            $this->load->library('artilium', array(
                'companyid' => $companyid
            ));
            $this->load->library('magebo', array(
                'companyid' => $companyid
            ));
            if ($reminders->count > 0) {
                foreach ($reminders->data as $reminder) {
                    // print_r($reminder);
                    echo "Checking {$reminder->userid} for unpaid invoices\n";
                    if ($this->magebo->hasUnpaidInvoice(getiAddressNbr($reminder->userid))) {
                        $services = $this->Admin_model->getServices($reminder->userid);
                        echo "We found {$reminder->userid} has unpaid invoices\n";
                        if ($services) {
                            foreach ($services as $serv) {
                                if ($serv->status == "Active") {
                                    send_growl(array(
                                        'companyid' => $companyid,
                                        'message' => date('Y-m-d H:i:s') . "  Suspending {$serv->domain} due to unpaid invoices reminder3\n"
                                    ));
                                    echo "  Suspending {$serv->domain} due to unpaid invoices reminder3\n";
                                    $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($serv->sn));
                                    $this->Admin_model->save_state($serv->id, $pack);
                                    $this->artilium->BlockOriginating(trim($serv->sn), $pack, '0');
                                    $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                        'bar' => 1,
                                        'date_modified' => date('Y-m-d H:i:s')
                                    ));
                                    $this->Admin_model->update_services($serv->id, array(
                                        'status' => 'Suspended'
                                    ));
                                    logAdmin(array(
                                        'companyid' => $companyid,
                                        'userid' => $reminder->userid,
                                        'serviceid' => $serv->id,
                                        'user' => 'System',
                                        'ip' => '127.0.0.1',
                                        'description' => $serv->domain . ' has been suspended due to overdue invoice reminder3 send 5 days ago'
                                    ));
                                }
                            }
                        }
                    }
                }
            }
            unset($this->magebo);
            unset($this->artilium);
        }
    }
    public function create_pppoe_username()
    {
        $this->load->helper('string');
        $this->load->model('Radius_model');
        $q = $this->db->query("select a.*,b.mageboid from a_services_xdsl a left join a_clients b on b.id=a.userid where a.status = 'Validated' and a.pppoe_username is null");
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $password = random_string('alnum', 10);
                echo 'TU0000' . $row->mageboid . '.' . get_pppcounter($row->userid) . "\n";
                if ($this->Radius_model->insertUser(array(
                    'username' => 'TU0000' . $row->mageboid . '.' . get_pppcounter($row->userid),
                    'value' => $password
                ))) {
                    $this->db->where('id', $row->id);
                    $this->db->update("a_services_xdsl", array(
                        'pppoe_username' => 'TU0000' . $row->mageboid . '.' . get_pppcounter($row->userid),
                        'pppoe_password' => $password
                    ));
                }
            }
        } else {
            echo date('Y-m-d H:i:s') . "      Nothing to create\n";
        }
    }



    public function process_sepa_to_paid($companyid)
    {
        //, $bankid, $fileid

        $xml = $this->db->query("select * from a_sepa_directdebit where companyid=? and status =? order by id asc", array($companyid, 'new'));
        if ($xml->num_rows()>0) {
            log_message("error", "Found ".$xml->num_rows()." SEPA Directdebit to be process");
            foreach ($xml->result() as $I) {
                //set them processing first
                $this->db->query("update a_sepa_directdebit set status=? where id=?", array("processing", $I->id));
            }



            foreach ($xml->result() as $xml) {
                log_message("error", "Processing ".$xml->filename." BankIdexId : ".$xml->iBankIndex);
                if (empty($companyid)) {
                    log_message('error', 'Failed to process directdebit sepa to paid'.print_r($xml, true));
                    die('companyid, bankid, fileid');
                }

                $text = "";
                $row = (object) array('BatchPaymentId' => $xml->iBankIndex,  'date_transaction' => date('Y-m-d'), 'fileid' => $xml->id, 'companyid' => $companyid);

                $this->load->library('magebo', array('companyid' => $row->companyid));

                if (is_numeric($row->BatchPaymentId)) {
                    $invoices = $this->magebo->getBankDetails($row->BatchPaymentId);

                    if ($invoices) {
                        log_message("error", "Processing Directdebit payments " . $row->BatchPaymentId);
                        foreach ($invoices as $invoice) {
                            log_message("error", "Processing Invoice Directdebit payments " .  $invoice->iInvoiceNbr);

                            if (trim($invoice->cTypeDescription) == "SEPA FIRST") {
                                $paymentform = 'SEPA FIRST';
                            } else {
                                $paymentform = 'SEPA REOCCURRING';
                            }

                            $res = $this->magebo->apply_payment(array('cPaymentForm' => $paymentform,
                            'mPaymentAmount' => $invoice->mBankFileAmount,
                            'cPaymentFormDescription' => "Sepa DD Payment " . $invoice->iInvoiceNbr,
                            'dPaymentDate' => date("m/d/Y", strtotime($row->date_transaction)),
                            'iInvoiceNbr' => $invoice->iInvoiceNbr,
                            'step' => 1,
                            'iAddressNbr' => $invoice->iAddressNbr,
                            'cPaymentRemark' => $row->fileid));


                            log_message("error", "Processing :" . $paymentform . " " . $invoice->mBankFileAmount . " " . $invoice->iInvoiceNbr . " " . $invoice->iAddressNbr);
                            $client = getClientDetailidbyMagebo($invoice->iAddressNbr);
                            $this->db->insert('a_payments', array(
                            'date' => date('Y-m-d'),
                            'invoiceid' => trim($invoice->iInvoiceNbr),
                            'userid' => $client->id,
                            'companyid' => $client->companyid,
                            'paymentmethod' => 'DD',
                            'transid' => 'BatchId: ' . $row->BatchPaymentId,
                            'amount' => $invoice->mBankFileAmount,
                            'adminid' => $this->session->id,
                            'raw' => 'Executed by: ' . $this->session->firstname . ' ' . $this->session->lastname));
                            unset($res);
                        }
                    }
                }
                $this->db->query("update a_sepa_directdebit set status=? where id=?", array("completed", $xml->id));

                $headers = "From: noreply@united-telecom.be" . "\r\n" ."CC: simson.parlindungan@pareteum.be";
                mail("it@united-telecom.be", "Sepa BankfileId: ".$row->BatchPaymentId. " Has been completed for Companyid: ".$companyid." has been completed", "", $headers);
                unset($row);
            }
        } else {
            log_message("error", "No SEPA Directdebit to be process");
        }
    }

    public function updateInvoices()
    {
        if (file_exists(APPPATH . 'lock/import_invoice.lock')) {
            log_message("error", "Invoice Import is running");
            exit;
        }
        log_message("error", "Starting Invoice Update cron");
        file_put_contents(APPPATH . 'lock/import_invoice.lock', 1);
        if ($this->masterconfig->disable_invoice_sync == 1) {
            die('disable_invoice_sync flag is On, therefore this task will end');
        }
        $this->load->model('Cron_model');
        $this->load->model('Admin_model');
        $date     = $this->Cron_model->getLastInvoice();
        //echo "test\n";
        $invoices = $this->Cron_model->getInvoicetoImport($date);
        if ($invoices) {
            $rec = count($invoices);
            $mec = $rec-1;
            log_message("error", "Found ".$mec." Invoice(s) that need to be updated");
            if (count($invoices) == 1) {
                $last = json_decode(file_get_contents(APPPATH . 'logs/app/LastInvoice.log'), true);
                if ($last['dLastUpdate'] == $invoices[0]['dLastUpdate'] && $last['iInvoiceNbr'] == $invoices[0]['iInvoiceNbr']) {
                    exit;
                }
            }
            foreach ($invoices as $invoice) {
                if (in_array($invoice['iCompanyNbr'], array(
                    53,
                    54,
                    56
                ))) {
                    //  send_growl(array('companyid' => $invoice['iCompanyNbr'], 'message' => 'Processing Invoice: '.$invoice['iInvoiceNbr']));
                    $this->load->library('magebo', array(
                        'companyid' => $invoice['iCompanyNbr']
                    ));
                    if (!$this->magebo->hasUnpaidInvoice($invoice['iAddressNbr'])) {
                        log_message("error", "All Invoice seem Paid, Let's Check Suspended Services");
                        $this->load->library('artilium', array(
                            'companyid' => $invoice['iCompanyNbr']
                        ));
                        $client   = $this->Admin_model->getClient(getWhmcsid($invoice['iAddressNbr']));
                        $services = $this->Admin_model->getServices($client->id);
                        if ($services) {
                            //print_r($services);
                            foreach ($services as $serv) {
                                if ($serv->status == "Suspended") {
                                    send_growl(array(
                                        'companyid' => $client->companyid,
                                        'message' => 'Unsuspending Serviceid: ' . $serv->id
                                    ));
                                    if ($serv->type == "mobile") {
                                        log_message("error", "Opps We found " . $serv->id . ", is in suspended status Let's UnSuspended Services");
                                        $ii   = $this->Admin_model->getPackageState($serv->id);
                                        $pack = (array) json_decode($ii);
                                        $this->artilium->UnBlockOriginating(trim($serv->sn), $pack, '1');
                                        $this->Admin_model->update_services_data('mobile', $serv->id, array(
                                            'bar' => 0,
                                            'date_modified' => date('Y-m-d H:i:s')
                                        ));
                                    }
                                    $this->Admin_model->update_services($serv->id, array(
                                        'status' => 'Active'
                                    ));
                                    $this->sendmail($serv->id, 'service', 'service_suspended');
                                    logAdmin(array(
                                        'companyid' => $client->companyid,
                                        'userid' => $client->id,
                                        'serviceid' => $serv->id,
                                        'user' => 'System',
                                        'ip' => '127.0.0.1',
                                        'description' => $serv->domain . ' has been unsuspended because all Invoices has been Paid'
                                    ));
                                }
                            }
                        }
                        $this->Admin_model->ChangeClientDunnigProfile($client->id, 'No');
                        unset($this->artilium);
                    }
                    unset($this->magebo);
                }
                file_put_contents(APPPATH . 'logs/app/LastInvoice.log', json_encode($invoice));
                $this->Cron_model->UpdateInvoice($invoice);
            }
        } else {
            log_message("error", "No Invoice that need to be imported at the moment");
        }
        unlink(APPPATH . 'lock/import_invoice.lock');
    }
    public function proces_bankfiles($companyid)
    {
        $this->load->model('Cron_model', 'cron');
        $s = $this->db->query("select * from a_sepa_items where (name_owner='THIRD PARTY FUNDS EMS' or name_owner ='Kosten' or name_owner='TrendCall Nederland B.V.') and amount < '0' and companyid= 54");
        if ($s->num_rows() > 0) {
            foreach ($s->result() as $row) {
                //$this->db->query("update a_sepa_items set status='Completed' where id=?", array($row->id));
            }
        }
        $q = $this->db->query("select * from a_sepa_files where status != 'Done' and companyid=? order by id asc", array(
            $companyid
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                if ($this->cron->is_completed($row->id)) {
                    $this->db->query("update a_sepa_files set status='Done' where id=?", array(
                        $row->id
                    ));
                }
            }
        }
    }
    public function updateInvoiceDescription($indexid = false)
    {
        $contents = file('/root/list1');
        foreach ($contents as $indexid) {
            $this->load->library('magebo', array(
                'companyid' => 53
            ));
            $q = $this->db->query('select c.GPcRemark,c.iInvoiceDescription,a.id,a.date_contract,b.msisdn_status,b.msisdn,b.msisdn_sn,b.msisdn_type from a_services a left join a_services_mobile b on b.serviceid=a.id left join a_products c on c.id=a.packageid where a.iGeneralPricingIndex=?', array(
                trim($indexid)
            ));
            if ($q->num_rows() > 0) {
                print_r($q->row());
                $mobile = $this->Admin_model->GetServiceCli($q->row()->id);
                //print_r($mobile);
                print_r($this->magebo->updateCRemarks(trim($indexid), $q->row(), $mobile));
            }
        }
    }
    public function pxm_processing()
    {
        $this->load->model('Cron_model');
        $this->db->where("processed", "0");
        $this->db->where("orderid !=", "");
        $this->db->order_by('id', 'asc');
        $this->db->limit(3);
        $this->load->model('Admin_model');
        $list = $this->db->get("a_services_xdsl_callback");
        //log_message('error', $this->db->last_query());
        if ($list->num_rows() > 0) {
            foreach ($list->result_array() as $c) {
                $this->db->query("update a_services_xdsl_callback set processed=1 where id=?", array(
                    $c['id']
                ));
                $array = json_decode($c['rawdata']);
                $check = $this->Cron_model->CheckRequestId($array->customerOrder->customerOrderIdentifier->id);
                if (trim($array->interaction->feedback->description) == "The customer order is technically validated") {
                    log_message("error", date('Y-m-d H:i:s') . "     Got Validated " . $c['orderid'] . "\n");
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->subject,
                        'status' => $array->customerOrder->status->code->value,
                        'circuitid' => $array->customerOrder->customerOrderItem[0]->product->serviceIdentifier->id,
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                        'status' => 'Validated',
                        'last_updated' => date('Y-m-d H:i:s'),
                        'circuitid' => $array->customerOrder->customerOrderItem[0]->product->serviceIdentifier->id
                    ));
                    foreach ($array->customerOrder->customerOrderItem as $order) {
                        if ($order->customerOrderItemIdentifier->idContext->value == "OLO") {
                            if (!empty($order->product->action->dueDate)) {
                                $updatedata['appointment_date_confirmed'] = $order->product->action->dueDate;
                                $updatedata['circuitid']                  = $order->product->serviceIdentifier->id;
                            }
                        } else {
                            if ($order->product->name == "Wholesale Installation Method") {
                                if ($order->product->childProduct->name == "IM_Remote") {
                                    $updatedata['type_install'] = "Remote";
                                } elseif ($order->product->childProduct->name == 'IM_Without_Customer_Visit') {
                                    $updatedata['type_install'] = "Remote";
                                } elseif ($order->product->childProduct->name == 'IM_With_Customer_Visit_Splicing_Incl') {
                                    $this->Cron_model->insertOrderStatus(array(
                                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                                        'serviceid' => $check['serviceid'],
                                        'subject' => "SNA Splicing work detected",
                                        'status' => $array->customerOrder->status->code->value,
                                        'circuitid' => $array->customerOrder->customerOrderItem[0]->product->serviceIdentifier->id,
                                        'feedback_description' => 'This order will be executed with Splicing work, extra charge may be applied'
                                    ));

                                    $updatedata['type_install'] = "Visit";
                                // $updatedata['status'] = 'On-Hold';
                                } else {
                                    $updatedata['type_install'] = "Visit";
                                }
                            }
                        }
                    }
                    //print_r($updatedata);
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, $updatedata);
                }
                if (trim($array->interaction->feedback->description) == "The customer order is cancelled") {
                    $updatedata['status'] = "Cancelled";
                    $updatedata['last_updated'] = date('Y-m-d H:i:s');
                    $this->Cron_model->changeService($check['serviceid'], array(
                        'status' => 'Cancelled'
                     ));
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, $updatedata);
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->subject,
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                }

                if (trim($array->interaction->feedback->description) == "Street plans received") {
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->subject,
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description." Duedate:".$array->customerOrder->dueDate
                    ));
                }


                if ($array->interaction->feedback->type == "DISCARDED") {
                    if (!empty($array->customerOrder->status->code->value)) {
                        if ($array->customerOrder->status->code->value == "Cancelled") {
                            $updatedata['last_updated'] = date('Y-m-d H:i:s');
                            $updatedata['status']       = 'Rejected';
                        } elseif ($array->customerOrder->status->code->value == "Discarded") {
                            $updatedata['last_updated'] = date('Y-m-d H:i:s');
                            $updatedata['status']       = 'Rejected';
                        }
                        $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, $updatedata);
                    }
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->subject,
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                }


                if (trim($array->interaction->feedback->description) == "List of work orders for which appointment scheduling is needed") {
                    log_message("error", date('Y-m-d H:i:s') . "     Got Booking Request " . $c['orderid'] . "\n");
                    $updatedata['status'] = 'Workorder-Needed';
                    if (!empty($array->referenceData->workOrder->identifier->id)) {
                        log_message("error", "with workorderID\n");
                        $workorder                 = " Workorder needed do get slot";
                        $updatedata['workorderid'] = $array->referenceData->workOrder->identifier->id;
                    } else {
                        $workorder = " No needed just confirm it";
                        log_message("error", "No workorderID\n");
                    }
                    if (!empty($array->customerOrder->customerOrderItem->product)) {
                        if ($array->customerOrder->customerOrderItem->product->childProduct->name == 'IM_Without_Customer_Visit') {
                            $updatedata['type_install'] = "Remote";
                        } elseif ($array->customerOrder->customerOrderItem->product->childProduct->name == 'IM_With_Customer_Visit_Splicing_Incl') {
                            $updatedata['type_install'] = "Visit";
                            $updatedata['SNA'] = 1;
                        } elseif ($array->customerOrder->customerOrderItem->product->childProduct->name == 'IM_Remote') {
                            $updatedata['type_install'] = "Remote";
                        } else {
                            $updatedata['type_install'] = "Visit";
                        }
                    }
                    // print_r($array);
                    if (!empty($array->customerOrder->charVal->characteristicName)) {
                        if ($array->customerOrder->charVal->characteristicName == "earliestInstallationDate") {
                            $updatedata['appointment_date_confirmed'] = $array->customerOrder->charVal->value;
                        }
                    } else {
                        if (!empty($array->customerOrder->charVal[0]->value)) {
                            $updatedata['appointment_date_confirmed'] = $array->customerOrder->charVal[0]->value;
                        }
                        if (!empty($array->customerOrder->charVal[1]->value)) {
                            $updatedata['appointment_date_requested'] = $array->customerOrder->charVal[1]->value;
                        }
                    }
                    $updatedata['last_updated'] = date('Y-m-d H:i:s');
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, $updatedata);
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->subject . ' ' . $workorder,
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                }
                /* if (trim($array->interaction->feedback->description) == "Manual handling required at PXS, please wait for further info")
                 {
                     echo date('Y-m-d H:i:s') . "     Got Onhold " . $c['orderid'] . "\n";
                     $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                         'status' => 'On-Hold',
                         'last_updated' => date('Y-m-d H:i:s')
                     ));
                     $this->Cron_model->insertOrderStatus(array(
                         'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                         'serviceid' => $check['serviceid'],
                         'subject' => $array->interaction->subject,
                         'status' => $array->customerOrder->status->code->value,
                         'feedback_description' => $array->interaction->feedback->description
                     ));
                 }
                 */
                if (trim($array->interaction->feedback->description) == "SNA detected while customer order submitted with 'SNA not allowed' indication") {
                    log_message("error", date('Y-m-d H:i:s') . "     Got SNA " . $c['orderid'] . "\n");
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                        'status' => 'Rejected',
                        'last_updated' => date('Y-m-d H:i:s')
                    ));
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => "Order Rejected",
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                }
                if (!empty($array->interaction->subject)) {
                    if (trim($array->interaction->subject) == "Technically Executed") {
                        log_message("error", date('Y-m-d H:i:s') . "  Got Done " . $c['orderid'] . "\n");
                        $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                            'status' => 'Done',
                            'last_updated' => date('Y-m-d H:i:s'),
                            'date_activation' => date('Y-m-d')
                        ));
                        $this->Cron_model->insertOrderStatus(array(
                            'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                            'serviceid' => $check['serviceid'],
                            'subject' => $array->interaction->subject,
                            'status' => $array->customerOrder->status->code->value,
                            'feedback_description' => $array->interaction->feedback->description
                        ));
                        $this->Cron_model->changeService($check['serviceid'], array(
                            'date_contract' => date('m-d-Y'),
                            'date_contract_formated' => date('Y-m-d'),
                            'status' => 'Active'
                         ));
                        $service  = $this->Admin_model->getServiceCli($check['serviceid']);
                        $this->load->library('magebo', array('companyid' => '2'));
                    }
                }

                if (trim($array->interaction->feedback->description) == "Amend due date confirmation") {
                    log_message("error", date('Y-m-d H:i:s') . "     Got Amend Confirmation " . $c['orderid'] . "\n");
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                        'status' => 'Amend-Confirmed',
                        'last_updated' => date('Y-m-d H:i:s'),
                        'appointment_date_confirmed' => $array->customerOrder->dueDate
                    ));
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => "Amend-Confirmed",
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                }

                if (trim($array->interaction->feedback->description) == "Booking successful") {
                    // log_message("error", date('Y-m-d H:i:s') . "     Got Amend Confirmation " . $c['orderid'] . "\n");
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                        'status' => 'Appointment-Booked',
                        'last_updated' => date('Y-m-d H:i:s'),
                        'appointment_date_confirmed' => $array->customerOrder->dueDate
                    ));
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->feedback->description,
                        'status' => 'Wait for PXS feedback',
                        'feedback_description' => 'Proximus Has confirmed our booking, please wait for validation'
                    ));
                }

                if (trim($array->interaction->feedback->description) == "Service identifier does not match with address") {
                    log_message("error", date('Y-m-d H:i:s') . "     Got Amend Confirmation " . $c['orderid'] . "\n");
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                        'status' => 'Pending',
                        'last_updated' => date('Y-m-d H:i:s'),
                        'ordercycle' => 'CORRECTION',
                        'circuitid' => ''
                    ));
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => "CID not correct",
                        'status' => "Order Rejected",
                        'feedback_description' => $array->interaction->feedback->description.' System has removed the circuitID please click again execute Proximus Order to re-order by removing circuitid'
                    ));
                }
                if (!empty($array->interaction->subject)) {
                    if (trim($array->interaction->subject) == "OLO-tic") {
                        log_message("error", date('Y-m-d H:i:s') . "     Got OLO Tic " . $c['orderid'] . "\n");
                        if (!empty($array->referenceData->networkTerminationPoint->networkPhysicalResource->charVal->characteristicName)) {
                            if ($array->referenceData->networkTerminationPoint->networkPhysicalResource->charVal->characteristicName == "introductionPairUsed") {
                                $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                                'introductionPairUsed' => $array->referenceData->networkTerminationPoint->networkPhysicalResource->charVal->value
                            ));
                            }
                        }
                        $comment = "";
                        if (!empty($array->referenceData->workOrder->status->description->name)) {
                            $comment = $array->referenceData->workOrder->status->description->name;
                        }
                        if (!empty($array->referenceData->workOrder->status->comment)) {
                            $comment .= ": " . $array->referenceData->workOrder->status->comment;
                        }
                        $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->subject,
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description,
                        'remark' => json_encode($array->referenceData->workOrder->charVal)
                    ));
                        if ($array->referenceData->workOrder->charVal) {
                            $text = "";
                            foreach ($array->referenceData->workOrder->charVal as $row) {
                                $text .= $row->characteristicName . ": " . $row->value . "\n";
                            }
                            $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                            'remark_installation' => $text,
                            'last_updated' => date('Y-m-d H:i:s')
                        ));
                        }
                    }
                }
                if (trim($array->interaction->feedback->description) == "End-user absent") {
                    log_message("error", date('Y-m-d H:i:s') . "  Got On Hold Customer is Absent" . $c['orderid'] . "\n");
                    $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                        'status' => 'On-Hold',
                        'last_updated' => date('Y-m-d H:i:s')
                    ));
                    $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => "Customer is Absent, please create amend",
                        'status' => $array->customerOrder->status->code->value . " : Customer is Absent",
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                }
                //The customer order is closed
                if (!empty($array->interaction->subject)) {
                    if (trim($array->interaction->subject) == "Order Closed") {
                        log_message("error", date('Y-m-d H:i:s') . "     Got Live Active from Proximus " . $c['orderid'] . "\n");
                        $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                        'status' => 'Active',
                        'last_updated' => date('Y-m-d H:i:s')
                    ));
                        $this->Cron_model->changeService($check['serviceid'], array(
                        'status' => 'Active'
                    ));
                        $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->subject,
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                    }
                    if (trim($array->interaction->subject) == "On HOLD") {
                        log_message("error", date('Y-m-d H:i:s') . "     Got On Hold " . $c['orderid'] . "\n");
                        $this->Cron_model->changeOrder($array->customerOrder->customerOrderIdentifier->id, array(
                        'status' => 'On-Hold',
                        'last_updated' => date('Y-m-d H:i:s')
                    ));
                        $this->Cron_model->insertOrderStatus(array(
                        'proximusid' => $array->customerOrder->customerOrderIdentifier->id,
                        'serviceid' => $check['serviceid'],
                        'subject' => $array->interaction->subject,
                        'status' => $array->customerOrder->status->code->value,
                        'feedback_description' => $array->interaction->feedback->description
                    ));
                    }
                }
            }
        }
    }
    public function MidnightTask()
    {
        $this->termination();
        //$this->cleanOrder();
        $this->ActivateSim();
        $this->activate_porting_due();
        $this->activate_porting_accepted();
        $this->SendDataCon(53);
        //$this->SendDataCon(56);
        shell_exec('bash /usr/local/bin/clean_phpcache');
        $this->enable_services();
        $this->check_double_mageboid();
        shell_exec('chown mvno:mvno /home/mvno -R');
    }
    public function cleanOrder()
    {
        $this->load->model('Admin_model');
        $q = $this->db->query("select * from a_services where autodelete=1");
        if ($q->num_rows()>0) {
            foreach ($q->result() as $row) {
                $service = $this->Admin_model->getService($row->id);
                if ($service->type == "xdsl") {
                    $this->db->query("delete from a_services where id=?", array(
                        $row->id
                    ));
                    $this->db->query("delete from a_services_xdsl where serviceid=?", array(
                        $row->id
                    ));
                    $this->db->query("delete from a_services_xdsl_products where orderid=?", array(
                    $service->details->proximus_orderid
                    ));
                    $this->db->query("delete from a_services_addons where serviceid=?", array(
                        $row->id
                    ));
                } elseif ($service->type == "mobile") {
                    $this->db->query("delete from a_services where id=?", array(
                        $row->id
                    ));
                    $this->db->query("delete from a_services_mobile where serviceid=?", array(
                        $row->id
                    ));
                    $this->db->query("delete from a_services_addons where serviceid=?", array(
                        $row->id
                    ));
                }

                logAdmin(array(
                    'companyid' => $row->companyid,
                    'serviceid' => $row->id,
                    'userid' =>  $row->userid,
                    'user' => 'System',
                    'ip' =>  '127.0.0.1',
                    'description' => 'Service : ' .  $row->id . ' has been deleted due to order reject'
                ));
            }
        }
    }
    public function Teum_Tasks()
    {
        if (file_exists(APPPATH.'lock/teum_task/'.date('Y-m-d'))) {
            log_message('error', 'Task run for TEUM renewal has been aborted');
            die('This task has been run for '.date('Y-m-d'));
        }
        $headers = "From: noreply@pareteum.cloud" . "\r\n" .
        "BCC: mail@simson.one";
        $qq = $this->db->query('select * from a_configuration where val=? and name=? order by companyid asc', array('1', 'teum_task'));

        foreach ($qq->result() as $company) {
            $items = "Dear MVNO\nPlease find today renew reports below:\n\n";
            $setting = globofix($company->companyid);
            $q = $this->db->query("select a.*,b.userid,c.agentid,b.msisdn
        from a_services_addons a
        left join a_services_mobile b on b.serviceid=a.serviceid
        left join a_clients c on c.id=b.userid
        where a.teum_NextRenewal = ?
        and a.companyid = ?
        and teum_autoRenew > 0", array(date('Y-m-d'), $company->companyid));
            log_message('error', $this->db->last_query());
            log_message('error', 'Found '.$q->num_rows().' auto renew');
            if ($q->num_rows()>0) {
                foreach ($q->result() as $row) {
                    log_message("error", print_r($row, true));
                    $addon_name =  $this->Teum_addBundle(array(
                'companyid' => $row->companyid,
                'serviceid' => $row->serviceid,
                'userid' => $row->userid,
                'bundleid' => $row->addonid,
                'month' => $row->teum_autoRenew,
                'id' => $row->id));
                    $items .= $row->msisdn." ".$row->serviceid." ".$addon_name."\n";
                    $this->Admin_model->insertTopup(
                    array(
                    'companyid' => $row->companyid,
                    'serviceid' =>  $row->serviceid,
                    'userid' =>  $row->userid,
                    'income_type' => 'bundle',
                    'agentid' => $row->agentid,
                    'amount' => $row->recurring_total,
                    'bundle_name' => $row->name,
                    'user' => 'System Autorenew')
                    );
                }
            }

            if ($setting->email_notification) {
                $items .="\nNighly Cronjob";
                mail($setting->email_notification, 'Renew Report '.date('d/m/Y'), $items, $headers);
            } else {
                $items .="\nNighly Cronjob";
                mail('mail@simson.one', 'Renew Report '.date('Y-m-d'), $items, $headers);
            }
        }
        file_put_contents(APPPATH.'lock/teum_task/'.date('Y-m-d'));
    }


    public function Teum_addBundle($vars)
    {
        $this->load->model('Admin_model');
        $service_teum = $this->Admin_model->getServiceCli($vars['serviceid']);

        if ($service_teum) {
            $reseller_sim_card = $this->db->query("select * from a_reseller_simcard where MSISDN=?", array($service_teum->details->msisdn));
            $this->load->library('pareteum', array(
                'companyid' => $vars['companyid']
            ));
            $client    = $this->Admin_model->getClient($vars['userid']);
            $addons    = getAddonInformation($vars['bundleid']);
            log_message("error", print_r($addons, true));
            $AccountId = $service_teum->details->teum_accountid;
            $subs = array(
                "CustomerId" => (int) $client->teum_CustomerId,
                "SubscriptionId" => $reseller_sim_card->row()->SubscriptionId,
                "Channel" => "UnitedPortal V1 Nightly Cronjob",
                "Offerings" => array(
                    array(
                    "ProductOfferingId" => (int) $addons->bundleid,
                    "OrderedProductCharacteristics" => array(
                        array(
                            "Name" => "MSISDN",
                            "Value" => $service_teum->details->msisdn
                        )
                    )
                    )
                    )

            );

            log_message('error', print_r($subs, true));
            $subscription = $this->pareteum->subscriptions_offerings($subs);
            log_message('error', print_r($subscription, true));
            if ($subscription->resultCode == "0") {
                if ($vars['month'] >= "100") {
                    $m = 600;
                } else {
                    $m =$vars['month']-1;
                }
                $offering = array(
                    'teum_autoRenew' => $m,
                    'teum_NextRenewal' => date('Y-m-d', strtotime("+31 days")),
                    'teum_DateEnd' => date('Y-m-d', strtotime("+31 days"))
                );
                $this->Admin_model->updateAddon($offering, $vars['id']);
                logAdmin(array(
                        'companyid' => $vars['companyid'],
                        'serviceid' => $vars['serviceid'],
                        'userid' =>  $vars['userid'],
                        'user' => 'System',
                        'ip' =>  '127.0.0.1',
                        'description' => 'Service : ' .  $vars['serviceid'] . ' Bundle: '.$addons->name.' has been renewed successfuly'
                    ));

                $success= "Success";
            } else {
                mail('mail@simson.one', 'Renew Order TeumFailed', print_r($subscription, true));
                log_message('error', $vars['serviceid'].' bundle has not been  renewed');
                $success= "Failed";
            }
            unset($this->pareteum);
            return $addons->name." ".$success." ".date('Y-m-d H:i:s');
        }
    }

    public function UnitedPortout()
    {
        //$this->load->library('magebo', array('companyid' => 2));
        $this->load->model('Admin_model');
        //if (($pl->TaskName == 'PortOut') &&  ($pl->JobName == 'Request') &&  ($pl->FinalJob == 'false') && ($pl->Success == 'true')) {
        $this->db->where('status', '0');
        $this->db->where('TaskName', 'PortOut');
        $this->db->where('JobName', 'Request');
        $this->db->where('FinalJob', 'false');
        $this->db->where('Success', 'true');
        $this->db->group_by('SubscriptionId');
        $q = $this->db->get('a_event_log_united');
        if ($q->num_rows()>0) {
            log_message('error', 'Found Portout ticket :'.$q->num_rows());
            foreach ($q->result() as $row) {
                $this->db->query('update a_event_log_united set status=? where SubscriptionId like ? and TaskName= ? and JobName=? and FinalJob =?', array(
                1,
                $row->SubscriptionId,
                $row->TaskName,
                $row->JobName,
                $row->FinalJob
                ));


                if (substr(trim($row->SubscriptionId), 0, 2) == "31") {
                    continue;
                }
                $rp= $this->Admin_model->getResellerINParams(trim($row->ResellerId));
                print_r(array(trim($row->SubscriptionId) => $rp));
                $sci = $this->Admin_model->getSpecicifCLIInfo(trim($row->SubscriptionId), $rp);
                print_r(array(trim($row->SubscriptionId) => $sci));
                $fc = $this->Admin_model->getfinancialcondition($sci[0]['MsisdnNr']);
                print_r(array(trim($row->SubscriptionId) =>$fc));
                $gr= $this->Admin_model->getRequest($rp, 'NONE', 'PENDINGANDREJECTED', trim($row->SubscriptionId));
                print_r($gr);
                $message="Operation Type : ".$gr['OperationType']." \n".
                        "Request Type : ".$gr['RequestType']." \n".
                        "Donor Operator : ".$gr['DonorOperator']." \n".
                        "MSISDN : ".$gr['MSISDN']." \n".
                        "DonorSIM : ".$gr['DonorSIM']." \n".
                        "Status : ".$gr['Status']." \n".
                        "\n"."_________________________________\n".
                        "iAddressNbr : ".$fc['iAddressNbr']." \n".
                        "Company : ".$fc['Company']." \n".
                        "cName : ".$fc['cName']." \n".
                        "GsmNumber : ".$fc['GsmNumber']." \n".
                        "iPincode : ".$fc['iPincode']." \n".
                        "Created From : European Portal \n";
                $subject="Port OUT Request :".$sci[0]['MsisdnNr']." - ".$fc['cName'];
                $name=$sci[0]['MsisdnNr'];
                $email="info@info.com";

                print_r($message);

                if ($this->Admin_model->checkticket($subject)) {
                    $ticket =   $this->Admin_model->createWHMCSticket(array('deptid' => 13, 'name' => $name, 'email' => $email,'subject' => $subject,'message' => $message));
                    log_message('error', print_r($ticket, true));
                    log_message('error', "Msisdn: ".$fc['iPincode']." ". trim($row->SubscriptionId)." Ticket has been created");
                } else {
                    log_message('error', "Msisdn: ".$fc['iPincode']." ". trim($row->SubscriptionId)." Ticket Has been created, aborted!");
                }
                //
            }
            //  print_r($q->result());
        } else {
            log_message('error', 'No portout for UnitedTelecom need to be process');
        }
    }

    public function import_sims_mvno()
    {
    }
}
