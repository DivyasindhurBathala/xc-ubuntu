<?php
defined('BASEPATH') or exit('No direct script access allowed');
class TableController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();

        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }
        $this->companyid = get_companyidby_url(base_url());
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Table extends TableController
{
    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('default', true);
        $this->sql_details = array(
            'user' => $this->db->username,
            'pass' => $this->db->password,
            'db' => $this->db->database,
            'host' => $this->db->hostname,
        );

        error_reporting(0);
        $this->companyid = get_companyidby_url(base_url());

        $this->load->library('ssp');
    }
    public function getclients_payments()
    {
        $this->load->library('magebo', array('companyid' => $this->companyid));
        echo json_encode($this->magebo->getPaymentList($this->uri->segment(4)));
    }
    public function getclient_invoices()
    {
        $this->db = $this->load->database('magebo', true);
        $userid = $this->uri->segment(4);
        if ($userid == "0") {
            echo json_encode(array());
            exit;
        }
        $q = $this->db->query("SELECT CASE
    WHEN a.iInvoiceType = '40' THEN 'Invoice'
    WHEN a.iInvoiceType = '41' THEN 'Creditnote'
    ELSE 'Unknown'
    END as Type, a.iInvoiceType,a.iInvoiceNbr,CONVERT(char(10), a.dInvoiceDate,126) as dInvoiceDate,CONVERT(char(10), a.dInvoiceDueDate,126) as dInvoiceDueDate, case when a.iInvoiceStatus  = '53' THEN 'Sepa Presented'
       when a.iInvoiceStatus = '54' then 'Paid' else 'Unpaid' end as  iInvoiceStatus  ,b.cName,a.mInvoiceAmount,a.iAddressNbr
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE a.iAddressNbr=?
    and a.iInvoiceType in ('40','41')
    ORDER BY a.iInvoiceNbr DESC", array($userid));

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $r['mInvoiceAmount'] = number_format($r['mInvoiceAmount'], 2);
                $result[] = $r;
            }
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }
    public function getclient_creditnotes()
    {
        $this->db = $this->load->database('magebo', true);
        $userid = $this->uri->segment(4);
        if ($userid == "0") {
            echo json_encode(array());
            exit;
        }
        $q = $this->db->query("SELECT a.iInvoiceNbr,CONVERT(char(10), a.dInvoiceDate,126) as dInvoiceDate,CONVERT(char(10), a.dInvoiceDueDate,126) as dInvoiceDueDate, case when a.iInvoiceStatus  = '53' THEN 'Unpaid'
       when a.iInvoiceStatus = '54' then 'Paid' else 'Open' end as  iInvoiceStatus  ,b.cName,a.mInvoiceAmount,a.iAddressNbr
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE a.iAddressNbr=?
    and a.iInvoiceType = ?
    ORDER BY a.iInvoiceNbr DESC", array($userid, '41'));

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $r['mInvoiceAmount'] = number_format($r['mInvoiceAmount'], 2);
                $result[] = $r;
            }
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }
    public function get_sum_reports()
    {
        $companyid = $this->session->cid;
        $table = <<<EOT
 (
SELECT a.id,a.msisdn,a.amount,a.target,a.used,a.extra,a.date,a.current_slack_percent,a.current_slack_amount,a.expectation_slack_percent,a.expectation_slack_amount,concat(c.firstname,' ',c.lastname) as customername, b.serviceid, b.userid,c.mvno_id
FROM a_sum_report a
LEFT JOIN a_services_mobile b on b.msisdn=a.msisdn
LEFT JOIN a_services d on d.id=b.serviceid
LEFT JOIN a_clients c on c.id=b.userid
WHERE a.companyid= '$companyid'
AND d.status ='Active'

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'mvno_id',
                'dt' => 0,
            ),

            array(
                'db' => 'customername',
                'dt' => 1,
            ),
            array(
                'db' => 'msisdn',
                'dt' => 2,
            ),
            array(
                'db' => 'date',
                'dt' => 3,
            ),
            array(
                'db' => 'amount',
                'dt' => 4,
            ),

            array(
                'db' => 'target',
                'dt' => 5,
            ),
            array(
                'db' => 'used',
                'dt' => 6,
            ),
            array(
                'db' => 'current_slack_percent',
                'dt' => 7,
            ),
            array(
                'db' => 'current_slack_amount',
                'dt' => 8,
            ),
              array(
                'db' => 'expectation_slack_percent',
                'dt' => 9,
            ),
            array(
                'db' => 'expectation_slack_amount',
                'dt' => 10,
            ),
            array(
                'db' => 'date',
                'dt' => 11,
            ),
             array(
                'db' => 'id',
                'dt' => 12,
            ),
              array(
                'db' => 'userid',
                'dt' => 13,
            ),
                array(
                'db' => 'serviceid',
                'dt' => 14,
            ),


        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getclients()
    {
        $table = <<<EOT
 (
  SELECT b.paymentmethod,b.mvno_id,b.id,b.date_created,concat(b.firstname,' ', b.lastname) as fullname,b.email,b.companyname,b.phonenumber, (select count(*) from a_services a where a.status='Active' and a.userid=b.id) as active_service, (select count(*) from a_services a where a.status in('Pending','New') and a.userid=b.id) as pending_service,c.agent,b.agentid
  FROM a_clients b
  left join a_clients_agents c on c.id=b.agentid
  where b.companyid= '$this->companyid' ORDER BY b.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'mvno_id',
                'dt' => 0,
            ),

            array(
                'db' => 'id',
                'dt' => 1,
            ),
            array(
                'db' => 'fullname',
                'dt' => 2,
            ),
            array(
                'db' => 'email',
                'dt' => 3,
            ),

            array(
                'db' => 'pending_service',
                'dt' => 4,
            ),
            array(
                'db' => 'active_service',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 6,
            ),
            array(
                'db' => 'agent',
                'dt' => 7,
            ),
            array(
                'db' => 'date_created',
                'dt' => 8,
            ),
            array(
                'db' => 'agentid',
                'dt' => 9,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }


    public function getResellerToups()
    {
        $userid = $this->uri->segment(4);
        if ($userid) {
            $table = <<<EOT
            (
             Select id,date,round(amount,2) as amount,descriptions,executor,channels from a_clients_agents_topups
             where companyid= '$this->companyid'
             and agentid='$userid'

            ) temp
           EOT;
        }else{
            $table = <<<EOT
            (
             Select id,date,round(amount,2) as amount,descriptions,executor,channels from a_clients_agents_topups
             where companyid= '$this->companyid'


            ) temp
           EOT;
        }


        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'amount',
                'dt' => 1,
            ),
            array(
                'db' => 'descriptions',
                'dt' => 2,
            ),
            array(
                'db' => 'executor',
                'dt' => 3,
            ),
            array(
                'db' => 'channels',
                'dt' => 4,
            ),



        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_sms_log()
    {

        $batchid = $this->uri->segment(4);
        if ($batchid) {
            $table = <<<EOT
 (
  SELECT b.mvno_id, a.id,a.userid,a.date,a.date_sent,a.number,a.message,a.status,a.send_by,concat(b.initial,' ',b.firstname,' ',b.lastname) as customername
  FROM a_sms_log a
  left join a_clients b on b.id=a.userid
  where a.companyid= '$this->companyid' and a.batchid='$batchid' ORDER BY b.id DESC

 ) temp
EOT;
        } else {
            $table = <<<EOT
 (
  SELECT b.mvno_id, a.id,a.userid,a.date,a.date_sent,a.number,a.message,a.status,a.send_by,concat(b.initial,' ',b.firstname,' ',b.lastname) as customername
  FROM a_sms_log a
  left join a_clients b on b.id=a.userid
  where a.companyid= '$this->companyid' ORDER BY b.id DESC

 ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'number',
                'dt' => 1,
            ),
            array(
                'db' => 'mvno_id',
                'dt' => 2,
            ),
            array(
                'db' => 'message',
                'dt' => 3,
            ),
            array(
                'db' => 'date_sent',
                'dt' => 4,
            ),

            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 6,
            ),
            array(
                'db' => 'send_by',
                'dt' => 7,
            ),
            array(
                'db' => 'userid',
                'dt' => 8,
            ),
            array(
                'db' => 'customername',
                'dt' => 9,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_sms_log_batch()
    {
        $table = <<<EOT
 (
  SELECT batchid,a.date,a.message,id,(select count(c.id) from a_sms_log c where c.batchid=a.batchid) as counter
  FROM a_sms_log a
  where a.companyid= '$this->companyid'
  group by a.batchid

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'batchid',
                'dt' => 0,
            ),

            array(
                'db' => 'date',
                'dt' => 1,
            ),
            array(
                'db' => 'message',
                'dt' => 2,
            ),
             array(
                'db' => 'counter',
                'dt' => 3,
            ),
              array(
                'db' => 'id',
                'dt' => 4,
            ),


        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }


    public function get_proformas()
    {
        $cid = $this->session->cid;
        if (!empty($this->uri->segment(4))) {
            $userid = $this->uri->segment(4);

            $table = <<<EOT
(
 SELECT a.invoicenum, a.userid,a.date as date,a.duedate,round(a.total,2) as total,a.id,a.status
 FROM a_invoices a
 WHERE a.userid = '$userid'
 AND a.companyid = '$cid'
 ORDER BY a.id DESC

) temp
EOT;

            $primaryKey = 'id';
            $columns = array(

                array(
                    'db' => 'invoicenum',
                    'dt' => 0,
                ),
                array(
                    'db' => 'date',
                    'dt' => 1,
                ),
                array(
                    'db' => 'duedate',
                    'dt' => 2,
                ),
                array(
                    'db' => 'total',
                    'dt' => 3,
                ),
                array(
                    'db' => 'status',
                    'dt' => 4,
                ),
                array(
                    'db' => 'id',
                    'dt' => 5,
                ),
                array(
                    'db' => 'userid',
                    'dt' => 6,
                ),

            );
        } else {
            $table = <<<EOT
(
 SELECT a.invoicenum, a.userid,a.date,a.duedate,round(a.total,2) as total,a.id,a.status,concat(b.companyname, ' ',b.firstname,' ',b.lastname) as fullname
 FROM a_invoices a
 left join a_clients b on b.id=a.userid
 WHERE a.companyid = '$cid'
 ORDER BY a.id DESC

) temp
EOT;

            $primaryKey = 'id';
            $columns = array(

                array(
                    'db' => 'invoicenum',
                    'dt' => 0,
                ),
                array(
                    'db' => 'fullname',
                    'dt' => 1,
                ),
                array(
                    'db' => 'date',
                    'dt' => 2,
                ),
                array(
                    'db' => 'duedate',
                    'dt' => 3,
                ),
                array(
                    'db' => 'total',
                    'dt' => 4,
                ),
                array(
                    'db' => 'status',
                    'dt' => 5,
                ),
                array(
                    'db' => 'id',
                    'dt' => 6,
                ),
                array(
                    'db' => 'userid',
                    'dt' => 7,
                ),

            );
        }

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_bundleusage_log()
    {
        $cid = $this->session->cid;

        $table = <<<EOT
(
 SELECT a.id,a.date,a.description,a.userid,a.serviceid,b.msisdn,concat(c.firstname,' ',c.lastname) as customername
 FROM a_logs a
 LEFT JOIN a_services_mobile b on b.serviceid=a.serviceid
 LEFT JOIN a_clients c on c.id=a.userid
 WHERE a.user = 'EventHandler'
 AND a.companyid = '$cid'
 ORDER BY a.id DESC

) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

                array(
                    'db' => 'date',
                    'dt' => 0,
                ),
                array(
                    'db' => 'msisdn',
                    'dt' => 1,
                ),
                array(
                    'db' => 'customername',
                    'dt' => 2,
                ),
                array(
                    'db' => 'description',
                    'dt' => 3,
                ),
                array(
                    'db' => 'userid',
                    'dt' => 4,
                ),
                array(
                    'db' => 'serviceid',
                    'dt' => 5,
                ),


            );


        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getpromotionscode()
    {
        $cid = $this->session->cid;

        $table = <<<EOT
(
 SELECT
id,
promo_name,
promo_description,
promo_value,
promo_duration,
promo_code,
date_valid,
date_expired,
date_created,
modified_by
 FROM a_promotion a
 where a.companyid = '$cid'
 ORDER BY a.promo_code ASC

) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

                array(
                    'db' => 'promo_code',
                    'dt' => 0,
                ),
                array(
                    'db' => 'promo_description',
                    'dt' => 1,
                ),
                array(
                    'db' => 'promo_duration',
                    'dt' => 2,
                ),
                array(
                    'db' => 'promo_value',
                    'dt' => 3,
                ),
                array(
                    'db' => 'date_valid',
                    'dt' => 4,
                ),
                array(
                    'db' => 'date_expired',
                    'dt' => 5,
                ),
                 array(
                    'db' => 'date_created',
                    'dt' => 6,
                ),
                 array(
                    'db' => 'modified_by',
                    'dt' => 7,
                ),
                array(
                    'db' => 'id',
                    'dt' => 8,
                ),


            );


        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_agents()
    {
        if ($this->data['setting']->mobile_platform == "ARTA") {
            $table = <<<EOT
        (
         SELECT b.reseller_type,b.last_month_earning,
         case when b.comission_type  = 'Percentage' THEN '%'
         when b.comission_type = 'FixedAmount' then ''
         when b.comission_type = 'None' then 'Balance'
         else 'Unknown' end as comission_type,
         case when b.comission_type  = 'Percentage' THEN b.comission_value
         when b.comission_type = 'FixedAmount' then b.comission_value
         when b.comission_type = 'None' then b.reseller_balance
         else 'Unknown' end as comission_value,
         b.id,b.agent,b.contact_name,b.email,b.phonenumber,concat(b.address1,' ',b.postcode,' ',b.city) as address, (select count(x.id) from a_services x left join a_clients v on v.id=x.userid where v.agentid=b.id and x.status = 'Active') as counter,(select  ROUND(sum(xx.recurring)*(b.comission_value/100),2) from a_services xx left join a_clients vv on vv.id=xx.userid where vv.agentid=b.id and xx.status = 'Active') as amount FROM a_clients_agents b where b.companyid= '$this->companyid' ORDER BY b.id DESC

        ) temp
EOT;

            $primaryKey = 'id';
            $columns = array(
            array(
                'db' => 'id',
                'dt' => 0,
            ),

            array(
                'db' => 'agent',
                'dt' => 1,
            ),
            array(
                'db' => 'contact_name',
                'dt' => 2,
            ),
            array(
                'db' => 'email',
                'dt' => 3,
            ),
            array(
                'db' => 'counter',
                'dt' => 4,
            ),

            array(
                'db' => 'last_month_earning',
                'dt' => 5,
            ),
            array(
                'db' => 'comission_type',
                'dt' => 6,
            ),
            array(
                'db' => 'amount',
                'dt' => 7,
            ),
            array(
                'db' => 'comission_value',
                'dt' => 8,
            ),
            array(
                'db' => 'reseller_type',
                'dt' => 9,
            ),

            );
        } else {
            $date = date('Y-m')."%";
            $table = <<<EOT
        (
        SELECT b.reseller_type,b.last_month_earning,b.id, b.reseller_balance, b.agent,b.contact_name,b.email,b.phonenumber,concat(b.address1,' ',b.postcode,' ',b.city) as address, (select count(x.id) from a_services x left join a_clients v on v.id=x.userid where v.agentid=b.id and x.status = 'Active') as counter, case when b.reseller_type = 'Prepaid' THEN '0.00' else (select ROUND(sum(xx.amount), 2) from a_topups xx left join a_clients vv on vv.id=xx.userid where vv.agentid=b.id and xx.date like '$date') end as amount FROM a_clients_agents b where b.companyid= '$this->companyid' ORDER BY b.id DESC
        ) temp
EOT;

            $primaryKey = 'id';
            $columns = array(
            array(
            'db' => 'id',
            'dt' => 0,
            ),

            array(
            'db' => 'agent',
            'dt' => 1,
            ),
            array(
            'db' => 'contact_name',
            'dt' => 2,
            ),
            array(
            'db' => 'email',
            'dt' => 3,
            ),
            array(
            'db' => 'counter',
            'dt' => 4,
            ),
            array(
            'db' => 'amount',
            'dt' => 5,
            ),
            array(
            'db' => 'reseller_balance',
            'dt' => 6,
            ),
            array(
            'db' => 'reseller_type',
            'dt' => 7,
            ),
            array(
            'db' => 'last_month_earning',
            'dt' => 8,
            ),
            array(
            'db' => 'reseller_type',
            'dt' => 9,
            )
            );
        }



        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_reminder()
    {
        $table = <<<EOT
 (
  SELECT a.*,concat(b.salutation,' ',b.firstname,' ',b.lastname) as name,mvno_id,mageboid
  FROM a_reminder a
  left join a_clients b on b.id=a.userid
  where a.companyid= '$this->companyid'
  ORDER BY b.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'date_sent',
                'dt' => 0,
            ),

            array(
                'db' => 'name',
                'dt' => 1,
            ),
            array(
                'db' => 'invoicenum',
                'dt' => 2,
            ),
            array(
                'db' => 'type',
                'dt' => 3,
            ),
            array(
                'db' => 'amount',
                'dt' => 4,
            ),
            array(
                'db' => 'mvno_id',
                'dt' => 5,
            ),
            array(
                'db' => 'userid',
                'dt' => 6,
            ),
            array(
                'db' => 'id',
                'dt' => 7,
            ),
            array(
                'db' => 'mageboid',
                'dt' => 8,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    //filename    uploaded    date    iBankIndex    companyid
    public function get_online_transactions()
    {
        $table = <<<EOT
   (
   SELECT
  a.id,
   a.companyid,
   a.date,
   a.invoiceid,
   a.userid,
   a.paymentmethod,
   a.transid,
   a.amount,
   concat(b.firstname,' ',b.lastname) as name,
   b.mageboid
    FROM `a_payments` a
    left join a_clients b on b.id=a.userid
    where a.companyid= '$this->companyid'
    ORDER BY a.id DESC

   ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'date',
                'dt' => 0,
            ),
            array(
                'db' => 'invoiceid',
                'dt' => 1,
            ),
            array(
                'db' => 'name',
                'dt' => 2,
            ),
            array(
                'db' => 'paymentmethod',
                'dt' => 3,
            ),
            array(
                'db' => 'amount',
                'dt' => 4,
            ),
            array(
                'db' => 'transid',
                'dt' => 5,
            ),
            array(
                'db' => 'userid',
                'dt' => 6,
            ),
            array(
                'db' => 'mageboid',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_agent_simcards()
    {
        $agentid = $this->uri->segment(4);
        $table = <<<EOT
   (
      select * from a_reseller_simcard a
      where a.resellerid='$agentid'
      and a.companyid = '$this->companyid'
   ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
           array(
               'db' => 'simcard',
               'dt' => 0,
           ),
           array(
               'db' => 'serviceid',
               'dt' => 1,
           ),

           array(
               'db' => 'MSISDN',
               'dt' => 2,
           ),
           array(
               'db' => 'import_via',
               'dt' => 3,
           ),
           array(
               'db' => 'id',
               'dt' => 4,
           ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_agentorders()
    {
        $agentid = $this->uri->segment(4);

        $table = <<<EOT
   (
      select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,c.mvno_id,
         case when a.type  = 'mobile' THEN d.msisdn
         when a.type = 'xdsl' then e.circuitid
         when a.type = 'voip' then f.cli
         else 'Unknown' end as domain,
         case when a.type  = 'mobile' THEN d.msisdn_status
         when a.type = 'xdsl' then e.status
         when a.type = 'voip' then f.status
         else 'Unknown' end as orderstatus
      from a_services a
      left join a_products b on b.id=a.packageid
      left join a_clients c on c.id=a.userid
      left join a_services_mobile d on d.serviceid=a.id
      left join a_services_xdsl e on e.serviceid=a.id
      left join a_services_voip f on f.serviceid=a.id
      where a.status in ('Pending','Active')
      and a.companyid = '$this->companyid'
      and c.agentid = '$agentid'
      group by a.id
   ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'mvno_id',
                'dt' => 0,
            ),
            array(
                'db' => 'packagename',
                'dt' => 1,
            ),

            array(
                'db' => 'clientname',
                'dt' => 2,
            ),
            array(
                'db' => 'orderstatus',
                'dt' => 3,
            ),
            array(
                'db' => 'recurring',
                'dt' => 4,
            ),
            array(
                'db' => 'domain',
                'dt' => 5,
            ),
            array(
                'db' => 'date_contract',
                'dt' => 6,
            ),
            array(
                'db' => 'billingcycle',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_email_log()
    {
        $userid = $this->uri->segment(4);

        $table = <<<EOT
   (
      SELECT *
      FROM a_email_log
      WHERE userid='$userid'
      ORDER BY id DESC
   ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'id',
                'dt' => 0,
            ),

            array(
                'db' => 'date',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'status',
                'dt' => 3,
            ),
            array(
                'db' => 'userid',
                'dt' => 4,
            ),

            array(
                'db' => 'id',
                'dt' => 5,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_sepa_files()
    {
        $companyid = $this->session->cid;
        $table = <<<EOT
 (
    SELECT id,date_transaction,transactioncode,amount,iban,concat(end2endid,' ',message) as tol,name_owner
    FROM a_sepa_items
    WHERE companyid='$companyid'
    AND amount > 0
    AND iban NOT IN ('NL39INGB0005300756','NL35RABO0117713678')
   AND status = 'Pending'
    ORDER BY id DESC
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'id',
                'dt' => 0,
            ),

            array(
                'db' => 'date_transaction',
                'dt' => 1,
            ),
            array(
                'db' => 'transactioncode',
                'dt' => 2,
            ),
            array(
                'db' => 'amount',
                'dt' => 3,
            ),
            array(
                'db' => 'iban',
                'dt' => 4,
            ),

            array(
                'db' => 'tol',
                'dt' => 5,
            ),

            array(
                'db' => 'name_owner',
                'dt' => 6,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_sepa_items()
    {
        $companyid = $this->uri->segment(4);
        $fileid = $this->uri->segment(5);
        $table = <<<EOT
 (
    SELECT
    id,
    date_transaction,
    debitcredit,
    companyid,
    transactioncode,
    transactionissuer,
    amount,
    iban,
    end2endid,
    mandateid,
    message,
    return_code,
    BatchPaymentId,
    status
    FROM a_sepa_items
    WHERE companyid='$companyid'
    and fileid = '$fileid'
    ORDER BY id DESC
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'date_transaction',
                'dt' => 0,
            ),
            array(
                'db' => 'amount',
                'dt' => 1,
            ),
            array(
                'db' => 'end2endid',
                'dt' => 2,
            ),
            array(
                'db' => 'mandateid',
                'dt' => 3,
            ),
            array(
                'db' => 'BatchPaymentId',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'transactioncode',
                'dt' => 6,
            ),
            array(
                'db' => 'id',
                'dt' => 7,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_reseller_credit_usage()
    {
        $companyid = $this->session->cid;
        $agentid = $this->uri->segment(4);
        if (!is_numeric($agentid)) {
            die('Hack atempt');
        }
        $table = <<<EOT
 (
    SELECT a.id,a.date,a.income_type,a.user,a.bundle_name,round(a.amount, 2) as amount,b.msisdn FROM `a_topups` a left join a_services_mobile b on b.serviceid=a.serviceid WHERE a.agentid = '$agentid' and a.companyid = '$companyid' ORDER BY `id` DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'income_type',
                'dt' => 1,
            ),
            array(
                'db' => 'amount',
                'dt' => 2,
            ),
            array(
                'db' => 'msisdn',
                'dt' => 3,
            ),
            array(
                'db' => 'user',
                'dt' => 4,
            ),
            array(
                'db' => 'id',
                'dt' => 5,
            ),
            array(
                'db' => 'bundle_name',
                'dt' => 6,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }



    public function get_sepa_directdebit()
    {
        $companyid = $this->uri->segment(4);
        $table = <<<EOT
 (
    SELECT *
    FROM a_sepa_directdebit
    WHERE companyid='$companyid'
    ORDER BY id DESC
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'filename',
                'dt' => 1,
            ),
            array(
                'db' => 'uploaded',
                'dt' => 2,
            ),
            array(
                'db' => 'iBankIndex',
                'dt' => 3,
            ),
            array(
                'db' => 'id',
                'dt' => 4,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getvoip()
    {
        $table = <<<EOT
 (
    SELECT *
    FROM a_mobile_onnet
    ORDER BY id DESC
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'id',
                'dt' => 0,
            ),

            array(
                'db' => 'number',
                'dt' => 1,
            ),
            array(
                'db' => 'date_created',
                'dt' => 2,
            ),
            array(
                'db' => 'createdby',
                'dt' => 3,
            ),
            array(
                'db' => 'voipid',
                'dt' => 4,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getservices()
    {
        if (!empty($this->uri->segment(4))) {
            if ($this->uri->segment(4) == "pendingactivation") {
                $table = <<<EOT
      (
         select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
            case when a.type  = 'mobile' THEN d.msisdn
            when a.type = 'xdsl' then concat(e.circuitid,' ',e.proximus_orderid)
            when a.type = 'voip' then f.cli
            else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
            when a.type = 'xdsl' then e.status
            when a.type = 'voip' then f.status
            else 'Unknown' end as orderstatus
         from a_services a
         left join a_products b on b.id=a.packageid
         left join a_clients c on c.id=a.userid
         left join a_services_mobile d on d.serviceid=a.id
         left join a_services_xdsl e on e.serviceid=a.id
         left join a_services_voip f on f.serviceid=a.id
         where a.status = 'Pending'
         and a.companyid = '$this->companyid'
         group by a.id
      ) temp
EOT;
            } elseif ($this->uri->segment(4) == "cancelled") {
                $table = <<<EOT
      (
         select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid
            case when a.type  = 'mobile' THEN d.msisdn
            when a.type = 'xdsl' then e.circuitid
            when a.type = 'voip' then f.cli
            else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
            when a.type = 'xdsl' then e.status
            when a.type = 'voip' then f.status
            else 'Unknown' end as orderstatus
         from a_services a
         left join a_products b on b.id=a.packageid
         left join a_clients c on c.id=a.userid
         left join a_services_mobile d on d.serviceid=a.id
         left join a_services_xdsl e on e.serviceid=a.id
         left join a_services_voip f on f.serviceid=a.id
         where a.status in ('Cancelled','Terminated')
         and a.companyid = '$this->companyid'
         group by a.id
      ) temp
EOT;
            } elseif ($this->uri->segment(4) == "needactions") {
                $table = <<<EOT
   (
      select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
         case when a.type  = 'mobile' THEN d.msisdn
         when a.type = 'xdsl' then e.circuitid
         when a.type = 'voip' then f.cli
         else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
         when a.type = 'xdsl' then e.status
         when a.type = 'voip' then f.status
         else 'Unknown' end as orderstatus
      from a_services a
      left join a_products b on b.id=a.packageid
      left join a_clients c on c.id=a.userid
      left join a_services_mobile d on d.serviceid=a.id
      left join a_services_xdsl e on e.serviceid=a.id
      left join a_services_voip f on f.serviceid=a.id
      where a.status in ('Cancelled','Terminated')
      and a.companyid = '$this->companyid'
      group by a.id
   ) temp
EOT;
            } else {
                $status = $this->uri->segment(4);
                $table = <<<EOT
      (
         select a.id,a.status as orderstatus,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
            case when a.type  = 'mobile' THEN d.msisdn
            when a.type = 'xdsl' then e.circuitid
            when a.type = 'voip' then f.cli
            else 'Unknown' end as domain,
            case when a.type  = 'mobile' THEN d.msisdn_status
            when a.type = 'xdsl' then e.status
            when a.type = 'voip' then f.status
            else 'Unknown' end as status
         from a_services a
         left join a_products b on b.id=a.packageid
         left join a_clients c on c.id=a.userid
         left join a_services_mobile d on d.serviceid=a.id
         left join a_services_xdsl e on e.serviceid=a.id
         left join a_services_voip f on f.serviceid=a.id
         where a.status = '$status'
         and a.companyid = '$this->companyid'
         group by a.id
      ) temp
EOT;
            }
        } else {
            $table = <<<EOT
   (
      select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle, date_contract,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,a.companyid,
         case when a.type  = 'mobile' THEN d.msisdn
         when a.type = 'xdsl' then e.proximus_orderid
         when a.type = 'voip' then f.cli
         else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
         when a.type = 'xdsl' then e.status
         when a.type = 'voip' then f.status
         else 'Unknown' end as orderstatus
      from a_services a
      left join a_products b on b.id=a.packageid
      left join a_clients c on c.id=a.userid
      left join a_services_mobile d on d.serviceid=a.id
      left join a_services_xdsl e on e.serviceid=a.id
      left join a_services_voip f on f.serviceid=a.id
      where a.status in ('Pending','Active','Suspended')
      and a.companyid = '$this->companyid'
      group by a.id
   ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'id',
                'dt' => 0,
            ),
            array(
                'db' => 'packagename',
                'dt' => 1,
            ),
            array(
                'db' => 'clientname',
                'dt' => 2,
            ),
            array(
                'db' => 'orderstatus',
                'dt' => 3,
            ),
            array(
                'db' => 'recurring',
                'dt' => 4,
            ),
            array(
                'db' => 'domain',
                'dt' => 5,
            ),
            array(
                'db' => 'date_contract',
                'dt' => 6,
            ),
            array(
                'db' => 'status',
                'dt' => 7,
            ),
            array(
                'db' => 'billingcycle',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),
              array(
                'db' => 'companyid',
                'dt' => 10,
            ),
            array(
                'db' => 'id',
                'dt' => 11,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getclient_services()
    {
        $currency = $this->data['setting']->currency;
        $userid = $this->uri->segment(4);

        $table = <<<EOT
 (
    select a.companyid,a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.userid = '$userid'
    and a.companyid = '$this->companyid'
    group by a.id

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'packagename',
                'dt' => 0,
            ),

            array(
                'db' => 'date_created',
                'dt' => 1,
            ),
            array(
                'db' => 'status',
                'dt' => 2,
            ),
            array(
                'db' => 'recurring',
                'dt' => 3,
            ),
            array(
                'db' => 'domain',
                'dt' => 4,
            ),
            array(
                'db' => 'billingcycle',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 6,
            ),

                array(
                'db' => 'companyid',
                'dt' => 7,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getdocuments_clients()
    {
        $userid = $this->uri->segment(4);
        $table = <<<EOT
 (
    select *
  	from a_clients_files
    where userid='$userid'


 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'title',
                'dt' => 0,
            ),

            array(
                'db' => 'filename',
                'dt' => 1,
            ),
            array(
                'db' => 'id',
                'dt' => 2,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getcompany_logs()
    {
        $cid = $this->session->cid;
        $table = <<<EOT
 (
    SELECT DATE_FORMAT(a.date, '%Y-%m-%d %H:%i:%s') as date,a.id,a.description,a.user,a.ip,a.serviceid,concat(b.firstname,' ',b.lastname) as customername,a.userid
    FROM  a_logs a
    left join a_clients b on b.id=a.userid
  	where a.companyid='$cid'
    ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'customername',
                'dt' => 1,
            ),
            array(
                'db' => 'description',
                'dt' => 2,
            ),
            array(
                'db' => 'ip',
                'dt' => 3,
            ),
            array(
                'db' => 'user',
                'dt' => 4,
            ),
             array(
                'db' => 'serviceid',
                'dt' => 5,
            ),
            array(
                'db' => 'userid',
                'dt' => 6,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getlogs_client()
    {
        $userid = $this->uri->segment(4);
        $table = <<<EOT
 (
    SELECT DATE_FORMAT(a.date, '%Y-%m-%d %H:%i:%s') as date,a.id,a.description,a.user,a.ip,a.serviceid
    FROM  a_logs a
  	where a.userid='$userid'
    ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'description',
                'dt' => 1,
            ),
            array(
                'db' => 'user',
                'dt' => 2,
            ),
            array(
                'db' => 'ip',
                'dt' => 3,
            ),
            array(
                'db' => 'id',
                'dt' => 4,
            ),
             array(
                'db' => 'serviceid',
                'dt' => 5,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getnotes_client()
    {
        $userid = $this->uri->segment(4);
        $table = <<<EOT
 (
    SELECT a.*
    FROM a_clients_notes a
  	where a.userid='$userid'
    ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'created',
                'dt' => 0,
            ),

            array(
                'db' => 'note',
                'dt' => 1,
            ),
            array(
                'db' => 'admin',
                'dt' => 2,
            ),
            array(
                'db' => 'id',
                'dt' => 3,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getclient_contacts()
    {
        $userid = $this->uri->segment(4);
        $table = <<<EOT
 (
    SELECT id,concat(firstname,' ',lastname) as fullname,companyname as role,email,phonenumber,gsm
    FROM a_clients_contacts
    where userid='$userid'
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'fullname',
                'dt' => 0,
            ),
            array(
                'db' => 'role',
                'dt' => 1,
            ),
            array(
                'db' => 'email',
                'dt' => 2,
            ),
            array(
                'db' => 'phonenumber',
                'dt' => 3,
            ),

            array(
                'db' => 'id',
                'dt' => 4,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_pending_orders()
    {
        $table = <<<EOT
 (
  select a.companyid,c.nationalnr,a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,case when c.nationalnr is not null then '1' when c.nationalnr = '' then '0' else '0' end as hasNN,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain,
        case when a.type  = 'mobile'  THEN d.msisdn_status
       when a.type = 'xdsl'  then e.status
       when a.type = 'voip'  then f.status
       else 'Unknown' end as order_status,
       (select z.status from a_invoiceitems v left join a_invoices z on z.id=v.invoiceid where v.serviceid=a.id limit 1) as invoicestatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.status ='New'
    and a.companyid = '$this->companyid'
    group by a.id
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'id',
                'dt' => 0,
            ),
            array(
                'db' => 'domain',
                'dt' => 1,
            ),
            array(
                'db' => 'clientname',
                'dt' => 2,
            ),
            array(
                'db' => 'date_created',
                'dt' => 3,
            ),
            array(
                'db' => 'packagename',
                'dt' => 4,
            ),
            array(
                'db' => 'order_status',
                'dt' => 5,
            ),
            array(
                'db' => 'userid',
                'dt' => 6,
            ),
            array(
                'db' => 'billingcycle',
                'dt' => 7,
            ),
            array(
                'db' => 'nationalnr',
                'dt' => 8,
            ),
            array(
                'db' => 'companyid',
                'dt' => 9,
            ),
            array(
                'db' => 'invoicestatus',
                'dt' => 10,
            ),
            array(
                'db' => 'hasNN',
                'dt' => 11,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getservices_changes()
    {
        if (!empty($this->uri->segment(4))) {
            $table = <<<EOT
 (
select a.date_request,a.executor,a.requestor,a.serviceid,a.id,a.date_commit,a.new_pid, a.old_pid,concat(c.firstname,' ',c.lastname) as customername,c.id as userid, c.mvno_id,(select concat(name, ' (', a.new_price,')') as price from a_products c where c.id=a.new_pid) as newname, (select name from a_products c where c.id=a.old_pid) as oldname, case when g.type = 'mobile' THEN d.msisdn when g.type = 'xdsl' then e.circuitid when g.type = 'voip' then f.cli else 'Unknown' end as domain from a_subscription_changes a left join a_services g on g.id=a.serviceid left join a_clients c on c.id=g.userid left join a_services_mobile d on d.serviceid=a.serviceid left join a_services_xdsl e on d.serviceid=a.serviceid left join a_services_voip f on d.serviceid=a.serviceid where a.status = '1' and g.companyid='$this->companyid' group by a.id order by a.date_commit
 ) temp
EOT;
        } else {
            $table = <<<EOT
 (
select a.date_request,a.executor,a.requestor,a.serviceid,a.id,a.date_commit,a.new_pid, a.old_pid,concat(c.firstname,' ',c.lastname) as customername,c.id as userid, c.mvno_id,(select concat(name, ' (',a.new_price,')') as price from a_products c where c.id=a.new_pid) as newname, (select name from a_products c where c.id=a.old_pid) as oldname, case when g.type = 'mobile' THEN d.msisdn when g.type = 'xdsl' then e.circuitid when g.type = 'voip' then f.cli else 'Unknown' end as domain from a_subscription_changes a left join a_services g on g.id=a.serviceid left join a_clients c on c.id=g.userid left join a_services_mobile d on d.serviceid=a.serviceid left join a_services_xdsl e on d.serviceid=a.serviceid left join a_services_voip f on d.serviceid=a.serviceid where a.status = '0' and g.companyid='$this->companyid' group by a.id order by a.date_commit
 ) temp
EOT;
        }
        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'id',
                'dt' => 0,
            ),
            array(
                'db' => 'domain',
                'dt' => 1,
            ),
            array(
                'db' => 'customername',
                'dt' => 2,
            ),
            array(
                'db' => 'oldname',
                'dt' => 3,
            ),
            array(
                'db' => 'newname',
                'dt' => 4,
            ),
            array(
                'db' => 'requestor',
                'dt' => 5,
            ),
            array(
                'db' => 'date_commit',
                'dt' => 6,
            ),
            array(
                'db' => 'userid',
                'dt' => 7,
            ),
            array(
                'db' => 'mvno_id',
                'dt' => 8,
            ),
            array(
                'db' => 'serviceid',
                'dt' => 9,
            ),
            array(
                'db' => 'executor',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_termination_list()
    {
        if (!empty($this->uri->segment(4))) {
            $table = <<<EOT
 (
    select a.id,a.status,a.userid,a.date_terminate,round(a.recurring , 2) as recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
       when a.type = 'xdsl' then e.status
       when a.type = 'voip' then f.status
       else 'Unknown' end as orderstatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.companyid = '$this->companyid'
    and a.termination_status ='1'
    and a.date_terminate is not null
    group by a.id
 ) temp
EOT;
        } else {
            $table = <<<EOT
 (
    select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.date_terminate,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
       case when a.type  = 'mobile' THEN d.msisdn
       when a.type = 'xdsl' then e.circuitid
       when a.type = 'voip' then f.cli
       else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
       when a.type = 'xdsl' then e.status
       when a.type = 'voip' then f.status
       else 'Unknown' end as orderstatus
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    left join a_services_xdsl e on d.serviceid=a.id
    left join a_services_voip f on d.serviceid=a.id
    where a.companyid = '$this->companyid'
        and a.termination_status !='1'
    and a.date_terminate is not null
    group by a.id
 ) temp
EOT;
        }
        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'id',
                'dt' => 0,
            ),
            array(
                'db' => 'packagename',
                'dt' => 1,
            ),

            array(
                'db' => 'clientname',
                'dt' => 2,
            ),
            array(
                'db' => 'domain',
                'dt' => 3,
            ),
            array(
                'db' => 'recurring',
                'dt' => 4,
            ),
            array(
                'db' => 'date_terminate',
                'dt' => 5,
            ),
            array(
                'db' => 'date_created',
                'dt' => 6,
            ),
            array(
                'db' => 'billingcycle',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getdepartment()
    {
        $companyid = $this->session->cid;
        $table = <<<EOT
 (
    select id,email,name from a_helpdesk_department where companyid='$companyid'

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'name',
                'dt' => 0,
            ),

            array(
                'db' => 'email',
                'dt' => 1,
            ),
            array(
                'db' => 'id',
                'dt' => 2,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_email_templates()
    {
        $table = <<<EOT
 (
    SELECT name,description,date_created,date_modified,last_modifier,id,subject,case when status='1' then 'Enabled' else 'Disabled' end as status
    FROM a_email_templates
     WHERE companyid='$this->companyid'
     AND language ='english'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'name',
                'dt' => 0,
            ),
            array(
                'db' => 'description',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'date_modified',
                'dt' => 3,
            ),
            array(
                'db' => 'last_modifier',
                'dt' => 4,
            ),
            array(
                'db' => 'id',
                'dt' => 5,
            ),
            array(
                'db' => 'status',
                'dt' => 6,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_pdf_templates()
    {
        if ($this->companyid == "54") {
            $table = <<<EOT
 (
    SELECT
    a.id,
   a.name,
    a.package_gid,
    a.description,
    a.body,
    a.companyid,
    c.companyname,
    language,
    font,
    font_size
    FROM a_products_pdf a
    LEFT join a_products_group b on a.package_gid=b.id
    LEFT join a_mvno c on c.companyid=a.companyid
     WHERE a.companyid='$this->companyid'
     AND language ='english'
     and a.package_gid='1'
 ) temp
EOT;
        } else {
            $table = <<<EOT
 (
    SELECT
    a.id,
   a.name,
   a.description,
    a.package_gid,
    b.name as groupname,
    a.body,
    a.companyid,
    c.companyname,
    language,
    font,
    font_size
    FROM a_products_pdf a
    LEFT join a_products_group b on a.package_gid=b.id
    LEFT join a_mvno c on c.companyid=a.companyid
     WHERE a.companyid='$this->companyid'
     AND language ='english'
 ) temp
EOT;
        }
        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'name',
                'dt' => 0,
            ),
            array(
                'db' => 'description',
                'dt' => 1,
            ),
            array(
                'db' => 'font',
                'dt' => 2,
            ),
            array(
                'db' => 'font_size',
                'dt' => 3,
            ),
            array(
                'db' => 'companyid',
                'dt' => 4,
            ),
            array(
                'db' => 'id',
                'dt' => 5,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getadmins()
    {
        $table = <<<EOT
 (
    SELECT *,concat(firstname,' ',lastname) as fullname
    FROM a_admin
     WHERE companyid='$this->companyid'
     AND status ='Active'
     AND master !=  '1'

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(
            array(
                'db' => 'picture',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'email',
                'dt' => 2,
            ),
            array(
                'db' => 'status',
                'dt' => 3,
            ),
            array(
                'db' => 'role',
                'dt' => 4,
            ),
            array(
                'db' => 'id',
                'dt' => 5,
            ),


        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getcompanies()
    {
        if ($this->session->master != 1) {
            die('No access');
        }

        $table = <<<EOT
 (
    SELECT a.*,
    case when b.val  = '1' THEN 'enabled'
         else 'disabled' end as invoicing,
         case when c.val  = '1' THEN 'enabled'
         else 'disabled' end as helpdesk,
    d.val as currency,
    case when a.status  = '1' THEN 'Active'
         else 'Suspended' end as mvno_status
    FROM a_mvno a
    left join a_configuration b on b.companyid=a.companyid and b.name='mage_invoicing'
    left join a_configuration c on c.companyid=a.companyid and c.name='whmcs_ticket'
    left join a_configuration d on d.companyid=a.companyid and d.name='currency'
    WHERE a.portal_url not like 'https://partner.united-telecom.be/'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'companyid',
                'dt' => 0,
            ),
            array(
                'db' => 'companyname',
                'dt' => 1,
            ),
            array(
                'db' => 'portal_url',
                'dt' => 2,
            ),
            array(
                'db' => 'invoicing',
                'dt' => 3,
            ),
            array(
                'db' => 'helpdesk',
                'dt' => 4,
            ),
            array(
                'db' => 'currency',
                'dt' => 5,
            ),

            array(
                'db' => 'id',
                'dt' => 6,
            ),
            array(
                'db' => 'apache',
                'dt' => 7,
            ),
            array(
                'db' => 'mvno_status',
                'dt' => 8,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getclient_tickets()
    {
        $userid = $this->uri->segment(4);
        $companyid = $this->companyid;
        $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    WHERE a.companyid='$companyid'
    and a.userid='$userid'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'date',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'deptname',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 6,
            ),
            array(
                'db' => 'userid',
                'dt' => 7,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getapi_logs()
    {
        $companyid = $this->companyid;
        $table = <<<EOT
 (
  SELECT *
     FROM a_apilog a
    WHERE a.companyid='$companyid'
    and a.api_env not in( '951_302','906_302')
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'api_env',
                'dt' => 0,
            ),
            array(
                'db' => 'date',
                'dt' => 1,
            ),
            array(
                'db' => 'api_call',
                'dt' => 2,
            ),
            array(
                'db' => 'result',
                'dt' => 3,
            ),
            array(
                'db' => 'id',
                'dt' => 4,
            ),


        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getcompany_products()
    {
        if ($this->session->master != 1) {
            die('No access');
        }
        $companyid = $this->uri->segment(4);
        $table = <<<EOT
 (
    SELECT d.val as currency,a.id,a.name,a.product_type,ROUND(a.recurring_total, 2) as recurring_total,a.PriceList,a.PriceListSubType,a.notes,a.iInvoiceGroupNbr
    FROM a_products a
    left join a_configuration d on d.companyid=a.companyid and d.name='currency'
    WHERE a.companyid='$companyid'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'name',
                'dt' => 0,
            ),
            array(
                'db' => 'product_type',
                'dt' => 1,
            ),
            array(
                'db' => 'recurring_total',
                'dt' => 2,
            ),
            array(
                'db' => 'PriceList',
                'dt' => 3,
            ),
            array(
                'db' => 'PriceListSubType',
                'dt' => 4,
            ),
            array(
                'db' => 'notes',
                'dt' => 5,
            ),
            array(
                'db' => 'iInvoiceGroupNbr',
                'dt' => 6,
            ),

            array(
                'db' => 'id',
                'dt' => 7,
            ),
            array(
                'db' => 'currency',
                'dt' => 8,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getcompany_addons()
    {
        if ($this->session->master != 1) {
            die('No access');
        }
        $companyid = $this->uri->segment(4);
        $table = <<<EOT
 (
    SELECT d.val as currency,ROUND(a.recurring_total, 2) as recurring_total,a.id,a.name,a.name_desc,a.bundle_type,a.show_customer,a.status,a.bundleid
    FROM a_products_mobile_bundles a
    left join a_configuration d on d.companyid=a.companyid and d.name='currency'
    WHERE a.companyid='$companyid'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

          array(
              'db' => 'name',
              'dt' => 0,
          ),
          array(
              'db' => 'name_desc',
              'dt' => 1,
          ),
          array(
              'db' => 'bundle_type',
              'dt' => 2,
          ),
          array(
              'db' => 'recurring_total',
              'dt' => 3,
          ),
          array(
              'db' => 'show_customer',
              'dt' => 4,
          ),
          array(
              'db' => 'status',
              'dt' => 5,
          ),
          array(
              'db' => 'bundleid',
              'dt' => 6,
          ),

          array(
              'db' => 'id',
              'dt' => 7,
          ),

          array(
            'db' => 'currency',
            'dt' => 8,
        ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }


    public function getcompany_product_options()
    {
        if ($this->session->master != 1) {
            die('No access');
        }
        $companyid = $this->uri->segment(4);
        $table = <<<EOT
 (
    SELECT a.* from
    FROM a_products a
    WHERE a.companyid='$companyid'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'companyid',
                'dt' => 0,
            ),
            array(
                'db' => 'companyname',
                'dt' => 1,
            ),
            array(
                'db' => 'portal_url',
                'dt' => 2,
            ),
            array(
                'db' => 'invoicing',
                'dt' => 3,
            ),
            array(
                'db' => 'helpdesk',
                'dt' => 4,
            ),

            array(
                'db' => 'id',
                'dt' => 5,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getclientinventory()
    {
        $table = <<<EOT
 (
    SELECT a.*,concat(b.firstname,' ',b.lastname,' - ', b.companyname) as fullname
    FROM clients_inventories a
    left join clients b on b.id=a.userid
    WHERE b.companyid='$this->companyid'
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'fullname',
                'dt' => 0,
            ),
            array(
                'db' => 'name',
                'dt' => 1,
            ),
            array(
                'db' => 'id',
                'dt' => 2,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function gettokens()
    {
        $companyid = $this->companyid;
        $table = <<<EOT
 (
    SELECT * from a_tokens
    where companyid = '$companyid'
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'token',
                'dt' => 0,
            ),
            array(
                'db' => 'date_created',
                'dt' => 1,
            ),
            array(
                'db' => 'creator',
                'dt' => 2,
            ),
            array(
                'db' => 'id',
                'dt' => 3,
            ),
            array(
                'db' => 'level',
                'dt' => 4,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getips()
    {
        $companyid = $this->companyid;
        $table = <<<EOT
 (
    SELECT * from a_whitelist_ip
    where companyid='$companyid'
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'ip',
                'dt' => 0,
            ),
            array(
                'db' => 'date_created',
                'dt' => 1,
            ),
            array(
                'db' => 'creator',
                'dt' => 2,
            ),
            array(
                'db' => 'id',
                'dt' => 3,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_cdr()
    {
        $incoming =false;
        $id = $this->uri->segment(4);
        $bytes = array();
        $voice = array();
        $sms = array();
        $companyid = $this->uri->segment(5);
        if (!empty($this->uri->segment(6))) {
            $incoming =true;
        }
        $this->load->library('artilium', array('companyid' => $companyid));
        $cdr = array();
        $this->load->model('Admin_model');
        $order = $this->Admin_model->getService($id);
        // print_r($order);
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));

        //$result = $this->artilium->GetCDRList($sn);
        $result = $this->artilium->get_cdr($order);
        // mail('mail@simson.one','cdr',print_r($result, true));
        //print_r($result);
        /*
        $req = array('action' => 'GetCDRList',
        'type' => $this->data['mobile']->PaymentType,
        'From' => date('Y-m-01\T00:00:00'),
        'Till' => date('Y-m-d\TH:i:s', strtotime('+1 days')),
        'SN' => $this->uri->segment(4),
        'PageIndex' => 0,
        'PageSize' => 2500,
        'SortBy' => 0,
        'SortOrder' => 0,
        'companyid' => $this->data['setting']->companyid);
         */
        //mail('lainard@gmail.com', 'CDR Request', print_r($req, true));
        //$result = $this->Admin_model->artiliumPost($req);

        foreach ($result['cdr'] as $row) {
            if ($row['Cause'] != "1000") {
                if (empty($row['DestinationCountry'])) {
                    $country = "";
                } else {
                    $country = $row['DestinationCountry'];
                }

                //if ($row['TypeCallId'] != "5") {
                if ($row['MaskDestination'] != "72436") {
                    if ($row['TrafficTypeId'] == "2") {
                        if (convertToReadableSize($row['DurationConnection']) != "0 bytes") {
                            $cdr[] = array('Begintime' => format_cdr_time($row['Begintime']), 'DestinationCountry' => $country, 'MaskDestination' => $row['MaskDestination'], 'DurationConnection' => convertToReadableSize($row['DurationConnection']), 'TrafficTypeId' => $row['TrafficTypeId'], 'TypeCallId' => $row['TypeCallId'], 'CurNumUnitsUsed' => $row['CurNumUnitsUsed']);
                        }
                    } else {
                        if ($row['TrafficTypeId'] == "1" && $row['TypeCallId'] == "2") {
                            $cdr[] = array('Begintime' => format_cdr_time($row['Begintime']), 'DestinationCountry' => $country, 'MaskDestination' => format_number($row['MaskDestination']), 'DurationConnection' => gmdate("H:i:s", $row['DurationConnection']), 'TrafficTypeId' => $row['TrafficTypeId'], 'TypeCallId' => $row['TypeCallId'], 'CurNumUnitsUsed' => $row['CurNumUnitsUsed']);
                        } else {
                            $cdr[] = array('Begintime' => format_cdr_time($row['Begintime']), 'DestinationCountry' => $country, 'MaskDestination' => format_number($row['MaskDestination']), 'DurationConnection' => gmdate("H:i:s", $row['DurationConnection']), 'TrafficTypeId' => $row['TrafficTypeId'], 'TypeCallId' => $row['TypeCallId'], 'CurNumUnitsUsed' => $row['CurNumUnitsUsed']);
                        }
                    }
                }
                // }

                // if ($row['TypeCallId'] != 5) {
                if (!in_array($row['TrafficTypeId'], array(2, 3))) {
                    if (strlen($row['MaskDestination']) > 9) {
                        if ($row['TrafficTypeId']) {
                            if ($row['TrafficTypeId'] == "1" && $row['DurationConnection'] < 1) {
                            } elseif ($row['TrafficTypeId'] == "5" && $row['DurationConnection'] < 1) {
                            } else {
                                if ($row['TrafficTypeId'] == "1" && $row['DurationConnection'] > 1) {
                                    $voice[] = $row['DurationConnection'];
                                } elseif ($row['TrafficTypeId'] == "5" && $row['DurationConnection'] >= 1) {
                                    $sms[] = $row['DurationConnection'];
                                }
                            }
                        }
                    }
                } elseif ($row['TrafficTypeId'] == 2) {
                    $bytes[] = $row['DurationConnection'];
                }
                // }
            }
        }
        echo json_encode($cdr);
    }
    public function get_tickets_priority()
    {
        $priority = $this->uri->segment(4);
        $companyid = $this->companyid;

        if ($priority == "high") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.priority <= '2'
    AND a.status IN('Open','In-Progress', 'Awaiting-Reply','On-Hold')
 ) temp
EOT;
        } else {
            $table = <<<EOT
   (
    SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
      case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
      FROM a_helpdesk_tickets a
      left join a_clients b on b.id=a.userid
      left join a_helpdesk_department c on c.id=a.deptid
      left join a_admin d on d.id=a.assigne
      left join a_helpdesk_category x on x.id=a.categoryid
      WHERE a.companyid='$companyid'
      AND a.priority = '$priority'
      AND a.status IN('Open','In-Progress', 'Awaiting-Reply','On-Hold')
   ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'deptname',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'assigned',
                'dt' => 6,
            ),
            array(
                'db' => 'priority',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),
            array(
                'db' => 'categoryname',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getallnotes()
    {
        $companyid = $this->session->cid;
        $table = <<<EOT
   (
   select a.*,b.mvno_id from a_clients_notes a left join a_clients b on b.id=a.userid where a.companyid='$companyid'
   ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'created',
                'dt' => 0,
            ),
            array(
                'db' => 'admin',
                'dt' => 1,
            ),
            array(
                'db' => 'mvno_id',
                'dt' => 2,
            ),
            array(
                'db' => 'note',
                'dt' => 3,
            ),
            array(
                'db' => 'sticky',
                'dt' => 4,
            ),
            array(
                'db' => 'id',
                'dt' => 5,
            ),
            array(
                'db' => 'userid',
                'dt' => 6,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_mytickets_assign()
    {
        $status = $this->uri->segment(4);
        $companyid = $this->companyid;
        $userid = $this->session->id;
        if ($status == "Open") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Open','In-Progress', 'Awaiting-Reply','On-Hold')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "Closed") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Closed')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "Answered") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Answered')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "On-Hold") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('On-Hold')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "Resolved") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Resolved')
    AND a.assigne = '$userid'
 ) temp
EOT;
        } elseif ($status == "Proefbilling") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
     left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Proefbilling')
    AND a.assigne = '$userid'
 ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'deptname',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'assigned',
                'dt' => 6,
            ),
            array(
                'db' => 'priority',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),
            array(
                'db' => 'categoryname',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_tickets_status()
    {
        $status = $this->uri->segment(4);
        $companyid = $this->companyid;

        if ($status == "Open") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN('Open')
 ) temp
EOT;
        } elseif ($status == "Closed") {
            $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
    AND a.status IN ('Closed')
 ) temp
EOT;
        } elseif ($status == "Answered") {
            $table = <<<EOT
       (
        SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
          case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
          FROM a_helpdesk_tickets a
          left join a_clients b on b.id=a.userid
          left join a_helpdesk_department c on c.id=a.deptid
          left join a_admin d on d.id=a.assigne
          left join a_helpdesk_category x on x.id=a.categoryid
          WHERE a.companyid='$companyid'
          AND a.status IN ('Answered')

       ) temp
EOT;
        } elseif ($status == "On-Hold") {
            $table = <<<EOT
       (
        SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
          case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
          FROM a_helpdesk_tickets a
          left join a_clients b on b.id=a.userid
          left join a_helpdesk_department c on c.id=a.deptid
          left join a_admin d on d.id=a.assigne
          left join a_helpdesk_category x on x.id=a.categoryid
          WHERE a.companyid='$companyid'
          AND a.status IN ('On-Hold')

       ) temp
EOT;
        } elseif ($status == "Resolved") {
            $table = <<<EOT
       (
        SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
          case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
          FROM a_helpdesk_tickets a
          left join a_clients b on b.id=a.userid
          left join a_helpdesk_department c on c.id=a.deptid
          left join a_admin d on d.id=a.assigne
          left join a_helpdesk_category x on x.id=a.categoryid
          WHERE a.companyid='$companyid'
          AND a.status IN ('Resolved')
       ) temp
EOT;
        } elseif ($status == "Proefbilling") {
            $table = <<<EOT
       (
        SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
          case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
          FROM a_helpdesk_tickets a
          left join a_clients b on b.id=a.userid
          left join a_helpdesk_department c on c.id=a.deptid
          left join a_admin d on d.id=a.assigne
          left join a_helpdesk_category x on x.id=a.categoryid
          WHERE a.companyid='$companyid'
          AND a.status IN ('Proefbilling')
       ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'deptname',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'assigned',
                'dt' => 6,
            ),
            array(
                'db' => 'priority',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),
            array(
                'db' => 'categoryname',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_tickets()
    {
        $companyid = $this->companyid;
        $table = <<<EOT
 (
  SELECT a.id,a.tid,a.subject,a.userid,a.status,a.date,a.priority,d.firstname as assigned,a.categoryid,x.name as categoryname,
    case when a.userid  <= '0' THEN a.name else concat(b.firstname,' ', b.lastname) end as fullname,c.name as deptname
    FROM a_helpdesk_tickets a
    left join a_clients b on b.id=a.userid
    left join a_helpdesk_department c on c.id=a.deptid
    left join a_admin d on d.id=a.assigne
    left join a_helpdesk_category x on x.id=a.categoryid
    WHERE a.companyid='$companyid'
 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'tid',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'subject',
                'dt' => 2,
            ),
            array(
                'db' => 'deptname',
                'dt' => 3,
            ),
            array(
                'db' => 'date',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'assigned',
                'dt' => 6,
            ),
            array(
                'db' => 'priority',
                'dt' => 7,
            ),
            array(
                'db' => 'id',
                'dt' => 8,
            ),

            array(
                'db' => 'userid',
                'dt' => 9,
            ),
            array(
                'db' => 'categoryname',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getclient_inventory()
    {
        $userid = $this->uri->segment(3);
        $table = <<<EOT
 (
    SELECT a.*
    FROM clients_inventories a
    where a.userid='$userid'
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'id',
                'dt' => 0,
            ),
            array(
                'db' => 'name',
                'dt' => 1,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getproducts()
    {
        $table = <<<EOT
 (
    SELECT a.*,b.name as suppliername
    FROM products a
    left join suppliers b on b.id=a.supplierid
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'name',
                'dt' => 0,
            ),
            array(
                'db' => 'price',
                'dt' => 1,
            ),
            array(
                'db' => 'stock',
                'dt' => 2,
            ),
            array(
                'db' => 'suppliername',
                'dt' => 3,
            ),
            array(
                'db' => 'id',
                'dt' => 4,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getpurchases()
    {
        $table = <<<EOT
 (
  SELECT a.id,a.date,a.invoicenum,a.total,b.name as suppliername FROM purchases a left join suppliers b on a.supplierid=b.id ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'date',
                'dt' => 0,
            ),
            array(
                'db' => 'suppliername',
                'dt' => 1,
            ),
            array(
                'db' => 'invoicenum',
                'dt' => 2,
            ),
            array(
                'db' => 'total',
                'dt' => 3,
            ),
            array(
                'db' => 'id',
                'dt' => 4,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getmanufactures()
    {
        $table = <<<EOT
 (
    SELECT *
    FROM manufactures
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'name',
                'dt' => 0,
            ),
            array(
                'db' => 'logo',
                'dt' => 1,
            ),
            array(
                'db' => 'id',
                'dt' => 2,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_sepa_rejected()
    {
        if (!$this->session->companyid) {
            exit;
        }
        $companyid = $this->session->companyid;
        if ($companyid == 54) {
            $table = <<<EOT
      (
      select a.date_transaction,a.iban,a.amount,a.return_code as coding,a.iAddressNbr,a.id,a.invoicenumber,a.extra_status,b.mvno_id,b.id as userid,concat(b.firstname,' ',b.lastname) as name, (select count(*) from a_sepa_items x where x.BatchPaymentId = '' and x.status ='Completed' and x.companyid = '$companyid'  and x.amount < 0 and x.transactioncode = 'st' and x.iAddressNbr=a.iAddressNbr ) as counter from a_sepa_items a left join a_clients b on b.mageboid=a.iAddressNbr where a.BatchPaymentId = '' and a.status ='Completed' and a.companyid = '$companyid'  and a.amount < 0 and a.transactioncode = 'st' order by a.id ASC
      ) temp
EOT;
        } else {
            $table = <<<EOT
      (
         select a.date_transaction,a.iban,a.amount,concat(a.return_code,' ', x.description) as coding,a.iAddressNbr,a.id,a.invoicenumber,a.extra_status,b.mvno_id,b.id as userid,concat(b.firstname,' ',b.lastname) as name, (select count(*) from a_sepa_items x where x.BatchPaymentId is NULL and x.status ='Completed' and x.companyid = '$companyid'  and x.amount < 0 and x.iAddressNbr=a.iAddressNbr ) as counter from a_sepa_items a left join a_clients b on b.mageboid=a.iAddressNbr left join a_sepa_reject_codes x on x.rejectcode=a.return_code where a.BatchPaymentId is NULL and a.status ='Completed' and a.companyid = '$companyid'  and a.amount < 0 order by a.id ASC
      ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'date_transaction',
                'dt' => 0,
            ),
            array(
                'db' => 'iban',
                'dt' => 1,
            ),
            array(
                'db' => 'amount',
                'dt' => 2,
            ),
            array(
                'db' => 'coding',
                'dt' => 3,
            ),
            array(
                'db' => 'iAddressNbr',
                'dt' => 4,
            ),
            array(
                'db' => 'invoicenumber',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 6,
            ),
            array(
                'db' => 'counter',
                'dt' => 7,
            ),
            array(
                'db' => 'name',
                'dt' => 8,
            ),
            array(
                'db' => 'extra_status',
                'dt' => 9,
            ),
            array(
                'db' => 'userid',
                'dt' => 10,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getsuppliers()
    {
        $table = <<<EOT
 (
    SELECT *
    FROM suppliers
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'name',
                'dt' => 0,
            ),
            array(
                'db' => 'logo',
                'dt' => 1,
            ),
            array(
                'db' => 'id',
                'dt' => 2,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getinvoices()
    {
        $this->db = $this->load->database('magebo', true);
        $userid = $this->uri->segment(4);
        if ($userid == "0") {
            echo json_encode(array());
            exit;
        }

        if (!empty($this->uri->segment(5))) {
            $q = $this->db->query("SELECT a.iInvoiceNbr,CONVERT(char(10), a.dInvoiceDate,126) as dInvoiceDate,CONVERT(char(10), a.dInvoiceDueDate,126) as dInvoiceDueDate, case when a.iInvoiceStatus  = '53' THEN 'Sepa Presented' when a.iInvoiceStatus  = '52' THEN 'Unpaid'
       when a.iInvoiceStatus = '54' then 'Paid' else 'Open' end as  iInvoiceStatus  ,b.cName,a.mInvoiceAmount,a.iAddressNbr
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE b.iCompanyNbr=?
    and iInvoiceType in ('40','41')
    and iInvoiceStatus = 52
    and dInvoiceDueDate <= ?
    ORDER BY a.iInvoiceNbr DESC", array($this->companyid,date('Y-m-d')));
        } else {
            $q = $this->db->query("SELECT a.iInvoiceNbr,CONVERT(char(10), a.dInvoiceDate,126) as dInvoiceDate,CONVERT(char(10), a.dInvoiceDueDate,126) as dInvoiceDueDate, case when a.iInvoiceStatus  = '53' THEN 'Sepa Presented' when a.iInvoiceStatus  = '52' THEN 'Unpaid'
       when a.iInvoiceStatus = '54' then 'Paid' else 'Open' end as  iInvoiceStatus  ,b.cName,a.mInvoiceAmount,a.iAddressNbr
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE b.iCompanyNbr=?
    and iInvoiceType in ('40','41')
    ORDER BY a.iInvoiceNbr DESC", array($this->companyid));
        }


        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $r['mInvoiceAmount'] = number_format($r['mInvoiceAmount'], 2);
                $result[] = $r;
            }
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }

    public function get_child_clients()
    {
        $this->db = $this->load->database('magebo', true);
        $userid = $this->uri->segment(4);
        if ($userid == "0") {
            echo json_encode(array());
            exit;
        }
        $q = $this->db->query("SELECT a.*,b.cName,c.cAddressData FROM tblLocation a LEFT JOIN tblAddress b on b.iAddressNbr=a.iAddressLocationNbr LEFT JOIN tblAddressData c on c.iAddressNbr=a.iAddressLocationNbr and c.iTypeNbr=2524 where a.iAddressNbr=? and a.iAddressLocationNbr != ?", array($userid, $userid));

        if ($q->num_rows() > 0) {
            echo json_encode($q->result_array());
        } else {
            echo json_encode(array());
        }
    }

    public function get_invoices()
    {
        $companyid = $this->session->cid;
        $userid = $this->uri->segment(4);
        $table = <<<EOT
 (
    SELECT a.id,a.iInvoiceNbr,a.dInvoiceDate,a.dInvoiceDueDate,a.iInvoiceStatus,round(mInvoiceAmount,2) as mInvoiceAmount,a.iAddressNbr,'' as cName
    FROM a_tblInvoice a
    WHERE a.iCompanyNbr='$companyid'
    AND iInvoiceType in ('40','41')
    AND dInvoicedate >= '2017-01-01'

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

           array(
               'db' => 'iInvoiceNbr',
               'dt' => 0,
           ),
           array(
               'db' => 'iAddressNbr',
               'dt' => 1,
           ),
           array(
               'db' => 'dInvoiceDate',
               'dt' => 2,
           ),
           array(
               'db' => 'dInvoiceDueDate',
               'dt' => 3,
           ),
           array(
               'db' => 'mInvoiceAmount',
               'dt' => 4,
           ),
           array(
               'db' => 'iInvoiceStatus',
               'dt' => 5,
           ),
           array(
               'db' => 'cName',
               'dt' => 6,
           ),

           array(
               'db' => 'id',
               'dt' => 7,
           ),



        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getPaymentUnAassign()
    {
        $this->db = $this->load->database('magebo', true);
        $userid = $this->uri->segment(4);
        if ($userid == "0") {
            echo json_encode(array());
            exit;
        }

        if (!empty($userid)) {
            //CONVERT(char(10), a.dInvoiceDate,126) as dInvoiceDate,CONVERT(char(10), a.dInvoiceDueDate,126) as dInvoiceDueDate
            $q = $this->db->query("select P.iAddressNbr, P.iPaymentNbr, T.cTypeDescription as Type, CONVERT(char(10), P.dPaymentDate,126) as PaymentDate, P.mPaymentAmount, A.UsedAmount,
 P.mPaymentAmount - A.UsedAmount as AmountLeft, P.cPaymentFormDescription, ''
from tblPayment P
 left join ( select iPaymentNbr, sum(mAssignedAmount) as UsedAmount
    from tblPaymentAssignment
    where iPaymentNbr in ( select iPaymentNbr from tblPayment
        where iAddressNbr in ( select iAddressNbr from tblAddress
            where iCompanyNbr = ? ))
    group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr
  left join tblType T ON T.iTypeNbr = P.iPaymentForm
where iAddressNbr in ( select iAddressNbr from tblAddress
    where iCompanyNbr = ? )
  AND P.mPaymentAmount <> A.UsedAmount
UNION
--> Not yet used payments
select P.iAddressNbr, P.iPaymentNbr, T.cTypeDescription as Type, CONVERT(char(10), P.dPaymentDate,126) as PaymentDate, P.mPaymentAmount, cast(0 as money) as UsedAmount,
 P.mPaymentAmount as AmountLeft, P.cPaymentFormDescription, 'x'
from tblPayment P
  left join tblType T ON T.iTypeNbr = P.iPaymentForm
where iAddressNbr in ( select iAddressNbr from tblAddress
    where iCompanyNbr = ? )
  AND P.iPaymentNbr not in ( select iPaymentNbr from tblPaymentAssignment
    where iPaymentNbr in ( select iPaymentNbr from tblPayment
        where iAddressNbr in ( select iAddressNbr from tblAddress
            where iCompanyNbr = ? )))
  AND P.cPaymentRemark not like 'Payed with paymentnumber:%'
  AND P.iAddressNbr=? order by P.iPaymentNbr ASC", array($this->companyid,$this->companyid,$this->companyid,$this->companyid, $userid));
        } else {
            $q = $this->db->query("select P.iAddressNbr, P.iPaymentNbr, T.cTypeDescription as Type, CONVERT(char(10), P.dPaymentDate,126) as PaymentDate, P.mPaymentAmount, A.UsedAmount,
 P.mPaymentAmount - A.UsedAmount as AmountLeft, P.cPaymentFormDescription, ''
from tblPayment P
 left join ( select iPaymentNbr, sum(mAssignedAmount) as UsedAmount
    from tblPaymentAssignment
    where iPaymentNbr in ( select iPaymentNbr from tblPayment
        where iAddressNbr in ( select iAddressNbr from tblAddress
            where iCompanyNbr = ? ))
    group by iPaymentNbr ) A ON A.iPaymentNbr = P.iPaymentNbr
  left join tblType T ON T.iTypeNbr = P.iPaymentForm
where iAddressNbr in ( select iAddressNbr from tblAddress
    where iCompanyNbr = ? )
  AND P.mPaymentAmount <> A.UsedAmount
UNION
--> Not yet used payments
select P.iAddressNbr, P.iPaymentNbr, T.cTypeDescription as Type, CONVERT(char(10), P.dPaymentDate,126) as PaymentDate, P.mPaymentAmount, cast(0 as money) as UsedAmount,
 P.mPaymentAmount as AmountLeft, P.cPaymentFormDescription, 'x'
from tblPayment P
  left join tblType T ON T.iTypeNbr = P.iPaymentForm
where iAddressNbr in ( select iAddressNbr from tblAddress
    where iCompanyNbr = ? )
  AND P.iPaymentNbr not in ( select iPaymentNbr from tblPaymentAssignment
    where iPaymentNbr in ( select iPaymentNbr from tblPayment
        where iAddressNbr in ( select iAddressNbr from tblAddress
            where iCompanyNbr = ? )))
  AND P.cPaymentRemark not like 'Payed with paymentnumber:%'
order by P.iPaymentNbr ASC", array($this->companyid,$this->companyid,$this->companyid,$this->companyid));
        }


        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $row) {
                $c = getClientDetailidbyMagebo($row['iAddressNbr']);
                $row['mPaymentAmount'] = '€'.number_format($row['mPaymentAmount'], 2);
                $row['CustomerName'] = $c->firstname.' '.$c->lastname;
                $row['CustomerId'] =  $c->mvno_id;
                $row['id'] =  $c->id;
                $row['AmountLeft'] = '€'.number_format($row['AmountLeft'], 2);
                $row['UsedAmount'] = '€'.number_format($row['UsedAmount'], 2);
                $result[] = $row;
            }
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }

    public function getcreditnotes()
    {
        $this->db = $this->load->database('magebo', true);
        $userid = $this->uri->segment(4);
        if ($userid == "0") {
            echo json_encode(array());
            exit;
        }
        $q = $this->db->query("SELECT a.iInvoiceNbr,a.dInvoiceDate,a.dInvoiceDueDate, case when a.iInvoiceStatus  = '53' THEN 'Unpaid'
       when a.iInvoiceStatus = '54' then 'Paid' else 'Open' end as  iInvoiceStatus  ,b.cName,a.mInvoiceAmount,a.iAddressNbr
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE b.iCompanyNbr=?
    and iInvoiceType = ?
    ORDER BY a.iInvoiceNbr DESC", array($this->companyid, '41'));

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $r['mInvoiceAmount'] = number_format($r['mInvoiceAmount'], 2);
                $result[] = $r;
            }
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }
    public function getsim_replace()
    {
        $table = <<<EOT
 (
    select a.id,a.status,a.userid,round(a.recurring , 2) as recurring,a.billingcycle,DATE_FORMAT(a.date_created, '%d/%m/%Y') as date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,d.msisdn,c.mvno_id
    from a_services a
    left join a_products b on b.id=a.packageid
    left join a_clients c on c.id=a.userid
    left join a_services_mobile d on d.serviceid=a.id
    where (a.status != 'New' or a.status != 'Cancelled' or a.status != 'Terminated')
    and a.companyid = '$this->companyid'
    and d.msisdn_swap = '2'
    group by a.id


 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'mvno_id',
                'dt' => 0,
            ),
            array(
                'db' => 'clientname',
                'dt' => 1,
            ),
            array(
                'db' => 'msisdn',
                'dt' => 2,
            ),

            array(
                'db' => 'packagename',
                'dt' => 3,
            ),

            array(
                'db' => 'date_created',
                'dt' => 4,
            ),
            array(
                'db' => 'userid',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 5,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getcreditnotes_client()
    {
        $this->db = $this->load->database('magebo', true);
        $userid = $this->uri->segment(4);
        if ($userid == "0") {
            echo json_encode(array());
            exit;
        }
        $q = $this->db->query("SELECT a.iInvoiceNbr,a.dInvoiceDate,a.dInvoiceDueDate, case when a.iInvoiceStatus  = '53' THEN 'Unpaid'
       when a.iInvoiceStatus = '54' then 'Paid' else 'Open' end as  iInvoiceStatus  ,b.cName,a.mInvoiceAmount,a.iAddressNbr
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE b.iCompanyNbr=?
    and iInvoiceType = ?
    and a.iAddressNbr = ?
    ORDER BY a.iInvoiceNbr DESC", array($this->companyid, '41', $userid));

        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $r['mInvoiceAmount'] = number_format($r['mInvoiceAmount'], 2);
                $result[] = $r;
            }
            echo json_encode($result);
        } else {
            echo json_encode(array());
        }
    }
    public function getquotes()
    {
        $table = <<<EOT
 (
    SELECT a.id,a.userid,a.status,a.total,a.quotenum,
       DATE_FORMAT(a.date, '%d/%m/%Y') as date,
       DATE_FORMAT(a.duedate, '%d/%m/%Y') as duedate, concat(b.companyname, ' ',b.firstname,' ',b.lastname) as fullname
    FROM quotes a
    left join clients b on b.id=a.userid
    ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'quotenum',
                'dt' => 0,
            ),
            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'date',
                'dt' => 2,
            ),
            array(
                'db' => 'duedate',
                'dt' => 3,
            ),
            array(
                'db' => 'total',
                'dt' => 4,
            ),
            array(
                'db' => 'status',
                'dt' => 5,
            ),
            array(
                'db' => 'id',
                'dt' => 6,
            ),
            array(
                'db' => 'userid',
                'dt' => 7,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getquotes_client()
    {
        $userid = $this->uri->segment(3);
        $table = <<<EOT
 (
    SELECT a.*
    FROM quotes a
  	where a.userid='$userid'
    ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'quotenum',
                'dt' => 0,
            ),

            array(
                'db' => 'date',
                'dt' => 1,
            ),
            array(
                'db' => 'duedate',
                'dt' => 2,
            ),
            array(
                'db' => 'total',
                'dt' => 3,
            ),
            array(
                'db' => 'status',
                'dt' => 4,
            ),
            array(
                'db' => 'id',
                'dt' => 5,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getlogs()
    {
        $table = <<<EOT
 (
    SELECT a.*,concat(b.firstname,' ',b.lastname, ' ',b.companyname) as fullname
    FROM log_clients a
    left join clients b on id=a.userid
    ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'description',
                'dt' => 2,
            ),
            array(
                'db' => 'id',
                'dt' => 3,
            ),
            array(
                'db' => 'userid',
                'dt' => 4,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getnotes()
    {
        $table = <<<EOT
 (
    SELECT a.*,concat(b.firstname,' ',b.lastname, ' ',b.companyname) as fullname
    FROM tblnotes a
    left join tblclients b on id=a.userid
    Where b.companyid='$this->companyid'
    ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'date',
                'dt' => 0,
            ),

            array(
                'db' => 'fullname',
                'dt' => 1,
            ),
            array(
                'db' => 'notes',
                'dt' => 2,
            ),
            array(
                'db' => 'id',
                'dt' => 3,
            ),
            array(
                'db' => 'userid',
                'dt' => 4,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getsubscriptions()
    {
        if (!empty($this->uri->segment(3))) {
            $userid = $this->uri->segment(3);
            $table = <<<EOT
 (
    SELECT a.*,b.name,b.gid
    FROM subscriptions a
    left join recurring_services b on b.id=a.packageid
    where a.userid=?
    ORDER BY a.id DESC

 ) temp
EOT;
        } else {
            $table = <<<EOT
 (
    SELECT a.*,b.name,b.gid
    FROM subscriptions a
    left join recurring_services b on b.id=a.packageid
    ORDER BY a.id DESC

 ) temp
EOT;
        }

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'id',
                'dt' => 0,
            ),

            array(
                'db' => 'name',
                'dt' => 1,
            ),
            array(
                'db' => 'status',
                'dt' => 2,
            ),
            array(
                'db' => 'cycle',
                'dt' => 3,
            ),
            array(
                'db' => 'recurring',
                'dt' => 4,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function getbillableitems_client()
    {
        $userid = $this->uri->segment(3);

        $table = <<<EOT
 (
    SELECT id,description,invoicetype,a.status, case when a.invoicedaterecurring = '0000-00-00' THEN a.invoicedate else invoicedaterecurring end as date
    FROM billableitems a
    where userid='$userid'
    ORDER BY a.id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'description',
                'dt' => 0,
            ),

            array(
                'db' => 'date',
                'dt' => 1,
            ),
            array(
                'db' => 'invoicetype',
                'dt' => 2,
            ),
            array(
                'db' => 'status',
                'dt' => 3,
            ),
            array(
                'db' => 'id',
                'dt' => 4,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_coda()
    {
        $table = <<<EOT
 (
    SELECT  a.id,a.filename,DATE_FORMAT(a.date, '%d/%m/%Y %H:%i:%s') as date,a.status,concat(b.firstname,' ',b.lastname) as adminame
    FROM mod_dailytransaction a
    left join a_admin b on b.id=a.adminid

    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'id',
                'dt' => 0,
            ),

            array(
                'db' => 'filename',
                'dt' => 1,
            ),
            array(
                'db' => 'date',
                'dt' => 2,
            ),
            array(
                'db' => 'adminame',
                'dt' => 3,
            ),
            array(
                'db' => 'status',
                'dt' => 4,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function get_roles()
    {
        $companyid = $this->companyid;
        $table = <<<EOT
 (
    SELECT  *
    FROM a_role
    where companyid='$companyid'
    ORDER BY id DESC

 ) temp
EOT;

        $primaryKey = 'id';
        $columns = array(

            array(
                'db' => 'id',
                'dt' => 0,
            ),

            array(
                'db' => 'name',
                'dt' => 1,
            ),
            array(
                'db' => 'description',
                'dt' => 2,
            ),
            array(
                'db' => 'id',
                'dt' => 3,
            ),

        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }
    public function getpaymentlist()
    {
        $id = $this->uri->segment(3);

        if (!empty($id)) {
            $table = <<<EOT
 (
     SELECT  DATE_FORMAT(a.date, '%d/%m/%Y') as date,a.paymentmethod,a.id,a.invoiceid,a.amount,a.transid,concat(b.firstname,' ',b.lastname) as fullname,c.invoicenum
    FROM payments a
    left join clients b on b.id=a.userid
    left join invoices c on c.id=a.invoiceid
    where a.userid='$id'
    ORDER BY a.date DESC

 ) temp
EOT;

            $primaryKey = 'id';
            $columns = array(

                array(
                    'db' => 'date',
                    'dt' => 0,
                ),

                array(
                    'db' => 'invoicenum',
                    'dt' => 1,
                ),
                array(
                    'db' => 'transid',
                    'dt' => 2,
                ),
                array(
                    'db' => 'amount',
                    'dt' => 3,
                ),
                array(
                    'db' => 'paymentmethod',
                    'dt' => 4,
                ),
            );
        } else {
            $table = <<<EOT
 (
   SELECT  DATE_FORMAT(a.date, '%d/%m/%Y') as date,a.paymentmethod,a.id,a.invoiceid,a.amount,a.transid,concat(b.firstname,' ',b.lastname) as fullname,c.invoicenum
    FROM payments a
    left join clients b on b.id=a.userid
    left join invoices c on c.id=a.invoiceid

    ORDER BY a.date DESC

 ) temp
EOT;

            $primaryKey = 'id';
            $columns = array(

                array(
                    'db' => 'date',
                    'dt' => 0,
                ),

                array(
                    'db' => 'invoicenum',
                    'dt' => 1,
                ),
                array(
                    'db' => 'fullname',
                    'dt' => 2,
                ),
                array(
                    'db' => 'amount',
                    'dt' => 3,
                ),
                array(
                    'db' => 'paymentmethod',
                    'dt' => 4,
                ),
                array(
                    'db' => 'transid',
                    'dt' => 5,
                ),
            );
        }
        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_coda_trx()
    {
        $id = $this->uri->segment(3);
        $type = $this->uri->segment(4);
        if (!empty($type)) {
            if ($type == 'Done') {
                $table = <<<EOT
 (
    SELECT
    a.trx_id, a.name, a.iban, a.ogm, a.amount_paid,DATE_FORMAT(a.date_trx, '%d/%m/%Y') as date_trx, a.invoiceid, a.userid, a.invnum, a.invoice_amount, a.msg,b.status as paymentstatus
    FROM mod_dailytransactionitems a
    LEFT JOIN invoices b on b.id=a.invoiceid
    where a.batch_id='$id'
	and a.item_status = 'Done'
    ORDER BY a.trx_id DESC
 ) temp
EOT;
            } elseif ($type == 'all') {
                $table = <<<EOT
 (
    SELECT
    a.trx_id, a.name, a.iban, a.ogm, a.amount_paid, DATE_FORMAT(a.date_trx, '%d/%m/%Y') as date_trx, a.invoiceid, a.userid, a.invnum, a.invoice_amount, a.msg,b.status as paymentstatus
    FROM mod_dailytransactionitems a
    LEFT JOIN invoices b on b.id=a.invoiceid
    where a.batch_id='$id'
    ORDER BY a.trx_id DESC
 ) temp
EOT;
            } else {
                $table = <<<EOT
 (
    SELECT
    a.trx_id, a.name, a.iban, a.ogm, a.amount_paid, DATE_FORMAT(a.date_trx, '%d/%m/%Y') as date_trx, a.invoiceid, a.userid, a.invnum, a.invoice_amount, a.msg,b.status as paymentstatus
    FROM mod_dailytransactionitems a
    LEFT JOIN invoices b on b.id=a.invoiceid
    where a.batch_id='$id'
    and  a.status='$type'
	and a.item_status <> 'Done'
    ORDER BY a.trx_id DESC
 ) temp
EOT;
            }
        } else {
            $table = <<<EOT
 (
    SELECT
    a.trx_id, a.name, a.iban, a.ogm, a.amount_paid, DATE_FORMAT(a.date_trx, '%d/%m/%Y') as date_trx, a.invoiceid, a.userid, a.invnum, a.invoice_amount, a.msg,b.status as paymentstatus
    FROM mod_dailytransactionitems a
    LEFT JOIN invoices b on b.id=a.invoiceid
    where a.batch_id='$id'
    ORDER BY a.trx_id DESC
 ) temp
EOT;
        }

        $primaryKey = 'trx_id';
        $columns = array(
            array(
                'db' => 'trx_id',
                'dt' => 0,
            ),
            array(
                'db' => 'name',
                'dt' => 1,
            ),
            array(
                'db' => 'iban',
                'dt' => 2,
            ),
            array(
                'db' => 'date_trx',
                'dt' => 3,
            ),
            array(
                'db' => 'msg',
                'dt' => 4,
            ),
            array(
                'db' => 'amount_paid',
                'dt' => 5,
            ),
            array(
                'db' => 'invoice_amount',
                'dt' => 6,
            ),
            array(
                'db' => 'invnum',
                'dt' => 7,
            ),
            array(
                'db' => 'paymentstatus',
                'dt' => 8,
            ),
            array(
                'db' => 'invoiceid',
                'dt' => 9,
            ),
            array(
                'db' => 'userid',
                'dt' => 10,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function get_coda_history()
    {
        $type = $this->uri->segment(3);
        if (!empty($type)) {
            if ($type == 'Done') {
                $table = <<<EOT
 (
    SELECT
    a.trx_id, a.name, a.iban, a.ogm, a.amount_paid,DATE_FORMAT(a.date_trx, '%d/%m/%Y') as date_trx, a.invoiceid, a.userid, a.invnum, a.invoice_amount, a.msg,b.status as paymentstatus
    FROM mod_dailytransactionitems a
    LEFT JOIN invoices b on b.id=a.invoiceid
	where a.item_status = 'Done'
    ORDER BY a.trx_id DESC
 ) temp
EOT;
            } elseif ($type == 'all') {
                $table = <<<EOT
 (
    SELECT
    a.trx_id, a.name, a.iban, a.ogm, a.amount_paid, DATE_FORMAT(a.date_trx, '%d/%m/%Y') as date_trx, a.invoiceid, a.userid, a.invnum, a.invoice_amount, a.msg,b.status as paymentstatus
    FROM mod_dailytransactionitems a
    LEFT JOIN invoices b on b.id=a.invoiceid
    ORDER BY a.trx_id DESC
 ) temp
EOT;
            } else {
                $table = <<<EOT
 (
    SELECT
    a.trx_id, a.name, a.iban, a.ogm, a.amount_paid, DATE_FORMAT(a.date_trx, '%d/%m/%Y') as date_trx, a.invoiceid, a.userid, a.invnum, a.invoice_amount, a.msg,b.status as paymentstatus
    FROM mod_dailytransactionitems a
    LEFT JOIN invoices b on b.id=a.invoiceid

    where  a.status='$type'
	and a.item_status <> 'Done'
    ORDER BY a.trx_id DESC
 ) temp
EOT;
            }
        } else {
            $table = <<<EOT
 (
    SELECT
    a.trx_id, a.name, a.iban, a.ogm, a.amount_paid, DATE_FORMAT(a.date_trx, '%d/%m/%Y') as date_trx, a.invoiceid, a.userid, a.invnum, a.invoice_amount, a.msg,b.status as paymentstatus
    FROM mod_dailytransactionitems a
    LEFT JOIN invoices b on b.id=a.invoiceid

    ORDER BY a.trx_id DESC
 ) temp
EOT;
        }

        $primaryKey = 'trx_id';
        $columns = array(
            array(
                'db' => 'trx_id',
                'dt' => 0,
            ),
            array(
                'db' => 'name',
                'dt' => 1,
            ),
            array(
                'db' => 'iban',
                'dt' => 2,
            ),
            array(
                'db' => 'date_trx',
                'dt' => 3,
            ),
            array(
                'db' => 'msg',
                'dt' => 4,
            ),
            array(
                'db' => 'amount_paid',
                'dt' => 5,
            ),
            array(
                'db' => 'invoice_amount',
                'dt' => 6,
            ),
            array(
                'db' => 'invnum',
                'dt' => 7,
            ),
            array(
                'db' => 'paymentstatus',
                'dt' => 8,
            ),
            array(
                'db' => 'invoiceid',
                'dt' => 9,
            ),
            array(
                'db' => 'userid',
                'dt' => 10,
            ),
        );

        echo json_encode($this->ssp->simple($_GET, $this->sql_details, $table, $primaryKey, $columns));
    }

    public function platform_log()
    {
        $res = array();
        $this->load->library('artilium', array('companyid' => $this->companyid));
        $sn = $this->artilium->ServiceLogging('GetLoggingBySn', array('Sn' => trim($this->uri->segment(4)), 'From' => date('Y-m-02'), 'Till' => date('Y-m-t'), 'MaxResults' => '100'));
        /*
        if($_SESSION['id']==1){
            log_message('error',print_r($sn, 1));
        }
        */
        if (!empty($sn->LogId)) {
            $m = (array) $sn;
            $res[] =$m;
        } else {
            foreach ($sn as $s) {
                $m = (array) $s;
                if (empty($s->Value)) {
                    $m['Value'] = '';
                    $res[] = $m;
                }
            }
        }

        echo json_encode($res);
    }

    public function invoice()
    {
        // $this->load->library('mssp')
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "iInvoiceNbr";

        /* DB table to use */
        $sTable = "tblInvoice";

        /* Database connection information */
        $gaSql['user'] = "Spongebobke";
        $gaSql['password'] = "spongebobke";
        $gaSql['db'] = "GDC_ERP";
        $gaSql['server'] = "10.17.2.50";

        /*
         * Columns
         * If you don't want all of the columns displayed you need to hardcode $aColumns array with your elements.
         * If not this will grab all the columns associated with $sTable
         */

        $aColumns = array(
            '0' => 'iInvoiceNbr',
            '1' => 'iAddressNbr',
            '2' => 'mInvoiceAmount',
            '3' => 'dInvoiceDate',
            '4' => 'dInvoiceDueDate',
        );
        //echo $this->mssp->getDataTable($sTable, $sIndexColumn, $aColumns )
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP server-side, there is
         * no need to edit below this line
         */

        /*
         * ODBC connection
         */
        $connectionInfo = array("UID" => $gaSql['user'], "PWD" => $gaSql['password'], "Database" => $gaSql['db'], "ReturnDatesAsStrings" => true);
        $gaSql['link'] = sqlsrv_connect($gaSql['server'], $connectionInfo);
        $params = array();
        $options = array("Scrollable" => SQLSRV_CURSOR_KEYSET);

        /* Ordering */
        $sOrder = "";
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
                   " . addslashes($_GET['sSortDir_' . $i]) . ", ";
                }
            }
            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }

        /* Filtering */
        $sWhere = "";
        if (isset($_GET['sSearch']) && $_GET['sSearch'] != "") {
            $sWhere = "WHERE (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . " LIKE '%" . addslashes($_GET['sSearch']) . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }
        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++) {
            if (isset($_GET['bSearchable_' . $i]) && $_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . addslashes($_GET['sSearch_' . $i]) . "%' ";
            }
        }

        /* Paging */
        $top = (isset($_GET['iDisplayStart'])) ? ((int) $_GET['iDisplayStart']) : 0;
        $limit = (isset($_GET['iDisplayLength'])) ? ((int) $_GET['iDisplayLength']) : 10;
        $sQuery = "SELECT TOP $limit " . implode(",", $aColumns) . "
       FROM $sTable
       $sWhere " . (($sWhere == "") ? " WHERE " : " AND ") . " $sIndexColumn NOT IN
       (
           SELECT $sIndexColumn FROM
           (
               SELECT TOP $top " . implode(",", $aColumns) . "
               FROM $sTable
               $sWhere
               $sOrder
           )
           as [virtTable]
       )
       $sOrder";

        $rResult = sqlsrv_query($gaSql['link'], $sQuery) or die("$sQuery: " . sqlsrv_errors());

        $sQueryCnt = "SELECT * FROM $sTable $sWhere";
        $rResultCnt = sqlsrv_query($gaSql['link'], $sQueryCnt, $params, $options) or die(" $sQueryCnt: " . sqlsrv_errors());
        $iFilteredTotal = sqlsrv_num_rows($rResultCnt);

        $sQuery = " SELECT * FROM $sTable ";
        $rResultTotal = sqlsrv_query($gaSql['link'], $sQuery, $params, $options) or die(sqlsrv_errors());
        $iTotal = sqlsrv_num_rows($rResultTotal);

        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iFilteredTotal,
            "aaData" => array(),
        );

        while ($aRow = sqlsrv_fetch_array($rResult)) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                if ($aColumns[$i] != ' ') {
                    $v = $aRow[$aColumns[$i]];
                    $v = mb_check_encoding($v, 'UTF-8') ? $v : utf8_encode($v);
                    $row[] = $v;
                }
            }
            if (!empty($row)) {
                $output['aaData'][] = $row;
            }
        }
        echo json_encode($output);
    }
}
