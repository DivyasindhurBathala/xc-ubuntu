<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CompleteController extends CI_Controller
{
    protected $data = [];
    public function __construct()
    {
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }

        $this->companyid       = get_companyidby_url(base_url());
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->data['setting'] = globofix($this->session->cid);
    }
}
class Complete extends CompleteController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->model('Api_model');
        $this->load->model('Teams_model');
        error_reporting(0);
    }
    public function get_templates()
    {
        $name      = "adminresetpassword";
        $companyid = '53';
        $language  = 'dutch';
        echo getMailContent($name, $language, $companyid);
    }
    public function pxs_submitcustomerordering()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service->details->status == "Discarded") {
            echo json_encode(array(
                "result" => "error",
                "message" => "Sorry the Orderid has been expired please regenerateid before executing this"
            ));
        } else {
            if ($service->details->bussiness_pack == 1) {
                $_POST['FIXIP'] = 1;
            }
            log_message('error', "Data :\n".print_r($_POST, 1));
            $res = $this->Admin_model->proximus_api($_POST);
            if ($res->result == "success") {
                $this->Admin_model->update_services_data('xdsl', $service->id, array('ordercycle' => 'CORRECTION'));
                $client = $this->Admin_model->getClient($_POST['userid']);
            }
            echo json_encode($res);
        }
    }
    public function pxs_book_appointment()
    {
        if (empty($_POST['workOrderId'])) {
            $q = $this->db->query("select * from a_proximus_appointment_time_slot where id=?", array($_POST['id']
            ));



            $data = array(
                "action" => "appointment",
                "method" => "BookAppointment",
                "requestedDate" => $q->row()->startDateTime,
                "orderid" => $q->row()->proximusid
                );
            log_message('error', "Appointment Data :\n".print_r($data, 1));
            $res  = $this->Admin_model->proximus_api($data);
            echo json_encode($res);
        } else {
            $q = $this->db->query("select * from a_proximus_appointment_time_slot where id=?", array(
            $_POST['id']
            ));
            if ($q->num_rows() > 0) {
                $s    = $this->db->query("select * from a_services_xdsl where proximus_orderid=?", array(
                $q->row()->proximusid
                ));
                $data = array(
                "action" => "appointment",
                "method" => "BookAppointment",
                "workOrderId" => $_POST['workOrderId'],
                "orderid" => $q->row()->proximusid,
                "slot" => $q->row()->timeSlot,
                "Technician" => $s->row()->type_install,
                "startDateTime" => $q->row()->startDateTime,
                "endDateTime" => $q->row()->endDateTime
                );
                log_message('error', "Appointment Data :\n".print_r($data, 1));
                $res  = $this->Admin_model->proximus_api($data);
                echo json_encode($res);
            } else {
                echo json_encode(array(
                "result" => "error",
                "message" => "We could not found any time slot yet from proximus",
                "id" => $_POST['id']
                ));
            }
        }
    }
    public function pxs_get_available_slot()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $result['html'] ="";
        $result['result']="success";
        if (empty($service->details->workorderid)) {
            $this->db->insert('a_proximus_appointment_time_slot', array('proximusid' => $service->details->proximus_orderid, 'startDateTime' => $service->details->appointment_date_confirmed));
            $id =  $this->db->insert_id();
            $result['html'] .= '<option value="' . $id . '">FROM: ' . $service->details->appointment_date_confirmed . ' to ' . $service->details->appointment_date_confirmed . '</option>';

            echo json_encode($result);
        } else {
            if (!in_array($service->orderstatus, array(
            "Ordered",
            "Workorder-Needed"
            ))) {
                echo json_encode(array(
                "result" => "error",
                "message" => "The status of the order must be Ordered to be able to request avaibility slot for workorder",
                "status" => $service->orderstatus
                ));
                exit;
            } else {
                if (empty($_POST['workOrderId'])) {
                    echo json_encode(array(
                    "result" => "error",
                    "message" => "Workorderid not found"
                    ));
                    exit;
                }
                if (empty($_POST['requestedDate'])) {
                    echo json_encode(array(
                    "result" => "error",
                    "message" => "requestedDate not found"
                    ));
                    exit;
                }

                log_message('error', "Appointment Slot Data :\n".print_r($_POST, 1));
                $res = $this->Admin_model->proximus_api($_POST);
                if ($res->result == "success") {
                    $client = $this->Admin_model->getClient($_POST['userid']);
                }
                echo json_encode($res);
            }
        }
    }
    public function pxs_regenerate_pxs_orderid()
    {
        echo json_encode($this->Admin_model->proximus_api($_POST));
    }
    public function pxs_cancel_pxs_orderid()
    {
        echo json_encode($this->Admin_model->proximus_api($_POST));
    }
    public function get_sepa_item()
    {
        $q = $this->db->query("select * from a_sepa_items where id=?", array(
            $this->uri->segment(4)
        ));
        echo json_encode($q->row_array());
    }
    public function testx1()
    {
        $path = $this->data['setting']->DOC_PATH . $this->companyid . "/attachments/";
        $this->load->library('magebo', array(
            'companyid',
            $this->companyid
        ));
        $filename = $this->magebo->CreateWelcomeLetter($this->uri->segment(4));
        if (file_exists($path . $filename)) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $filename . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($path . $filename);
        } else {
            die('File was not created');
        }
    }
    public function view_log()
    {
        $this->data['title']        = "Event Queue";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'log';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function read_queue()
    {
        if ($this->uri->segment(4) == '54') {
            $handle = fopen('/var/log/mq_trendcall.log', 'r');
        } else {
            $handle = fopen('/var/log/mq_delta.log', 'r');
        }
        if (isset($this->session->offset)) {
            $data = stream_get_contents($handle, -1, $this->session->offset);
            echo nl2br($data);
        } else {
            fseek($handle, 0, SEEK_END);
            //$_SESSION['offset'] = ftell($handle);
            $this->session->set_userdata('offset', ftell($handle));
        }
        exit();
    }
    public function terminate_do()
    {
        $post     = file_get_contents("php://input");
        $obj      = (array) json_decode($post);
        $this->db = $this->load->database('default', true);
        $this->db->where('id', trim($obj['id']));
        $this->db->update('a_services', array(
            'termination_status' => '1'
        ));
        echo json_encode(array(
            'result' => $this->db->affected_rows()
        ));
    }
    public function addMageboClient()
    {
        $data = $this->Admin_model->getClient($_POST['id']);
        unset($_POST['id']);
        if ($data->mageboid > 0) {
            echo json_encode(array(
                'result' => 'error',
                'message' => lang('This customer has aleady mageboid: ' . $data->mageboid)
            ));
            exit;
        }
        $client = (array) $data;
        unset($client['stats']);
        $client['mvno_id'] = strtoupper(trim($client['mvno_id']));
        if ($client['country'] == "NL") {
            $pcode = explode(' ', trim($client["postcode"]));
            if (count($pcode) != 2) {
                echo json_encode(array(
                    'result' => 'error',
                    'message' => lang('Postcode must: NNNN XX formaat (102) ')
                ));
                exit;
            } else {
                if (strlen($pcode[0]) != 4) {
                    $this->session->set_userdata('registration', $client);
                    echo json_encode(array(
                        'result' => 'error',
                        'message' => lang('Postcode must: NNNN XX formaat (102) ')
                    ));
                    exit;
                } elseif (strlen($pcode[1]) != 2) {
                    echo json_encode(array(
                        'result' => 'error',
                        'message' => lang('Postcode ust: NNNN XX formaat (103) ')
                    ));
                    exit;
                }
            }
        } elseif ($client['country'] == "BE") {
            $pcode = explode(' ', 'BE ', trim($client["postcode"]));
        } else {
            $pcode = trim($client["postcode"]);
        }
        if (!empty($client['iban'])) {
            $bic = $this->Api_model->getBic(trim($client['iban']));
            if (!$bic->valid) {
            } else {
                $client['bic'] = $bic->bankData->bic;
            }
        }
        $client['date_created'] = date('Y-m-d');
        $client['email']        = strtolower(trim($client['email']));
        $magebo                 = $this->magebo->AddClient($client);
        if ($magebo->result == "success") {
            $client['mageboid'] = $magebo->iAddressNbr;
            $this->session->set_flashdata('success', lang('Client has been pushed in to magebo'));
            $this->db->where('id', $data->id);
            $this->db->update('a_clients', array(
                'mageboid' => $magebo->iAddressNbr
            ));
            $this->db->insert('tblcustomfieldsvalues', array(
                'fieldid' => 900,
                'relid' => $data->id,
                'value' => $magebo->iAddressNbr
            ));
            echo json_encode(array(
                'result' => 'success',
                'magebid' => $magebo->iAddressNbr
            ));
        } else {
            echo json_encode($magebo);
            exit;
        }
    }
    public function activate_porting()
    {
        $date = date('Y-m-d H:i:s');
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->load->helper('mq');
        $sn      = trim($this->uri->segment(4));
        $service = $this->db->query("select * from a_services_mobile where msisdn_sn =?", array(
            trim($sn)
        ));
        if ($service->num_rows() > 0) {
            $this->load->library('artilium', array(
                'companyid' => 53
            ));
            echo $service->row()->serviceid;
            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($sn));
            $kk   = $this->artilium->UpdateServices($msisdn->data->SN, $pack, '1');
            $this->db->query("update a_services set status='Active' where id=?", array(
                $service->row()->serviceid
            ));
            $this->db->query("update a_services_mobile set date_ported=? where serviceid=?", array(
                $date,
                $service->row()->serviceid
            ));
            ChangeServiceStatus($service->row()->serviceid, 'Active');
            $order = $this->Admin_model->getService($service->row()->serviceid);
            print_r($order);
            $this->Api_model->PortIngAction1($order);
            $this->Api_model->PortIngAction2($order);
            $this->Api_model->PortIngAction3($order);
            $this->Api_model->PortIngAction4($order);
            $this->Api_model->PortIngAction5($order->companyid);
            addSimcardLoggid(trim($order->companyid), trim($service->row()->msisdn_sim), '612');
        }
    }
    public function check_iban()
    {
        $this->load->model('Api_model');
        $iban = $this->uri->segment(4);
        $bic  = $this->Api_model->getBic(trim($iban));
        if (!$bic->valid) {
            echo json_encode(array(
                'result' => false,
                'data' => $bic
            ));
        } else {
            echo json_encode(array(
                'result' => true,
                'iban' => strtoupper(trim($iban)),
                'bic' => $bic->bankData->bic
            ));
        }
    }
    public function get_out_of_bundle_usage()
    {
        $userid = $this->uri->segment(4);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $total = '0.00';
        $this->load->library('xml');
        $usage                       = array(
            '0.00'
        );
        $this->data['activemobiles'] = getMobileActiveCli($userid);
        if (empty($this->data['activemobiles'])) {
            $usage = 0;
        } else {
            $f   = array();
            $cdr = array();
            foreach ($this->data['activemobiles'] as $IN => $order) {
                if ($order['domainstatus'] == 'Active') {
                    $this->data["service" . $IN] = $this->Admin_model->getService($order['id']);
                    $mobile                      = $this->data["service" . $IN]->details;
                    $f[]                         = array(
                        'domain' => $mobile->msisdn,
                        'sn' => $mobile->msisdn_sn,
                        'mobile' => $mobile
                    );
                    $cdr                         = $this->artilium->get_cdr($this->data["service" . $IN]);
                    $total                       = $cdr['total'] + $total;
                    unset($cdr);
                }
            }
            $usage = $total;
        }
        echo json_encode(array(
            'usage' => $usage
        ));
    }
    public function get_packages_detail_complete()
    {
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->client['language']);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        $packaged = $this->artilium->GetListPackageOptionsForSnAdvance($this->uri->segment(4));
        if ($packaged) {
            foreach ($packaged as $ee) {
                $rr                        = (array) $ee;
                $rr['CallModeDescription'] = lang(trim($rr['CallModeDescription']));
                $a['packages'][]           = $rr;
                unset($rr);
                unset($ee);
            }
        } else {
            $a['packages'] = array();
        }
        echo json_encode($a);
    }
    public function get_cdr_in()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $res = array();
        $id                    = $this->uri->segment(4);
        $order                 = $this->Admin_model->getService($id);
        $data = $this->artilium->GetFullCDRListWithDetails($order->details->msisdn_sn);

        echo json_encode($data);
    }
    public function get_service_detail()
    {
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->client['language']);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->lang->load('client');
        $this->load->model('Admin_model');
        header("Content-type: application/json");
        $this->data['result']  = 'success';
        $res                   = array();
        $id                    = $this->uri->segment(4);
        $order                 = $this->Admin_model->getService($id);
        $this->data['service'] = $order;
        $this->data['client']  = $this->Admin_model->getClient($this->data['service']->userid);
        if (getPlatform($id) == "ARTA") {
            $this->load->library('artilium', array(
            'companyid' => $this->companyid
            ));
            //$this->data['cdr'] = $this->artilium->get_cdr($order);
            //$this->data['cdrin'] = $this->artilium->GetFullCDRListWithDetails($order->details->msisdn_sn);
            if (in_array($this->data['service']->type, array(
            "mobile"
            ))) {
                $this->data['mobile'] = $this->data["service"]->details;
                $msisdn               = trim($this->data["service"]->details->msisdn_sn);
                $ss                   = $this->artilium->GetSn(trim($this->data["service"]->details->msisdn_sim));
                $this->data['sn']     = $ss->data;
                if (empty($this->data['mobile'])) {
                    $this->session->set_flashdata('error', lang('No data recieved from Unity Server for GetSim'));
                    redirect('errors/exceptions/' . $id);
                    exit;
                }
                $packaged = $this->artilium->GetListPackageOptionsForSn($order->details->msisdn_sn);
                $cli      = $this->artilium->getCLI($order->details->msisdn_sn);
                if ($this->session->email == 'simson.parlindungan@united-telecom.be') {
                }
                $voicemail = '0';
                if ($cli) {
                    $this->data['CountryGroup'] = $cli->CountryGroup;
                    if ($cli->DestinationsAcceptedRejected === 'false') {
                        $voicemail = '1';
                    } else {
                        $voicemail = '0';
                    }
                }
                if ($packaged) {
                    foreach ($packaged as $ee) {
                        $rr                        = (array) $ee;
                        $rr['CallModeDescriptionTooltip'] = lang(trim('tooltip :'.$rr['CallModeDescription']));
                        $rr['CallModeDescription'] = lang(trim($rr['CallModeDescription']));

                        $this->data['packages'][]  = $rr;
                        unset($rr);
                        unset($ee);
                    }
                } else {
                    $this->data['packages'] = array();
                }
                if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                    $this->data['packages'][] = array(
                    'CallModeDescription' => lang("Voicemail"),
                    'PackageDefinitionId' => '100000',
                    'CallModeDescriptionTooltip' => lang(trim('tooltip :Voicemail')),
                    'Available' => $voicemail,
                    'cli' => $cli
                    );
                }
                $xx = $this->artilium->GetBundleAssignList1($order->details->msisdn_sn);
                if ($xx) {
                    foreach ($xx as $x) {
                        // if($x->ValidUntil <= date('Y-m-d')){
                        $bd[] = $x;

                        // }
                    }
                } else {
                    $bd = array();
                }

                $this->data['bundles'] = $bd;

                //mail('mail@simson.one','undle',print_r($this->data['bundles'], true));
                $this->data['sum']     = $this->artilium->GetSUMAssignmentList(trim($order->details->msisdn_sn));
            }
        }
        unset($this->data['setting']);
        unset($this->data['mobile']);
        unset($this->data['service']);
        echo json_encode($this->data);
    }
    public function changeCountryGroup()
    {
        $_POST['serviceid']    = $this->uri->segment(4);
        $_POST['countrygroup'] = $this->uri->segment(5);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $res     = $this->artilium->ChangeCountryGroup($service->details->msisdn_sn, (string) $_POST['countrygroup']);
        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $service->userid,
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => $_SERVER['REMOTE_ADDT'],
            'description' => 'Changing  CountryGroup to ' . $_POST['countrygroup'] . ' for Serviceid ' . $_POST['serviceid']
        ));
        echo json_encode(array(
            'res' => $res,
            'post' => $_POST
        ));
    }
    public function get_bundlePrice()
    {
        $this->db = $this->load->database('default', true);
        $id       = $this->uri->segment(4);
        $q        = $this->db->query("select * from a_products_mobile_bundles where id=?", array(
            $id
        ));
        if ($q->num_rows() > 0) {
            echo json_encode(array(
                'result' => true,
                'price' => number_format($q->row()->recurring_total, 2),
                'bid' => $id
            ));
        } else {
            echo json_encode(array(
                'result' => true,
                'price' => '0.00',
                'bid' => $id
            ));
        }
    }
    public function getPriceProduct()
    {
        $product  = $this->uri->segment(4);
        $duration = $this->uri->segment(5);
        if (!$this->data['setting']->specific_pricing) {
            $q = $this->db->query("select * from a_products where id=?", array($product));

            $res = array(
                'price' => $this->data['setting']->currency.' ' . number_format($q->row()->recurring_total, 2)
            );
            $gid = $this->getproductinfo($product);
            if ($gid) {
                $res['gid']         = $gid;
                $res['addons']      = 'yes';
                $res['addons_html'] = '<label for="">' . lang('Options') . ':</label>';
                foreach (getAddonSell('mobile', $this->session->cid, $gid) as $row) {
                    $res['addons_html'] .= '<div class="form-check">
                          <label class="form-check-label">
                            <input name="addon[' . $row->id . ']" type="checkbox" class="form-check-input optionaddon" value="' . $row->id . '"';
                    if ($row->id == "18") {
                        $res['addons_html'] .= ' id="handset_id"';
                    }
                    $res['addons_html'] .= '>';
                    $res['addons_html'] .= str_replace('Option ', '', $row->name) . ' '.$this->data['setting']->currency . str_replace('.', ',', number_format($row->recurring_total, 2)) . lang('/month') . '        </label></div>';
                }
            } else {
                $res['addons'] = 'no';
            }
            echo json_encode($res);
            exit;
        }

        if ($duration == 1) {
            $this->db->select('monthly as price');
        } elseif ($duration == 3) {
            $this->db->select('quarterly as price');
        } elseif ($duration == 6) {
            $this->db->select('semi_annually as price');
        } elseif ($duration == 12) {
            $this->db->select('annually as price');
        } elseif ($duration == 24) {
            $this->db->select('bienially as price');
        } elseif ($duration == 36) {
            $this->db->select('triennially as price');
        }
        $this->db->where('packageid', $product);
        $q   = $this->db->get('a_pricing_specific');
        $res = array(
            'price' => $this->data['setting']->currency . number_format($q->row()->price, 2)
        );
        $gid = $this->getproductinfo($product);
        if ($gid) {
            $res['gid']         = $gid;
            $res['addons']      = 'yes';
            $res['addons_html'] = '<label for="">' . lang('Options') . ':</label>';
            foreach (getAddonSell('mobile', $this->session->cid, $gid) as $row) {
                $res['addons_html'] .= '<div class="form-check">
                      <label class="form-check-label">
                        <input name="addon[' . $row->id . ']" type="checkbox" class="form-check-input optionaddon" value="' . $row->id . '"';
                if ($row->id == "18") {
                    $res['addons_html'] .= ' id="handset_id"';
                }
                $res['addons_html'] .= '>';
                $res['addons_html'] .= str_replace('Option ', '', $row->name) . ' ' .$this->data['setting']->currency. str_replace('.', ',', number_format($row->recurring_total, 2)) . lang('/month') . '        </label></div>';
            }
        } else {
            $res['addons'] = 'no';
        }
        echo json_encode($res);
    }
    public function getproductinfo($id)
    {
        $q = $this->db->query('select * from a_products where id=?', array(
            $id
        ));
        if ($q->num_rows() > 0) {
            return $q->row()->gid;
        } else {
            return flase;
        }
    }
    public function getParameterCLI()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        $parameters = $this->artilium->GetParametersCLI($this->uri->segment(4));
        foreach ($parameters->ParameterResult->Parameters->Parameter as $row) {
            if ($row->ParameterId == 20511) {
                $P['PortinID'] = $row->ParameterValue;
            }
            if ($row->ParameterId == 20519) {
                $P['Sn'] = trim($row->ParameterValue);
            }
            if ($row->ParameterId == 20508) {
                $P['Msisdn'] = $row->ParameterValue;
            }
            if ($row->ParameterId == 20527) {
                $P['RequestDate'] = str_replace('/', '-', str_replace(' 00:00:00', '', $row->ParameterValue));
            }
        }
        echo json_encode($P);
    }
    public function check_url()
    {
        $q =$this->db->query("select * from a_mvno where portal_url like ?", array($_POST['url']));
        if ($q->num_rows()>0) {
            echo json_encode(array('result'=> true));
        } else {
            echo json_encode(array('result'=> false));
        }
    }
    public function tets()
    {
        //  print_r($this->session->userdata);
    }
    public function success()
    {
        $id = $this->uri->segment(4);

        $service = $this->Admin_model->getService($id);

        if ($service->status == "Active") {
            $this->session->set_flashdata('success', lang('Order has been activated'));
            redirect('admin/subscription/detail/'.$id);
        } else {
            $this->session->set_flashdata('success', lang('Your order has been created with ID: ') . $id . lang(' you need to accept this order below before further steps'));
            redirect('admin/subscription/pendingorders');
        }
    }
    public function get_porting_status()
    {
        $cnt     = 0;
        $id      = $this->uri->segment(4);
        $service = $this->Admin_model->getServiceCli($id);
        $this->load->library('artilium', array(
            'companyid' => $this->uri->segment(5)
        ));
        if (!empty($service->details->date_ported)) {
            echo json_encode(array(
                'result' => true,
                'date' => $service->details->date_ported
            ));
        } else {
            $list = $this->artilium->GetParametersCLI($service->details->msisdn_sn);
            if ($list->ParameterResult->Result == "0") {
                foreach ($list->ParameterResult->Parameters->Parameter as $row) {
                    if ($row->ParameterId == 20526) {
                        if (!empty($row->ParameterValue)) {
                            $ActionDate = $row->ParameterValue;
                            $cnt        = $cnt + 1;
                        }
                    }
                }
            }
            if ($cnt > 0) {
                $this->Admin_model->update_services_data('mobile', $id, array(
                    'date_ported' => str_replace('/', '-', $ActionDate)
                ));
                echo json_encode(array(
                    'result' => true,
                    'date' => str_replace('/', '-', $ActionDate)
                ));
            } else {
                echo json_encode(array(
                    'result' => false
                ));
            }
        }
    }
    public function addorder_mobile()
    {
        $companyid = $this->uri->segment(4);
        $result    = array(
            'result' => 'error'
        );
        $activate  = false;
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));


        $this->Teams_model->send_msg($this->uri->segment(3), "<br /><br /> REQUEST:<br />" . print_r($_POST, true));

        if (!empty($_POST['skip_accept'])) {
            if ($_POST['skip_accept'] == "yes") {
                if (getPlatformbyPid($_POST['addonid']) == "TEUM") {
                    if (!isTeumSimcardFree($_POST['simnumberx'])) {
                        echo json_encode(array(
                        'result' => 'error',
                        'message' => lang('You are trying to use auto activation but the provided simcard could not be found in the database, please make sure you have imported this simcard and it is free to use')
                          ));
                        exit;
                    }
                }
            }
        }
        $recurring          = "0.00";
        $update_handset     = false;
        $pass               = random_str('alphanum', 8);
        $extra              = false;
        $_POST['companyid'] = $this->companyid;
        if ($_POST['customer_type'] == "new") {
            if (!empty($_POST['delta_username'])) {
                if (validate_deltausername(strtolower(trim($_POST['delta_username'])))) {
                    $deltausername = $_POST['delta_username'];
                } else {
                    echo json_encode(array(
                        'result' => 'error',
                        'message' => 'your SSO username is not unique'
                    ));
                    exit;
                }
            } else {
                $deltausername = false;
            }
            unset($_POST['delta_username']);
            if (!empty($_POST['iban'])) {
                $bic = $this->Api_model->getBic(trim($_POST['iban']));
                if (!$bic->valid) {
                    echo json_encode(array(
                        'result' => 'error',
                        'message' => 'Your IBAN: ' . $_POST['iban'] . ' does not pass Validation'
                    ));
                    exit;
                } else {
                    $_POST['bic'] = $bic->bankData->bic;
                }
            }

            if ($this->data['setting']->mvno_id_increment == 1) {
                $_POST['mvno_id'] = genMvnoId($this->session->cid);
            } else {
                $_POST['mvno_id'] = strtoupper(trim($_POST['mvno_id']));
            }

            if (!empty($_POST['agentid'])) {
                $agentid = $_POST["agentid"];
            } else {
                $agentid = getDefaultAgentid($this->session->cid);
            }
            $cd     = array(
                'companyid' => $this->companyid,
                'agentid' => $agentid,
                'uuid' => gen_uuid(),
                'mvno_id' => $_POST['mvno_id'],
                'email' => $_POST['email'],
                'phonenumber' => $_POST['phonenumber'],
                'vat' => $_POST['vat'],
                'companyname' => $_POST['companyname'],
                'initial' => $_POST['initial'],
                'salutation' => $_POST['salutation'],
                'firstname' => $_POST['firstname'],
                'lastname' => $_POST['lastname'],
                'address1' => $_POST['address1'],
                'housenumber' => $_POST['housenumber'],
                'alphabet' => $_POST['alphabet'],
                'postcode' => $_POST['postcode'],
                'city' => $_POST['city'],
                'country' => $_POST['country'],
                'language' => $_POST['language'],
                'iban' => $_POST['iban'],
                'bic' => $_POST['bic'],
                'paymentmethod' => 'banktransfer',
                'gender' => $_POST['gender'],
                'sso_id' => '0',
                'nationalnr' => $_POST['nationalnr'],
                'date_created' => date('Y-m-d'),
                'invoice_email' => $_POST['invoice_email'],
                'date_birth' => $_POST['date_birth'],
                'vat_exempt' => $_POST['vat_exempt']

            );



            if (getPlatformbyPid($_POST['addonid']) != "TEUM") {
                if ($this->data['setting']->create_arta_contact == "1") {
                    if ($_POST['agentid'] != "-1") {
                        $agentid =  getContactTypeid($_POST['agentid']);
                    } else {
                        $agentid = 7;
                    }

                    $this->load->library('artilium', array('companyid' => $this->companyid));
                    sendlog('CreateContact', array('agentid' => $agentid, 'post' => $_POST));
                    $cont = $this->artilium->CreateContact($_POST, $agentid);
                    if ($cont) {
                        $_POST['ContactType2Id'] = $cont;
                    }
                }
            } else {
                if (!empty($_POST['idcard_number'])) {
                    $cd['id_type'] = $_POST['id_type'];
                    $cd['idcard_number'] = $_POST['idcard_number'];
                }
            }
            if ($this->data['setting']->mage_invoicing == "1") {
                $magebo = $this->magebo->AddClient($cd);
                if ($magebo->result != "success") {
                    echo json_encode(array(
                    'result' => 'error',
                    'message' => 'Failed to insert customer in to magebo'
                    ));
                    exit;
                } else {
                    $cd['mageboid'] = $magebo->iAddressNbr;
                }
            }
            $cd['date_modified'] = date('Y-m-d H:i:s');
            $clients = $this->Api_model->insertClient($cd);
            if ($clients->result != "success") {
                echo json_encode(array(
                    'result' => 'error',
                    'message' => $clients->message
                ));
                exit;
            } else {
                $userid = $clients->clientid;
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $userid,
                    'user' => $this->session->firstname.' '.$this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Adding customer Id: ' . $userid . ' and MageboId:' .$magebo->iAddressNbr,
                ));
                if (getPlatformbyPid($_POST['addonid']) == "TEUM") {
                    $this->load->library('pareteum', array(
                        'companyid' => $this->companyid,
                        'api_id' => getTeumApiId($_POST['addonid'])
                    ));
                    $array = array(
                    "CustomerData" => array(
                        "ExternalCustomerId" => (string)$cd['mvno_id'],
                        "FirstName" => $cd['firstname'],
                        "LastName" => $cd['lastname'],
                        "LastName2" => 'NA',
                        "CustomerDocumentType" => $cd['id_type'],
                        "DocumentNumber" => $cd['idcard_number'],
                        "Telephone" => $cd['phonenumber'],
                        "Email" => $cd['email'],
                        "LanguageName" => "eng",
                        "FiscalAddress" => array(
                            "Address" => $cd['address1'],
                            "City" =>$cd['city'],
                            "CountryId" => "76",
                            "HouseNo" => $cd['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $cd['postcode'],
                        ),
                        "CustomerAddress" => array(
                            "Address" => $cd['address1'],
                            "City" => $cd['city'],
                            "CountryId" => "76",
                            "HouseNo" => $cd['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $cd['postcode']
                        ),
                        "Nationality" => "GB"
                    ),
                    "comments" => "Customer added via UnitedPortal V1 by" . $this->session->firstname . ' ' . $this->session->lastname
                    );
                    log_message('error', json_encode($array));
                    $teum = $this->pareteum->AddCustomers($array);

                    log_message('error', json_encode($teum));
                    if ($teum->resultCode == "0") {
                        $this->Admin_model->updateClient($userid, array('teum_CustomerId' => $teum->customerId, 'teum_OrderCode' => $teum->orderCode));
                    }
                }
                //$this->db->query("update a_clients set teum_CustomerId=?, teum_OrderCode=? where id=?", array($teum->customerId, $teum->orderCode, $cc['id']));

                if (!empty($deltausername)) {
                    if (validate_deltausername(strtolower($deltausername))) {
                        $this->Admin_model->InsertDeltaUsername($deltausername, $userid);
                    }
                }
                $password = $this->Admin_model->changePasswordClient($userid);
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset']  = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $this->data['info']     = (object) array(
                    'email' => strtolower(trim($_POST['email'])),
                    'password' => $password,
                    'name' => $_POST['firstname'] . ' ' . $_POST['lastname']
                );
                $this->data['language'] = $_POST['language'];
                $client                 = $this->Admin_model->getClient($userid);
                $subject                = getSubject('signup_email', $client->language, $client->companyid);
                $body                   = getMailContent('signup_email', $client->language, $client->companyid);
                $body                   = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
                $body                   = str_replace('{$password}', $password, $body);
                $body                   = str_replace('{$email}', trim(strtolower($client->email)), $body);
                $body                   = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to(strtolower(trim($_POST['email'])));
                $this->email->bcc('lainard@gmail.com');
                $this->email->subject($subject);
                $this->email->message($body);
                if (!isTemplateActive($client->companyid, 'signup_email')) {
                    log_message('error', 'Template signup_email is disabled sending aborted');
                } else {
                    if ($this->email->send()) {
                        send_growl(array(
                        'message' => $this->session->firstname . ' ' . $this->session->lastname . ' has just added new client with id: ' . $userid,
                        'companyid' => $client->companyid
                        ));
                    }
                }
            }
        } else {
            $userid = $_POST['userid'];
        }
        if ($userid > 0) {
            $result['result']   = "success";
            $result['clientid'] = $userid;
        }
        $cc          = $this->Admin_model->getClient($userid);
        if (getPlatformbyPid($_POST['addonid']) == "ARTA") {
            $order_array = array(
                'companyid' => $this->companyid,
                'type' => 'mobile',
                'status' => 'New',
                'packageid' => $_POST['addonid'],
                'userid' => $userid,
                'date_created' => date('Y-m-d'),
                'billingcycle' => 'Monthly',
                'date_contract' => $_POST['ContractDate'],
                'contract_terms' => $_POST['ContractDuration'],
                'recurring' => getPriceRecurring($_POST['addonid'], $_POST['ContractDuration']),
                'notes' => 'Order via portal by: ' . $this->session->firstname . ' ' . $this->session->lastname
            );
        } elseif (getPlatformbyPid($_POST['addonid']) == "TEUM") {
            if (!empty($_POST['ContractDuration'])) {
                $recurring = getTotalRecurringfromAddon($_POST['addon']);
            } else {
                $recurring = "0.00";
            }

            $order_array = array(
                'companyid' => $this->companyid,
                'type' => 'mobile',
                'status' => 'New',
                'packageid' => $_POST['addonid'],
                'userid' => $userid,
                'date_created' => date('Y-m-d'),
                'billingcycle' => 'Monthly',
                'date_contract' => date('m-d-Y'),
                'date_contract_formated' => date('Y-m-d'),
                'contract_terms' => 1,
                'recurring' => $recurring,
                'notes' => 'Order via portal by: ' . $this->session->firstname . ' ' . $this->session->lastname
            );
        } else {
            $order_array = array(
                'companyid' => $this->companyid,
                'type' => 'mobile',
                'status' => 'New',
                'packageid' => $_POST['addonid'],
                'userid' => $userid,
                'date_created' => date('Y-m-d'),
                'billingcycle' => 'Monthly',
                'date_contract' => $_POST['ContractDate'],
                'contract_terms' => $_POST['ContractDuration'],
                'recurring' => getPriceRecurring($_POST['addonid'], $_POST['ContractDuration']),
                'notes' => 'Order via portal by: ' . $this->session->firstname . ' ' . $this->session->lastname
            );
        }

        if (!empty($_POST['Promotion'])) {
            $order_array['promocode'] = trim($_POST['Promotion']);
        }

        $needproforma = NeedProforma($userid);
        $serviceid = $this->Api_model->insertOrder($order_array);
        if (!$serviceid) {
            echo json_encode(array(
                'result' => 'error',
                'message' => 'Order failed please contact Support +32 484889888'
            ));
            exit;
        } else {
            logAdmin(array(
                        'companyid' =>$this->companyid,
                                            'userid' => $userid,
                                            'user' => 'System',
                                            'ip' => '127.0.0.1',
                                            'description' => 'Order Received ID ' . $serviceid,
                                        ));

            $result['result']    = "success";
            $result['serviceid'] = $serviceid;
            if ($_POST['msisdn_type'] == "porting") {
                $mobiledata = array(
                    'msisdn_type' => $_POST['msisdn_type'],
                    'donor_msisdn' => $_POST['MsisdnNumber'],
                    'donor_provider' => $_POST['provider'],
                    'donor_sim' => $_POST['simnumber'],
                    'donor_type' => $_POST['PortingType'],
                    'donor_customertype' => $_POST['customertype1'],
                    'msisdn_status' => 'OrderWaiting',
                    'date_wish' => $_POST['PortInWishDate'],
                    'serviceid' => $serviceid,
                    'companyid' => $this->companyid,
                    'userid' => $userid,
                    'msisdn' => $_POST['MsisdnNumber'],
                    'msisdn_pin' => '1111',
                    'msisdn_languageid' => setVoiceMailLanguageByClientLang($cc->language),
                    'ptype' => getSimType($this->companyid)
                );
                if (!empty($_POST['ptype_belgium'])) {
                    $mobiledata['ptype_belgium'] = $_POST['ptype_belgium'];
                } else {
                    $mobiledata['ptype_belgium'] = 'NA';
                }
                if (getPlatformbyPid($_POST['addonid'])  == "ARTA") {
                    if ($_POST['customertype1'] == "1") {
                        $mobiledata['donor_accountnumber'] = $_POST['AccountNumber'];
                    }
                } else {
                    $mobiledata['donor_accountnumber'] = $_POST['AccountNumber'];
                    $mobiledata['donor_customertype'] = "0";
                    $mobiledata['donor_type'] = "0";
                }
            } else {
                $mobiledata = array(
                    'msisdn_type' => $_POST['msisdn_type'],
                    'msisdn_status' => 'OrderWaiting',
                    'serviceid' => $serviceid,
                    'companyid' => $this->companyid,
                    'userid' => $userid,
                    'msisdn_pin' => '1111',
                    'ptype' => 'POST PAID'
                );
            }

            $mobiledata['ptype'] = getpType($_POST['addonid']);
            if (!empty($_POST['skip_accept'])) {
                if ($_POST['skip_accept'] == "yes") {
                    if (getPlatformbyPid($_POST['addonid']) == "ARTA") {
                        $mobiledata['msisdn_sim']  = $_POST['simnumberx'];
                        $this->load->library('artilium', array('companyid' => $this->companyid));
                        $sim                       = $this->artilium->getSn($_POST['simnumberx']);
                        $mobiledata['msisdn_sn']   = trim($sim->data->SN);
                        $mobiledata['msisdn']      = trim($sim->data->MSISDNNr);
                        $mobiledata['msisdn_puk1'] = $sim->data->PUK1;
                        $mobiledata['msisdn_puk2'] = $sim->data->PUK2;
                        $mobiledata['initial_reload_amount'] = getProductReloadAmount($product->gid);
                    } else {
                        $mobiledata['msisdn_sim']  = $_POST['simnumberx'];
                        $sim                       = simcardExist($_POST['simnumberx']);
                        if ($sim) {
                            $mobiledata['msisdn_sn']   = trim($sim->MSISDN);
                            $mobiledata['msisdn']      = trim($sim->MSISDN);
                            $mobiledata['msisdn_puk1'] = $sim->PUK1;
                            $mobiledata['msisdn_puk2'] = $sim->PUK2;
                            $mobiledata['initial_reload_amount'] = '0';
                        }
                    }
                }
                $activate = true;
            }
            $product                             = getProduct($_POST['addonid']);


            if ($this->data['setting']->create_arta_contact_customer == 1) {
                $mobiledata['msisdn_contactid']      = $cc->ContactType2Id;
            } else {
                $mobiledata['msisdn_contactid']      = getContactId($product->gid);
            }




            if (!empty($_POST['sim_delivery'])) {
                $this->load->model('Helpdesk_model');
                if ($_POST['sim_delivery'] == "United") {
                    $delivery                   = true;
                    $mobiledata['sim_delivery'] = "United";
                    $headers                    = "From: " . $this->data['setting']->smtp_sender . "\r\n" . "CC: mail@simson.one";
                    mail("techniek@united-telecom.be", "Process Simcard Delivery Orderid: " . $serviceid, "Hello\n\nNew order has been arrived,\n\nOrderid: " . $serviceid . " MVNO decide that you need to process the simcard\n\nRegard", $headers);
                } else {
                    $delivery                   = false;
                    $mobiledata['sim_delivery'] = "MVNO";
                }
            }
            if (getPlatformbyPid($_POST['addonid']) == "TEUM") {
                $mobiledata['platform'] = "TEUM";
            }
            $service_mobile = $this->Api_model->insertMobileData($mobiledata);
            if ($service_mobile > 0) {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $userid,
                    'user' => 'Api User',
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Adding Order with Serviceid ' . $serviceid
                ));
                $email = getNotificationOrderEmail($this->companyid);
                if ($email) {
                    $product = getProduct($_POST['addonid']);
                    $headers = "From: noreply@united-telecom.be" . "\r\n" . "BCC: mail@simson.one";
                    mail($email, "New order notification", "Dear Team,\n\nNew Order has just been arrived, Please process this order as soon as possible:\n\nServiceid: " . $serviceid . "\nProduct: " . $product->name . "\n\nRegards", $headers);
                }
                $result['result'] = "success";
                send_growl(array(
                    'message' => $this->session->firstname . ' ' . $this->session->lastname . ' has just added new order for Artiliumid: ' . $userid,
                    'companyid' => $this->companyid
                ));



                if (getPlatform($serviceid) == "TEUM") {
                    if ($_POST['addon'] != "NONE") {
                        $this->Api_model->addAddon($serviceid, $_POST['addon']);
                        $this->db->query("update a_services_addons set teum_autoRenew=? where serviceid=? and addonid=?", array($_POST['ContractDuration']-1, $serviceid, $_POST['addon']));
                        log_message('error', $this->db->last_query());
                    }
                    /*
                    if (!empty($_POST['ContractDuration'])) {
                        foreach ($_POST['ContractDuration'] as $key => $value) {

                            log_message('error', $this->db->last_query());
                        }
                    }
                    */
                } else {
                    if ($_POST['addon']) {
                        foreach ($_POST['addon'] as $addonidnya) {
                            $this->Api_model->addAddon($serviceid, $addonidnya);
                        }
                    }
                }

                if ($this->companyid == 54) {
                    if ($needproforma) {
                        $ppp = $this->create_proforma($this->companyid, $cc, $_POST['addonid'], $serviceid);
                        if ($ppp['proformaid'] > 0) {
                            $resmail = $this->sendProforma(array(
                                'invoiceid' => $ppp['proformaid'],
                                'invoicenum' => $ppp['invoicenum'],
                                'companyid' => $this->companyid
                            ));
                        }
                    } else {
                        $this->Admin_model->CreateFutureInvoiceOnActivation($serviceid, $cc->vat_rate, 7);
                    }
                }
            } else {
                $result['result']  = "error";
                $result['message'] = "Service mobile was not created";
            }
        }
        if ($activate) {
            if ($_POST['msisdn_type'] == "porting") {
                if ($service_teum->details->platform == "TEUM") {
                    $result['serviceid'] = $serviceid;
                } else {
                    $this->Admin_model->update_services($serviceid, array('status' => 'Pending'));
                    $result['serviceid'] = $serviceid;
                }
            } else {
                $service_teum = $this->Admin_model->getService($serviceid);
                $client  = $this->Admin_model->getClient($service_teum->userid);
                if ($service_teum->details->platform == "TEUM") {
                    // print_r($service_teum);
                    // exit;
                    $this->load->library('pareteum', array(
                        'companyid' => $this->companyid,
                        'api_id' => $service_teum->api_id
                    ));
                    if (empty($service_teum->details->teum_accountid)) {
                        $account = array(
                            "AccountInfo" => array(
                                "AccountType" => "Prepaid",
                                "CustomerId" => (string) $client->teum_CustomerId,
                                "ExternalAccountId" => (string)$serviceid.''.rand(10000000, 99999999),
                                "AccountStatus" => "Active",
                                "Names" => array(
                                    array(
                                        "LanguageCode" => "eng",
                                        "Text" => "Account"
                                    )
                                ),
                                "Descriptions" => array(
                                    array(
                                        "LanguageCode" => "eng",
                                        "Text" => "Account"
                                    )
                                ),
                                "AccountCurrency" => "GBP",
                                "Balance" => 0,
                                "CreditLimit" => 0
                            )
                        );
                        $acct    = $this->pareteum->CreateAccount($account);
                        log_message('error', print_r($acct, true));
                        if ($acct->resultCode == "0") {
                            $AccountId = $acct->AccountId;

                            $this->Admin_model->update_services_data('mobile', $serviceid, array('teum_accountid' => $AccountId));
                        } else {
                            die('Error when creating Account');
                        }
                    } else {
                        $AccountId = $service_teum->details->teum_accountid;
                    }


                    $addons = getaddons_teum($serviceid, $service_teum->details->msisdn_sn);
                    $subs   = array(
                        "CustomerId" => (int) $client->teum_CustomerId,
                        "Items" => array(
                            array(
                                "AccountId" => (string) $AccountId,
                                "ProductOfferings" => $addons,
                                "ServiceAddress" => array(
                                    "Address" => $client->address1,
                                    "HouseNo" => $client->housenumber,
                                    "City" => $client->city,
                                    "ZipCode" => $client->postcode,
                                    "State" => "unknown",
                                    "CountryId" => "76"
                                )
                            )
                        ),
                        "channel" => "UnitedPortal V1 by ".$this->session->firstname
                    );
                    log_message('error', print_r($subs, true));
                    $subscription = $this->pareteum->AddSubscription($subs);
                    log_message('error', 'AddSubscription'.print_r($subscription, true));
                    if ($subscription->resultCode == "0") {
                        $this->Admin_model->update_simcard_reseller($service_teum->details->msisdn_sn, array('serviceid' => $serviceid, 'AccountId' =>$AccountId ));
                        $this->db->query("update a_reseller_simcard set SubscriptionId=? where serviceid=?", array($subscription->Subscription->SubscriptionId, $serviceid));
                        //mme
                        $this->Admin_model->update_services_data('mobile', $serviceid, array('teum_customerid' => $client->teum_CustomerId, 'teum_subscriptionid' => $subscription->Subscription->SubscriptionId));
                        foreach ($subscription->Subscription->Products as $key => $row) {
                            foreach ($addons as $key => $r) {
                                if ($r['ProductOfferingId'] == $row->ProductOfferingId) {
                                    $addx = getAddonsbyBundleID($row->ProductOfferingId);
                                    $days  = ($addx->teum_autoRenew+1)*30;
                                    $this->Admin_model->updateAddonTeum($serviceid, $row->ProductOfferingId, array(
                                        'companyid' => $this->companyid,
                                        'teum_DateStart' => date('Y-m-d'),
                                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                                        'teum_DateEnd' => getFuturedate(date('Y-m-d'), 'day', $days),
                                        'teum_CustomerOrderId' => $subscription->CustomerOrderId,
                                        'teum_SubscriptionId' => $subscription->Subscription->SubscriptionId,
                                        'teum_ProductId' => $row->ProductId,
                                        'teum_ProductChargePurchaseId' => $row->ProductChargePurchaseId,
                                        'teum_SubscriptionProductAssnId' => $row->SubscriptionProductAssnId,
                                        'teum_ServiceId' => $subscription->Subscription->Services[$key]->ServiceId
                                    ));
                                    if ($key == "0") {
                                        $this->db->query("update a_reseller_simcard set TeumServiceId=?,CustomerOrderId=? where serviceid=?", array($subscription->Subscription->Services[$key]->ServiceId,$subscription->CustomerOrderId, $serviceid));
                                    }
                                }
                            }
                        }





                        if ($order->details->msisdn_type =="porting") {
                            $this->Admin_model->update_services_data('mobile', $serviceid, array(
                            'msisdn_status' => 'PortinPending',
                            'date_modified' => date('Y-m-d H:i:s'),
                            'teum_accountid' => $AccountId
                            ));
                        } else {
                            $this->Admin_model->update_services_data('mobile', $serviceid, array(
                            'msisdn_status' => 'Active',
                            'date_modified' => date('Y-m-d H:i:s'),
                            'teum_accountid' => $AccountId
                            ));
                        }



                        $this->db->query("update a_services set status='Active',recurring='0.00' where id=?", array(
                            $serviceid
                        ));
                        $this->session->set_flashdata('success', lang('Activation has been Requested'));

                        send_growl(array(
                            'message' => $this->session->firstname . ' ' . $this->session->lastname . ' activate Number: ' . $order->details->msisdn,
                            'companyid' => $this->session->cid
                        ));
                        $service = $this->Admin_model->getService($serviceid);
                        $addonlist_nobase = getAddonsbySericeidNo_Base($serviceid);
                        if ($addonlist_nobase) {
                            $this->Admin_model->insertTopup(
                                array(
                                   'companyid' => $this->companyid,
                                   'serviceid' => $serviceid,
                                   'userid' =>  $service->userid,
                                   'income_type' => 'bundle',
                                   'agentid' => $service->agentid,
                                   'amount' => $addonlist_nobase->recurring_total,
                                   'bundle_name' => $addonlist_nobase->name,
                                   'user' => $this->session->firstname . ' ' . $this->session->lastname)
                            );
                        }



                        logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $serviceid,
                            'userid' => $client->id,
                            'user' => $this->session->firstname . ' ' . $this->session->lastname,
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' . $serviceid . ' '.lang('requested for').' '.$order->details->msisdn_type.' '.lang('Activation').' ' . $sim->MSISDN
                        ));
                    }
                    echo json_encode(array(
                        'result' => "success",
                        'message' => "Service was auto activated with id ".$serviceid,
                        'serviceid' => $serviceid
                    ));
                    exit;
                //redirect('admin/subscription/detail/' . $_POST['serviceid']);
                } else {
                    $this->Admin_model->update_services($serviceid, array('status' => 'Pending'));
                    $result['serviceid'] = $serviceid;
                }
            }
        }
        echo json_encode($result);
    }
    public function get_events()
    {
        $events = array();
        $query  = $this->db->query("SELECT * FROM calendar");
        foreach ($query->result_array() as $fetch) {
            $e           = array();
            $e['id']     = $fetch['id'];
            $e['title']  = $fetch['title'];
            $e['start']  = $fetch['startdate'];
            $e['end']    = $fetch['enddate'];
            $allday      = ($fetch['allDay'] == "true") ? true : false;
            $e['allDay'] = $allday;
            array_push($events, $e);
        }
        echo json_encode($events);
    }

    public function get_pending_port_out_pending()
    {
        $totali = 0;
        // error_reporting(1);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if ($this->companyid == 55) {
            $list = $this->artilium->GetPendingPorts();
        } else {
            $list = $this->artilium->GetPendingPortOuts();
        }
        //log_message("error", print_r($list, 1));

        $dd = array();
        if ($list->GetPendingPortOutsResult->TotalItems > 1) {
            foreach ($list->GetPendingPortOutsResult->ListInfo as $key => $row) {
                if ($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status != "CANCELED") {
                    //  if(empty($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Validity->EndDate)){
                    //print_r($row);
                    $s['Id']            = $row->PortingId;
                    $s['OperationType'] = "Port Out";
                    $s['Status']        = $row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status;
                    $s['MSISDN']        = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['ActionDate']    = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['Remark']        = "Please accept or reject";
                    $s['CustomerName']  = getCustomernamebySerial(trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn));
                    foreach ($row->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity as $p) {
                        if ($p->ParameterId == 20512) {
                            if ($p->ParameterValue == 13) {
                                $s['Status'] = "Accepted";
                            } elseif ($p->ParameterValue == 27) {
                                $s['Status'] = "PortoutFailed";
                            } else {
                                $totali = $totali + 1;
                            }
                        }
                        if ($p->ParameterId == "20500") {
                            $s['DonorOperator'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20527") {
                            $s['ActionDate'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20506") {
                            $s['AccountNumber'] = $p->ParameterValue;
                        }
                        $s['RequestType'] = "Port Out";
                    }
                    $s['button'] = ' <button type="button"class="btn btn-success btn-sm pxs"  onclick="OpenReject(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Reject</button>
                    <button type="button"class="btn btn-danger btn-sm pxs"  onclick="OpenAccept(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Accept</button>';
                    $dd[]        = $s;
                    //   } //$row['Status'] != "CANCELED"
                }
            } //$list as $key => $row
        } elseif ($list->GetPendingPortOutsResult->TotalItems == 1) {
            $row = $list->GetPendingPortOutsResult->ListInfo;
            //print_r($row);
            $totali = 0;
            if ($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status != "CANCELED") {
                if (empty($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Validity->EndDate)) {
                    //print_r($row);
                    $s['Id']            = $row->PortingId;
                    $s['OperationType'] = "Port Out";
                    $s['Status']        = $row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status;
                    $s['MSISDN']        = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['ActionDate']    = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['Remark']        = "Please accept or reject";
                    $s['CustomerName']  = getCustomernamebySerial(trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn));
                    foreach ($row->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity as $p) {
                        if ($p->ParameterId == 20512) {
                            if ($p->ParameterValue == 13) {
                                $s['Status'] = "Accepted";
                            } elseif ($p->ParameterValue == 27) {
                                $s['Status'] = "PortoutFailed";
                            } else {
                                $totali = $totali + 1;
                            }
                        }
                        if ($p->ParameterId == "20500") {
                            $s['DonorOperator'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20527") {
                            $s['ActionDate'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20506") {
                            $s['AccountNumber'] = $p->ParameterValue;
                        }
                        $s['RequestType'] = "Port Out";
                    }
                    $s['button'] = ' <button type="button"class="btn btn-success btn-sm pxs"  onclick="OpenReject(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Reject</button>
                    <button type="button"class="btn btn-danger btn-sm pxs"  onclick="OpenAccept(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Accept</button>';
                    $dd[]        = $s;
                } //$row['Status'] != "CANCELED"
            }
        }
        echo json_encode(array(
            'total' => $totali
        ));
    }
    public function create_proforma($companyid, $client, $pid, $serviceid)
    {
        $result['proformaid'] = 0;
        $result['invoicenum'] = 0;
        $this->load->model('Admin_model');
        $killdate = new DateTime(date('Y-m-d'));
        $killdate->modify('+' . $client->payment_duedays . ' day');
        $setupfee = getSetupFee($pid, $client->vat_rate, $client->id);
        if ($this->data['setting']->country_base == "NL") {
            $inum = getNewInvoicenum($companyid);
            $this->load->library('magebo', array(
                'companyid' => $companyid
            ));
            $ogm = $this->magebo->Mod11($inum);
        } else {
            $ogm = ogm(getNewInvoicenum($companyid));
        }
        $proforma   = array(
            'companyid' => $companyid,
            'invoicenum' => getNewInvoicenum($companyid),
            'userid' => $client->id,
            'subtotal' => $setupfee->subtotal,
            'tax' => $setupfee->tax,
            'total' => $setupfee->total,
            'date' => date('Y-m-d'),
            'duedate' => $killdate->format('Y-m-d'),
            'ogm' => $ogm,
            'notes' => str_replace(' ', '', $ogm),
            'paymentmethod' => 'banktransfer',
            'status' => 'Unpaid'
        );
        $proformaid = $this->Admin_model->CreateProforma($proforma);
        if ($proformaid) {
            $this->Admin_model->CreateProformaItems(array(
                'companyid' => $companyid,
                'invoiceid' => $proformaid,
                'userid' => $client->id,
                'description' => 'Subscription Activation Fee (one time)',
                'price' => $setupfee->total,
                'qty' => 1,
                'amount' => $setupfee->total,
                'taxrate' => $client->vat_rate,
                'serviceid' => $serviceid,
                'invoicetype' => 'Service'
            ));
            $result['proformaid'] = $proformaid;
            $result['invoicenum'] = $proforma['invoicenum'];
        }
        return $result;
    }
    public function update_event()
    {
        $arr = array();
        $this->db->where('id', $_POST['eventid']);
        $this->db->update('calendar', array(
            'title' => $_POST['title']
        ));
        echo json_encode(array(
            'status' => 'success'
        ));
    }
    public function getLastTickets()
    {
    }
    public function put_events()
    {
        $arr = array(
            'startdate' => $_POST['start'],
            'enddate' => $_POST['end'],
            'title' => $_POST['title']
        );
        if ($_POST['total'] == "86400000") {
            $arr['allDay'] = 1;
        }
        $this->db->insert('calendar', $arr);
        echo json_encode(array(
            'status' => 'success',
            'data' => $arr
        ));
    }
    public function get_lang()
    {
        if (!empty($this->data['setting']->currency)) {
            $currency = $this->data['setting']->currency;
        } else {
            $currency = '€';
        }
        if (empty($this->session->language)) {
            echo json_encode(array(
                'currency' =>$currency,
                'result' => ucfirst($this->session->client['language'])
            ));
        } else {
            echo json_encode(array(
                'currency' => $currency,
                'result' => ucfirst($this->session->language)
            ));
        }
    }
    public function searchclient()
    {
        $list1    = array();
        $list2    = array();
        $list3    = array();
        $list4   = array();
        $keyword  = '%' . $_POST['keyword'] . '%';
        $this->db = $this->load->database('default', true);
        $query1   = $this->db->query("SELECT 'client' as type, id as value,concat('Client ',firstname,' ',lastname, ' ', companyname, ' #', mvno_id) as label, companyname FROM a_clients where (firstname like ? or lastname like ? or companyname like ? or id like ? or address1 like ? or postcode like ? or phonenumber like ? or mvno_id LIKE ? or mageboid LIKE ? or email like ? or iban like ?) and companyid=? group by id order by firstname DESC LIMIT 0, 40", array(
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $this->companyid
        ));
        $query2   = $this->db->query("SELECT 'service' as type, a.serviceid as value,concat('Mobile # ',a.msisdn,' :',b.firstname,' ',b.lastname) as label, concat(b.firstname,' ',b.lastname) as companyname from a_services_mobile a left join a_clients b on b.id=a.userid where (a.id like ? or a.msisdn like ? or a.msisdn_sn like ? or a.msisdn_sim like ? or a.serviceid like ?) and a.companyid=?  group by a.serviceid order by a.id DESC LIMIT 0, 40", array(
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $this->companyid
        ));
        $query3   = $this->db->query("SELECT 'service' as type, a.serviceid as value,concat('XDSL #', a.serviceid,' ',a.circuitid) as label, serviceid as companyname from a_services_xdsl a left join a_clients b on b.id=a.userid where (a.circuitid like ? or a.proximus_orderid like ?) and a.companyid= ? group by a.serviceid order by a.serviceid DESC LIMIT 0, 40", array(
            $keyword,
            $keyword,
            $this->companyid
        ));
        /*
                if($this->session->cid != 2){
                    $query4   = $this->db->query("SELECT 'invoice' as type, a.iInvoiceNbr as value,concat('Invoice #', a.iInvoiceNbr,' €', round(a.mInvoiceAmount, 2),' - ', firstname, ' ', lastname) as label, concat(b.firstname, ' ',b.lastname) as companyname,a.iAddressNbr from a_tblInvoice a left join a_clients b on b.mageboid=a.iAddressNbr where a.iInvoiceNbr like ? and a.iCompanyNbr= ? group by a.iInvoiceNbr order by a.iInvoiceNbr DESC LIMIT 0, 40", array(
                        $keyword,
                        $this->companyid
                    ));
                }

        */

        if ($query1->num_rows() > 0) {
            $list1 = $query1->result_array();
        }
        if ($query2->num_rows() > 0) {
            $list2 = $query2->result_array();
        }
        if ($query3->num_rows() > 0) {
            $list3 = $query3->result_array();
        }

        echo json_encode(array_merge($list1, $list2, $list3));
    }
    public function testuri()
    {
        echo "1 ".  $this->uri->segment(1)."<br>";
        echo "2 ".$this->uri->segment(2)."<br>";
        echo "3 ".$this->uri->segment(3)."<br>";
        echo "4 ".$this->uri->segment(4)."<br>";
    }
    public function searchclientx()
    {
        $list1    = array();
        $list2    = array();
        $list3    = array();
        $list4   = array();
        $keyword  = '%' . $_POST['keyword'] . '%';
        $this->db = $this->load->database('default', true);

        if (is_numeric(trim($_POST['keyword']))) {
            $query1   = $this->db->query("SELECT 'client' as type, id as value,concat('Client ',firstname,' ',lastname, ' ', companyname, ' #', mvno_id) as label, companyname FROM a_clients where ( id = ?  or  mageboid = ?) and companyid=? group by id order by firstname DESC LIMIT 0, 40", array(
            trim($_POST['keyword']),
            trim($_POST['keyword']),
            $this->companyid
            ));
        } else {
            $query1   = $this->db->query("SELECT 'client' as type, id as value,concat('Client ',firstname,' ',lastname, ' ', companyname, ' #', mvno_id) as label, companyname FROM a_clients where (firstname like ? or lastname like ? or companyname like ? or id like ? or address1 like ? or postcode like ? or phonenumber like ? or mvno_id LIKE ? or mageboid LIKE ? or email like ? or iban like ?) and companyid=? group by id order by firstname DESC LIMIT 0, 40", array(
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $this->companyid
            ));
        }




        echo json_encode($list1);
    }

    public function searchclienty()
    {
        $list1    = array();

        $keyword  = '%' . $_POST['keyword'] . '%';
        $this->db = $this->load->database('default', true);


        $query1   = $this->db->query("SELECT 'client' as type, id as value,concat('Client ',firstname,' ',lastname, ' ', companyname, ' #', mvno_id) as label, companyname FROM a_clients where   mageboid = ? and companyid=? group by id order by firstname DESC LIMIT 0, 40", array(
            trim($_POST['keyword']),
            $this->companyid
        ));


        if ($query1->num_rows()>0) {
            $list1 = $q->result_array();
        }



        echo json_encode($list1);
    }


    public function searchmvnoid()
    {
        $list1    = array();
        $keyword  = '%' . trim($_POST['keyword']) . '%';
        $this->db = $this->load->database('default', true);
        $query1   = $this->db->query("SELECT mageboid FROM a_clients where mvno_id LIKE ? and companyid=? group by id order by firstname DESC LIMIT 0, 1", array(
            $keyword,
            $this->companyid
        ));
        if ($query1->num_rows() > 0) {
            $list1 = $this->getInvoices($query1->row()->mageboid);
        }
        echo json_encode($list1);
    }
    public function searchmvnoidonly()
    {
        $list1    = array();
        $keyword  = '%' . trim($_POST['keyword']) . '%';
        $this->db = $this->load->database('default', true);
        $query1   = $this->db->query("SELECT concat(firstname, ' ', lastname, ' - ', mvno_id, '  - ',mageboid) as label, firstname, lastname, id, mvno_id as value, mageboid FROM a_clients where (mvno_id LIKE ? or firstname like ? or lastname like ? or mageboid like ?) and companyid=? group by id order by firstname DESC LIMIT 0, 1", array(
            $keyword,
            $keyword,
            $keyword,
            $keyword,
            $this->companyid
        ));
        if ($query1->num_rows() > 0) {
            echo json_encode($query1->result());
        }
    }
    public function getInvoices($iAddressNbr)
    {
        $option   = array();
        $this->db = $this->load->database('magebo', true);
        $q        = $this->db->query("select a.*,b.cName from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.iAddressNbr=? and a.iInvoiceStatus in ('52','53') and b.iCompanyNbr = ?", array(
            $iAddressNbr,
            $this->session->cid
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $option[] = array(
                    'cName' => $row->cName,
                    'iAddressNbr' => $iAddressNbr,
                    'invoiceid' => $row->iInvoiceNbr,
                    'amount' => str_replace(',', '.', number_format($row->mInvoiceAmount, 2)),
                    'value' => $row->iInvoiceNbr,
                    'label' => $row->cName . " " . $row->iInvoiceNbr . ' ' . str_replace(' 00:00:00.000', '', $row->dInvoiceDate) . ' '.$this->data['setting']->currency . number_format($row->mInvoiceAmount, 2)
                );
            }
        } else {
            $option = array();
        }
        return $option;
    }
    public function chat()
    {
        $this->load->library('simchat');
        $this->data['title']        = "Chat List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'chat';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function checkaddress()
    {
        $data = $_POST;
        //mail('mail@simson.one', 'mailbox', print_r($data, true));
        $result = $this->Admin_model->proximus_api($data);
        echo json_encode($result);
    }

    public function getXDSLResult()
    {
        $this->db = $this->load->database('default', true);
        $Q = $this->db->query("select * from a_services_xdsl_callback where action=? and orderid=? order by id desc limit 1", array('GetDetailedNetworkAvailability',$_POST['orderid']));

        if ($Q->num_rows()>0) {
            $array =  json_decode($Q->row()->rawdata);
            if ($array->interaction->feedback->type == "DISCARDED") {
                $result['result'] = 'error';
                $result['message'] = $array->interaction->feedback->description;

                echo json_encode($result);
                exit;
            }
            if (!empty($array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath->type)) {
                $row = $array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath;
                $ty['averageLength'] = $row->charVal[0]->value;
                $ty['unitOfMeasure'] = $row->charVal[0]->unitOfMeasure;
                $ty['minimumAttenuation'] = $row->charVal[1]->value;
                $ty['maximumAttenuation'] = $row->charVal[2]->value;
                $ty['numberOfFreePairs'] = $row->charVal[3]->value;
                $this->db->where('orderid', $array->customerOrder->customerOrderIdentifier->id);
                $this->db->update('a_services_xdsl_products', array(
                    'numberOfFreePairs' => $ty['numberOfFreePairs'],
                    'averageLength' => $ty['averageLength'],
                    'unitOfMeasure' => $ty['unitOfMeasure'],
                    'minimumAttenuation' => $ty['minimumAttenuation'],
                    'maximumAttenuation' => $ty['maximumAttenuation'],
                    'networkType' => $array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath->networkType,
                ));
            } else {
                $row = $array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath[0];
                $ty['averageLength'] = $row->charVal[0]->value;
                $ty['unitOfMeasure'] = $row->charVal[0]->unitOfMeasure;
                $ty['minimumAttenuation'] = $row->charVal[1]->value;
                $ty['maximumAttenuation'] = $row->charVal[2]->value;
                $ty['numberOfFreePairs'] = $row->charVal[3]->value;
                $this->db->where('orderid', $array->customerOrder->customerOrderIdentifier->id);
                $this->db->update('a_services_xdsl_products', array(
                    'numberOfFreePairs' => $ty['numberOfFreePairs'],
                    'averageLength' => $ty['averageLength'],
                    'unitOfMeasure' => $ty['unitOfMeasure'],
                    'minimumAttenuation' => $ty['minimumAttenuation'],
                    'maximumAttenuation' => $ty['maximumAttenuation'],
                    'networkType' => $array->networkDetails->pathDetails->feasibilityNetworkPaths->networkPath[0]->networkType,
                ));
            }




            $tt = $this->db->query('SELECT * from a_services_xdsl_products where orderid = ? and numberOfFreePairs  > 0', array($_POST['orderid']));
            if ($tt->num_rows() > 0) {
                $z = $z+60;
                $result['orderid'] = $_POST['orderid'];

                foreach ($tt->result_array() as $product) {
                    if (($product['product_name'] == "Carrier VDSL2 GO") && in_array($product['serviceAvailabilityStatus'], array( "OK","OKBUT","" ))) {
                        $users3['GO'] = "OK";
                        $a['min_download'][] = preg_replace("/[^0-9,.]/", "", $product['measuredMaximumDownloadSpeed']);
                        $a['min_upload'][] = preg_replace("/[^0-9,.]/", "", $product['measuredMaximumUploadSpeed']);
                    }

                    if ($product['product_name'] == "Carrier VDSL2 START" && in_array($product['serviceAvailabilityStatus'], array( "OK","OKBUT","" ))) {
                        $users3['START'] = "OK";
                        $a['min_download'][] = preg_replace("/[^0-9,.]/", "", $product['measuredMaximumDownloadSpeed']);
                        $a['min_upload'][] = preg_replace("/[^0-9,.]/", "", $product['measuredMaximumUploadSpeed']);
                    }
                }
                if ($users3['GO'] == "OK" || $users3['START'] == "OK" || $users3['GO'] == "OKBUT" || $users3['START'] == "OKBUT") {
                    $result['vdsl'] = "OK";
                }
                $result['result'] = 'success';
                $result['min_download'] = max($a['min_download']);
                $result['min_upload'] =  max($a['min_upload']);
                $result['products'] = $tt->result_array();
                $this->session->set_userdata('XDSL', $result);
            //$_SESSION['XDSL'] = $result;
            } else {
                $result['result'] = 'error';
            }
        } else {
            $result['result'] = 'error';
        }

        echo json_encode($result);
    }
    public function zip()
    {
        $list     = array();
        $keyword  = $_POST['keyword'] . '%';
        $this->db = $this->load->database('default', true);
        $query    = $this->db->query("SELECT code as value,concat(code,' ',city) as label,city from public_zip where code like ?", array(
            $keyword
        ));
        if ($query->num_rows() > 0) {
            $list = $query->result_array();
        }
        echo json_encode($list);
    }
    public function street()
    {
        $list     = array();
        $zip      = $_POST['zipcode'];
        $keyword  = '%' . $_POST['keyword'] . '%';
        $this->db = $this->load->database('default', true);
        $query    = $this->db->query("SELECT name as value,name as label from public_street where zip=? and  name like ?", array(
            $zip,
            $keyword
        ));
        if ($query->num_rows() > 0) {
            $list = $query->result_array();
        }
        echo json_encode($list);
    }
    public function invoices()
    {
        $list     = array();
        $keyword  = $_POST['keyword'] . '%';
        $this->db = $this->load->database('magebo', true);
        $a        = $this->db->query("select TOP 10 a.*, b.cName from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.iInvoiceNbr LIKE ? and b.iCompanyNbr = ? and a.iInvoiceStatus = ? and a.iInvoiceType = ? order by a.iInvoiceNbr", array(
            $keyword,
            $this->companyid,
            '52',
            '40'
        ));
        if ($a->num_rows() > 0) {
            foreach ($a->result_array() as $res) {
                $list[] = array(
                    'cName' => $res['cName'],
                    'iAddressNbr' => $res['iAddressNbr'],
                    'mInvoiceAmount' => $res['mInvoiceAmount'],
                    'value' => $res['iInvoiceNbr'],
                    'label' => $res['iInvoiceNbr'] . ' '.$this->data['setting']->currency . number_format($res['mInvoiceAmount'], 2) . ' ' . $res['iAddressNbr'] . ' ' . $res['cName']
                );
            }
        }
        echo json_encode($list);
    }
    public function getCreditnoteInvoicetobeProcess()
    {
        $list     = array();
        $keyword  = $_POST['keyword'] . '%';
        $this->db = $this->load->database('magebo', true);
        $a        = $this->db->query("select TOP 10 a.*, b.cName from tblInvoice a left join tblAddress b on b.iAddressNbr=a.iAddressNbr where a.iInvoiceNbr LIKE ? and b.iCompanyNbr = ? and a.iInvoiceStatus in('52','54') and a.iInvoiceType = ? order by a.iInvoiceNbr", array(
            $keyword,
            $this->companyid,
            '40'
        ));
        if ($a->num_rows() > 0) {
            foreach ($a->result_array() as $res) {
                $list[] = array(
                    'cName' => $res['cName'],
                    'iAddressNbr' => $res['iAddressNbr'],
                    'mInvoiceAmount' => $res['mInvoiceAmount'],
                    'value' => $res['iInvoiceNbr'],
                    'label' => $res['iInvoiceNbr'] . ' '.$this->data['setting']->currency . number_format($res['mInvoiceAmount'], 2) . ' ' . $res['iAddressNbr'] . ' ' . $res['cName']
                );
            }
        }
        echo json_encode($list);
    }
    public function getcustomerid()
    {
        $id       = $_POST['userid'];
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from a_clients where id=? and companyid=?", array(
            $id,
            $this->companyid
        ));
        $m        = $this->db->query("select * from a_clients_sso where userid = ?", array(
            $id
        ));
        $client   = $q->row_array();
        if ($m->num_rows() > 0) {
            $client['DeltaUsername'] = $m->row()->username;
        } else {
            $client['DeltaUsername'] = "";
        }
        echo json_encode($client);
    }

    public function getcustomeridx()
    {
        $id       = $_POST['userid'];
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query("select * from a_clients where id=? and companyid=?", array(
            $id,
            $this->companyid
        ));
        $m        = $this->db->query("select * from a_clients_sso where userid = ?", array(
            $id
        ));
        $client   = $q->row_array();
        if ($m->num_rows() > 0) {
            $client['DeltaUsername'] = $m->row()->username;
        } else {
            $client['DeltaUsername'] = "";
        }
        echo json_encode($client);
    }
    public function tmpi()
    {
        $this->db = $this->load->database('default', true);
        $a        = $this->db->query('select id as productids, orderid from tblhosting');
        foreach ($a->result() as $row) {
            echo $this->Api_model->getRecurringPricing($row) . "\n";
        }
    }
    public function testx()
    {
        $serviceid = 700192;
        $q         = $this->Api_model->XgetServiceAddons($serviceid);
        print_r($q);
        foreach ($q->addon as $aa) {
            if (in_array($aa->addonid, array(
                '6',
                '7',
                '8',
                '9',
                '10',
                '11',
                '12',
                '13',
                '34',
                '35',
                '36',
                '37',
                '41',
                '40',
                '42',
                '43'
            ))) {
                $relid = $aa->id;
            }
        }
        echo $relid;
        $s = $this->Api_model->XgetAddonValue('iGeneralPricingIndex', $relid);
        print_r($s);
    }
    public function set_ticket_visible()
    {
        $companyid = $this->uri->segment(4);
        $this->db->where('companyid', $companyid);
        $this->db->where('id', $_POST['id']);
        $this->db->update('a_helpdesk_tickets', array(
            'adminonly' => 0
        ));
        echo json_encode(array(
            'result' => $this->db->affected_rows()
        ));
    }
    public function getInvoicesGraph()
    {
        if ($this->data['setting']->mage_invoicing) {
            $this->data['invoices'] = $this->Admin_model->getInvoicebyMonth($this->session->cid);
            echo json_encode(array('result' => true, 'data' => "[".implode(',', $this->data['invoices'])."]"));
        } else {
            echo json_encode(array('result' => false));
        }
    }
    public function getsimcard()
    {
        $companyrange = getCompanyRange($this->uri->segment(4));
        $setting = globofix($this->uri->segment(4));

        $list1        = array();
        $keyword      = '%' . $_POST['keyword'] . '%';

        if ($setting->mobile_platform == "TEUM") {
            $this->db     = $this->load->database('default', true);
            $query1       = $this->db->query("SELECT simcard as value, concat(MSISDN, ' ', simcard) as label,MSISDN as msisdn  from a_reseller_simcard where  MSISDN like ? and serviceid is null and companyid=? order by simcard asc LIMIT 10", array(
            $keyword,
            $this->companyid
            ));
        } else {
            if ($this->uri->segment(4) == "104") {
                $this->db     = $this->load->database('default', true);
                $query1       = $this->db->query("SELECT simcard as value, concat(MSISDN, ' ', simcard) as label, MSISDN as msisdn from a_reseller_simcard where simcard like ? and serviceid is null and companyid=? order by simcard asc LIMIT 10", array(
                $keyword,
                $this->companyid
                ));
            } else {
                $this->db     = $this->load->database('magebo', true);
                $query1       = $this->db->query("SELECT TOP 10 cSIMCardNbr as value, cSIMCardNbr as label, iPincode as msisdn from tblC_SimCard where iPincode is NULL and iCompanyRangeNbr = ? and cSIMCardNbr like ? order by cSIMCardNbr DESC", array(
                $companyrange,
                $keyword
                ));
            }
        }

        log_message('error', $this->db->last_query());

        if ($query1->num_rows() > 0) {
            $list1 = $query1->result_array();
        }
        echo json_encode($list1);
    }
    public function getorderdetails()
    {
        $this->db = $this->load->database('default', true);
        header('Content-Type: application/json');
        $html     = '<table class="table table-striped">';
        $id       = $_POST['orderid'];
        $s        = $this->db->query("select a.*,b.msisdn_sim,b.msisdn_type,d.name as productname,concat(c.firstname,' ',c.lastname) as customername,c.mvno_id
            from a_services a
            left join a_services_mobile b on b.serviceid=a.id
            left join a_clients c on c.id=a.userid
            left join a_products d on d.id=a.packageid
            where a.id=?", array(
            $id
        ));
        $contract = explode('-', $s->row()->date_contract);

        $addons = $this->db->query("select * from a_services_addons where serviceid=?", array($id));


        $html .= '
        <tr>
        <td width="30%">' . lang('Order ID') . '</td>
        <td width="70%" class="text-right">' . $s->row()->id . '</td>
        </tr>
        <tr>
        <td width="30%">' . lang('Order Date') . '</td>
        <td class="text-right">' . date("d/m/Y", strtotime($s->row()->date_created)) . '</td>
        </tr>

        <tr>
        <td width="30%">' . lang('Customer') . '</td>
        <td class="text-right"><a href="' . base_url() . 'admin/client/detail/' . $s->row()->userid . '">' . $s->row()->customername . ' ( #' . $s->row()->mvno_id . ' )</a></td>
        </tr>
        <tr>
        <td width="30%">' . lang('Product') . '</td>
        <td class="text-right"><b>' . $s->row()->productname . '</b></td>
        </tr>';


        if ($addons->num_rows()>0) {
            foreach ($addons->result() as $key => $oo) {
                $html .='
            <tr>
            <td width="30%">' . lang('Addon') .' ('.$oo->id.')</td>
            <td class="text-right text-danger"><b>' . $oo->name . '</b></td>
            </tr>';
            }
        }

        $html .='<tr>
        <td width="30%">' . lang('Billingcycle') . '</td>
        <td class="text-right">' . lang($s->row()->billingcycle) . '</td>
        </tr>
        <tr>
        <td width="30%">' . lang('MSISDN TYPE') . '</td>
        <td class="text-right text-danger">' . lang(ucfirst($s->row()->msisdn_type) . " Number") . '</td>
        </tr>
        <tr>
        <td width="30%">' . lang('Recurring') . '</td>
        <td class="text-right">'.$this->data['setting']->currency . number_format($s->row()->recurring, 2) . '</td>
        </tr>
        <tr>
        <td width="30%">' . lang('Promocode') . '</td>
        <td class="text-right">' . $s->row()->promocode . '</td>
        </tr>
        </tr>
        <td width="30%">' . lang('Contract Start Date') . '</td>
        <td class="text-right"><b>' . $contract[1] . "/" . $contract[0] . "/" . $contract[2] . '</b></td>
        </tr>
        <tr style="display:none;" id="msisdnxy">
        <td width="30%">' . lang('MSISDN') . '</td>
        <td class="text-right text-danger"><div id="msisdnxx"></div></td>
        </tr>

        ';
        $html .= '</table>';
        echo json_encode(array(
            'result' => $html,
            'serviceid' => $s->row()->id,
            'userid' => $s->row()->userid,
            'msisdn_sim' => $s->row()->msisdn_sim,
            'msisdn_type' => $s->row()->msisdn_type
        ));
    }

    public function getBundleList()
    {
        $html     = "";
        $number   = $_POST['msisdn'];
        $type     = $_POST['type'];
        $this->db = $this->load->database('default', true);
        if ($type == "tarif") {
            $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and status = 1 order by id asc", array(
                $type,
                $this->uri->segment(4)
            ));
        } else {
            if (!empty($_POST['agid'])) {
                $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and agid=? and status = 1 order by id asc", array(
                    $type,
                    $this->uri->segment(4),
                    $_POST['agid']
                ));
            } else {
                $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and status = 1 order by id asc", array(
                    $type,
                    $this->uri->segment(4)
                ));
            }
        }

        if (getPlatform($_POST['serviceid']) == "TEUM") {
            // $addons = $this->Admin_model->getOrderedAddonid($_POST['serviceid']);
            foreach ($q->result() as $bundle) {
                /* if ($addons) {
                     if (!in_array($bundle->id, $addons)) {
                         if ($bundle->recurring_total > 0) {
                             $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency . number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                         } else {
                             $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                         }
                     }
                 } else {
                     */
                if ($bundle->recurring_total > 0) {
                    $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency . number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                } else {
                    $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                }
                //}
            } //$q->result() as $bundle
        } else {
            foreach ($q->result() as $bundle) {
                if ($bundle->recurring_total > 0) {
                    $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency . number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                } else {
                    $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                }
            } //$q->result() as $bundle
        }
        echo json_encode(array(
            'result' => true,
            'html' => $html,
            'price' => number_format($q->row()->recurring_total, 2)
        ));
    }
    public function activate_sim()
    {
        $id = $this->uri->segment(4);
        $this->load->model('Admin_model');
        $m = $this->Admin_model->getService($id);
        print_r($m);
        $client = $this->Admin_model->getClient($m->userid);
        print_r($client);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $rest = $this->artilium->ActiveNewSIM($client->firstname . ' ' . $client->lastname, $m->details);
        print_r($rest);
    }
    public function portingme()
    {
        $id = $this->uri->segment(4);
        $this->load->model('Admin_model');
        $m = $this->Admin_model->getService($id);
        print_r($m);
        $client = $this->Admin_model->getClient($m->userid);
        print_r($client);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $result = $this->artilium->PortingNewSIM($client->firstname . ' ' . $client->lastname, $m->details);
        print_r($result);
    }
    public function sendProforma($dd)
    {
        error_reporting(1);
        echo "Sending Proforma\n";
        $this->data['setting'] = globofix($dd['companyid']);
        $this->load->model('Admin_model');
        $this->load->model('Proforma_model');
        print_r($dd);
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/';
        $invoiceid    = $dd['invoiceid'];
        $invoice      = $this->Proforma_model->getInvoice($invoiceid);
        $client       = $this->Admin_model->getClient($invoice['userid']);
        $this->load->library('magebo', array(
            'companyid' => $client->companyid
        ));
        if ($dd['invoicenum'] <= 0) {
            die('Invoice not found 11');
        }
        $invoice_ref = $this->magebo->Mod11($dd['invoicenum']);

        $res = $this->download_id($invoiceid, $dd['companyid'], $this->data['setting']);
        if ($res) {
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body   = getMailContent('proforma', $client->language, $client->companyid);
            $amount = $invoice['total'];
            $body   = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
            $body   = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $body);
            $body   = str_replace('{$invoiceid}', $invoice['id'], $body);
            $body   = str_replace('{$clientid}', $client->mvno_id, $body);
            $body   = str_replace('{$dInvoiceDate}', $invoice['date'], $body);
            $body   = str_replace('{$dInvoiceDueDate}', $invoice['duedate'], $body);
            $body   = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
            $body   = str_replace('{$name}', format_name($client), $body);
            $body   = str_replace('{$cInvoiceReference}', $invoice_ref, $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            $subject = getSubject('proforma', $client->language, $client->companyid);
            $subject = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $subject);
            $subject = str_replace('{$Companyname}', $this->data['setting']->companyname, $subject);
            $this->email->bcc($this->session->email);
            $this->email->subject($subject);
            $this->email->attach($res);
            $this->email->message($body);
            if (!isTemplateActive($client->companyid, 'proforma')) {
                log_message('error', 'Template proforma is disabled sending aborted');
                return array(
                    'result' => true
                );
            }

            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => $subject,
                    'message' => $body
                ));
                return array(
                    'result' => true
                );
            } else {
                return array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                );
            }
        } else {
            return array(
                'result' => false
            );
        }
    }
    public function download_id($id, $companyid, $setting)
    {
        $this->data['setting'] = $setting;
        set_time_limit(0);
        $this->load->model('Admin_model');
        if ($companyid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
            $this->lang->load('admin');
        }
        $this->config->set_item('language', 'english');
        $this->lang->load('admin');
        $invoice    = $this->Proforma_model->getInvoice($id);
        $customerku = $this->Proforma_model->getClient($invoice['userid']);
        $this->load->library('magebo', array(
            'companyid' => $companyid
        ));
        $this->load->library('Spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        $pdf->SetAutoPageBreak(true, 30);
        $pdf->SetAuthor('Simson Parlindungan & Tim Claesen');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->setHeaderFont(array(
            PDF_FONT_NAME_MAIN,
            '',
            PDF_FONT_SIZE_MAIN
        ));
        $pdf->setFooterFont(array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));
        $pdf->setFoot($brand->brand_footer);
        $pdf->setFooterFont('droidsans');
        $tax = round($invoice['tax']) . '%';
        $pdf->AddPage('P', 'A4');
        $pdf->Image($brand->brand_logo, 10, 10, 60);
        $style  = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $style1 = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                64,
                64,
                64
            ),
            'bgcolor' => false,
            'color' => array(
                255,
                255,
                255
            ),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $pdf->setY(13);
        $pdf->setX(130);
        $pdf->GetPage();
        $pdf->Image($brand->brand_logo, 10, 10, 90);
        $pdf->SetFont('droidsans', '', 10);
        if (!empty($customerku->companyname)) {
            $company = $customerku->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            $contact = $customerku->firstname . " " . $customerku->lastname . "\n";
        }
        //$address1 = str_replace("&#039;", "'", $customerku->address1) . "\n";
        $address1 = str_replace("&#039;", "'", $customerku->address1. ' '.$customerku->housenumber. ' '.$customerku->alphabet) . "\n";
        $pdf->setY(70);
        $city    = $customerku->postcode . ", " . $customerku->city . "\n";
        $country = $customerku->country . "\n";
        $pdf->SetFont('droidsans', 'B', 12);
        $pdf->setCellPaddings(0, 0, 0, 0);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $a = '';
        $pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 30, 'T');
        $pdf->MultiCell(90, 30, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryNameLang($customerku->country, $customerku->language), 0, 'L', 1, 1, '', '', true, 0, false, true, 30, 'M');
        if ($this->data['setting']->country_base == "BE") {
            $this->data['invoice_ref'] = ogm($this->uri->segment(4));
        } else {
            $this->data['invoice_ref'] = $this->magebo->Mod11($invoice['invoicenum']);
        }
        $pdf->SetFont('droidsans', 'B', 14);
        $pdf->cell(70, 4, lang("Proforma Invoice") . ": " . $invoice['invoicenum'], 0, 1, 'L');
        $pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(64, 64, 64);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        ));
        $pdf->MultiCell(35, 7, lang('Client Number'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(40, 7, lang('VAT'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(50, 7, lang('Reference'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Date'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Duedate'), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->setCellPaddings(1, 1, 1, 1);
        $pdf->setCellMargins(1, 1, 1, 1);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('droidsans', '', 9);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        ));
        $pdf->MultiCell(35, 5, trim($invoice['mvno_id']), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(40, 5, $customerku->vat, 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(50, 5, $this->data['invoice_ref'], 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['date'])), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['duedate'])), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->Ln(15);
        $tblhtml = '<table width="550" bgcolor="#404040" cellspacing="1" cellpadding="1" border="0">
    <tr height="40" bgcolor="#404040" style="font-weight:bold;text-align:center;">
    <td align="left" width="7%"  color="#fff" >' . lang('Qty') . '</td>
        <td align="left" width="58%"  color="#fff">' . lang('Description') . '</td>
         <td align="right" width="18%" color="#fff">' . lang('Price/pcs') . '</td>
        <td align="right" width="18%" color="#fff">' . lang('Amount') . '</td>
    </tr>';
        foreach ($invoice['items'] as $item) {
            $tblhtml .= '
    <tr bgcolor="#fff" border="1">
         <td align="left">' . $item['qty'] . '<br /></td>
        <td align="left">' . nl2br($item['description']) . '<br /></td>
        <td align="right">' . $this->data['setting']->currency . number_format($item['price'], 2) . '</td>
        <td align="right">' . $this->data['setting']->currency . exvat($item['taxrate'], $item['amount']) . '</td>
    </tr>';
        }
        $tblhtml .= '

</table>';
        $pdf->writeHTML($tblhtml, true, false, false, false, '');
        $pdf->Ln(5);
        $styles = array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        );
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        ));
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, 'Subtotal' . " :", 1, 'R', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['subtotal'], 2), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                64,
                64,
                64
            )
        ));
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, lang("VAT ") . floor($invoice['items'][0]['taxrate']) . '%:', 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['tax'], 2), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->SetFillColor(64, 64, 64);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->MultiCell(30, 5, lang("Total :"), 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['total'], 2), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $taxrate = 1;
        $lebar   = '38';
        $ypos    = '100';
        $xpos    = '161';
        $pdf->setY(-120);
        $pdf->SetLeftMargin(-10);
        $pdf->SetRightMargin(0);
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('droidsansb', 'B', 7);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setImageScale(1.53);
        $pdf->SetLineStyle(array(
            'width' => 0.5,
            'cap' => 'butt',
            'join' => 'miter',
            'dash' => 0,
            'color' => array(
                0,
                0,
                0
            )
        ));
        $pdf->RoundedRect(5, 240, 200, 40, 3.50, '1111');
        $pdf->setXY(10, 242);
        $pdf->SetFont('droidsansb', '', 12);
        $pdf->Cell(140, 0, lang("Please pay amount of ") . " ".$this->data['setting']->currency . number_format($invoice['total'], 2) . " " . lang("to be transfered before ") . date("d-m-Y", strtotime($invoice['duedate'])), 0, 0, 'R', 0, '0', 4);
        $pdf->setXY(10, 248);
        $pdf->Cell(100, 0, lang("On account number") . " : NL10RABO0147382548", 0, 0, 'R', 0, '0', 4);
        if ($invoice['items'][0]['taxrate'] > 0) {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        } else {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(30, 0, lang("Vat Exempt"), 0, 0, 'R', 0, '0', 4);
            $pdf->ln(4);
            $pdf->setXY(10, 270);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        }
        $pdf->Output($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf', 'F');
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf')) {
            return $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf';
        } else {
            return false;
        }
    }
}
