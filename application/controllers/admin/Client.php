<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ClientController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }

        $this->companyid = get_companyidby_url(base_url());
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Client extends ClientController
{
    public function __construct()
    {
        parent::__construct();
        /*
        $classes = get_class_methods($this);
        foreach ($classes as $class) {
        if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
        $this->db->insert('admin_permissions', array('folder' => 'admin', 'controller' => 'client', 'method' => $class));
        $this->db->insert('admin_roles', array('rolename' => 'superadmin', 'perms' => $this->db->insert_id()));
        }

        }
         */
        $this->load->model('Admin_model');
        $this->load->model('Teams_model');
        $this->load->model('Api_model');
        if (!empty($this->session->profiler)) {
            if ($this->session->profiler) {
                $this->output->enable_profiler(true);
                error_reporting(E_ALL);
                ini_set('display_errors', 1);
            }
        }
    }
    /*
      public function switchlang()
      {
          // Switch Language Function
          $this->session->set_userdata('language', $this->uri->segment(4));
          redirect('admin/auth');
      }
*/
    public function index()
    {
        $this->data['stats'] = $this->Admin_model->getClientsByMonth($this->session->cid);
        //$this->data['stats'] = $this->Admin_model->getStatistic();
        $this->data['dtt'] = 'clients.js?version=' . time();
        $this->data['title'] = "Client List Dasboard";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'client';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function map()
    {
        $this->data['maps'] = $this->Admin_model->getClientmaps($this->session->cid);


        $this->data['title'] = "Client Mapping";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'map';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function view_email_log()
    {
        $mail  = $this->Admin_model->getEmailLog($this->uri->segment(4), $this->session->cid);
        if (!$mail) {
            die('Acces Denied');
        }
        // print_r($mail);
        $this->data['email'] = $mail;
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'email_log', $this->data);
    }
    public function get_mageboid()
    {
        $clientid = getClientidbyMagebo($this->uri->segment(4));

        redirect('admin/client/detail/'.$clientid);
    }
    public function detail()
    {
        $this->data['client_vat'] = "";
        $id = $this->uri->segment(4);
        if (!isAllowed_Clientid($this->session->cid, $id)) {
            $this->session->set_flashdata('error', 'Client does not exists');

            redirect('admin/client');
        }
        if (!empty($this->uri->segment(5))) {
            if ($this->uri->segment(5) == "download_file") {
                $this->data['download'] = true;
            } elseif ($this->uri->segment(5) == "remove_session") {
                $this->session->unset_userdata('client');
                //unset($_SESSION['client']);
                $this->data['download'] = false;
            } else {
                $this->data['download'] = false;
            }
        } else {
            $this->data['download'] = false;
        }



        $this->data['dtt'] = 'clients_detail.js?version=32.4';
        $this->data['client'] = $this->Admin_model->getClient($id);
        if ($this->data['client']->mageboid == "0") {
            if ($this->data['setting']->mage_invoicing == 1) {
                if ($this->session->master) {
                    if ($this->addMageboClient($id)) {
                        $this->data['client'] = $this->Admin_model->getClient($id);
                    }
                }
            }
        }
        if (empty($this->data['client'])) {
            $this->session->set_flashdata('error', 'Client not found');
            redirect('admin/client');
        }
        if ($this->data['client']->paymentmethod == "directdebit") {
            $this->data['financial'] = $this->Admin_model->getMandateId($this->data['client']->mageboid);
            if ($this->session->id == 1) {
                if (empty($this->data['financial']->IBAN)) {
                    $this->sepa_amend_cli(array('type' => 'NEW', 'userid' => $this->data['client']->id, 'iban' => $this->data['client']->iban));
                    //print_r($this->data['financial']);
                    $this->data['financial'] = $this->Admin_model->getMandateId($this->data['client']->mageboid);
                }
            }
        }


        if ($this->data['setting']->mobile_platform == "TEUM") {
            $this->load->model('Api_model');
            $reqx = $this->Api_model->checkRequired(array(
                'housenumber','idcard_number','id_type','address1','city','postcode','firstname','lastname','mvno_id'
            ), (array)$this->data['client']);
            if ($reqx['result'] != "success") {
                $this->session->set_flashdata('error', "Client data Incomplete please edit before continue :".$reqx['message']);
                redirect('admin/client/edit/'.$this->data['client']->id);
            }



            if (empty($this->data['client']->teum_CustomerId)) {
                $roro = (array) $this->data['client'];
                $this->load->library('pareteum', array('companyid' => $this->companyid));
                $array = array(
                "CustomerData" => array(
                    "ExternalCustomerId" => (string)$roro['mvno_id'],
                    "FirstName" => $roro['firstname'],
                    "LastName" => $roro['lastname'],
                    "LastName2" => 'NA',
                    "CustomerDocumentType" => $roro['id_type'],
                    "DocumentNumber" => $roro['idcard_number'],
                    "Telephone" => $roro['phonenumber'],
                    "Email" => $roro['email'],
                    "LanguageName" => "eng",
                    "FiscalAddress" => array(
                        "Address" => $roro['address1'],
                        "City" =>$roro['city'],
                        "CountryId" => "76",
                        "HouseNo" => $roro['housenumber'],
                        "State" => "Unknown",
                        "ZipCode" => $roro['postcode'],
                    ),
                    "CustomerAddress" => array(
                        "Address" => $roro['address1'],
                        "City" => $roro['city'],
                        "CountryId" => "76",
                        "HouseNo" => $roro['housenumber'],
                        "State" => "Unknown",
                        "ZipCode" => $roro['postcode']
                    ),
                    "Nationality" => "GB"
                ),
                "comments" => "Customer added via UnitedPortal V1 by " . $this->session->firstname . ' ' . $this->session->lastname
                );
                log_message('error', json_encode($array));
                $teum = $this->pareteum->AddCustomers($array);
                // echo json_encode($teum);

                log_message('error', json_encode($teum));
                if ($teum->resultCode == "0") {
                    $this->db->query("update a_clients set teum_CustomerId=?, teum_OrderCode=? where id=?", array($teum->customerId, $teum->orderCode, $roro['id']));
                }
            }
        }

        //if($this->session->email == "simson.parlindungan@united-telecom.be"){


        if ($this->data['setting']->mage_invoicing) {
            $this->load->library('magebo', array('companyid' => $this->session->cid));
            $this->data['balance'] = $this->magebo->GetFinancialCondition($this->data['client']->mageboid);
            $this->data['payments'] = $this->magebo->viewPayments($this->data['client']->mageboid);
            $this->data['msubs'] = $this->Admin_model->getMSubscription($id);
            $this->data['nextInvoices'] = $this->Admin_model->getNextInvoices($this->data['client']->mageboid);
        } else {
            $this->data['payments'] =false;
            $this->data['msubs'] =false;
            $this->data['nextInvoices'] = false;
            $this->data['balance'] = "0.00";
        }
        //print_r($this->data);
        // print_r($nextInvoices);
        // exit;
        $this->data['title'] = lang("Client Summary") . ": " . $this->data['client']->firstname . ' ' . $this->data['client']->lastname;
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'client_detail';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function deleteSubscription()
    {
        $service = $this->Admin_model->getService($_POST['id']);
        if ($service->companyid == $this->session->cid) {
            $array = array("Pending");
            if (in_array($service->status, $array)) {
                $this->db->query("delete from a_services where id=?", array($_POST['id']));
                $this->db->query("delete from a_services_mobile where serviceid=?", array($_POST['id']));
                logAdmin(array('companyid' => $this->session->cid, 'userid' => $service->userid, 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Remove Subscription ID: ' . $_POST['id']));
                echo json_encode(array('result' => true, 'userid' => $service->userid));
            } else {
                echo json_encode(array('result' => false, 'message' => lang('You may not delete Service with Status Active or Terminated'), 'status' => $service->status));
            }
        } else {
            echo json_encode(array('result' => false, 'message' => lang('unauthorized access')));
        }
    }
    public function ciot()
    {
        $this->data['client'] = $this->Admin_model->getClient($id);
        $this->data['title'] = lang("Client CIOT Edit") . ": " . $this->data['client']->firstname . ' ' . $this->data['client']->lastname;
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'client_detail';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function add_sms_number()
    {
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_sms_notifications', $_POST);
        if ($this->db->insert_id()>0) {
            $this->session->set_flashdata('success', 'Number: '.$_POST['msisdn'].' was  added and set to received financial sms notification');
        } else {
            $this->session->set_flashdata('error', lang('Number was not added'));
        }

        echo json_encode(array('result' => true));
    }

    public function delete_sms_notification()
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('userid', $_POST['userid']);
        $this->db->where('msisdn', $_POST['msisdn']);
        $this->db->delete('a_sms_notifications');

        if ($this->db->affected_rows()>0) {
            $this->session->set_flashdata('success', 'Number deleted as requested');
        } else {
            $this->session->set_flashdata('error', 'Number was not deleted, please try again later');
        }


        echo json_encode(array('result' => true));
    }
    public function sync_general_pricing()
    {
        $this->load->library('magebo', array('companyid' => $this->companyid));
        $service = $this->db->query("select * from a_services where userid=? and status=?", array($_POST['userid'], 'Active'));
        if ($service->num_rows() > 0) {
            foreach ($service->result() as $row) {
                $mobile = $this->Admin_model->getServiceCli($row->id);
                if (!$row->iGeneralPricingIndex) {
                    // $this->magebo->addPricingSubscription($mobile) . "\n";
                    if ($mobile->detauls->msisdn_type == "porting" && substr($mobile->details->msisdn, 0, 2) == "31") {
                        $addsim = $this->magebo->AddPortinSIMToMagebo($mobile);
                        $this->magebo->addPricingSubscription($mobile) . "\n";
                        $this->load->model('Api_model');
                        $this->Api_model->PortIngAction1($mobile);
                        $this->Api_model->PortIngAction2($mobile);
                        $this->Api_model->PortIngAction3($mobile);
                        $this->Api_model->PortIngAction4($mobile);
                        $this->Api_model->PortIngAction5($mobile->companyid);
                        $this->magebo->addPricingSubscription($mobile) . "\n";
                    } else {
                        $this->magebo->addPricingSubscription($mobile) . "\n";
                    }
                    unset($mobile);
                }
            }
        }
        echo json_encode(array('result' => true));
    }
    public function import_client_from_magebo()
    {
        $mageboid  = trim($_POST['mageboid']);
        $this->load->library('magebo', array(
            'companyid' => $this->companyid,
        ));


        if ($this->Admin_model->isCustomer($this->companyid, $mageboid)) {
            $this->session->set_flashdata('error', 'Client Magebo: '.$mageboid.' exists already');
            redirect('admin/client/detail/');
            exit;
        }


        $client = $this->magebo->getiAddress($mageboid);
        $client['companyid'] = 2;
        $this->db = $this->load->database('default', true);
        $this->db->insert('a_clients', $client);
        $id =  $this->db->insert_id();

        $this->session->set_flashdata('success', 'Client Magebo: '.$mageboid.' has been imported to MVNO portal with clientid '.$id);
        redirect('admin/client/detail/'.$id);
    }
    public function list_all_notes()
    {
        $this->data['title'] = "List Notes";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'notes';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function download_file()
    {
        //Download files documents
        $pdf = $this->Admin_model->getWelcomePdf($this->uri->segment(4));
        //print_r($pdf);
        $decoded = base64_decode($pdf->doc);
        $file = "/tmp/" . $this->uri->segment(4);
        file_put_contents($file, $decoded);

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }
    public function get_client_state()
    {
        $id = $this->uri->segment(4);
        $whmcs = $this->Admin_model->getClientidByState($id);
        if ($whmcs) {
            redirect('admin/client/detail/' . $whmcs);
        } else {
            $this->session->set_flashdata('error', lang('Customer not found'));
            redirect('admin/dashboard');
        }
    }

    public function contact_add()
    {
        if (isPost()) {
            $this->db = $this->load->database('default', true);
            $_POST['companyname'] = $_POST['role'];
            unset($_POST['role']);
            $_POST['companyid'] = COMPANYID;
            $this->db->insert('a_clients_contacts', $_POST);
            if ($this->db->insert_id() > 0) {
                $this->session->set_userdata('registration', $_POST);
                $this->session->set_flashdata('success', lang('Contact successfully saved for customer ') . $_POST['userid']);
                redirect('admin/client/detail/' . $_POST['userid']);
            } else {
                $this->session->set_flashdata('error', lang('Contact was not inserted'));
                redirect('admin/client/contact_add/' . $_POST['userid']);
            }
        } else {
            // unset($_SESSION['registration']);
            $this->session->unset_userdata('registration');
        }

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'client_contact_add';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function contact_edit()
    {
        $id = $this->uri->segment(4);
        if (isPost()) {
            $_POST['companyname'] = $_POST['role'];

            unset($_POST['role']);
            $this->db = $this->load->database('default', true);
            $this->db->where('id', $id);
            $this->db->where('companyid', $this->companyid);
            $this->db->update('a_clients_contacts', $_POST);
            if ($this->db->affected_rows() > 0) {
                $this->session->set_flashdata('success', lang('Contact successfully saved for customer'));
            } else {
                $this->session->set_flashdata('error', lang('Contact was not updated or nothing changed'));
            }
        }




        $this->data['contact'] = $this->Admin_model->getContact($this->uri->segment(4));
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'client_contact_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function delete()
    {
        $id = $_POST['id'];

        if (!$id) {
            $id =  $this->uri->segment(4);
        }
        $client = $this->Admin_model->getClient($id);

        if ($client->mageboid == 0) {
            $ids = $this->Admin_model->DeleteClient($id);
            $this->Admin_model->Deleteservices($id);
            $this->session->set_flashdata('success', lang(' Customer has been deleted as requested'));
            redirect('admin/client');
        }

        if ($this->session->master) {
            $this->load->library('magebo', array('companyid' => $this->session->cid));
            if (count_services($id) == 0 && count_invoices($client->mageboid, $client->companyid) == 0) {
                $this->magebo->deleteClient($client->mageboid);
                $ids = $this->Admin_model->DeleteClient($id);
                $this->Admin_model->Deleteservices($id);
                echo json_encode(array('result' => 1, 'message' => lang('You can not delete a client who has invoice or services')));
            } else {
                if (empty($client->mageboid)) {
                    $ids = $this->Admin_model->DeleteClient($id);
                    $this->Admin_model->Deleteservices($id);
                    $this->magebo->deleteClient($client->mageboid);


                    if ($this->data['setting']->mobile_platform == "ARTA" && $this->data['setting']->create_arta_contact == "1") {
                        if (!empty($client->ContactType2Id) && $client->ContactType2Id != "7") {
                            $this->load->library('artilium', array('companyid' => $this->session->cid));
                            $this->artilium->DeleteContactType2(array('ContactType2Id' => $client->ContactType2Id, 'DeleteType' => 'MoveAllNumbersToParentFromThisContactOrChildren'));
                        }
                    }
                    $this->session->set_flashdata('success', lang($ids . ' Customer has been deleted as requested'));
                } else {
                    echo json_encode(array('result' => 0, 'message' => lang('You can not delete a client who has invoice or services')));
                    //$this->session->set_flashdata('error', lang('You can not delete a client who has invoice or services'));
                }
            }
        } else {
            echo json_encode(array('result' => 0, 'message' => lang('access unauthorized')));
        }

        //redirect('admin/client');
    }
    public function contact_delete()
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $this->uri->segment(4));
        $this->db->where('companyid', $this->companyid);
        $this->db->delete('a_clients_contacts');
        $this->session->set_flashdata('success', lang('Contact successfully removoed for customer'));
        redirect('admin/client/detail/' . $this->uri->segment(5));
    }

    public function send_email()
    {
        if (isPost()) {
            $this->load->library('umail', array('companyid' => $this->companyid));
            $setting = $this->data['setting'];
            //$result = $this->Admin_model->whmcs_api(array('action' => 'SendEmail', 'id' => $_POST['userid'], 'messagename' => 'Password Reset Validation'));
            $client = $this->Admin_model->getClient($_POST['userid']);
            $this->config->set_item('language', $client->language);
            if ($client) {
                $this->umail->send_email($client, $_POST);
            } else {
                $this->session->set_flashdata('error', 'Client not found');
                redirect('admin/client/');
            }
        } else {
            $this->session->set_flashdata('error', 'Email was not sent');
            redirect('admin/client/');
        }
    }
    public function send_password()
    {
        if (isPost()) {
            $setting = $this->data['setting'];
            $client = $this->Admin_model->getClient($_POST['userid']);
            $this->config->set_item('language', $client->language);
            if ($client) {
                $password = "tmp" . rand(1000000, 90000000);
                $this->db = $this->load->database('default', true);
                $this->db->where('id', $_POST['userid']);
                $this->db->where('companyid', $this->companyid);
                $this->db->update('a_clients', array('password' => password_hash($password, PASSWORD_DEFAULT)));
                $this->load->library('umail', array('companyid' => $client->companyid));
                $this->umail->client_send_password($client, $password);
            } else {
                echo json_encode(array('result' => false, 'error' => lang('Client not found')));
            }
        } else {
            echo json_encode(array('result' => 'false', 'error' => lang('wrong method')));
        }
    }
    public function assign_agent()
    {
        $this->db->where('companyid', $this->session->cid);
        $this->db->where('id', $_POST['userid']);
        if ($_POST['agentid'] == "0") {
            $_POST["agentid"] == null;
        }
        $this->db->update('a_clients', array('agentid' => $_POST['agentid']));

        logAdmin(array('companyid' => $this->session->cid, 'userid' => $_POST['userid'], 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Assign Customer ' . $_POST['userid'] . ' to Reseller :' . $_POST['agentid']));

        redirect('admin/client/detail/' . $_POST['userid']);
    }
    public function add()
    {
        $this->load->model('Api_model');
        if (isPost()) {
            $this->Teams_model->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " " . print_r($_SESSION, true));
            $this->session->set_userdata('registration', $_POST);
            if (in_array($this->session->cid, array(53, 56))) {
                if (!empty($_POST['sso_username'])) {
                    $sso_username = $_POST['sso_username'];
                    if (!validate_deltausername(strtolower($sso_username))) {
                        echo json_encode(array('result' => false, 'message' => 'Your ' . lang('Delta Username') . ' has been used by other account'));
                        exit;
                    }
                }
            }
            if ($this->data['setting']->mvno_id_increment == 1) {
                $_POST['mvno_id'] = genMvnoId($this->session->cid);
            } else {
                $_POST['mvno_id'] = strtoupper(trim($_POST['mvno_id']));
            }

            if ($_POST['country'] == "NL") {
                $pcode = explode(' ', trim($_POST["postcode"]));
                if (count($pcode) != 2) {
                    $this->session->set_userdata('registration', $_POST);
                    echo json_encode(array('result' => false, 'message' => 'Your POST CODE: ' . $_POST['postcode'] . ' is not valid example:  NNNN XX formaat'));
                    exit;
                } else {
                    if (strlen($pcode[0]) != 4) {
                        $this->session->set_userdata('registration', $_POST);
                        echo json_encode(array('result' => false, 'message' => 'Your POST CODE: ' . $_POST['postcode'] . ' is not valid example:  NNNN XX formaat'));
                        exit;
                    } elseif (strlen($pcode[1]) != 2) {
                        $this->session->set_userdata('registration', $_POST);
                        echo json_encode(array('result' => false, 'message' => 'Your POST CODE: ' . $_POST['postcode'] . ' is not valid example:  NNNN XX formaat'));
                        exit;
                    }
                }
            } elseif ($_POST['country'] == "BE") {
                $pcode = explode(' ', 'BE ', trim($_POST["postcode"]));
            } else {
                $pcode = trim($_POST["postcode"]);
            }

            if (!empty($_POST['iban'])) {
                $bic = $this->Api_model->getBic(trim(str_replace(' ', '', $_POST['iban'])));
                if (!$bic->valid) {
                    echo json_encode(array('result' => false, 'message' => 'Your IBAN: ' . $_POST['iban'] . ' is not valid'));
                    exit;
                } else {
                    $_POST['bic'] = $bic->bankData->bic;
                }
            }
            if ($_POST['paymentmethod'] == "directdebit") {
                if (empty($_POST['iban'])) {
                    echo json_encode(array('result' => false, 'message' => 'Fore directdebit payment you need to provide valid IBAN'));
                    exit;
                }
            }
            $_POST['date_created'] = date('Y-m-d');

            unset($_POST['sso_username']);

            $_POST['email'] = strtolower(trim($_POST['email']));
            $_POST['companyid'] = $this->companyid;


            if ($this->data['setting']->create_arta_contact == 1) {
                if ($_POST['agentid'] != "-1") {
                    $agentid =  getContactTypeid($_POST['agentid']);
                } else {
                    $agentid = 7;
                }
                $this->load->library('artilium', array('companyid' => $this->companyid));
                sendlog('CreateContact', array('agentid' => $agentid, 'post' => $_POST));
                $cont = $this->artilium->CreateContact($_POST, $agentid);


                if ($cont) {
                    $_POST['ContactType2Id'] = $cont;
                }
            }
            if ($this->data['setting']->mage_invoicing == 1) {
                $this->load->library('magebo', array('companyid' => $this->companyid));
                $magebo = $this->magebo->AddClient($_POST);
                if ($magebo->result == "success") {
                    $_POST['mageboid'] = $magebo->iAddressNbr;
                } else {
                    echo json_encode($magebo);
                    exit;
                }
            }
            $_POST['date_modified'] = date('Y-m-d H:i:s');
            $cc = $this->Admin_model->insertCustomer($_POST);

            if ($cc['result']) {
                if ($this->data['setting']->mobile_platform == "TEUM") {
                    $this->load->library('pareteum', array('companyid' => $this->companyid));
                    $array = array(
                    "CustomerData" => array(
                        "ExternalCustomerId" => (string)$_POST['mvno_id'],
                        "FirstName" => $_POST['firstname'],
                        "LastName" => $_POST['lastname'],
                        "LastName2" => 'NA',
                        "CustomerDocumentType" => $_POST['id_type'],
                        "DocumentNumber" => $_POST['idcard_number'],
                        "Telephone" => $_POST['phonenumber'],
                        "Email" => $_POST['email'],
                        "LanguageName" => "eng",
                        "FiscalAddress" => array(
                            "Address" => $_POST['address1'],
                            "City" =>$_POST['city'],
                            "CountryId" => "76",
                            "HouseNo" => $_POST['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $_POST['postcode'],
                        ),
                        "CustomerAddress" => array(
                            "Address" => $_POST['address1'],
                            "City" => $_POST['city'],
                            "CountryId" => "76",
                            "HouseNo" => $_POST['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $_POST['postcode']
                        ),
                        "Nationality" => "GB"
                    ),
                    "comments" => "Customer added via UnitedPortal V1 by " . $this->session->firstname . ' ' . $this->session->lastname
                    );
                    log_message('error', json_encode($array));
                    $teum = $this->pareteum->AddCustomers($array);
                    // echo json_encode($teum);

                    log_message('error', json_encode($teum));
                    if ($teum->resultCode == "0") {
                        $this->db->query("update a_clients set teum_CustomerId=?, teum_OrderCode=? where id=?", array($teum->customerId, $teum->orderCode, $cc['id']));
                    }
                }

                if ($_POST['paymentmethod'] == "directdebit") {
                    if ($this->data['setting']->mage_invoicing == 1) {
                        $sepa = $this->magebo->addMageboSEPA($_POST['iban'], $_POST['bic'], $magebo->iAddressNbr);
                        $this->Admin_model->addMandate(array('userid' => $cc['id'],
                        'companyid' => $this->companyid,
                        'mandate_id' => $sepa->SEPA_MANDATE,
                        'mandate_date' => $sepa->SEPA_MANDATE_SIGNATURE_DATE,
                        'mandate_status' => $sepa->SEPA_STATUS));
                    }
                }

                if (!empty($sso_username)) {
                    if (validate_deltausername(strtolower($sso_username))) {
                        $this->Admin_model->InsertDeltaUsername($sso_username, $cc['id']);
                    }
                }
                //$this->Admin_model->whmcs_api(array('action' => 'UpdateClient', 'clientid' => $client['id'], 'password2' => $pass));
                if (!empty($_SESSION['registration'])) {
                    $this->session->unset_userdata('registration');
                    // unset($_SESSION['registration']);
                }
                //$this->Admin_model->InsertCustomFields($client['id'], $customfields);

                $password = $this->Admin_model->changePasswordClient($cc['id']);
                $client = $this->Admin_model->getClient($cc['id']);
                $this->load->library('umail', array('companyid' => $this->session->cid));
                $this->umail->client_add($client, $password);
            } else {
                $this->session->set_userdata('registration', $_POST);
                $this->session->set_flashdata('error', lang($cc['message']));
                echo json_encode(array('result' => false, 'message' => lang($cc['message'])));
                exit;
            }
        }
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'client_add';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function add_extra_nextinvoice_items()
    {

        //print_r($_POST);
        //exit;
        $effectiveDate = date('Y-m-01', strtotime('+1 month'));
        $t = $_POST['terms'] - 1;
        $date = date('Y-m-d', strtotime("+" . $t . " months", strtotime($effectiveDate)));
        $_POST['end_date'] = $date;
        if ($this->Admin_model->IsAllowedClient($_POST['userid'], $this->session->cid)) {
            $this->load->library('magebo', array('companyid' => $this->session->cid));
            $client = $this->Admin_model->getClient($_POST['userid']);
            if ($_POST['type'] == "RECUR") {
                $this->db->insert('a_subscriptions_manual', $_POST);
                $terms = $_POST["terms"];

                if ($terms <= 1) {
                    $this->session->set_flashdata('error', 'You choose recurring but the terms is only 1 month, please choose One Time instead');
                    redirect('admin/client/detail/' . $_POST['userid']);
                }
            } else {
                $terms = 1;
            }
            $option = (object) array('terms' => $terms, 'name' => $_POST['description'], 'recurring_total' => str_replace(',', '.', $_POST['amount']));
            $contract = date('m-01-Y', strtotime('+1 month'));
            $id = $this->magebo->addExtraPricing($client, $option, $contract);
            if ($id > 0) {

                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $client->id,
                    'serviceid' =>'0',
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Adding Discount / New Pricing items  for '.$_POST['description'].' Amount: '.$_POST['amount']
                ));


                $this->magebo->updatePricingMonth($id, date('n'));
                $this->session->set_flashdata('success', 'Invoice Item has been planned');
            } else {
                $this->session->set_flashdata('error', 'This action was blocked');
            }
            redirect('admin/client/detail/' . $_POST['userid']);
        } else {
            $this->session->set_flashdata('error', 'This action was blocked');
            redirect('admin/client');
        }
    }
    public function getServices()
    {
        $html= '';
        $userid=$_POST['userid'];
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.id,c.name,a.recurring,a.status from a_services a left join a_services_mobile b on b.serviceid=a.id left join a_products c on c.id=a.packageid where a.type='mobile' and  a.userid=?", array($userid));
        if ($q->num_rows()>0) {
            foreach ($q->result() as $row) {
                $html .='<option value="'.$row->id.'">'.$row->id.' '.$row->name. ' '.$this->data['setting']->currency.$row->recurring.'</option>';
            }
            echo json_encode(array('result' => 'success', 'html' => $html));
        } else {
            echo json_encode(array('result' => 'error'));
        }
    }
    public function edit()
    {

        //echo "has".print_r($hasinvoice);
        $this->load->library('magebo', array('companyid' => $this->companyid));

        if (isPost()) {
            $client_detail = $this->Admin_model->getClient($_POST['id']);

            $this->Teams_model->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " " . print_r($_SESSION, true));

            $_POST['mvno_id'] = strtoupper(trim($_POST['mvno_id']));

            if (!empty($_POST['password2'])) {
                $_POST["password"] = password_hash(trim($_POST["password2"]), PASSWORD_DEFAULT);
            }
            unset($_POST["password2"]);

            if ($_POST['vat_exempt'] == "on") {
                $_POST['vat_rate'] == "0";
            }

            if ($_POST['vat_rate'] == "0") {
                $_POST['vat_exempt'] = 1;
            }
            $_POST['email'] = strtolower(trim($_POST['email']));
            $client = $this->Admin_model->updateCustomer($_POST);
            $d = $this->Admin_model->getClient($_POST['id']);
            if ($this->data['setting']->mage_invoicing == "1") {
                $_POST['date_modified'] = date('Y-m-d H:i:s');
                $result = $this->magebo->updateClient($d->mageboid, $_POST, $this->session->cid);
            }

            if ($client['result']) {
                if ($this->data['setting']->mobile_platform == "TEUM") {
                    $this->load->library('pareteum', array('companyid' => $this->companyid));
                    $array = array(
                    "CustomerId" => $client_detail->teum_CustomerId,
                    "CustomerData" => array(
                        "ExternalCustomerId" => $_POST['mvno_id'],
                        "FirstName" => $_POST['firstname'],
                        "LastName" => $_POST['lastname'],
                        "CustomerDocumentType" => $_POST['id_type'],
                        "DocumentNumber" => $_POST['idcard_number'],
                        "Telephone" => $_POST['phonenumber'],
                        "Email" => $_POST['email'],
                        "LanguageName" => "eng",
                        "FiscalAddress" => array(
                            "Address" => $_POST['address1'],
                            "City" =>$_POST['city'],
                            "CountryId" => "76",
                            "HouseNo" => $_POST['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $_POST['postcode'],
                        ),
                        "CustomerAddress" => array(
                            "Address" => $_POST['address1'],
                            "City" => $_POST['city'],
                            "CountryId" => "76",
                            "HouseNo" => $_POST['housenumber'],
                            "State" => "Unknown",
                            "ZipCode" => $_POST['postcode']
                        ),
                        "Nationality" => "GB"
                    ),
                    "comments" => "Customer added via UnitedPortal V1 by" . $this->session->firstname . ' ' . $this->session->lastname,
                    "channel" => "UnitedPortal V1"
                    );

                    $teum = $this->pareteum->UpdateCustomer($array);
                    log_message('error', 'Request'. print_r($array, 1));
                    log_message('error', 'response'.print_r($teum, 1));
                }


                $text = "";
                foreach ($_POST as $key => $value) {
                    $text .= $key. ": ".$value."<br/>";
                }

                /*send_growl(
                       array(
                       'message' => $this->session->firstname . ' ' . $this->session->lastname.' Modified customer ' . $_POST['id'] . ' with data:' . $text,
                       'companyid' => $this->session->cid)
                   );
                   */
                logAdmin(array('companyid' => $this->companyid, 'userid' => $_POST['id'], 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Modify customer ' . $_POST['id'] . ' with data:' . implode(' ', $_POST)));
                $this->session->set_flashdata('success', lang('Customer information has been updated'));

                if ($this->data['setting']->create_arta_contact == 1) {
                    $this->load->library('artilium', array('companyid' => $this->companyid));
                    // sendlog('UpdateContact', array('agentid' => $agentid, 'post' => $_POST));
                    $cc = $this->Admin_model->getClient($_POST['id']);


                    log_message('error', 'Contact Compare '.$_POST['agentid']." ".$cc->agentid);

                    $this->artilium->UpdateContact($cc, $updateroot);
                }
            }
            echo json_encode($client);
            exit;
            //redirect('admin/client/detail/' . $this->uri->segment(4));
        }
        if (!isAllowed_Clientid($this->session->cid, $this->uri->segment(4))) {
            $this->session->set_flashdata('error', 'Client does not exists');

            redirect('admin/client');
        }
        $this->data['client'] = $this->Admin_model->getClient($this->uri->segment(4));
        $this->data['hasinvoice'] = $this->magebo->hasInvoice($this->data['client']->mageboid, $this->data['client']->vat);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'client_edit';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function edit_ciot()
    {

        //echo "has".print_r($hasinvoice);
        $this->load->library('magebo', array('companyid' => $this->companyid));

        if (isPost()) {
            $this->Teams_model->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " " . print_r($_SESSION, true));
            $d = $this->Admin_model->getClientCIOT($_POST['id']);
            $_POST['mvno_id'] = strtoupper(trim($_POST['mvno_id']));
            $_POST['email'] = strtolower(trim($_POST['email']));
            unset($_POST['password2']);
            $_POST['ciot'] = 1;
            mail('mail@simson.one', 'cit', print_r($_POST, true));
            $client = $this->Admin_model->updateCustomerCIOT($_POST);




            if ($client['result']) {
                logAdmin(array('companyid' => $this->companyid, 'userid' => $_POST['id'], 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Modify CIOT customer ' . $_POST['id'] . ' with data:' . implode(' ', $_POST)));
                $this->session->set_flashdata('success', lang('Customer CIOT information has been updated'));
            }
            echo json_encode($client);
            exit;
            //redirect('admin/client/detail/' . $this->uri->segment(4));
        }
        if (!isAllowed_Clientid($this->session->cid, $this->uri->segment(4))) {
            $this->session->set_flashdata('error', 'Client does not exists');

            redirect('admin/client');
        }
        $this->data['client'] = $this->Admin_model->getClientCIOT($this->uri->segment(4));
        if (!$this->data['client']) {
            $dd = $this->Admin_model->getClientRaw($this->uri->segment(4));
            $dd['ciot'] = 1;
            $this->db->insert('a_clients_ciot', $dd);
            $this->data['client'] = $this->Admin_model->getClientCIOT($this->uri->segment(4));
        }


        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'client_edit_ciot';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function login_as_client()
    {
        $id = $this->uri->segment(4);
        logAdmin(array('companyid' => $this->companyid, 'userid' => $id, 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Loggedin as customer ' . $id));
        $res =   $this->Admin_model->getClientSession($id);
        $res['islogged'] = true;
        $this->session->set_userdata('client', $res);
        // $_SESSION['client'] = $this->Admin_model->getClientSession($id);

        //$_SESSION['client']['islogged'] = true;


        redirect('client');
    }


    public function add_iban()
    {
        /*
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
         */
        if (isPost()) {
            //print_r($_POST);
            $id = $this->Admin_model->AddIban($_POST);
            if ($id) {
                logAdmin(array('companyid' => COMPANYID, 'userid' => $_POST['id'], 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'adding iban for clientid ' . $_POST['userid']));

                $this->session->set_flashdata('success', lang('IBAN has been inserted'));
            } else {
                $this->session->set_flashdata('error', lang('Error inserting IBAN'));
            }
            redirect('admin/client/detail/' . $_POST['userid'] . '/' . $id);
        } else {
        }
    }
    public function get_note()
    {
        $this->db = $this->load->database('default', true);
        $id = $this->uri->segment(4);
        $q = $this->db->query("select * from a_clients_notes where id=? and companyid=?", array($id, $this->companyid));

        echo json_encode(array('id' => $q->row()->id, 'description' => $q->row()->note,'sticky' => $q->row()->sticky, 'result' => true));
    }
    public function edit_note()
    {
        log_message('error', print_r($_POST, 1));
        $this->db = $this->load->database('default', true);
        $id = $_POST['id'];
        unset($_POST['id']);

        if (empty($_POST['sticky'])) {
            $_POST['sticky'] = "0";
        }
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->companyid);
        $this->db->update('a_clients_notes', $_POST);
        $this->session->set_flashdata('success', 'Note has been updated');
        redirect('admin/client/detail/' . $_POST['userid']);
    }
    public function delete_note()
    {
        $this->db = $this->load->database('default', true);
        // $this->db->query('delete from a_clients_notes where id=? and companyid=?', array($_POST['id'], $_POST['companyid']));
        $this->db->where('id', $_POST['id']);
        $this->db->where('companyid', $_POST['companyid']);
        $this->db->delete('a_clients_notes');
        $this->session->set_flashdata('success', 'Note has been deleted');
        echo json_encode(array('result' => $this->db->affected_rows()));
    }

    public function add_note()
    {
        if (isPost()) {
            $id = $this->Admin_model->AddNotes($_POST);
            if ($id) {
                logAdmin(array('companyid' => COMPANYID, 'userid' => $_POST['userid'], 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'adding note for clientid ' . $_POST['userid']));

                $this->session->set_flashdata('success', lang('Note has been inserted'));
            } else {
                $this->session->set_flashdata('error', lang('Error inserting note'));
            }
            redirect('admin/client/detail/' . $_POST['userid'] . '/' . $id);
        } else {
        }
    }
    public function getLastNotes()
    {
        $res = array('notes' => array(), 'tickets' => array());
        //$res['usage'] = $this->get_current_usage($this->companyid, $this->uri->segment(4));

        $notes = $this->db->query('select * from a_clients_notes where userid=? and sticky=? limit 5', array($this->uri->segment(4), '1'));
        $tickets = $this->db->query("select * from a_helpdesk_tickets where userid=?  and status in ('In-Progress','Awaiting-Reply','On-Hold','Open','Closed') order by id desc limit 5", array($this->uri->segment(4)));

        if ($notes->num_rows() > 0) {
            $res['notes'] = $notes->result();
        }
        if ($tickets->num_rows() > 0) {
            $res['tickets'] = $tickets->result();
        }

        $userid = $this->uri->segment(4);



        //$this->load->library('xml');

        $this->data['activemobiles'] = getMobileActiveCli($userid);
        $usage = "0.00";
        if (empty($this->data['activemobiles'])) {
            $usage = $this->data['setting']->currency."0.00";
        } else {
            foreach ($this->data['activemobiles'] as $mob) {
                $sns[] = $mob['sn'];
            }
            /*

             $f = array();
             $cdr = array();

             foreach ($this->data['activemobiles'] as $IN => $order) {

                 if ($order['domainstatus'] == 'Active') {
                     $this->data["service" . $IN] = $this->Admin_model->getService($order['id']);

                     $mobile = $this->data["service" . $IN]->details;

                     $f[] = array('domain' => $mobile->msisdn, 'sn' => $mobile->msisdn_sn, 'mobile' => $mobile);
                     $cdr = $this->artilium->get_cdr($this->data["service" . $IN]);

                     $total = $cdr['total'] + $total;
                     unset($cdr);

                 }

             }
              */
            if ($this->data['setting']->mobile_platform == "ARTA") {
                $this->load->library('artilium', array('companyid' => $this->companyid));
                if ($sns) {
                    $usage = $this->artilium->getOutofBundleusagePostpaid(array_filter($sns));
                }
            }
        }

        if ($usage>0) {
            $res['usage'] = str_replace('.', ',', number_format($usage, 2));
        } else {
            $res['usage'] = "0.00";
        }


        echo json_encode($res);
    }

    public function get_current_usage($companyid, $userid)
    {
        $this->load->library('artilium', array('companyid' => $companyid));
        $total = '0.00';
        $this->load->library('xml');
        $usage = array('0.00');
        $this->data['activemobiles'] = getMobileActive($companyid, $userid);
        //print_r($this->data);
        if (empty($this->data['activemobiles'])) {
            $this->data['showusage'] = false;
        } else {
            $f = array();
            $cdr = array();

            foreach ($this->data['activemobiles'] as $IN => $order) {
                if ($order['domainstatus'] == 'Active') {
                    $this->data["service" . $IN] = $this->Admin_model->getService($order['id']);

                    $mobile = $this->data["service" . $IN]->details;

                    $f[] = array('domain' => $mobile->msisdn, 'sn' => $mobile->msisdn_sn, 'mobile' => $mobile);
                    $cdr = $this->get_cdr(trim($mobile->msisdn_sn), $mobile->msisdn, $mobile);

                    $total = $cdr['total'] + $total;
                    unset($cdr);
                }
            }
        }

        return str_replace('.', ',', number_format($total, 2));
    }
    public function addMageboClient($id)
    {
        $this->load->library('magebo', array('companyid' => $this->companyid));
        $data = $this->Admin_model->getClient($id);
        unset($_POST['id']);
        if ($data->mageboid > 0) {
            return false;
            exit;
        }
        $client = (array) $data;
        unset($client['stats']);

        $client['mvno_id'] = strtoupper(trim($client['mvno_id']));

        if ($client['country'] == "NL") {
            $pcode = explode(' ', trim($client["postcode"]));
            if (count($pcode) != 2) {
                // $this->session->set_userdata('registration', $client);
                return false;

                exit;
            } else {
                if (strlen($pcode[0]) != 4) {
                    $this->session->set_userdata('registration', $client);
                    //echo json_encode(array('result' => 'error', 'message' => lang('Postcode must: NNNN XX formaat (102) ')));
                    return false;
                    exit;
                } elseif (strlen($pcode[1]) != 2) {
                    return false;

                    exit;
                }
            }
        } elseif ($client['country'] == "BE") {
            $pcode = explode(' ', 'BE ', trim($client["postcode"]));
        } else {
            $pcode = trim($client["postcode"]);
        }

        if (!empty($client['iban'])) {
            $bic = $this->Api_model->getBic(trim($client['iban']));
            if (!$bic->valid) {
            } else {
                $client['bic'] = $bic->bankData->bic;
            }
        }

        $client['date_created'] = date('Y-m-d');
        $client['email'] = strtolower(trim($client['email']));

        $magebo = $this->magebo->AddClient($client);

        if ($magebo->result == "success") {
            $this->db->query("update a_clients set mageboid=? where id=?", array($magebo->iAddressNbr, $id));
            return $magebo->iAddressNbr;
        } else {
            return false;
        }
    }

    public function get_cdr($sn, $msisdn, $mobile)
    {
        $total = 0;
        $bytes = array();
        // $this->load->model('Admin_model');

        $cdr = array();

        //print_r($this->data['mobile']);
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
        //echo $msisdn . "<br />";
        //print_r($mobile);
        //$result = $this->artilium->GetCDRList($sn);
        $params = array('companyid' => $this->companyid,
            'action' => 'GetCDRList',
            'type' => 'POST PAID',
            'SN' => trim($sn),
            'From' => date('Y-m-01\T00:00:00+02:00'),
            'Till' => date('Y-m-d\TH:i:s'),
            'PageIndex' => 0,
            'PageSize' => 2500,
            'SortBy' => 0,
            'SortOrder' => 0);

        $sms = array();
        $voice = array();
        $bytes = array();

        $result = $this->Admin_model->artiliumPost($params);

        foreach ($result->ListInfo as $row) {
            if ($row->Cause != "1000") {
                if ($row->TypeCallId != 5) {
                    if ($row->MaskDestination != "72436") {
                        if (!empty($row->DestinationCountry)) {
                            $country = $row->DestinationCountry;
                        } else {
                            $country = "";
                        }

                        if ($row->TrafficTypeId == 2) {
                            $total = $total + includevat('21', $row->CurNumUnitsUsed);
                        } else {
                            if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {
                            } else {
                                if ($row->TypeCallId != "3") {
                                    $total = $total + includevat('21', $row->CurNumUnitsUsed);
                                }
                            }
                        }
                    }
                }
            }

            if ($row->TypeCallId != 5) {
                if (!in_array($row->TrafficTypeId, array(2, 3))) {
                    if (strlen($row->MaskDestination) > 9) {
                        if ($row->TrafficTypeId) {
                            if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {
                            } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {
                            } else {
                                if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
                                    $voice[] = $row->DurationConnection;
                                } elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
                                    $sms[] = $row->DurationConnection;
                                }
                            }
                        }
                    }
                } elseif ($row->TrafficTypeId == 2) {
                    $bytes[] = $row->DurationConnection;
                }
            }
        }

        if (!$sms) {
            $smsi = '0';
        } else {
            $smsi = array_sum($sms);
        }
        return array('data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => $smsi, 'total' => $total);

        //return array('cdr' => $cdr, 'data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => array_sum($sms));
    }

    public function generate_reminder()
    {
        $this->load->library('spdf');
        $id = $this->uri->segment(4);
        $client = $this->Admin_model->getClient($id);
        $this->load->library('magebo', array('companyid' => $this->session->cid));
        $invoices = $this->magebo->getDueInvoices($client->mageboid);
        if ($this->companyid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
        } else {
        }
        if (!$invoices) {
            echo "No due Invoices";
        } else {
            $html = getPdfTemplate('Reminder', $client->companyid, $client->language);
            $body = $html->body;
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$clientid}', $client->mvno_id, $body);
            $body = str_replace('{$address1}', $client->address1, $body);
            $body = str_replace('{$postcode}', $client->postcode, $body);
            $body = str_replace('{$city}', $client->city, $body);
            $body = str_replace('{$country}', getCountryNameLang($client->country, $client->language), $body);
            $body = str_replace('{$language}', $client->language, $body);
            $body = str_replace('{$phonenumber}', $client->phonenumber, $body);
            $body = str_replace('{$Today}', date('d-m-Y'), $body);
            $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Simson Asuni');
            $pdf->SetTitle('Reminder Client ' . $id);
            //$pdf->setInvoicenumber($id);
            $pdf->setPrintFooter(false);
            $pdf->SetSubject('Reminder Invoice ' . $id);
            $pdf->SetKeywords('TCPDF, Invoice, Cdr, ' . $id);
            $pdf->AddPage('P', 'A4');
            $pdf->Image($brand->brand_logo, 10, 10, 60);
            //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
            $pdf->ln(10);
            $pdf->SetFont($html->font, '', $html->font_size);
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $pdf->setFoot($brand->brand_footer);
            $pdf->setFooterFont('droidsans');
            // $pdf->setPrintFooter(false);
            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "15");

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            if ($client->language == "dutch") {
                $table = '<p style="margin-left: 21pt;">
 <table border="1" cellpadding="2" cellspacing="1" style="margin-left: 21pt;">
 <thead>
 <tr bgcolor="#111" color="#fff" >

     <th>' . lang('Factuurnr.') . '</th>
     <th>' . lang('Datum') . '</th>
     <th>' . lang('Vervaldatum') . '</th>
     <th>' . lang('Bedrag') . '</th>
 </tr>
 </thead>
     <tbody>';
            } else {
                $table = '<p style="margin-left: 21pt;">
 <table border="1" cellpadding="2" cellspacing="1" style="margin-left: 21pt;">
 <thead>
 <tr bgcolor="#111" color="#fff" >

     <th>' . lang('Invoice.') . '</th>
     <th>' . lang('Date') . '</th>
     <th>' . lang('Duedate') . '</th>
     <th>' . lang('Amount') . '</th>
 </tr>
 </thead>
     <tbody>';
            }
            // ---------------------------------------------------------

            foreach ($invoices as $invoice) {
                $table .= ' <tr>

        <td> ' . $invoice->iInvoiceNbr . ' </td>
        <td> ' . date("d-m-Y", strtotime(str_replace(' 00:00:00.000', '', $invoice->dInvoiceDate))) . ' </td>
        <td> ' . date("d-m-Y", strtotime(str_replace(' 00:00:00.000', '', $invoice->dInvoiceDueDate))) . '</td>
        <td> '.$this->data['setting']->currency . number_format($invoice->mInvoiceAmount, 2) . '</td>
    </tr>';
            }

            $table .= '
     </tbody>
 </table></p>
';

            if ($html) {
                $body = str_replace('{$table}', $table, $body);

                $pdf->writeHTML($body, true, false, true, false, 'L');

                $pdf->Output($this->data['setting']->DOC_PATH . $client->companyid . '/reminders/' . $id . '.pdf', 'I');
            } else {
                echo "Template not found";
            }
        }
    }
    public function sepa_amend_cli($data)
    {
        $this->load->library('magebo', array('companyid' => $this->companyid));
        if (!empty($data['iban']) && !empty($data['userid'])) {
            $client = $this->Admin_model->getClient($data['userid']);
            $bic = $this->Api_model->getBic(trim($data['iban']));
            if (!$bic->valid) {
                $this->session->set_flashdata('error', 'Your IBAN is invalid');
            } else {
                $data['bic'] = $bic->bankData->bic;
                if ($client) {
                    if ($data['type'] == "NONE") {
                        $this->session->set_flashdata('error', 'You need to choose AMEND or NEW');
                    } else {
                        if ($data['type'] == "NEW") {
                            $this->magebo->insertIban($client->mageboid, array('iban' => strtoupper(str_replace(' ', '', trim($data['iban']))), 'bic' => trim($data['bic'])));
                            $this->magebo->addMageboSEPA(strtoupper(str_replace(' ', '', trim($data['iban']))), trim($data['bic']), $client->mageboid);
                        } else {
                            $this->magebo->amendMageboSEPA(strtoupper(str_replace(' ', '', trim($data['iban']))), trim($data['bic']), $client->mageboid);
                        }
                        $this->Admin_model->update_sepa($data);
                        logAdmin(array('companyid' => $this->companyid, 'userid' => $client->id, 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Execute Sepa Amendement for  ' . $client->mageboid . ' IBAN: ' . $data['iban']));
                    }
                    $this->session->set_flashdata('success', 'Sepa has been amended');
                } else {
                    $this->session->set_flashdata('error', 'Billing Id cant be found');
                }
            }
        } else {
            $this->session->set_flashdata('error', 'Please check your Sepa information, this customer set to directdebit but we could not found any Bank information');
        }

        return true;
    }

    public function sepa_amend()
    {
        $this->load->library('magebo', array('companyid' => $this->companyid));
        if (!empty($_POST['iban']) && !empty($_POST['userid'])) {
            $client = $this->Admin_model->getClient($_POST['userid']);
            $bic = $this->Api_model->getBic(trim($_POST['iban']));
            if (iban_block($this->companyid, trim($_POST['iban']))) {
                $this->session->set_flashdata('error', 'You may not use this IBAN');
                redirect('admin/client/detail/' . $_POST["userid"]);
                exit;
            } else {
                if (!$bic->valid) {
                    if (!empty($_POST['bic'])) {
                    } else {
                        $this->session->set_flashdata('error', 'We can not detect BIC please provide it, error code: 012');
                        redirect('admin/client/detail/' . $_POST["userid"]);
                        exit;
                    }
                } else {
                    if (!empty($_POST['bic'])) {
                    } else {
                        if (!empty($bic->bankData->bic)) {
                            $_POST['bic'] = $bic->bankData->bic;
                        } else {
                            $bc = $this->Api_model->getBicTry(trim($_POST['iban']));
                            if (!empty($bc->result->data->swift_code)) {
                                $_POST['bic'] = $bc->result->data->swift_code;
                            }
                        }
                    }

                    if (empty($_POST['bic'])) {
                        $this->session->set_flashdata('error', 'We can not detect BIC please provide it, error code: 011');
                        redirect('admin/client/detail/' . $_POST["userid"]);
                        exit;
                    }

                    if ($client) {
                        if ($_POST['type'] == "NONE") {
                            $this->session->set_flashdata('error', 'You need to choose AMEND or NEW');
                        } else {
                            if ($_POST['type'] == "NEW") {
                                $this->magebo->deleteIban($client->mageboid);
                                $this->magebo->insertIban($client->mageboid, array('iban' => strtoupper(str_replace(' ', '', trim($_POST['iban']))), 'bic' => trim($_POST['bic'])));
                                $this->magebo->addMageboSEPA(strtoupper(str_replace(' ', '', trim($_POST['iban']))), trim($_POST['bic']), $client->mageboid, $_POST['mandateid']);
                            } else {
                                $this->magebo->amendMageboSEPA(strtoupper(str_replace(' ', '', trim($_POST['iban']))), trim($_POST['bic']), $client->mageboid);
                            }
                            $this->Admin_model->update_sepa($_POST);
                            logAdmin(array('companyid' => $this->companyid, 'userid' => $client->id, 'user' => $this->session->firstname . ' ' . $this->session->lastname, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Execute Sepa Amendement for  ' . $client->mageboid . ' IBAN: ' . $_POST['iban']));
                        }
                        $this->session->set_flashdata('success', 'Sepa has been amended');
                    } else {
                        $this->session->set_flashdata('error', 'Billing Id cant be found');
                    }
                }
            }
        } else {
            $this->session->set_flashdata('success', 'Please check your data');
        }

        redirect('admin/client/detail/' . $_POST["userid"]);
    }

    public function export_clients()
    {
        $check = $this->db->query("select * from a_request_export where date like ? and companyid =? and status != 'failed' order by id desc", array(date('Y-m-d').'%', $this->companyid));
        if ($check->num_rows() <= 0) {
            $this->db->insert('a_request_export', array('type_export' => 'client', 'companyid' => $this->companyid, 'recipient' => $this->session->email));
            $id = $this->db->insert_id();
            if ($id>0) {
                $this->session->set_flashdata('success', 'your request for client export has been accepted with queue number: #'.$id.' You will receive the file in your mailbox: '.$this->session->email.' once ready, it could takes to 30 minutes depend the size of your customer database');
            } else {
                $this->session->set_flashdata('error', 'your request failed, please contact our administrator');
            }
        } else {
            if ($check->row()->status =="created") {
                $this->session->set_flashdata('error', 'There is already request created and will be sent to '.$check->row()->recipient.' this export function only allowed once a day for server performance');
            } elseif ($check->row()->status =="processing") {
                $this->session->set_flashdata('error', 'There is  request in progress and will be sent to '.$check->row()->recipient.' this export function only allowed once a day due to server performance');
            } else {
                $this->session->set_flashdata('error', 'Please contact '.$check->row()->recipient.' this person has requested export today, therefore this request is denied');
            }
        }
        redirect('admin/client');
    }

    public function gen_anonymous()
    {
        $this->load->helper('string');
        $data = array(
                'companyid' => $this->companyid,
                'uuid' => gen_uuid(),
                'mvno_id' => rand(100000000, 9999999999),
                'email' => random_string('alnum', 10).'@'.random_string('alnum', 10).'.com',
                'phonenumber' => rand(1000000000, 9999999999),
                'vat' => random_string('alnum', 10),
                'companyname' => random_string('alnum', 10),
                'initial' => 'AN',
                'salutation' => 'Mr.',
                'firstname' => 'Anon_'.random_string('alnum', 10),
                'lastname' => 'Anon_'.random_string('alnum', 10),
                'address1' => random_string('alnum', 10),
                'housenumber' => rand(10, 120),
                'postcode' => rand(1000, 9999),
                'city' => random_string('alnum', 10),
                'country' => $this->data['setting']->country_base,
                'language' => 'english',
                'paymentmethod' => 'banktransfer',
                'gender' => 'male',
                'sso_id' => '0',
                'nationalnr' => random_string('alnum', 10),
                'date_created' => date('Y-m-d'),
                'invoice_email' => 'yes',
                'date_birth' => '1970-01-01',
                'vat_exempt' => 0,
                'idcard_number' => rand(10000000000, 99999999999));

        echo json_encode($data);
    }
}
