<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CalController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }

        $this->table = 'calendar';
        $this->load->model('Globalmodel', 'modeldb');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }

}

class Calendar extends CalController
{

    public function index()
    {
        $data_calendar = $this->modeldb->get_list($this->table, $this->session->id);
        $calendar = array();
        foreach ($data_calendar as $key => $val) {
            $calendar[] = array(
                'id' => intval($val->id),
                'title' => $val->title,
                'description' => trim($val->description),
                'start' => date_format(date_create($val->start_date), "Y-m-d H:i:s"),
                'end' => date_format(date_create($val->end_date), "Y-m-d H:i:s"),
                'color' => $val->color,
            );
        }

        $this->data['get_data'] = json_encode($calendar);

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'calendar';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);

        //$this->load->view('calendar', $data);
    }
    public function save()
    {
        $response = array();
        $this->form_validation->set_rules('title', 'Title cant be empty ', 'required');
        if ($this->form_validation->run() == true) {
            $param = $this->input->post();
            $calendar_id = $param['calendar_id'];
            $param['adminid'] = $this->session->id;
            unset($param['calendar_id']);
            if ($calendar_id == 0) {
                $param['create_at'] = date('Y-m-d H:i:s');
                $insert = $this->modeldb->insert($this->table, $param);
                if ($insert > 0) {
                    $response['status'] = true;
                    $response['notif'] = 'Success add calendar';
                    $response['id'] = $insert;
                } else {
                    $response['status'] = false;
                    $response['notif'] = 'Server wrong, please save again';
                }
            } else {
                $where = ['id' => $calendar_id];
                $param['modified_at'] = date('Y-m-d H:i:s');
                $update = $this->modeldb->update($this->table, $param, $where);
                if ($update > 0) {
                    $response['status'] = true;
                    $response['notif'] = 'Success add calendar';
                    $response['id'] = $calendar_id;
                } else {
                    $response['status'] = false;
                    $response['notif'] = 'Server wrong, please save again';
                }
            }
        } else {
            $response['status'] = false;
            $response['notif'] = validation_errors();
        }
        echo json_encode($response);
    }
    public function delete()
    {
        $response = array();
        $calendar_id = $this->input->post('id');
        if (!empty($calendar_id)) {
            $where = ['id' => $calendar_id];
            $delete = $this->modeldb->delete($this->table, $where);
            if ($delete > 0) {
                $response['status'] = true;
                $response['notif'] = 'Success delete calendar';
            } else {
                $response['status'] = false;
                $response['notif'] = 'Server wrong, please save again';
            }
        } else {
            $response['status'] = false;
            $response['notif'] = 'Data not found';
        }
        echo json_encode($response);
    }
}
