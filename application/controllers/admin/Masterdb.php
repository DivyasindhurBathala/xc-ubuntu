<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masterdb extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->library('grocery_CRUD');
		if(!$this->session->master){
			die('Access Denied');
		}
	}

	public function _db_output($output = null)
	{
	
		
		$this->load->view('themes/clear/admins/crud.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_db_output($output);
	}

	public function index()
	{
		$this->_db_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function clients()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('a_clients');
			$crud->set_subject('Clients');
			$crud->required_fields('status');
			//$crud->columns('firstname','lastname','email','postcode','country','city','status');
			$crud->order_by('id','desc');
			$output = $crud->render();

			$this->_db_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function services()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('a_services');
			//$crud->set_relation('userid','a_clients','id');
			//$crud->display_as('userid','Customerid');
			$crud->set_subject('Services');
			$crud->order_by('id','desc');
			//$crud->required_fields('lastName');

			//$crud->set_field_upload('file_url','assets/uploads/files');

			$output = $crud->render();

			$this->_db_output($output);
	}

	public function service_mobile()
	{
			$crud = new grocery_CRUD();
			$crud->set_theme('flexigrid');
			$crud->set_table('a_services_mobile');
			$crud->order_by('id','desc');
			$crud->set_subject('Service Mobile');
			//$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

			$output = $crud->render();

			$this->_db_output($output);
	}

	public function products()
	{
			$crud = new grocery_CRUD();

			
			$crud->set_table('a_products');
			$crud->set_subject('Products');
			$output = $crud->render();
			$crud->order_by('id','desc');
			$this->_db_output($output);
	}

	public function invoices()
	{
			$crud = new grocery_CRUD();

			$crud->columns('iInvoiceNbr','dInvoiceDate','dInvoiceDueDate','mInvoiceAmount','iInvoiceStatus','mageboid');
			$crud->set_table('a_tblInvoice');
			$crud->set_relation('iAddressNbr','a_clients','mageboid');
			$crud->set_subject('Invoices');
			$crud->where('iCompanyNbr',$this->session->cid);
			$output = $crud->render();
			$crud->order_by('id','desc');
			$this->_db_output($output);
	}

	public function configuration()
	{
			$crud = new grocery_CRUD();

			$crud->set_table('a_configuration');
			$crud->set_subject('Global Config');
			$crud->order_by('id','desc');
			$output = $crud->render();

			$this->_db_output($output);
	}

	public function valueToEuro($value, $row)
	{
		return $value.' &euro;';
	}

	public function a_event_status()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('a_event_status');
		$crud->order_by('id','desc');
		$output = $crud->render();

		$this->_db_output($output);
	}

	public function api_logs()
	{
		try{
			$crud = new grocery_CRUD();

		
		$crud->set_table('a_apilog');
		$crud->order_by('id','desc');
		$output = $crud->render();

		$this->_db_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function logs()
	{
		try{
			$crud = new grocery_CRUD();

		
		$crud->set_table('a_logs');
		$crud->order_by('id','desc');
		$output = $crud->render();

		$this->_db_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function a_email_templates()
	{
		try{
			$crud = new grocery_CRUD();

		
		$crud->set_table('a_email_templates');
		$crud->order_by('id','desc');
		$output = $crud->render();

		$this->_db_output($output);

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	public function a_sepa_trx_code()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('a_sepa_trx_codes');
			//$crud->set_relation('userid','a_clients','id');
			//$crud->display_as('userid','Customerid');
			$crud->set_subject('Transaction Code');
			$crud->order_by('id','desc');
			//$crud->required_fields('lastName');

			//$crud->set_field_upload('file_url','assets/uploads/files');

			$output = $crud->render();

			$this->_db_output($output);
	}

	public function a_sepa_items()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('a_sepa_items');
			//$crud->set_relation('userid','a_clients','id');
			//$crud->display_as('userid','Customerid');
			$crud->set_subject('Transaction Items');
			$crud->order_by('id','desc');
			//$crud->required_fields('lastName');

			//$crud->set_field_upload('file_url','assets/uploads/files');

			$output = $crud->render();

			$this->_db_output($output);
	}

	function multigrids()
	{
		$this->config->load('grocery_crud');
		$this->config->set_item('grocery_crud_dialog_forms',true);
		$this->config->set_item('grocery_crud_default_per_page',10);

		$output1 = $this->offices_management2();

		$output2 = $this->employees_management2();

		$output3 = $this->customers_management2();

		$js_files = $output1->js_files + $output2->js_files + $output3->js_files;
		$css_files = $output1->css_files + $output2->css_files + $output3->css_files;
		$output = "<h1>List 1</h1>".$output1->output."<h1>List 2</h1>".$output2->output."<h1>List 3</h1>".$output3->output;

		$this->_db_output((object)array(
				'js_files' => $js_files,
				'css_files' => $css_files,
				'output'	=> $output
		));
	}

	public function offices_management2()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('offices');
		$crud->set_subject('Office');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_db_output($output);
		} else {
			return $output;
		}
	}

	public function employees_management2()
	{
		$crud = new grocery_CRUD();

		$crud->set_theme('datatables');
		$crud->set_table('employees');
		$crud->set_relation('officeCode','offices','city');
		$crud->display_as('officeCode','Office City');
		$crud->set_subject('Employee');

		$crud->required_fields('lastName');

		$crud->set_field_upload('file_url','assets/uploads/files');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_db_output($output);
		} else {
			return $output;
		}
	}

	public function customers_management2()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('customers');
		$crud->columns('customerName','contactLastName','phone','city','country','salesRepEmployeeNumber','creditLimit');
		$crud->display_as('salesRepEmployeeNumber','from Employeer')
			 ->display_as('customerName','Name')
			 ->display_as('contactLastName','Last Name');
		$crud->set_subject('Customer');
		$crud->set_relation('salesRepEmployeeNumber','employees','lastName');

		$crud->set_crud_url_path(site_url(strtolower(__CLASS__."/".__FUNCTION__)),site_url(strtolower(__CLASS__."/multigrids")));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_db_output($output);
		} else {
			return $output;
		}
	}

}
