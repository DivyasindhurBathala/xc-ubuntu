<?php
defined('BASEPATH') or exit('No direct script access allowed');
class AgentController extends CI_Controller
{
    protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->companyid = get_companyidby_url(base_url());
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }
        if (file_exists(APPPATH.'language/'.$this->session->language.'/'.$this->companyid.'_admin.php')) {
            log_message('error', lang('Using Custom language files'));
            $this->lang->load($this->companyid.'_admin');
        } else {
            $this->lang->load('admin');
        }
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Agent extends AgentController
{
    public function __construct()
    {
        parent::__construct();
        /*
        //Generate Permissions
        $classes = get_class_methods($this);
        if ($this->session->email == "simson.parlindungan@united-telecom.be") {
            foreach ($classes as $class) {
                log_message('error', $class);
                if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
                    $this->db->insert('a_admin_permissions', array('folder' => 'admin', 'controller' => 'agent', 'method' => $class));
                }
            }
        }
        */



        $this->load->model('Agent_model');
        $this->db = $this->load->database('default', true);
        if (!empty($this->session->profiler)) {
            if ($this->session->profiler) {
                $this->output->enable_profiler(true);
                error_reporting(E_ALL);
                ini_set('display_errors', 1);
            }
        }
    }

    public function index()
    {
        $this->load->helper('string');
        $this->data['title']        = lang("Agent List Dashboard");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'agent_'.strtolower($this->data['setting']->mobile_platform);
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function detail()
    {
        $this->data['agent'] = $this->Agent_model->getAgent($this->uri->segment(4));
        if (empty($this->data['agent'])) {
            $this->session->set_flashdata('error', lang('You do not have access for this agent'));
            redirect('admin/agent');
        }
        $this->data['dtt']          = 'agents.js?version=' . time();
        $this->data['title']        = lang("Agent Summary");
        $this->data['perms']        = $this->Agent_model->getPermission($this->uri->segment(4));

        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'agent_detail';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function change_permission()
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('companyid', $this->companyid);
        $this->db->where('agentid', $_POST['agentid']);
        $this->db->update('a_clients_agents_perms', array($_POST['permission'] => $_POST['val']));
        echo json_encode(array('result' => true));
    }
    public function getStatistic()
    {
        $today = date('Y-m-d');
        $thisweek = get_dates_of_thisweek();
        $thismonth = get_dates_of_thismonth();

        $result['counter'] = array(
            'subscription' => array('year' => 0, 'month' =>0, 'week' => 0, 'today' => 0),
            'topup' => array('year' => 0, 'month' =>0, 'week' => 0, 'today' => 0),
            'bundle' => array('year' => 0, 'month' =>0, 'week' => 0, 'today' => 0),

        );
        $result['amount'] = array(
            'topup' => array('year' => '0.00', 'month' =>'0.00', 'week' => '0.00', 'today' => '0.00'),
            'bundle' => array('year' => '0.00', 'month' =>'0.00', 'week' => '0.00', 'today' => '0.00'),
        );
        $q = $this->db->query("select a.* from a_services a left join a_clients b on a.userid=b.id where a.companyid=? and b.agentid=? and a.status=?", array($this->companyid, $this->uri->segment(4), 'Active'));
        $r = $this->db->query("select a.* from a_topups a  where a.companyid=? and a.agentid=?", array($this->companyid, $this->uri->segment(4)));

        if ($q->num_rows()>0) {
            foreach ($q->result_array() as $row) {
                if (substr($row['date_contract_formated'], 0, 4) == date('Y')) {
                    $result['counter']['subscription']['year']  = $result['counter']['subscription']['year'] +1;
                }
                if (in_array($row['date_contract_formated'], $thisweek)) {
                    $result['counter']['subscription']['week']  = $result['counter']['subscription']['week'] +1;
                }

                if (in_array($row['date_contract_formated'], $thismonth)) {
                    $result['counter']['subscription']['month']  = $result['counter']['subscription']['month'] +1;
                }
                if ($row['date_contract_formated'] == date('Y-m-d')) {
                    $result['counter']['subscription']['today']  = $result['counter']['subscription']['today'] +1;
                }
            }
        }


        // echo json_encode($r->result());
        //exit;

        if ($r->num_rows()>0) {
            foreach ($r->result_array() as $g) {
                if ($g["income_type"]  == "bundle") {
                    if (substr($g['date'], 0, 4) == date('Y')) {
                        $result['amount']['bundle']['year']  = number_format($result['amount']['bundle']['year'] + $g['amount'], 2);
                        $result['counter']['bundle']['year']  = $result['counter']['bundle']['year'] +1;
                    }
                    if (in_array(substr($g['date'], 0, 10), $thisweek)) {
                        $result['amount']['bundle']['week']  = number_format($result['amount']['bundle']['week'] + $g['amount'], 2);
                        $result['counter']['bundle']['week']  = $result['counter']['bundle']['week'] +1;
                    }

                    if (in_array(substr($g['date'], 0, 10), $thismonth)) {
                        $result['amount']['bundle']['month']  = number_format($result['amount']['bundle']['month'] + $g['amount'], 2);
                        $result['counter']['bundle']['month']  = $result['counter']['bundle']['month'] +1;
                    }
                    if (substr($g['date'], 0, 10) == date('Y-m-d')) {
                        $result['amount']['bundle']['today']  = number_format($result['amount']['bundle']['today'] + $g['amount'], 2);
                        $result['counter']['bundle']['today']  = $result['counter']['bundle']['today'] +1;
                    }
                }

                if ($g["income_type"]  == "topup") {
                    if (substr($g['date'], 0, 4) == date('Y')) {
                        $result['amount']['topup']['year']  = number_format($result['amount']['topup']['year'] + $g['amount'], 2);
                        $result['counter']['topup']['year']  = $result['counter']['topup']['year'] +1;
                    }
                    if (in_array(substr($g['date'], 0, 10), $thisweek)) {
                        $result['amount']['topup']['week']  = number_format($result['amount']['topup']['week'] + $g['amount'], 2);
                        $result['counter']['topup']['week']  = $result['counter']['topup']['week'] +1;
                    }

                    if (in_array(substr($g['date'], 0, 10), $thismonth)) {
                        $result['amount']['topup']['month']  =number_format($result['amount']['topup']['month'] + $g['amount'], 2);
                        $result['counter']['topup']['month']  = $result['counter']['topup']['month'] +1;
                    }
                    if (substr($g['date'], 0, 10) == date('Y-m-d')) {
                        $result['amount']['topup']['today']  = number_format($result['amount']['topup']['today'] + $g['amount'], 2);
                        $result['counter']['topup']['today']  = $result['counter']['topup']['today'] +1;
                    }
                }
            }
        }




        echo json_encode($result);
    }
    public function add()
    {
        $password = $_POST['password'];
        $_POST['email'] = strtolower(trim($_POST['email']));
        $_POST['password'] = password_hash(trim($_POST['password']), PASSWORD_DEFAULT);

        if ($this->data['setting']->create_arta_contact == 1) {
            $agentid = $this->session->reseller['id'];
            $d = $_POST;
            $d['companyname'] = 'Partner - '.$_POST['agent'];
            $this->load->library('artilium', array('companyid' => $this->session->cid));
            sendlog('CreateContact', array('agentid' => -1, 'post' => $d));
            $cont = $this->artilium->CreateContact($d, -1);
            if ($cont) {
                $_POST['ContactType2Id'] = $cont;
            }
        }
        $_POST['uuid'] = gen_uuid();
        $_POST['comission_value'] = str_replace(',', '.', $_POST['comission_value']);
        $_POST['min_topup'] = str_replace(',', '.', $_POST['min_topup']);
        $_POST['max_topup'] = str_replace(',', '.', $_POST['max_topup']);
        $_POST['reseller_balance'] = str_replace(',', '.', $_POST['reseller_balance']);
        //sendlog('agentadd',$_POST);
        $id = $this->Agent_model->addAgent($_POST);

        if ($id) {
            $this->Agent_model->addPermission($_POST['reseller_type'], array('agentid' => $id, 'companyid' => $this->session->cid));
            $headers = "From: ".$setting->smtp_sender . "\r\n" .
            "BCC: mail@simson.one";
            mail(
                trim(strtolower($_POST['email'])),
                "Your ".$setting->companyname." Reseller Account information",
                "Hello\n\nWe would glad to inform you that your Reseller/Agent account has been created,\nPlease find below credential to your Panel:\n\nUserName: ".$_POST['email']."\nPassword: ".$password."\nUrl: ".base_url()."reseller\n\nKind Regard\nMvno Team",
                $headers
            );
            $this->session->set_flashdata('success', lang('New Agent has been added and the credential has been sent to the email address provided during registration'));
        } else {
            $this->session->set_flashdata('error', lang('Error while adding new agent'));
        }
        redirect('admin/agent');
    }
    public function topup()
    {
        $agent = $this->Agent_model->getAgent($_POST['agentid']);
        if ($agent) {
            $this->db->where('id', $_POST['agentid']);
            $this->db->update('a_clients_agents', array('reseller_balance' => $agent->reseller_balance+str_replace(',', '.', $_POST['Amount'])));

            if ($this->db->affected_rows()>0) {
                $this->db->insert('a_clients_agents_topups', array('date' => date('Y-m-d H:i:s'),
                'amount' => $_POST['Amount'],
                'descriptions' => $_POST['description'],
                'companyid' => $this->companyid,
                'agentid' => $_POST['agentid'],
                'executor' => $this->session->firstname.' '.$this->session->lastname,
                'channels' => 'AdminPortal'));

                if ($this->db->insert_id()>0) {
                    $this->session->set_flashdata('success', 'Balance has been updated');
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => '0',
                        'userid' =>  '0',
                        'user' => $this->session->firstname.' '.$this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Agent balance has been added amount :'.$_POST['amount']
                    ));
                }
            }
        } else {
            $this->session->set_flashdata('error', 'Agent not found');
        }



        redirect('admin/agent/detail/'.$_POST['agentid']);
    }
    public function update()
    {
        log_message('error', print_r($_POST, 1));
        if (!empty($_POST['password'])) {
            $_POST['password'] = password_hash(trim($_POST['password']), PASSWORD_DEFAULT);
        } else {
            unset($_POST['password']);
        }
        if ($_POST['reseller_type'] == "Prepaid") {
            unset($_POST["comission_value"]);
            $_POST['comission_value'] = $_POST["discount"];
            unset($_POST["discount"]);
        } else {
            unset($_POST["discount"]);
        }


        log_message('error', print_r($_POST, 1));

        $_POST['min_topup'] = str_replace(',', '.', $_POST['min_topup']);
        $_POST['max_topup'] = str_replace(',', '.', $_POST['max_topup']);


        $id = $this->Agent_model->updateAgent($_POST);
        if ($id) {
            $this->session->set_flashdata('success', lang('Agent Information has been updated'));
        } else {
            $this->session->set_flashdata('error', lang('Error while Updating agent, or nothing to be updated'));
        }
        redirect('admin/agent/detail/'.$_POST['id']);
    }

    public function delete()
    {
        $id = $this->Agent_model->deleteAgent($_POST);

        if ($id['result'] == "success") {
            $this->session->set_flashdata('success', lang('Agent Information has been deleted'));
        } else {
            $this->session->set_flashdata('error', lang($id['message']));
        }
        redirect('admin/agent');
    }
    public function export_agent_subscription()
    {
        $this->load->library('xlswriter');
        $fileName = 'Subscription-Summary_by_Date_' .date('Y-m-d') . '.xlsx';
        $header = array(
                'SubscriptionID' => 'integer',
                'Status' => 'string',
                'ArtiliumID' => 'integer',
                'MvnoID' => 'string',
                'Recurring' => 'string',
                'DateContract MM-DD-YYYY' => 'string',
                'DateCreated' => 'string',
                'Companyname' => 'string',
                'ProductName' => 'string',
                'Identifiers' => 'string',
                'OrderStatus' => 'string',
                'PackageId' => 'integer',

            );
        $companyid = $this->session->cid;
        $agentid = $this->uri->segment(4);
        $this->db = $this->load->database('default', true);
        $query = "select a.id,a.status,a.userid,c.mvno_id,a.recurring as recurring,date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
	case when a.type  = 'mobile' THEN d.msisdn
	when a.type = 'xdsl' then e.circuitid
	when a.type = 'voip' then f.cli
	else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
	when a.type = 'xdsl' then e.status
	when a.type = 'voip' then f.status
	else 'Unknown' end as orderstatus,a.packageid
 from a_services a
 left join a_products b on b.id=a.packageid
 left join a_clients c on c.id=a.userid
 left join a_services_mobile d on d.serviceid=a.id
 left join a_services_xdsl e on e.serviceid=a.id
 left join a_services_voip f on f.serviceid=a.id
 where a.status in ('Pending','Active')
 and a.companyid = ".$companyid."
 and c.agentid = '$agentid'
 group by a.id";

        $q = $this->db->query($query);

        if ($q->num_rows() > 0) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->xlswriter->writeSheet($q->result_array(), 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Subscription Export');
            $this->xlswriter->setCompany('United');
            echo $this->xlswriter->writeToString();

            $this->session->set_flashdata('success', lang('Downloaded as requested'));
        } else {
            $this->session->set_flashdata('error', lang('No data found'));
        }

        redirect('reseller');
    }
    public function login_as_reseller()
    {
        $agent = (array) $this->Agent_model->getAgent($this->uri->segment(4));
        $this->session->unset_userdata('client');

        if (empty($agent)) {
            $this->session->set_flashdata('error', lang('You do not have access for this agent'));
            redirect('admin/agent');
        } else {
            $agent['islogged'] = true;
        }

        $this->session->set_userdata('reseller', $agent);

        redirect('reseller/dashboard');
    }


    public function assign_simcard_by_scanner()
    {
        if (isPost()) {
            if (simcardExist($_POST['msisdn_sim'])) {
                echo json_encode(array('result' => false, 'message' => lang('Error: Barcode already assigned to another reseller')));
                exit;
            }

            $this->db->insert('a_reseller_simcard', array(
                'simcard' => trim($_POST['msisdn_sim']),
                    'resellerid' => $_POST['agentid'],
                    'companyid' => $this->companyid,
                    'created_by' => $this->session->firstname.' '.$this->session->lastname,
                    'import_via' => 'scanner'));
            echo json_encode(array('result' => $this->db->insert_id()));
            exit;
        } else {
            $this->data['agent']= $this->Agent_model->getAgent($this->uri->segment(4));
            if (empty($this->data['agent'])) {
                $this->session->set_flashdata('error', lang('You do not have access for this agent'));
                redirect('admin/agent');
            }
        }
        $this->data['title']        = lang("Simcard Scanner and assign to Customer");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'agent_assign_sim_scanner';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function send_password()
    {
        if (isPost()) {
            $setting = $this->data['setting'];
            //$result = $this->Admin_model->whmcs_api(array('action' => 'SendEmail', 'id' => $_POST['userid'], 'messagename' => 'Password Reset Validation'));
            $client = $this->Agent_model->getAgent($_POST['resellerid']);
            $this->config->set_item('language', $client->language);
            if ($client) {
                $password = "tmp" . rand(1000000, 90000000);
                $this->db = $this->load->database('default', true);
                $this->db->where('id', $_POST['resellerid']);
                $this->db->where('companyid', $this->companyid);
                $this->db->update('a_clients_agents', array('password' => password_hash($password, PASSWORD_DEFAULT)));
               
                $this->load->library('umail', array('companyid' => $this->session->cid));
                $this->umail->reseller_send_password($client, $password);
            } else {
                echo json_encode(array('result' => false, 'message' => lang('Reseller not found')));
            }
        } else {
            echo json_encode(array('result' => 'false', 'error' => lang('wrong method')));
        }
    }

    public function suspend_reseller()
    {
        if (isPost()) {
            $setting = $this->data['setting'];
            //$result = $this->Admin_model->whmcs_api(array('action' => 'SendEmail', 'id' => $_POST['userid'], 'messagename' => 'Password Reset Validation'));
            $client = $this->Agent_model->getAgent($_POST['resellerid']);
            $this->config->set_item('language', $client->language);
            if ($client) {
                $password = "tmp" . rand(1000000, 90000000);
                $this->db = $this->load->database('default', true);
                $this->db->where('id', $_POST['resellerid']);
                $this->db->where('companyid', $this->companyid);
                $this->db->update('a_clients_agents', array('status' => 'Inactive'));
                //logClient(array('userid' => $_POST['userid'], 'description' => 'Password has been changed and send to customer by ' . $_SESSION['firstname']));
                if ($this->db->affected_rows()>0) {
                    echo json_encode(array('result' => true));
                } else {
                    echo json_encode(array('result' => false, 'message' => lang('Error suspending reseller, maybe already suspended')));
                }
            } else {
                echo json_encode(array('result' => false, 'message' => lang('Reseller not found')));
            }
        }
    }

    public function unsuspend_reseller()
    {
        if (isPost()) {
            $setting = $this->data['setting'];
            //$result = $this->Admin_model->whmcs_api(array('action' => 'SendEmail', 'id' => $_POST['userid'], 'messagename' => 'Password Reset Validation'));
            $client = $this->Agent_model->getAgent($_POST['resellerid']);
            $this->config->set_item('language', $client->language);
            if ($client) {
                $password = "tmp" . rand(1000000, 90000000);
                $this->db = $this->load->database('default', true);
                $this->db->where('id', $_POST['resellerid']);
                $this->db->where('companyid', $this->companyid);
                $this->db->update('a_clients_agents', array('status' => 'Active'));
                //logClient(array('userid' => $_POST['userid'], 'description' => 'Password has been changed and send to customer by ' . $_SESSION['firstname']));
                if ($this->db->affected_rows()>0) {
                    echo json_encode(array('result' => true));
                } else {
                    echo json_encode(array('result' => false, 'message' => lang('Error Unsuspending reseller, maybe already Active')));
                }
            } else {
                echo json_encode(array('result' => false, 'message' => lang('Reseller not found')));
            }
        }
    }

    public function assign_product()
    {
        if (!empty($_POST['productids'])) {
            if (!empty($_POST['agentid'])) {
                $this->db->query('delete from a_reseller_products where agentid=?', array($_POST['agentid']));
                foreach ($_POST['productids'] as $row) {
                    $this->db->insert('a_reseller_products', array('agentid' => $_POST['agentid'], 'productid' => $row));
                    $this->session->set_flashdata('success', lang('You have assigned ').count($_POST['productids'].' '.lang('product for this reseller')));
                }
            } else {
                $this->session->set_flashdata('error', lang('Hack atempt'));
            }
        } else {
            $this->session->set_flashdata('error', lang('You must assign at least 1 product'));
        }

        redirect('admin/agent/detail/'.$_POST['agentid']);
    }
    public function export_reseller_credit_usage()
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $this->uri->segment(4));
        $this->db->where('companyid', $this->companyid);
        $q = $this->db->get('a_clients_agents');

        if ($q->num_rows()>0) {
            $this->load->library('xlswriter');
            $fileName  = 'CreditUsage-Summary_' . date('Y-m-d') . '.xlsx';
            $header    = array(
                'Date' => 'string',
                'Type' => 'string',
                'Amount ('.$this->data['setting']->currency.')' => 'string',
                'MSISDN' => 'string',
                'User' => 'string'
            );
            $this->db  = $this->load->database('default', true);
            $q         = $this->db->query("SELECT a.date,a.income_type, ROUND(a.amount,2) as amount,b.msisdn,a.user FROM `a_topups` a left join a_services_mobile b on b.serviceid=a.serviceid WHERE a.agentid = ? and a.companyid = ? ORDER BY a.id ASC", array($this->uri->segment(4), $this->session->cid));
            if ($q->num_rows() > 0) {
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment; filename="' . $fileName . '"');
                $this->xlswriter->writeSheet($q->result_array(), 'Sheet1', $header);
                $this->xlswriter->setAuthor('Simson Parlindungan');
                $this->xlswriter->setTitle('Credit Usage Export');
                $this->xlswriter->setCompany('Pareteum-United');
                echo $this->xlswriter->writeToString();
                $this->session->set_flashdata('success', 'Downloaded as requested');
            } else {
                $this->session->set_flashdata('error', 'No data found');
            }
        } else {
            $this->session->set_flashdata('error', 'Access Denied');
        }
        redirect('admin/agent/detail/'.$this->uri->segment(4));
    }
    public function assign_simcards()
    {
        $error= false;
        if ($this->data['setting']->mobile_platform == "ARTA") {
            $this->load->library('artilium', array('companyid' => $this->companyid));
        }
        $counter = 0;
        $config['upload_path'] = $this->data['setting']->DOC_PATH . $this->companyid . '/csv/';
        if (!file_exists($config['upload_path'])) {
            mkdir($this->data['setting']->DOC_PATH . $this->companyid);
            mkdir($config['upload_path']);
            // exit;
        }
        $config['allowed_types'] = 'csv|txt';
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('error', $this->upload->display_errors());
        } else {
            $data = $this->upload->data();
            $array = array_map('str_getcsv', file($this->data['setting']->DOC_PATH . $this->companyid . '/csv/' . $data['file_name']));
            log_message('error', print_r($array, true));
            foreach ($array as $row) {
                if (count($row) == 1) {
                    if (strlen(trim($row[0])) == "11") {
                        if ($this->data['setting']->mobile_platform == "ARTA") {
                            $this->load->library('magebo', array('companyid' => $this->companyid));
                            $magebosim = $this->magebo->getSimcardbyMsisdn(trim($row[0]));
                            log_message('error', print_r($magebosim, true));
                            if ($magebosim) {
                                // $counter = $counter+1;
                                $this->db->insert('a_reseller_simcard', array(
                                    'simcard' => trim($magebosim->cSIMCardNbr),
                                    'MSISDN' => trim($row[0]),
                                    'PIN1' => '1111',
                                    'PUK1' =>  $magebosim->cPUK1,
                                    'PUK2' => $magebosim->cPUK2,
                                    'resellerid' => $_POST['agentid'],
                                    'companyid' => $this->companyid,
                                    'IMSI' => $magebosim->cIMSI,
                                    'created_by' => $this->session->firstname.' '.$this->session->lastname,
                                    'import_via' => 'csv'));
                                if ($this->db->db_insert_id()>0) {
                                    $counter = $counter+1;
                                }
                                //$this->session->set_flashdata('success', $counter.' '.lang('Simcards Has been Imported successfull'));
                            } else {
                                $error .=  trim($row[0])." ".lang('Was Not Imported')."<br />";
                            }
                        }
                    } else {
                        if (!empty($row[0])) {
                            $test = $this->artilium->getSn(trim($row[0]));
                            log_message('error', print_r($test, 1));

                            $this->db->insert('a_reseller_simcard', array(
                                'simcard' => trim($row[0]),
                                'MSISDN' => trim($test->data->SN),
                                'PIN1' => '0000',
                                'PUK1' =>  $test->data->PUK1,
                                'PUK2' => $test->data->PUK2,
                                'resellerid' => $_POST['agentid'],
                                'companyid' => $this->companyid,
                                'created_by' => $this->session->firstname.' '.$this->session->lastname,
                                'import_via' => 'csv'));
                            if ($this->db->db_insert_id()>0) {
                                $counter = $counter+1;
                            }
                            $this->session->set_flashdata('success', $counter.' '.lang('Simcards Has been Imported successfull'));
                        }
                    }
                } elseif (count($row) == 7) {
                    if (strtolower($row[0]) != 'imsi') {
                        $this->db->insert('a_reseller_simcard', array(
                        'simcard' => trim($row[1]),
                        'IMSI'=> $row[0],
                        'PIN1'=> $row[2],
                        'PUK1'=> $row[3],
                        'PIN2'=> $row[4],
                        'PUK2'=> $row[5],
                        'MSISDN'=> '44'.substr($row[6], -10),
                        'resellerid' => $_POST['agentid'],
                        'companyid' => $this->companyid,
                        'created_by' => $this->session->firstname.' '.$this->session->lastname,
                        'import_via' => 'csv'));

                        if ($this->db->affected_rows() > 0) {
                            $counter++;
                            log_message('error', 'Insert log SIM '.$counter);
                        }
                    }
                    $this->session->set_flashdata('success', $counter.' '.lang('Simcards Has been Imported successfully'));
                    if ($error) {
                        $this->session->set_flashdata('error', $error);
                    }
                } else {
                    $this->session->set_flashdata('error', $counter.' '.lang('Fail Importing simcards'));
                }
            }
        }

        redirect('admin/agent/detail/'.$_POST['agentid']);
    }

    public function delete_simcard()
    {
        $id = $this->uri->segment(4);
        $this->db->where('id', $id);
        $this->db->where('companyid', $this->session->cid);
        $this->db->delete('a_reseller_simcard');
        if ($this->db->affected_rows()>0) {
            $this->session->set_flashdata('success', lang('Simcard removed'));
            echo json_encode(array('result' => true));
        } else {
            echo json_encode(array('result' => false));
        }
    }
}
