<?php
defined('BASEPATH') or exit('No direct script access allowed');
class VoipController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->lang->load('admin');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
    }

}
class Voip extends VoipController
{

    public function __construct()
    {
        parent::__construct();
        /*
        $classes = get_class_methods($this);
        foreach ($classes as $class) {
        if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
        $this->db->insert('admin_permissions', array('folder' => 'admin', 'controller' => 'subscription', 'method' => $class));
        $this->db->insert('admin_roles', array('rolename' => 'superadmin', 'perms' => $this->db->insert_id()));
        }

        }
         */

        $this->load->model('Admin_model');

    }

    public function index()
    {
        //echo $this->data['setting']->companyid;
        //$this->data['voips'] = $this->Admin_model->artiliumgetGlobal(array('GetNumberPortList', $this->data['setting']->companyid));
        //$this->data['dtt'] = 'voip.js?version=1.8';
        $this->data['title'] = "Voip Number List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'voip';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    function list() {
        $this->load->library('artilium', array($this->data['setting']->companyid, 'POST PAID'));
        $result = $this->artilium->GetNumberPort('91');

        print_r($result);
        exit;
        $q = simplexml_load_string($result->GetNumberPortListResult->ListInfo->any);
        print_r($q);
        //$this->load->model('Admin_model');
        //$this->data['voips'] = $this->Admin_model->artiliumPost(array('type' => 'POST PAID', 'action' => 'GetNumberPortList', 'companyid' => $this->data['setting']->companyid));

        //print_r($this->data['voips']);
    }
    public function add_number()
    {
        $this->load->library('artilium', array($this->data['setting']->companyid, 'POST PAID'));
        $data = $_POST;
        if (substr(trim($data['Prefix']), 0, 2) != "31") {
            $this->session->set_flashdata('error', 'Number Must start with 31 and remove leading 0  ex: 31111482662');
            redirect('admin/voip');
            exit;

        }

        $data['OperatorId'] = "91";
        $data['Prefix'] = "00" . trim($_data['Prefix']);
        $data['ValidFrom'] = $data['ValidFrom'] . 'T' . date('H:i:s');
        $result = $this->artilium->AddNumberPort($data);

        if ($result->AddNumberPortResult->Result == "0") {
            $this->db = $this->load->database('local', true);
            $this->db->replace('voip', array('number' => "00" . $_POST['Prefix'], 'voipid' => $result->AddNumberPortResult->NewItemId, 'createdby' => $this->session->firstname . ' ' . $this->session->lastname));

            $this->session->set_flashdata('result', 'Number: ' . $_POST['Prefix'] . ' has been inserted');

        } else {
            $this->session->set_flashdata('error', 'Number was not inserted');
        }

        redirect('admin/voip');
    }
    public function import_number()
    {
        $this->load->library('artilium', array($this->data['setting']->companyid, 'POST PAID'));
        $filename = '/tmp/num_voip.tst';
        $contents = file($filename);

        foreach ($contents as $line) {

            if (substr(trim($line), 0, 2) != "31") {
                print_r(array('error', 'Number Must start with 31 and remove leading 0  ex: 31111482662'));
                echo $line;
                exit;

            }

            $data['OperatorId'] = "91";
            $data['Prefix'] = "00" . trim($line);
            $data['ValidFrom'] = date('Y-m-d') . 'T' . date('H:i:s');
            $result = $this->artilium->AddNumberPort($data);

            if ($result->AddNumberPortResult->Result == "0") {

                $this->db = $this->load->database('local', true);
                $this->db->replace('voip', array('number' => "00" . trim($line), 'voipid' => $result->AddNumberPortResult->NewItemId, 'createdby' => $this->session->firstname . ' ' . $this->session->lastname));

                print_r(array('result', 'Number: ' . trim($line) . ' has been inserted'));

            } else {
                print_r(array('error', 'Number was not inserted', 'message' => $result->AddNumberPortResult->Result));
            }

            echo trim($line) . "\n";

        }
    }
    public function del_number()
    {
        $this->load->library('artilium', array($this->data['setting']->companyid, 'POST PAID'));
        $data = $_POST;
        $result = $this->artilium->DelNumberPort($data['Prefix']);

        if ($result->DeleteNumberPortResult == "0") {

            $this->db = $this->load->database('local', true);
            $this->db->where('voipid', $data['Prefix']);
            $this->db->delete('voip');
            print_r($data);
            $this->session->set_flashdata('result', 'Port id : ' . $_POST['Prefix'] . ' has been removed');

        } else {
            $this->session->set_flashdata('error', 'Number was not removed Error Code: ' . $result->DeleteNumberPortResult);
        }
        //redirect('admin/voip');
    }

    public function fix_activate_issue()
    {
        $filename = '/tmp/txt.json';
        $contents = file($filename);

        foreach ($contents as $line) {
            print_r($this->Admin_model->getHosting(trim($line)));
        }

    }

}
