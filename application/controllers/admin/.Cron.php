<?php
class Cron extends CI_Controller
{
    /**
     * @ApiDescription(section="User", description="Get information about user")
     * @ApiMethod(type="get")
     * @ApiRoute(name="/user/get/{id}")
     * @ApiReturnHeaders(sample="HTTP 200 OK")
     * @ApiReturn(type="object", sample="{'user_id':'int','external_id':'int','extra':{'type':'integer'}}")
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Api_model');
        $this->load->model('Admin_model');
        $this->load->model('Cron_model');
        $this->load->model('Teams_model');
        //$this->load->library('artilium', array(COMPANYID, PLATFORM));
    }

    public function hours()
    {
        $this->updateAddonsPrice();
        $this->termination();
        $this->SendDataCon();
    }

    public function updateAddonsPrice()
    {
        echo "Running Update Price\n";
        $this->db = $this->load->database('default', true);
        $a = $this->db->query("select a.hostingid,a.id,a.addonid,a.recurring,b.name,c.monthly from tblhostingaddons a
			left join tbladdons b on b.id=a.addonid
			left join tblpricing c on c.relid=b.id and c.type='addon'");
        foreach ($a->result() as $row) {
            if (in_array($row->addonid, array('6', '7', '8', '9', '10', '11', '12', '13', '34', '35', '36', '37', '41', '40', '42', '43'))) {
                $this->Api_model->UpdateAddonPricing($row->addonid, $row->id);
            }

            if (substr(trim(strtolower($row->name)), 0, 5) == "delta") {
                $res[$row->hostingid][] = $row->monthly;
            } else {
                $res[$row->hostingid][] = $row->recurring;
            }
        }
        print_r($res);

        $this->Admin_model->UpdateClientProductPrice($res);
        //redirect('admin');
    }
    public function getActivationDate()
    {
        $res = array();
        $q = $this->db->query("select a.userid,a.domain,a.username,a.id from tblhosting a where a.userid > 0 order by a.userid asc");
        foreach ($q->result_array() as $row) {
            $row['iAddressNbr'] = "No found";
            $user = $this->Api_model->whmcs_api(array('action' => "GetClientsdetails", "clientid" => $row['userid']));
            //print_r($user);
            foreach ($user->customfields as $r) {
                if ($r->id == "900") {
                    $row['iAddressNbr'] = $r->value;
                }
            }
            $row['domain'] = trim($row['domain']);
            $row['username'] = trim($row['username']);
            $row['package'] = $this->Cron_model->getProduct($row['id']);
            $row['activation_date'] = $this->Cron_model->getActivationEventfromSN(trim($row['username']));
            unset($row['id']);
            echo implode(';', $row) . "\n";
        }
    }

  

    public function finddate()
    {
        $id = $this->uri->segment(4);
        $res = array();
        $q = $this->db->query("select a.userid,a.domain,a.username,a.id from tblhosting a where  id=?", array($id));
        $res = $this->Cron_model->getActivationEventfromSN(trim($q->row()->username));
        $a = explode(' ', $res);
        echo $a[0];
    }
    public function finddatex()
    {
        $id = $this->uri->segment(4);
        $res = array();
        $q = $this->db->query("select a.userid,a.domain,a.username,a.id from tblhosting a where a.domainstatus =  'Active' and id=?", array($id));
        $res = $this->Cron_model->getActivationEventfromSN(trim($q->row()->username));
        $a = explode(' ', $res);
        return $a[0];
    }
    public function getnoiban()
    {

        $q = $this->db->query('select id,firstname,lastname,companyname,deltaid from tblclients');
        foreach ($q->result() as $row) {
            $client = $this->Api_model->whmcs_api(array('clientid' => $row->id, 'action' => 'GetClientsdetails'));
            $iban = false;
            foreach ($client->customfields as $r) {
                if ($r->id == "901") {
                    $iban = $r->value;
                }
            }
            if (!$iban) {
                echo $row->id . ";" . $row->firstname . ";" . $row->lastname . ";" . $row->companyname . ";" . $row->deltaid . ";" . $iban . "\n";
            }
        }
    }
    public function update_bic()
    {

        $q = $this->db->query('select id,firstname,lastname,companyname,deltaid from tblclients');
        foreach ($q->result() as $row) {
            $client = $this->Api_model->whmcs_api(array('clientid' => $row->id, 'action' => 'GetClientsdetails'));
            $iban = false;
            foreach ($client->customfields as $r) {
                if ($r->id == "901") {
                    $iban = $r->value;
                }
            }

            if ($iban) {
                $bic = $this->Api_model->getBic(trim($iban));
                if ($bic) {
                    if ($bic->valid) {
                        $this->Api_model->whmcs_api(array('action' => 'UpdateClient', 'clientid' => $row->id, 'customfields' => base64_encode(serialize(array("1422" => $bic->bankData->bic)))));
                        $bank_name = $bic->bankData->name;
                        $bank_bic = $bic->bankData->bic;
                        echo $row->id . ";VALID;\n";
                    } else {
                        echo $row->id . ";" . $row->firstname . ";" . $row->lastname . ";" . $row->companyname . ";" . $row->deltaid . ";" . $iban . ";" . $bank_name . ";" . $bank_bic . "\n";
                        $bank_name = "NA";
                        $bank_bic = "NA";
                    }
                    //$this->Api_model->whmcs_api(array('action' => 'UpdateClient', 'customfields' => base64_encode(serialize(array("1422" => $bic)))));
                } else {
                    $bank_name = "NA";
                    $bank_bic = "NA";
                }
            }

            unset($client);
            unset($iban);
            unset($bic);
        }
    }

    public function updateSepaMagebo()
    {
        /*
        $filename = '/tmp/iban.txt';
        $contents = file($filename);

        foreach ($contents as $line) {
        $f = explode(';', $line);
        $clientids[] = trim($f[0]);
        }
         */
        $q = $this->db->query("select id,firstname,lastname,companyname,deltaid from tblclients");
        foreach ($q->result() as $row) {
            $client = $this->Api_model->whmcs_api(array('clientid' => $row->id, 'action' => 'GetClientsdetails'));
            $iban = false;
            $bics = false;
            $iAddressNbr = false;
            foreach ($client->customfields as $r) {
                if ($r->id == "900") {
                    $iAddressNbr = $r->value;
                }
                if ($r->id == "901") {
                    $iban = $r->value;
                }

                if ($r->id == "1422") {
                    $bics = $r->value;
                }
            }

            if ($iban && $bics && $iAddressNbr) {
                $res = $this->Api_model->custom_api_sepa(array('action' => 'magebosepaadd', 'iAddressNbr' => $iAddressNbr, 'bic' => $bics, 'iban' => $iban));
                echo $row->id . ";" . $row->firstname . ";" . $row->lastname . ";" . $iAddressNbr . ";" . $row->deltaid . ";" . $iban . ";" . $bics . "\n";
                print_r($res);
            }

            unset($client);
            unset($iban);
            unset($bics);
            unset($iAddressNbr);
        }
    }

    public function updatePricingNow()
    {
        $this->db = $this->load->database('default', true);

        $q = $this->db->query("select id,domain,username from tblhosting where domainstatus = ? order by id asc", array('Active'));
        //echo $q->num_rows();
        foreach ($q->result_array() as $row) {
            echo "Executing: " . $row['id'] . " - " . $row['domain'] . "- " . $row['username'] . "\n";

            $s = $row;
            $s['addon'] = $this->Api_model->getDeltaHosting($row['id']);
            $s['api'] = $this->Api_model->custom_api(array('action' => 'porting', 'serviceid' => $row['id'], 'step' => 4));
            if (!empty($s['addon'])) {
                $s['result'] = $this->Api_model->whmcs_api(array('action' => 'UpdateClientAddon', 'id' => $s['addon']['id'], 'status' => 'Pending'));
                $s['result'] = $this->Api_model->whmcs_api(array('action' => 'UpdateClientAddon', 'id' => $s['addon']['id'], 'status' => 'Active'));

                $services = $this->Api_model->GetServiceDetails($row['id']);
                if ($services->customfields['Porting'] == "on") {
                    $this->Api_model->PortIngAction1($services);
                    $this->Api_model->PortIngAction2($services);
                    $this->Api_model->PortIngAction3($services);
                    $this->Api_model->PortIngAction4($services);

                    $this->Api_model->custom_api(array('action' => 'porting', 'serviceid' => $row['id'], 'step' => 6));
                }

                unset($s);
            }
        }
    }

    public function test()
    {

        $services = $this->Api_model->GetServiceDetails('700431');
        print_r($services);
        if ($services->customfields['Porting'] == "on") {
            $this->Api_model->PortIngAction3($services);

            //$this->Api_model->custom_api(array('action' => 'porting', 'serviceid' => $row['id'], 'step' => 6));
        }
    }
    public function process_porting()
    {

        $res = $this->db->query("select a.id,a.serviceid,b.domain,b.username,b.userid from event_status a left join tblhosting b on b.id=a.serviceid where a.status=? and a.ack=?", array('PortingActivation', '0'));

        if ($res->num_rows() > 0) {
            foreach ($res->result_array() as $row) {
                if ($row['userid']) {
                    $iAddressNbr = getMageboid($row['userid']);
                }
                $s['addon'] = $this->Api_model->getDeltaHosting($row['serviceid']);
                $s['api'] = $this->Api_model->custom_api(array('action' => 'porting', 'serviceid' => $row['serviceid'], 'step' => 4));
                if (!empty($s['addon'])) {
                    $s['result'] = $this->Api_model->whmcs_api(array('action' => 'UpdateClientAddon', 'id' => $s['addon']['id'], 'status' => 'Pending'));
                    $s['result'] = $this->Api_model->whmcs_api(array('action' => 'UpdateClientAddon', 'id' => $s['addon']['id'], 'status' => 'Active'));

                    $services = $this->Api_model->GetServiceDetails($row['serviceid']);
                    if ($services->customfields['Porting'] == "on") {
                        $this->Api_model->PortIngAction1($services);
                        $this->Api_model->PortIngAction2($services);
                        $this->Api_model->PortIngAction3($services);
                        $this->Api_model->PortIngAction4($services);
                        $this->Api_model->PortIngAction5($iAddressNbr);
                        $this->Api_model->custom_api(array('action' => 'porting', 'serviceid' => $row['serviceid'], 'step' => 6));
                        unset($iAddressNbr);
                    }

                    unset($s);
                }
                $this->db->query("update event_status set ack=? where id=?", array('1', $row['id']));
            }
        } else {
            echo "nothing to process\n";
        }
    }
    public function updateclientInfo()
    {
        $magebo['Einvoice'] = false;
        $magebo['DB'] = false;
        $magebo['NN'] = false;
        $q = $this->db->query("SELECT distinct(relid) as id FROM `tblcustomfieldsvalues` WHERE `fieldid` = 900 and value > 0 ORDER BY `tblcustomfieldsvalues`.`value` ASC");
        foreach ($q->result() as $row) {
            $client = $this->Api_model->whmcs_api(array('action' => "GetClientsdetails", "clientid" => $row->id));

            foreach ($client->customfields as $r) {
                if ($r->id == "900") {
                    $magebo['iAddressNbr'] = $r->value;
                }

                if ($r->id == "901") {
                    $magebo['IBAN'] = $r->value;
                }

                if ($r->id == "910") {
                    $magebo['Einvoice'] = $r->value;
                }

                if ($r->id == "1422") {
                    $magebo['BIC'] = $r->value;
                }

                if ($r->id == "908") {
                    $magebo['NN'] = $r->value;
                }

                if ($r->id == "907") {
                    $magebo['DB'] = $r->value;
                }
            }

            if (!empty($client->companyname)) {
                $name = $client->companyname;
            } else {
                $name = $client->firstname . ' ' . $client->lastname;
            }
            if ($magebo['iAddressNbr'] > 0) {
                //Update Name:
                $this->Api_model->UpdateMageboTblAddress($magebo['iAddressNbr'], 'cName', $name);
                //Update Address:
                $this->Api_model->UpdateMageboTblAddress($magebo['iAddressNbr'], 'cStreet', $client->address1);
                //Update City:
                $this->Api_model->UpdateMageboTblAddress($magebo['iAddressNbr'], 'cCity', $client->city);

                $zip = explode(' ', trim($client->postcode));
                if (count($zip) == 2) {
                    //Update Postcode:
                    $this->Api_model->UpdateMageboTblAddress($magebo['iAddressNbr'], 'iZipCode', $zip[0]);
                    //Update Postcode Suffix:
                    $this->Api_model->UpdateMageboTblAddress($magebo['iAddressNbr'], 'cZIPSuffix', $zip[1]);
                }

                //Update Country
                $countryid = $this->Api_model->getCountryIndex($client->country);
                $this->Api_model->UpdateMageboTblAddress($magebo['iAddressNbr'], 'iCountryIndex', $countryid);

                //Update Email
                if ($this->Api_model->isUpdateMageboTblAddressData($magebo['iAddressNbr'], '34')) {
                    $this->Api_model->UpdateMageboTblAddressData($magebo['iAddressNbr'], '34', $client->email);
                } else {
                    //$id, $groupnbr, $fieldname, $value
                    $this->Api_model->InsertMageboTblAddressData($magebo['iAddressNbr'], 6, 'E-MAIL', $client->email);
                }

                //Update Phonenumber
                if ($this->Api_model->isUpdateMageboTblAddressData($magebo['iAddressNbr'], '18')) {
                    $this->Api_model->UpdateMageboTblAddressData($magebo['iAddressNbr'], '18', $client->phonenumber);
                } else {
                    $this->Api_model->InsertMageboTblAddressData($magebo['iAddressNbr'], 4, 'TELEPHONENUMBER', $client->phonenumber);
                }
                //update National Number
                if ($magebo['NN']) {
                    if ($this->Api_model->isUpdateMageboTblAddressData($magebo['iAddressNbr'], '896')) {
                        $this->Api_model->UpdateMageboTblAddressData($magebo['iAddressNbr'], '896', $magebo['NN']);
                    } else {
                        $this->Api_model->InsertMageboTblAddressData($magebo['iAddressNbr'], 6, 'IDENTITY CARD NUMBER', $magebo['NN']);
                    }
                }

                //update Date Of birth
                if ($magebo['DB']) {
                    if ($this->Api_model->isUpdateMageboTblAddressData($magebo['iAddressNbr'], '494')) {
                        $this->Api_model->UpdateMageboTblAddressData($magebo['iAddressNbr'], '494', $magebo['DB']);
                    } else {
                        $this->Api_model->InsertMageboTblAddressData($magebo['iAddressNbr'], 7, 'BIRTHDATE', $magebo['DB']);
                    }
                }
                // update vatexempt need manual on the portal
                //$this->Api_model->UpdateMageboTblAddressData($magebo['iAddressNbr'], 'iCountryIndex', $countryid);
                // update eInvoice
                if ($magebo['Einvoice']) {
                    $this->Api_model->updateEinvoice($magebo['iAddressNbr'], $magebo['Einvoice']);
                }
            }

            echo $row->id . " - " . implode(' - ', $magebo) . "\n";
            $magebo['Einvoice'] = false;
            $magebo['DB'] = false;
            $magebo['NN'] = false;
            unset($client);
        }
    }

    public function updatIban()
    {

        $filename = '/tmp/iban.txt';
        $contents = file($filename);

        foreach ($contents as $line) {
            $f = explode(';', $line);
            print_r($f);
            $s = $this->Api_model->whmcs_api(array('action' => 'UpdateClient', 'clientid' => trim($f['0']), 'customfields' => base64_encode(serialize(array("901" => trim($f['1']))))));
            print_r($s);
            unset($f);
        }
    }

    public function ActivateServices()
    {
        $companies = getCompanies();
        foreach ($companies as $cid) {
            $this->load->library('artilium', array('companyid' => $cid));
            $this->load->library('magebo', array('companyid' => $cid));
            $sims = $this->db->query("select b.* from a_services a left join a_services_mobile b on b.id=a.serviceid where a.type=? where a.date_contract=? and a.companyid=?", array('mobile', date('m-d-Y'), $cid));

           // foreach ($sims->result() as $sim) {

            if ($cid == "53") {
                print_r($this->artilium->getSN('8931162111842049485'));
            } elseif ($cid == "54") {
                print_r($this->artilium->getSN('8931162111800020411'));
            }
            /*
            $order = $this->Admin_model->getServiceCli($sim->serviceid);
            $this->magebo->addPricingSubscription($order);
            unset($order);
            ChangeServiceStatus($sim->serviceid, 'Active');
            ChangeOrdertatus($sim->serviceid, 'Active');

            $msisdn = $this->artilium->getSn($sim->msisdn_sim);
            $pack = $this->artilium->GetListPackageOptionsForSn($msisdn->data->SN);
            $this->artilium->UpdateServices($msisdn->data->SN, $pack->NewDataSet->PackageOptions, '1');
            unset($msisdn);
            unset($pack);
            */
           // }
           // unset($sims);
            unset($cid);
            unset($this->magebo);
            unset($this->artilium);
        }
    }

    public function update_Promotion()
    {
        $filename = '/tmp/icode.txt';
        $contents = file($filename);

        foreach ($contents as $line) {
            $clientid[] = trim($line);
        }
        $q = $this->db->query("select id,userid,domain,username,domainstatus from tblhosting where promoid=?", array(1));
        $count = 0;
        foreach ($q->result() as $row) {
            //print_r($row);
            $client = $this->Api_model->whmcs_api(array('action' => 'GetClientsdetails', 'clientid' => $row->userid));
            $magebo['whmcsid'] = $row->userid;
            //print_r($client);
            foreach ($client->customfields as $r) {
                if ($r->id == "900") {
                    $magebo['iAddressNbr'] = $r->value;
                }
            }

            $magebo['activation_date'] = $this->Cron_model->getActivationEventfromSN(trim($row->username));

            if ($magebo['iAddressNbr'] > 0) {
                if (in_array($magebo['iAddressNbr'], $clientid)) {
                    $count = $count + 1;
                    $res = $this->Api_model->insertPromo($magebo['iAddressNbr'], -4.1322, 'Introductie Korting €5, 6mnd: ' . $this->Api_model->getNumber($row->domain), $magebo['activation_date']);
                    print_r($res);
                    $this->Api_model->InsertNote($row->id, 'Discount Applied in Magebo');
                } else {
                    echo $row->userid . " " . $row->domain . " " . $row->domainstatus . "\n";
                }
                echo $count . "\n";
            }

            unset($client);
            unset($magebo);
        }
        //execute spInsertGeneralPricing 20000000, 'DELTA MOBILE', 'Discount', 1, -4.1322, 21, 1, 6, '10/04/2018', 8, 2018, 1, 'Promo abracadabra', 'NO REPORTING', 'Promo abracadabra', 1
    }

    public function getEvents()
    {
        $this->db = $this->load->database('events', true);
        $q = $this->db->query("select * from events_raw where reseller is null");
        foreach ($q->result() as $row) {
            $resellerid = $this->xml_toarray($row->rawdata);
            echo $row->id . " " . $resellerid . "\n";
            $this->Cron_model->update_resellerid($row->id, $resellerid);
        }
    }

    public function xml_toarray($str)
    {

        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML($str);
        $XMLresults = $doc->getElementsByTagName("ResellerId");
        return $XMLresults->item(0)->nodeValue;
    }

    public function soap2array($str)
    {
        $xml = file_get_contents($str);
    // SimpleXML seems to have problems with the colon ":" in the <xxx:yyy> response tags, so take them out
        //$xml = preg_replace(/(<\/?)(\w+):([^>]*>)/, $1$2$3, $xml);
        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", '$1$2$3', $result);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        return json_decode($json, true);
    }

    public function GetAvailableProvisioningSims($sim)
    {
        //$this->load->library('artilium', array(COMPANYID, PLATFORM));

        $res = $this->artilium->GetAvailableProvisioningSims($sim);
        print_r($res);
    }

    public function GetActivationCPSList()
    {
        //$this->load->library('artilium', array(COMPANYID, PLATFORM));

        $res = $this->artilium->GetActivationCPSList($sim);
        print_r($res);
    }

    public function diffrent($companyid)
    {
        $filename = 'OnnetVoipMsisdn_' . date('YmdHis') . '.csv';
        $myfile = fopen("/tmp/" . $filename, "w") or die("Unable to open file!");

        $this->load->library('artilium', array("companyid" => $companyid));
        $res = $this->artilium->GetSimList();
       
        foreach ($res as $number) {
            if ($number['Status'] == 5) {
                $r = getSimcardInfo(trim($number['SIMNr']));
                
                if ($r) {
                   // print_r($r);
                    $d = explode('.', $number['DateStatusChanged']);
                    $e = str_replace('T', ' ', $d[0]);
                    $number['domain'] = $r['msisdn'];
                    $number['serial'] = $r['msisdn_sn'];
                    if ($number['serial'] != $r['msisdn_sn']) {
                        echo date('Y-m-d H:i:s')." -- ". $r['msisdn_sn'] . "," . $r['domainstatus'] . "," . $e . "\n";
                        fwrite($myfile, $r['msisdn_sn'] . "," . $r['domainstatus'] . "," . $e . "\n");
                    }
                   

                    if (strpos($e, "+")) {
                        $x = explode('+', $e);
                        $e = $x[0];
                    }
                    if ($r['status'] = "ACTIVE") {
                        echo date('Y-m-d H:i:s')." -- ".$r['msisdn'] . "," . $r['domainstatus'] . "," . $e . "\n";
                        fwrite($myfile, $r['msisdn'] . "," . $r['domainstatus'] . "," . $e . "\n");
                    }

                    unset($e);
                    unset($x);
                    unset($d);
                    unset($r);
                } else {
                    $s['errror'][] = $number;
                }
                $s[] = $number;

                unset($number);
            }
        }

        fclose($myfile);
        $this->upload_file($filename);
    }

    public function SendDataCon($companyid)
    {
        $this->load->model('Admin_model');
        $count = 0;
        $filename = 'OnnetVoipMsisdn_' . date('YmdHis') . '.csv';

        //$this->load->library('artilium', array(COMPANYID, PLATFORM));
        $res = $this->db->query("select a.date_terminate,a.id,a.date_contract as date,a.status,b.msisdn as domain,b.msisdn_sn as username from a_services a left join a_services_mobile b on b.serviceid=a.id where a.status in ('Active','Terminated') and b.companyid=? and datacon_ftp = 0", array($companyid));
        if ($res->num_rows() > 0) {
            $myfile = fopen("/tmp/" . $filename, "w") or die("Unable to open file!");
            echo  "Found ".$res->num_rows()." files to be uploaded\n";
            foreach ($res->result_array() as $number) {
                if (in_array($number['status'], array('Active'))) {
                    $status = 'ACTIVE';
                    print_r($number);
                    $t = explode('-', $number['date']);
                    echo date('Y-m-d H:i:s')." -- ". $number['domain'] . "," . $status . "," . $t[2]."-".$t[0]."-".$t[1]." 00:00:00\n";
                    fwrite($myfile, $number['domain'] . "," . $status . "," . $t[2]."-".$t[0]."-".$t[1]." 00:00:00\n");
                    $this->Admin_model->update_services_data('mobile', $number['id'], array('datacon_ftp' => 1,'datacon_date' => date('Y-m-d')));
                    unset($t);
                    unset($status);
                    unset($number);
                } elseif (in_array($number['status'], array('Terminated'))) {
                    $status = 'DEACTIVE';
                   
                    if (empty($number['date_terminate'])) {
                        $number['date_terminate'] = "2019-01-01";
                    }
                    echo date('Y-m-d H:i:s')." -- ".$number['username'] . "," . $status . "," . $number['date_terminate']." 00:00:00\n";
                        fwrite($myfile, $number['username'] . "," . $status . "," . $number['date_terminate']." 00:00:00\n");
                        $this->Admin_model->update_services_data('mobile', $number['id'], array('datacon_ftp' => 1,'datacon_date' => date('Y-m-d')));
                    unset($status);
                    unset($number);
                }
            }
            fclose($myfile);
            $this->upload_file($filename);
        }
    }

    public function upload_file($filename)
    {

        $localFile = '/tmp/' . $filename;

        $remoteFile = '/export/' . $filename;
        /*
        $host = "ftp.datacon.nl";
        $port = 22;
        $user = "delta";
        $pass = "D3lt@S3cr3t!";
         */
        $host = "ftp.datacon.nl";
        $port = 22;
        $user = "onnet@zeelandnet.nl";
        $pass = "^B2Jq5I";
        $connection = ssh2_connect($host, $port);
        ssh2_auth_password($connection, $user, $pass);
        $sftp = ssh2_sftp($connection);
        if ($sftp) {
            echo date('Y-m-d H:i:s')." -- Uploading Files ".$filename."\n".
            $stream = fopen("ssh2.sftp://$sftp$remoteFile", 'w');
            $file = file_get_contents($localFile);
            fwrite($stream, $file);
            unlink($localFile);
        }

        fclose($stream);
    }

    public function process_product_changes()
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select a.*,b.companyid,c.userid from subscription_changes a left join tblhosting c on c.id=a.serviceid left join tblclients b on b.id=c.userid where a.date_commit=? and a.status=1", array(date('Y-m-d')));
        if ($q->num_rows() > 0) {
            echo "We found " . $q->num_rows() . " changes for today\n";
            sleep(4);
            foreach ($q->result() as $row) {
                if ($row->id != "1") {
                    if ($this->Cron_model->ChangeProduct($row)) {
                        logAdmin(array('serviceid' => $row->serviceid, 'userid' => $row->userid, 'user' => 'System', 'ip' => '127.0.0.1', 'description' => 'Service : ' . $row->serviceid . ' has been Changed to ' . $row->new_pid));
                        print_r($row);
                    }
                }
            }
        } else {
            echo "No changes applied for today\n";
        }
    }

    public function genApache()
    {

        $q = $this->db->query("select * from a_mvno where apache=2");

        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $url = url_to_domain(trim($row->portal_url));
                if (!file_exists('/etc/apache2/sites-enabled/' . $url . '.conf')) {
                    $table = <<<EOT

<VirtualHost *:80>
ServerAdmin webmaster@localhost
DocumentRoot /home/mvno/public_html/
Options -Indexes
ServerName $url
LogFormat "%v %h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" cplus
CustomLog /var/log/apache2/$url.log cplus
ServerSignature On
LogLevel debug
</VirtualHost>

        <VirtualHost *:443>
DocumentRoot /var/www/partner
ServerName $url
SSLEngine on
        SSLCACertificateFile /etc/apache2/ssl/globalsignca.crt
        LogFormat "%v %h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" cplus
        CustomLog /var/log/apache2/$url.log cplus
        ServerSignature On
  <FilesMatch "\.php$">
        SetHandler "proxy:fcgi://127.0.0.1:9999/"
  </FilesMatch>

<IfModule mod_ssl.c>
    CustomLog /var/log/apache2/$url.log common
    ErrorLog /var/log/apache2/$urlerror.log
    LogLevel debug
  </IfModule>
LogLevel debug
Include /etc/letsencrypt/options-ssl-apache.conf
SSLCertificateFile /etc/letsencrypt/live/trendcall.united-telecom.nl/fullchain.pem
SSLCertificateKeyFile /etc/letsencrypt/live/trendcall.united-telecom.nl/privkey.pem
</VirtualHost>

EOT;

                    $myfile = fopen("/etc/apache2/sites-enabled/" . trim($url) . ".conf", "w") or die("Unable to open file!");

                    fwrite($myfile, $table);

                    fclose($myfile);
                    unset($table);
                    mail('simson.parlindungan@united-telecom.be', 'Certificate Required', 'Please configure Certificate of ' . $url . ' by executing ./certbot-auto -d ' . $url . ' --apache -n  on server: ' . gethostbyname('delta.united-telecom.nl'));
                    unset($url);

                    $this->db->query("update a_mvno set apache=1 where id=?", array($row->id));
                } else {
                    mail('simson.parlindungan@united-telecom.be', 'Apache Configuration Failed', 'Url ' . $url . ' already exist on development server ' . gethostbyname('delta.united-telecom.nl'));
                }
            }

            shell_exec('service apache2 restart');
        }
    }

  
}
