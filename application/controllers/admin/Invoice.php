<?php
defined('BASEPATH') or exit('No direct script access allowed');
class InvoicesController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }

        $this->companyid = get_companyidby_url(base_url());
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->data['setting'] = globofix($this->companyid);
        $this->load->library('magebo', array('companyid' => $this->companyid));
    }
}
class Invoice extends InvoicesController
{
    public function __construct()
    {
        parent::__construct();
        if (!empty($this->session->profiler)) {
            if ($this->session->profiler) {
                $this->output->enable_profiler(true);
                error_reporting(E_ALL);
                ini_set('display_errors', 1);
            }
        }
        $this->companyid = get_companyidby_url(base_url());
        /*
        $classes = get_class_methods($this);
        foreach ($classes as $class) {
        if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
        $this->db->insert('admin_permissions', array('folder' => 'admin', 'controller' => 'invoice', 'method' => $class));
        $this->db->insert('admin_roles', array('rolename' => 'superadmin', 'perms' => $this->db->insert_id()));
        }

        }
         */
        $this->load->model('Admin_model');
        if ($this->session->email == "simson.parlindungan@united-telecom.be") {
            // $this->output->enable_profiler(TRUE);
        }
    }
    public function sendInvoice_Reminder()
    {
        error_reporting(1);
        $companyid = $_POST['companyid'];
        $type = $_POST['reminder_type'];
        $this->load->model('Admin_model');

        $this->load->library('artilium', array('companyid' => $this->companyid));

        $invoice = $this->magebo->getReminderInvoice($_POST['iInvoiceNbr'], $type);
        // echo json_encode($invoice);
        // exit;
        $client = $this->Admin_model->getClient(getWhmcsid($_POST['iAddressNbr']));
        if ($type == "reminder_2") {
            $services = $this->Admin_model->getServices($client->id);
            foreach ($services as $serv) {
                if ($serv->bar != 1) {
                    $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($serv->sn));
                    $this->Admin_model->save_state($serv->id, $pack);
                    $this->artilium->BlockOriginating(trim($serv->sn), $pack, '0');
                    $this->Admin_model->update_services_data('mobile', $serv->id, array('bar' => 1));
                }
            }
        }

        $this->Admin_model->ChangeClientDunnigProfile($client->id, $type);
        $setting = globofix($companyid);
        //print_r($this->data['setting']);
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = true;
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent($type, $client->language, $client->companyid);
        //mail('mail@simson.one', 'reminder', print_r($_POST, true));
        $invoice_ref = $this->magebo->Mod11($_POST['iInvoiceNbr']);

        $payment = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
        $amount = $invoice->mInvoiceAmount - $payment;
        $body = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
        $body = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$dInvoiceDate}', $invoice->dInvoiceDate, $body);
        $body = str_replace('{$dInvoiceDueDate}', $invoice->dInvoiceDueDate, $body);
        $body = str_replace('{$Companyname}', $setting->companyname, $body);
        $body = str_replace('{$name}', format_name($client), $body);
        // $body = str_replace('{$base_url}', 'mijnmobiel.delta.nl', $body);
        $cc = getContactInvoiceEmail($client->id);
        if ($cc) {
            $this->email->cc($cc);
        }
        $body = str_replace('{$cInvoiceReference}', $invoice_ref, $body);
        $this->email->set_newline("\r\n");
        $this->email->from($setting->smtp_sender, $setting->smtp_name);
        $client_email = getInvoiceEmail($client->id);
        $this->email->to($client_email);
        $this->email->bcc($this->session->email);
        //$this->email->to(array('mail@simson.one', 'melody.kohne@trendcall.com'));
        $subject = getSubject($type, $client->language, $companyid);
        $subject = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $subject);
        $subject = str_replace('{$Companyname}', $setting->companyname, $subject);
        $this->email->subject($subject);
        $this->email->message($body);
        if ($companyid == "54") {
            $cdr = true;
        } else {
            $cdr = false;
        }

        $file = false;
        if ($file) {
            $this->email->attach($file);
        }
        if (!isTemplateActive($client->companyid, $type)) {
            log_message('error', 'Template '.$type.' is disabled sending aborted');
            $this->Admin_model->insertReminderLog(array(
                'invoicenum' => $_POST['iInvoiceNbr'],
                'userid' => $client->id,
                'type' => ucfirst(str_replace('_', '', $type)),
                'companyid' => $companyid,
                'amount' => $invoice->mInvoiceAmount));

            echo json_encode(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'result' => false,
                'error' => 'this template is disabled by admin'
            ));
            exit;
        }

        if ($this->email->send()) {
            logEmailOut(array(
                'userid' => $client->id,
                'to' => $client->email,
                'companyid' => $client->companyid,
                'subject' => $subject,
                'message' => $body,
            ));
            if ($cc) {
                foreach ($cc as $em) {
                    logEmailOut(array(
                    'userid' => $client->id,
                    'to' => $em,
                    'subject' => $subject,
                    'message' => $body,
                    'companyid' => $companyid,
                    ));
                }
            }
            $this->Admin_model->insertReminderLog(array(
                'invoicenum' => $_POST['iInvoiceNbr'],
                'userid' => $client->id,
                'type' => ucfirst(str_replace('_', '', $type)),
                'companyid' => $companyid,
                'amount' => $invoice->mInvoiceAmount));

            echo json_encode(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'result' => true,
            ));
        } else {
            echo json_encode(array(
                'invoicenum' => $invoice->iInvoiceNbr,
                'result' => false,
                'error' => $this->email->print_debugger(),
            ));
        }
    }

    public function update_duedate_invoice()
    {
        $this->load->library('magebo', array('companyid' => $this->session->cid));
        $this->magebo->updateInvoiceDuedate($_POST, $this->session->cid);
        $this->session->set_flashdata('success', 'Please verify the update below');
        redirect('admin/invoice/detail/' . $_POST['iInvoiceNbr'] . '/' . $_POST['iAddressNbr']);
    }
    public function index()
    {
        $this->data['stats'] = $this->Admin_model->getInvoicebyMonth($this->session->cid);
        $this->data['dtt'] = 'invoices.js?version=1.2';
        $this->data['title'] = "Invoices List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'invoice';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function ssp()
    {
        //  $this->data['stats'] = $this->Admin_model->getInvoicebyMonth($this->session->cid);
        //   $this->data['dtt'] = 'invoices.js?version=1.2';
        $this->data['title'] = "Invoices List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'invoices_ssp';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }


    public function overdue()
    {
        // $this->data['stats'] = $this->Admin_model->getStatistic();
        $this->data['dtt'] = 'invoices.js?version=1.2';
        $this->data['title'] = "Overdue Invoices List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'invoices_overdue';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function online_transaction()
    {
        $this->data['title'] = "Online Transaction List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'online_transactions';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function invoice_reminders()
    {
        $this->data['title'] = "Invoice Reminder List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'invoice_reminders';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function detail()
    {
        if ($this->session->firstname == "Simson") {
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
        }
        if (iAllowedInvoice($this->companyid, $this->uri->segment(5))) {
            //  print_r($this->data);
            $clientid = getClientidbyMagebo($this->uri->segment(5));
            $this->data['whmcsid'] = $clientid;

            if ($this->companyid == 54) {
                // $this->load->library('magebo', array('companyid' => 54));
                $this->data['invoice_ref'] = $this->magebo->Mod11($this->uri->segment(4));
            } else {
                if ($this->data['setting']->country_base == "BE") {
                    $this->data['invoice_ref'] = ogm($this->uri->segment(4));
                } else {
                    $this->data['invoice_ref'] = $this->uri->segment(4);
                }
            }
            $this->data['client'] = $this->Admin_model->getClient($clientid);
            $this->data['invoice'] = $this->Admin_model->getInvoicedetail($this->uri->segment(4));
            $this->load->library('sisow', array("2537491064", "503ef491ae8da9b02a9eaed101f93de602264d89"));
            $this->data['payments'] = $this->magebo->getPaymentListInvoice($this->uri->segment(4));
            $this->data['rejects'] = $this->Admin_model->getSEPARejectListInvoice($this->uri->segment(4));

            $this->data['title'] = "Client List Dasboard";
            $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'invoice_detail';
            $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
        } else {
            $this->session->set_flashdata('error', 'This Invoice does not exist');
            redirect('admin/invoice');
        }
    }
    public function export_invoice_pdf_bydate()
    {
        $filename = time() . '_Invoices.pdf';
        $this->db = $this->load->database('magebo_replication', true);
        $this->db->select("iInvoiceNbr as id,iInvoiceNbr as invoicenum");
        $this->db->from("magebo_invoices");
        if (!empty($_POST['userid'])) {
            $this->db->where("iAddressNbr", $_POST['userid']);
        }
        if ($_POST['status'] != "ALL") {
            $this->db->where("iInvoiceStatus", $_POST['status']);
        }
        $this->db->where("dInvoiceDate >=", $_POST['start']);
        $this->db->where("dInvoiceDate <=", $_POST['end']);
        $this->db->where("iCompanyNbr", $this->data['setting']->companyid);
        $this->db->order_by("iInvoiceNbr", "ASC");
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $data) {
                $this->download_id($data['id']);

                $invoices[] = $this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $data['id'] . '.pdf';
            }
            $files = implode(' ', $invoices);

            echo shell_exec('pdftk ' . $files . ' output ' . $this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $filename);
            ob_clean();
            flush();

            //echo $this->data['setting']->DOC_PATH . 'invoices/' . $invoice['id'] . '.pdf';
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($this->data['setting']->DOC_PATH . $this->companyid . "/invoices/" . $filename));
            header('Content-Disposition: attachment; filename="' . $filename . '"');
            readfile($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $filename);
            unlink($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/' . $filename);
        } else {
            $this->session->set_flashdata('error', lang('There is no result to be exported'));
            redirect('admin/invoice');
        }
    }

    public function export_invoice_csv_bydate()
    {
        $fileName = 'Invoice-Summary_by_Date_' . time() . '.csv';
        $this->db = $this->load->database('magebo_replication', true);

        $this->db->select("magebo_invoices.iInvoiceNbr as Invoicenumber,magebo_invoices.iAddressNbr as ClientId,tblAddress.cName as Customer,magebo_invoices.dInvoiceDate as Date,magebo_invoices.dInvoiceDueDate as Duedate,
            CASE magebo_invoices.iInvoiceStatus
  WHEN '54' THEN 'PAID'
  ELSE 'UNPAID'
  END as 'Status',magebo_invoices.mInvoiceAmount as Amount");
        $this->db->from("magebo_invoices");
        $this->db->join("tblAddress", "tblAddress.iAddressNbr=magebo_invoices.iAddressNbr", "LEFT");
        if (!empty($_POST['userid'])) {
            $this->db->where("magebo_invoices.iAddressNbr", $_POST['userid']);
        }
        if ($_POST['status'] != "ALL") {
            $this->db->where("magebo_invoices.iInvoiceStatus", $_POST['status']);
        }
        $this->db->where("magebo_invoices.dInvoiceDate >=", $_POST['start']);
        $this->db->where("magebo_invoices.dInvoiceDate <=", $_POST['end']);
        $this->db->where("magebo_invoices.iCompanyNbr", $this->data['setting']->companyid);
        $this->db->order_by("magebo_invoices.iInvoiceNbr", "ASC");
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header('Content-Description: File Transfer');
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename={$fileName}");
            header("Expires: 0");
            header("Pragma: public");

            $fh = @fopen('php://output', 'w');
            $headerDisplayed = false;
            foreach ($q->result_array() as $data) {
                // Add a header row if it hasn't been added yet
                if (!$headerDisplayed) {
                    // Use the keys from $data as the titles
                    fputcsv($fh, array_keys($data));
                    $headerDisplayed = true;
                }

                // Put the data into the stream
                fputcsv($fh, $data);
            }
            // Close the file
            fclose($fh);
            // Make sure nothing else is sent, our file is done
            exit;
        } else {
            $this->session->set_flashdata('error', lang('There is no result to be exported'));
            redirect('admin/invoice');
        }
    }

    public function download()
    {
        if (!file_exists($this->data['setting']->DOC_PATH  . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . $this->companyid . '/invoices/', 0755, true);
        }
        if (!file_exists($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr')) {
            mkdir($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH  . $this->companyid . '/invoices/';

        $id = $this->uri->segment(4);
        if ($this->data['setting']->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }

        if ($this->Admin_model->IsAllowedInvoice($id, $companyid)) {
            $invoice = $this->Admin_model->getInvoicePdf($id);

            //print_r($invoice->PDF);
            //exit;
            $file = $invoice_path . $id . '.pdf';
            file_put_contents($file, $invoice->PDF);
            //if ($this->session->cid != "53") {
            $cdr = $this->downloadcdrpdf($id, $this->session->cid);
            //print_r($cdr);

            if ($cdr) {
                shell_exec('pdftk ' . $file . ' ' . $cdr . ' cat output ' . $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf');
                unlink($file);
                copy($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf', $file);
                $file = $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf';
            }
            //}

            if (file_exists($file)) {
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $id . '.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);
            } else {
                //echo  $invoice_path ."\n";
                echo "file does not exists";
            }
        } else {
            $this->session->set_flashdata('error', lang('This acction has been denied'));
            redirect('errors/access_denied');
        }
    }
    public function downloadcdrpdf($id, $companyid)
    {
        error_reporting(1);
        $this->load->library('magebo', array('companyid', $companyid));
        $setting = globofix($companyid);

        $inv =  $this->magebo->getInvoice($id);
        $clientid = getClientidbyMagebo($inv->iAddressNbr);
        if (!empty($clientid)) {
            $client = $this->Admin_model->getClient($clientid);
            $this->config->set_item('language', $client->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }


        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }



        $cdrs = $this->magebo->getInvoiceCdrFile($id);
        //print_r($cdr);
        $gprs = $this->magebo->getDataCdr($id);
        // print_r($cdrs);
        //$cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);
        // exit;
        if (!$cdrs) {
            if ($gprs) {
                $gp = "";
            } else {
                return false;
            }
        } else {
            //print_r($cdrs);
            // exit;
            $table = '<h2>CDR Voice & SMS: ' . $pincode . '</h2><br />
            <table><thead>
            <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
            <th width="12%">' . lang('Date') . '</th>
            <th width="10%">' . lang('Number') . '</th>
            <th width="15%">' . lang('Destination') . '</th>
            <th width="42%">' . lang('Description') . '</th>
            <th width="10%">' . lang('Duration') . '</th>
            <th width="10%" align="right">' . lang('Cost') . ' '.lang('Incl. VAT').'</th>
            </tr>
            </thead>
            <tbody>
            ';
            // mail('mail@simson.one','cdr',print_r($cdrs, true));
            foreach ($cdrs as $array) {
                $table .= '
                <tr>
                <td width="12%">' . substr($array->dCallDate, 0, -3) . '</td>
                <td width="10%">' . $array->iPincode . '</td>
                <td width="15%">' . $array->cDialedNumber . '</td>
                <td width="42%">' . $array->cInvoiceGroupDescription.' - ' .$array->cCallTranslation. '</td>
                <td width="10%">' . gmdate("H:i:s", $array->iInvoiceDuration) . '</td>
                <td width="10%" align="right">'.$this->data['setting']->currency . str_replace('.', ',', includevat($array->VATPercentage, $array->mInvoiceSale)) . '</td>
                </tr>
                ';
            }

            $table .= '</tbody></table>';
        }

        if ($gprs) {
            foreach ($gprs as $pincode => $cdr) {
                $gp .= '<h2>CDR DATA: ' . $pincode . '</h2><br />
                <table><thead>
                <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
                <th width="15%">' . lang('Date') . '</th>
                <th width="13%">' . lang('Number') . '</th>
                <th width="10%">' . lang('Type') . '</th>
                <th width="35%">' . lang('Zone') . ' </th>
                <th width="10%">' . lang('Bytes') . '</th>
                <th width="10%" align="right">' . lang('Cost') . ' '.lang('Incl. VAT').'</th>


                </tr>
                </thead>
                <tbody>
                ';

                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp .= '
                    <tr>
                    <td width="15%">' . substr($array->SessionDate, 0, -3) . '</td>
                    <td width="13%">' . $array->iPincode . '</td>
                    <td width="10%">' . $array->Type . '</td>
                    <td width="35%">' . $array->Zone . ' '.$array->RoamingCountry . '</td>
                    <td width="10%">' . formatSizeUnits($array->Bytes) . '</td>
                    <td width="10%" align="right">' . $setting->currency . str_replace('.', ',', includevat('21', $array->PRICE)) . '</td>
                    </tr>
                    ';
                }
                $gp .= '</tbody></table><br /><br /><strong>'.lang('Total Data usage').'  ' . $pincode . ' : ' . formatSizeUnits(array_sum($bytes)) . '</strong>';
                unset($bytes);
            }
        }

        $this->load->library('spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Simson Asuni');
        $pdf->SetTitle('CDR');
        $pdf->setInvoicenumber($id);
        $pdf->SetSubject('CDR Invoice ' . $id);
        $pdf->SetKeywords('TCPDF, Invoice, Cdr, ' . $id);
        $pdf->AddPage('L', 'A4');
        //$pdf->SetFont('droidsans', '', '12');
        //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
        $pdf->ln(10);
        //$pdf->SetFont('droidsans', '', '9');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
        $pdf->SetPrintFooter(false);
        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, "10");

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // print TEXT
        $pdf->SetFont('helvetica', '', '9');
        //  $pdf->PrintChapter(1, lang('Call Detail Records'), $table, true);
        $pdf->writeHTML($table, true, false, true, false, 'J');
        if ($gprs) {
            $pdf->AddPage('L', 'A4');
            //$pdf->SetFont('droidsans', '', '12');
            //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
            $pdf->ln(10);
            $pdf->SetFont('helvetica', '', '9');
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "10");

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            $pdf->writeHTML($gp, true, false, true, false, 'J');
        }
        // print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', FCPATH.'data/chapter_demo_2.txt', true);

        // ---------------------------------------------------------

        //Close and output PDF document
        if (!file_exists($setting->DOC_PATH . $companyid . '/invoices/cdr')) {
            mkdir($setting->DOC_PATH . $companyid . '/invoices/cdr');
        }
        $pdf->Output($setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf', 'F');
        return $setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf';
    }
    public function download_id($id)
    {
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';

        if ($this->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }
        if ($this->Admin_model->IsAllowedInvoice($id, $companyid)) {
            if (!file_exists($invoice_path . $id . '.pdf')) {
                $invoice = $this->Admin_model->getInvoicePdf($id);
                //$file = '/tmp/' . $id . '.pdf';

                file_put_contents($invoice_path . $id . '.pdf', $invoice->PDF);
                //copy($file, $this->data['setting']->DOC_PATH . '/invoices/' . $id . '.pdf');
                //unlink($file);
            }
        } else {
        }
    }
    public function send()
    {
        if (isPost()) {
            //echo json_encode(array('result' => false, 'data' => $_POST)); dcoppieters@delta.nl
            if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
                mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
            }
            $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';
            $invoiceid = $_POST['invoiceid'];
            $invoice = $this->Admin_model->getInvoicedetail($invoiceid);

            $client = $this->Admin_model->getClient($_POST['whmcsid']);

            //wwderijck@zeelandnet.nl
            $res = $this->download_id($invoiceid);
            if (!file_exists($invoice_path . $invoiceid . '.pdf')) {
                $this->download_id($invoiceid);

                echo json_encode(array('result' => true));
            } else {
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);


                //$attachments = getAttachmentEmail('invoice', $client->language, $client->companyid);
                $this->load->library('magebo', array('companyid' => $client->companyid));
                $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
                // $body = $this->load->view('email/content', $this->data, true);
                //$body = str_replace('{$mInvoiceAmount}', number_format($invoice->mInvoiceAmount, 2), $body);
                $payment = $this->magebo->getInvoiceBalance($invoice->iInvoiceNbr, $invoice->iAddressNbr);
                $amount = $invoice->mInvoiceAmount - $payment;
                if ($amount >= 0) {
                    $tpl = "invoice";
                } else {
                    $tpl = "creditnote";
                }

                $body = getMailContent($tpl, $client->language, $client->companyid);
                $body = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
                $body = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
                $body = str_replace('{$clientid}', $client->mvno_id, $body);
                $body = str_replace('{$dInvoiceDate}', $invoice->dInvoiceDate, $body);
                $body = str_replace('{$dInvoiceDueDate}', $invoice->dInvoiceDueDate, $body);
                $body = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
                $body = str_replace('{$name}', format_name($client), $body);
                $body = str_replace('{$cInvoiceReference}', $invoice_ref, $body);

                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $client_email = getInvoiceEmail($client->id);
                $this->email->to($client_email);
                $subject = getSubject($tpl, $client->language, $client->companyid);
                $subject = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $subject);
                $subject = str_replace('{$Companyname}', $this->data['setting']->companyname, $subject);
                $this->email->bcc($this->session->email);
                $cc = getContactInvoiceEmail($client->id);
                if ($cc) {
                    $this->email->cc($cc);
                }
                $this->email->subject($subject);
                $this->email->attach($invoice_path . $invoiceid . '.pdf');
                $this->email->message($body);
                if (!isTemplateActive($client->companyid, 'invoice')) {
                    log_message('error', 'Template invoice is disabled sending aborted');
                    echo json_encode(array('result' => false, 'error' => 'Template invoice is disabled sending aborted'));
                    exit;
                }

                if ($this->email->send()) {
                    if ($cc) {
                        foreach ($cc as $em) {
                            logEmailOut(array(
                            'userid' => $client->id,
                            'to' => $em,
                            'subject' => $subject,
                            'message' => $body,
                            'companyid' => $client->companyid,
                            ));
                        }
                    }
                    logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => $subject, 'message' => $body));

                    echo json_encode(array('result' => true));
                } else {
                    echo json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
                }
            }
        } else {
            echo json_encode(array('result' => false));
        }
    }
    public function send_creditnote()
    {
        if (isPost()) {
            //echo json_encode(array('result' => false, 'data' => $_POST)); dcoppieters@delta.nl
            if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
                mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
            }
            $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';
            $invoiceid = $_POST['invoiceid'];
            $invoice = $this->Admin_model->getCreditnoteDetailsMagebo($invoiceid);

            $client = $this->Admin_model->getClient($_POST['whmcsid']);

            //wwderijck@zeelandnet.nl
            $res = $this->download_id($invoiceid);
            if (!file_exists($invoice_path . $invoiceid . '.pdf')) {
                $this->download_id($invoiceid);

                echo json_encode(array('result' => true));
            } else {
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => $this->data['setting']->smtp_host,
                        'smtp_port' => $this->data['setting']->smtp_port,
                        'smtp_user' => $this->data['setting']->smtp_user,
                        'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                        'mailtype' => 'html',
                        'charset' => 'utf-8',
                        'starttls' => true,
                        'wordwrap' => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset'] = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $body = getMailContent('creditnote', $client->language, $client->companyid);
                $attachments = getAttachmentEmail('creditnote', $client->language, $client->companyid);
                if ($attachments) {
                    foreach ($attachments as $att) {
                        $this->email->attach($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/attachments/' . trim($att));
                    }
                }
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->salutation . ' ' . $client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                $body = str_replace('{$clientid}', $client->mvno_id, $body);
                $body = str_replace('{$iInvoiceNbr}', $invoice->iInvoiceNbr, $body);
                $body = str_replace('{$mInvoiceAmount}', number_format($invoice->mInvoiceAmount, 2), $body);

                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($client->email);
                $this->email->subject(getSubject('creditnote', $client->language, $client->companyid));
                $this->email->bcc($this->session->email);
                $this->email->attach($invoice_path . $invoiceid . '.pdf');
                $this->email->message($body);
                if (!isTemplateActive($client->companyid, 'creditnote')) {
                    log_message('error', 'Template creditnote is disabled sending aborted');
                    echo json_encode(array('result' => false, 'error' => 'Template creditnote is disabled sending aborted'));
                    exit;
                }


                if ($this->email->send()) {
                    logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => getSubject('creditnote', $client->language, $client->companyid), 'message' => $body));

                    echo json_encode(array('result' => true));
                } else {
                    echo json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
                }
            }
        } else {
            echo json_encode(array('result' => false));
        }
    }

    public function downloadcdr()
    {
        $id = $this->uri->segment(4);
        $this->load->library('xlswriter');
        $this->load->library('magebo', array('companyid', $this->session->cid));
        $cdrs = $this->magebo->getInvoiceCdrFileSplit($id);

        $gprs = $this->magebo->getDataCdr($id);

        if (!$gprs && !$cdrs) {
            $this->session->set_flashdata('error', 'no data to be exported');
            redirect('admin/invoice');
            exit;
        }
        $fileName = 'CDR-Summary_invoice_' . $id . '.xlsx';
        /*
        lang('Date') => 'string',
        lang('Type') => 'string',
        lang('Destination') => 'string',
        lang('Duration') => 'string',

        lang('Cost') => 'string',
         */
        $header = array(

            lang('Date') => 'string',
            lang('Destination') => 'string',
            lang('Description') => 'string',
            lang('Duration') => 'string',
            lang('Cost') => 'string',

        );

        $gprs_header = array(

            lang('Date') => 'string',
            lang('Number') => 'string',
            lang('Type') => 'string',
            lang('Zone') => 'string',
            lang('Bytes') => 'string',

        );

        if (count($cdrs) > 0) {
            foreach ($cdrs as $pin => $cdr) {
                foreach ($cdr as $array) {
                    $to[] = array('datum' => substr($array->dCallDate, 0, -3), 'Destination' => $array->cDialedNumber, 'Description' => $array->cInvoiceGroupDescription, 'Duration' => gmdate("H:i:s", $array->iInvoiceDuration), 'Cost' => $this->data['setting']->currency . str_replace('.', ',', includevat('21', $array->mInvoiceSale)));
                }

                $this->xlswriter->writeSheet($to, 'Voice & SMS ' . $pin, $header);
                unset($to);
            }
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        if ($gprs) {
            $gp = array();
            foreach ($gprs as $pincode => $cdr) {
                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp[] = array(
                        'Date' => substr($array->SessionDate, 0, -3),
                        'Number' => $array->iPincode,
                        'Type' => $array->Type,
                        'DuratiZoneon' => $array->Zone,
                        'Bytes' => convertToReadableSize($array->Bytes));
                }
                $gp[] = array(
                    'Date' => '',
                    'Number' => '',
                    'Type' => 'Total Usage: ',
                    'DuratiZoneon' => '',
                    'Bytes' => convertToReadableSize(array_sum($bytes)));
                unset($bytes);

                $this->xlswriter->writeSheet($gp, 'DATA ' . $pincode, $gprs_header);
                unset($gp);
            }
        }
        $this->xlswriter->setAuthor('Simson Parlindungan');
        $this->xlswriter->setTitle('Cdr Export');
        $this->xlswriter->setCompany('Delta');
        echo $this->xlswriter->writeToString();
    }

    public function getDataCdr($id)
    {
        $this->load->library('magebo', array('companyid', '54'));
        $data = $this->magebo->getDataCdr($id);
        print_r($data);
    }
    public function apply_ayment()
    {
        $this->load->model('Admin_model');
        $this->load->library('magebo', array('companyid' => $this->session->cid));

        $postfields = $_POST;

        $response = $this->magebo->apply_payment($postfields);
        if ($response->result) {
            $this->session->set_flashdata('success', lang('Payment has been inserted'));
        } else {
            $this->session->set_flashdata('error', lang('there was an error, please contact admin'));
        }
        if (!$this->magebo->hasUnpaidInvoice($postfields['iAddressNbr'])) {
            $client = getClientDetailidbyMagebo($_POST['iAddressNbr']);
            $this->Admin_model->ChangeClientDunnigProfile($client->id, 'No');
        }
        echo json_encode($response);
    }

    public function testx()
    {
        $this->load->view('invoicex');
    }

    public function disable_reminder()
    {
        $data =array('companyid' => $this->session->cid, 'iInvoiceNbr' => $_POST['iInvoiceNbr'],'flag' => $this->session->firstname.' '.$this->session->lastname);
        $this->db = $this->db->insert('a_reminder_exclude', $data);
        $this->session->set_flashdata('success', 'This invoice wont be sent to customer on reminder 1, and reminder 2 except if he has other invoices which is not flagged and applicable for reminder this invoice will be on the list');
        redirect('admin/invoice/detail/'.$_POST['iInvoiceNbr'].'/'.$_POST['iAddressNbr']);
    }

    public function enable_reminder()
    {
        $data =array('companyid' => $this->session->cid, 'iInvoiceNbr' => $_POST['iInvoiceNbr'],'flag' => $this->session->firstname.' '.$this->session->lastname);

        $this->db = $this->load->database('default', true);
        $this->db->where('iInvoiceNbr', $_POST['iInvoiceNbr']);
        $this->db->delete('a_reminder_exclude');
        $this->session->set_flashdata('success', 'Disable reminder flag has been disabled, but if the time window of reminder has been passes customer wont received reminder anylonger');
        redirect('admin/invoice/detail/'.$_POST['iInvoiceNbr'].'/'.$_POST['iAddressNbr']);
    }
    public function export_invoices()
    {
        $this->load->library('xlswriter');
        $fileName = 'Clients-Invoices_export_' . date('Y-m-d') . '.xlsx';
        $header = array(
            'iInvoiceNbr' => 'string',
            'BILLING ID' => 'string',
            'dInvoiceDate' => 'string',
            'dInvoiceDueDate' => 'string',
            'mInvoiceAmount' => '#,##0.00 [$€-407];[RED]-#,##0.00 [$€-407]',
            'iInvoiceStatus' => 'string',
            'InvoiceType' => 'string',
            'cName' => 'string',

        );

        $links = array(
            'MVNO ID' => 'string',
            'BILLING ID' => 'string',

        );
        $companyid = $this->session->companyid;
        $this->db = $this->load->database('magebo', true);

        $q = $this->db->query("select b.iInvoiceNbr, b.iAddressNbr, CONVERT(char(10), b.dInvoiceDate,126) as dInvoiceDate, CONVERT(char(10), b.dInvoiceDueDate,126) as dInvoiceDueDate, ROUND(b.mInvoiceAmount, 2) as mInvoiceAmount, case when b.iInvoiceStatus  = '52' THEN 'Unpaid'
        when b.iInvoiceStatus = '53' then 'SEPA Presented'
        else 'Paid' end as iInvoiceStatus,
        case when b.iInvoiceType  = '40' THEN 'Invoice'
        else 'Creditnote' end as InvoiceType, a.cName from tblInvoice b left join tblAddress a on b.iAddressNbr=a.iAddressNbr where a.iCompanyNbr=? and dInvoiceDate >= ? and dInvoiceDate <= ? order by b.iInvoiceNbr asc", array($companyid, $_POST['date_start'], $_POST['date_end']));

        if ($q->num_rows() > 0) {
            $res = $q->result_array();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->xlswriter->writeSheet($res, 'Invoices', $header);
            $this->xlswriter->writeSheet($this->getClients($companyid), 'Client Links', $links);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Invoices Export');
            $this->xlswriter->setCompany('United');
            echo $this->xlswriter->writeToString();

            $this->session->set_flashdata('success', 'Downloaded as requested');
        } else {
            $this->session->set_flashdata('error', 'No data found');
        }

        redirect('admin/invoice');
    }

    public function getClients($companyid)
    {
        $this->db = $this->load->database('default', true);
        $q = $this->db->query("select mvno_id,mageboid from a_clients where companyid=? group by mvno_id order by mvno_id asc", array($companyid));
        return $q->result_array();
    }
}
