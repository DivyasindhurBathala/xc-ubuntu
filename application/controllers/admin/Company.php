<?php
defined('BASEPATH') or exit('No direct script access allowed');
class CompanyController extends CI_Controller
{
    protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->companyid = get_companyidby_url(base_url());
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'english');
        }
        if (file_exists(APPPATH.'language/'.$this->session->language.'/'.$this->companyid.'_admin.php')) {
            log_message('error', lang('Using Custom language files'));
            $this->lang->load($this->companyid.'_admin');
        } else {
            $this->lang->load('admin');
        }
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Company extends CompanyController
{
    public function __construct()
    {
        parent::__construct();
        /*
        //Generate Permissions
        $classes = get_class_methods($this);
        if ($this->session->email == "simson.parlindungan@united-telecom.be") {
            foreach ($classes as $class) {
                log_message('error', $class);
                if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
                    $this->db->insert('a_admin_permissions', array('folder' => 'admin', 'controller' => 'agent', 'method' => $class));
                }
            }
        }
        */

        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('Company_model');
        $this->db = $this->load->database('default', true);
        if (!empty($this->session->profiler)) {
            if ($this->session->profiler) {
                $this->output->enable_profiler(true);
                error_reporting(E_ALL);
                ini_set('display_errors', 1);
            }
        }
    }

    public function index()
    {
        $this->load->helper('string');
        $this->data['title']        = lang("Agent List Dashboard");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'show_company';
        $this->data['data']=$this->Company_model->dispchildcompanies($this->session->cid);
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data, $result);
    }


    public function createcompany()
    {
        
        $this->load->helper('string');
        $this->data['title']        = lang("Agent List Dashboard");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'create_company';
        $this->data['data']=$this->Company_model->dispchildcompanies($this->session->cid);
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data, $result);

        if($this->input->post('save'))
        {
        //get form's data and store in local varable
        $n=$this->input->post('company_id');
        $e=$this->input->post('company_name');
        $m=$this->input->post('parent_companyid');
        
        $this->Company_model->savecomprecord($n,$e,$m);     
        echo "Child Company Created Successfully";
         
         }
        
    }


    public function add()

    {

        echo "add";
        $n=$_POST['company_id'];
        $e=$_POST['company_name'];
        $m=$_POST['parent_companyid'];
        $this->Company_model->savecomprecord($n,$e,$m);
        
        redirect('admin/company');
    }
   
        
}