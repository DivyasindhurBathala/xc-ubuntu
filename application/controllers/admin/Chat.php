<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Chat extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *     http://example.com/index.php/welcome
     *  - or -
     *     http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->lang->load('admin');
        header('Content-type: application/json');

        if (!$this->session->id) {
            redirect('admin/auth');
        }
        $this->companyid = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
        $this->load->model('Admin_model');
    }
    public function chatheartbeat()
    {
        print_r($_SESSION);
    }

    public function send_notification()
    {
        $user = $this->encryption->encrypt(base_url().$this->uri->segment(4));
        require APPPATH . 'third_party/pusher/vendor/autoload.php';
        $options = array(
            'cluster' => 'eu',
            'encrypted' => true,
        );
        $pusher = new Pusher\Pusher(
            '09ff32d32ccc7d0cbd67',
            '59b662e3046c448e923d',
            '534092',
            $options
        );
        $data['from'] = 'notification';
        $data['message'] = 'New Portout has been completed by simson';
        $res = $pusher->trigger('chatChannel', md5(base_url().'Simson'), $data);
    }
    public function sendchat()
    {
        $from = $this->session->chatusername;
        $to = $_POST['to'];
        $message = $_POST['message'];


        $res =  $this->session->openChatBoxes;
        $res[$_POST['to']] =  date('Y-m-d H:i:s', time());

        $this->session->set_userdata('openChatBoxes', $res);
        //$_SESSION['openChatBoxes'][$_POST['to']] = date('Y-m-d H:i:s', time());

        $messagesan = $this->sanitize($message);

        if (!isset($_SESSION['chatHistory'][$_POST['to']])) {
            $kol  =  $_SESSION['chatHistory'];
            $kol[$_POST['to']] = '';
            $this->session->set_userdata('chatHistory', $kol);

            //$_SESSION['chatHistory'][$_POST['to']] = '';
        }
        // $this->session->unset_userdata(array('tsChatBoxes' => ))
        if ($this->session->tsChatBoxes[$_POST['to']]) {
            foreach ($this->session->tsChatBoxes as $key => $value) {
                if ($key != $_POST['to']) {
                    $tobeadded[$key] = $value;
                }
            }

            $this->session->set_userdata('tsChatBoxes', $tobeadded);
        }
        // unset($_SESSION['tsChatBoxes'][$_POST['to']]);

        //$sql = "insert into chat (chat.from,chat.to,message,sent) values ('" . mysql_real_escape_string($from) . "', '" . mysql_real_escape_string($to) . "','" . mysql_real_escape_string($message) . "',NOW())";
        $this->db->insert('a_chat', array('from' => $from, 'to' => $to, 'message' => $message, 'sent' => date('Y-m-d H:i:s')));
        /*require APPPATH . 'third_party/pusher/vendor/autoload.php';

        $options = array(
            'cluster' => 'eu',
            'encrypted' => true,
        );
        $pusher = new Pusher\Pusher(
            '09ff32d32ccc7d0cbd67',
            '59b662e3046c448e923d',
            '534092',
            $options
        );

        $data['from'] = $from;
        $data['message'] = $message;
        $pusher->trigger('chatChannel', md5(base_url().$to), $data);
        //$pusher = $this->pusher->get_pusher();
        //$pusher->trigger('chatChannel', $to, array('message' => $from));
        */

        send_chat(array('to' => $to, 'from' => $from, 'message' => $message, 'sent' => date('Y-m-d H:i:s')));
        echo json_encode(array('result' => true, 'myname' => $from));
    }
    public function getOpenBox()
    {
        if (!empty($this->session->id)) {
            $id = $this->session->id;
            $ip = $_SERVER['REMOTE_ADDR'];
            $this->db = $this->load->database('default', true);
            $this->db->where('id', $id);
            $this->db->update('a_admin', array('lastseen' => time(), 'ip' => $ip));
        }
        $open_events = $this->Admin_model->getOpenEvents($this->session->cid);
        $res['events'] = $open_events->count;
        if ($open_events->count) {
            $res['event_items'] = $open_events->items;
        }
        if (!empty($this->session->chatbox)) {
            $res['result'] = true;
           
            $res['users'] = $this->session->chatbox;
        } else {
            $res['result'] = false;
        }
        echo json_encode($res);
    }
    public function gethistory()
    {
        $res =  $_SESSION['chatbox'];
        $res[] = $_GET['with'];
        //$_SESSION['chatbox'][] = $_GET['with'];
        $this->session->set_userdata('chatbox', $res);
        $a = array();
        $b = array();
        $s = $this->db->query("SELECT * FROM `a_chat` where `a_chat`.`from` = ? and `a_chat`.`to` = ? ORDER BY `a_chat`.`id` DESC LIMIT 10", array($this->session->chatusername, $_GET['with']));
        $r = $this->db->query("SELECT * FROM `a_chat` where `a_chat`.`from` = ? and `a_chat`.`to` = ? ORDER BY `a_chat`.`id` DESC LIMIT 10", array($_GET["with"], $this->session->chatusername));

        if ($s->num_rows() > 0) {
            $a = $s->result_array();
        }
        if ($r->num_rows() > 0) {
            $b = $r->result_array();
        }

        $results = array_merge($a, $b);
        $sortArray = array();

        foreach ($results as $result) {
            foreach ($result as $key => $value) {
                if (!isset($sortArray[$key])) {
                    $sortArray[$key] = array();
                }
                $sortArray[$key][] = $value;
            }
        }

        $orderby = "id"; //change this to whatever key you want from the array

        array_multisort($sortArray[$orderby], SORT_ASC, $results);

        echo json_encode($results);
    }
    public function closechat()
    {
        foreach ($this->session->chatbox as $foo) {
            if ($foo != $_POST['chatbox']) {
                $new_session[] = $foo;
            }
        }
        //unset($_SESSION['chatbox']);
        $this->session->unset_userdata('chatbox');
        if (!empty($new_session)) {
            $res =  $this->session->chatbox;
            $res[] = $foo;
            $this->session->unset_userdata('chatbox', $res);
        }
    }
    public function startchatsession()
    {
        $items = '';
        if (!empty($_SESSION['openChatBoxes'])) {
            foreach ($_SESSION['openChatBoxes'] as $chatbox => $void) {
                $items .= $this->chatBoxSession($chatbox);
            }
        }

        if ($items != '') {
            $items = substr($items, 0, -1);
        }

        echo json_encode(array('username' => $this->session->chatusername, 'items' => $items));
    }

    public function chatBoxSession($chatbox)
    {
        $items = array();

        if (isset($_SESSION['chatHistory'][$chatbox])) {
            $items = $_SESSION['chatHistory'][$chatbox];
        }

        return $items;
    }

    public function sanitize($text)
    {
        $text = htmlspecialchars($text, ENT_QUOTES);
        $text = str_replace("\n\r", "\n", $text);
        $text = str_replace("\r\n", "\n", $text);
        $text = str_replace("\n", "<br>", $text);
        return $text;
    }
}
