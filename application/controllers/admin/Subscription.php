<?php
defined('BASEPATH') or exit('No direct script access allowed');
use Box\Spout\Common\Type;

class SubsController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } //!empty($this->session->language)
        else {
            $this->config->set_item('language', 'english');
        }

        $this->companyid       = get_companyidby_url(base_url());
        if (file_exists(APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin_lang.php')) {
            // log_message('error', 'Using Custom language files');
            $this->lang->load($this->companyid.'_admin');
        } else {
            // log_message('error', APPPATH.'language/'.$this->config->item('language').'/'.$this->companyid.'_admin.php');
            $this->lang->load('admin');
        }
        $this->data['setting'] = globofix($this->companyid);
    }
}
class Subscription extends SubsController
{
    public function __construct()
    {
        parent::__construct();
        /*
        $classes = get_class_methods($this);
        foreach ($classes as $class) {
        log_message('error', $class);
        if ($class != 'setProperty' && $class != 'get_instance' && $class != '__construct') {
        $this->db->insert('a_admin_permissions', array('folder' => 'admin', 'controller' => 'subscription', 'method' => $class));
        //$this->db->insert('tarif', array('rolename' => 'superadmin', 'perms' => $this->db->insert_id()));

        }

        }
        */
        if (!empty($this->session->profiler)) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            if ($this->session->profiler) {
                $this->output->enable_profiler(true);
                error_reporting(E_ALL);
                ini_set('display_errors', 1);
            }
        }
        $this->load->model('Admin_model');
        $this->load->model('Teams_model', 'teams');
    }
    public function index()
    {
        $this->data['chartStatus']  = $this->Admin_model->getSubscriptionstatuses($this->session->cid);
        $this->data['stats']        = $this->Admin_model->getSubscriptionActiveByMonth($this->session->cid);
        //$this->data['dtt']          = 'services.js?version=' . time();
        $this->data['billing_stats']        =  $this->Admin_model->get_service_statistic($this->session->cid);
        $this->data['title']        = "Services List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscriptions';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function change_promotion()
    {

            //changeProduct_addon
        echo json_encode($this->Admin_model->changePromotion($_POST));
        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $this->uri->segment(4),
            'serviceid' => $_POST['serviceid'],
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Change Promotion code To promotion ID: '.getPromoname($_POST['id'])
            ));
    }
    public function accept_reseller_changes()
    {
        $id                         = $this->uri->segment(4);
        $serv                       = $this->db->query("select a.terms,a.userid,a.packageid as new_pid,b.packageid as old_pid, a.serviceid, b.companyid, 'Reseller' as requestor, a.date_request, d.msisdn,d.msisdn_sn as sn, '0' as status, '0' as cron_status  from a_reseller_request_changes a left join a_services b on b.id=a.serviceid left join a_products c on c.id=b.packageid left join a_services_mobile d on d.serviceid=a.serviceid where a.id=? and b.companyid=?", array(
            $id,
            $this->companyid
        ));
        $this->data['changes']      = $serv->row();
        $this->data['service']      = $this->Admin_model->getService($serv->row()->serviceid);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'reseller/subscriptions_accept_reseller_request';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function suspended()
    {
        $this->data['title']        = "Suspended Services List";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscriptions_suspended';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function reset_porting_on_demand_counter()
    {
        $id = $_POST['serviceid'];
        $this->Admin_model->ResetCounterPod($id);
        $this->session->set_flashdata('success', 'counter portin on demand has been reset to 0');
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function sim_replacement()
    {
        $this->data['dtt']          = 'sim_replace.js?version=1.0';
        $this->data['title']        = lang("Sim Replacement List");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'sim_replacement';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function detail()
    {
        error_reporting(1);
        $this->data['title'] = "Service Information";
        $id                  = $this->uri->segment(4);

        if (!isAllowed_Service($this->companyid, $id)) {
            $this->session->set_flashdata('error', 'Subscription does not exists');
            redirect('admin/subscription');
        }
        $order                 = $this->Admin_model->getService($id);
        $client                = $this->Admin_model->getClient($order->userid);
        $this->data['service'] = $order;
        if (!$order) {
            $this->session->set_flashdata('error', 'Service id does not exist');
            redirect('admin/subscription');
        } //!$order
        if (!$order) {
            $this->session->set_flashdata('error', 'Serviceid ' . $id . ' '.lang('no longer exist'));
            redirect('admin/subscription');
            exit;
        }
        if (in_array($order->status, array(
            "Cancelled",
            "Terminated"
        ))) {
            $this->session->set_flashdata('error', lang('This subscription has been terminated or cancelled'));
            $this->data['terminated'] = true;
        } elseif (in_array($order->status, array(
            "Active",
            "Suspended"
        ))) {
        } else {
            $this->session->set_flashdata('success', lang('This Service has not been activated,if the simcard has been received by customer you can activate it'));
            redirect('admin/subscription/activate_' . $order->type . '/' . $id);
        }
        if ($this->data['service']->iGeneralPricingIndex) {
            $pricing = explode(',', $this->data['service']->iGeneralPricingIndex);
            foreach ($pricing as $price) {
                if (!$this->Admin_model->getiPricingGeneralIndex($price)) {
                }
            }
        }
        if ($order->type == "xdsl") {
            $this->load->model('Radius_model');
            $this->data['usage'] = $this->Radius_model->getusage($order->details->pppoe_username);
            $this->load->helper('string');
            $this->data['client'] = $client;

            $this->data['xdsl']   = (object) array(
                 'username' => $order->details->pppoe_username,
                 'password' => $order->details->pppoe_password,
                 'ipaddress' => '',
                 'sessions' => false,
                 'offer' => '',
                 'last_auth' => '',
                 'last_auth_status' => '',
                 'status' => 'OFFLINE'
             );


            if ($this->session->email=="simson.parlindungan@united-telecom.be") {
                //echo print_r($order);
                // exit;
            }

            if (!empty($order->details->pppoe_username)) {
                $this->data['xdsl'] = $this->Admin_model->getInternetStatus($order->details->pppoe_username, $order->details->realm);
            }
            $this->data['history'] = $this->Admin_model->GetProximusHistory($id);
            $this->data['invoice'] = $this->Admin_model->getProforma($id);
        } else {
            $this->data['credit']         = "0.00";
            $this->data['terminated']     = false;
            $this->data['product_change'] = hasChangeProductPlanned($id);
            $this->data['mobile_status']  = 0;
            $this->data['client']         = $client;
            $this->data['mobile']         = $order->details;
            // log_message('error', print_r($order->details, 1));
            if ($order->details->platform == "ARTA") {
                if ($order->product_sub_type == "Prepaid") {
                    log_message('error', print_r($this->data['mobile'], 1));

                    $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                    ));
                    $mm                   = $this->artilium->GetSpecificCliInfo($order->details->msisdn_sn);
                    $this->data['credit'] = $mm->Balance;
                }
            }
        }
        if ($this->uri->segment(5) == "json") {
            echo json_encode($this->data);
        } else {
            if ($order->type == "mobile") {
                if ($order->details->platform  == "TEUM") {
                    $this->load->library('pareteum', array(
                    'companyid' => $this->companyid,
                    'api_id' => $this->data['service']->api_id
                                ));


                    if ($this->data['service']->details->divert) {
                        $this->data['divert'] = $this->pareteum->gsmServicesAndDiverts(array('msisdn' => $this->data['service']->details->msisdn));
                    }
                    $this->data['service_info'] = $this->pareteum->accountInformation(array(
                                    'msisdn' => $this->data['service']->details->msisdn
                                ));

                    log_message('error', print_r($this->data['service_info'], true));
                    $this->data['bundles'] = $this->Admin_model->getOptions($this->uri->segment(4));
                    //print_r($this->data['bundles']);
                    $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . service_theme('teum');
                } else {
                    $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . service_theme($order->type);
                }
            } else {
                $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . service_theme($order->type);
            }



            $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
        }
    }
    public function get_pending_msisdn()
    {
        $res      = '';
        $userid   = $_POST['id'];
        $services = $this->Admin_model->getPendingMsisdn($userid);
        if ($services) {
            $re = true;
            foreach ($services as $row) {
                $res .= '<option value="' . $row['id'] . '">' . $row['name'] . ' ' . $row['msisdn'] . '</option>';
            }
        } else {
            $re = false;
        }
        if (count($services) > 0) {
            echo json_encode(array(
                'result' => $re,
                'data' => $res
            ));
        } else {
            echo json_encode(array(
                'result' => $re,
                'request' => $_POST
            ));
        }
    }
    public function add_pricing_extra($id)
    {
        $ids     = array();
        $service = $this->Admin_model->getService($id);
        $client  = $this->Admin_model->getClient($service->userid);
        $option  = $this->Admin_model->getOptions($service->id);
        if ($service->iGeneralPricingIndex) {
            $ids = explode(',', $service->iGeneralPricingIndex);
        }
        $this->load->library('magebo', array(
            'companyid' => $service->companyid
        ));
        if ($option) {
            foreach ($option as $row) {
                if ($row->recurring_total > 0) {
                    //$x = $this->magebo->addExtraPricing($client, $row, str_replace('-', '/', $service->date_contract), $service);
                    if ($this->data['setting']->create_magebo_bundle) {
                        $create = 1;
                    } //$this->data['setting']->create_magebo_bundle
                    else {
                        $create = 0;
                    }
                    $this->magebo->ProcessPricing($service, $client->mageboid, $row->addonid, $create, true, $service->date_contract);
                    $this->Admin_model->updateBundlePricingid($x, $row->id);
                    unset($x);
                }
            }
            //$this->Admin_model->updateBundleID($service->id, implode(',', $ids));
            return implode(',', $ids);
        } else {
            return false;
        }
    }
    public function add_pricing_extra_portal()
    {
        $ids     = array();
        $service = $this->Admin_model->getService($this->uri->segment(4));
        if ($service->iGeneralPricingIndex) {
            $ids = explode(',', $service->iGeneralPricingIndex);
        }
        $client = $this->Admin_model->getClient($service->userid);
        $option = $this->Admin_model->getOptions($service->id);
        $this->load->library('magebo', array(
            'companyid' => $service->companyid
        ));
        if ($option) {
            foreach ($option as $row) {
                if ($row->recurring_total > 0) {
                    $this->magebo->addExtraPricing($client, $row, str_replace('-', '/', $service->date_contract));
                }
            }
            $this->Admin_model->updateBundleID($service->id, implode(',', $ids));
            $this->session->set_flashdata('success', $option->name . ' has been added in to magebo for invoicing');
        } else {
            $this->session->set_flashdata('error', 'This subscription does not have Extra Options');
        }
        redirect('admin/subscription/detail/' . $this->uri->segment(4));
    }
    public function open_reject()
    {
        $msisdn   = $this->uri->segment(4);
        $this->db = $this->load->database('default', true);
        $q        = $this->db->query('select * from a_services_mobile where msisdn=?', array(
            trim($msisdn)
        ));
        //print_r($q->row());
        //exit;
        if ($q->num_rows() > 0) {
            $this->db->query("update a_services_mobile set msisdn_status=? where serviceid=?", array(
                'PortinRejected',
                $q->row()->serviceid
            ));
            redirect('admin/subscription/activate_mobile/' . $q->row()->serviceid . '/found');
        } //$q->num_rows() > 0
        else {
            die('This action has been blocked for this cli, please contact support');
        }
    }
    public function bundle_usage_logs()
    {
        $this->data['title']        = "Usage Monitoring";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'bundle_usage_monitor';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function termination_service_list()
    {
        $this->data['c']            = true;
        //xzxz$this->data['dtt'] = 'termination_services.js?version=' . date('ymdhis');
        $this->data['title']        = lang("Termination Services List");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'termination_services';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function change_call_ceilling()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        //echo json_encode($_POST);
        $order = $this->Admin_model->getService($_POST['serviceid']);
        $this->load->library('artilium', array(
            'companyid' => $order->companyid
        ));
        echo json_encode($this->artilium->CreateAssignment(array(
            'SN' => $_POST['sn'],
            'ActionSetId' => $_POST['SetId'],
            'AssignmentId' => preg_replace('/\D/', '', $_POST['PlanId'])
        )));
    }
    public function download_welcome_letter()
    {
        $this->load->library('pdf');
        $id        = $this->uri->segment(4);
        $service   = $this->Admin_model->getService($id);
        $client    = $this->Admin_model->getClient($service->userid);
        $brand     = $this->Admin_model->getBrandPdfFooter($service->gid);
        $body_data = $this->Admin_model->getWelcomeBody($service->gid, trim($client->language));
        $name      = format_name($client);
        $body      = str_replace('{$name}', trim($name), $body_data->body);
        $body      = str_replace('{$date}', date('d F Y'), $body);
        $body      = str_replace('{$msisdn_sim}', $service->details->msisdn_sim, $body);
        $body      = str_replace('{$ActionDate}', convert_contract_dutch($service->date_contract), $body);
        $body      = str_replace('{$clientid}', $client->mvno_id, $body);
        //$body      = str_replace('{$msisdn}', $service->details->msisdn, $body);
        $body      = str_replace('{$msisdn}', '0' . substr($service->details->msisdn, 2), $body);
        $body      = str_replace('{$msisdn_puk1}', $service->details->msisdn_puk1, $body);
        $body      = str_replace('{$msisdn_puk2}', $service->details->msisdn_puk2, $body);
        $body      = str_replace('{$email}', $this->data['setting']->smtp_sender, $body);
        $body      = str_replace('{$companyname}', $brand->name, $body);
        $body      = str_replace('{$portal_url}', base_url(), $body);
        $body      = str_replace('{$Today}', date('d F Y'), $body);
        //$this->pdf->SetHeaderData($brand->brand_logo, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
        // set auto page breaks
        $this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $this->pdf->setFoot($brand->brand_footer);
        $this->pdf->setFooterFont($body_data->font);
        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $this->pdf->setLanguageArray($l);
        }
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->AddPage(); // pretty self-explanatory
        //$this->pdf->Image(FCPATH . 'assets/img/simcard.png', 130, 228, 70);
        $this->pdf->Image($brand->brand_logo, 10, 10, 60);
        if ($this->session->cid == 54) {
            $this->pdf->Ln(10);
            $this->pdf->setCellPaddings(0, 0, 0, 0);
            $this->pdf->setCellMargins(0, 0, 0, 0);
            $this->pdf->SetFillColor(255, 255, 255);
            $i = $brand->name . "\nJoan Muyskenweg 134\n1114 AN Amsterdam\n";
            $this->pdf->MultiCell(100, 20, $i, 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        }
        $style  = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'text' => true,
            'font' => 'roboto',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $style1 = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'color' => array(
                255,
                255,
                255
            ),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $this->pdf->ln(20);
        $this->pdf->setY(13);
        $this->pdf->setX(130);
        //$this->pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');
        $this->pdf->GetPage();
        $this->pdf->setY(50);
        if (!empty($client->companyname)) {
            $company = $client->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            if ($client->language == "japanese") {
                $contact = $name . "-san\n";
            } else {
                $contact = $name . "\n";
            }
        }
        $add      = format_name_address($client);
        $address1 = str_replace("&#039;", "'", $client->address1. ' '.$client->housenumber.' '.$client->alphabet) . "\n";
        $city     = $client->postcode . " " . $client->city . "\n";
        $country  = $client->country;
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size + 1);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->setCellMargins(0, 0, 0, 0);
        $this->pdf->SetFillColor(255, 255, 255);
        $a = '';
        $this->pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 20, 'T');
        if ($country != "NL") {
            $this->pdf->MultiCell(100, 30, $company . '' . $add . "\n" . $address1 . '' . $city . '' . getCountryName($country) . "\n", 0, 'L', 1, 1, '', '', true, 0, false, true, 30, 'M');
        } else {
            $this->pdf->MultiCell(100, 30, $company . '' . $add . "\n" . $address1 . '' . $city . '' . getCountryName($country) . "\n", 0, 'L', 1, 1, '', '', true, 0, false, true, 30, 'M');
        }
        //$this->pdf->Ln(10);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->writeHTML($body, true, false, true, false, '');
        //$this->pdf->writeHTML(0, 0, '', '', $brand->brand_footer, 0, 0, false, "L", true);
        $this->pdf->Output('Welcome_letter_' . $client->mvno_id . '_' . $service->details->msisdn . '.pdf'); // send the file inline to the browser (default).
    }


    public function download_porting_letter()
    {
        $this->load->library('pdf');
        $id        = $this->uri->segment(4);
        $service   = $this->Admin_model->getService($id);
        $client    = $this->Admin_model->getClient($service->userid);
        $brand     = $this->Admin_model->getBrandPdfFooter($service->gid);
        $body_data = $this->Admin_model->getPdfBody($service->gid, trim($client->language), 'Porting');
        if (!empty($client->salutation)) {
            $name = $client->salutation . " " . $client->firstname . " " . $client->lastname;
        } else {
            $name = $client->firstname . " " . $client->lastname;
        }
        $body = str_replace('{$name}', $name, $body_data->body);
        $body = str_replace('{$date}', date('d F Y'), $body);
        $body = str_replace('{$msisdn_sim}', $service->details->msisdn_sim, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        // $body = str_replace('{$msisdn}', $service->details->msisdn, $body);
        $body = str_replace('{$msisdn}', '0' . substr($service->details->msisdn, 2), $body);
        $body = str_replace('{$msisdn_puk1}', $service->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $service->details->msisdn_puk2, $body);
        $body = str_replace('{$email}', $this->data['setting']->smtp_sender, $body);
        $body = str_replace('{$companyname}', $brand->name, $body);
        $body = str_replace('{$portal_url}', base_url(), $body);
        $body = str_replace('{$ActionDate}', convert_contract_dutch($service->date_contract), $body);
        $body = str_replace('{$date_ported}', $service->date_ported, $body);
        //$this->pdf->SetHeaderData($brand->brand_logo, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
        // set auto page breaks
        $this->pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
        $this->pdf->setFoot($brand->brand_footer);
        $this->pdf->setFooterFont($body_data->font);
        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $this->pdf->setLanguageArray($l);
        }
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->AddPage(); // pretty self-explanatory
        //$this->pdf->Image(FCPATH . 'assets/img/simcard.png', 130, 228, 70);
        $this->pdf->Image($brand->brand_logo, 10, 10, 60);
        $style  = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'text' => true,
            'font' => 'roboto',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $style1 = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'color' => array(
                255,
                255,
                255
            ),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $this->pdf->ln(20);
        //$this->pdf->writeHTML($tblhtml, true, false, false, false, '');
        $this->pdf->setY(13);
        $this->pdf->setX(130);
        //$this->pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');
        $this->pdf->GetPage();
        $this->pdf->setY(50);
        if (!empty($client->companyname)) {
            $company = $client->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            if (!empty($client->firstname)) {
                if ($client->language == "japanese") {
                    $contact = format_name_address($client) . "-san" . "\n";
                } else {
                    $contact = format_name_address($client) . "\n";
                }
            } else {
                if ($client->language == "japanese") {
                    $contact = format_name_address($client) . "-san\n";
                } else {
                    $contact = format_name_address($client) . "\n";
                }
            }
        }
        $address1 = str_replace("&#039;", "'", $client->address1. ' '.$client->housenumber.' '.$client->alphabet) . "\n";
        $city     = $client->postcode . " " . $client->city . "\n";
        $country  = $client->country;
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size + 1);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->setCellMargins(0, 0, 0, 0);
        $this->pdf->SetFillColor(255, 255, 255);
        $a = '';
        $this->pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 20, 'T');
        if ($country != "NL") {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryName($country), 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        } else {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . '' . $city, 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        }
        $this->pdf->Ln(10);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->writeHTML($body, true, false, true, false, '');
        //$this->pdf->writeHTML(0, 0, '', '', $brand->brand_footer, 0, 0, false, "L", true);
        $this->pdf->Output('Welcome_letter_' . $client->mvno_id . '_' . $service->details->msisdn . '.pdf'); // send the file inline to the browser (default).
    }
    public function download_swap_letter()
    {
        $this->load->library('pdf');
        $id        = $this->uri->segment(4);
        $service   = $this->Admin_model->getService($id);
        $client    = $this->Admin_model->getClient($service->userid);
        $brand     = $this->Admin_model->getBrandPdfFooter($service->gid);
        $body_data = $this->Admin_model->getPdfBody($service->gid, trim($client->language), 'Swap');
        if (!empty($client->salutation)) {
            $name = $client->salutation . " " . $client->firstname . " " . $client->lastname;
        } else {
            $name = $client->firstname . " " . $client->lastname;
        }
        $body = str_replace('{$name}', $name, $body_data->body);
        $body = str_replace('{$date}', date('d F Y'), $body);
        $body = str_replace('{$msisdn_sim}', $service->details->msisdn_sim, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        // $body = str_replace('{$msisdn}', $service->details->msisdn, $body);
        $body = str_replace('{$msisdn}', '0' . substr($service->details->msisdn, 2), $body);
        $body = str_replace('{$msisdn_puk1}', $service->details->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $service->details->msisdn_puk2, $body);
        $body = str_replace('{$email}', $this->data['setting']->smtp_sender, $body);
        $body = str_replace('{$companyname}', $brand->name, $body);
        $body = str_replace('{$portal_url}', base_url(), $body);
        //$this->pdf->SetHeaderData($brand->brand_logo, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);
        // set auto page breaks
        $this->pdf->SetAutoPageBreak(false, PDF_MARGIN_BOTTOM);
        $this->pdf->setFoot($brand->brand_footer);
        $this->pdf->setFooterFont($body_data->font);
        // set image scale factor
        $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $this->pdf->setLanguageArray($l);
        }
        $this->pdf->SetPrintHeader(true);
        $this->pdf->SetPrintFooter(true);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->AddPage(); // pretty self-explanatory
        //$this->pdf->Image(FCPATH . 'assets/img/simcard.png', 130, 228, 70);
        $this->pdf->Image($brand->brand_logo, 10, 10, 60);
        $style  = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'text' => true,
            'font' => 'roboto',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $style1 = array(
            'position' => '',
            'align' => 'R',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(
                0,
                0,
                0
            ),
            'bgcolor' => false,
            'color' => array(
                255,
                255,
                255
            ),
            'text' => true,
            'font' => 'droidsans',
            'fontsize' => 8,
            'stretchtext' => 6
        );
        $this->pdf->ln(20);
        $this->pdf->setY(13);
        $this->pdf->setX(130);
        //$this->pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');
        $this->pdf->GetPage();
        $this->pdf->setY(50);
        if (!empty($client->companyname)) {
            $company = $client->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            if (!empty($client->firstname)) {
                if ($client->language == "japanese") {
                    $contact = $client->salutation . " " . $client->firstname . " " . $client->lastname . "-san" . "\n";
                } else {
                    $contact = $client->salutation . " " . $client->firstname . " " . $client->lastname . "\n";
                }
            } else {
                if ($client->language == "japanese") {
                    $contact = $client->salutation . " " . $client->initial . " " . $client->lastname . "-san\n";
                } else {
                    $contact = $client->salutation . " " . $client->initial . " " . $client->lastname . "\n";
                }
            }
            if ($client->language == "japanese") {
                $contact = $contact;
            }
        }
        $address1 = str_replace("&#039;", "'", $client->address1);
        $city     = $client->postcode . " " . $client->city . "\n";
        $country  = $client->country . "\n";
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size + 1);
        $this->pdf->setCellPaddings(0, 0, 0, 0);
        $this->pdf->setCellMargins(0, 0, 0, 0);
        $this->pdf->SetFillColor(255, 255, 255);
        $a = '';
        $this->pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 20, 'T');
        if ($country != "NL") {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . ' ' .$client->housenumber.' '.$client->alphabet. "\n". $city . '' . getCountryName($country), 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        } else {
            $this->pdf->MultiCell(100, 20, $company . '' . $contact . '' . $address1 . ' '.$client->housenumber.' '.$client->alphabet."\n". $city .'' . $city, 0, 'L', 1, 1, '', '', true, 0, false, true, 20, 'M');
        }
        $this->pdf->Ln(10);
        $this->pdf->SetFont($body_data->font, '', $body_data->font_size);
        $this->pdf->writeHTML($body, true, false, true, false, '');
        //$this->pdf->writeHTML(0, 0, '', '', $brand->brand_footer, 0, 0, false, "L", true);
        $this->pdf->Output('Welcome_letter_' . $client->mvno_id . '_' . $service->details->msisdn . '.pdf'); // send the file inline to the browser (default).
    }
    public function cancel_terminate_sim()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        //Array ( [msisdn] => 31610011149 [SN] => 31648924532 [userid] => 902559 [serviceid] => 1802193 [new_simnumber] => 8931162111800023258 [date_cancellation] => now [date] => 2018-12-05 )
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $mobile    = $this->Admin_model->getService($_POST['serviceid']);
        $terminate = $this->artilium->CancelTerminateCLI(trim($_POST['SN']));
        if ($terminate->result == "success") {
            $this->magebo->CancelTerminateRequest($mobile);
            $this->db->where('id', $_POST['serviceid']);
            $this->db->where('companyid', $this->companyid);
            $this->db->update('a_services', array(
                'date_terminate' => null
            ));
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $client->id,
                'serviceid' => $_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Request Termination for ' . $_POST['serviceid'] . ' has been cancelled'
            ));
            $this->session->set_flashdata('success', lang('Cancellation has been voided'));
        } else {
            $this->session->set_flashdata('error', lang('There were error when cancelling this request, please submit ticket support with the serviceid or phonenumber to United telecom'));
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function terminate_sim()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        //Array ( [msisdn] => 31610011149 [SN] => 31648924532 [userid] => 902559 [serviceid] => 1802193 [new_simnumber] => 8931162111800023258 [date_cancellation] => now [date] => 2018-12-05 )
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $terminate_now = false;
        //date_cancellation 2018-09-13T11:18:01.000
        //Array ( [msisdn] => 31628320767 [SN] => 31628320767 [userid] => 800296 [serviceid] => 700469 [new_simnumber] => 8931162111842020296 [date_cancellation] => future [date] => 2018-09-14 )
        $mobile        = $this->Admin_model->getService($_POST['serviceid']);
        $time          = date('H:i:s');
        if ($_POST['date_cancellation'] == "future") {
            $date = $_POST["date"] . 'T00:00:00.000';
            $this->magebo->TerminateRequest($mobile, $_POST["date"] . ' 00:00:00');
            $this->db->where('id', $_POST['serviceid']);
            $this->db->where('companyid', $this->companyid);
            $this->db->update('a_services', array(
                'date_terminate' => $_POST["date"]
            ));
            //$tcli = date($_POST["date"], strtotime("-1 days")) . "T23:59:59";
            $tt = new DateTime($_POST["date"]);
            $tt->sub(new DateInterval('P1D'));
            $tcli = $tt->format('Y-m-d') . "T23:59:59";
            $this->magebo->TerminateComplete($mobile, $_POST["date"]);
        } //$_POST['date_cancellation'] == "future"
        else {
            $terminate_now = true;
            $date          = date('Y-m-d') . 'T' . $time;
            $this->db->where('id', $_POST['serviceid']);
            $this->db->where('companyid', $this->companyid);
            $this->db->update('a_services', array(
                'date_terminate' => date('Y-m-d'),
                'status' => 'Terminated'
            ));
            $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                'datacon_ftp' => 0,
                'date_modified' => date('Y-m-d H:i:s')
            ));
            echo $this->db->affected_rows();
            $this->magebo->TerminateRequest($mobile, date('Y-m-d', strtotime("+1 days")) . "T00:00:00");
            $this->magebo->TerminateComplete($mobile, date('Y-m-d', strtotime("+1 days")));
            $tcli = date('Y-m-d') . "T" . $time;
        }
        $client = $this->Admin_model->getClient($mobile->userid);
        if ($mobile) {
            if ($_POST['date_cancellation'] != "future") {
                $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($_POST['SN']));
                $this->artilium->UpdateServices(trim($_POST['SN']), $pack, '0');
            }
            $terminate = $this->artilium->TerminateCLI(trim($_POST['SN']), $tcli);
            $this->sendmail($_POST['serviceid'], 'service', 'subscription_mobile_terminated', array(
                'terminate_date' => $tcli
            ));
            //mail("mail@simson.one","Terminate_sim",print_r($terminate, true).print_r($_POST, true));
            if ($terminate->result == "success") {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $client->id,
                    'serviceid' => $_POST['serviceid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'SIMCARD for serviceid ' . $_POST['serviceid'] . ' has been requested to be Terminated on '.$date
                ));
                $headers = "From: noreply@united-telecom.be";
                $body    = "Termination request has just been reqested by : " . $this->session->firstname . " \n\nPlease terminate and create prorata for this customer\n\nMsisdn: " . $mobile->details->msisdn . "\nTermination Date: " . $date . "\niAddressNbr: " . $client->mageboid . "\n\nTo Get the list of Termination requests, go to: " . base_url() . "admin/subscription/termination_service_list";
                mail('thierry.van.eylen@united-telecom.be', 'Manual Termination', $body, $headers);
                $this->session->set_flashdata('success', lang('Terminate has been submited'));
            } //$terminate->result == "success"
            else {
                $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
                $body    = "Termination request has just been reqested by : " . $this->session->firstname . " \n\nPlease terminate and create prorata for this customer\n\nMsisdn: " . $mobile->details->msisdn . "\nTermination Date: " . $date . "\niAddressNbr: " . $client->mageboid . "\n\nTo Get the list of Termination requests, go to: " . base_url() . "admin/subscription/termination_service_list";
                mail('thierry.van.eylen@united-telecom.be', 'Manual Terminate not working', $body, $headers);
                $this->session->set_flashdata('error', lang('Terminatation was not successfull'));
            }
        } //$mobile
        else {
            $this->session->set_flashdata('error', lang('Error GetSIM Information'));
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function cancel_portin()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        print_r($_POST);
        //$q = $this->db->query("select * from a_services_mobile where msisdn=?", array($_POST['MSISDN']));
        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
        //$cancel = $this->Admin_model->artiliumPost(array('companyid' => $this->data['setting']->companyid, 'action' => 'CancelPortIn', 'type' => $this->data['mobile']->PaymentType, 'SN' => trim($this->data['sn']->SN)));
        $cancel = $this->artilium->CancelPortIn($_POST['SN']);
        // mail('mail@simson.one','cancel portin', print_r($cancel, true));
        if ($cancel->result == "success") {
            //logClient(array('userid' => $order->userid, 'description' => $this->session->firstname . ' ' . $this->session->lastname . ' ' . lang('Assigned SIMCARD for Order id') . ' #' . $order->orderid));
            $this->session->set_flashdata("success", lang("Cancel Request has been sent, this action can be delayed, so please be patient before requesting new porting to this number"));
        } //$cancel->result == "success"
        else {
            $this->session->set_flashdata("error", $cancel->message);
        }
        redirect('admin/subscription/pendingporting');
    }
    public function change_contract_terms()
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $_POST['serviceid']);
        $this->db->where('userid', $_POST['userid']);
        $this->db->where('companyid', $this->session->cid);
        $this->db->update('a_services', array(
            'contract_terms' => $_POST['contract_terms']
        ));
        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success', lang('Contract has been modified to').' ' . $_POST['contract_terms'] . ' month(s)');
        } else {
            $this->session->set_flashdata('error', lang('Your changes was not submitted'));
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function reloadCredit()
    {



        //echo json_encode($_POST);
        // exit;
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $client  = $this->Admin_model->getService($_POST['userid']);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);
        if ($_POST['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $_POST['serviceid']);
                        break;
                    }
                } while (true);
            }
            if ($agent->reseller_type == "Prepaid") {
                if ($_POST['credit'] > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('Reseller does not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $_POST['serviceid']);
                    redirect('admin/subscription/detail/'.$_POST['serviceid']);
                }
            }
        }
        if ($service) {
            $res = $this->artilium->CReloadCredit($_POST['credit'], $service->details->msisdn_sn, rand(100000000000000, 99999999999999999), 'Ordered by:' . $this->session->firstname);
            if ($res->ReloadResult->Result == 0) {
                if ($_POST['reseller_charge'] == "Yes") {
                    if ($agent->reseller_type == "Prepaid") {
                        $new_balance = $agent->reseller_balance - $_POST['credit'];
                        $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                        unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                    }
                }
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $_POST['userid'],
                    'serviceid' => $_POST['serviceid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'add credit amount of ' . $_POST['credit'] . ' to serviceid: ' . $_POST['serviceid']
                ));
                echo json_encode(array(
                    'result' => true,
                    'reload' => $res
                ));
            } else {
                echo json_encode(array(
                    'result' => false,
                    'data' => $res,
                    'service' => $service
                ));
            }
        } else {
            echo json_encode(array(
                'result' => false
            ));
        }
    }
    public function addPorting()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if (isPost()) {
            if (empty($_POST["serviceid"])) {
                $this->session->set_flashdata('error', lang('Your Serviceid is invalid'));
                redirect('admin/subscription/detail/' . $_POST["serviceid"]);
                exit;
            } //empty($_POST["serviceid"])
            //$this->teams->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " " . print_r($_SESSION, true));
            if (strlen($_POST['MsisdnNumber']) != 11) {
                //$this->teams->send_msg($this->uri->segment(3), "<br /><br /> Response:<br />Your Msisdn number is invalid");
                $this->session->set_flashdata('error', lang('Your Msisdn number is invalid'));
                redirect('admin/subscription/detail/' . $_POST["serviceid"] . '/invalid');
                exit;
            } //strlen($_POST['MsisdnNumber']) != 11
            $relid = $_POST["serviceid"];
            // Change msisdn_type to Porting
            $this->db->where('serviceid', $_POST['serviceid']);
            $this->db->update('a_services_mobile', array(
                'msisdn' => $_POST['MsisdnNumber'],
                'donor_msisdn' => $_POST['MsisdnNumber'],
                'donor_provider' => $_POST['provider'],
                'donor_sim' => $_POST['simnumber'],
                'donor_customertype' => $_POST['customertype1'],
                'donor_type' => $_POST['PortingType'],
                'date_wish' => $_POST['PortInWishDate'],
                'msisdn_type' => 'porting'
            ));
            // activate again service it will pickup the porting request
            $order   = $this->Admin_model->getService($_POST['serviceid']);
            $client  = $this->Admin_model->getClient($_POST['userid']);
            $porting = $this->artilium->PortingNewSIM($client->salutation . '  ' . $client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $order->details);
            //$t = $this->Admin_model->whmcs_api(array('action' => 'ModuleCustom', 'accountid' => $relid, 'func_name' => 'ActivateSim'));
            if ($porting->result == "success") {
                $this->load->library('magebo', array(
                    'companyid' => $this->companyid
                ));
                $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                $this->Admin_model->ChangeStatusOrderID($_POST['serviceid'], 'PortinPending', 'mobile');
                //$this->teams->send_msg($this->uri->segment(3), "<br /><br /> Response:<br />Success");
                //logClient(array('userid' => $_POST['userid'], 'description' => $this->session->firstname . ' ' . $this->session->lastname . ' Activate Order id #' . $_POST['orderid']));
                $this->session->set_flashdata('success', lang('Your Porting request has been submitted and will be executed on') . $_POST['PortInWishDate']);
                //$this->session->set_flashdata('success', lang('Service Has been updated and you can download the files in the customer summary page'));
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $client->id,
                    'serviceid' => $_POST['serviceid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => lag('SIMCARD Data').' ' . json_encode($_POST) . ' '.lang('for serviceid'). ' ' . $_POST['serviceid'] . ' '.lang('has been requested to be ported')
                ));
                redirect('admin/subscription/detail/' . $_POST['serviceid']);
            } //$porting->result == "success"
            else {
                $this->session->set_flashdata('error', lang('Error executing ActivateSim: ') . $porting->message);
                redirect('admin/subscription/detail/' . $_POST["serviceid"]);
            }
        } //isPost()
    }
    public function swap_simcard()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $this->data['mobile'] = $this->Admin_model->getServiceCli($_POST['serviceid']);
        $number               = $this->magebo->getPincodeRef(trim($this->data['mobile']->details->msisdn));
        $swap                 = $this->artilium->SwapSim(array(
            'SN' => trim($_POST['SN']),
            'NewSimNr' => $_POST['new_simnumber']
        ));
        if ($swap->result == "success") {
            $this->artilium->UpdateCLI($sn->SN, 1);
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $this->data['mobile']->userid,
                'serviceid' => $_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'SIMCARD has been successfully Swaped from ' . $this->data['mobile']->details->msisdn_sim . ' to ' . $_POST['new_simnumber']
            ));
            $this->session->set_flashdata('success', 'SimSwap successfully executed');
            if ($_POST['charge'] == "YES") {
                if (!empty($_POST['amount'])) {
                    $amount = $_POST['amount'];
                } //!empty($_POST['amount'])
                else {
                    $amount = getSwapCost($this->data['mobile']->packageid, $this->companyid);
                }
                if ($amount > 0) {
                    $client = $this->Admin_model->getClient($this->data['mobile']->userid);
                    $this->magebo->x_magebosubscription_addPricing($client->mageboid, 359, 1, exvat4($client->vat_rate, $amount), 1, 'Sim Replacement ' . $number . ' - ' . date('d/m/Y'), date('m/d/Y'), date('m'), date('Y'), 1, 0, 'Sim Replacement  ' . $number . ' - ' . date('d/m/Y'), 'Sim Replacement  ' . $number . ' - ' . date('d/m/Y'), 0);
                } //$amount > 0
            } //$_POST['charge'] == "YES"
            $ss = $this->artilium->GetSn($_POST['new_simnumber']);
            $sn = (object) $ss->data;
            $this->Admin_model->updateService($_POST["serviceid"], array(
                'msisdn_swap' => 1,
                'msisdn_puk1' => $sn->PUK1,
                'msisdn_puk2' => $sn->PUK2,
                'msisdn_sim' => trim($_POST['new_simnumber']),
                'msisdn_sn' => trim($sn->SN)
            ));
            $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
            $body    = "";
            $body .= json_encode($_POST) . ' ' . print_r($sn, true);

            redirect('admin/subscription/detail/' . $_POST['serviceid']);
        } //$swap->result == "success"
        else {
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $this->data['mobile']->userid,
                'serviceid' => $_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'SIMCARD  Swaped Failed from ' . $this->data['mobile']->details->msisdn_sim . ' to ' . $_POST['new_simnumber'].' Error Message:'.$swap->message
             ));
            $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
            $body    = "";
            $body .= json_encode($_POST);
            //mail('simson.parlindungan@united-telecom.be', 'Swap Failed', $body, $headers);
            mail('simson.parlindungan@united-telecom.be', 'Swap failed', $body, $headers);
            $this->session->set_flashdata('error', "Swap Error: ".$swap->message);
            redirect('admin/subscription/detail/' . $_POST['serviceid']);
        }
    }
    /*
    public function assign_mobile()
    {
        $id                    = $this->uri->segment(4);
        $this->data['service'] = $s->products->product[0];
        foreach ($this->data['service']->customfields->customfield as $val) {
            $mob[$val->name] = (object) array(
                'id' => $val->id,
                'value' => $val->value
            );
        } //$this->data['service']->customfields->customfield as $val
        if ($this->session->id == "1") {
            //print_r($mob);
        } //$this->session->id == "1"
        $this->data['service_setting'] = (object) $mob;
        if (in_array(trim($this->data['service_setting']->SimStatus->value), array(
            "ActivationRequested",
            "OrderActive",
            "ActivationPending",
            "ActivationError",
            "SimAssigned"
        ))) {
            redirect('admin/subscription/activate_mobile/' . $id);
        } //in_array(trim($this->data['service_setting']->SimStatus->value), array( "ActivationRequested", "OrderActive", "ActivationPending", "ActivationError", "SimAssigned" ))
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscription_assign_mobile';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    */
    public function reject_mobile()
    {
        $order    = $this->Admin_model->getService($_POST['serviceid']);
        $client   = $this->Admin_model->getclient($order->userid);
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $_POST['serviceid']);
        $this->db->update('a_services', array(
            'status' => 'Cancelled',
            'autodelete' => 1,
            'reject_reason' => $_POST['reject_reason']
        ));
        if ($order->details->platform == "TEUM") {

            //FREE SIMCARD TO THE POOL
            $this->Admin_model->update_simcard_reseller($order->details->msisdn, array('serviceid' => null));
        }
        $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array('msisdn_status' => 'Cancelled'));
        $this->session->set_flashdata('success', lang('Order has been Rejected'));
        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $order->userid,
            'serviceid' => $order->id,
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => lang('Service').' : ' . $_POST['serviceid'] . ' '.lang('has been Rejected')
        ));
        $this->sendOrderRecjected($_POST['serviceid'], $client);
        //logClient(array('userid' => $order->userid, 'description' => $this->session->firstname . ' ' . $this->session->lastname . ' ' . lang('Rejected order id') . '#' . $order->orderid));
        redirect('admin/subscription/pendingorders');
    }
    public function reject_mobile_pending()
    {
        $id       = $this->uri->segment(4);
        $order    = $this->Admin_model->getOrderid($id);
        $this->db = $this->load->database('default', true);
        $o        = $this->db->query("select * from tblhosting where id=?", array(
            $id
        ));
        if ($o->row()->domainstatus == "Pending") {
            $this->db->where('id', $order->orderid);
            $this->db->update('tblorders', array(
                'status' => 'Rejected'
            ));
            $this->Admin_model->reject_hosting($id);
            $this->Admin_model->reject_hostingaddons($id);
            $this->session->set_flashdata('success', lang('Order has been Cancelled'));
            logAdmin(array(
                'name' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Service : ' . $id . ' has been Rejected'
            ));
            logClient(array(
                'userid' => $order->userid,
                'description' => $this->session->firstname . ' ' . $this->session->lastname . ' ' . lang('Cancelled order id') . ' #' . $order->orderid
            ));
            $client = $this->Admin_model->getclient($order->userid);
            $this->sendOrderRecjected($id, $client);
            redirect('admin/subscription');
        } //$o->row()->domainstatus == "Pending"
    }
    public function cancelUpgradeChanges()
    {
        $this->db->where('serviceid', $_POST['serviceid']);
        $this->db->delete('a_subscription_changes');
        echo json_encode(array(
            'result' => $this->db->affected_rows()
        ));
        //$this->Admin_model->update_services($_POST['serviceid'], array(''))
    }
    public function export_subscription()
    {
        $this->load->library('xlswriter');
        $fileName  = 'Subscription-Summary_by_Date_' . date('Y-m-d') . '.xlsx';
        $header    = array(
            'SubscriptionID' => 'integer',
            'Status' => 'string',
            'ArtiliumID' => 'integer',
            'MvnoID' => 'string',
            'Recurring' => 'string',
            'DateContract MM-DD-YYYY' => 'string',
            'DateCreated' => 'string',
            'Companyname' => 'string',
            'ProductName' => 'string',
            'Identifiers' => 'string',
            'OrderStatus' => 'string',
            'PackageId' => 'integer',
            'Agent' => 'string'
        );
        $companyid = $this->session->cid;
        $this->db  = $this->load->database('default', true);
        $query     = "select a.id,a.status,a.userid,c.mvno_id,a.recurring as recurring,date_contract,a.date_created,concat(c.firstname,' ', c.lastname, ' ', companyname)  as clientname, b.name as packagename,
    case when a.type  = 'mobile' THEN d.msisdn
    when a.type = 'xdsl' then e.circuitid
    when a.type = 'voip' then f.cli
    else 'Unknown' end as domain, case when a.type  = 'mobile' THEN d.msisdn_status
    when a.type = 'xdsl' then e.status
    when a.type = 'voip' then f.status
    else 'Unknown' end as orderstatus,a.packageid,g.agent
 from a_services a
 left join a_products b on b.id=a.packageid
 left join a_clients c on c.id=a.userid
 left join a_clients_agents g on c.agentid=g.id
 left join a_services_mobile d on d.serviceid=a.id
 left join a_services_xdsl e on e.serviceid=a.id
 left join a_services_voip f on f.serviceid=a.id
 where a.status in ('Pending','Active')
 and a.companyid = " . $companyid . "
 group by a.id";
        $q         = $this->db->query($query);
        if ($q->num_rows() > 0) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');
            $this->xlswriter->writeSheet($q->result_array(), 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('Subscription Export');
            $this->xlswriter->setCompany('United');
            echo $this->xlswriter->writeToString();
            $this->session->set_flashdata('success', 'Downloaded as requested');
        } else {
            $this->session->set_flashdata('error', 'No data found');
        }
        redirect('admin/subscription');
    }
    public function addSim()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $mobile = $this->Admin_model->getServiceCli($this->uri->segment(4));
        $addsim = $this->magebo->AddSIMToMagebo($mobile);
        redirect('admin/subscription/detail/' . $this->uri->segment(4));
    }
    public function addSimPorting()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $order  = $this->Admin_model->getServiceCli($this->uri->segment(4));
        $addsim = $this->magebo->AddPortinSIMToMagebo($order);
        $this->load->model('Api_model');
        $this->Api_model->PortIngAction1($order);
        $this->Api_model->PortIngAction2($order);
        $this->Api_model->PortIngAction3($order);
        $this->Api_model->PortIngAction4($order);
        $this->Api_model->PortIngAction5($order->companyid);
        redirect('admin/subscription/detail/' . $this->uri->segment(4));
    }
    public function activate_xdsl()
    {
        $id = $this->uri->segment(4);
        $this->load->helper('string');
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $order                      = $this->Admin_model->getService($id);
        $client                     = $this->Admin_model->getClient($order->userid);
        $this->data['service']      = $order;
        $this->data['client']       = $client;
        $this->data['history']      = $this->Admin_model->GetProximusHistory($id);
        $this->data['invoice']      = $this->Admin_model->getProforma($id);
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscription_activate_xdsl';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function ConvertADSL2VDSL()
    {

        //print_r($_POST);
        $price = getPriceRecurring($_POST['packageid'], 1);
        if (!$price) {
            $price = "0";
        }
        $this->db->query('update a_services set packageid=?, recurring=? where id=?', array($_POST['packageid'],$price, $_POST['serviceid']));
        log_message('error', 'Convert Change Price :'.$this->db->last_query());
        $order = $this->Admin_model->getService($_POST['serviceid']);
        //print_r($order);
        $client = $this->Admin_model->getClient($_POST['userid']);

        if ($order->details->proximus_orderid) {
            if (trim($_POST['productname']) == "Carrier VDSL2 GO") {
                $product_id = "CKHHJ";
            } elseif (trim($_POST['productname']) == "Carrier VDSL2 START") {
                $product_id = "CIWDJ";
            } elseif (trim($_POST['productname']) == "Carrier VDSL2 PLUS") {
                $product_id = "PQOJO";
            } elseif (trim($_POST['productname']) == "Carrier VDSL2 LIGHT") {
                $product_id = "MFUPG";
            }


            $this->Admin_model->update_services_data('xdsl', $_POST['serviceid'], array('product_name' => $_POST['productname'], 'product_id'=> $product_id, 'appointment_date_requested' => $_POST['request_date']));
            $convert_data = array(
                'method' => 'Convert2VDSL',
                'action' => 'order',
                'ut_orderid' => $_POST['serviceid'],
                'productid' => $product_id,
                'circuitid' => $order->details->circuitid,
                'ProductName' => $_POST['productname'],
                'requestedDate' => $_POST['request_date'],
                'orderid' => $order->details->proximus_orderid,
                'firstname' => $client->firstname,
                'lastname' => $client->lastname,
                'language' => 'FR',
                'phonenumber' => $client->phonenumber,
                'cycle' => 'CORRECTION',
                'agent' => $this->session->firstname.' '.$this->session->lastname);

            $res = $this->Admin_model->proximus_api($convert_data);
            log_message('error', "Convert Data :\n".print_r($convert_data, 1));
            if ($res->result == "success") {
                $this->session->set_flashdata('success', 'new request has been sent to proximus');
                $this->Admin_model->update_services_data('xdsl', $_POST['serviceid'], array('requestid' => $res->requestid));
            }
        } else {
            $res = $this->Admin_model->proximus_api(array('method' => 'FindGeographicLocation', 'action' => 'preordering', 'circuitid' => trim($order->domain)));

            if (!empty($order->domain)) {
                if ($res->result == "success") {
                    if (trim($_POST['productname']) == "Carrier VDSL2 GO") {
                        $product_id = "CKHHJ";
                    } elseif (trim($_POST['productname']) == "Carrier VDSL2 START") {
                        $product_id = "CIWDJ";
                    } elseif (trim($_POST['productname']) == "Carrier VDSL2 PLUS") {
                        $product_id = "PQOJO";
                    } elseif (trim($_POST['productname']) == "Carrier VDSL2 LIGHT") {
                        $product_id = "MFUPG";
                    }
                    $this->Admin_model->update_services_data('xdsl', $_POST['serviceid'], array('ordercycle' => 'NEW', 'requestid' => $res->orderid, 'product_name' => $_POST['productname'], 'product_id' => $product_id,'appointment_date_requested' => $_POST['request_date'] ));
                    sleep(3);
                    set_time_limit(50);
                    do {
                        $ordering =  $this->Admin_model->getProximusOrder($res->orderid);
                        log_message('error', 'Trying to fetch order proximus Result: '.print_r($ordering, 1));
                        sleep(3);
                        if ($ordering) {
                            if($ordering->result != "success"){
                                $orderid = false;
                                //die('Proximus Feedback:'.$ordering->data->interation->feedback->description);
                                break;
                            }else{
                                $orderid = $ordering->orderid;
                                break;
                            }

                        }
                    } while (true);
                    if(!$orderid){
                        $this->session->set_flashdata('error', 'Proximus Feedback :'.$ordering->data->interaction->feedback->description);
                        redirect('admin/subscription/detail/'.$_POST['serviceid']);
                    }

                    $this->Admin_model->update_services_data('xdsl', $_POST['serviceid'], array('ordercycle' => 'NEW','proximus_orderid' => $orderid,  'requestid' => $res->orderid, 'product_name' => $_POST['productname']));
                    $service = $this->Admin_model->getService($_POST['serviceid']);
                    $order_data = array(
                        'method' => 'Convert2VDSL',
                        'action' => 'order',
                        'ut_orderid' => $_POST['serviceid'],
                        'productid' => $product_id,
                        'circuitid' => $order->details->circuitid,
                        'ProductName' => $_POST['productname'],
                        'requestedDate' => $_POST['request_date'],
                        'orderid' => $orderid,
                        'firstname' => $client->firstname,
                        'lastname' => $client->lastname,
                        'language' => 'NL',
                        'phonenumber' => $client->phonenumber,
                        'cycle' => 'NEW',
                        'agent' => $this->session->firstname.' '.$this->session->lastname);
                    $res = $this->Admin_model->proximus_api($order_data);
                    log_message('error', "Order Data :\n".print_r($order_data, 1));
                    if ($res->result == "success") {
                        $this->session->set_flashdata('success', 'new request has been sent to proximus');
                        $this->Admin_model->update_services_data('xdsl', $_POST['serviceid'], array('requestid' => $res->requestid));
                    } else {
                        $this->session->set_flashdata('error', 'Issue requesting proximus');
                    }
                    //redirect('admin/subscription/detail/'.$_POST['serviceid']);
                }
            } else {
                $this->session->set_flashdata('error', 'Serviceid Not found');
                //redirect('admin/subscription/detail/'.$_POST['serviceid']);
            }
        }

        redirect('admin/subscription/detail/'.$_POST['serviceid']);
    }
    public function assign_proximus_package()
    {
        $order = $this->Admin_model->getService($_POST['serviceid']);
        //print_r($order);
        //exit;
        foreach ($order->xdsl as $row) {
            if (trim($row->productid) == trim($_POST['productname'])) {
                $this->db->query('update a_services_xdsl set product_name=?, product_id=? where serviceid=?', array(
                    $row->product_name,
                    $row->productid,
                    $_POST['serviceid']
                ));
            }
        }
        $this->session->set_flashdata('success', 'Product has been assigned: ' . $_POST['productname']);
        redirect('admin/subscription/activate_xdsl/' . $_POST['serviceid']);
    }
    public function move_subscription()
    {
        if (!empty($_POST['userid']) && !empty($_POST['serviceid'])) {
            $service = $this->Admin_model->getService($_POST['serviceid']);
            if (!$service) {
                $this->session->set_flashdata('error', 'Client ' . $_POST['userid'] . ' not found');
                redirect('admin/subscription/detail/' . $_POST['serviceid']);
            }
            logAdmin(array(
                'companyid' => $this->companyid,
                'serviceid' => $_POST['serviceid'],
                'userid' => $_POST['olduserid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Service : ' . $_POST['serviceid'] . ' '.lang('has been moved to customer id').' ' . $_POST['userid']
            ));
            logAdmin(array(
                'companyid' => $this->companyid,
                'serviceid' => $_POST['serviceid'],
                'userid' => $_POST['userid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Service : ' . $_POST['serviceid'] . ' '.lang('imported from customer').' ' . $_POST['olduserid']
            ));
            $_POST['companyid'] = $this->companyid;

            $client                = $this->Admin_model->getClient($_POST['userid']);

            $this->Admin_model->MoveSubscription($_POST);
            //$this->send_TicketUnited($_POST['serviceid'], $_POST['olduserid']);
            $mobile                = $this->Admin_model->getServiceCli($_POST['serviceid']);
            //$client                = $this->Admin_model->getClient($mobile->userid);

            $oldclient             = $this->Admin_model->getClient($_POST['olduserid']);
            $body = "Hello,\nCustomer has moved subscription from \niAddressNbr: " . $oldclient->mageboid . "\nNew iAddressNbr: " . $client->mageboid . "\nMsisdn : " . $mobile->details->msisdn . "\nDate : " . date('Y-m-d') . "\n\nPlease Process it in Magebo.\n\n Regards, \MVNO Portal\n";


            if ($this->data['setting']->create_arta_contact) {
                $this->load->library('artilium', array('companyid' => $this->companyid));
                $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array('msisdn_contactid' => $client->ContactType2Id));
                $this->artilium->MoveCLI($mobile->details->msisdn_sn, $client->ContactType2Id, 1);
            }
            if ($this->data['setting']->mage_invoicing) {
                $ticket =   $this->Admin_model->createWHMCSticket(array('deptid' => 11, 'name' => $this->session->firstname.' '.$this->session->lastname, 'email' => $this->session->email,'subject' => 'Please Move CLI: ' . $mobile->details->msisdn . ' to Mageboid/iAddressNbr: ' . $client->mageboid,'message' => $body));
                log_message('error', print_r($ticket, true));
            }
            $this->session->set_flashdata('success', lang('subscription has been moved to').': ' . $_POST['userid']);
        } else {
            $this->session->set_flashdata('error', lang('Error moving subscription'));
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function send_TicketUnited($id, $olduserid)
    {
        $this->load->model('Admin_model');
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $oldclient             = $this->Admin_model->getClient($olduserid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->data['setting']->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'text';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = "Hello,\nCustomer has moved subscription from \niAddressNbr: " . $oldclient->mageboid . "\nNew iAddressNbr: " . $client->mageboid . "\nMsisdn : " . $mobile->details->msisdn . "\nDate : " . date('Y-m-d') . "\n\nPlease Process it in Magebo.\n\n Regards, \MVNO Portal\n";
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to('ticket@mvno.ticket.united-telecom.be');
        $this->email->subject('Please Move CLI: ' . $mobile->details->msisdn . ' to Mageboid/iAddressNbr: ' . $client->mageboid);
        //$this->email->bcc('mail@simson.one');
        $this->email->message($body);
        if ($this->email->send()) {
            logEmailOut(array(
                'userid' => $client->id,
                'to' => 'ticket@mvno.ticket.united-telecom.be',
                'subject' => 'Please Move CLI: ' . $mobile->details->msisdn . ' to Mageboid/iAddressNbr: ' . $client->mageboid,
                'message' => $body
            ));
        }
    }

    public function info()
    {
        print_r($this->Admin_model->getService($this->uri->segment(4)));
    }
    public function activate_mobile()
    {
        $this->load->helper('string');
        $this->load->helper('mq');
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        if (!empty($_POST['action'])) {
            $id = $_POST['serviceid'];
            $this->teams->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " " . print_r($_SESSION, true));
            $this->Admin_model->ChangeStatusService($_POST['serviceid'], 'Pending');
            $this->Admin_model->ChangeStatusOrderID($_POST['serviceid'], 'OrderWaiting', 'mobile');
            if (!empty($_POST['msisdn_sim'])) {
                $fs = $this->db->query("select * from a_services_mobile where msisdn_sim=? and serviceid != ?", array(
                    $_POST['msisdn_sim'],
                    $_POST['serviceid']
                ));
                if ($fs->num_rows() > 0) {
                    $this->session->set_flashdata('error', 'Simcard: ' . $_POST['msisdn_sim'] . ' '.lang('has been asigned to another services').': ' . $fs->serviceid . ' this action has been denied');
                    redirect('admin/dashboard');
                }
                $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                    'msisdn_sim' => $_POST['msisdn_sim'],
                    'date_modified' => date('Y-m-d H:i:s')
                ));
            }
            send_growl(array(
                'message' => $this->session->firstname . ' ' . $this->session->lastname . ' has assign simcard ' . $_POST['msisdn_sim'] . ' to  service id: #' . $_POST['serviceid'],
                'companyid' => $this->session->cid
            ));
            logAdmin(array(
                'companyid' => $this->companyid,
                'serviceid' => $_POST['serviceid'],
                'userid' => $_POST['userid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => lang('Order has been accepted and Service').' : #' . $_POST['serviceid'] . ' assigned Simcard: ' . $_POST['msisdn_sim']
            ));
            $order  = $this->Admin_model->getService($id);
            if ($order->details->platform == "ARTA") {
                $client = $this->Admin_model->getClient($order->userid);
            } else {
                $sim = simcardExist(trim($_POST['msisdn_sim']));
                if ($sim) {
                    $this->db->query("update a_reseller_simcard set serviceid=? where simcard=?", array(
                        $_POST['serviceid'],
                        trim($_POST['msisdn_sim'])
                    ));
                    $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                        'msisdn_sim' => $_POST['msisdn_sim'],
                        'msisdn' => $sim->MSISDN,
                        'platform' => 'TEUM',
                        'msisdn_pin' => $sim->PIN1,
                        'msisdn_sn' => $sim->MSISDN,
                        'msisdn_puk1' => $sim->PUK1,
                        'msisdn_puk2' => $sim->PUK2,
                        'date_modified' => date('Y-m-d H:i:s')
                    ));
                }
                $client = $this->Admin_model->getClient($order->userid);
            }
            redirect('admin/subscription/detail/' . $_POST['serviceid']);
            exit;
        } else {
            unset($_POST['promotion_code']);
            if (!empty($this->uri->segment(5))) {
                if ($this->uri->segment(5) == "force") {
                    $this->Admin_model->ChangeStatusService($this->uri->segment(4), 'Active');
                    $this->Admin_model->ChangeStatusOrderID($this->uri->segment(4), 'Active', 'mobile');
                }
            }
            $next = true;
            $id   = $this->uri->segment(4);
            if (!isAllowed_Service($this->companyid, $id)) {
                $this->session->set_flashdata('error', lang('Subscription does not exists'));
                redirect('admin/subscription');
            }
            $order = $this->Admin_model->getService($id);
            //disallow double simcard for secure
            if (!$this->Admin_model->check_double_sim($order->details->msisdn_sim)) {
                $this->session->set_flashdata('error', lang('This simcard').' ' . $order->details->msisdn_sim . ' '.lang('does not pass validation process, please contact it@united-telecom.be, we detect that this simcard is used on other CLI'));
                redirect('admin/client/detail/' . $order->userid);
            }

            //mail('mail@simson.one','activate_mobile status', print_r($status, true));
            if ($order->details->platform == "ARTA") {
                $this->load->library('artilium', array(
                    'companyid' => $this->companyid
                ));
                $status = $this->artilium->GetParametersCLI($order->details->msisdn_sn);
                if ($status->ParameterResult->Result == "0") {
                    $cnt = 0;
                    if (is_array($status->ParameterResult->Parameters->Parameter)) {
                        foreach ($status->ParameterResult->Parameters->Parameter as $st) {
                            if ($st->ParameterId == 20515) {
                                $platform_status = getPortinStatus($st->ParameterValue);

                                if ($order->details->msisdn_type == "porting") {
                                    $this->Admin_model->update_services_data('mobile', $id, array(
                                        'msisdn_status' => $platform_status,
                                        'date_modified' => date('Y-m-d H:i:s')
                                    ));
                                }

                                if ($platform_status == "PortinPending") {
                                    $cnt = $cnt + 1;
                                }
                            }
                            if ($st->ParameterId == 20526) {
                                if (!empty($st->ParameterValue)) {
                                    $cnt = $cnt + 1;
                                }
                            }
                        }
                        if ($cnt == 2) {
                            $this->Admin_model->update_services_data('mobile', $id, array(
                                'msisdn_status' => 'PortinAccepted',
                                'date_modified' => date('Y-m-d H:i:s')
                            ));
                        }
                    }
                    $order = $this->Admin_model->getService($id);
                }
            }
            if ($order->status == "Active") {
                redirect('admin/subscription/detail/' . $id);
            } //$order->status == "Active"
            elseif ($order->status == "Cancelled") {
                $this->session->set_flashdata('error', lang('This order has been cancelled, access to it is now disabled'));
                redirect('admin/subscription');
            } //$order->status == "Cancelled"
            if (in_array($order->details->msisdn_status, array(
                "PortinRejected",
                "PortInValidateVerificationCodeRejected",
                "PortinCancelled",
                "PortinFailed",
                "ActivationError"
            ))) {
                $noAddSim = true;
            } //$order->details->msisdn_status == "PortinRejected"
            else {
                $noAddSim = false;
            }
            $client = $this->Admin_model->getClient($order->userid);
            if ($order->details->platform == "ARTA") {
                if (empty($order->details->msisdn_sn)) {
                    if (trim($order->details->msisdn_sim) != '1111111111111111111') {
                        if (!empty(trim($order->details->msisdn_sim))) {
                            $test = $this->artilium->getSn(trim($order->details->msisdn_sim));
                            $this->Admin_model->update_services_data('mobile', $id, array(
                                'msisdn_sn' => trim($test->data->SN),
                                'msisdn_imsi' => $test->data->IMSI,
                                'msisdn_puk2' => $test->data->PUK2,
                                'msisdn_puk1' => $test->data->PUK1,
                                'msisdn_languageid' => setVoiceMailLanguageByClientLang($client->language),
                                'date_modified' => date('Y-m-d H:i:s')
                            ));
                            $this->session->set_flashdata('success', lang('Serial Number has been updated..'));
                            $order = $this->Admin_model->getService($id);
                        } //!empty(trim($order->details->msisdn_sim))
                    } //trim($order->details->msisdn_sim) != '1111111111111111111'
                } //empty($order->details->msisdn_sn)
            }
            if ($order->status == "New") {
                $this->session->set_flashdata('error', lang('Please accept this order ID: ') . ' ' . $id . ' ' . lang('before activate or assign SIMCARD'));
                redirect('admin/subscription/pendingorders');
            } //$order->status == "New"
            if (empty($id)) {
                $this->session->set_flashdata('error', lang('Service id required'));
                redirect('admin/subscription');
            } //empty($id)
            if (isPost()) {
                log_message('error', print_r(array_merge(array('Action' => 'Activation'), $_POST), true));

                $this->teams->send_msg($this->uri->segment(3), "<br /><br /> Request:<br />" . print_r($_POST, true) . " By" . $this->session->firstname . ' ' . $this->session->firstname);
                $service_teum = $this->Admin_model->getService($_POST['serviceid']);
                if ($service_teum->details->platform == "TEUM") {
                    // print_r($service_teum);
                    // exit;
                    $this->load->library('pareteum', array(
                        'companyid' => $this->companyid,
                        'api_id' => $service_teum->api_id
                    ));
                    if (empty($service_teum->details->teum_accountid)) {
                        $account = array(
                            "AccountInfo" => array(
                                "AccountType" => "Prepaid",
                                "CustomerId" => (string) $client->teum_CustomerId,
                                "ExternalAccountId" => (string)$_POST['serviceid'].''.rand(10000000, 99999999),
                                "AccountStatus" => "Active",
                                "Names" => array(
                                    array(
                                        "LanguageCode" => "eng",
                                        "Text" => "Account"
                                    )
                                ),
                                "Descriptions" => array(
                                    array(
                                        "LanguageCode" => "eng",
                                        "Text" => "Account"
                                    )
                                ),
                                "AccountCurrency" => "GBP",
                                "Balance" => 0,
                                "CreditLimit" => 0
                            )
                        );
                        $acct    = $this->pareteum->CreateAccount($account);
                        log_message('error', print_r($acct, true));
                        if ($acct->resultCode == "0") {
                            $AccountId = $acct->AccountId;

                            $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array('teum_accountid' => $AccountId));
                        } else {
                            die('Error when creating Account');
                        }
                    } else {
                        $AccountId = $service_teum->details->teum_accountid;
                    }


                    $addons = getaddons_teum($_POST['serviceid'], $service_teum->details->msisdn_sn);
                    $subs   = array(
                        "CustomerId" => (int) $client->teum_CustomerId,
                        "Items" => array(
                            array(
                                "AccountId" => (string) $AccountId,
                                "ProductOfferings" => $addons,
                                "ServiceAddress" => array(
                                    "Address" => $client->address1,
                                    "HouseNo" => $client->housenumber,
                                    "City" => $client->city,
                                    "ZipCode" => $client->postcode,
                                    "State" => "unknown",
                                    "CountryId" => "76"
                                )
                            )
                        ),
                        "channel" => "UnitedPortal V1 by ".$this->session->firstname
                    );
                    log_message('error', print_r($subs, true));
                    $subscription = $this->pareteum->AddSubscription($subs);
                    log_message('error', 'AddSubscription'.print_r($subscription, true));
                    if ($subscription->resultCode == "0") {
                        $this->db->query("update a_reseller_simcard set SubscriptionId=? where serviceid=?", array($subscription->Subscription->SubscriptionId, $_POST['serviceid']));
                        //mme
                        $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array('teum_customerid' => $client->teum_CustomerId, 'teum_subscriptionid' => $subscription->Subscription->SubscriptionId));
                        foreach ($subscription->Subscription->Products as $key => $row) {
                            foreach ($addons as $key => $r) {
                                if ($r['ProductOfferingId'] == $row->ProductOfferingId) {
                                    $addx = getAddonsbyBundleID($row->ProductOfferingId);
                                    $days  = ($addx->teum_autoRenew+1)*30;
                                    $this->Admin_model->updateAddonTeum($_POST['serviceid'], $row->ProductOfferingId, array(
                                        'companyid' => $this->companyid,
                                        'teum_DateStart' => date('Y-m-d'),
                                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                                        'teum_DateEnd' => getFuturedate(date('Y-m-d'), 'day', $days),
                                        'teum_CustomerOrderId' => $subscription->CustomerOrderId,
                                        'teum_SubscriptionId' => $subscription->Subscription->SubscriptionId,
                                        'teum_ProductId' => $row->ProductId,
                                        'teum_ProductChargePurchaseId' => $row->ProductChargePurchaseId,
                                        'teum_SubscriptionProductAssnId' => $row->SubscriptionProductAssnId,
                                        'teum_ServiceId' => $subscription->Subscription->Services[$key]->ServiceId
                                    ));
                                    if ($key == "0") {
                                        $this->db->query("update a_reseller_simcard set TeumServiceId=?,CustomerOrderId=? where serviceid=?", array($subscription->Subscription->Services[$key]->ServiceId,$subscription->CustomerOrderId, $_POST['serviceid']));
                                    }
                                }
                            }
                        }





                        if ($order->details->msisdn_type =="porting") {
                            $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                            'msisdn_status' => 'PortinPending',
                            'date_modified' => date('Y-m-d H:i:s'),
                            'teum_accountid' => $AccountId
                            ));
                        } else {
                            $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                            'msisdn_status' => 'Active',
                            'date_modified' => date('Y-m-d H:i:s'),
                            'teum_accountid' => $AccountId
                            ));
                        }



                        $this->db->query("update a_services set status='Active' where id=?", array(
                            $_POST['serviceid']
                        ));
                        $this->session->set_flashdata('success', lang('Activation has been Requested'));

                        send_growl(array(
                            'message' => $this->session->firstname . ' ' . $this->session->lastname . ' activate Number: ' . $order->details->msisdn,
                            'companyid' => $this->session->cid
                        ));
                        $service = $this->Admin_model->getService($_POST['serviceid']);
                        $addonlist_nobase = getAddonsbySericeidNo_Base($_POST['serviceid']);
                        if ($addonlist_nobase) {
                            $this->Admin_model->insertTopup(
                                array(
                                   'companyid' => $this->companyid,
                                   'serviceid' => $_POST['serviceid'],
                                   'userid' =>  $service->userid,
                                   'income_type' => 'bundle',
                                   'agentid' => $service->agentid,
                                   'amount' => $addonlist_nobase->recurring_total,
                                   'bundle_name' => $addonlist_nobase->name,
                                   'user' => $this->session->firstname . ' ' . $this->session->lastname)
                            );
                        }



                        logAdmin(array(
                            'companyid' => $this->companyid,
                            'serviceid' => $serviceid,
                            'userid' => $client->id,
                            'user' => $this->session->firstname . ' ' . $this->session->lastname,
                            'ip' => $_SERVER['REMOTE_ADDR'],
                            'description' => 'Service : ' . $serviceid . ' '.lang('requested for').' '.$order->details->msisdn_type.' '.lang('Activation').' ' . $sim->MSISDN
                        ));
                    }
                    echo json_encode(array(
                        'result' => "success"
                    ));
                    redirect('admin/subscription/detail/' . $_POST['serviceid']);
                }
                $serviceid     = $_POST["serviceid"];
                $date_contract = $_POST["date_contract"];
                unset($_POST["serviceid"]);
                unset($_POST["date_contract"]);
                $sn = $this->artilium->getSn($_POST['msisdn_sim']);
                log_message('error', print_r($sn, true));
                if ($sn->result != "success") {
                    $this->session->set_flashdata('error', lang('Simcard you provided').' :' . $_POST["msisdn_sim"] . ' '.lang('is not provisioned, please check the number'));
                    redirect('admin/subscription/activate_mobile/' . $id);
                    exit;
                } //$sn->result != "success"
                $_POST["msisdn_sn"]   = trim($sn->data->SN);
                $_POST["msisdn_puk1"] = $sn->data->PUK1;
                $_POST["msisdn_puk2"] = $sn->data->PUK2;
                if ($_POST["msisdn_type"] == "porting") {
                    $_POST["msisdn"] = $_POST['donor_msisdn'];
                    $this->Admin_model->AddSmsNumber(array(
                        'userid' => $client->id,
                        'msisdn' => $_POST['donor_msisdn']
                    ));
                } //$_POST["msisdn_type"] == "porting"
                else {
                    $this->Admin_model->AddSmsNumber(array(
                        'userid' => $client->id,
                        'msisdn' => $sn->data->MSISDNNr
                    ));
                    $_POST["msisdn"]             = $sn->data->MSISDNNr;
                    $_POST["donor_type"]         = "";
                    $_POST["donor_provider"]     = "";
                    $_POST["donor_customertype"] = "";
                }
                /*
                $update = $this->Admin_model->UpdateService($_POST["serviceid"],
                array('msisdn_sim' => $_POST["msisdn_sim"],
                'donor_msisdn' => $_POST["donor_msisdn"],
                'msisdn_type' => $_POST["msisdn_type"],
                'donor_operator' => $_POST["donor_operator"],
                'date_wish' => $_POST["date_wish"],
                'donor_sim' => $_POST["donor_sim"]));
                */
                $this->Admin_model->updateProductDetails($serviceid, trim($order->type), $_POST);
                $this->Admin_model->updateContractDate($serviceid, $date_contract);
                // Start Get fresh parameters
                $order = $this->Admin_model->getServiceCli($serviceid);
                //log_message('error', print_r($order, true));
                //log_message('error', print_r($client, true));
                if ($order->details->msisdn_type == "porting") {
                    if (substr($order->details->msisdn, 0, 2) == "31") {
                        if ($_POST['date_wish'] < date('Y-m-d')) {
                            $this->session->set_flashdata('error', lang('Your Portin DateWish should be in The future'));
                            redirect('admin/subscription/detail/' . $id);
                        }
                        if ($_POST['date_wish'] != $order->details->date_wish) {
                            $this->Admin_model->update_services_data('mobile', $id, array(
                                'date_wish' => trim($_POST['date_wish']),
                                'date_modified' => date('Y-m-d H:i:s')
                            ));
                            $order = $this->Admin_model->getService($id);
                        }
                    }
                }
                $nc = explode("-", $order->date_contract);
                if ($order->details->msisdn_type == "porting") {
                    if (substr($order->details->msisdn, 0, 2) == "31") {
                        if (!$order->details->porting_sms) {
                            $this->send_PortinInitiation($serviceid);
                        }
                    }
                }
                $result = $this->artilium->ActiveNewSIM($client->firstname . ' ' . $client->lastname, $order->details);
                log_message('error', print_r($result, true));
                if ($result->result == "success") {
                    if ($this->data['setting']->create_arta_contact == 1) {
                        if (!$order->details->arta_packageid) {
                            $artaPackages = $this->artilium->AddCustomerProduct(array('ContactType2Id' => $order->details->msisdn_contactid, 'PackageId' => $order->ArtaPackageId, 'SN'=> $order->details->msisdn_sn));
                            if ($artaPackages->AddCustomerProductResult->Result == "0") {
                                $this->Admin_model->update_services_data('mobile', $serviceid, array(
                                'arta_packageid' => $artaPackages->AddCustomerProductResult->NewItemId,
                                'date_modified' => date('Y-m-d H:i:s')
                                ));
                            }
                            log_message('error', 'Packages Add contactid'.print_r($artaPackages, true));
                        }
                    }


                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $serviceid,
                        'userid' => $client->id,
                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' . $serviceid . ' '.lang('requested for Activation')
                    ));
                    if ($order->details->msisdn_type == "porting") {
                        //$this->send_PortinInitiation($serviceid);
                        $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                        $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
                    } //$order->details->msisdn_type == "porting"
                    else {
                        if (date('Y-m-d') === $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                            log_message('error', "Date activation compare: ".date('Y-m-d')." ".$nc[2] . '-' . $nc[0] . '-' . $nc[1]);
                            if (substr($order->details->msisdn, 0, 2) == "31") {
                                $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($order->details->msisdn_sn));
                                $this->artilium->UpdateServices(trim($order->details->msisdn_sn), $pack, '1');
                            }
                            $this->Admin_model->ChangeStatusService($serviceid, 'Active');
                            $this->Admin_model->ChangeStatusOrderID($serviceid, 'Active', 'mobile');
                            $this->send_welcome_email($serviceid);
                        } else {
                            $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                            $this->Admin_model->ChangeStatusOrderID($serviceid, 'ActivationRequested', 'mobile');
                        }
                    }
                    $mobile    = $this->Admin_model->getService($serviceid);

                    if ($this->data['setting']->mage_invoicing) {
                        $pricingid = $this->add_pricing_extra($order->id);
                    } else {
                        $pricingid = false;
                    }

                    //log_message('error', print_r($pricingid, true));
                    if (!$noAddSim) {
                        if ($this->data['setting']->mage_invoicing) {
                            $addsim = $this->magebo->AddSIMToMagebo($mobile);
                            if ($addsim->result == "success") {
                                if (date('Y-m-d') == $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                                    //Prevent adding subscription if the contract date is today and it's a porting
                                    if ($order->details->msisdn_type != 'porting') {
                                        $this->magebo->addPricingSubscription($mobile);
                                    } else {
                                        //activate simcard and add to magebo with the snnumber
                                        $this->magebo->addPricingSubscription($mobile, false, false, true);
                                    }
                                } else {
                                    if (substr($mobile->details->msisdn, 0, 2) == "32") {
                                        $this->Admin_model->updateContractDate($serviceid, date('m-d-Y'));
                                        //$this->magebo->addPricingSubscription($mobile);
                                    }
                                }
                                // Add Simcardlog into Magebo
                            } //$addsim->result == "success"
                        }
                        //assign Bundles when sim is activated
                        $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
                        log_message('error', "bundles ".print_r($bundles, true));
                        if ($bundles) {
                            $IDS = array();
                            // activate tarief per packages
                            foreach ($bundles as $bundleid) {
                                $r = $this->artilium->AddBundleAssign($order->details->msisdn_sn, $bundleid, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
                                if ($r->result == "success") {
                                    if ($this->data['setting']->create_magebo_bundle) {
                                        $create = 1;
                                    } //$this->data['setting']->create_magebo_bundle
                                    else {
                                        $create = 0;
                                    }
                                    if ($this->data['setting']->mage_invoicing) {
                                        $this->magebo->ProcessPricing($order, $client->mageboid, $bundleid, $create, false, $order->date_contract);
                                    }
                                    $IDS[] = $r->id;
                                } //$r->result == "success"
                                else {
                                    $IDS[] = $r;
                                }
                            } //$bundles as $bundleid
                            log_message('error', print_r($IDS, true));
                            if ($IDS) {
                                logAdmin(array(
                                    'companyid' => $this->companyid,
                                    'serviceid' => $serviceid,
                                    'userid' => $client->id,
                                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                                    'ip' => $_SERVER['REMOTE_ADDR'],
                                    'description' => 'Service : ' . $serviceid . ' '.lang('added bundle id').' ' . implode(',', $IDS)
                                ));
                                send_growl(array(
                                    'message' => $this->session->firstname . ' ' . $this->session->lastname . ' '.lang('added bundle id').' ' . implode(',', $IDS),
                                    'companyid' => $this->session->cid
                                ));
                                $this->Admin_model->updateBundleID($order->id, implode(',', $IDS));
                            } //$IDS
                        } //$bundles
                        //$options_extra = $this->
                    } //!$noAddSim
                    //Check if contract date is today otherwise disable all services
                    $date_contract = $nc[2] . '-' . $nc[0] . '-' . $nc[1];
                    log_message('error', print_r($date_contract, true));
                    if (date('Y-m-d') < $date_contract) {
                        if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                            $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                            $pack   = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                            $this->artilium->UpdateServices($msisdn->data->SN, $pack, '0');
                        } else {
                            $this->artilium->PartialFullBar($order->details, 0);
                        }
                    } else {
                        if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                            $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                            $pack   = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
                            $this->artilium->UpdateServices($msisdn->data->SN, $pack, '1');
                            $this->artilium->UpdateCLI($msisdn->data->SN, 1);
                        } else {
                            $this->artilium->PartialFullBar($order->details, 0);
                        }
                    }
                    if ($_POST["msisdn_type"] == "porting") {
                        sleep(2);
                        if ($this->data['setting']->mage_invoicing) {
                            $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                        }
                        $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
                    } else {
                        $this->artilium->UpdateCLI($msisdn->data->SN, 1);
                        if ($this->data['setting']->mage_invoicing) {
                            $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');

                            sleep(2);
                            $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
                        }
                    }
                    // $this->session->set_flashdata('success', lang('Activation has been Requested'));
                    send_growl(array(
                        'message' => $this->session->firstname . ' ' . $this->session->lastname . ' '.lang('activate Number').': ' . $order->details->msisdn,
                        'companyid' => $this->session->cid
                    ));
                }
                //echo json_encode($result);
                //exit;
                redirect('admin/client/detail/' . $client->id);
                // End activating simcard
            } //isPost()
            $this->data['service']      = $order;
            $this->data['client']       = $client;
            $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscription_activate_mobile';
            $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
        }
    }
    public function delete_service()
    {
        if (!$this->session->master) {
            die('not allowed');
        }
        if ($this->uri->segment(4) > 0) {
            $service = $this->Admin_model->getService($this->uri->segment(4));
            if ($service->type == "xdsl") {
                $this->db->query("delete from a_services where id=?", array(
                    $this->uri->segment(4)
                    ));
                $this->db->query("delete from a_services_xdsl where serviceid=?", array(
                    $this->uri->segment(4)
                ));
                $this->db->query("delete from a_services_xdsl_products where orderid=?", array(
                    $service->details->proximus_orderid
                ));
                $this->db->query("delete from a_services_addons where serviceid=?", array(
                    $this->uri->segment(4)
                ));
            } elseif ($service->type == "mobile") {
                $this->db->query("delete from a_services where id=?", array(
                $this->uri->segment(4)
                ));
                $this->db->query("delete from a_services_mobile where serviceid=?", array(
                $this->uri->segment(4)
                ));
                $this->db->query("delete from a_services_addons where serviceid=?", array(
                $this->uri->segment(4)
                ));
            }
            redirect('admin/client/detail/' . $service->userid);
        } //$this->uri->segment(4) > 0
    }
    public function update_contractdate()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->session->cid
        ));
        $serviceid = $_POST['serviceid'];
        $mobile    = $this->Admin_model->getService($serviceid);
        $date      = $_POST['date_contract'];
        $fmt = explode('-', $date);
        $this->db->where('id', $serviceid);
        $this->db->update('a_services', array(
            'date_contract' => $date,
            'date_contract_formated'=> $fmt[2].'-'.$fmt[0].'-'.$fmt[1]
        ));
        $dt = explode('-', $date);
        $this->magebo->change_contract_date($mobile->details->msisdn, $date);
        $this->magebo->change_contract_date($mobile->details->msisdn_sn, $date);
        $this->magebo->updatePricingMonthbypin($mobile->details->msisdn, $dt[2], $dt[0], $dt[1]);
        $this->magebo->updatePricingMonthbypin($mobile->details->msisdn_sn, $dt[2], $dt[0], $dt[1]);
        logAdmin(array(
            'companyid' => $this->session->cid,
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'serviceid' => $_POST['serviceid'],
            'userid' => $mobile->details->userid,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => lang('Changes Contract Date for').' ' . $mobile->details->msisdn . ' from  ' . $mobile->date_contract . ' to ' . $_POST['date_contract']
        ));
        send_growl(array(
            'message' => $this->session->firstname . ' ' . $this->session->lastname . ' '.lang('Changes Contract Date for').' ' . $mobile->details->msisdn . ' from  ' . $mobile->date_contract . ' to ' . $_POST['date_contract'],
            'companyid' => $this->session->cid
        ));
        redirect('admin/subscription/activate_mobile/' . $serviceid);
    }
    public function textx()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        $bundles = $this->artilium->GetBundleAssignList($this->uri->segment(4));
        print_r($bundles);
    }
    public function updatebundleValidity()
    {
        $bundlename = "";
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        $bundles = $this->artilium->GetBundleAssignList($_POST['sn']);
        $succes  = "error";
        $date    = explode('T', $_POST['ValidUntil']);
        foreach ($bundles->NewDataSet->BundleAssign as $bundle) {
            if ($bundle->BundleAssignId == $_POST['BundleAssignId']) {
                $bundlename = getMobilebundleName($bundle->BundleId, $this->companyid);
                $res        = $this->artilium->UpdateBundleAssignV2(array(
                    'SN' => trim($_POST['sn']),
                    'BundleAssignId' => $bundle->BundleAssignId,
                    'ValidUntil' => $date[0] . "T23:59:59"
                ));
                if ($res->UpdateBundleAssignV2Result->Result == "0") {
                    $t = $this->artilium->GetBundleUsageList($_POST['sn'], $bundle->BundleAssignId);
                    if (is_array($t->NewDataSet->BundleUsage)) {
                        //because artilium stupidity about array and object
                        foreach ($t->NewDataSet->BundleUsage as $usage) {
                            $this->artilium->UpdateBundleUsage(array(
                                "Sn" => $_POST['sn'],
                                "BundleUsageId" => $usage->BundleUsageId,
                                "BundleUsageSetId" => $usage->BundleUsageSetId,
                                "ValidUntil" => date('Y-m-d', strtotime('+1 day', strtotime($date[0]))) . "T00:00:00",
                                "UpdateBundleAssignment" => false
                            ));
                        }
                    } else {
                        $this->artilium->UpdateBundleUsage(array(
                            "Sn" => $_POST['sn'],
                            "BundleUsageId" => $t->NewDataSet->BundleUsage->BundleUsageId,
                            "BundleUsageSetId" => $t->NewDataSet->BundleUsage->BundleUsageSetId,
                            "ValidUntil" => date('Y-m-d', strtotime('+1 day', strtotime($date[0]))) . "T00:00:00",
                            "UpdateBundleAssignment" => false
                        ));
                    }
                    unset($t);
                }
                $succes = "success";
            }
        }
        logAdmin(array(
            'companyid' => $this->companyid,
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'serviceid' => $_POST['serviceid'],
            'userid' => $_POST['userid'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'updatebundleValidity of ' . $bundlename . ' to: ' . $_POST['ValidUntil']
        ));


        $this->session->set_flashdata('success', lang('Bundle expiredate has been updated'));
        echo json_encode(array(
            'result' => $success
        ));
    }
    public function block_mobile_originating()
    {
        $this->load->model('Admin_model');
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($_POST['SN']));
        $this->Admin_model->save_state($_POST['serviceid'], $pack);
        $this->artilium->BlockOriginating(trim($_POST['SN']), $pack, '0');
        $this->Admin_model->update_services_data('mobile', $_POST["serviceid"], array(
            'bar' => 1,
            'date_modified' => date('Y-m-d H:i:s')
        ));
        $this->Admin_model->update_services($_POST["serviceid"], array(
            'status' => 'Suspended'
        ));

        logAdmin(array(
            'companyid' => $this->companyid,
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'serviceid' => $_POST['serviceid'],
            'userid' => $_POST['userid'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Outbound traffic has been blocked'
         ));
        $this->session->set_flashdata('success', lang('All Originating Service include International Calls has been barred'));
        send_growl(array(
            'message' => $this->session->firstname . ' ' . $this->session->lastname . ' '.lang('Block outbound traffic for').'serviceid:' . $_POST["serviceid"],
            'companyid' => $this->session->cid
        ));
        echo json_encode($_POST);
    }
    public function unblock_mobile_originating()
    {
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getPackageState($_POST["serviceid"]);
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        logAdmin(array(
            'companyid' => $this->companyid,
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'serviceid' => $_POST['serviceid'],
            'userid' => $_POST['userid'],
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Outbound traffic has been unblocked'
          ));
        $pack = (array) json_decode($service);
        $this->artilium->UnBlockOriginating(trim($_POST['SN']), $pack, '1');
        $this->Admin_model->update_services($_POST["serviceid"], array(
            'status' => 'Active'
        ));
        $this->Admin_model->update_services_data('mobile', $_POST["serviceid"], array(
            'bar' => 0,
            'date_modified' => date('Y-m-d H:i:s')
        ));
        $this->session->set_flashdata('success', lang('All Originating Service include International Calls has been barred'));
        echo json_encode($pack);
    }
    public function fixbundle()
    {
        $serviceid   = $this->uri->segment(4);
        $create_arta = $this->uri->segment(5);
        if (!empty($this->uri->segment(6))) {
            $change_status = true;
        } //!empty($this->uri->segment(6))
        else {
            $change_status = false;
        }
        //$change_status = $this->uri->segment(6);
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $order  = $this->Admin_model->getService($this->uri->segment(4));
        //assign Bundles when sim is activated
        $client = $this->Admin_model->getClient($order->userid);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        // $result = $this->artilium->ActiveNewSIM($client->firstname . ' ' . $client->lastname, $order->details);
        $this->session->set_flashdata('success', lang('Service Has been activated'));
        if ($change_status) {
            if ($order->details->msisdn_type == "porting") {
                $this->Admin_model->ChangeStatusService($serviceid, 'Pending');
                $this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
            } //$order->details->msisdn_type == "porting"
            else {
                $this->Admin_model->ChangeStatusService($serviceid, 'Active');
                $this->Admin_model->ChangeStatusOrderID($serviceid, 'Active', 'mobile');
            }
        } //$change_status
        $order = $this->Admin_model->getService($serviceid);

        if ($order) {
            $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
            $addsim  = $this->magebo->AddSIMToMagebo($order);
            // $addsim = (object) array('result' => true);
            if ($addsim->result == "success") {
                if ($order->details->msisdn_type == "porting") {
                    sleep(2);
                // $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                    //$this->Admin_model->ChangeStatusOrderID($serviceid, 'PortinPending', 'mobile');
                } //$order->details->msisdn_type == "porting"
                else {
                    //$this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '599');
                    sleep(2);
                    //$this->magebo->addSimcardLog(trim($order->details->msisdn_sim), '612');
                }
                //$this->magebo->addPricingSubscription($order);
                if ($this->data['setting']->create_magebo_bundle) {
                    $create = 1;
                } //$this->data['setting']->create_magebo_bundle
                else {
                    $create = 0;
                }
            } //$addsim->result == "success"
            $bundles = $this->Admin_model->getBundlebyProduct($order->packageid);
            log_message('error', 'Fixing Bundle:'.print_r($bundles, 1));
            if ($bundles) {
                $IDS = array();
                // activate tarief per packages
                foreach ($bundles as $bundleid) {
                    if ($create_arta == 2) {
                        //create bundle
                        $r = $this->artilium->AddBundleAssign($order->details->msisdn_sn, $bundleid, date('Y-m-d') . 'T' . date('H:i:s'), '2099-12-31T23:59:59');
                        log_message('error', 'Adding Bundle '.$bundleid.': '.print_r($r, 1));
                    } //$create_arta == 2
                    else {
                        $r = (object) array(
                            'result' => 'success'
                        );
                    }
                    if ($r->result == "success") {
                        if ($this->data['setting']->create_magebo_bundle) {
                            $create = 1;
                        } //$this->data['setting']->create_magebo_bundle
                        else {
                            $create = 0;
                        }
                        $this->magebo->ProcessPricing($order, $client->mageboid, $bundleid, $create);
                        $IDS[] = $r->id;
                    } //$r->result == "success"
                    else {
                        $IDS[] = $r;
                    }
                } //$bundles as $bundleid
                if ($IDS) {
                    $this->Admin_model->updateBundleID($order->id, implode(',', $IDS));
                } //$IDS
            } //$bundles
            //Check if contract date is today otherwise disable all services
            if (date('Y-m-d') <= $nc[2] . '-' . $nc[0] . '-' . $nc[1]) {
                $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                $pack   = $this->artilium->GetListPackageOptionsForSn($msisdn->data->SN);
                $this->artilium->UpdateServices($msisdn->data->SN, $pack, '0');
                $this->session->set_flashdata('success', lang('Bundle Has been added and service is disabled until').' ' . $nc[2] . '-' . $nc[0] . '-' . $nc[1]);
            } //date('Y-m-d') <= $nc[2] . '-' . $nc[0] . '-' . $nc[1]
            else {
                $this->session->set_flashdata('success', lang('Bundle Has been added'));
            }
            sleep(2);
            $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), 599);
            //$this->session->set_flashdata('success', 'Bundle Has been added');
            if (substr($order->details->msisdn, 0, 2) == "31") {
                $this->fixbundlePorting($this->uri->segment(4));
            }
            redirect('admin/subscription/detail/' . $this->uri->segment(4));
        } //$order
    }
    public function addlog()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $serviceid = $this->uri->segment(4);
        $order     = $this->Admin_model->getService($serviceid);
        $this->magebo->addSimcardLog(trim($order->details->msisdn_sim), 599);
    }
    public function activate_now()
    {
        $this->load->model('Admin_model');
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        $this->db = $this->load->database('default', true);
        $this->load->library('magebo', array(
            'companyid' => $this->session->cid
        ));
        $this->Admin_model->update_services($_POST['serviceid'], array(
            'date_contract' => date('m-d-Y')
        ));
        $q = $this->db->query("SELECT a.msisdn,a.msisdn_sn,a.msisdn_sim,a.serviceid,b.date_contract,a.companyid,a.msisdn_status,b.status,a.msisdn_type
        FROM a_services_mobile a
        LEFT JOIN a_services b ON b.id=a.serviceid
        WHERE b.companyid IN (53,54,55)
        AND b.type=?
        AND b.date_contract=?
        AND b.id =?
        AND a.msisdn_sn IS NOT NULL
        AND b.status NOT IN('Suspended','Terminated','Cancelled')", array(
            'mobile',
            date('m-d-Y'),
            $_POST['serviceid']
        ));
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $row) {
                $cid   = $row->companyid;
                //
                $order = $this->Admin_model->getServiceCli($row->serviceid);
                if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                    $this->send_welcome_email($row->serviceid);
                    //echo $row->status." ".$row->msisdn_status."\n";
                    // $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
                    if ($row->status == "Pending" && $row->msisdn_type == "new") {
                        // echo $cid." ".$row->serviceid . " " . date('Y-m-d H:i:s') . " " . $order->details->msisdn . " has been activated\n";
                        $pack = $this->artilium->GetListPackageOptionsForSnAdvance($order->details->msisdn_sn);
                        $this->artilium->UpdateServices($order->details->msisdn_sn, $pack, '1');
                        $this->artilium->UpdateCli($order->details->msisdn_sn, '1');
                        $this->magebo->addPricingSubscription($order);
                        $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                        $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                    }
                    if (($row->status == "Active") && ($row->msisdn_status != "Active") && ($row->msisdn_type == "new")) {
                        // echo $cid." ".$row->serviceid . " " . date('Y-m-d H:i:s') . " " . $order->details->msisdn . " was syncronized\n";
                        $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                        $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                    }
                } else {
                    if ($row->status == "Pending" && $row->msisdn_type == "new") {
                        $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                        $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                        $this->artilium->PartialFullBar($order->details, '0');
                    }
                }
                unset($order);
                unset($cid);
                unset($this->artilium);
            }
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function fixbundlePorting($id)
    {
        $service = $this->Admin_model->getService($id);
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $addsim = $this->magebo->AddPortinSIMToMagebo($service);
        $this->load->model('Api_model');
        $this->Api_model->PortIngAction1($service);
        $this->Api_model->PortIngAction2($service);
        $this->Api_model->PortIngAction3($service);
        $this->Api_model->PortIngAction4($service);
        $this->Api_model->PortIngAction5($service->companyid);
        $this->Api_model->pinmove($service->details->donor_msisdn, $service->details->msisdn_sn, $service->details->msisdn);
        //redirect('admin/subscription/detail/' . $id);
    }
    public function sum_reports()
    {
        $this->data['title']        = "Sum Reports";
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'sum_reports';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function disablex()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $id     = $this->uri->segment(4);
        $order  = $this->Admin_model->getService($id);
        $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
        $pack   = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
        $this->artilium->UpdateServices($msisdn->data->SN, $pack, '0');
    }
    public function fixbundle2()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $id = $this->uri->segment(4);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $order  = $this->Admin_model->getService($id);
        $msisdn = $this->artilium->getSn($order->details->msisdn_sim);
        $pack   = $this->artilium->GetListPackageOptionsForSnAdvance($msisdn->data->SN);
        print_r($pack);
        exit;
        $res = $this->artilium->UpdateServices($msisdn->data->SN, $pack, '1');
        print_r($res);
    }
    public function send_welcome_email($id)
    {
        $this->load->model('Admin_model');
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent('order_accepted', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', $client->salutation . ' ' . $client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        if ($client->companyid == 53) {
            $combi = $this->Admin_model->GetProductType($mobile->packageid);
            if ($combi == 2) {
                $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. Vanaf nu ontvangt u daarom thuis 25 Mbps extra downloadsnelheid (tot maximaal 400 Mbps). Nog sneller streamen, downloaden en gamen dus!</p>";
            } //$combi == 2
            elseif ($combi == 1) {
                $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. U ontvangt daarom elke maand de volgende gratis extra’s:</p><p>
-   Dubbele data en Onbeperkt bellen met je mobiel<br />
-   ".$this->data['setting']->currency." 5,– korting op uw DELTA Internet factuur<br />
-   25 Mbps extra downloadsnelheid (tot maximaal 400Mbps).<br />
-   Een extra TV-pakket boordevol sport-, muziek-, entertainment-, en jeugdzenders<br /></p>";
            } //$combi == 1
            elseif ($combi == 0) {
                $com = "<p>We zijn erg blij met u als klant. Daarom krijgt u als welkomstcadeau een halfjaar ".$this->data['setting']->currency." 5,– maandelijkse korting op uw mobiele abonnement. U hoeft daar verder niets voor te doen. </p>";
            } //$combi == 0
            $body = str_replace('{$combi}', $com, $body);
        }
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('order_accepted', $client->language, $client->companyid));
        //$this->email->subject(lang("Your New Password"));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'order_accepted')) {
            log_message('error', 'Template order_accepted is disabled sending aborted');
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                    'companyid' => $client->companyid,
                    'userid' => $client->id,
                    'to' => $client->email,
                    'subject' => getSubject('order_accepted', $client->language, $client->companyid),
                    'message' => $body
                ));
            }
        }
    }
    public function change_packagesetting()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if (!empty($_POST['sn']) && !empty($_POST['id']) && isset($_POST['val'])) {
            if ($_POST['id'] == 100000) {
                if ($_POST['val'] == 1) {
                    $_POST['val'] = '0';
                } else {
                    $_POST['val'] = '1';
                }
                $result = $this->artilium->SwitchVoiceMail($_POST['msisdn'], $_POST['sn'], $_POST['val']);
            } else {
                $result = $this->artilium->UpdatePackageOptionsForSN(trim($_POST['sn']), $_POST['id'], $_POST['val']);
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $_POST['userid'],
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'UpdatePackageOptionsForSn for PackageDefinitionId ' . $_POST['id'] . ' to ' . $_POST['val']
                ));
            }
            echo json_encode($result);
        } //!empty($_POST['sn']) && !empty($_POST['id']) && isset($_POST['val'])
        else {
            echo json_encode(array(
                'result' => 'error',
                'message' => 'Not enough data: sn,id,val'
            ));
        }
    }
    public function getorderdetails()
    {
        $this->db = $this->load->database('default', true);
        header('Content-Type: application/json');
        $html     = '<table class="table table-striped">';
        $id       = $_POST['orderid'];
        $s        = $this->db->query("select a.*,b.msisdn_sim,b.msisdn_type,d.name as productname,concat(c.firstname,' ',c.lastname) as customername,c.mvno_id
            from a_services a
            left join a_services_mobile b on b.serviceid=a.id
            left join a_clients c on c.id=a.userid
            left join a_products d on d.id=a.packageid
            where a.id=?", array(
            $id
        ));
        $contract = explode('-', $s->row()->date_contract);
        $html .= '
        <tr>
        <td width="30%">' . lang('Order ID') . '</td>
        <td width="70%" class="text-right">' . $s->row()->id . '</td>
        </tr>
        <tr>
        <td width="30%">' . lang('Order Date') . '</td>
        <td class="text-right">' . date("d/m/Y", strtotime($s->row()->date_created)) . '</td>
        </tr>

        <tr>
        <td width="30%">' . lang('Customer') . '</td>
        <td class="text-right"><a href="' . base_url() . 'admin/client/detail/' . $s->row()->userid . '">' . $s->row()->customername . ' ( #' . $s->row()->mvno_id . ' )</a></td>
        </tr>
        <tr>
        <td width="30%">' . lang('Product') . '</td>
        <td class="text-right"><b>' . $s->row()->productname . '</b></td>
        </tr>
        <tr>
        <td width="30%">' . lang('Billingcycle') . '</td>
        <td class="text-right">' . lang($s->row()->billingcycle) . '</td>
        </tr>
        <tr>
        <td width="30%">' . lang('MSISDN TYPE') . '</td>
        <td class="text-right text-danger">' . lang(ucfirst($s->row()->msisdn_type) . " Number") . '</td>
        </tr>
        <tr>
        <td width="30%">' . lang('Recurring') . '</td>
        <td class="text-right">'.$this->data['setting']->currency . number_format($s->row()->recurring, 2) . '</td>
        </tr>
        <tr>
        <td width="30%">' . lang('Promocode') . '</td>
        <td class="text-right">' . $s->row()->promocode . '</td>
        </tr>
        </tr>
        <td width="30%">' . lang('Contract Start Date') . '</td>
        <td class="text-right"><b>' . $contract[1] . "/" . $contract[0] . "/" . $contract[2] . '</b></td>
        </tr>
        ';
        $html .= '</table>';
        echo json_encode(array(
            'result' => $html,
            'serviceid' => $s->row()->id,
            'userid' => $s->row()->userid,
            'msisdn_sim' => $s->row()->msisdn_sim,
            'msisdn_type' => $s->row()->msisdn_type
        ));
    }
    public function portingonDemand_RequestPorting()
    {
        $this->load->database('default', true);
        $q = $this->db->query("select * from a_services_mobile where msisdn like ?", array(
            trim($this->uri->segment(4))
        ));
        $this->load->library('artilium', array(
            'companyid' => $q->row()->companyid
        ));
        if ($q->num_rows() > 0) {
            $client    = $this->Admin_model->getClient($q->row()->userid);
            $resportin = $this->artilium->PortingNewSIM(format_name($client), $q->row());
            //print_r($resportin);
            //exit;
            //$resportin = $this->artilium->ActiveNewSIM($client->firstname . ' ' . $client->lastname, $q->row());
            if ($resportin->result == "success") {
                if ($q->row()->pod_counter >= 3) {
                    $this->session->set_flashdata('error', lang('Sorry you have reach your maximum threshold of requesting code, please contact our support'));
                    redirect('admin/subscription/detail/' . $q->row()->serviceid);
                }
                $counter = $q->row()->pod_counter + 1;
                $this->db->query("update a_services_mobile set pod_counter = ?, pod_lastrequest=? where msisdn like ? and msisdn_status = ?", array(
                    $counter,
                    time(),
                    trim($this->uri->segment(4)),
                    'PortinPending'
                ));
                $q               = $this->db->query("select * from a_services_mobile where msisdn like ? and msisdn_status = ?", array(
                    trim($this->uri->segment(4)),
                    'PortinPending'
                ));
                $this->session->set_userdata('pod', $_POST);
                //$_SESSION['pod'] = $_POST;
                $this->session->set_flashdata('success', lang('Your request has been processed, please enter the verification code below'));
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $q->row()->serviceid,
                    'userid' => $client->id,
                    'user' => $client->firstname . ' ' . $client->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Service : ' . $q->row()->serviceid . ' ' . lang(' requested for new code portin demand successfully')
                ));
            } else {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $q->row()->serviceid,
                    'userid' => $client->id,
                    'user' => $client->firstname . ' ' . $client->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Service : ' . $q->row()->serviceid . ' ' . lang('requested for new code portin demand failed ') . print_r($result, true)
                ));
            }
        } else {
            //mail('mail@simson.one', 'Portin On demand request code failed', print_r($_POST, true));
            $this->session->set_flashdata('error', lang('Sorry your Number can not be found in portinpending status'));
        }
        redirect('admin/subscription/detail/' . $q->row()->serviceid);
    }
    public function PortingonDemand_ConfirmPorting()
    {
        $this->session->set_userdata('pod', $_POST);
        $this->load->model('Admin_model');
        $this->load->database('default', true);
        $q = $this->db->query("select a.* from a_services_mobile a left join a_services b on b.id=a.serviceid where a.msisdn like ? and b.status != ? order by a.id desc", array(
            trim($_POST['msisdn']),
            'Cancelled'
        ));
        if ($q->num_rows() > 0) {
            $this->load->library('artilium', array(
                'companyid' => $q->row()->companyid
            ));
            if ($q->row()->msisdn_sn) {
                $request = $this->artilium->SaveVerificationCode($q->row()->msisdn_sn, trim($_POST['code']));
                //mail('mail@simson.one', 'porting save array', print_r($request, true));
                $client  = $this->Admin_model->getClient($q->row()->userid);
                if ($request->result == "success") {
                    $this->db->query("update a_services_mobile set msisdn_status =? where serviceid=?", array(
                        'PortinPending',
                        $q->row()->serviceid
                    ));
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $q->row()->serviceid,
                        'userid' => $client->id,
                        'user' => $client->firstname . ' ' . $client->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' . $q->row()->serviceid . ' ' . lang('requested for confirmation successfull with code: ') . $_POST['code']
                    ));
                    $this->session->set_flashdata('success', lang('Your request has been processed'));
                } else {
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $q->row()->serviceid,
                        'userid' => $client->id,
                        'user' => $client->firstname . ' ' . $client->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' . $q->row()->serviceid . ' ' . lang('requested for confirmation failed with code') . ': ' . $_POST['code']
                    ));
                    $this->session->set_flashdata('error', lang('ErrorCode: ') . ': ' . $request->result);
                }
            } else {
                // mail('mail@simson.one', 'Portin On demand request confirm failed', print_r($_POST, true));
                $this->session->set_flashdata('error', lang('We are unable to process your request at the moment, our agent has been notified'));
            }
        } else {
            $this->session->set_flashdata('error', lang('Sorry your Number can not be found'));
        }
        redirect('admin/subscription/detail/' . $_POST["serviceid"]);
    }
    public function GetAddonsPrice($id)
    {
        //700480
        $amount   = 0;
        $this->db = $this->load->database('default', true);
        $a        = $this->db->query("select a.hostingid,a.id,a.recurring,b.name,c.monthly from tblhostingaddons a left join tbladdons b on b.id=a.addonid left join tblpricing c on c.relid=b.id and c.type='addon' where a.hostingid=?", array(
            $id
        ));
        foreach ($a->result() as $row) {
            if (substr(trim($row->name), 0, 5) == "Delta") {
                $amount = $amount + $row->monthly;
            } //substr(trim($row->name), 0, 5) == "Delta"
            else {
                $amount = $amount + $row->recurring;
            }
        } //$a->result() as $row
        //print_r($a->result());
        return number_format($amount, 2);
    }
    public function mobile_unstolen()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->session->cid
        ));
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        if (!empty($_POST['msisdn']) && !empty($_POST['SN']) && !empty($_POST['SimCardNbr'])) {
            $result = $result = $this->artilium->UpdateCLI($_POST['SN'], 1);
            if ($result->result == "success") {
                $this->Admin_model->update_services($_POST['serviceid'], array(
                    'status' => 'Active'
                ));
            }
            $this->Admin_model->record_unstolen($_POST['msisdn'], $_POST['serviceid'], $_POST['userid']);
        }
        logAdmin(array(
            'companyid' => $this->companyid,
            'serviceid' => $_POST['serviceid'],
            'userid' => $_POST['userid'],
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => lang('Mobile').': ' . $_POST['msisdn'] . ' '.lang('has been removed from record stolen phone')
        ));
        $this->session->set_flashdata('success', lang('Number has been removed from stolen records'));
        redirect('admin/subscription/' . $_POST['serviceid']);
    }
    public function mobile_stolen()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->session->cid
        ));
        $this->load->library('artilium', array(
            'companyid' => $this->session->cid
        ));
        if (!empty($_POST['msisdn']) && !empty($_POST['typebar']) && !empty($_POST['SN']) && !empty($_POST['SimCardNbr'])) {
            $result = $result = $this->artilium->UpdateCLI($_POST['SN'], 0);
            if ($result->result == "success") {
                $this->Admin_model->update_services($_POST['serviceid'], array(
                    'status' => 'Suspended'
                ));
                //$this->db->query("update a_services set status=? where id=?", array('Suspended', $_POST['serviceid']));
                $magebo = $this->magebo->addSimBlocking($_POST['SimCardNbr']);
                if ($magebo->result == "success") {
                    $this->Admin_model->record_stolen($_POST['msisdn'], $_POST['serviceid'], $_POST['userid']);
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' => $_POST['userid'],
                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Mobile: ' . $_POST['msisdn'] . ' '.lang('has been reported stolen')
                    ));
                    $this->send_simblock_email($_POST['serviceid']);
                    $this->session->set_flashdata('success', lang('Phone Stolen action was succesfull, our agent will do necessary action, however your simcard has been blocked imediately'));
                } //$magebo->result == "success"
                else {
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' => $_POST['userid'],
                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Mobile: ' . $_POST['msisdn'] . ' '.lang('Failed to be blocked for stolen report')
                    ));
                    $this->session->set_flashdata('error', lang('AddSimBlockingLog action was not succesfull'));
                }
            } //$result->result == "success"
            else {
                logAdmin(array(
                    'name' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Mobile: ' . $_POST['msisdn'] . ' '.lang('Baring action was not successfull')
                ));
                $this->session->set_flashdata('error', lang('Bar action was not succesfull result: ') . print_r($result, true));
            }
        } //!empty($_POST['msisdn']) && !empty($_POST['typebar']) && !empty($_POST['SN']) && !empty($_POST['SimCardNbr'])
        else {
            $this->session->set_flashdata('error', lang('Bar action was not succesfull you need to enter typebar, SN and SimCardNbr'));
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function platform_suspend()
    {
        $serviceid = $this->uri->segment(4);
        $service   = $this->Admin_model->getService($serviceid);
        $mobile    = $service->details;
        if (!$service) {
            echo json_encode(array(
                'result' => false,
                'message' => lang('You do not have access to this subscription')
            ));
            exit;
        }
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if (!empty($mobile->msisdn) && isset($mobile->msisdn_sn)) {
            $result = $this->artilium->UpdateCLI($mobile->msisdn_sn, 0);
            //$result = $this->Admin_model->artiliumPost(array('companyid' => $this->data['mobile']->iCompanyNbr, 'type' => $this->data['mobile']->PaymentType, 'action' => 'UpdateCLI', 'Status' => '0', 'SN' => $_POST['SN']));
            //print_r(array('companyid' => $this->data['mobile']->iCompanyNbr, 'type' => $this->data['mobile']->PaymentType, 'action' => 'UpdateParametersCLI', 'ParameterId' => 20541, 'ParameterValue' => $_POST['typebar'], 'SN' => $_POST['SN']));
            if ($result->result == "success") {
                $this->db->query("update a_services set status=? where id=?", array(
                    $serviceid,
                    'Suspended'
                ));
                //$pack = $this->artilium->GetListPackageOptionsForSn(trim($_POST['SN']));
                //$this->artilium->UpdateServices(trim($_POST['SN']), $pack, '0');
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $serviceid,
                    'userid' => $service->userid,
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Baring Request for ' . $mobile->msisdn . ' has been executed successfully'
                ));
                echo json_encode(array(
                    'result' => true,
                    'message' => lang('request has been accepted')
                ));
                exit;
            //$this->session->set_flashdata('success', $_POST['msisdn'] . lang(' has been suspended as requested.'));
                //$this->send_simblock_email($_POST['serviceid']);
            } //$result->result == "success"
            else {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $serviceid,
                    'userid' => $service->userid,
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Baring Request for ' . $mobile->msisdn . ' has been failed'
                ));
                echo json_encode(array(
                    'result' => false,
                    'message' => lang('error on blocking the services')
                ));
                exit;
            }
        } //!empty($_POST['msisdn']) && isset($_POST['SN'])
        else {
            echo json_encode(array(
                'result' => false,
                'message' => lang('This subscription is not correctly configured in Portal please contact our sysadmin')
            ));
        }
        //redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function platform_unsuspend()
    {
        $serviceid = $this->uri->segment(4);
        $service   = $this->Admin_model->getService($serviceid);
        $mobile    = $service->details;
        if (!$service) {
            echo json_encode(array(
                'result' => false,
                'message' => lang('You do not have access to this subscription')
            ));
            exit;
        }
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if (!empty($mobile->msisdn) && isset($mobile->msisdn_sn)) {
            $result = $this->artilium->UpdateCLI($mobile->msisdn_sn, 1);
            //$result = $this->Admin_model->artiliumPost(array('companyid' => $this->data['mobile']->iCompanyNbr, 'type' => $this->data['mobile']->PaymentType, 'action' => 'UpdateCLI', 'Status' => '0', 'SN' => $_POST['SN']));
            //print_r(array('companyid' => $this->data['mobile']->iCompanyNbr, 'type' => $this->data['mobile']->PaymentType, 'action' => 'UpdateParametersCLI', 'ParameterId' => 20541, 'ParameterValue' => $_POST['typebar'], 'SN' => $_POST['SN']));
            if ($result->result == "success") {
                $this->db->query("update a_services set status=? where id=?", array(
                    $serviceid,
                    'Active'
                ));
                $this->Admin_model->update_services($serviceid, array(
                    'status' => 'Active'
                ));
                //$pack = $this->artilium->GetListPackageOptionsForSn(trim($_POST['SN']));
                //$this->artilium->UpdateServices(trim($_POST['SN']), $pack, '0');
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $serviceid,
                    'userid' => $service->userid,
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Baring Request for ' . $mobile->msisdn . ' '.lang('has been executed successfully')
                ));
                echo json_encode(array(
                    'result' => true,
                    'message' => lang('request has been accepted')
                ));
                exit;
            //$this->session->set_flashdata('success', $_POST['msisdn'] . lang(' has been suspended as requested.'));
                //$this->send_simblock_email($_POST['serviceid']);
            } //$result->result == "success"
            else {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $serviceid,
                    'userid' => $service->userid,
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' =>lang('Baring Request for').' ' . $mobile->msisdn . ' '.lang('has been failed')
                ));
                echo json_encode(array(
                    'result' => false,
                    'message' => 'error on unblocking the services'
                ));
                exit;
            }
        } //!empty($_POST['msisdn']) && isset($_POST['SN'])
        else {
            echo json_encode(array(
                'result' => false,
                'message' => lang('This subscription is not correctly configured in Portal please contact our sysadmin')
            ));
        }
        //redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function suspend()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if (!empty($_POST['msisdn']) && isset($_POST['SN'])) {
            $result = $this->artilium->UpdateCLI($_POST['SN'], 0);
            //$result = $this->Admin_model->artiliumPost(array('companyid' => $this->data['mobile']->iCompanyNbr, 'type' => $this->data['mobile']->PaymentType, 'action' => 'UpdateCLI', 'Status' => '0', 'SN' => $_POST['SN']));
            //print_r(array('companyid' => $this->data['mobile']->iCompanyNbr, 'type' => $this->data['mobile']->PaymentType, 'action' => 'UpdateParametersCLI', 'ParameterId' => 20541, 'ParameterValue' => $_POST['typebar'], 'SN' => $_POST['SN']));
            if ($result->result == "success") {
                if (!empty($_POST['suspend_reason'])) {
                    $this->Admin_model->update_services($_POST['serviceid'], array(
                        'suspend_reason' => $_POST['suspend_reason']
                    ));
                } //!empty($_POST['suspend_reason'])
                $this->Admin_model->ChangeStatusService($_POST['serviceid'], 'Suspended');
                $pack = $this->artilium->GetListPackageOptionsForSn(trim($_POST['SN']));
                $this->artilium->UpdateServices(trim($_POST['SN']), $pack, '0');
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $_POST['userid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => lang('Baring Request for').' ' . $_POST['msisdn'] . ' '.lang('has been executed successfully')
                ));
                $this->session->set_flashdata('success', $_POST['msisdn'] . lang('has been suspended as requested.'));
                //$this->send_simblock_email($_POST['serviceid']);
                $this->sendmail($_POST['serviceid'], 'service', 'service_suspended');
            } //$result->result == "success"
            else {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $_POST['userid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Baring Request for ' . $_POST['msisdn'] . ' has been failed'
                ));
                $this->session->set_flashdata('error', $_POST['msisdn'] . ' '.lang('failed to be suspended.'));
            }
        } //!empty($_POST['msisdn']) && isset($_POST['SN'])
        else {
            $this->session->set_flashdata('error', lang('failed to execute, msisdn,typebar and SN must not be empty'));
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function unsuspend()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if (!empty($_POST['msisdn']) && isset($_POST['SN'])) {
            $result = $this->artilium->UpdateCLI($_POST['SN'], 1);
            $this->Admin_model->removeStolen($_POST['serviceid']);
            if ($result->result == "success") {
                $this->Admin_model->ChangeStatusService($_POST['serviceid'], 'Active');
                $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($_POST['SN']));
                $this->artilium->UpdateServices(trim($_POST['SN']), $pack, '1');
                $this->sendmail($_POST['serviceid'], 'service', 'service_unsuspended');
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $_POST['userid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Service  ' . $_POST['msisdn'] . ' '.lang('has been Unsuspended')
                ));
                $this->session->set_flashdata('success', $_POST['msisdn'] . lang('has been unsuspended as requested.'));
            } //$result->result == "success"
            else {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $_POST['userid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Service  ' . $_POST['msisdn'] . 'has been failed to be released'
                ));
                $this->session->set_flashdata('error', $_POST['msisdn'] . lang(' has been released.'));
            }
        } //!empty($_POST['msisdn']) && isset($_POST['SN'])
        else {
            $this->session->set_flashdata('error', lang('failed to execute, msisdn,typebar and SN must not be empty'));
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function update_sim_language()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $whmcsid               = $_POST['userid'];
        $this->data['service'] = $this->Admin_model->getService($whmcsid);
        if ($this->data['setting']->companyid == 2) {
            $companyid = array(
                1,
                2,
                3,
                4,
                6,
                8,
                9,
                25,
                26,
                28,
                29,
                31,
                34,
                35,
                37,
                41,
                47,
                50
            );
        } //$this->data['setting']->companyid == 2
        else {
            $companyid[] = $this->companyid;
        }
        if ($this->Admin_model->IsAllowedMobile($_POST['msisdn'], $companyid)) {
            $result = $this->artilium->UpdateLanguage($_POST['SN'], $_POST['language']);
            if ($result->result == "success") {
                $this->db->query("update a_services_mobile set msisdn_languageid = ? where serviceid=?", array(
                    $_POST['language'],
                    $_POST['serviceid']
                ));
                $this->magebo->ChangeSimLanguage($_POST['msisdn'], $_POST['language']);
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $_POST['userid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Voicemail language for  ' . $_POST['msisdn'] . ' '.lang('has been changed to').' ' . $_POST['language']
                ));
                $this->session->set_flashdata('success', lang('Language has been changed.'));
            } //$result->result == "success"
            else {
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'serviceid' => $_POST['serviceid'],
                    'userid' => $_POST['userid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Mobile: ' . $_POST['msisdn'] . ' '.lang('Failed to change the voicemail language')
                ));
                $this->session->set_flashdata('error', lang('There were an error while changing your voice mail language.'));
            }
        } //$this->Admin_model->IsAllowedMobile($_POST['msisdn'], $companyid)
        else {
            $this->session->set_flashdata('error', 'Access denied');
        }
        //print_r($_POST);
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function addon2order()
    {
        $addons = $_POST['addon'];
        foreach ($addons as $row) {
            $addon = getAddonid($_POST['type'], $row);
            $this->db->insert('a_services_addons', array(
                   'companyid' => $this->companyid,
                'serviceid' => $_POST['serviceid'],
                'addonid' => $row,
                'recurring_total' => $addon->recurring_total,
                'addon_type' => $addon->addon_type,
                'name' => $addon->name,
                'arta_bundleid' => $addon->bundleid,
                'cycle' => $addon->cycle,
                'terms' => $addon->terms
            ));
        } //$addons as $row
        redirect('admin/subscription/edit_order/' . $_POST['serviceid']);
    }
    public function handset2order()
    {
        $recurring_total = $_POST['handset_price'] / $_POST['handset_month'];
        $addon           = getAddonid($_POST['type'], $_POST['addonid']);
        $this->db->insert('a_services_addons', array(
            'companyid' => $this->companyid,
            'serviceid' => $_POST['serviceid'],
            'name' => $_POST['handset_name'],
            'terms' => $_POST['handset_month'],
            'addonid' => $_POST['addonid'],
            'recurring_total' => round($recurring_total, 4),
            'addon_type' => $addon->addon_type
        ));
        $this->session->set_flashdata('success', lang('Handset has been inserted'));
        redirect('admin/subscription/edit_order/' . $_POST['orderid']);
        //echo $id;
    }
    public function changeProduct_addon()
    {

        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $this->uri->segment(4),
            'serviceid' =>$_POST['serviceid'],
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Change Product From: '.getProductName($_POST['oldid']).' to :'.getProductName($_POST['newid']).' Contract Term :'.$_POST['contract_terms'].' months'
            ));


        $this->Admin_model->ChangeProductAddonId($_POST);
        //echo json_encode('result' => true);
        echo json_encode($_POST);
    }
    public function delete_addon()
    {
        $i = $this->Admin_model->deleteAddon($_POST);
        if ($i) {
            echo json_encode(array(
                'result' => true
            ));
        } //$i
        else {
            echo json_encode(array(
                'result' => false
            ));
        }
    }
    public function edit_order()
    {
        $this->data['addons'] = array();
        $id                   = $this->uri->segment(4);
        //echo $id;
        $this->db             = $this->load->database('default', true);
        $h                    = $this->db->query("select a.*,b.name from a_services a left join a_products b on b.id=a.packageid where a.id=?", array(
            $id
        ));
        if ($h->row()->status !== "New") {
            $this->session->set_flashdata('error', lang('You can not edit This order any longer, it has been passed the editable periode'));
            redirect('admin/subscription');
            exit;
        } //$h->row()->status !== "New"
        $this->data['service'] = $h->row();
        //print_r($h->row());
        if ($h->row()->type == "mobile") {
            $addons = $this->db->query("select a.*, case when a.name  = '' THEN b.name
       else a.name end as name from a_services_addons a left join a_products_mobile_bundles b on b.id=a.addonid where a.serviceid=?", array(
                $id
            ));
        } //$h->row()->type == "mobile"
        elseif ($h->row()->type == "voip") {
            $addons = $this->db->query("select a.*,case when a.name  = '' THEN b.name
       else a.name end as name  from a_services_addons a left join a_products_voip_bundles b on b.id=a.addonid where a.serviceid=?", array(
                $id
            ));
        } //$h->row()->type == "voip"
        elseif ($h->row()->type == "xdsl") {
            $addons = $this->db->query("select a.*,case when a.name  = '' THEN b.name
       else a.name end as name  from a_services_addons a left join a_products_xdsl_bundles b on b.id=a.addonid where a.serviceid=?", array(
                $id
            ));
        } //$h->row()->type == "xdsl"
        else {
            $addons = $this->db->query("select a.*,case when a.name  = '' THEN b.name
       else a.name end as name  from a_services_addons a left join a_products_mobile_bundles b on b.id=a.addonid where a.serviceid=?", array(
                $id
            ));
        }
        if ($addons->num_rows() > 0) {
            $this->data['addons'] = $addons->result();
        } //$addons->num_rows() > 0
        //print_r($this->data['addons']);
        //exit;
        $this->data['contractdate']  = $this->data['service']->date_contract;
        $this->data['mobile_status'] = 0;
        $this->data['client']        = $this->Admin_model->getClient($this->data['service']->userid);
        $this->data['title']         = "Edit Order Information";
        $this->data['main_content']  = admin_theme($this->data['setting']->default_theme) . 'mobile_edit_order';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function open_by_number()
    {
        $query2 = $this->db->query("SELECT 'service' as type, id as value,concat('MSISDN #',id,' ',domain) as label, orderid as companyname from tblhosting where domain like ?  order by domain DESC LIMIT 0, 40", array(
            $keyword
        ));
        $query3 = $this->db->query("SELECT 'service' as type, relid as value,concat('MSISDN #',relid,' ',value) as label, relid as companyname from tblcustomfieldsvalues where value like ? and fieldid in ('6','15') and value is not null group by relid order by relid DESC LIMIT 0, 40", array(
            $keyword
        ));
    }
    public function pendingporting()
    {
        date_default_timezone_set('Europe/Paris');
        $this->data['last_updated'] = gmdate("Y-m-d H:i:s", file_get_contents(APPPATH . 'queue/' . $this->session->cid . '_portin.date') + 7200);
        $this->data['title']        = lang("Pending Porting List");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'portin';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function pending_porting_out()
    {
        $this->data['title']        = lang("Pending Porting List");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'portout';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function add_internet()
    {
        $this->data['title']        = lang("Create Services");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscription_add_internet';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function add_mobile()
    {
        $this->data['title']        = lang("Create Services");
        /* Deprecated need to be updated soon to getPlatform($_POST['serviceid']) */
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscription_add_mobile_'.strtolower(trim($this->data['setting']->mobile_platform));
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function addorder_internet()
    {
        $this->load->model('Api_model');
        $req = $this->Api_model->checkRequired(array(
            'firstname',
            'lastname',
            'phonenumber',
            'email',
            'address1',
            'postcode',
            'city',
            'country',
            'language',
            'paymentmethod'
        ), $_POST);
        if ($req['result'] == "error") {
            echo json_encode(array(
                'result' => false,
                'message' => $req['messa']
            ));
            exit;
        }
        $this->load->library('magebo', array(
            'companyid' => $this->session->cid
        ));
        if ($_POST['customer_type'] == "new") {
            $recurring          = "0.00";
            $update_handset     = false;
            $pass               = random_str('alphanum', 8);
            $extra              = false;
            $_POST['companyid'] = $this->companyid;
            $cd                 = array(
                'companyid' => $this->companyid,
                'uuid' => gen_uuid(),
                'mvno_id' => null,
                'email' => $_POST['email'],
                'phonenumber' => $_POST['phonenumber'],
                'vat' => $_POST['vat'],
                'companyname' => $_POST['companyname'],
                'salutation' => $_POST['salutation'],
                'initial' => $_POST['initial'],
                'firstname' => $_POST['firstname'],
                'lastname' => $_POST['lastname'],
                'address1' => $_POST['address1'],
                'housenumber' => $_POST['housenumber'],
                'alphabet' => $_POST['alphabet'],
                'postcode' => $_POST['postcode'],
                'city' => $_POST['city'],
                'country' => $_POST['country'],
                'language' => $_POST['language'],
                'paymentmethod' => $_POST['paymentmethod'],
                'password' => password_hash($pass, PASSWORD_DEFAULT),
                'gender' => $_POST['gender'],
                'sso_id' => '0',
                'nationalnr' => $_POST['nationalnr'],
                'date_created' => date('Y-m-d'),
                'invoice_email' => $_POST['invoice_email'],
                'iban' => '',
                'vat_exempt' => $_POST['vat_exempt']
            );
            $magebo             = $this->magebo->AddClient($cd);
            if ($magebo->result != "success") {
                echo json_encode(array(
                    'result' => false,
                    'message' => 'Failed to insert customer in to magebo'
                ));
                exit;
            } else {
                $cd['mageboid'] = $magebo->iAddressNbr;
            }
            $clients = $this->Api_model->insertClient($cd);
            if ($clients->result != "success") {
                echo json_encode(array(
                    'result' => false,
                    'message' => $clients->message
                ));
                exit;
            } else {
                //Adding client to magebo after successfull insert
                $userid   = $clients->clientid;
                $password = $this->Admin_model->changePasswordClient($userid);
                /* _to_domain(base_url()), $body);

                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to(strtolower(trim($_     'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
                );
                } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = true;
                }
                $this->email->clear(true);
                $this->email->initialize($config);
                $this->data['info'] = (object) array('email' => strtolower(trim($_POST['email'])), 'password' => $password, 'name' => $_POST['firstname'] . ' ' . $_POST['lastname']);
                $this->data['language'] = $_POST['language'];



                $subject = getSubject('signup_email', $client->language, $client->companyid);


                $body = getMailContent('signup_email', $client->language, $client->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
                $body = str_replace('{$password}', $password, $body);
                $body = str_replace('{$email}', trim(strtolower($client->email)), $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);

                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to(strtolower(trim($_POST['email'])));
                $this->email->bcc('lainard@gmail.com');
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                //mail('simson@exocom.be', 'email sent', 'email really sent');
                } else {
                //mail('simson@exocom.be', 'email not sent', 'email really not sent.' . $this->email->print_debugger());
                //echo json_encode(array('result' => false, 'error' => $this->email->print_debugger()));
                }
                */
            }
        } else {
            $userid = $_POST['userid'];
        }
        if ($_POST['migration'] == "no") {
            $type_order = "PROVIDE";
        } else {
            $type_order = "CHANGE_OWNER";
        }
        $cc          = $this->Admin_model->getClient($userid);
        $pricing     = getMageboProduct($_POST['packageid']);
        $order_array = array(
            'companyid' => $this->companyid,
            'type' => 'xdsl',
            'status' => 'Pending',
            'packageid' => $_POST['packageid'],
            'userid' => $userid,
            'date_created' => date('Y-m-d'),
            'billingcycle' => 'Monthly',
            'date_contract' => date('m-d-Y'),
            'contract_terms' => 6,
            'recurring' => $pricing->recurring_total,
            'notes' => 'Order via portal by: ' . $this->session->firstname . ' ' . $this->session->lastname
        );
        $assign      = get_promotion_auto_assign($_POST['packageid']);
        $serviceid   = $this->Api_model->insertOrder($order_array);
        $pro         = $this->Admin_model->getProximusProduct($_POST['packageid']);
        $proximus    = array(
            'userid' => $userid,
            'serviceid' => $serviceid,
            'companyid' => $this->companyid,
            'appointment_date_requested' => $_POST['dateRequested']
        );
        $this->Admin_model->AssignProximusOrderid1($proximus, $_POST['proximus_orderid']);
        if (!empty($_POST['voipnumber'])) {
            $this->Admin_model->update_services_data('xdsl', $serviceid, array('dialnumber' => $_POST['voipnumber']));
        }

        $proximus['product_name'] = $pro->productname;
        $proximus['product_id']   = $pro->productid;
        $proximus['type_order']   = $type_order;
        $proximus['last_updated'] = date('Y-m-d H:i:s');
        if (!empty($_POST['circuitid'])) {
            $proximus['circuitid'] = $_POST['circuitid'];
        }
        $this->Admin_model->AssignProximusOrderid2($proximus, $_POST['proximus_orderid']);
        if ($serviceid) {
            $this->Admin_model->CreateModemSubscription($serviceid, $_POST['modem'], $pricing->name);
            $this->session->set_flashdata('success', 'Serviceid ' . $serviceid . ' '.lang('has been created'));
            $client = $this->Admin_model->getClient($userid);
            if ($_POST['modem'] == "2") {
                $modem = true;
            } else {
                $modem = false;
            }
            $this->create_proforma($this->companyid, $client, $_POST['packageid'], $serviceid);
            echo json_encode(array(
                'result' => true,
                'serviceid' => $serviceid
            ));
        } else {
            echo json_encode(array(
                'result' => false,
                'serviceid' => $serviceid
            ));
        }
    }

    public function getproducts()
    {
        $html     = array();
        $this->db = $this->load->database('default', true);
        $id       = $_POST['id'];
        $q        = $this->db->query("select id,name from tblproducts where gid=?", array(
            $id
        ));
        if ($q->num_rows() > 0) {
            $html = $q->result_array();
        } //$q->num_rows() > 0
        echo json_encode($html);
    }
    public function pendingorders()
    {
        $this->data['title']        = lang("Pending Orders/Services");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscription_pending_orders';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }

    public function add()
    {
        $this->data['title']        = lang("Pending Porting List");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'subscription_add';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function execute_portin()
    {
        print_r($_POST);
    }
    public function reject_portout()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $condition = $this->magebo->GetFinancial($_POST['msisdn']);
        $PortingId = $_POST['PortingId'];
        $data      = array(
            'Sn' => $_POST['RejectId']
        );
        if (!empty($_POST["FirstPossibleDate"])) {
            $data['FirstPossibleDate'] = $_POST['FirstPossibleDate'];
        }
        $result = $this->artilium->RejectPortOut($PortingId, $data);
        echo json_encode($result);
    }
    public function addSum()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->uri->segment(4)
        ));
        $result = $this->artilium->createSUMassignment($_POST['sn']);
        echo json_encode($result);
    }
    public function process_porting()
    {
        $this->load->model('Admin_model');
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        unset($_POST['serviceid']);
        if ($_POST['Type'] == "Accept") {
            $template = 'portout_accept';
            $result   = $this->artilium->AcceptPortOut($_POST);
            // mail('mail@simson.one','Process Porting', print_r($_POST, true). print_r($result, true));
            if ($result->result == "success") {
                $sn      = $this->uri->segment(4);
                $service = $this->Admin_model->getServiceBySN($sn);
                if ($service) {
                    $this->Admin_model->update_services($service->serviceid, array(
                        'date_terminate' => $_POST['FirstPossibleDate']
                    ));
                    $simmy = explode(' ', $_POST);
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'userid' => $service->userid,
                        'serviceid' => $service->serviceid,
                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'SIMCARD Data ' . $simmy . ' for serviceid ' . $service->serviceid . ' has been requested to be Terminated on: ' . $_POST['FirstPossibleDate']
                    ));
                    $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
                    $body    = "Termination request has just been reqested by : " . $this->session->firstname . " \n\nPlease terminate and create prorata for this customer\n\nMsisdn: " . $service->msisdn . "\nTermination Date: " . $_POST['FirstPossibleDate'] . "\niAddressNbr: " . $service->mageboid . "\n\nTo Get the list of Termination requests, go to: " . base_url() . "admin/subscription/termination_service_list";
                    mail('thierry.van.eylen@united-telecom.be', 'Manual Termination', $body, $headers);
                } else {
                    //  mail('mail@simson.one', 'Portin Accepted but Serviceid Not found', print_r($_POST, true));
                }
            }
        } else {
            $template  = 'portout_rejected';
            $PortingId = $_POST['PortingId'];
            $data      = array(
                'Sn' => '      ' . $_POST['Sn'],
                'RejectId' => $_POST['RejectId'],
                'FirstPossibleDate' => $_POST['FirstPossibleDate']
            );
            $sn        = $this->uri->segment(4);
            $service   = $this->Admin_model->getServiceBySN($_POST['Sn']);
            $result    = $this->artilium->RejectPortOut($PortingId, $data);
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $service->userid,
                'serviceid' => $service->serviceid,
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Has ben reject the porting request'
            ));
        }
        if ($result->result == "success") {
            $extradata = array('porting_date' => $_POST['FirstPossibleDate']);
            $this->sendmail($service->serviceid, 'service', $template, $extradata);
            sleep(5);
            $this->session->set_flashdata('success', lang('Request has been ' . $_POST['Type'] . 'ed'));
        } else {
            $this->session->set_flashdata('error', 'Failed to ' . $_POST['Type'] . ' '.lang('portout reason').': ' . $result->message);
        }
        echo json_encode((array) $result);
    }
    public function export_pending_portin()
    {
        $this->db->where('id', $this->session->id);
        $this->db->update('a_admin', array(
            'request_export_portinlist' => 1
        ));
        echo json_encode(array(
            'result' => $this->db->affected_rows()
        ));
    }
    public function accept_portout()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $result = $this->artilium->AcceptPortOut($_POST);

        echo json_encode($result);
    }
    public function export_livecdr()
    {
        set_time_limit(0);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('xlswriter');
        $serviceid = $this->uri->segment(5);
        $mobile    = $this->Admin_model->getService($serviceid);
        $c         = $this->artilium->get_cdr($mobile);
        $header    = array(
            lang('Begintime') => 'string',
            lang('Type') => 'string',
            lang('Destination') => 'string',
            lang('Description') => 'string',
            lang('Duration') => 'string',
            lang('Cost') => 'string'
        );
        if (count($c['cdr']) > 0) {
            foreach ($c['cdr'] as $x) {
                if ($x['TrafficTypeId'] == 2) {
                    $x['DurationConnection'] = convertToReadableSize($x['DurationConnection']);
                }
                if ($x['DurationConnection'] != "0 bytes") {
                    $cdr[] = array(
                        'Begintime' => $x['Begintime'],
                        'TrafficTypeId' => getTraficName($x['TrafficTypeId']),
                        'MaskDestination' => $x['MaskDestination'],
                        'Description' => $x['DestinationCountry'],
                        'DurationConnection' => $x['DurationConnection'],
                        'CurNumUnitsUsed' => $x['CurNumUnitsUsed']
                    );
                }
            } //$c['cdr'] as $x
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $this->xlswriter->writeSheet($cdr, 'Sheet1', $header);
            $this->xlswriter->setAuthor('Simson Parlindungan');
            $this->xlswriter->setTitle('LIVE CDR Export');
            $this->xlswriter->setCompany('United');
            echo $this->xlswriter->writeToString();
        } //count($c['cdr']) > 0
        else {
            $this->session->set_flashdata('error', 'no data to be exported');
            redirect('admin/subscription/detail/' . $serviceid);
        }
    }
    public function changeorder_startcontract()
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('id', $_POST['serviceid']);
        $this->db->update('a_services', array(
            'date_contract' => $_POST['date']
        ));
    }
    public function changePromotion()
    {
        logAdmin(array(
            'companyid' => $this->companyid,
            'userid' => $this->uri->segment(4),
            'serviceid' => $_POST['serviceid'],
            'user' => $this->session->firstname . ' ' . $this->session->lastname,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'description' => 'Change Promotion code To promotion: '.getPromonamebyId($_POST['id'])
            ));
        $this->Admin_model->ApplyPromotion($_POST);
        echo json_encode($_POST);
    }
    public function fill_sn()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->db = $this->load->database('local', true);
        $q        = $this->db->query('select * from batch_suspend   limit 3');
        //$this->load->library('artilium', array('20', PLATFORM));
        foreach ($q->result() as $row) {
            //print_r($row);
            //sn$sn = $this->Admin_model->mageboGet('GetSim/' . substr(trim($row->msisdn), -9));
            $sn = $this->artilium->GetSn($row->msisdn);
            //$this->update_sn($row->id, array('sn' => $sn->SN));
            print_r($sn);
            //unset($sn);
        } //$q->result() as $row
    }
    public function update_sn($id, $data)
    {
        $this->db = $this->load->database('local', true);
        $this->db->where('id', $id);
        $this->db->update('batch_suspend', $data);
    }
    public function get_pending_port_out()
    {
        // error_reporting(1);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if ($this->companyid == 55) {
            $list = $this->artilium->GetPendingPorts();
        } else {
            $list = $this->artilium->GetPendingPortOuts();
            if ($this->session->email == "simson.parlindungan@united-telecom.be") {
                //print_r($list);
            }
        }


        $dd = array();
        if ($list->GetPendingPortOutsResult->TotalItems > 1) {
            foreach ($list->GetPendingPortOutsResult->ListInfo as $key => $row) {
                if ($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status != "CANCELED") {
                    //  if(empty($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Validity->EndDate)){
                    //print_r($row);
                    $s['Id']            = $row->PortingId;
                    $s['OperationType'] = "Port Out";
                    $s['Status']        = $row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status;
                    $s['MSISDN']        = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['ActionDate']    = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['Remark']        = "Please accept or reject";
                    $s['CustomerName']  = getCustomernamebySerial(trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn));
                    foreach ($row->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity as $p) {
                        if ($p->ParameterId == 20512) {
                            if ($p->ParameterValue == 13) {
                                $s['Status'] = "Accepted";
                            } elseif ($p->ParameterValue == 27) {
                                $s['Status'] = "Portin-Failed";
                            }
                        }
                        if ($p->ParameterId == "20500") {
                            $s['DonorOperator'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20527") {
                            $s['ActionDate'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20506") {
                            $s['AccountNumber'] = $p->ParameterValue;
                        }
                        $s['RequestType'] = "Port Out";
                    }
                    if (!in_array($s['Status'], array(
                        "Accepted",
                        "Portin-Failed"
                    ))) {
                        $s['button'] = ' <button type="button"class="btn btn-success btn-sm pxs"  onclick="OpenReject(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Reject</button>
                    <button type="button"class="btn btn-danger btn-sm pxs"  onclick="OpenAccept(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Accept</button>';
                    } else {
                        $s['button'] = 'Processed';
                        /*
                        $s['button'] = ' <button type="button"class="btn btn-success btn-sm pxs"  onclick="OpenReject(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Reject</button>
                        <button type="button"class="btn btn-danger btn-sm pxs"  onclick="OpenAccept(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Accept</button>';
                        */
                    }
                    $dd[] = $s;
                    //   } //$row['Status'] != "CANCELED"
                }
            } //$list as $key => $row
        } elseif ($list->GetPendingPortOutsResult->TotalItems == 1) {
            $row = $list->GetPendingPortOutsResult->ListInfo;

            if ($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status != "CANCELED") {
                // if (empty($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Validity->EndDate)) {
                //print_r($row);
                $s['Id']            = $row->PortingId;
                $s['OperationType'] = "Port Out";
                $s['Status']        = $row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status;
                $s['MSISDN']        = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                $s['ActionDate']    = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                $s['Remark']        = "Please accept or reject";
                $s['CustomerName']  = getCustomernamebySerial(trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn));
                foreach ($row->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity as $p) {
                    if ($p->ParameterId == 20512) {
                        if ($p->ParameterValue == 13) {
                            $s['Status'] = "Accepted";
                        } elseif ($p->ParameterValue == 27) {
                            $s['Status'] = "Portin-Failed";
                        }
                    }
                    if ($p->ParameterId == "20500") {
                        $s['DonorOperator'] = $p->ParameterValue;
                    }
                    if ($p->ParameterId == "20527") {
                        $s['ActionDate'] = $p->ParameterValue;
                    }
                    if ($p->ParameterId == "20506") {
                        $s['AccountNumber'] = $p->ParameterValue;
                    }
                    $s['RequestType'] = "Port Out";
                }
                if (!in_array($s['Status'], array(
                        "Accepted",
                        "Portin-Failed"
                    ))) {
                    $s['button'] = ' <button type="button"class="btn btn-success btn-sm pxs"  onclick="OpenReject(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Reject</button>
                    <button type="button"class="btn btn-danger btn-sm pxs"  onclick="OpenAccept(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Accept</button>';
                } else {
                    $s['button'] = 'Processed';
                    /*
                    $s['button'] = ' <button type="button"class="btn btn-success btn-sm pxs"  onclick="OpenReject(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Reject</button>
                    <button type="button"class="btn btn-danger btn-sm pxs"  onclick="OpenAccept(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Accept</button>';
                    */
                }
                $dd[] = $s;
                //  } //$row['Status'] != "CANCELED"
            }
        }
        echo json_encode($dd);
    }
    public function get_pending_port_out_pending()
    {
        $totali = 0;
        // error_reporting(1);
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        if ($this->companyid == 55) {
            $list = $this->artilium->GetPendingPorts();
        } else {
            $list = $this->artilium->GetPendingPortOuts();
        }
        $dd = array();
        if ($list->GetPendingPortOutsResult->TotalItems > 1) {
            foreach ($list->GetPendingPortOutsResult->ListInfo as $key => $row) {
                if ($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status != "CANCELED") {
                    //  if(empty($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Validity->EndDate)){
                    //print_r($row);
                    $s['Id']            = $row->PortingId;
                    $s['OperationType'] = "Port Out";
                    $s['Status']        = $row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status;
                    $s['MSISDN']        = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['ActionDate']    = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['Remark']        = "Please accept or reject";
                    $s['CustomerName']  = getCustomernamebySerial(trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn));
                    foreach ($row->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity as $p) {
                        if ($p->ParameterId == 20512) {
                            if ($p->ParameterValue == 13) {
                                $s['Status'] = "Accepted";
                            } elseif ($p->ParameterValue == 27) {
                                $s['Status'] = "PortoutFailed";
                            } else {
                                $totali = $totali + 1;
                            }
                        }
                        if ($p->ParameterId == "20500") {
                            $s['DonorOperator'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20527") {
                            $s['ActionDate'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20506") {
                            $s['AccountNumber'] = $p->ParameterValue;
                        }
                        $s['RequestType'] = "Port Out";
                    }
                    $s['button'] = ' <button type="button"class="btn btn-success btn-sm pxs"  onclick="OpenReject(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Reject</button>
                    <button type="button"class="btn btn-danger btn-sm pxs"  onclick="OpenAccept(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Accept</button>';
                    $dd[]        = $s;
                    //   } //$row['Status'] != "CANCELED"
                }
            } //$list as $key => $row
        } elseif ($list->GetPendingPortOutsResult->TotalItems == 1) {
            $row = $list->GetPendingPortOutsResult->ListInfo;
            if ($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status != "CANCELED") {
                if (empty($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Validity->EndDate)) {
                    //print_r($row);
                    $s['Id']            = $row->PortingId;
                    $s['OperationType'] = "Port Out";
                    $s['Status']        = $row->PortSubscriberList->PortSubscriberDataEntity->PinData->Status;
                    $s['MSISDN']        = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['ActionDate']    = $row->PortSubscriberList->PortSubscriberDataEntity->Msisdn;
                    $s['Remark']        = "Please accept or reject";
                    $s['CustomerName']  = getCustomernamebySerial(trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn));
                    foreach ($row->PortSubscriberList->PortSubscriberDataEntity->Parameters->ParameterDataEntity as $p) {
                        if ($p->ParameterId == 20512) {
                            if ($p->ParameterValue == 13) {
                                $s['Status'] = "Accepted";
                            } elseif ($p->ParameterValue == 27) {
                                $s['Status'] = "PortoutFailed";
                            } else {
                                $totali = $totali + 1;
                            }
                        }
                        if ($p->ParameterId == "20500") {
                            $s['DonorOperator'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20527") {
                            $s['ActionDate'] = $p->ParameterValue;
                        }
                        if ($p->ParameterId == "20506") {
                            $s['AccountNumber'] = $p->ParameterValue;
                        }
                        $s['RequestType'] = "Port Out";
                    }
                    $s['button'] = ' <button type="button"class="btn btn-success btn-sm pxs"  onclick="OpenReject(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Reject</button>
                    <button type="button"class="btn btn-danger btn-sm pxs"  onclick="OpenAccept(\'' . trim($row->PortSubscriberList->PortSubscriberDataEntity->PinData->Sn) . '\');">Accept</button>';
                    $dd[]        = $s;
                } //$row['Status'] != "CANCELED"
            }
        }
        echo json_encode(array(
            'total' => $totali
        ));
    }
    public function enable_all_services()
    {
        $id     = $this->uri->segment(4);
        $mobile = $this->Admin_model->getService($id);
        $this->load->library('artilium', array(
            'companyid' => $mobile->details->companyid
        ));
        if ($mobile->details->companyid == $this->session->cid) {
            $pack = $this->artilium->GetListPackageOptionsForSnAdvance(trim($mobile->details->msisdn_sn));
            $this->artilium->UpdateServices(trim($mobile->details->msisdn_sn), $pack, '1');
            $this->session->set_flashdata('success', 'All services has been enabled');
            $res = true;
        } else {
            $this->session->set_flashdata('success', 'Failed to enable all services');
            $res = false;
        }
        redirect('admin/subscription/detail/' . $id);
    }
    public function get_pending()
    {
        $rep = "CLI;Clientid;EMAIL\n";
        if (!empty($this->uri->segment(4))) {
            $sts = $this->uri->segment(4);
        } else {
            $sts = "All";
        }
        $tr = "";
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        //$this->load->library('artilium', array(COMPANYID, PLATFORM));
        $dd   = array();
        $list = $this->artilium->GetPendingPortIns();
        $s    = null;
        if (!empty($list)) {
            if ($this->companyid == 53) {
                file_put_contents(FCPATH.'assets/portin_delta.log', print_r($list, true));
            }

            //mail('mail@simson.one', 'Portin pending status',print_r($list, true));
            foreach ($list as $key => $row) {
                if (!in_array(trim($row['Status']), array(
                    "CANCELED",
                    "Port in Canceled"
                ))) {
                    //print_r($row);
                    $s['Id']            = $key;
                    $s['OperationType'] = $row['OperationType'];
                    $s['RequestType']   = $row['RequestType'];
                    $s['DonorOperator'] = $row['DonorOperator'];
                    $s['MSISDN']        = $row['MSISDN'];
                    $s['SN']            = $row['SN'];
                    if (!empty($row['Remark'])) {
                        $s['Remark'] = $row['Remark'];
                    } else {
                        $s['Remark'] = '';
                    }
                    $e                 = explode(' ', $row['EnteredDate']);
                    $s['EnteredDate']  = str_replace('/', '-', $e['0']);
                    $s['ActionDate']   = str_replace(' 00:00:00', '', str_replace('/', '-', $row['ActionDate']));
                    $s['CustomerName'] = getCustomernamebySerial(trim($row['SN']));
                    if (hasNote($s['MSISDN'])) {
                        $s['InfoNote'] = "<button  onclick=\"openNotes(" . $s['MSISDN'] . ");\"><i class=\"fa fa-info\"></i></button>";
                    } else {
                        $s['InfoNote'] = "";
                    }
                    /*
                    // Send List Pending Verification
                    if(trim($row["Status"]) == "Port in Wait for verificationcode"){

                    $rep .= $row['MSISDN'].";".getCustomerEmailBySerial($row['MSISDN'], $row['SN']);
                    }

                    */
                    if ($row['Country'] == "NL") {
                        if ($row['ErrorID'] == "0") {
                            $s['Remark'] = $row['ErrorComment'];
                        } elseif ($row['ErrorID'] == "27") {
                            $s['Remark'] = lang("Telephone number and customer ID do not correspond");
                        } elseif ($row['ErrorID'] == "63") {
                            $s['Remark'] = lang("beyond notice terms / Porting not possible within porting window");
                        } elseif ($row['ErrorID'] == "99") {
                            $s['Remark'] = lang("Other");
                        } elseif ($row['ErrorID'] == "41") {
                            $s['Remark'] = lang("Multiple number types are not allowed.");
                        } elseif ($row['ErrorID'] == "44") {
                            $s['Remark'] = lang("Multiple DNOs or DSPs not allowed.");
                        } elseif ($row['ErrorID'] == "45") {
                            $s['Remark'] = lang("Telephone number is not portable");
                        } elseif ($row['ErrorID'] == "02") {
                            $s['Remark'] = lang("Porting request for this/these number(s) is already in progress");
                        } elseif ($row['ErrorID'] == "01") {
                            $s['Remark'] = lang("Other");
                        } else {
                            $s['Remark'] = "";
                        }
                    }
                    if ($row['Status'] == "Port in Rejected") {
                        $s['Status'] = '<a href="javascript:void(0)" class="data-toggle="tooltip" data-placement="top" title="">' . $row['Status'] . '</a>';
                    } //$row['Status'] == "Port in Rejected
                    elseif ($row['Status'] == "Port in Cancelled") {
                        $s['Status'] = "batal";
                    } //$row['Status'] == "Port in Rejected"
                    else {
                        $s['Status'] = $row['Status'];
                    }
                    if ($row['OperationType'] == "PORT_OUT") {
                        if ($row['Status'] != 'ACCEPTED') {
                            $s['button'] = ' <button type="button"class="btn btn-success btn-sm"  onclick="OpenAccept(\'' . $row['MSISDN'] . '\', \'' . $row['SN'] . '\');">ACCEPT</button>
                    <button type="button"class="btn btn-danger btn-sm"  onclick="OpenReject(\'' . $row['MSISDN'] . '\');">REJECT</button>';
                        } //$row['Status'] != 'ACCEPTED'
                    } //$row['OperationType'] == "PORT_OUT"
                    else {
                        if ($row['Status'] == 'Port in Rejected') {
                            $s['button'] = ' <button type="button" class="btn btn-primary btn-sm" onclick="OpenExecute(\'' . $row['MSISDN'] . '\');">Rexecute</button>';
                        } //$row['Status'] == 'Port in Rejected'
                        elseif ($row['Status'] == 'Port in Wait for verificationcode') {
                            $s['button'] = ' <button type="button" class="btn btn-primary btn-sm" onclick="OpenPod(\'' . $row['MSISDN'] . '\');">Porting on Demand</button>';
                        } elseif ($row['Status'] == 'Port in Failed') {
                            $s['button'] = '  <button type="button" class="btn btn-primary btn-sm" onclick="OpenExecute(\'' . $row['MSISDN'] . '\');">EXECUTE</button>';
                        } //$row['Status'] == 'Port in Accepted'
                        else {
                            $s['button'] = '';
                        }
                        $s['button'] .= '<button type="button" class="btn btn-primary btn-sm" onclick="OpenCancel(\'' . $row['SN'] . '\');">Cancel</button>';
                    }
                    //unset($row);
                    if ($s != null) {
                        //echo $sts;
                        if ($sts == "Rejected") {
                            if (trim($row["Status"]) == "Port in Rejected") {
                                $dd[] = $s;
                            }
                        } elseif ($sts == "Pending") {
                            if (in_array($row["Status"], array(
                                "Port in Wait for verificationcode",
                                "Port in Not yet complete",
                                "Port in Pending"
                            ))) {
                                $dd[] = $s;
                            }
                        } elseif ($sts == "Accepted") {
                            if (trim($row["Status"]) == "Port in Accepted") {
                                $dd[] = $s;
                            }
                        } elseif ($sts == "Fails") {
                            if (trim($row["Status"]) == "Port in Failed") {
                                $dd[] = $s;
                            }
                        } else {
                            $dd[] = $s;
                        }
                    }
                } //$row['Status'] != "CANCELED"
            } //$list as $key => $row
        } //!empty($list)
        // mail('mail@simson.one','List Pending Verification', $rep);
        echo json_encode($dd);
    }
    public function change_service()
    {
        $this->db = $this->load->database('default', true);
        if (isPost()) {
            $_POST['companyid'] = $this->companyid;
            $d                  = $_POST;
            unset($d['charge']);
            unset($d['amount']);
            //unset($d['ContractDuration']);
            $d['new_price'] = preg_replace("/[^0-9,.]/", "", $d['new_price']);
            $this->db->insert('a_subscription_changes', $d);
            if ($this->db->insert_id() > 0) {




                // $this->send_service_change_request_email($_POST['serviceid'], $_POST['new_pid']);
                $service = $this->Admin_model->getService($_POST['serviceid']);
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $service->userid,
                    'serviceid' =>$_POST['serviceid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => 'Request for Service Changes For:'.$service->details->msisdn. ' From: '.getProductName($_POST['old_pid']).' to '.getProductName($_POST['new_pid']).' ON: ' . $_POST['date_commit']
                    ));
                $client  = $this->Admin_model->getClient($service->userid);
                // sending customer notification
                $this->send_service_change_request_email($_POST['serviceid'], $_POST['new_pid'], str_replace('.', ',', $d['new_price']));
                // sending Billing manager notification
                $headers = "From: noreply@united-telecom.be" . "\r\n" . "CC: simson.parlindungan@united-telecom.be";
                $body    = "";
                $body .= "Hello, \n\n this is a notification of Service request For\n\n Number: " . trim($service->details->msisdn) . "\nSN: " . trim($service->details->msisdn_sn) . "\n\nhttps://mijnmobiel.delta.nl/admin/super/change_service_list\n\nPlease do required action in Magebo , Until we make it automaticly\n\nRegards";
                //mail('thierry.van.eylen@united-telecom.be', 'Delta Manual Service Change Request', $body, $headers);
                $this->session->set_flashdata('success', lang('Your subscription has been planned to be changed on').' ' . $_POST['date_commit']);
                if ($_POST['charge'] == "YES") {
                    $this->load->library('magebo', array(
                        'companyid' => $this->companyid
                    ));
                    if (!empty($_POST['amount'])) {
                        $amount = str_replace(',', '.', $_POST['amount']);
                    } //!empty($_POST['amount'])
                    else {
                        $amount = getChangeCost($service->packageid, $this->companyid);
                    }
                    if ($amount > 0) {
                        $this->magebo->x_magebosubscription_addPricing($client->mageboid, 359, 1, exvat4($client->vat_rate, $amount), 1, 'Subscription Changes Cost ' . $number . ' - ' . date('d/m/Y'), date('m/d/Y'), date('m'), date('Y'), 1, 0, 'Subscription Changes Cost  ' . $number . ' - ' . date('d/m/Y'), 'Subscription Changes Cost ' . $number . ' - ' . date('d/m/Y'), 0);
                    } //$amount > 0
                } //$_POST['charge'] == "YES"
                if (!empty($_POST['agent_request'])) {
                    $this->Admin_model->updateResellerChangeRequest($_POST['agent_request'], $_POST);
                }
            } //$this->db->insert_id() > 0
            else {
                $this->session->set_flashdata('error', lang('Your request was not completed, please contact our support ERROR: ')." " . $this->db->_error_message());
            }
            redirect('admin/subscription/detail/' . $_POST['serviceid']);
        } //isPost()
        else {
            echo "action not permited";
        }
    }
    public function porting_on_demand()
    {
        if (isPost()) {
            echo json_encode($_POST);
            exit;
        } //isPost()
        if (!empty($this->uri->segment(4))) {
            $this->data['msisdn'] = $this->uri->segment(4);
        } //!empty($this->uri->segment(4))
        if (!empty($this->uri->segment(5))) {
            $this->data['sn'] = $this->uri->segment(5);
        } //!empty($this->uri->segment(5))
        if (!empty($this->uri->segment(6))) {
            $this->data['sn'] = $this->uri->segment(5);
        } //!empty($this->uri->segment(6))
        $this->data['title']        = lang("Change Services List");
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'porting_ondemand';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
    public function GetRequestPorting()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $sn = $this->artilium->GetRequestPorting('31628324686');
        print_r($sn);
    }
    public function GetClientsProducts()
    {
        $id = $this->uri->segment(4);
        $f  = $this->Admin_model->getClientProduct($id);
        print_r($f);
    }
    public function sendPortinIntial($id, $client)
    {
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } //$this->data['setting']->smtp_type == 'smtp'
        else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $order = $this->Admin_model->getServiceCLI($id);
        $body  = getMailContent('portin_initiation', $client->language, $this->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body  = str_replace('{$name}', format_name($client), $body);
        $body  = str_replace('{$address1}', $client->address1, $body);
        $body  = str_replace('{$housenumber}', $client->housenumber, $body);
        $body  = str_replace('{$alphabet}', $client->alphabet, $body);
        $body  = str_replace('{$postcode}', $client->postcode, $body);
        $body  = str_replace('{$city}', $client->city, $body);
        $body  = str_replace('{$country}', $client->country, $body);
        //$body  = str_replace('{$msisdn}', $order->details->msisdn, $body);
        $body  = str_replace('{$msisdn}', '0' . substr($order->details->msisdn, 2), $body);
        $body  = str_replace('{$msisdn_puk1}', $order->details->msisdn_puk1, $body);
        $body  = str_replace('{$msisdn_puk2}', $order->details->msisdn_puk2, $body);
        $body  = str_replace('{$msisdn_sim}', $order->details->msisdn_sim, $body);
        $body  = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        // $this->data['main_content'] = 'email/' . $this->data['language'] . '/order_accepted.php';
        //   $body = $this->load->view('email/content', $this->data, true);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('portin_initiation', $client->language, $this->session->cid));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'portin_initiation')) {
            log_message('error', 'Template portin_initiation is disabled sending aborted');
            echo json_encode(array(
                'result' => false,
                'message' => 'Tempalate is disabled'
            ));
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('portin_initiation', $client->language, $client->companyid),
                    'message' => $body
                ));
                echo json_encode(array(
                    'result' => true
                ));
            } //$this->email->send()
            else {
                echo json_encode(array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                ));
            }
        }
    }
    public function sendOrderRecjected($id, $client)
    {
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } //$this->data['setting']->smtp_type == 'smtp'
        else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $order = $this->Admin_model->getServiceCLI($id);
        $body  = getMailContent('order_reject', $client->language, $this->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body  = str_replace('{$name}', format_name($client), $body);
        $body  = str_replace('{$address1}', $client->address1, $body);
        $body  = str_replace('{$housenumber}', $client->housenumber, $body);
        $body  = str_replace('{$alphabet}', $client->alphabet, $body);
        $body  = str_replace('{$postcode}', $client->postcode, $body);
        $body  = str_replace('{$city}', $client->city, $body);
        $body  = str_replace('{$country}', $client->country, $body);
        //$body  = str_replace('{$msisdn}', $order->details->msisdn, $body);
        $body  = str_replace('{$msisdn}', '0' . substr($order->details->msisdn, 2), $body);
        $body  = str_replace('{$msisdn_puk1}', $order->details->msisdn_puk1, $body);
        $body  = str_replace('{$msisdn_puk2}', $order->details->msisdn_puk2, $body);
        $body  = str_replace('{$msisdn_sim}', $order->details->msisdn_sim, $body);
        $body  = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        // $this->data['main_content'] = 'email/' . $this->data['language'] . '/order_accepted.php';
        //   $body = $this->load->view('email/content', $this->data, true);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('order_reject', $client->language, $this->session->cid));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'order_reject')) {
            log_message('error', 'Template order_reject is disabled sending aborted');
            echo json_encode(array(
                'result' => false,
                'message' => 'Tempalate is disabled'
            ));
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('order_reject', $client->language, $client->companyid),
                    'message' => $body
                ));
                echo json_encode(array(
                    'result' => true
                ));
            } //$this->email->send()
            else {
                echo json_encode(array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                ));
            }
        }
    }
    public function SendWelcomeEmail($order, $password, $client)
    {
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } //$this->data['setting']->smtp_type == 'smtp'
        else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $this->data['language'] = "dutch";
        $this->data['client']   = $client;
        $this->data['service']  = $order;
        $this->data['password'] = $password;
        /*

        mix combi= 2
        combi = 1
        mix non combi = 3
        non combi =0
        */
        $combi                  = $this->Admin_model->GetProductType($order->packageid);
        if ($combi == 2) {
            $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. Vanaf nu ontvangt u daarom thuis 25 Mbps extra downloadsnelheid (tot maximaal 400 Mbps). Nog sneller streamen, downloaden en gamen dus!</p>";
        } //$combi == 2
        elseif ($combi == 1) {
            $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. U ontvangt daarom elke maand de volgende gratis extra’s:</p><p>
-   Dubbele data en Onbeperkt bellen met je mobiel<br />
-   ".$this->data['setting']->currency." 5,– korting op uw DELTA Internet factuur<br />
-   25 Mbps extra downloadsnelheid (tot maximaal 400Mbps).<br />
-   Een extra TV-pakket boordevol sport-, muziek-, entertainment-, en jeugdzenders<br /></p>";
        } //$combi == 1
        elseif ($combi == 0) {
            $com = "<p>We zijn erg blij met u als klant. Daarom krijgt u als welkomstcadeau een halfjaar ".$this->data['setting']->currency." 5,– maandelijkse korting op uw mobiele abonnement. U hoeft daar verder niets voor te doen. </p>";
        } //$combi == 0
        //$body = $this->load->view('email/resetpassword.php', $this->data, true);
        $body = getMailContent('order_accepted', $client->language, $this->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$address1}', $client->address1, $body);
        $body = str_replace('{$housenumber}', $client->housenumber, $body);
        $body = str_replace('{$alphabet}', $client->alphabet, $body);
        $body = str_replace('{$postcode}', $client->postcode, $body);
        $body = str_replace('{$city}', $client->city, $body);
        $body = str_replace('{$country}', $client->country, $body);
        $body = str_replace('{$combi}', $combi, $body);
        //$body = str_replace('{$msisdn}', $order->details->msisdn, $body);
        $body  = str_replace('{$msisdn}', '0' . substr($order->details->msisdn, 2), $body);
        $body = str_replace('{$msisdn_sim}', $order->details->msisdn_sim, $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        // $this->data['main_content'] = 'email/' . $this->data['language'] . '/order_accepted.php';
        //   $body = $this->load->view('email/content', $this->data, true);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('order_accepted', $client->language, $this->session->cid));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'order_accepted')) {
            log_message('error', 'Template order_accepted is disabled sending aborted');
            echo json_encode(array(
                'result' => true,
                'message' => 'Tempate  order_accepted is disabled'
            ));
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('order_accepted', $client->language, $client->companyid),
                    'message' => $body
                ));
                echo json_encode(array(
                    'result' => true
                ));
            } //$this->email->send()
            else {
                echo json_encode(array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                ));
            }
        }
    }
    public function sendChangeService($order, $newpid, $client)
    {
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } //$this->data['setting']->smtp_type == 'smtp'
        else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->data['combi_changes'] = is_this_combi($newpid);
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent('service_change_request', $client->language, $client->companyid);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$new_bundle}', getProductName($newpid), $body);
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('service_change_request', $client->language, $client->companyid));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'service_change_request')) {
            log_message('error', 'Template service_change_request is disabled sending aborted');
            echo json_encode(array(
                'result' => true,
                'message' => 'Tempate  service_change_request is disabled'
            ));
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('service_change_request', $client->language, $client->companyid),
                    'message' => $body
                ));
                return array(
                    'result' => true
                );
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('service_change_request', $client->language, $client->companyid),
                    'message' => $body,
                    'status' => 'error'
                ));
                return array(
                    'result' => false,
                    'error' => $this->email->print_debugger()
                );
            }
        }
    }
    public function getBundleList()
    {
        $html     = "";
        $number   = $_POST['msisdn'];
        $type     = $_POST['type'];
        $this->db = $this->load->database('default', true);
        if ($type == "tarif") {
            $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and status = 1 order by id asc", array(
                $type,
                $this->uri->segment(4)
            ));
        } else {
            if (!empty($_POST['agid'])) {
                $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and agid=? and status = 1 order by id asc", array(
                    $type,
                    $this->uri->segment(4),
                    $_POST['agid']
                ));
            } else {
                $q = $this->db->query("select id,name,recurring_total from a_products_mobile_bundles where bundle_type like ? and companyid = ? and status = 1 order by id asc", array(
                    $type,
                    $this->uri->segment(4)
                ));
            }
        }
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service->details->platform == "TEUM") {
            $addons = $this->Admin_model->getOrderedAddonid($_POST['serviceid']);
            foreach ($q->result() as $bundle) {
                if ($addons) {
                    if (!in_array($bundle->id, $addons)) {
                        if ($bundle->recurring_total > 0) {
                            $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency . number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                        } else {
                            $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                        }
                    }
                } else {
                    if ($bundle->recurring_total > 0) {
                        $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency . number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                    } else {
                        $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                    }
                }
            } //$q->result() as $bundle
        } else {
            foreach ($q->result() as $bundle) {
                if ($bundle->recurring_total > 0) {
                    $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' -  '.$this->data['setting']->currency . number_format($bundle->recurring_total, 2) . ' /' . lang('month') . ' </option>';
                } else {
                    $html .= '<option value="' . $bundle->id . '">' . str_replace('Option ', '', $bundle->name) . ' </option>';
                }
            } //$q->result() as $bundle
        }
        echo json_encode(array(
            'result' => true,
            'html' => $html,
            'price' => number_format($q->row()->recurring_total, 2)
        ));
    }
    public function getPricingAddon()
    {
        $q = $this->db->query("select * from a_products_mobile_bundles where id=? and companyid=?", array(
            $this->uri->segment(4),
            $this->session->cid
        ));
        echo json_encode(array(
            'price' => $q->row()->recurring_total
        ));
    }
    public function addBundle()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));


        $this->load->model('Agent_model');
        $client = $this->Admin_model->getClient($_POST['userid']);
        $agent = $this->Agent_model->getAgent($client->agentid);
        if ($_POST['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $_POST['serviceid']);
                        break;
                    }
                } while (true);
            }
        }
        $addons    = getAddonInformation($_POST['bundleid']);
        if ($_POST['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                if ($addons->recurring_total > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('You do not have enough balance to topup this subscriber'));
                    redirect('admin/subscription/detail/'.$_POST['serviceid']);
                }
            }
        }
        $bundle    = $this->Admin_model->getBundleOption($_POST['bundleid']);
        $bundleid  = $_POST['bundleid'];
        $serviceid = $_POST['serviceid'];
        $date      = explode('T', $_POST['from']);
        $s         = explode('-', $date[0]);
        if ($bundle) {
            $future = getFuturedate($date[0], $bundle->bundle_duration_type, $bundle->bundle_duration_value);
        } //$bundle
        if (!empty($_POST['to'])) {
            $future = $_POST['to'];
            $toto   = explode('T', $_POST['to']);
        }
        if ($toto == date('Y-m-t')) {
            $months = 1;
        } else {
            $months = getMonthDif($date[0], $toto[0]);
        }
        $addon_id = $this->Admin_model->insert_option_bundle(array(
            'name' => $bundle->name,
            'terms' => $bundle->bundle_duration_value,
            'cycle' => $bundle->bundle_duration_type,
            'serviceid' => $_POST['serviceid'],
            'addonid' => $_POST['bundleid'],
            'recurring_total' => $bundle->recurring_total,
            'addon_type' => $bundle->bundle_type,
            'arta_bundleid' => $bundle->bundleid,
            'iGeneralPricingIndex' => null
        ));
        if ($addon_id > 0) {
            $service = $this->Admin_model->getService($_POST['serviceid']);
            // $client  = $this->Admin_model->getClient($_POST['userid']);
            $result  = $this->artilium->AddBundleAssign($service->details->msisdn_sn, $_POST['bundleid'], $_POST['from'], $future);
            //print_r($result);
            if ($result->result == "success") {
                if ($_POST['reseller_charge'] == "Yes") {
                    if ($agent->reseller_type == "Prepaid") {
                        $new_balance = $agent->reseller_balance - $addons->recurring_total;
                        $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance,$this->session->reseller['id']));
                        unlink(APPPATH.'lock/reseller_balance_'.$this->session->reseller['id'], $_POST['serviceid']);
                    }
                }
                $magebo = $this->magebo->ProcessPricing($service, $client->mageboid, $bundleid, 1, $months);
                $this->send_bundleorder_email($_POST['serviceid'], $bundle, $_POST['from'], $future, 'mobile_bundle30d');
                $this->session->set_flashdata('success', 'Addon has been added as requested please do not add again otherwise it will be double');
                $this->Admin_model->insertTopup(
                    array(
                      'companyid' => $this->companyid,
                      'serviceid' => $_POST['serviceid'],
                      'userid' =>  $_POST['userid'],
                      'income_type' => 'bundle',
                      'agentid' => $client->agentid,
                      'amount' => $addons->recurring_total,
                      'user' =>  $this->session->reseller['agent'])
                );
               // Service request from
                logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $_POST['userid'],
                'serviceid' =>$_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Added Bundle '.$_POST['bundleid'].' to '.$_POST['serviceid'].' ' . $this->session->firstname . ' ' . $this->session->lastname
                ));
            } //$result->result == "success"
            else {
                $this->session->set_flashdata('success', 'There was error on updating addong please contract +32484889888 for urgent support');
            }
        } //$addon_id > 0
        else {
            $this->session->set_flashdata('error', 'Addon order failed, please contact our support');
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function addBundleTarif()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('magebo', array(
            'companyid' => $this->companyid
        ));
        $bundle    = $this->Admin_model->getBundleOption($_POST['bundleid']);
        $bundleid  = $_POST['bundleid'];
        $serviceid = $_POST['serviceid'];
        $date      = explode('T', $_POST['from']);
        $s         = explode('-', $date[0]);
        if (!empty($_POST['to'])) {
            $toto  = explode('T', $_POST['to']);
            $to    = $toto[0] . 'T23:59:59';
            $date2 = $toto[0];
        } else {
            $to    = '2099-12-31T23:59:59';
            $date2 = "2099-12-31";
        }
        $months = getMonthDif($date[0], $date2);
        if ($bundle) {
            $future = getFuturedate($date[0], $bundle->bundle_duration_type, $bundle->bundle_duration_value);
        } //$bundle
        if ($_POST['charge'] == "1") {
            $price     = $_POST['price'];
            $recurring = 1;
        } else {
            $recurring = 0;
            $price     = $bundle->recurring_total;
        }
        $addon_id = $this->Admin_model->insert_option_bundle(array(
            'name' => $bundle->name,
            'terms' => $bundle->bundle_duration_value,
            'cycle' => $bundle->bundle_duration_type,
            'serviceid' => $_POST['serviceid'],
            'addonid' => $_POST['bundleid'],
            'recurring_total' => $price,
            'addon_type' => $bundle->bundle_type,
            'arta_bundleid' => $bundle->bundleid,
            'iGeneralPricingIndex' => null
        ));
        if ($addon_id > 0) {
            $service = $this->Admin_model->getService($_POST['serviceid']);
            $client  = $this->Admin_model->getClient($_POST['userid']);
            $result  = $this->artilium->AddBundleAssign($service->details->msisdn_sn, $_POST['bundleid'], $_POST['from'], $to);
            if ($result->result == "success") {
                if (!strpos($this->session->email, "@united-telecom.be")) {
                    $this->send_bundleorder_email($_POST['serviceid'], $bundle, $_POST['from'], $future, 'mobile_bundle_recurring');
                }
                if ($this->data['setting']->create_magebo_bundle) {
                    $create = 1;
                } //$this->data['setting']->create_magebo_bundle
                else {
                    $create = 0;
                }
                if ($recurring) {
                    mail('mail@simson.one', 'post', print_r($_POST, true) . ' ' . $create . ' ' . print_r($service, true));
                    $this->magebo->ProcessPricingExtraBundle($service, $client->mageboid, $_POST['bundleid'], $create, $price, $s, $months + 1, true);
                } else {
                    $this->magebo->ProcessPricing($service, $client->mageboid, $_POST['bundleid'], $create, 1, $s[1] . '-' . $s[2] . '-' . $s[0]);
                }
                // $this->magebo->ProcessPricingExtraBundle($service, $client->mageboid, $_POST['bundleid'], $create, $price, $s);
                logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $_POST['userid'],
                'serviceid' =>$_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Added Bundle '.$_POST['bundleid'].' to '.$_POST['serviceid'].' ' . $this->session->firstname . ' ' . $this->session->lastname .' with Price: '.$price
                ));

                $this->session->set_flashdata('success', 'Addon has been added as requested please do not add again otherwise it will be double');
            } //$result->result == "success"
            else {
                $this->session->set_flashdata('success', 'There was error on updating addong please contract +32484889888 for urgent support');
            }
        } //$addon_id > 0
        else {
            $this->session->set_flashdata('error', 'Addon has been added as requested');
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function convert_order_portin_to_new_number()
    {
        if (isAllowed_Service($this->session->cid, $_POST['serviceid'])) {
            $service = $this->Admin_model->getService($_POST['serviceid']);
            $d = explode('-', $service->date_contract);
            $contract = $d[2].'-'.$d[0].'-'.$d[1];
            $this->load->library('artilium', array(
                'companyid' => $this->companyid
            ));

            //$q = $this->db->query("select * from a_services_mobile where msisdn=?", array($_POST['MSISDN']));
            //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
            //$cancel = $this->Admin_model->artiliumPost(array('companyid' => $this->data['setting']->companyid, 'action' => 'CancelPortIn', 'type' => $this->data['mobile']->PaymentType, 'SN' => trim($this->data['sn']->SN)));
            $cancel = $this->artilium->CancelPortIn($service->details->msisdn_sn);
            log_message('error', 'cancel portin because convert '.print_r($cancel, true));

            if ($contract <= date('Y-m-d')) {
                $status = 'Active';
            } else {
                $status = 'ActivationRequested';
            }

            $this->db->where('serviceid', $_POST['serviceid']);
            $this->db->update('a_services_mobile', array(
             'msisdn_type'=>'new',
             'msisdn'=> $service->details->msisdn_sn,
             'msisdn_status' => $status,
             'date_modified' => date('Y-m-d H:i:s')));
            log_message('error', 'Convert portin to new number ==>'.$this->db->affected_rows());
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $service->userid,
                'serviceid' =>$_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Convert porting  '.$_POST['serviceid'].' to new simcard activation by '.$this->session->firstname . ' ' . $this->session->lastname
                ));

            echo json_encode(array('result'=> true, 'message'=> 'Request has been accepted and the page will be refreshed'));
        } else {
            echo json_encode(array('result'=> false, 'message'=> 'access denied'));
        }
    }
    public function insert_Promo()
    {
        $this->db->query('update a_services set promocode=? where id=?', array(
            'DELARTA002',
            $this->uri->segment(4)
        ));
        $this->load->library('magebo', array(
            'companyd' => $this->companyid
        ));
        $mobile = $this->Admin_model->getService($this->uri->segment(4));
        // print_r($mobile);
        $this->magebo->insertPromo($mobile);
        redirect('admin/subscription');
    }
    public function cancel_service()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $this->load->library('magebo', array(
            'companyd' => $this->companyid
        ));
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service->status == "Pending") {
            if (in_array($service->details->msisdn_status, array(
                'PortinAccepted',
                'ActivationRequested',
                'PortinRejected',
                'PortinPending',
                'PortInValidateVerificationCodeAccepted',
                'PortinCancelled',
                'PortinFailed'
            ))) {
                $this->Admin_model->ChangeStatusOrderID($_POST['serviceid'], 'Cancelled', 'mobile');
                $this->Admin_model->ChangeStatusService($_POST['serviceid'], 'Cancelled');
                if ($service->details->msisdn_type == 'porting') {
                    if (!empty($service->details->msisdn)) {
                        $sim    = $this->artilium->GetSpecificCliInfo($service->details->msisdn);
                        //$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
                        $ss     = $this->artilium->GetSn($sim->SIMNr);
                        $sn     = (object) $ss->data;
                        //$cancel = $this->Admin_model->artiliumPost(array('companyid' => $this->data['setting']->companyid, 'action' => 'CancelPortIn', 'type' => $this->data['mobile']->PaymentType, 'SN' => trim($this->data['sn']->SN)));
                        $cancel = $this->artilium->CancelPortIn(trim($sn->SN));
                        if ($cancel->result != "success") {
                            $this->session->set_flashdata("error", $cancel->message);
                        }
                    }
                    echo json_encode(array(
                        'result' => true,
                        'message' => 'Request porting/activation has been cancelled, sim has been blocked and put back to pool'
                    ));
                } else {
                    echo json_encode(array(
                        'result' => true,
                        'message' => 'order cancelled'
                    ));
                }

                $this->magebo->cancel_pincode($service->details->msisdn_sn);
                $this->magebo->cancel_pincode($service->details->msisdn);
                $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                    'msisdn_sn' => null,
                    'msisdn_sim' => null,
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $service->userid,
                    'serviceid' => $_POST['serviceid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => $this->session->firstname . ' ' . $this->session->lastname.' has cancelled serviceid: '.$_POST['serviceid']
                    ));
            } elseif ($service->details->msisdn_status == "PortinPending") {
                $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                    'msisdn_sn' => null,
                    'msisdn_sim' => null,
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                $this->Admin_model->ChangeStatusOrderID($_POST['serviceid'], 'Cancelled', 'mobile');
                $this->Admin_model->ChangeStatusService($_POST['serviceid'], 'Cancelled');
                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $service->userid,
                    'serviceid' => $_POST['serviceid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => $this->session->firstname . ' ' . $this->session->lastname.' has cancelled serviceid: '.$_POST['serviceid']
                    ));
                echo json_encode(array(
                    'result' => true,
                    'message' => 'Service has been cancelled, however you still need to contact  for request cancellation porting via the pending portin request'
                ));
            } else {
                $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                    'msisdn_sn' => null,
                    'msisdn_sim' => null,
                    'date_modified' => date('Y-m-d H:i:s')
                ));
                $this->Admin_model->ChangeStatusOrderID($_POST['serviceid'], 'Cancelled', 'mobile');
                $this->Admin_model->ChangeStatusService($_POST['serviceid'], 'Cancelled');
                $this->load->library('magebo', array(
                    'companyd' => $this->companyid
                ));
                $this->magebo->cancel_pincode($service->details->msisdn_sn);
                $this->magebo->cancel_pincode($service->details->msisdn);

                logAdmin(array(
                    'companyid' => $this->companyid,
                    'userid' => $service->userid,
                    'serviceid' => $_POST['serviceid'],
                    'user' => $this->session->firstname . ' ' . $this->session->lastname,
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'description' => $this->session->firstname . ' ' . $this->session->lastname.' has cancelled serviceid: '.$_POST['serviceid']
                    ));


                echo json_encode(array(
                    'result' => true,
                    'message' => 'order cancelled'
                ));
            }
        } else {
            echo json_encode(array(
                'result' => false,
                'message' => 'can\'t use this method on cancellation'
            ));
        }
        //redirect('admin/client/detail/'.$service->userid);
    }
    public function send_bundleorder_email($id, $bundle, $validfrom, $validuntil, $template)
    {
        $this->load->model('Admin_model');
        $service                = $this->Admin_model->getServiceCli($id);
        $mobile                 = $service->details;
        $brand                 = $this->Admin_model->getBrandPdfFooter($service->gid);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);

        $body = getMailContent($template, $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$bundle_validfrom}', $validfrom, $body);
        $body = str_replace('{$bundle_validuntil}', $validuntil, $body);
        $body = str_replace('{$bundle_name}', $bundle->name, $body);
        $body = str_replace('{$bundle_price}', $bundle->recurring_total, $body);
        $body = str_replace('{$msisdn_sim}', $mobile->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->msisdn_puk2, $body);
        //$body = str_replace('{$msisdn}', $mobile->msisdn, $body);
        $body  = str_replace('{$msisdn}', '0' . substr($mobile->msisdn, 2), $body);
        $body = str_replace('{$Companyname}', $brand->name, $body);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $subject = getSubject($template, $client->language, $client->companyid);
        $this->email->subject($subject);
        if (empty($subject)) {
            return array(
                'result' => 'success'
            );
        }
        if (empty($body)) {
            return array(
                'result' => 'success'
            );
        }
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, $template)) {
            log_message('error', 'Template ' . $template . ' is disabled sending aborted');
            echo json_encode(array(
                'result' => true,
                'message' => 'Tempate  ' . $template . ' is disabled'
            ));
            return array(
                'result' => 'success'
            );
        } else {
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => $subject,
                    'message' => $body
                ));
                return array(
                    'result' => 'success'
                );
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => $subject,
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
                return array(
                    'result' => 'error',
                    'message' => $this->email->print_debugger()
                );
            }
        }
    }
    public function send_portout_email($id)
    {
        $this->load->model('Admin_model');
        $s                = $this->Admin_model->getServiceCli($id);
        $mobile             = $s->details;
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->load->library('umail', array('companyid' => $client->companyid));
        return $this->umail->simcard_blocking($client, $mobile);
    }
    public function send_simblock_email($id)
    {
        $this->load->model('Admin_model');
        $s                = $this->Admin_model->getServiceCli($id);
        $mobile            = $s->details;
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->load->library('umail', array('companyid' => $client->companyid));
        return $this->umail->simcard_blocking($client, $mobile);
    }

    public function send_simunblock_email($id)
    {
        $this->load->model('Admin_model');
        $s                = $this->Admin_model->getServiceCli($id);
        $mobile = $s->details;
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->load->library('umail', array('companyid' => $client->companyid));
        $this->umail->simcard_unblocking($client, $mobile);
    }


    public function send_service_change_request_email($id, $newid, $amount)
    {
        $this->load->model('Admin_model');
        $ss                    = $this->Admin_model->getServiceCli($id);
        $mobile                = $ss->details;
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);
        $body = getMailContent('service_change_request', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', $client->salutation . ' ' . $client->initial . ' ' . $client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$clientid}', $client->mvno_id, $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->msisdn_sim, $body);

        $body = str_replace('{$msisdn_puk1}', $mobile->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->msisdn_puk2, $body);
        // $body = str_replace('{$msisdn}', $mobile->msisdn, $body);
        $body = str_replace('{$msisdn}', '0' . substr($mobile->msisdn, 2), $body);
        $body = str_replace('{$new_bundle}', getProductName($newid), $body);
        $body = str_replace('{$new_cost}', $amount, $body);
        if ($client->companyid == 53) {
            $combi = $this->Admin_model->GetProductType($mobile->packageid);
            if ($combi == 2) {
                $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. Vanaf nu ontvangt u daarom thuis 25 Mbps extra downloadsnelheid (tot maximaal 400 Mbps). Nog sneller streamen, downloaden en gamen dus!</p>";
            } //$combi == 2
            elseif ($combi == 1) {
                $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. U ontvangt daarom elke maand de volgende gratis extra’s:</p><p>
    -   Dubbele data en Onbeperkt bellen met je mobiel<br />
    -   ".$this->data['setting']->currency." 5,– korting op uw DELTA Internet factuur<br />
    -   25 Mbps extra downloadsnelheid (tot maximaal 400Mbps).<br />
    -   Een extra TV-pakket boordevol sport-, muziek-, entertainment-, en jeugdzenders<br /></p>";
            } //$combi == 1
            elseif ($combi == 0) {
                $com = "<p>We zijn erg blij met u als klant. Daarom krijgt u als welkomstcadeau een halfjaar ".$this->data['setting']->currency." 5,– maandelijkse korting op uw mobiele abonnement. U hoeft daar verder niets voor te doen. </p>";
            } //$combi == 0
            $body = str_replace('{$combi}', $com, $body);
        }
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->subject(getSubject('service_change_request', $client->language, $client->companyid));
        //$this->email->subject(lang("Your New Password"));
        $this->email->message($body);
        if (!isTemplateActive($client->companyid, 'service_change_request')) {
            log_message('error', 'Template service_change_request is disabled sending aborted');
            echo json_encode(array(
                'result' => true,
                'message' => 'Tempate service_change_request is disabled'
            ));
            return array(
                'result' => 'success'
            );
        }
        if ($this->email->send()) {
            logEmailOut(array(
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'to' => $client->email,
                'subject' => getSubject('service_change_request', $client->language, $client->companyid),
                'message' => $body
            ));
            return array(
                'result' => 'success'
            );
        } else {
            logEmailOut(array(
                'userid' => $client->id,
                'companyid' => $client->companyid,
                'to' => $client->email,
                'subject' => getSubject('service_change_request', $client->language, $client->companyid),
                'message' => $body,
                'status' => 'error',
                'error_message' => $this->email->print_debugger()
            ));
            return array(
                'result' => 'error',
                'message' => $this->email->print_debugger()
            );
        }
    }
    public function test_sendinit()
    {
        error_reporting(1);
        //$this->send_PortinInitiation($this->uri->segment(4));
        $num = "32484889888";
        $this->load->library('sms', array(
            'username' => $this->data['setting']->sms_username,
            'password' => $this->data['setting']->sms_password,
            'companyid' => $this->companyid,
            'userid' => ''
        ));
        $res = $this->sms->send_message_bulksms(array(
            array(
                'from' => $this->data['setting']->sms_senderid,
                'to' => '+' . $num,
                'body' => $this->data['setting']->sms_content
            )
        ));
        print_r($res);
    }
    public function send_PortinInitiation($id)
    {
        /*if(empty($id)){
        $id = $this->uri->segment(4);
        }
        */
        $mobile                = $this->Admin_model->getServiceCli($id);
        $client                = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        $this->db->query("update a_services_mobile set porting_sms = ? where serviceid=?", array(
            1,
            $id
        ));
        if ($mobile->details->donor_accountnumber) {
            return array(
                'result' => 'succes'
            );
        } else {
            if ($this->data['setting']->enable_sms == "1" && $this->data['setting']->sms_content != "") {
                $num = trim($mobile->details->msisdn);
                $this->load->library('sms', array(
                    'username' => $this->data['setting']->sms_username,
                    'password' => $this->data['setting']->sms_password,
                    'companyid' => $this->companyid,
                    'userid' => $client->id
                ));
                $sms_res = $this->sms->send_message_bulksms(array(
                    array(
                        'from' => $this->data['setting']->sms_senderid,
                        'to' => '+' . $num,
                        'body' => $this->data['setting']->sms_content
                    )
                ));
                unset($this->sms);
            }
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent('portin_initiation', $client->language, $client->companyid);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
            // $body = str_replace('{$msisdn}', $mobile->etails->msisdn, $body);
            $body  = str_replace('{$msisdn}', '0' . substr($mobile->details->msisdn, 2), $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            //$this->email->bcc('mail@simson.one');
            $this->email->subject(getSubject('portin_initiation', $client->language, $client->companyid));
            $this->email->message($body);
            if (!isTemplateActive($client->companyid, 'portin_initiation')) {
                log_message('error', 'Template portin_initiation is disabled sending aborted');
                echo json_encode(array(
                    'result' => true,
                    'message' => 'Tempate portin_initiation is disabled'
                ));
                return array(
                    'result' => 'success'
                );
            }
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('portin_initiation', $client->language, $client->companyid),
                    'message' => $body
                ));
                return array(
                    'result' => 'success'
                );
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('service_change_request', $client->language, $client->companyid),
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
                return array(
                    'result' => 'error',
                    'message' => 'error when sending email'
                );
            }
        }
    }
    public function send_reminder_custom()
    {
        $serviceid = $_POST['serviceid'];
        $type      = $this->uri->segment(4);
        if ($type == "reminder_id_1") {
            $tt = 'custom_last_id_bank_reminder';
        } elseif ($type == "reminder_id_2") {
            $tt = 'custom_id_bank_reminder';
        } else {
            die('Error on checking validation');
        }
        //service/'+id+'/custom_last_id_bank_reminder'
        redirect('admin/subscription/send_custom_email/service/' . $serviceid . '/' . $tt . '/rediect_client');
    }
    public function send_portin_email()
    {
        $redirect_client = true;
        $type            = $_POST['email_type'];
        $cid             = $_POST['serviceid'];
        $template        = $_POST['reminder_type'];
        if ($type == "service") {
            $mobile                = $this->Admin_model->getServiceCli($cid);
            $client                = $this->Admin_model->getClient($mobile->userid);
            $brand                 = $this->Admin_model->getBrandPdfFooter($mobile->gid);
            $this->data['setting'] = globofix($client->companyid);
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent($template, $client->language, $client->companyid);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
            // $body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
            $body  = str_replace('{$msisdn}', '0' . substr($mobile->details->msisdn, 2), $body);
            $body = str_replace('{$address1}', $client->address1, $body);
            $body  = str_replace('{$housenumber}', $client->housenumber, $body);
            $body  = str_replace('{$alphabet}', $client->alphabet, $body);
            $body = str_replace('{$city}', $client->city, $body);
            $body = str_replace('{$postcode}', $client->postcode, $body);
            $body = str_replace('{$country}', getCountryName($client->country), $body);
            $body = str_replace('{$client_companyname}', $client->companyname, $body);
            $body = str_replace('{$Companyname}', $brand->name, $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            $this->email->bcc($this->session->email);
            $this->email->subject(getSubject($template, $client->language, $client->companyid));
            $this->email->message($body);
            if (!isTemplateActive($client->companyid, $template)) {
                log_message('error', 'Template ' . $template . ' is disabled sending aborted');
                echo json_encode(array(
                    'result' => true,
                    'message' => 'Tempate ' . $template . ' is disabled'
                ));
                $this->session->set_flashdata('success', 'Email has not been sent, template is disabled');
            // return array('result' => 'success');
            } else {
                if ($this->email->send()) {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email,
                        'subject' => getSubject($template, $client->language, $client->companyid),
                        'message' => $body
                    ));
                    // redirect('admin/subscription/detail/'.$cid);
                    $this->session->set_flashdata('success', 'Email has been sent');
                } else {
                    logEmailOut(array(
                        'userid' => $client->id,
                        'companyid' => $client->companyid,
                        'to' => $client->email,
                        'subject' => getSubject('service_change_request', $client->language, $client->companyid),
                        'message' => $body,
                        'status' => 'error',
                        'error_message' => $this->email->print_debugger()
                    ));
                    $this->session->set_flashdata('error', 'Email was not sent');
                }
            }
            if ($redirect_client) {
                redirect('admin/client/detail/' . $client->id);
            }
            redirect('admin/subscription/detail/' . $cid);
        }
    }
    public function send_custom_email()
    {
        $redirect_client = false;
        $type            = $this->uri->segment(4);
        $cid             = $this->uri->segment(5);
        $template        = $this->uri->segment(6);
        if (!empty($this->uri->segment(7))) {
            $redirect_client = true;
        }
        if ($type == "service") {
            $mobile                = $this->Admin_model->getServiceCli($cid);
            $client                = $this->Admin_model->getClient($mobile->userid);
            $brand                 = $this->Admin_model->getBrandPdfFooter($mobile->gid);
            $this->data['setting'] = globofix($client->companyid);
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent($template, $client->language, $client->companyid);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
            // $body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
            $body = str_replace('{$msisdn}', '0' . substr($mobile->details->msisdn, 2), $body);
            $body = str_replace('{$address1}', $client->address1, $body);
            $body = str_replace('{$housenumber}', $client->housenumber, $body);
            $body = str_replace('{$alphabet}', $client->alphabet, $body);
            $body = str_replace('{$city}', $client->city, $body);
            $body = str_replace('{$postcode}', $client->postcode, $body);
            $body = str_replace('{$country}', getCountryName($client->country), $body);
            $body = str_replace('{$client_companyname}', $client->companyname, $body);
            $body = str_replace('{$Companyname}', $brand->name, $body);
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            $this->email->bcc($this->session->email);
            $this->email->subject(getSubject($template, $client->language, $client->companyid));
            $this->email->message($body);
            if (!isTemplateActive($client->companyid, $template)) {
                log_message('error', 'Template ' . $template . ' is disabled sending aborted');
                echo json_encode(array(
                    'result' => true,
                    'message' => 'Tempate ' . $template . ' is disabled'
                ));
                $this->session->set_flashdata('error', 'Email was not sent template is disabled');
                redirect('admin/subscription/detail/' . $cid);
            }
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject($template, $client->language, $client->companyid),
                    'message' => $body
                ));
                // redirect('admin/subscription/detail/'.$cid);
                $this->session->set_flashdata('success', 'Email has been sent');
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => getSubject('service_change_request', $client->language, $client->companyid),
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
                $this->session->set_flashdata('error', 'Email was not sent');
            }
            if ($redirect_client) {
                redirect('admin/client/detail/' . $client->id);
            }
            redirect('admin/subscription/detail/' . $cid);
        }
    }
    public function sendmail($cid, $type, $template, $extradata = false)
    {
        if ($type == "service") {
            $mobile                = $this->Admin_model->getServiceCli($cid);
            $client                = $this->Admin_model->getClient($mobile->userid);
            $brand                 = $this->Admin_model->getBrandPdfFooter($mobile->gid);
            $this->data['setting'] = globofix($client->companyid);
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent($template, $client->language, $client->companyid);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
            $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
            $body = str_replace('{$msisdn_puk1}', $mobile->details->msisdn_puk1, $body);
            $body = str_replace('{$msisdn_puk2}', $mobile->details->msisdn_puk2, $body);
            //$body = str_replace('{$msisdn}', $mobile->details->msisdn, $body);
            $body = str_replace('{$msisdn}', '0' . substr($mobile->details->msisdn, 2), $body);
            $body = str_replace('{$address1}', $client->address1, $body);
            $body = str_replace('{$housenumber}', $client->housenumber, $body);
            $body = str_replace('{$alphabet}', $client->alphabet, $body);
            $body = str_replace('{$city}', $client->city, $body);
            $body = str_replace('{$postcode}', $client->postcode, $body);
            $body = str_replace('{$country}', getCountryName($client->country), $body);
            $body = str_replace('{$client_companyname}', $client->companyname, $body);
            $body = str_replace('{$Companyname}', $brand->name, $body);



            if ($extradata) {
                foreach ($extradata as $key => $value) {
                    $body = str_replace('{$' . $key . '}', $value, $body);
                    // subscription_mobile_terminated', array('terminate_date
                }
            }
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            //$this->email->bcc('mail@simson.one');
            $subject = getSubject($template, $client->language, $client->companyid);
            $this->email->subject($subject);
            $this->email->message($body);
            if (empty($body)) {
                log_message('error', 'Template ' . $template . ' was not sent because body empty');
                return false;
            }
            if (empty($subject)) {
                log_message('error', 'Template ' . $template . ' was not sent because subject empty');
                return false;
            }
            if (!isTemplateActive($client->companyid, $template)) {
                log_message('error', 'Template ' . $template . ' is disabled sending aborted');
                return true;
            }
            if ($this->email->send()) {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => $subject,
                    'message' => $body
                ));
                return true;
            // redirect('admin/subscription/detail/'.$cid);
                //$this->session->set_flashdata('success','Email has been sent');
            } else {
                logEmailOut(array(
                    'userid' => $client->id,
                    'companyid' => $client->companyid,
                    'to' => $client->email,
                    'subject' => $subject,
                    'message' => $body,
                    'status' => 'error',
                    'error_message' => $this->email->print_debugger()
                ));
                return false;
                //$this->session->set_flashdata('error','Email was not sent');
            }
        }
    }
    public function disable_invoicing()
    {
        $this->load->library('magebo', array(
            'companyid' => $this->session->cid
        ));
        if ($this->magebo->disableGeneralPricing($_POST['id'])) {
            logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $_POST['userid'],
                'serviceid' => '',
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Subscription id ' . $_POST['id'] . ' has been disabled by ' . $this->session->firstname . ' ' . $this->session->lastname
            ));
            echo json_encode(array(
                'result' => true,
                'data' => $_POST
            ));
        } else {
            echo json_encode(array(
                'result' => error,
                'Sorry your action was denied'
            ));
        }
    }
    public function create_proforma($companyid, $client, $pid, $serviceid, $modem = false)
    {
        $result['proformaid'] = 0;
        $result['invoicenum'] = 0;
        $killdate             = new DateTime(date('Y-m-d'));
        $killdate->modify('+' . $client->payment_duedays . ' day');
        $setupfee = getSetupFee($pid, $client->vat_rate, $client->id);
        if ($this->data['setting']->country_base == "NL") {
            $inum = getNewInvoicenum($companyid);
            $this->load->library('magebo', array(
                'companyid' => $companyid
            ));
            $ogm = $this->magebo->Mod11($inum);
        } else {
            $ogm = ogm(getNewInvoicenum($companyid));
        }
        $proforma   = array(
            'companyid' => $companyid,
            'invoicenum' => getNewInvoicenum($companyid),
            'userid' => $client->id,
            'subtotal' => $setupfee->subtotal,
            'tax' => $setupfee->tax,
            'total' => $setupfee->total,
            'date' => date('Y-m-d'),
            'duedate' => $killdate->format('Y-m-d'),
            'ogm' => $ogm,
            'notes' => str_replace(' ', '', $ogm),
            'paymentmethod' => 'banktransfer',
            'status' => 'Unpaid'
        );
        $proformaid = $this->Admin_model->CreateProforma($proforma);
        if ($proformaid) {
            //$pin = "0" . substr($iPincode, 2, 1) . "-" . substr($iPincode, 3, 2) . " " . substr($iPincode, 5, 2) . " " . substr($iPincode, 7, 2) . " " . substr($iPincode, 9, 2);
            $service = $this->Admin_model->getService($serviceid);
            $this->Admin_model->CreateProformaItems(array(
                'companyid' => $companyid,
                'invoiceid' => $proformaid,
                'userid' => $client->id,
                'description' => 'Activatie ' . $service->packagename,
                'price' => $setupfee->total,
                'qty' => 1,
                'amount' => $setupfee->total,
                'taxrate' => $client->vat_rate,
                'serviceid' => $serviceid,
                'invoicetype' => 'Service'
            ));
            if ($modem) {
                $this->Admin_model->CreateProformaItems(array(
                    'companyid' => $companyid,
                    'invoiceid' => $proformaid,
                    'userid' => $client->id,
                    'description' => 'Modem ' . $service->packagename,
                    'price' => 19,
                    'qty' => 1,
                    'amount' => 19,
                    'taxrate' => 21,
                    'serviceid' => $serviceid,
                    'invoicetype' => 'Service'
                ));
            }
            $result['proformaid'] = $proformaid;
            $result['invoicenum'] = $proforma['invoicenum'];
        }
        return $result;
    }
    public function addPortinPending_Notes()
    {
        $this->db        = $this->load->database('default', true);
        $_POST['msisdn'] = $this->uri->segment(4);
        $this->db->insert('a_portin_notes', $_POST);
        echo json_encode(array(
            'result' => $this->db->insert_id()
        ));
    }
    public function getPortinPending_Notes()
    {
        $this->db = $this->load->database('default', true);
        $this->db->where('msisdn', $_POST['msisdn']);
        $result = $this->db->get('a_portin_notes');
        if ($result->num_rows() > 0) {
            $html = '';
            foreach ($result->result_array() as $row) {
                $html .= '<tr><td>' . $row['date'] . '</td><td>' . $row['agent'] . '</td><td>' . $row['description'] . '</td></tr>';
            }
            echo json_encode(array(
                'result' => true,
                'html' => $html
            ));
        } else {
            echo json_encode(array(
                'result' => false,
                'msisdn' => $_POST['msisdn']
            ));
        }
        //echo json_encode($result->result_array());
    }
    public function setSumPlanAction()
    {
        $this->load->library('artilium', array(
            'companyid' => $this->companyid
        ));
        $res = $this->artilium->SetPlanState(array(
            'SumAssignmentId' => $_POST['SUMAssignmentId'],
            'SN' => $_POST['sn'],
            'State' => $_POST['State']
        ));
        if ($res->SetPlanStateResult == "0") {
            $this->session->set_flashdata('success', 'Your SumPlan has been ' . $_POST['State']);
        } else {
            $this->session->set_flashdata('error', 'there has been error while trying to ' . $_POST['State'] . ' please contact our support');
        }
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }
    public function xdsl_update_information()
    {
        $recurring = $_POST['recurring'];
        unset($_POST['recurring']);
        $this->db->where('serviceid', $_POST['serviceid']);
        $this->db->update('a_services_xdsl', $_POST);
        if ($recurring > 0) {
            $this->db->query('update a_services set recurring=? where id=?', array(
                $recurring,
                $_POST['serviceid']
            ));
        }
        echo json_encode(array(
            'result' => $this->db->affected_rows()
        ));
    }
    public function Teum_Swap()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $client    = $this->Admin_model->getClient($service->userid);
        $this->load->model('Agent_model');
        if ($service) {
            $swap = array(
            "msisdn" =>  $service->details->msisdn,
            "newIccId" =>$_POST['simcard'],
            "oldIccId" => $service->details->msisdn_sim,
            "externalReference" => $_POST['serviceid'],
            "channel" =>  "UnitedPortal V1",
            "comments" => "Executed by ".$this->session->firstname." ".$this->session->lastname
            );
            log_message('error', 'Request Teum Swap :'.print_r($swap, true));
            $result = $this->parateum->swap($swap);
            log_message('error', 'Result Teum Swap :'.print_r($result, true));

            if (strtolower($result->resultType) == "ok") {
                logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $service->userid,
                'serviceid' => $_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'SIMCARD swapped from ' . $service->details->msisdn_sim . ' to ' . $_POST['simcard']
                ));
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false, 'message' => implode(', ', $result->messages)));
            }
        } else {
            echo json_encode(array('result' => false, 'message' => 'access denied'));
        }
    }

    public function Teum_Divert()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $client    = $this->Admin_model->getClient($service->userid);
        $this->load->model('Agent_model');
        if ($service) {
            $divert = array("diverts" =>  array(
            array(
            "Active" =>  true,
            "Code" => "10300",
            "WaitTime" => "30",
            "msisdn" => $_POST['diver_number']
            )
            ),
            "externalReference" => "1",
            "channel" =>  "UnitedPortal V1",
            "comments" => "Executed by ".$this->session->firstname." ".$this->session->lastname
            );



            log_message('error', 'Request Teum Swap :'.print_r($divert, true));
            $result = $this->parateum->divert($service->details->msisdn, $divert);
            log_message('error', 'Result Teum divert :'.print_r($result, true));

            if (strtolower($result->resultType) == "ok") {
                logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $service->userid,
                'serviceid' => $_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Msisdn Diverted to ' .$_POST['diver_number']
                ));
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false, 'message' => implode(', ', $result->messages)));
            }
        } else {
            echo json_encode(array('result' => false, 'message' => 'access denied'));
        }
    }

    public function Teum_AcceptPortout()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $client    = $this->Admin_model->getClient($service->userid);
        $this->load->model('Agent_model');
        if ($service) {
            $swap = array(
            "msisdn" =>  $service->details->msisdn,
            "newIccId" =>$_POST['simcard'],
            "oldIccId" => $service->details->msisdn_sim,
            "externalReference" => $_POST['serviceid'],
            "channel" =>  "UnitedPortal V1",
            "comments" => "Executed by ".$this->session->firstname." ".$this->session->lastname
            );
            log_message('error', 'Request Teum Swap :'.print_r($swap, true));
            $result = $this->parateum->swap($swap);
            log_message('error', 'Result Teum Swap :'.print_r($result, true));

            if (strtolower($result->resultType) == "ok") {
                logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $service->userid,
                'serviceid' => $_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'SIMCARD swapped from ' . $service->details->msisdn_sim . ' to ' . $_POST['simcard']
                ));
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false, 'message' => implode(', ', $result->messages)));
            }
        } else {
            echo json_encode(array('result' => false, 'message' => 'access denied'));
        }
    }
    public function Teum_RejectPortout()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $client    = $this->Admin_model->getClient($service->userid);
        $this->load->model('Agent_model');
        if ($service) {
            $swap = array(
            "msisdn" =>  $service->details->msisdn,
            "newIccId" =>$_POST['simcard'],
            "oldIccId" => $service->details->msisdn_sim,
            "externalReference" => $_POST['serviceid'],
            "channel" =>  "UnitedPortal V1",
            "comments" => "Executed by ".$this->session->firstname." ".$this->session->lastname
            );
            log_message('error', 'Request Teum Swap :'.print_r($swap, true));
            $result = $this->parateum->swap($swap);
            log_message('error', 'Result Teum Swap :'.print_r($result, true));

            if (strtolower($result->resultType) == "ok") {
                logAdmin(array(
                'companyid' => $this->companyid,
                'userid' => $service->userid,
                'serviceid' => $_POST['serviceid'],
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'SIMCARD swapped from ' . $service->details->msisdn_sim . ' to ' . $_POST['simcard']
                ));
                echo json_encode(array('result' => true));
            } else {
                echo json_encode(array('result' => false, 'message' => implode(', ', $result->messages)));
            }
        } else {
            echo json_encode(array('result' => false, 'message' => 'access denied'));
        }
    }
    public function Teum_Topup()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $client    = $this->Admin_model->getClient($service->userid);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);
        if ($_POST['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $_POST['serviceid']);
                        break;
                    }
                } while (true);
            }

            if ($agent->reseller_type == "Prepaid") {
                if ($_POST['amount'] > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('You do not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $_POST['serviceid']);
                    redirect('admin/subscription/detail/'.$_POST['serviceid']);
                }
            }
        }
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->topup(array(
                'msisdn' => $service->details->msisdn,
                'amount' => $_POST['amount'] * 100
            ));
            $amount =  $_POST['amount'] * 100;

            if ($res->resultCode == "0") {
                if ($_POST['reseller_charge'] == "Yes") {
                    if ($agent->reseller_type == "Prepaid") {
                        $new_balance = $agent->reseller_balance - $_POST['amount'];
                        $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                        unlink(APPPATH.'lock/reseller_balance_'. $agent->id, $_POST['serviceid']);
                    }
                }
                $this->Admin_model->insertTopup(
                    array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' =>  $_POST['userid'],
                        'income_type' => 'topup',
                        'agentid' => $service->agentid,
                        'amount' => $_POST['amount'],
                      'user' => $this->session->firstname . ' ' . $this->session->lastname)
                );
                $this->session->set_flashdata('success', 'Topup ' . $amount . ' has been added to the msisdn');
                logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' =>  $_POST['userid'],
                        'user' => $this->session->firstname . ' ' . $this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' .  $_POST['serviceid'] . ' Topup amount : has been loaded '.$amount.' to '.$_POST['SN']
                    ));
            } else {
                $this->session->set_flashdata('error', 'there was an error handling your request');
            }
        } else {
            die('Access Denied');
        }
        //print_r($_POST);
        redirect('admin/subscription/detail/' . $_POST['serviceid']);
    }

    public function Teum_CompletePortin()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);

        $this->load->library('pareteum', array('companyid' => $this->session->cid));
        $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array('msisdn_status' => 'Active', 'msisdn'=> $service->details->donor_msisdn));
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $client = $this->Admin_model->getClient($service->userid);
        $AccountId = $service->details->teum_accountid;
        $subscriptionid = $service->details->teum_subscriptionid;
        /*
        // Replaced with Swap Msisdn
        $subs   = array(
                        "CustomerId" => (int) $client->teum_CustomerId,
                        "Items" => array(
                            array(
                                "AccountId" => (string) $AccountId,
                                "ProductOfferings" => getaddons_teum_base($_POST['serviceid'], $service->details->msisdn),
                                "ServiceAddress" => array(
                                    "Address" => $client->address1,
                                    "HouseNo" => $client->housenumber,
                                    "City" => $client->city,
                                    "ZipCode" => $client->postcode,
                                    "State" => "unknown",
                                    "CountryId" => "76"
                                )
                            )
                        ),
                        "channel" => "UnitedPortal V1 by ".$this->session->firstname
                    );
        log_message('error', print_r($subs, true));
        $subscription = $this->pareteum->AddSubscription($subs);

        */
        $subscription = $this->pareteum->swap_portin_msisdn($subscriptionid, array('from' => $service->details->msisdn_sn, 'to' => $service->details->donor_msisdn));
        log_message('error', 'Swapsim on Portability complete'.print_r($subscription, true));
        if ($subscription->resultCode == "0") {
            $this->session->set_flashdata('success', 'Portin has been set to completed');
            // $this->db->query("update a_reseller_simcard set SubscriptionId=? where serviceid=?", array($subscription->Subscription->SubscriptionId, $serviceid));
            //mme
           // $this->Admin_model->update_services_data('mobile', $serviceid, array('teum_customerid' => $client->teum_CustomerId, 'teum_subscriptionid' => $subscription->Subscription->SubscriptionId));
        }
        echo json_encode(array('result' => true));
    }


    public function Teum_addPortin()
    {
        $service_teum = $this->Admin_model->getService($_POST['serviceid']);

        $res = $this->Admin_model->update_services_data('mobile', $_POST['serviceid'], array(
                'msisdn_type' => 'porting',
                 'msisdn_status' => 'PortinPending',
                 'donor_msisdn' => $_POST['msisdn'],
                 'donor_accountnumber' => $_POST['pac'],
                 'date_modified' => date('Y-m-d H:i:s')
            ));

        if ($res) {
            echo json_encode(array('result' => $res));
        } else {
            echo json_encode(array('result' => $res, 'message' => 'data was not updated'));
        }
    }

    public function Teum_addBundle()
    {
        $service_teum = $this->Admin_model->getService($_POST['serviceid']);
        $client    = $this->Admin_model->getClient($service_teum->userid);
        $addons    = getAddonInformation($_POST['bundleid']);
        $this->load->model('Agent_model');
        $agent = $this->Agent_model->getAgent($client->agentid);

        if ($_POST['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                //we need to set time limit to 0 because we are going to lock balance to avoid the same request at the same time
                set_time_limit(0);
                do {
                    if (!file_exists(APPPATH.'lock/reseller_balance_'.$client->agentid)) {
                        log_message('error', 'No locking, we continue adding balance and now we lock the customer balance');
                        file_put_contents(APPPATH.'lock/reseller_'.$client->agentid, $_POST['serviceid']);
                        break;
                    }
                } while (true);
            }
        }


        log_message("error", print_r($_POST, true));
        if ($_POST['reseller_charge'] == "Yes") {
            if ($agent->reseller_type == "Prepaid") {
                if ($addons->recurring_total > $agent->reseller_balance) {
                    $this->session->set_flashdata('error', lang('You do not have enough balance to topup this subscriber'));
                    unlink(APPPATH.'lock/reseller_balance_'.$client->agentid, $_POST['serviceid']);
                    redirect('admin/subscription/detail/'.$_POST['serviceid']);
                }
            }
        }
        if ($service_teum) {
            $reseller_sim_card = $this->db->query("select * from a_reseller_simcard where MSISDN=?", array($service_teum->details->msisdn));
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service_teum->api_id
            ));


            //log_message("error", print_r($addons, true));
            $AccountId = $service_teum->details->teum_accountid;
            $subs = array(
                "CustomerId" => (int) $client->teum_CustomerId,
                "SubscriptionId" => $reseller_sim_card->row()->SubscriptionId,
                "Channel" => "UnitedPortal V1 ".$this->session->firstname,
                "Offerings" => array(
                    array(
                    "ProductOfferingId" => (int) $addons->bundleid,
                    "OrderedProductCharacteristics" => array(
                        array(
                            "Name" => "MSISDN",
                            "Value" => $service_teum->details->msisdn
                        )
                    )
                    )
                    )

            );

            log_message('error', print_r($subs, true));
            $subscription = $this->pareteum->subscriptions_offerings($subs);
            log_message('error', print_r($subscription, true));
            if ($subscription->resultCode == "0") {
                if ($_POST['reseller_charge'] == "Yes") {
                    if ($agent->reseller_type == "Prepaid") {
                        $new_balance = $agent->reseller_balance - $addons->recurring_total;
                        $this->db->query("update a_clients_agents set reseller_balance=? where id=?", array($new_balance, $agent->id));
                        unlink(APPPATH.'lock/reseller_balance_'.$agent->id, $_POST['serviceid']);
                    }
                }
                $ext = checkaddon_existance($_POST['bundleid'], $_POST['serviceid']);
                if ($ext) {
                    $offering = array(
                        'name' => $addons->name,
                        'terms' => $addons->bundle_duration_value,
                        'cycle' => $addons->bundle_duration_type,
                        'serviceid' => $_POST['serviceid'],
                        'addonid' => $_POST['bundleid'],
                        'companyid' => $this->companyid,
                        'recurring_total' => $addons->recurring_total,
                        'addon_type' => 'option',
                        'arta_bundleid' => $addons->bundleid,
                        'teum_autoRenew' => $_POST['month']-1,
                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                        'teum_DateStart' => $subscription->PurchaseOrder->CompletionDate,
                        'teum_CustomerOrderId' => $subscription->PurchaseOrder->CustomerOrderId,
                        'teum_SubscriptionId' => $reseller_sim_card->row()->SubscriptionId,
                        'teum_ProductId' => null,
                        'teum_ProductChargePurchaseId' => null,
                        'teum_SubscriptionProductAssnId' => null,
                        'teum_ServiceId' => null
                        );
                    $this->Admin_model->insertAddon($offering);


                    $this->Admin_model->updateAddon(array(
                        'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31)), $ext->id);
                } else {
                    $offering = array(
                    'name' => $addons->name,
                    'terms' => $addons->bundle_duration_value,
                    'cycle' => $addons->bundle_duration_type,
                    'serviceid' => $_POST['serviceid'],
                    'addonid' => $_POST['bundleid'],
                    'companyid' => $this->companyid,
                    'recurring_total' => $addons->recurring_total,
                    'addon_type' => 'option',
                    'arta_bundleid' => $addons->bundleid,
                    'teum_autoRenew' => $_POST['month']-1,
                    'teum_NextRenewal' => getFuturedate(date('Y-m-d'), 'day', 31),
                    'teum_DateStart' => $subscription->PurchaseOrder->CompletionDate,
                    'teum_CustomerOrderId' => $subscription->PurchaseOrder->CustomerOrderId,
                    'teum_SubscriptionId' => $reseller_sim_card->row()->SubscriptionId,
                    'teum_ProductId' => null,
                    'teum_ProductChargePurchaseId' => null,
                    'teum_SubscriptionProductAssnId' => null,
                    'teum_ServiceId' => null
                    );
                    $this->Admin_model->insertAddon($offering);
                }
                $this->Admin_model->insertTopup(
                    array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' =>  $_POST['userid'],
                        'income_type' => 'bundle',
                        'agentid' => $client->agentid,
                        'amount' => $addons->recurring_total,
                        'bundle_name' => $addon->name,
                        'user' => $this->session->firstname . ' ' . $this->session->lastname)
                );

                $this->session->set_flashdata('success', 'bundle has been added');
                logAdmin(array(
                        'companyid' => $this->companyid,
                        'serviceid' => $_POST['serviceid'],
                        'userid' =>  $_POST['userid'],
                        'user' => $this->session>firstname . ' ' . $this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Service : ' .  $_POST['serviceid'] . ' Bundle: '.$addons->name.' has been added'
                    ));
            } else {
                $this->session->set_flashdata('error', 'bundle has not been  added');
            }
            redirect('admin/subscription/detail/' . $_POST['serviceid']);
        }
    }
    public function Teum_Usage()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->usage(array(
                'msisdn' => $service->details->msisdn,
                'fromDate' => trim($_POST['from']).'T00:00:00',
                'toDate' => trim($_POST['to']).'T23:59:59'
            ));
            echo json_encode($res);
        } else {
            die('Access Denied');
        }
    }

    public function Teum_deleteSubscription()
    {
        /*
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->deleteSubscription($_POST['subscriptionid']);
            echo json_encode($res);
        } else {
            die('Access Denied');
        }
        */

        die('Access Denied');
    }

    public function Teum_Freeze()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->freeze(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'SubscriptionFreezeReason' => 'FREEZE_CUSTREQUEST',
                'ExternalReference' => $_POST['ExternalRef'],
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ));
            log_message('error', 'request'.print_r(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'SubscriptionFreezeReason' => 'FREEZE_CUSTREQUEST',
                'ExternalReference' => $_POST['ExternalRef'],
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ), true));

            log_message('error', 'response'.print_r($res, true));
            if (strtolower($res->resultType) != "ok") {
                $this->session->set_flashdata('error', implode(' ', $res->messages));
            } else {
                $this->session->set_flashdata('success', 'Freeze successfull');
            }

            redirect('admin/subscription/detail/'.$_POST['serviceid']);
        } else {
            die('Access Denied');
        }
    }
    public function Teum_Unfreeze()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($service) {
            $this->load->library('pareteum', array(
                'companyid' => $this->companyid,
                'api_id' => $service->api_id
            ));
            $res = $this->pareteum->unfreeze(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'ExternalReference' => '',
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ));
            log_message('error', 'request'.print_r(array(
                'msisdn' => $service->details->msisdn,
                'SubscriptionId' => $this->Admin_model->getTeumSubscriptionId($service->details->msisdn),
                'ExternalReference' => '',
                'channel' => 'UnitedPortal V1',
                'comments' => 'Freeze by: ' . $this->session->firstname . ' ' . $this->session->lastname
            ), true));
            log_message('error', 'response'.print_r($res, true));
            echo json_encode($res);

            if (strtolower($res->resultType) == "ok") {
                $this->session->set_flashdata('success', 'Subscirption has been resumed, it may takes sometimes to take effect');
            } else {
                $this->session->set_flashdata('error', implode(' ', $res->messages));
            }

            redirect('admin/subscription/detail/'.$_POST['serviceid']);
        } else {
            die('Access Denied');
        }
    }
    public function Teum_addBundlex()
    {
        /*$service = $this->Admin_model->getService($_POST['serviceid']);
        if($service){

        $this->load->library('pareteum', array('companyid' => $this->companyid));
        $res = $this->pareteum->usage(array('msisdn'=> $service->details->msisdn, 'fromDate' => $_POST['from'], 'toDate' =>  $_POST['to']));

        echo json_encode($res);

        }else{

        die('Access Denied');
        }
        */
    }

    public function Teum_getSubscription()
    {
        $service = $this->Admin_model->getService($_POST['serviceid']);
        $this->load->library('pareteum', array(
            'companyid' => $this->companyid,
                'api_id' => $service->api_id
        ));
        $res = $this->pareteum->getsubscription(array(
                'CustomerId' => 1065076740,
                'IncludeInactiveSubscriptions' => 1,
                'ActiveFrom' => '2018-07-01',
                'ActiveTo' => '2019-07-26',
                'MSISDN' => '447421966432',
                'IncludeHistoricInformation' => 1,
                'channel' => 'MVNO portal V1'
            ));
        print_r($res);
    }

    public function Teum_Enable_Disable_Autorenew()
    {
        $addon  = getAddonsbyID($_POST['addonid']);
        $service = $this->Admin_model->getService($_POST['serviceid']);
        if ($_POST['typeaction'] == "1") {
            if ($addon->teum_NextRenewal >= date('Y-m-d')) {
                $this->db->query("update a_services_addons set teum_autoRenew =? where id= ? ", array($_POST['ContractDuration'], $_POST['addonid']));
            //log_message('error', $this->db->last_query());
            } else {
            }

            logAdmin(array(
                'companyid' => $this->companyid,
                'serviceid' => $_POST['serviceid'],
                'userid' =>    $service->userid,
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Service : ' .  $_POST['serviceid'] . ' has been set to be auto renewed for  '.$_POST['ContractDuration'].' for addon id: '.$_POST['addonid']
            ));
        } else {
            $this->db->query("update a_services_addons set teum_autoRenew =? where id= ? ", array(0, $_POST['addonid']));
            logAdmin(array(
                'companyid' => $this->companyid,
                'serviceid' => $_POST['serviceid'],
                'userid' =>    $service->userid,
                'user' => $this->session->firstname . ' ' . $this->session->lastname,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'description' => 'Service: ' .  $_POST['serviceid'] . ' auto renew has been set be disabled  for addon id: '.$_POST['addonid']
            ));
        }
        $this->session->set_flashdata('success', lang('Auto renewal has been set'));
        redirect('admin/subscription/detail/'.$_POST['serviceid']);
    }

    public function Teum_list_sim_pool()
    {
        $this->data['main_content'] = admin_theme($this->data['setting']->default_theme) . 'simpool';
        $this->load->view(admin_theme($this->data['setting']->default_theme) . 'content', $this->data);
    }
}
