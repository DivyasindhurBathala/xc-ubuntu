<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ApiController extends CI_Controller {

	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		$this->config->set_item('language', 'english');
		$this->lang->load('admin');
		$this->setProperty();
	}

	public function setProperty() {
		$this->data['setting'] = globo();
	}

}
class Xdsl extends ApiController {

	public function __construct() {
		parent::__construct();

		$this->load->model('Admin_model');
		$this->load->library('Arraytoxml');
		$this->headers = array_change_key_case($this->input->request_headers(), CASE_LOWER);

		$this->clientip = $_SERVER['REMOTE_ADDR'];
		if (!empty($this->headers['content-type'])) {
			if ($this->headers['content-type'] == "application/xml") {
				header('Content-Type: application/xml');

				$this->type_request = "xml";
			} else {
				header('Content-Type: application/json');
				$this->type_request = "json";
			}
		} else {
			header('Content-Type: application/json');
			$this->type_request = "json";
		}

		$keys = array('dW5pdGVkLXRlbGVjb20tHnJO9FNVBVD76kGfjlMxWbzl', 'dW5pdGVkLXRlbGVjb20tdGVzdC1hY2NvdW50');
		if (!empty($this->headers['x-api-key'])) {
			if (!in_array($this->headers['x-api-key'], $keys)) {
				header("HTTP/1.1 401 Unauthorized");
				echo json_encode(array('result' => 'error', 'message' => 'Authentication required'));
				exit;
			}
		} else {
			header("HTTP/1.1 401 Unauthorized");
			echo json_encode(array('result' => 'error', 'message' => 'Authentication required'));
			exit;
		}
		$post = file_get_contents("php://input");
		$obj = (array) json_decode($post);
		$this->dpost = $obj;
	}
	public function fonts() {
		echo "
@font-face {
  font-family: 'Campton';
  font-style: normal;
  font-weight: 400;
  src: local('Tangerine Regular'), local('Tangerine-Regular'), url(https://fonts.gstatic.com/s/tangerine/v9/IurY6Y5j_oScZZow4VOxCZZMprNA4A.woff2) format('woff2');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}";

	}
	public function checkaddress() {

		$error = array();
		$required = array('postcode', 'city', 'number', 'street');

		foreach ($required as $key) {
			if (!array_key_exists($key, $this->dpost)) {

				$error[] = $key . ' is missing';

			}
		}

		if (empty($error)) {
			$this->dpost['action'] = "checkaddress";
			$result = $this->Admin_model->billi_api($this->dpost);
			$result = (array) $result;
			$result['adsl'] = 'NOTOK';
			unset($result['status']);
			header("HTTP/1.1 200 OK");
			echo $this->set_response(array('result' => 'success', 'xdsl_info' => $result, 'address' => $this->dpost));
		} else {
			header("HTTP/1.1 400 Bad Request");
			echo $this->set_response(array('result' => 'error', 'message' => 'missing required fields', 'fields' => $error));
		}

	}

	public function set_response($res) {
		if ($this->type_request == "xml") {
			$xml = $this->arraytoxml->createXML('data', $res);

			return $xml->saveXML();
		} else {

			return json_encode($res);
		}
	}

}