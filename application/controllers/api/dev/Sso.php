<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SsoController extends CI_Controller {

	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		$this->config->set_item('language', 'english');
		$this->lang->load('admin');
		$this->setProperty();
	}

	public function setProperty() {
		$this->data['setting'] = globo();
	}

}
class Sso extends SsoController {

	public function __construct() {
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->model('Api_model');
		$this->load->library('Arraytoxml');
		$this->headers = array_change_key_case($this->input->request_headers(), CASE_LOWER);

		$this->clientip = $_SERVER['REMOTE_ADDR'];
		if (!empty($this->headers['content-type'])) {
			if ($this->headers['content-type'] == "application/xml") {
				header('Content-Type: application/xml');

				$this->type_request = "xml";
			} else {
				header('Content-Type: application/json');
				$this->type_request = "json";
			}
		} else {
			header('Content-Type: application/json');
			$this->type_request = "json";
		}

		//$keys = array('dW5pdGVkLXRlbGVjb20tHnJO9FNVBVD76kGfjlMxWbzl', 'dW5pdGVkLXRlbGVjb20tdGVzdC1hY2NvdW50');
		if (!empty($this->headers['x-api-key'])) {
			if (!$this->Api_model->ValidateKey($this->headers['x-api-key'])) {
				header("HTTP/1.1 401 Unauthorized");
				echo json_encode(array('result' => 'error', 'message' => 'Authentication required.', 'key' => $this->headers['x-api-key']));
				exit;
			}
		} else {
			header("HTTP/1.1 401 Unauthorized");
			echo json_encode(array('result' => 'error', 'message' => 'Authentication required'));
			exit;
		}
		$post = file_get_contents("php://input");
		$obj = (array) json_decode($post);
		$this->dpost = $obj;
	}

	public function auth() {
		if (!empty($this->dpost['userid'])) {
			$auth = $this->Api_model->getSSOToken($this->dpost['userid']);
			echo json_encode($auth);
		} else {
			echo json_encode(array('result' => 'error', 'message' => 'required field missing'));
		}

	}

}