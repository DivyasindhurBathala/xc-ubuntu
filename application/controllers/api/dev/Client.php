<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ClientController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        header('Content-Type: application/json');
        $this->load->model('Clientapi_model', 'api');
    }
}
class Client extends ClientController
{
    public function __construct()
    {
        error_reporting(1);
        parent::__construct();
        $this->load->helper('string');
        if (isset($_SERVER['HTTP_ORIGIN']))
        {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');
        }
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS')
        {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            {
                header("Access-Control-Allow-Methods: GET");
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            {
                header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }
            exit(0);
        }
        $g = $this->input->request_headers();
        foreach ($g as $key => $header)
        {
            $headers[strtolower($key)] = $header;
        }
        if (!in_array($this->uri->segment(4), array(
            'login',
            'reset',
            'getcompanylist',
            'confirm',
            'upload',
            'download_invoice',
            'download_proforma'
        )))
        {
            $this->load->helper(array('jwt', 'authorization'));
            $decrypt = json_decode($this->encryption->decrypt($headers['x-api-key']));
            $auth    = $this->api->validate_client_token($decrypt);
            if (!$auth)
            {
            	header("HTTP/1.0 401 Authentication Required");
                echo json_encode(array(
                    'result' => 'error',
                    'message' => 'Unauthorized access',
                    'auth' => $decrypt
                ));
                exit;
            }
           // $this->api->update_present($auth->id);
        }
        $this->clientid         = $auth->id;
        $this->client_name      = $auth->firstname . ' ' . $auth->lastname;
        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr']    = $_SERVER['REMOTE_ADDR'];
        $this->companyid       = get_companyidby_url(base_url());
        $this->headers         = $headers;
        $ip                    = $_SERVER['REMOTE_ADDR'];
        $this->ip              = $ip;
        $post                  = file_get_contents("php://input");
        if (isPost())
        {
            $obj         = (array) json_decode($post);
            $this->dpost = $obj;
        }
        else
        {
            if (!empty($post))
            {
                $obj         = (array) json_decode($post);
                $this->dpost = $obj;
            }
            else
            {
                $this->dpost = array();
            }
        }
        $this->setProperty();
    }
    public function setProperty()
    {
        $this->data['setting'] = globofix($this->companyid);
        //$this->getStafOnline();
    }
    public function __response($data){

    	echo json_encode($data);
    	exit;
    }

    public function login()
    {
       $this->load->model('Auth_model');
        if (!empty($this->dpost['username']) && !empty($this->dpost['password']))
        {
            if (isPost())
            {
                $valid = $this->Auth_model->client_auth($this->dpost);
                if($valid['result'] != "error"){
                	 unset($valid['client']['password']);
                $valid['validity']     = time() + 31556926;
                //$valid['ip'] =$_SERVER['REMOTE_ADDR'];
                //$valid['socket_token'] = $this->encryption->encrypt($this->companyid . '#' . $valid['admin']['id'] . "#" . $valid['username'] . '#' . time());
                $valid['token']        = $this->encryption->encrypt(json_encode(array(
                    'companyid' => $this->companyid,
                    'clientid' => $valid['client']['id'],
                    'email' => $valid['client']['email'],
                    'ip' => $_SERVER['REMOTE_ADDR']
                )));
                unset($valid['client']);

                }else{

                	unset($valid['token']);

                }
               
                echo json_encode($valid);
            }
        }
        else
        {
            echo json_encode(array(
                'result' => false,
                'message' => 'Username & Password my not be empty'
            ));
        }
        
    }

    public function profile()
    {

    	$this->__response($this->api->getClient($this->clientid));


    }




}