<?php
defined('BASEPATH') or exit('No direct script access allowed');
class ApiController extends CI_Controller
{

    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'english');
        $this->lang->load('admin');
        $this->setProperty();
    }

    public function setProperty()
    {
        $this->data['setting'] = globo();
    }
}
class Xdsl extends ApiController
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Admin_model');
        $this->load->library('Arraytoxml');
        $this->headers = array_change_key_case($this->input->request_headers(), CASE_LOWER);

        $this->clientip = $_SERVER['REMOTE_ADDR'];
        if (!empty($this->headers['content-type'])) {
            if ($this->headers['content-type'] == "application/xml") {
                header('Content-Type: application/xml');

                $this->type_request = "xml";
            } else {
                header('Content-Type: application/json');
                $this->type_request = "json";
            }
        } else {
            header('Content-Type: application/json');
            $this->type_request = "json";
        }

        $keys = array('dW5pdGVkLXRlbGVjb20tHnJO9FNVBVD76kGfjlMxWbzl', 'dW5pdGVkLXRlbGVjb20tdGVzdC1hY2NvdW50');
        if (!empty($this->headers['x-api-key'])) {
            if (!in_array($this->headers['x-api-key'], $keys)) {
                header("HTTP/1.1 401 Unauthorized");
                echo json_encode(array('result' => 'error', 'message' => 'Authentication required'));
                exit;
            }
        } else {
            header("HTTP/1.1 401 Unauthorized");
            echo json_encode(array('result' => 'error', 'message' => 'Authentication required'));
            exit;
        }
        $post = file_get_contents("php://input");
        $obj = (array) json_decode($post);
        $this->dpost = $obj;
    }

    public function checkaddress()
    {

        $error = array();
        $required = array('postcode', 'city', 'number', 'street');

        foreach ($required as $key) {
            if (!array_key_exists($key, $this->dpost)) {
                $error[] = $key . ' is missing';
            }
        }

        if (empty($error)) {
            $this->dpost['action'] = "checkaddress";
            $result = $this->Admin_model->billi_api($this->dpost);
            $result = (array) $result;
            $result['adsl'] = 'NOTOK';
            unset($result['status']);
            header("HTTP/1.1 200 OK");
            echo $this->set_response(array('result' => 'success', 'xdsl_info' => $result, 'address' => $this->dpost));
        } else {
            header("HTTP/1.1 400 Bad Request");
            echo $this->set_response(array('result' => 'error', 'message' => 'missing required fields', 'fields' => $error));
        }
    }

    function get_city()
    {
        $list = array();
        $keyword = trim($this->uri->segment(5)) . '%';
        $this->db = $this->load->database('default', true);
        $query = $this->db->query("SELECT code as value,concat(code,' ',city) as label,city from public_zip where code like ? LIMIT 20", array($keyword));
        if ($query->num_rows() > 0) {
            $list = $query->result_array();
        }
        echo json_encode($list);
    }

    function get_street()
    {
        $list = array();
        $zip = trim($this->uri->segment(5));
        $keyword = '%' . $this->dpost['keyword'] . '%';
        if (!empty($keyword)) {
            $this->db = $this->load->database('default', true);
            $query = $this->db->query("SELECT name as value,name as label from public_street where zip=? and  name like ? LIMIT 20", array($zip, $keyword));
            if ($query->num_rows() > 0) {
                $list = $query->result_array();
            }
        }

        echo json_encode($list);
    }
    public function set_response($res)
    {
        if ($this->type_request == "xml") {
            $xml = $this->arraytoxml->createXML('data', $res);

            return $xml->saveXML();
        } else {
            return json_encode($res);
        }
    }
}
