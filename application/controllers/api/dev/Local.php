<?php
defined('BASEPATH') or exit('No direct script access allowed');
class LocalController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        $this->config->set_item('language', 'dutch');
        $this->lang->load('admin');

        header('Content-Type: application/json');
    }
}
class Local extends LocalController
{

    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model("Admin_model");
        $this->load->model("Api_model");
        $this->load->model('Teams_model');

        $g = $this->input->request_headers();
        foreach ($g as $key => $header) {
            $headers[strtolower($key)] = $header;
        }
        $headers['uri_string'] = $this->uri->uri_string();
        $headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
        if (!in_array($headers['ip_addr'], array( "51.255.146.43","35.187.167.30" ))) {
            echo json_encode(array('result' => 'error',$headers['ip_addr'] ."Blocked" ));
        }
        $this->headers = $headers;
        if (isPost()) {
            $post = file_get_contents("php://input");
            $obj = (array) json_decode($post);
            $this->dpost = $obj;
            $this->companyid = $obj['companyid'];
        } else {
            $this->dpost = array();
        }
    }
    public function test()
    {
        echo json_encode($this->dpost);
    }
    public function sync_general_pricing()
    {
        $this->load->library('magebo', array('companyid' => $this->companyid));
        $service = $this->db->query("select * from a_services where id=?", array($this->dpost['serviceid']));
        if ($service->num_rows()>0) {
            foreach ($service->result() as $row) {
                $mobile = $this->Admin_model->getServiceCli($row->id);
                if (!$row->iGeneralPricingIndex) {
                    echo $this->magebo->addPricingSubscription($mobile)."\n";
                    if ($mobile->details->msisdn_type == "porting") {
                        if (substr(trim($mobile->details->msisdn_sn), 0, 2) == '31') {
                            $this->Api_model->PortIngAction1($mobile);
                            $this->Api_model->PortIngAction2($mobile);
                            $this->Api_model->PortIngAction3($mobile);
                            $this->Api_model->PortIngAction4($mobile);
                            $this->Api_model->PortIngAction5($mobile->companyid);
                            $headers = "From: noreply@united-telecom.be" . "\r\n" .
                            "BCC: mail@simson.one";
                            mail('simson.parlindungan@united-telecom.be', 'Please check Porting activation in Magebo ', print_r($mobile, true), $headers);
                        } else {
                            $this->artilium->PartialFullBar($mobile->details, '0');
                        }
                        unset($mobile);
                    }
                }
            }
            echo json_encode(array('result' => 'success'));
        }
    }
    public function addSimPorting()
    {

        $this->load->library('magebo', array('companyid' => $this->companyid));
        $order = $this->Admin_model->getServiceCli($this->dpost['serviceid']);
        $addsim = $this->magebo->AddPortinSIMToMagebo($order);
        $this->Api_model->PortIngAction1($order);
        $this->Api_model->PortIngAction2($order);
        $this->Api_model->PortIngAction3($order);
        $this->Api_model->PortIngAction4($order);
        $this->Api_model->PortIngAction5($order->companyid);
        echo json_encode(array('result' => 'success'));
    }

    public function enable_service()
    {
        $this->db  = $this->load->database('default', true);
        $this->load->library('artilium', array('companyid' => $this->dpost['companyid']));
        $this->load->library('magebo', array('companyid' => $this->dpost['companyid']));

        if (substr(trim($this->dpost['sn']), 0, 2) == '32') {
            $order = $this->Admin_model->GetServiceCli($this->dpost['serviceid']);
            $this->artilium->PartialFullBar($order->details, '0');
        } else {
            $pack   = $this->artilium->GetListPackageOptionsForSnAdvance(trim($this->dpost['sn']));
            $this->artilium->UpdateServices(trim($this->dpost['sn']), $pack, '1');
            $order = $this->Admin_model->GetServiceCli($this->dpost['serviceid']);
        }
        $this->magebo->addPricingSubscription($order);
        if($order->details->msisdn_type == "porting"){
            if (substr(trim($order->details->msisdn), 0, 2) == "31") {
                $addsim = $this->magebo->AddPortinSIMToMagebo($order);
                $this->Api_model->PortIngAction1($order);
                $this->Api_model->PortIngAction2($order);
                $this->Api_model->PortIngAction3($order);
                $this->Api_model->PortIngAction4($order);
                $this->Api_model->PortIngAction5($order->companyid);

                $headers = "From: noreply@united-telecom.be" . "\r\n" .
                "BCC: mail@simson.one";
                mail('simson.parlindungan@united-telecom.be', 'Please check Porting activation in Magebo ', print_r($order, true), $headers);
            }
        }
                $this->Admin_model->ChangeServiceStatus($row->serviceid, 'Active');
                $this->Admin_model->ChangeOrdertatus($row->serviceid, 'Active');
                $this->Admin_model->setEventDone($row->id);
                print_r($row);
                unset($row);
                unset($order);
                unset($pack);
                unset($this->artilium);
                unset($this->magebo);
    }
    function send_email()
    {
        $service  = $this->db->query("select a.*,b.date_contract,c.mageboid,c.mvno_id from a_services_mobile a left join a_services b on b.id=a.serviceid  left join a_clients c on c.id=b.userid where a.serviceid = ?", array(
            $this->dpost['serviceid']
         ));
        if ($service->num_rows()>0) {
            $client        = getCompanydataByUserID($service->row()->userid);
            if ($this->dpost['type'] == "Accepted") {
               echo $this->sendEmailNotification($service->row(), "Number Porting has been Accepted on ".date('Y-m-d H:i:s') ." SN:" . $service->row()->msisdn_sn, "Accept", $client);
            } elseif ($this->dpost['type'] == "Rejected") {
               echo  $this->sendEmailNotification($service->row(), "Number Porting has been Rejected on ".date('Y-m-d H:i:s') ." SN:" . $service->row()->msisdn_sn, "Rejected", $client);
            } elseif ($this->dpost['type'] == "Done") {
               echo  $this->sendEmailNotification($service->row(), "Number Porting has been Rejected on ".date('Y-m-d H:i:s') ." SN:" . $service->row()->msisdn_sn, "Completed", $client);
            } elseif($this->dpost['type'] == "OrderActive"){
                echo $this->send_welcome_email($this->dpost['serviceid']);
            } elseif($this->dpost['type'] == "PortinRejected"){
                echo $this->send_PortinRejected($this->dpost['serviceid']);
            } elseif($this->dpost['type'] == "PortinAccepted"){
                echo $this->send_PortinAccepted($this->dpost['serviceid']);
            }
        } else {
            echo json_encode(array('result' => 'error','message' => 'service not found', 'data' => $this->dpost));
        }
    }
    function sendEmailNotification($service, $subject, $message, $client)
    {
        if (!empty($client->email_notification)) {
            $this->data['setting'] = globofix($service->companyid);

            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                 'protocol'  => 'smtp',
                 'smtp_host' => $this->data['setting']->smtp_host,
                 'smtp_port' => $this->data['setting']->smtp_port,
                 'smtp_user' => $this->data['setting']->smtp_user,
                 'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                 'mailtype'  => 'html',
                 'charset'   => 'utf-8',
                 'starttls'  => true,
                 'wordwrap'  => true,
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset']  = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);

            $msg = "Hello Team,\n";
            $msg .= "\n";
            $msg .= "Client ID         : ".$client->mvno_id."\n";
            $msg .= "Artilium ID       : ".$client->mageboid."\n";
            $msg .= "MSISDN Number     : ".$service->msisdn."\n";
            $msg .= "Serial Number     : ".$service->msisdn_sn."\n";
            $msg .= "Simcard Number    : ".$service->msisdn_sim."\n";
            $msg .= "Event Type ID     : ".$message."\n";
            $msg .= "Please Open The client and check this ".$client->portal_url."admin/client/detail/".$client->id."\n\n";
            $msg .= "Regards\n";
            $msg .= "\n";
            $msg .= "If you wish to recieved  this event to be sent to your webservices, please contact our support\n";
            $body = $msg;
            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->bcc('mail@simson.one');
            $this->email->to($client->email_notification);
            $this->email->subject($subject);

            $this->email->message($body);
            if (!$this->email->send()) {
                logEmailOut(array('userid' => $client->id, 'to' => $client->email_notification, 'subject' => $subject, 'message' => $msg, 'status' => 'not_sent', 'error_message' => $this->email->print_debugger()));
                return json_encode(array('result' => 'error', 'message' => $this->email->print_debugger()));
            }else{
                logEmailOut(array('userid' => $client->id, 'to' => $client->email_notification, 'subject' => $subject, 'message' => $msg));
                return json_encode(array('result' => 'success'));
            }
        }
    }

    public function send_welcome_email($id){
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(TRUE);
        $this->email->initialize($config);


        $body = getMailContent('order_accepted', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', $client->salutation.' '.$client->initial.' '.$client->firstname . ' ' . $client->lastname, $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        if($client->companyid == 53){
        $combi   = $this->Admin_model->GetProductType($mobile->packageid);
                if ($combi == 2)
                {
                        $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. Vanaf nu ontvangt u daarom thuis 25 Mbps extra downloadsnelheid (tot maximaal 400 Mbps). Nog sneller streamen, downloaden en gamen dus!</p>";
                } //$combi == 2
                elseif ($combi == 1)
                {
                        $com = "<p>U heeft thuis al internet van DELTA. Wij geven trouwe klanten graag meer dan alleen een bedankje. U ontvangt daarom elke maand de volgende gratis extra’s:</p><p>
-   Dubbele data en Onbeperkt bellen met je mobiel<br />
-   € 5,– korting op uw DELTA Internet factuur<br />
-   25 Mbps extra downloadsnelheid (tot maximaal 400Mbps).<br />
-   Een extra TV-pakket boordevol sport-, muziek-, entertainment-, en jeugdzenders<br /></p>";
                } //$combi == 1
                elseif ($combi == 0)
                {
                        $com = "<p>We zijn erg blij met u als klant. Daarom krijgt u als welkomstcadeau een halfjaar € 5,– maandelijkse korting op uw mobiele abonnement. U hoeft daar verder niets voor te doen. </p>";
                } //$combi == 0
        $body = str_replace('{$combi}', $com, $body);

        }
        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $subject = getSubject('order_accepted', $client->language, $client->companyid);
        $this->email->subject($subject);
        //$this->email->subject(lang("Your New Password"));
        $this->email->bcc('mail@simson.one');
        $this->email->message($body);

        if (!$this->email->send()) {
            logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => $subject, 'message' => $body, 'status' => 'not_sent', 'error_message' => $this->email->print_debugger()));
            return json_encode(array('result' => 'error', 'message' => $this->email->print_debugger()));
        }else{
            logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => $subject, 'message' => $body));
            return json_encode(array('result' => 'success'));
        }
    }

    public function send_PortinRejected($id)
    {
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);


        $body = getMailContent('portin_rejected', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->etails->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->etails->msisdn_puk2, $body);
        $body = str_replace('{$msisdn', $mobile->etails->msisdn, $body);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);

        $subject = getSubject('portin_rejected', $client->language, $client->companyid);
        $this->email->subject($subject);
        $this->email->bcc('mail@simson.one');
        $this->email->message($body);
        if (!$this->email->send()) {
            logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => $subject, 'message' => $body, 'status' => 'not_sent', 'error_message' => $this->email->print_debugger()));
            return json_encode(array('result' => 'error', 'message' => $this->email->print_debugger()));
        }else{
            logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => $subject, 'message' => $body));
            return json_encode(array('result' => 'success'));
        }
    }
    public function send_PortinAccepted($id)
    {
        $this->load->model('Admin_model');
        $mobile = $this->Admin_model->getServiceCli($id);
        $client = $this->Admin_model->getClient($mobile->userid);
        $this->data['setting'] = globofix($client->companyid);
        if ($this->data['setting']->smtp_type == 'smtp') {
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => $this->data['setting']->smtp_host,
                'smtp_port' => $this->data['setting']->smtp_port,
                'smtp_user' => $this->data['setting']->smtp_user,
                'smtp_pass' => $this->data['setting']->smtp_pass,
                'mailtype' => 'html',
                'charset' => 'utf-8',
                'starttls' => true,
                'wordwrap' => true,
            );
        } else {
            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['mailtype'] = 'html';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = true;
        }
        $this->email->clear(true);
        $this->email->initialize($config);


        $body = getMailContent('portin_accepted', $client->language, $client->companyid);
        // $body = $this->load->view('email/content', $this->data, true);
        $body = str_replace('{$name}', format_name($client), $body);
        $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
        $body = str_replace('{$msisdn_sim}', $mobile->details->msisdn_sim, $body);
        $body = str_replace('{$msisdn_puk1}', $mobile->etails->msisdn_puk1, $body);
        $body = str_replace('{$msisdn_puk2}', $mobile->etails->msisdn_puk2, $body);
        $body = str_replace('{$msisdn', $mobile->etails->msisdn, $body);

        $this->email->set_newline("\r\n");
        $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
        $this->email->to($client->email);
        $this->email->bcc('mail@simson.one');
        $subject = getSubject('portin_accepted', $client->language, $client->companyid);
        $this->email->subject($subject);
        $this->email->message($body);
        if (!$this->email->send()) {
            logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => $subject, 'message' => $body, 'status' => 'not_sent', 'error_message' => $this->email->print_debugger()));
            return json_encode(array('result' => 'error', 'message' => $this->email->print_debugger()));
        }else{
            logEmailOut(array('userid' => $client->id, 'to' => $client->email, 'subject' => $subject, 'message' => $body));
            return json_encode(array('result' => 'success'));
        }
    }

    public function update_client(){

   $client_detail = $this->Admin_model->getClient($this->dpost['id']);
   $this->dpost['mvno_id'] = strtoupper(trim($this->dpost['mvno_id']));
   $this->dpost['email'] = strtolower(trim($this->dpost['email']));
   $client         = $this->Admin_model->updateCustomer($this->dpost);
   $d              = $this->Admin_model->getClient($this->dpost['id']);
   $this->load->library('magebo', array('companyid' => $d->companyid));
   $result = $this->magebo->updateClient($d->mageboid, $this->dpost);

   if ($client['result']) {
    logAdmin(array('companyid' => $d->companyid, 'userid' => $this->dpost['id'], 'user' => 'Lex Van Hondel', 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Modify customer ' . $this->dpost['id'] . ' with data:' . implode(' ', $this->dpost)));
    $this->session->set_flashdata('success', lang('Customer information has been updated'));
   }
   echo json_encode($this->dpost);
    }

}
