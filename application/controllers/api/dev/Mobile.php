<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MobileController extends CI_Controller {
	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		$this->config->set_item('language', 'english');
		$this->lang->load('admin');
		$this->setProperty();
		header('Content-Type: application/json');
	}

	public function setProperty() {
		$this->data['setting'] = globo();
	}

}
class Mobile extends MobileController {

	public function __construct() {
		error_reporting(1);
		parent::__construct();
		$this->load->model('Teams_model');
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');
		}
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
				header("Access-Control-Allow-Methods: GET");
			}
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
				header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
			}
			exit(0);
		}
		$g = $this->input->request_headers();
		foreach ($g as $key => $header) {
			$headers[strtolower($key)] = $header;

		}
		$headers['uri_string'] = $this->uri->uri_string();
		$headers['ip_addr'] = $_SERVER['REMOTE_ADDR'];
		$this->headers = $headers;
		$ip = $_SERVER['REMOTE_ADDR'];
		$this->ip = $ip;
		if (isPost()) {
			$post = file_get_contents("php://input");
			$obj = (array) json_decode($post);
			$this->dpost = $obj;

		} else {
			$this->dpost = array();
		}
		$this->Teams_model->send_msg($this->ip . ' :' . $this->uri->segment(4), "<br /><br /> REQUEST:<br />" . print_r($this->dpost, true));
	}
	public function getClients(){

		if($_GET['type'] == "all"){
			$this->db->like('firstname', '%'.$_GET['terms'].'%');
			$this->db->or_like('lastname', '%'.$_GET['terms'].'%');
			$q = $this->db->get('a_clients');
		}else{
			$this->db->like('firstname', '%'.$_GET['terms'].'%');
			$this->db->limit('4');
			$q = $this->db->get('a_clients');
		}

		echo json_encode($q->result_array());

	}
	public function login() {
		
		$this->load->model('Auth_model');
		if (!empty($this->dpost['username']) && !empty($this->dpost['password'])) {
			if (isPost()) {
				$valid = $this->Auth_model->client_auth($this->dpost);
				unset($valid['password']);
				$valid['validity'] = time();
				if (empty($valid['client']['uuid'])) {

					$valid['client']['uuid'] = gen_uuid();
					$this->Auth_model->updateUUId($valid['client']);
				}
				echo json_encode($valid);

			}

		} else {

			echo json_encode(array('result' => false, 'message' => 'Username & Password my not be empty'));
		}

	}

	public function reset_pass() {
		$this->load->helper('string');
		$this->db = $this->load->database('default', true);
		$this->db->like('email', $this->dpost['email']);
		$q = $this->db->get('tblclients');
		if ($q->num_rows() > 0) {
			$this->load->model('Client_model');
			$password = rand(100000000, 999999999);
			$cliens = $this->Client_model->whmcs_api(array('action' => 'UpdateClient', 'clientid' => $q->row()->id, 'password2' => $password));
			if ($this->data['setting']->smtp_type == 'smtp') {
				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => $this->data['setting']->smtp_host,
					'smtp_port' => $this->data['setting']->smtp_port,
					'smtp_user' => $this->data['setting']->smtp_user,
					'smtp_pass' => $this->data['setting']->smtp_pass,
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'starttls' => true,
					'wordwrap' => TRUE,
				);
			} else {
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['mailtype'] = 'html';
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;
			}
			$this->email->initialize($config);

			$this->data['name'] = $q->row()->firstname . ' ' . $q->row()->lastname;
			$this->data['language'] = "dutch";
			$this->data['info'] = (object) array('email' => $q->row()->email, 'password' => $password, 'name' => $q->row()->firstname . ' ' . $q->row()->lastname);
			//$body = $this->load->view('email/resetpassword.php', $this->data, true);
			$this->data['main_content'] = 'email/' . $this->data['language'] . '/resetpassword.php';
			$body = $this->load->view('email/content', $this->data, true);
			$this->email->set_newline("\r\n");
			$this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
			$this->email->to(trim($this->dpost['email']));
			$this->email->subject(lang("Reset Password successfully"));
			$this->email->message($body);
			if ($this->email->send()) {
				echo json_encode(array('result' => true, 'res' => $cliens));
			} else {
				echo json_encode(array('result' => false));
			}

		} else {
			echo json_encode(array('result' => false));
		}

	}
	public function session_show() {
		echo json_encode($_SESSION);
	}

	public function getinvoices() {
		$this->load->model('Auth_model');
		$fieldid = getCustomfieldsidbyName('iAddressNbr');
		$iAddressNbr = $this->Auth_model->getiAddressNbr($fieldid, $this->dpost["clientid"]);
		if ($iAddressNbr) {
			$this->db = $this->load->database('magebo', true);
			$q = $this->db->query("SELECT a.iInvoiceNbr,a.iAddressNbr,a.dInvoiceDate,a.dInvoiceDueDate, a.iInvoiceStatus,b.cName,a.mInvoiceAmount
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE a.iAddressNbr=?
    ORDER BY a.iInvoiceNbr DESC", array($iAddressNbr));

			if ($q->num_rows() > 0) {
				foreach ($q->result_array() as $inv) {

					if ($inv['iInvoiceStatus'] == 53) {
						$inv['status'] = "Unpaid";
					} elseif ($inv['iInvoiceStatus'] == 54) {
						$inv['status'] = "Paid";
					} else {
						$inv['status'] = "Other";
					}

					$r[] = $inv;
				}
				$ss = $r;
				echo json_encode($ss);
			} else {
				echo json_encode(array());
			}
		} else {
			echo json_encode(array());
		}
	}

	public function getinvoices_unpaid($id) {
		$total = 0;
		$this->load->model('Auth_model');
		$fieldid = getCustomfieldsidbyName('iAddressNbr');
		$iAddressNbr = $this->Auth_model->getiAddressNbr($fieldid, $id);
		if ($iAddressNbr) {
			$this->db = $this->load->database('magebo', true);
			$q = $this->db->query("SELECT a.iInvoiceNbr,a.iAddressNbr,a.dInvoiceDate,a.dInvoiceDueDate, a.iInvoiceStatus,b.cName,a.mInvoiceAmount
    FROM tblInvoice a
    left join tblAddress b on b.iAddressNbr=a.iAddressNbr
    WHERE a.iAddressNbr=?
    ORDER BY a.iInvoiceNbr DESC", array($iAddressNbr));

			if ($q->num_rows() > 0) {
				foreach ($q->result_array() as $inv) {

					if ($inv['iInvoiceStatus'] != 54) {
						$total = $total + $inv['mInvoiceAmount'];
					}

				}

				return $total;
			} else {
				return '0.00';
			}
		} else {
			echo json_encode(array());
		}
	}

	public function getmobile() { 
		$this->load->model('Admin_model');

		$result = $this->Api_model->getClientServices_mobile($this->dpost['clientid']);
		echo json_encode($result);

	}

	public function getmobiledetails() {
		$this->load->model('Admin_model');
		$id = $this->dpost['serviceid'];
		$this->data['service'] = $this->Admin_model->getService($id);
		if ($this->data['service']->billingcycle == "Free Account") {
			$this->Admin_model->ChangeStatusService($id, 'Active');
		}
		$this->data['mobile_status'] = 0;
		$this->data['client'] = $this->Admin_model->getClient($this->data['service']->userid);
		$this->data['title'] = "Service Information";
		if (in_array($this->data['service']->servertype, array("artilium", "artiliumlegacy"))) {

			//print_r($this->data['service']);
			if ($this->data["service"]->porting->porting) {
				$this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . trim($this->data["service"]->porting->number));
				$msisdn = $this->data["service"]->porting->number;
				if (empty($this->data['mobile'])) {
					$this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . trim($this->data['service']->domain));
					$msisdn = $this->data['service']->domain;
				}
				//$this->data['sn'] = $this->Admin_model->artiliumGet('GetSn/' . trim($this->data['service']->domain), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);

			} else {

				$this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . trim($this->data['service']->domain));
				//$this->data['sn'] = $this->Admin_model->artiliumGet('GetSn/' . trim($this->data['service']->username), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);
				$msisdn = $this->data['service']->domain;

			}

			//$this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . trim($this->data['service']->domain));

			if (empty($this->data['mobile'])) {

				$this->session->set_flashdata('error', lang('No data recieved from Unity Server for GetSim'));
				redirect('admin/errors/exceptions/' . $id);
				exit;
			} else {
				if (!empty($this->data['mobile']->name)) {
					if ($this->data['mobile']->name == "TimeoutError") {
						$this->session->set_flashdata('error', lang('No data recieved from Unity Server for GetSim'));
						redirect('admin/errors/exceptions/' . $id);
					}
				}

			}

			$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));
			$this->data['sn'] = $this->Admin_model->artiliumGet('GetSn/' . trim($this->data['service']->domain), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);

			$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));

			$ss = $this->artilium->GetSn($this->data['mobile']->SIMCardNbr);

			$this->data['sn'] = (object) $ss->data;

			//$this->data['sn'] = $this->artilium->GetSn($this->data['mobile']->SIMCardNbr);

			$this->data['bar'] = $this->Admin_model->artiliumGet('GetParametersCLI/' . trim($this->data['sn']->SN), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);
			$this->data['cliinfo'] = $this->Admin_model->artiliumGet('GetSpecificCliInfo/' . trim($this->data['sn']->SN), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);
			$this->data['sim'] = $this->Admin_model->artiliumGet('GetSim/' . trim($this->data['mobile']->SIMCardNbr), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);

			//$array = array('action' => 'GetListPackageOptionsForSn', 'type' => $this->data['mobile']->PaymentType, 'SN' => trim($this->data['sn']->SN));
			$packages = $this->Admin_model->artiliumGet('GetListPackageOptionsForSn/' . trim($this->data['sn']->SN), $this->data['mobile']->PaymentType, $this->data['setting']->companyid);
			//$packages = $this->artilium->GetListPackageOptionsForSn($this->data['sn']->SN);
			//print_r($packages);
			//print_r($this->data['bar']);
			//$this->data['bundles'] = array();
			$this->data['bundles'] = $this->Admin_model->artiliumPost(array('companyid' => $this->data['setting']->companyid, 'action' => 'GetBundleAssignList', 'type' => $this->data['mobile']->PaymentType, 'SN' => trim($this->data['sn']->SN)));

			//print_r($this->data['bundles']);

			$bundle_list = $this->Admin_model->artiliumPost(array('companyid' => $this->data['setting']->companyid, 'action' => 'GetBundleUsageList', 'type' => $this->data['mobile']->PaymentType, 'SN' => trim($this->data['sn']->SN), 'PageIndex' => '0', 'PageSize' => 100, 'SortBy' => '0', 'OrderBy' => '0'));

			//print_r('Mochael' . print_r($bundle_list));

			foreach ($this->data['bar']->Parameters->Parameter as $s) {

				if ($s->ParameterId == "20541") {

					$this->data['mobile_status'] = $s->ParameterValue;
				}

			}

			$bun = array();
			if (empty($bundle_list->BundleUsage->BundleUsageId)) {

				foreach (objectToArray($bundle_list->BundleUsage) as $bundle) {

					if ($bundle['UsedValue'] == 0 && $bundle['AssignedValue'] == 0) {

						$percentage = "0";
					} else {
						$p = $bundle['UsedValue'] / $bundle['AssignedValue'];
						$percentage = $p * 100;
					}
					if (array_key_exists('BundleDefinitionId', $bundle)) {
						$name = $this->Admin_model->artiliumGet('GetBundleDefinition/' . $bundle['BundleDefinitionId'], $this->data['mobile']->PaymentType, $this->data['setting']->companyid);
					} else {
						$name = (object) array('Description' => 'Bundle Combi', 'RatingUnit' => 4);
					}
					$bun[] = array('name' => $name,
						'ValidFrom' => $bundle['ValidFrom'],
						'ValidUntil' => $bundle['ValidUntil'],
						'AssignedValue' => $bundle['AssignedValue'],
						'UsedValue' => $bundle['UsedValue'],
						'Percentage' => $percentage);
					unset($percentage);
				}

			} else {
				$bundle = objectToArray($bundle_list->BundleUsage);
				if ($bundle['UsedValue'] == 0 && $bundle['AssignedValue'] == 0) {

					$percentage = "0";
				} else {
					$p = $bundle['UsedValue'] / $bundle['AssignedValue'];
					$percentage = $p * 100;
				}
				if (array_key_exists('BundleDefinitionId', $bundle)) {
					$name = $this->Admin_model->artiliumGet('GetBundleDefinition/' . $bundle['BundleDefinitionId'], $this->data['mobile']->PaymentType, $this->data['setting']->companyid);
				} else {
					$name = (object) array('Description' => 'Bundle Combi', 'RatingUnit' => 4);
				}

				$bun[] = array('name' => $name,
					'ValidFrom' => $bundle['ValidFrom'],
					'ValidUntil' => $bundle['ValidUntil'],
					'AssignedValue' => $bundle['AssignedValue'],
					'UsedValue' => $bundle['UsedValue'],
					'Percentage' => $percentage);

			}
			$this->data['bundle_list'] = $bun;
			foreach ($packages->PackageOptions as $row) {

				if (!array_key_exists('TrafficId', $row)) {

					if (!in_array(trim($row->CallModeDescription), $excludes)) {
						$res[] = $row;
					}

				}

			}

			$this->data['packages'] = $res;
		}

		echo json_encode(array('sn' => $this->data['sn'], 'client' => $this->data['client'], 'service' => $this->data['service'], 'mobile' => $this->data['mobile'], 'packages' => $this->data['packages'], 'bundle' => $this->data['bundle_list']));

	}

	function get_cdr() {

		$s = $this->dpost;

		$bytes = array();
		$this->load->model('Admin_model');
		//echo $msisdn;

		//	$this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . trim($msisdn));

		//print_r($this->data['mobile']);
		//$this->load->library('artilium', array($this->data['setting']->companyid, $this->data['mobile']->PaymentType));

		//$result = $this->artilium->GetCDRList($sn);

		$sms = array();
		$voice = array();
		$bytes = array();
		/*$req = array('action' => 'GetCDRList',
				'type' => $this->data['mobile']->PaymentType,
				'From' => date('Y-m-01\TH:i:s'),
				'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
				'SN' => $sn,
				'PageIndex' => 0,
				'PageSize' => 2500,
				'SortBy' => 0,
				'SortOrder' => 0,
				'companyid' => $this->data['setting']->companyid);
			$result = $this->Admin_model->artiliumPost($req);
		*/
		$params = array('companyid' => '53',
			'action' => 'GetCDRList',
			'type' => 'POST PAID',
			'SN' => trim($s['sn']),
			'From' => date('Y-m-01\T00:00:00+02:00'),
			'Till' => date('Y-m-d\TH:i:s'),
			'PageIndex' => 0,
			'PageSize' => 2500,
			'SortBy' => 0,
			'SortOrder' => 0);

		$result = $this->Admin_model->artiliumPost($params);

		foreach ($result->ListInfo as $row) {
			if ($row->Cause != "1000") {
				if (empty($row->DestinationCountry)) {
					$country = "";
				} else {

					$country = $row->DestinationCountry;
				}

				if ($row->TypeCallId != "5") {
					if ($row->MaskDestination != "72436") {

						if ($row->TrafficTypeId == "2") {
							$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $country, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => round($row->DurationConnection / 1048576, 2) . 'MB', 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed));
						} else {
							if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {

							} else {

								if ($row->TypeCallId != "3") {
									$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $row->DestinationCountry, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
								}
							}
							/*
								$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'DestinationCountry' => $row->DestinationCountry, 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'TrafficTypeId' => $row->TrafficTypeId, 'CurNumUnitsUsed' => '€ ' . includevat('21', $row->CurNumUnitsUsed));
							*/
						}

					}

				}

				if ($row->TypeCallId != 5) {
					if (!in_array($row->TrafficTypeId, array(2, 3))) {
						if (strlen($row->MaskDestination) > 9) {

							if ($row->TrafficTypeId) {

								if ($row->TrafficTypeId == "1" && $row->DurationConnection < 1) {

								} elseif ($row->TrafficTypeId == "5" && $row->DurationConnection < 1) {

								} else {
									if ($row->TrafficTypeId == "1" && $row->DurationConnection > 1) {
										$voice[] = $row->DurationConnection;
									} elseif ($row->TrafficTypeId == "5" && $row->DurationConnection >= 1) {
										$sms[] = $row->DurationConnection;
									}

								}

							}

						}

					} elseif ($row->TrafficTypeId == 2) {

						$bytes[] = $row->DurationConnection;

					}
				}

			}
		}
		//print_r($cdr);
		//exit;
		if (!$sms) {
			$smsi = '0';
		} else {
			$smsi = array_sum($sms);
		}
		echo json_encode(array('session' => $_SESSION, 'cdr' => $cdr, 'data' => array_sum($bytes), 'voice' => array_sum($voice), 'sms' => $smsi));
		//return array('cdr' => $cdr, 'data' => array_sum($bytes));

	}

	function getUsage() {
		$this->load->library('artilium', array('companyid' => $this->companyid));
        $total = '0.00';
        $this->load->library('xml');
        $usage = array('0.00');
        $this->data['activemobiles'] = getMobileActive($this->companyid);
        //print_r($this->data);
        if (empty($this->data['activemobiles'])) {

            $this->data['showusage'] = false;
        } else {

            $f = array();
            $cdr = array();

            foreach ($this->data['activemobiles'] as $IN => $order) {

                if ($order['domainstatus'] == 'Active') {
                    $this->data["service" . $IN] = $this->Admin_model->getService($order['id']);

                    $mobile = $this->data["service" . $IN]->details;

                    $f[] = array('domain' => $mobile->msisdn, 'sn' => $mobile->msisdn_sn, 'mobile' => $mobile);
                    $cdr = $this->get_cdr(trim($mobile->msisdn_sn), $mobile->msisdn, $mobile);

                    $total = $cdr['total'] + $total;
                    unset($cdr);

                }

            }
            $this->data['consumption'] = $total;

            $this->data['showusage'] = true;
            $this->data['bundles'] = $this->artilium->GetBundleAssignList1($f['0']['sn']);
            if ($this->session->master) {
                //print_r($_SESSION);
                //print_r($this->data['activemobiles']);
                //exit;
                // print_r($this->data['bundles']);
                // exit;
            }
            // $this->data['iusage'] = $this->Admin_model->artiliumPost(array('SN' => trim($f[0]['sn']), 'companyid' => $this->data['setting']->companyid, 'action' => 'GetBundleAssignUsage', 'PageIndex' => 0, 'PageSize' => 1000, 'SortBy' => 0, 'SortOrder' => 0, 'type' => 'POST PAID'));
        }
			echo json_encode(array('usage' => $total));
		}
	
	public function getStats() {
		$mageboid = "2343546546";
		$this->load->model('Client_model');
		$cliens = $this->Client_model->whmcs_api(array('action' => 'GetClientsDetails', 'clientid' => $this->dpost['id']));
		foreach ($cliens->customfields as $row) {
			if ($row->id == "900") {
				$mageboid = $row->value;
			}
		}
		$stats = array('service' => $this->Client_model->getServices($this->dpost['id']), 'invoice_amountpaid' => $this->getinvoices_unpaid($this->dpost['id']));

		echo json_encode($stats);
	}
	public function check_password() {
		$this->db = $this->load->database('default', true);
		$this->db->where('id', $this->dpost['id']);
		$q = $this->db->get('tblclients');
		if ($q->num_rows() > 0) {
			if (password_verify($this->dpost['password'], $q->row()->password)) {
				echo json_encode(array('result' => true));
			} else {
				echo json_encode(array('result' => false));
			}
		} else {
			echo json_encode(array('result' => false));
		}

	}

	public function changePassword() {
		$this->load->model('Client_model');
		$cliens = $this->Client_model->whmcs_api(array('action' => 'UpdateClient', 'clientid' => $this->dpost['id'], 'password2' => $this->dpost['password']));
		if ($cliens->result == "success") {
			echo json_encode(array('result' => true));

		} else {
			echo json_encode(array('result' => false));
		}
	}

	function export_livecdr($sn, $msisdn) {
		$total = 0;
		set_time_limit(0);

		$bytes = array();
		$this->load->model('Admin_model');

		$cdr = array();
		$this->data['mobile'] = $this->Admin_model->mageboGet('GetSim/' . $msisdn);

		$sms = array();
		$voice = array();
		$bytes = array();
		$req = array('action' => 'GetCDRList',
			'type' => $this->data['mobile']->PaymentType,
			'From' => date('Y-m-01\TH:i:s+02:00'),
			'Till' => date('Y-m-d\TH:i:s+02:00', strtotime(date('Y-m-d') . ' + 1 days')),
			'SN' => $sn,
			'PageIndex' => 0,
			'PageSize' => 5000,
			'SortBy' => 0,
			'SortOrder' => 0,
			'companyid' => $this->data['setting']->companyid);
		$result = $this->Admin_model->artiliumPost($req);
		$header = array(
			lang('Begintime') => 'string',
			lang('Type') => 'string',
			lang('Destination') => 'string',
			lang('Duration') => 'string',

			lang('Cost') => 'string',

		);
		foreach ($result->ListInfo as $row) {
			if ($row->Cause != "1000") {
				if ($row->TypeCallId != 5) {
					if ($row->MaskDestination != "72436") {

						if (!empty($row->DestinationCountry)) {
							$country = $row->DestinationCountry;
						} else {

							$country = "";
						}

						if ($row->TrafficTypeId == 2) {
							$total = $total + includevat('21', $row->CurNumUnitsUsed);
							$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => str_replace('.', ',', round($row->DurationConnection / 1048576, 2)) . 'MB', 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
						} else {
							if ($row->TrafficTypeId == "1" && $row->TypeCallId == "2") {

							} else {
								if ($row->TypeCallId != "3") {
									$total = $total + includevat('21', $row->CurNumUnitsUsed);
									$cdr[] = array('Begintime' => format_cdr_time($row->Begintime), 'TrafficTypeId' => getTraficName($row->TrafficTypeId), 'MaskDestination' => $row->MaskDestination, 'DurationConnection' => gmdate("H:i:s", $row->DurationConnection), 'CurNumUnitsUsed' => '€ ' . str_replace('.', ',', includevat('21', $row->CurNumUnitsUsed)));
								}
							}
						}

					}

				}

			}
		}

		return $total;

	}
}