<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class HomeController extends CI_Controller {

	protected $data = [];

	public function __construct() {
		// Ensure you run parent constructor
		parent::__construct();
		$this->config->set_item('language', $this->session->userdata('language'));
		$this->lang->load('admin');
		$this->setProperty();
	}

	public function setProperty() {
		$this->data['setting'] = globo();
	}

}
class Home extends HomeController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 *     http://example.com/index.php/welcome
	 *  - or -
	 *     http://example.com/index.php/welcome/index
	 *  - or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();

	}

	public function index() {

		redirect('client');
	}
}