<?php
defined('BASEPATH') or exit('No direct script access allowed');
class PayController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->lang->load('admin');
        $this->companyid       = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
        $this->payu_live  = false;
        if ($this->data['setting']->payulantam_env == "live") {
            $this->payu_live  = true;
        }

        $this->load->model('Agent_model');
    }
}
class Pay extends PayController
{
    public function reload()
    {
        log_message('error', print_r($_POST['info'], 1));

        foreach ($_POST['info'] as $row) {
            $_POST['info'][$row['name']] = $row['value'];
        }

        log_message('error', print_r($_POST['info'], 1));
        if ($_POST['type'] == "banktransfer") {
            $this->payu_banktransfer($_POST['info']);
        } elseif ($_POST['type'] == "cash") {
            $this->payu_cash($_POST['info']);
        } elseif ($_POST['type'] == "creditcard") {
            $this->payu_creditcard($_POST['info']);
        }
    }
    public function payu_banktransfer($data)
    {
        try {


            require_once APPPATH . 'third_party/PayU.php';
            log_message('error', print_r($this->data['setting']));
            if ($this->payu_live) {
            PayU::$apiKey     = $this->data['setting']->payulantam_key; //Enter your own apiKey here.
            PayU::$apiLogin   = $this->data['setting']->payulantam_login; //Enter your own apiLogin here.
            PayU::$merchantId = $this->data['setting']->payulantam_merchantid; //Enter your commerce Id here.
            Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi");
                Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi");
                Environment::setSubscriptionsCustomUrl("https://api.payulatam.com/payments-api/rest/v4.9/");
            } else {
                PayU::$apiKey     = "4Vj8eK4rloUd272L48hsrarnUA"; //Enter your own apiKey here.
            PayU::$apiLogin   = "pRRXKOl8ikMmt9u"; //Enter your own apiLogin here.
            PayU::$merchantId = "508029"; //Enter your commerce Id here.
            Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
                Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi");
                Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.9/");
            }
            PayU::$language   = SupportedLanguages::EN; //Select the language.
        PayU::$isTest     = $this->payu_live; //Leave it True when testing. $reference  = $_SESSION['reseller']['id'] . "_" . time();

        $reference  = $_SESSION['reseller']['id'] . "_" . time();
            $value      = $data['amount'];
            $parameters = array(
            // Enter the account’s identifier here.
            PayUParameters::ACCOUNT_ID =>  $this->data['setting']->payulantam_accountid,
            // Enter the reference code here.
            PayUParameters::REFERENCE_CODE => $reference,
            // Enter the description here.
            PayUParameters::DESCRIPTION => "RESELLER RELOAD " . $value,
            // -- Values --
            // Enter the value here.
            PayUParameters::VALUE => $value,
            // Enter the value of the VAT (Value Added Tax only valid for Colombia) of the transaction,
            // if no VAT is sent, the system will apply 19% automatically. It can contain two decimal digits.
            // Example 19000.00. In case you have no VAT you should fill out 0.
            PayUParameters::TAX_VALUE => "0",
            // Enter the value of the base value on which VAT (only valid for Colombia) is calculated.
            // If you do not have VAT should be sent to 0.
            PayUParameters::TAX_RETURN_BASE => "0",
            // Enter the currency here.
            PayUParameters::CURRENCY => "COP",
            //Enter the buyer's email here.
            PayUParameters::BUYER_EMAIL => $_SESSION['reseller']['email'],
            //Enter the payer's name here.
            PayUParameters::PAYER_NAME => $_SESSION['reseller']['contact_name'],
            //Enter the payer's email here.
            PayUParameters::PAYER_EMAIL =>$_SESSION['reseller']['email'],
            //Enter the payer's contact phone here.
            PayUParameters::PAYER_CONTACT_PHONE => $_SESSION['reseller']['phonenumber'],
            //-- Mandatory information for PSE –
            // Enter the bank PSE code here.
            PayUParameters::PSE_FINANCIAL_INSTITUTION_CODE => $data['bank_code'],
            // Enter the person type here (Natural or legal).
            PayUParameters::PAYER_PERSON_TYPE => $data['bank_person'],
            //Enter the payer's contact document here.
            PayUParameters::PAYER_DNI => $data['bank_identification_type'],
            // Enter the payer’s document type here: CC, CE, NIT, TI, PP,IDC, CEL, RC, DE.
            PayUParameters::PAYER_DOCUMENT_TYPE => $data['bank_identification_number'],
            // Enter the payment method name here.
            PayUParameters::PAYMENT_METHOD => "PSE",
            // Enter the name of the country here.
            PayUParameters::COUNTRY => PayUCountries::CO,
            // Payer IP
            PayUParameters::IP_ADDRESS =>  $_SERVER['REMOTE_ADDR'],
                // Cookie of the current session.
            PayUParameters::PAYER_COOKIE => md5($_SESSION['reseller']['id']).'_'.$_SESSION['reseller']['id'],

            // User agent of the current session.
            PayUParameters::USER_AGENT => "Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0",
            //Response page to which the payer will be redirected.
            PayUParameters::RESPONSE_URL => base_url() . 'reseller'
        );
            log_message('error', print_r($parameters, 1));
            $response   = PayUPayments::doAuthorizationAndCapture($parameters);
            log_message('error', print_r($response, 1));
            // {"code":"SUCCESS","transactionResponse":{"orderId":846694524,"transactionId":"01e35159-d724-4dbd-9ca4-55832772f72c","state":"DECLINED","responseCode":"DECLINED_TEST_MODE_NOT_ALLOWED","errorCode":"ENTITY_NO_RESPONSE","additionalInfo":{"paymentNetwork":"GTECH","rejectionType":"HARD_DECLINE","transactionType":"AUTHORIZATION_AND_CAPTURE"}}}
            $this->Agent_model->insert_paymentlog(array(
            "companyid" => $this->session->cid,
            "agentid" => $_SESSION['reseller']['id'],
            "rawdata" => json_encode($response),
            "gateway" => "payu",
            "payment_method" => "banktransfer"
        ));
            if ($response) {
                $response->transactionResponse->orderId;
                $response->transactionResponse->transactionId;
                $response->transactionResponse->state;
                if ($response->transactionResponse->state) {
                    if ($response->transactionResponse->state == "PENDING") {
                        $response->transactionResponse->pendingReason;
                        $response->transactionResponse->extraParameters->BANK_URL;
                        $redirect= true;
                    // header('Location: '. $response->transactionResponse->extraParameters->BANK_URL);
                    } else {
                        $redirect = false;
                        $this->session->set_flashdata('error', $response->transactionResponse->responseCode);
                        //redirect('reseller');
                    }
                }
                $response->transactionResponse->responseCode;
            }
            if ($redirect) {
                echo json_encode(array('redirect' => $redirect, 'data' => $response));
            } else {
                echo json_encode(array('redirect' => $redirect, 'message' =>  $response->transactionResponse->responseCode, 'data' => $response));
            }
        } catch (Exception $e) {
            echo json_encode(array('redirect' => $redirect, 'message' =>  $e->getMessage(), 'data' => $response));
        }
    }
    public function payu_creditcard($data)
    {
        try {
            $s      = $_SESSION['reseller'];
            $client = (object) $s;
            require_once APPPATH . 'third_party/PayU.php';
            if ($this->payu_live) {
                PayU::$apiKey     = $this->data['setting']->payulantam_key; //Enter your own apiKey here.
                PayU::$apiLogin   = $this->data['setting']->payulantam_login; //Enter your own apiLogin here.
                PayU::$merchantId = $this->data['setting']->payulantam_merchantid; //Enter your commerce Id here.
                Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi");
                Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi");
                Environment::setSubscriptionsCustomUrl("https://api.payulatam.com/payments-api/rest/v4.9/");
            } else {
                PayU::$apiKey     = "4Vj8eK4rloUd272L48hsrarnUA"; //Enter your own apiKey here.
                PayU::$apiLogin   = "pRRXKOl8ikMmt9u"; //Enter your own apiLogin here.
                PayU::$merchantId = "508029"; //Enter your commerce Id here.
                Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
                Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi");
                Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.9/");
            }

            PayU::$language   = SupportedLanguages::EN; //Select the language.
            PayU::$isTest     = $this->payu_live; //Leave it True when testing. $reference  = $_SESSION['reseller']['id'] . "_" . time();
            $value      = $data['amount'];
            $parameters = array(
                // Enter the account’s identifier here.
                PayUParameters::ACCOUNT_ID =>  $this->data['setting']->payulantam_accountid,
                // Enter the reference code here.
                PayUParameters::REFERENCE_CODE => $reference,
                // Enter the description here.
                PayUParameters::DESCRIPTION => "RESELLER RELOAD " . $value,
                // -- Values --
                // Enter the value here.
                PayUParameters::VALUE => $value,
                // Enter the value of the VAT (Value Added Tax only valid for Colombia) of the transaction,
                // if no VAT is sent, the system will apply 19% automatically. It can contain two decimal digits.
                // Example 19000.00. In case you have no VAT you should fill out 0.
                PayUParameters::TAX_VALUE => "0",
                // Enter the value of the base value on which VAT (only valid for Colombia) is calculated.
                // If you do not have VAT should be sent to 0.
                PayUParameters::TAX_RETURN_BASE => "0",
                // Enter the currency here.
                PayUParameters::CURRENCY => "COP",
                // -- Buyer --
                //Enter the buyer Id here.
                PayUParameters::BUYER_NAME =>$_SESSION['reseller']['agent'],
                //Enter the buyer's email here.
                PayUParameters::BUYER_EMAIL => $_SESSION['reseller']['email'],
                //Enter the buyer's contact phone here.
                PayUParameters::BUYER_CONTACT_PHONE =>  $_SESSION['reseller']['phonenumber'],
                //Enter the buyer's contact document here.



                //Enter the buyer's address here.
                PayUParameters::BUYER_STREET => $_SESSION['reseller']['address1'],
                PayUParameters::BUYER_STREET_2 => "",
                PayUParameters::BUYER_CITY => $_SESSION['reseller']['city'],
                PayUParameters::BUYER_STATE => $_SESSION['reseller']['state'],
                PayUParameters::BUYER_COUNTRY => $_SESSION['reseller']['country'],
                PayUParameters::BUYER_POSTAL_CODE => $_SESSION['reseller']['postcode'],
                PayUParameters::BUYER_PHONE => $_SESSION['reseller']['phonenumber'],
                // -- Payer --
                //Enter the payer's name here.
                PayUParameters::PAYER_NAME => $_SESSION['reseller']['contact_name'],
                //Enter the payer's email here.
                PayUParameters::PAYER_EMAIL => $_SESSION['reseller']['email'],
                //Enter the payer's contact phone here.
                PayUParameters::PAYER_CONTACT_PHONE => $_SESSION['reseller']['phonenumber'],
                //Enter the payer's contact document here.

                //Enter the payer's address here.
                PayUParameters::PAYER_STREET => $_SESSION['reseller']['address1'],
                PayUParameters::PAYER_STREET_2 => "",
                PayUParameters::PAYER_CITY => $_SESSION['reseller']['city'],
                PayUParameters::PAYER_STATE => $_SESSION['reseller']['state'],
                PayUParameters::PAYER_COUNTRY => $_SESSION['reseller']['country'],
                PayUParameters::PAYER_POSTAL_CODE => $_SESSION['reseller']['postcode'],
                PayUParameters::PAYER_PHONE => $_SESSION['reseller']['phonenumber'],
                // -- Credit card data --
                // Enter the number of the credit card here
                PayUParameters::CREDIT_CARD_NUMBER => $data['cc_number'],
                // Enter expiration date of the credit card here
                PayUParameters::CREDIT_CARD_EXPIRATION_DATE =>  $data['cc_expiry'],
                //Enter the security code of the credit card here
                PayUParameters::CREDIT_CARD_SECURITY_CODE => $data['cc_ccv'],
                //Enter the name of the credit card here
                //VISA||MASTERCARD||AMEX||DINERS
                PayUParameters::PAYMENT_METHOD =>  $data['cc_type'],
                // Enter the number of installments here.
                PayUParameters::INSTALLMENTS_NUMBER => "1",
                // Enter the name of the country here.
                PayUParameters::COUNTRY => PayUCountries::CO,
                //Session id del device.
                PayUParameters::DEVICE_SESSION_ID => md5($_SESSION['reseller']['id']),
                // Payer IP
                PayUParameters::IP_ADDRESS =>  $_SERVER['REMOTE_ADDR'],
                // Cookie of the current session.
                PayUParameters::PAYER_COOKIE => md5($_SESSION['reseller']['id']).'_'.$_SESSION['reseller']['id'],
                // User agent of the current session.
                PayUParameters::USER_AGENT => "Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
            );

            if (!empty($data['cc_dni'])) {
                $parameters[PayUParameters::BUYER_DNI] = $data['cc_dni'];
            } else {
                $parameters[PayUParameters::BUYER_DNI] =   $_SESSION['reseller']['agent_dni'];
            }
            if (!empty($data['cc_dni'])) {
                $parameters[PayUParameters::PAYER_DNI] =  $data['cc_dni'];
            } else {
                $parameters[PayUParameters::PAYER_DNI] =  $_SESSION['reseller']['agent_dni'];
            }
            log_message('error', print_r($parameters, 1));
            $response   = PayUPayments::doAuthorizationAndCapture($parameters);
            log_message('error', 'Creditcard :' . print_r($response, true));
            $this->Agent_model->insert_paymentlog(array(
                "companyid" => $this->session->cid,
                "agentid" => $_SESSION['reseller']['id'],
                "rawdata" => json_encode($response),
                "gateway" => "payu",
                "payment_method" => "creditcard"
            ));
            if ($response) {
                $response->transactionResponse->orderId;
                $response->transactionResponse->transactionId;
                $response->transactionResponse->state;
                if ($response->transactionResponse->state == "PENDING") {
                    $response->transactionResponse->pendingReason;
                    $redirect  = true;
                } else {
                    $this->session->set_flashdata('error', $response->transactionResponse->responseCode);
                    //redirect('reseller');
                    $redirect  = false;
                }
                $response->transactionResponse->paymentNetworkResponseCode;
                $response->transactionResponse->paymentNetworkResponseErrorMessage;
                $response->transactionResponse->trazabilityCode;
                $response->transactionResponse->responseCode;
                $response->transactionResponse->responseMessage;
            }
        } catch (Exception $e) {
            echo json_encode(array('redirect' => $redirect, 'message' =>  $e->getMessage(), 'data' => $response));
        }
        if ($redirect) {
            echo json_encode(array('redirect' => $redirect, 'data' => $response));
        } else {
            echo json_encode(array('redirect' => $redirect, 'message' => $response->transactionResponse->responseCode, 'data' => $response));
        }
    }
    public function payu_cash($data)
    {
        $this->load->model('Agent_model');
        try {
            $s      = $_SESSION['reseller'];
            $client = (object) $s;
            require_once APPPATH . 'third_party/PayU.php';

            if ($this->payu_live) {
                PayU::$apiKey     = $this->data['setting']->payulantam_key; //Enter your own apiKey here.
                PayU::$apiLogin   = $this->data['setting']->payulantam_login; //Enter your own apiLogin here.
                PayU::$merchantId = $this->data['setting']->payulantam_merchantid; //Enter your commerce Id here.
                Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi");
                Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi");
                Environment::setSubscriptionsCustomUrl("https://api.payulatam.com/payments-api/rest/v4.9/");
            } else {
                PayU::$apiKey     = "4Vj8eK4rloUd272L48hsrarnUA"; //Enter your own apiKey here.
                PayU::$apiLogin   = "pRRXKOl8ikMmt9u"; //Enter your own apiLogin here.
                PayU::$merchantId = "508029"; //Enter your commerce Id here.
                Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
                Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi");
                Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.9/");
            }

            PayU::$language   = SupportedLanguages::EN; //Select the language.
            PayU::$isTest     = $this->payu_live; //Leave it True when testing.

            $reference  = $_SESSION['reseller']['id'] . "_" . time();
            $value      = $data['amount'];
            $stop_date = date('Y-m-d');
            $stop_date = date('Y-m-d', strtotime($stop_date . ' +1 day'));
            $parameters = array(
                // Enter the account’s identifier here.
                PayUParameters::ACCOUNT_ID =>  $this->data['setting']->payulantam_accountid,
                //PayUParameters::ACCOUNT_ID => "512321",
                // Enter the reference code here.
                PayUParameters::REFERENCE_CODE => $reference,
                // Enter the description here.
                PayUParameters::DESCRIPTION => "RESELLER RELOAD " . $value,
                // -- Values --
                // Enter the value here.
                PayUParameters::VALUE => $value,
                // Enter the value of the VAT (Value Added Tax only valid for Colombia) of the transaction,
                // if no VAT is sent, the system will apply 19% automatically. It can contain two decimal digits.
                // Example 19000.00. In case you have no VAT you should fill out 0.
                PayUParameters::TAX_VALUE => "0",
                // Enter the value of the base value on which VAT (only valid for Colombia) is calculated.
                // If you do not have VAT should be sent to 0.
                PayUParameters::TAX_RETURN_BASE => "0",
                // Enter the currency here.
                PayUParameters::CURRENCY => "COP",
                //Enter the buyer's email here.
                PayUParameters::BUYER_EMAIL =>  $_SESSION['reseller']['email'],
                //Enter the payer's name here.
                PayUParameters::PAYER_NAME =>  $_SESSION['reseller']['agent'],
                //Enter the payer's contact document here.
                PayUParameters::PAYER_DNI => $_SESSION['reseller']['agent_dni'],
                //Enter the cash payment method name here.
                PayUParameters::PAYMENT_METHOD => $data['bank_code'], //EFECTY
                // Enter the name of the country here.
                PayUParameters::COUNTRY => PayUCountries::CO,
                //Enter the expiration date here.

                PayUParameters::EXPIRATION_DATE => $stop_date.'T'.date('H:i:s'),
                // Payer IP
                PayUParameters::IP_ADDRESS => $_SERVER['REMOTE_ADDR']
            );
            log_message('error', 'Cash :' . print_r($parameters, true));
            $response   = PayUPayments::doAuthorizationAndCapture($parameters);
            log_message('error', 'Cash :' . print_r($response, true));
            $this->Agent_model->insert_paymentlog(array(
                "companyid" => $this->session->cid,
                "agentid" => $_SESSION['reseller']['id'],
                "rawdata" => json_encode($response),
                "gateway" => "payu",
                "payment_method" => "cash"
            ));
            if ($response) {
                $response->transactionResponse->orderId;
                $response->transactionResponse->transactionId;
                $response->transactionResponse->state;
                if ($response->transactionResponse->state == "PENDING") {
                    $response->transactionResponse->pendingReason;
                    $response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML;
                    $response->transactionResponse->extraParameters->REFERENCE;
                    // header('Location: '. $response->transactionResponse->extraParameters->URL_PAYMENT_RECEIPT_HTML);
                    $redirect = true;
                } else {
                    $redirect = false;
                    //  $this->session->set_flashdata('error', $response->transactionResponse->responseCode);
                }
                $response->transactionResponse->responseCode;
            }
        } catch (Exception $e) {
            echo json_encode(array('redirect' => $redirect, 'message' =>  $e->getMessage(), 'data' => $response));
        }
        if ($redirect) {
            echo json_encode(array('redirect' => $redirect, 'data' => $response));
        } else {
            echo json_encode(array('redirect' => $redirect, 'message' => $response->transactionResponse->responseCode, 'data' => $response));
        }
    }
    public function payu_getcash()
    {
        $html = '
<div class="form-group">
<label class="control-label" for="Bank">' . lang('Cash Method') . '<span class="text-danger">*</span></label>
<select name="bank_code" class="form-control">';
        foreach (array(
            'BALOTO',
            'EFECTY'
        ) as $b) {
            $html .= '<option value="' . $b . '">' . $b . '</option>';
        }
        $html .= '</select></div>';
        echo json_encode(array(
            'result' => true,
            'html' => $html
        ));
    }
    public function payu_getcc()
    {
        /*
        require_once('application/vendor/mcred/detect-credit-card-type/src/Detector.php');
        $detector = new CardDetect\Detector();
        $card = $_POST['cc_number'];
        echo json_encode(array('result' => $detector->detect($card))); //Visa
        */
        require_once('application/vendor/inacho/php-credit-card-validator/src/CreditCard.php');
        $card = CreditCard::validCreditCard($_POST['cc_number'], 'visa');
        echo json_encode(array(
            'result' => $card
        ));
    }
    public function payu_getbanks()
    {
        require_once APPPATH . 'third_party/PayU.php';
        if ($this->payu_live) {
            log_message('error', print_r($this->data['setting'], 1));
            PayU::$apiKey     = $this->data['setting']->payulantam_key; //Enter your own apiKey here.
            PayU::$apiLogin   = $this->data['setting']->payulantam_login; //Enter your own apiLogin here.
            PayU::$merchantId = $this->data['setting']->payulantam_accountid; //Enter your commerce Id here.
            Environment::setPaymentsCustomUrl("https://api.payulatam.com/payments-api/4.0/service.cgi");
            Environment::setReportsCustomUrl("https://api.payulatam.com/reports-api/4.0/service.cgi");
            Environment::setSubscriptionsCustomUrl("https://api.payulatam.com/payments-api/rest/v4.9/");
        } else {
            PayU::$apiKey     = "4Vj8eK4rloUd272L48hsrarnUA"; //Enter your own apiKey here.
            PayU::$apiLogin   = "pRRXKOl8ikMmt9u"; //Enter your own apiLogin here.
            PayU::$merchantId = "508029"; //Enter your commerce Id here.
            Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");
            Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi");
            Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.9/");
        }

        PayU::$language   = SupportedLanguages::EN; //Select the language.
        PayU::$isTest     = $this->payu_live; //Leave it True when testing.

        //Enter the payment method name here
        $parameters = array(
            // Insert  the payment method here.
            PayUParameters::PAYMENT_METHOD => "PSE",
            // Enter the name of the country here.
            PayUParameters::COUNTRY => PayUCountries::CO
        );
        $array      = PayUPayments::getPSEBanks($parameters);
        $banks      = $array->banks;
        $html       = '
<div class="form-group">
<label class="control-label" for="Bank">' . lang('Bank') . '<span class="text-danger">*</span></label>
<select name="bank_code" class="form-control">';
        foreach ($banks as $b) {
            $html .= '<option value="' . $b->pseCode . '">' . $b->description . '</option>';
        }
        $html .= '</select></div>';
        $html .= '
<div class="form-group">
<label class="control-label" for="Bank">' . lang('Client Type') . '<span class="text-danger">*</span></label>
<select name="bank_person" class="form-control">
<option value="N">' . lang('Natural Person') . '</option>
<option value="J">' . lang('Legal Person') . '</option>
</select>
</div>';
        $html .= '
<div class="row">
<div class="form-group col-sm-6">
<label class="control-label" for="Bank">' . lang('Identification Type') . '<span class="text-danger">*</span></label>
<select name="bank_identification_type" class="form-control">
<option value="CC">' . lang('Certificate Citizenship') . '</option>
<option value="CE">' . lang('Immigrant Certificate.') . '</option>
<option value="NIT">' . lang('Company TAX ID Number') . '</option>
<option value="TI">' . lang('Identity Card.') . '</option>
<option value="PP">' . lang('Passport') . '</option>
<option value="IDC">' . lang('Customer Unique ID') . '</option>
<option value="CEL">' . lang('Cell Number') . '</option>
<option value="RC">' . lang('Civil Registration.') . '</option>
<option value="DE">' . lang('Foreign ID') . '</option>
</select>
</div>
<div class="form-group col-sm-6">
<label class="control-label" for="Bank">' . lang('Identification Number') . '<span class="text-danger">*</span></label>
<input name="bank_identification_number" class="form-control" id="bank_identification_number"  value="" required>
</div>
</div>
';
        $html .= '<div class="form-group">
<label class="control-label" for="Bank">' . lang('Phone Number') . '<span class="text-danger">*</span></label>
<input name="bank_phonenumber" class="form-control" id="bank_phonenumber"  value="" required>

</div>
';
        echo json_encode(array(
            'result' => true,
            'html' => $html
        ));
    }
}
