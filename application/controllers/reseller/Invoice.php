<?php
defined('BASEPATH') or exit('No direct script access allowed');
class InvoiceController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
    }
}
class Invoice extends InvoiceController
{
    public function __construct()
    {
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $_SESSION['reseller']['language']);
        } else {
            $this->config->set_item('language', 'english');
        }
        $this->lang->load('admin');
        $this->companyid = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
        $this->load->model('Admin_model');
    }
    public function detail()
    {
            $invoiceid = $this->uri->segment(4);
            $magebid = $this->uri->segment(5);

        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        if (!file_exists($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr')) {
            mkdir($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';

        $id = $this->uri->segment(4);
        if ($this->data['setting']->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }

        if ($this->Admin_model->IsAllowedInvoice($id, $companyid)) {
            $invoice = $this->Admin_model->getInvoicePdf($id);

            //print_r($invoice->PDF);
            // exit;
            $file = $invoice_path . $id . '.pdf';
            file_put_contents($file, $invoice->PDF);
            if ($this->session->cid != "53") {
                $cdr = $this->downloadcdrpdf($id, $this->session->cid);
                if ($cdr) {
                    shell_exec('pdftk ' . $file . ' ' . $cdr . ' cat output ' . $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf');
                    unlink($file);
                    copy($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf', $file);
                    $file = $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf';
                }
            }

            if (file_exists($file)) {
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $id . '.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);
            } else {
                echo "file does not exists";
            }
        } else {
            $this->session->set_flashdata('error', lang('This acction has been denied'));
            redirect('errors/access_denied');
        }
    }

    public function downloadcdrpdf($id, $companyid)
    {
        error_reporting(1);
        $setting = globofix($companyid);
        //$mageboid = $this->uri->segment(5);
        $this->load->library('magebo', array('companyid', $companyid));
        $cdrs = $this->magebo->getInvoiceCdrFile($id);
        $gprs = $this->magebo->getDataCdr($id);
        // print_r($cdrs);
        //$cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);
        // exit;
        if (!$cdrs) {
            if ($gprs) {
                $gp = "";
            } else {
                return false;
            }
        } else {
            //print_r($cdrs);
            // exit;
            $table = '<h2>CDR Voice & SMS: ' . $pincode . '</h2><br />
            <table><thead>
            <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
            <th width="12%">' . lang('Date') . '</th>
            <th width="10%">' . lang('Number') . '</th>
            <th width="15%">' . lang('Destination') . '</th>
            <th width="42%">' . lang('Description') . '</th>
            <th width="10%">' . lang('Duration') . '</th>
            <th width="10%">' . lang('Cost') . ' (incl. VAT)</th>
            </tr>
            </thead>
            <tbody>
            ';
           // mail('mail@simson.one','cdr',print_r($cdrs, true));
            foreach ($cdrs as $array) {
                $table .= '
                <tr>
                <td width="12%">' . substr($array->dCallDate, 0, -3) . '</td>
                <td width="10%">' . $array->iPincode . '</td>
                <td width="15%">' . $array->cDialedNumber . '</td>
                <td width="42%">' . $array->cInvoiceGroupDescription.' - ' .$array->cCallTranslation. '</td>
                <td width="10%">' . gmdate("H:i:s", $array->iInvoiceDuration) . '</td>
                <td width="10%">'.$this->data['setting']->currency . str_replace('.', ',', includevat('21', $array->mInvoiceSale)) . '</td>
                </tr>
                ';
            }

            $table .= '</tbody></table>';
        }

        if ($gprs) {
            foreach ($gprs as $pincode => $cdr) {
                $gp .= '<h2>CDR DATA: ' . $pincode . '</h2><br />
                <table><thead>
                <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
                <th width="15%">' . lang('Date') . '</th>
                <th width="13%">' . lang('Number') . '</th>
                <th width="20%">' . lang('Type') . '</th>
                <th width="35%">' . lang('Zone') . ' </th>
                <th width="10%">' . lang('Bytes') . '</th>


                </tr>
                </thead>
                <tbody>
                ';
                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp .= '
                    <tr>
                    <td width="15%">' . substr($array->SessionDate, 0, -3) . '</td>
                    <td width="13%">' . $array->iPincode . '</td>
                    <td width="20%">' . $array->Type . '</td>
                    <td width="35%">' . $array->Zone . ' '.$array->RoamingCountry . '</td>
                    <td width="10%">' . convertToReadableSize($array->Bytes) . '</td>

                    </tr>
                    ';
                }
                $gp .= '</tbody></table><br /><br /><strong>Total Data usage  ' . $pincode . ' : ' . convertToReadableSize(array_sum($bytes)) . '</strong>';
                unset($bytes);
            }
        }

        $this->load->library('spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Simson Asuni');
        $pdf->SetTitle('CDR');
        $pdf->setInvoicenumber($id);
        $pdf->SetSubject('CDR Invoice ' . $id);
        $pdf->SetKeywords('TCPDF, Invoice, Cdr, ' . $id);
        $pdf->AddPage('L', 'A4');
        //$pdf->SetFont('droidsans', '', '12');
        //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
        $pdf->ln(10);
        //$pdf->SetFont('droidsans', '', '9');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
        $pdf->SetPrintFooter(false);
// set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(true, "10");

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------

// print TEXT
        $pdf->SetFont('helvetica', '', '9');
        //  $pdf->PrintChapter(1, lang('Call Detail Records'), $table, true);
        $pdf->writeHTML($table, true, false, true, false, 'J');
        if ($gprs) {
            $pdf->AddPage('L', 'A4');
            //$pdf->SetFont('droidsans', '', '12');
            //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
            $pdf->ln(10);
            $pdf->SetFont('helvetica', '', '9');
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "10");

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            $pdf->writeHTML($gp, true, false, true, false, 'J');
        }
// print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', FCPATH.'data/chapter_demo_2.txt', true);

// ---------------------------------------------------------

//Close and output PDF document
        if (!file_exists($setting->DOC_PATH . $companyid . '/invoices/cdr')) {
            mkdir($setting->DOC_PATH . $companyid . '/invoices/cdr');
        }
        $pdf->Output($setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf', 'F');
        return $setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf';
    }
    public function download_id($id)
    {
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';

        if ($this->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }
        if ($this->Admin_model->IsAllowedInvoice($id, $companyid)) {
            if (!file_exists($invoice_path . $id . '.pdf')) {
                $invoice = $this->Admin_model->getInvoicePdf($id);
                //$file = '/tmp/' . $id . '.pdf';

                file_put_contents($invoice_path . $id . '.pdf', $invoice->PDF);
                //copy($file, $this->data['setting']->DOC_PATH . '/invoices/' . $id . '.pdf');
                //unlink($file);
            }
        } else {
        }
    }
}
