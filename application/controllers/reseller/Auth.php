<?php
defined('BASEPATH') or exit('No direct script access allowed');
class AuthController extends CI_Controller
{

    protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $this->session->language);
        } else {
            $this->config->set_item('language', 'dutch');
        }
        $this->lang->load('admin');
        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);

    }

}
class Auth extends AuthController
{

    public function __construct()
    {
        parent::__construct();
        if (empty($_SESSION['theme'])) {
            $_SESSION['theme'] = "exocom";
        }
        $this->load->model('Auth_model');

    }

    public function index()
    {
        if (isPost()) {
            $valid = $this->Auth_model->reseller_auth($_POST);
            if ($valid['result'] == "success") {

                $valid['reseller']['islogged'] = true;
                unset($valid['reseller']['password']);
                $this->session->set_userdata($valid);
                $_SESSION['reseller']['chatusername'] = $valid['reseller']['contact_name'];
                $_SESSION['reseller']['username']     = $valid['reseller']['id'];

                redirect('reseller');

            } else {
                $this->session->set_flashdata('error', $valid['message']." ".password_hash($_POST['password'], PASSWORD_DEFAULT));

            }

		  }
		 // print_r($_SESSION);

        $this->load->view(reseller_theme($this->companyid).'auth_login', $this->data);

    }
    public function reset()
    {

        if (isPost()) {

            $reseller = $this->db->query('select * from a_clients_agents where email like ? and companyid =?', array(strtolower($_POST['email']), $this->companyid));

            if ($reseller->num_rows() > 0) {
                $code = $this->Auth_model->addCodeReseller(random_str('alphanum', '10'), $reseller->row()->id);
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                    'protocol'  => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8',
                    'starttls'  => true,
                    'wordwrap'  => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset']  = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->initialize($config);
                $this->data['language'] = "dutch";
                $body = getMailContent('resellerresetpassword', $reseller->row()->language, $reseller->row()->companyid);
                print_r($reseller);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $reseller->row()->contact_name, $body);
                $body = str_replace('{$code}', $code, $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to(trim(strtolower($_POST['email'])));
                $this->email->subject(getSubject('resellerresetpassword', $reseller->row()->language, $reseller->row()->companyid));
                $this->email->message($body);
                if ($this->email->send()) {

                } else {

                }
                //logAdmin(array('companyid' => $this->session->cid, 'userid' => 0, 'user' => $reseller->row()->contact_name, 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Requesting password request'));
                $this->session->set_flashdata('success', lang('a mail with token has been sent to your email to continue'));
                redirect('reseller/auth/entercode');
                exit;
            } else {
                $this->session->set_flashdata('error', lang('We can not find this email in our system'));
            }

        }
        $this->session->sess_destroy();
        $this->load->view(reseller_theme($this->companyid).'auth_reset', $this->data);
    }

    public function entercode()
    {

        if (isPost()) {
            $code             = $this->Auth_model->ResellergetCode($_POST['code']);
            $_SESSION['code'] = $_POST['code'];
            if ($code['result']) {
                $this->session->set_flashdata('Success', lang('Please Change your Password'));
                $this->load->view('themes/' . $this->data['setting']->default_theme . '/reseller/auth_resetwithcode', $this->data);

            } else {

                $this->session->set_flashdata('error', lang('Your token is invalid'));
            }
        } else {
            $this->load->view(reseller_theme($this->companyid).'auth_code', $this->data);
        }

    }

    public function resetwithcode()
    {
        $this->db = $this->load->database('default', true);
        if (isPost()) {
            $reseller            = $this->db->query('select * from a_clients_agents where resetcode like ? ', array($_POST['code']));
            $code             = $_POST['code'];
            $_SESSION['code'] = $code;
            if (trim($_POST['password1']) == trim($_POST['password2'])) {
                $this->Auth_model->ResellerChangePassword($_POST);
                $valid           = $reseller->row_array();
                $valid['logged'] = true;
                $this->session->sess_destroy();
                $this->session->set_userdata($valid);
                $this->session->set_userdata('code');
                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                    'protocol'  => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8',
                    'starttls'  => true,
                    'wordwrap'  => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset']  = 'utf-8';
                    $config['wordwrap'] = true;
                }
                $this->email->initialize($config);


                //$this->data['main_content'] = 'email/' . $this->data['language'] . '/adminpasswordchanged.php';
                //$body = $this->load->view('email/content', $this->data, true);

                $body = getMailContent('adminpasswordchanged', $reseller->row()->language, $reseller->row()->companyid);
                // $body = $this->load->view('email/content', $this->data, true);
                $body = str_replace('{$name}', $reseller->row()->contact_name, $body);
                //$body = str_replace('{$code}', $code, $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);

                unset($valid['code']);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to($valid['email']);
                $this->email->subject(getSubject('adminpasswordchanged', $reseller->row()->language, $reseller->row()->companyid));
                $this->email->message($body);
                if ($this->email->send()) {
                    $this->session->set_flashdata('success', lang('Your password has been changed successfully'));
                    redirect('reseller');
                    exit;
                } else {
                    $this->session->set_flashdata('success', lang('there was error sending your information'));
                    redirect('reseller');
                    exit;
                }

            } else {
                $this->session->set_flashdata('error', lang('your password do not match'));
                redirect('reseller');
                exit;
            }

        } else {
            $code = $this->Auth_model->ResellergetCode(trim($this->uri->segment(4)));
            if ($code['result']) {
                $_SESSION['code'] = trim($this->uri->segment(4));
                $this->load->view(reseller_theme($this->companyid).'auth_resetwithcode', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('Please enter valid token'));
                $this->load->view(reseller_theme($this->companyid).'auth_code', $this->data);
            }
        }

    }

    public function logout()
    {
        logAdmin(array('companyid' => $this->session->cid, 'userid' => 0, 'user' => $_SESSION['reseller']['contact_name'], 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Logged successfully'));
        $companyid = $this->session->cid;
        $this->session->sess_destroy();
        $_SESSION['cid'] = $companyid;
        redirect('reseller/auth');
    }

}