<?php
defined('BASEPATH') or exit('No direct script access allowed');
class DashboardController extends CI_Controller
{
    //protected $data = [];
    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();
    }
}
class Dashboard extends DashboardController
{
    public function __construct()
    {
        parent::__construct();
        if (!empty($this->session->language)) {
            $this->config->set_item('language', $_SESSION['reseller']['language']);
        } else {
            $this->config->set_item('language', 'english');
        }
        $this->lang->load('admin');
        $this->companyid = get_companyidby_url(base_url());
        $this->data['setting'] = globofix($this->companyid);
        $this->load->model('Admin_model');
    }
    public function index()
    {
        //echo $this->companyid;
        //echo reseller_theme($this->companyid);
        //print_r($_SESSION);
        $this->data['title'] = "Reseller Dasboard";
        $this->data['main_content'] = reseller_theme($this->companyid)."dashboard";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }

    public function client()
    {
        if (!reseller_allow_perm('editclient', $this->session->reseller['id'])) {
            $this->session->set_flashdata('error', lang('You do not have access to this feature, please contact your Upstream'));
            redirect('reseller');
        }
        $this->data['title'] = "Reseller Dasboard";
        $this->data['main_content'] = reseller_theme($this->companyid)."client";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }

    public function order()
    {
        if (isPost()) {
            $_SESSION['order'] = $_POST;
            redirect('reseller/dashboard/order_step1');
        }
        $this->data['title'] = "Order Dasboard";
        $this->data['main_content'] = reseller_theme($this->companyid)."order";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }
    public function order_step1()
    {
        if ($_SESSION['order']['clientid']) {
            $this->data['client'] = $this->Admin_model->getClient($_SESSION['order']['clientid']);
        }
        if (isPost()) {
            $_SESSION['order'] = array_merge($_SESSION['order'], $_POST);
            redirect('reseller/dashboard/order_step2');
        }
        if (!empty($_POST['order'])) {
            redirect('reseller/order');
        }

        //print_r($_SESSION['order']);
        $this->data['title'] = "Order Dasboard - STEP 1";
        $this->data['main_content'] = reseller_theme($this->companyid)."order_step1";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }
    public function order_step2()
    {
        if ($_SESSION['order']['clientid']) {
            $this->data['client'] = $this->Admin_model->getClient($_SESSION['order']['clientid']);
        }
        if (isPost()) {
            $_SESSION['order'] = array_merge($_SESSION['order'], $_POST);
            redirect('reseller/dashboard/order_step3');
        }
        if (!empty($_POST['order'])) {
            redirect('reseller/order');
        }

        //print_r($_SESSION['order']);
        $this->data['title'] = "Order Dasboard - STEP 2";
        $this->data['main_content'] = reseller_theme($this->companyid)."order_step2";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }

    public function order_step3()
    {
        if ($_SESSION['order']['clientid']) {
            $this->data['client'] = $this->Admin_model->getClient($_SESSION['order']['clientid']);
        }

        if (!empty($_POST['order'])) {
            redirect('reseller/order');
        }

        //print_r($_SESSION);
        $this->data['title'] = "Order Dasboard - STEP 3";
        $this->data['main_content'] = reseller_theme($this->companyid)."order_step3";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }

    public function finish_order()
    {
        if (isPost()) {
            $this->load->model('Api_model');
            $this->load->model('Admin_model');

            if (!empty($_POST['sso_username'])) {
                $sso_username = $_POST['sso_username'];
                if (!validate_deltausername(strtolower($sso_username))) {
                    echo json_encode(array('result' => false, 'message' => 'Your ' . lang('SSO Username') . ' has been used by other account'));
                    exit;
                }
            }

            if ($_SESSION['order']['customer_type'] == "new") {
                $_POST['mvno_id'] = strtoupper(trim($_POST['mvno_id']));

                if ($_POST['country'] == "NL") {
                    $pcode = explode(' ', trim($_POST["postcode"]));
                    if (count($pcode) != 2) {
                        $this->session->set_userdata('registration', $_POST);
                        // $this->session->set_flashdata('error', lang('Postcode moet: NNNN XX formaat (100)'));
                        echo json_encode(array('result' => false, 'message' => lang('Postcode moet: NNNN XX formaat (100)')));

                        exit;
                    } else {
                        if (strlen($pcode[0]) != 4) {
                            $this->session->set_userdata('registration', $_POST);
                            //$this->session->set_flashdata('error', lang('Postcode moet: NNNN XX formaat (102) '));
                            echo json_encode(array('result' => false, 'message' => lang('Postcode moet: NNNN XX formaat (100)')));

                            exit;
                        } elseif (strlen($pcode[1]) != 2) {
                            $this->session->set_userdata('registration', $_POST);
                            echo json_encode(array('result' => false, 'message' => lang('Postcode moet: NNNN XX formaat (100)')));

                            exit;
                        }
                    }
                } elseif ($_POST['country'] == "BE") {
                    $pcode = explode(' ', 'BE ', trim($_POST["postcode"]));
                } else {
                    $pcode = trim($_POST["postcode"]);
                }

                if (!empty($_POST['iban'])) {
                    $bic = $this->Api_model->getBic(trim($_POST['iban']));
                    if (!$bic->valid) {
                    } else {
                        $_POST['bic'] = $bic->bankData->bic;
                    }
                }

                $_POST['date_created'] = date('Y-m-d');
                unset($_POST['sso_username']);

                $_POST['email']     = strtolower(trim($_POST['email']));
                $_POST['companyid'] = $this->companyid;
                $_POST['agentid'] = $_SESSION['reseller']['id'];
                $_POST['date_modified'] = date('Y-m-d H:i:s');

                if ($this->data['setting']->mage_invoicing == 1) {
                    $this->load->library('magebo', array('companyid' => $this->companyid));
                    $magebo             = $this->magebo->AddClient($_POST);
                    if ($magebo->result == "success") {
                        $this->magebo->addMageboSEPA($_POST['iban'], $_POST['bic'], $magebo->iAddressNbr);
                        $_POST['mageboid'] = $magebo->iAddressNbr;
                    } else {
                        echo json_encode($magebo);
                        exit;
                    }
                }


                if ($this->data['setting']->mvno_id_increment == 1) {
                    $_POST['mvno_id'] = genMvnoId($this->session->cid);
                } else {
                    $_POST['mvno_id'] = strtoupper(trim($_POST['mvno_id']));
                }
                if ($this->data['setting']->mobile_platform != "TEUM") {
                    if ($this->data['setting']->create_arta_contact == "1") {
                        if ($_POST['agentid'] != "-1") {
                            $agentid =  getContactTypeid($_POST['agentid']);
                        } else {
                            $agentid = 7;
                        }

                        $this->load->library('artilium', array('companyid' => $this->companyid));
                        sendlog('CreateContact', array('agentid' => $agentid, 'post' => $_POST));
                        $cont = $this->artilium->CreateContact($_POST, $agentid);
                        if ($cont) {
                            $_POST['ContactType2Id'] = $cont;
                        }
                    } else {
                        $_POST['ContactType2Id'] = 7;
                    }
                } else {
                    $_POST['paymentmethod'] = "banktransfer";
                }


                $cc = $this->Admin_model->insertCustomer($_POST);

                if (!$cc['result']) {
                    $this->session->set_userdata('registration', $_POST);
                    $this->session->set_flashdata('error', lang($cc['message']));
                    echo json_encode(array('result' => false, 'message' => lang($cc['message'])));
                    exit;
                } else {
                    if ($this->data['setting']->mobile_platform == "TEUM") {
                        $this->load->library('pareteum', array('companyid' => $this->companyid));
                        $array = array(
                        "CustomerData" => array(
                            "ExternalCustomerId" => (string)$_POST['mvno_id'],
                            "FirstName" => $_POST['firstname'],
                            "LastName" => $_POST['lastname'],
                            "LastName2" => 'NA',
                            "CustomerDocumentType" => $_POST['id_type'],
                            "DocumentNumber" => $_POST['idcard_number'],
                            "Telephone" => $_POST['phonenumber'],
                            "Email" => $_POST['email'],
                            "LanguageName" => "eng",
                            "FiscalAddress" => array(
                                "Address" => $_POST['address1'],
                                "City" =>$_POST['city'],
                                "CountryId" => "76",
                                "HouseNo" => $_POST['housenumber'],
                                "State" => "Unknown",
                                "ZipCode" => $_POST['postcode'],
                            ),
                            "CustomerAddress" => array(
                                "Address" => $_POST['address1'],
                                "City" => $_POST['city'],
                                "CountryId" => "76",
                                "HouseNo" => $_POST['housenumber'],
                                "State" => "Unknown",
                                "ZipCode" => $_POST['postcode']
                            ),
                            "Nationality" => "GB"
                        ),
                        "comments" => "Customer added via UnitedPortal V1 by" . $this->session->firstname . ' ' . $this->session->lastname
                        );
                        log_message('error', json_encode($array));
                        $teum = $this->pareteum->AddCustomers($array);

                        log_message('error', json_encode($teum));
                        if ($teum->resultCode == "0") {
                            $this->Admin_model->updateClient($cc['id'], array('teum_CustomerId' => $teum->customerId, 'teum_OrderCode' => $teum->orderCode));
                        }
                    }
                    logAdmin(array(
                        'companyid' => $this->companyid,
                        'userid' => $cc['id'],
                        'user' => $this->session->firstname.' '.$this->session->lastname,
                        'ip' => $_SERVER['REMOTE_ADDR'],
                        'description' => 'Adding customer Id: ' . $cc['id'] . ' and MageboId:' .$magebo->iAddressNbr,
                        ));
                }
                if (!empty($sso_username)) {
                    if (validate_deltausername(strtolower($sso_username))) {
                        $this->Admin_model->InsertDeltaUsername($sso_username, $cc['id']);
                    }
                }

                if (!empty($_SESSION['registration'])) {
                    unset($_SESSION['registration']);
                }

                $password = $this->Admin_model->changePasswordClient($cc['id']);

                if ($this->data['setting']->smtp_type == 'smtp') {
                    $config = array(
                    'protocol'  => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype'  => 'html',
                    'charset'   => 'utf-8',
                    'starttls'  => true,
                    'wordwrap'  => true,
                    );
                } else {
                    $config['protocol'] = 'sendmail';
                    $config['mailpath'] = '/usr/sbin/sendmail';
                    $config['mailtype'] = 'html';
                    $config['charset']  = 'utf-8';
                    $config['wordwrap'] = true;
                }

                $this->email->clear(true);
                $this->email->initialize($config);

                $client = $this->Admin_model->getClient($cc['id']);
                $body   = getMailContent('signup_email', $client->language, $client->companyid);

                $body = str_replace('{$name}', $client->firstname . ' ' . $client->lastname, $body);
                $body = str_replace('{$password}', $password, $body);
                $body = str_replace('{$email}', trim(strtolower($_POST['email'])), $body);
                $body = str_replace('{$base_url}', url_to_domain(base_url()), $body);
                $this->email->set_newline("\r\n");
                $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
                $this->email->to(strtolower(trim($_POST['email'])));
                $this->email->bcc('lainard@gmail.com');

                $subject = getSubject('signup_email', $client->language, $client->companyid);
                $this->email->subject($subject);
                $this->email->message($body);
                if ($this->email->send()) {
                    logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => $subject, 'message' => $body));
                }

                logAdmin(array('companyid' => $this->companyid, 'userid' => $client->id, 'user' => $_SESSION['reseller']['contact_name'], 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Adding customer ' . $cc['id']));
            } else {
                $client = $this->Admin_model->getClient($_SESSION['order']['clientid']);
            }//end creating new customer
            $userid = $client->id;
            $needproforma = NeedProforma($client->id);
            log_message('error', "Neew Proforma:". print_r($needproforma, true));

            if ($this->data['setting']->mobile_platform == "ARTA") {
                $order_array = array(
                'companyid' => $this->companyid,
                'type' => 'mobile',
                'status' => 'New',
                'packageid' => $_SESSION['order']['pid'],
                'userid' => $client->id,
                'date_created' => date('Y-m-d'),
                'billingcycle' => 'Monthly',
                'date_contract' => $_SESSION['order']['date_contract'],
                'contract_terms' => $_SESSION['order']['contractduration'],
                'recurring' => getPriceRecurring($_SESSION['order']['pid'], $_SESSION['order']['contractduration']),
                'notes' => 'Reseller Order via portal by: ' . $_SESSION['reseller']['contact_name']);
            } elseif ($this->data['setting']->mobile_platform == "TEUM") {
                if (!empty($_SESSION['order']['ContractDuration'])) {
                    $recurring = getTotalRecurringfromAddons($_SESSION['order']['addon']);
                } else {
                    $recurring = "0.00";
                }
                $order_array = array(
                    'companyid' => $this->companyid,
                    'type' => 'mobile',
                    'status' => 'New',
                    'packageid' => $_SESSION['order']['pid'],
                    'userid' => $client->id,
                    'date_created' => date('Y-m-d'),
                    'billingcycle' => 'Monthly',
                    'date_contract' => date('m-d-Y'),
                    'date_contract_formated' => date('Y-m-d'),
                    'contract_terms' => 1,
                    'recurring' => $recurring,
                    'notes' => 'Reseller Order via portal by: ' . $_SESSION['reseller']['contact_name']);
            } else {
                $order_array = array(
                    'companyid' => $this->companyid,
                    'type' => 'mobile',
                    'status' => 'New',
                    'packageid' => $_SESSION['order']['pid'],
                    'userid' => $client->id,
                    'date_created' => date('Y-m-d'),
                    'billingcycle' => 'Monthly',
                    'date_contract' => $_SESSION['order']['date_contract'],
                    'contract_terms' => $_SESSION['order']['contractduration'],
                    'recurring' => getPriceRecurring($_SESSION['order']['pid'], $_SESSION['order']['contractduration']),
                    'notes' => 'Reseller Order via portal by: ' . $_SESSION['reseller']['contact_name']);
            }

            log_message('error', print_r($order_array, true));
            $assign = get_promotion_auto_assign($_SESSION['order']['pid']);

            if ($assign) {
                $order_array['promocode'] =  $assign;
            } else {
                if (trim($_POST['Promotion']) != "None") {
                    $order_array['promocode'] = trim($_SESSION['order']['promo_code']);
                }
            }



            $serviceid = $this->Api_model->insertOrder($order_array);


            if (!$serviceid) {
                echo json_encode(array('result' => 'error', 'message' => 'Order failed please contact Support +32 484889888'));
                exit;
            } else {
                logAdmin(array(
                                'companyid' =>$this->companyid,
                                'userid' =>$client->id,
                                'user' => 'System',
                                'ip' => $_SERVER['REMOTE_ADDR'],
                                'description' => 'Order Received ID ' . $serviceid,
                            ));
                $result['result'] = true;
                $result['serviceid'] = $serviceid;

                if ($_SESSION['order']['msisdn_type'] == "porting") {
                    $mobiledata = array('msisdn_type' => $_SESSION['order']['msisdn_type'],
                    'donor_msisdn' => $_SESSION['order']['msisdn'],
                    'donor_provider' => $_SESSION['order']['donor_provider'],
                    'donor_sim' => $_SESSION['order']['simnumber'],
                    'donor_type' => $_SESSION['order']['donor_type'],
                    'donor_customertype' => $_SESSION['order']['customertype1'],
                    'msisdn_status' => 'OrderWaiting',
                    'date_wish' => $_SESSION['order']['date_wish'],
                    'serviceid' => $serviceid,
                    'companyid' => $this->companyid,
                    'userid' => $userid,
                    'msisdn' => $_SESSION['order']['msisdn'],
                    'msisdn_pin' => '1111',
                    'ptype' => 'POST PAID');
                    if (!empty($_SESSION['order']['ptype_belgium'])) {
                        $mobiledata['ptype_belgium'] = $_SESSION['order']['ptype_belgium'];
                    } else {
                        $mobiledata['ptype_belgium'] ='NA';
                    }

                    $mobiledata['donor_accountnumber'] = $_SESSION['order']['donor_accountnumber'];
                } else {
                    $mobiledata = array(
                    'msisdn_type' => $_SESSION['order']['msisdn_type'],
                    'msisdn_status' => 'OrderWaiting',
                    'serviceid' => $serviceid,
                    'companyid' => $this->companyid,
                    'userid' => $userid,
                    'msisdn_pin' => '1111',
                    'ptype' => 'POST PAID');
                }


                if (!empty($_SESSION['order']['addon'])) {
                    if ($_SESSION['order']['addon'] != "NONE") {
                        $this->Api_model->addAddon($serviceid, $_SESSION['order']['addon']);
                    }
                }



                $product = getProduct($_SESSION['order']['pid']);

                $mobiledata['msisdn_contactid'] = getContactId($product->gid);
                $mobiledata['initial_reload_amount'] =  getProductReloadAmount($product->gid);
                if (!empty($_POST['sim_delivery'])) {
                    if ($_POST['sim_delivery'] == "United") {
                        $mobiledata['sim_delivery']  = "United";
                        $headers = "From: ".$this->data['setting']->smtp_sender . "\r\n" .
                        "CC: mail@simson.one";
                        mail("techniek@united-telecom.be", "Process Simcard Delivery Orderid: ".$serviceid, "Hello\n\nNew order has been arrived,\n\nOrderid: ".$serviceid." MVNO decide that you need to process the simcard\n\nRegard", $headers);
                    } else {
                        $mobiledata['sim_delivery']  = "MVNO";
                    }
                }

                $service_mobile = $this->Api_model->insertMobileData($mobiledata);
                if ($service_mobile > 0) {
                    logAdmin(array('companyid' => $this->companyid, 'userid' => $userid, 'user' => 'Api User', 'ip' => $_SERVER['REMOTE_ADDR'], 'description' => 'Adding Order with Serviceid ' . $serviceid));
                    $email = getNotificationEmail($this->companyid);

                    if (strpos($email, '|')) {
                        $e = explode('|', $email);
                    } else {
                        $e = false;
                    }
                    $product = getProduct($_POST['addonid']);
                    $headers = "From: noreply@united-telecom.be" . "\r\n" .
                        "BCC: mail@simson.one";
                    if ($e) {
                        if (count($e) > 1) {
                            foreach ($e as $ee) {
                                mail(
                                    $ee,
                                    "New order notification",
                                    "Dear Team,\n\nNew Order has just been arrived, Please process this order as soon as possible:\n\nServiceid: ".$serviceid."\nProduct: ".$product->name."\n\nRegards",
                                    $headers
                                );
                            }
                        } else {
                            mail(
                                                        $email,
                                                        "New order notification",
                                                        "Dear Team,\n\nNew Order has just been arrived, Please process this order as soon as possible:\n\nServiceid: ".$serviceid."\nProduct: ".$product->name."\n\nRegards",
                                                        $headers
                                                    );
                        }
                    }
                    $result['result'] = true;
                    if ($this->session->cid == 54) {
                        if ($needproforma) {
                            // mail('mail@simson.one','need proforma test reseller',print_r($client, true));
                            log_message('error', print_r($_POST, true));
                            $ppp = $this->create_proforma($this->companyid, $client, $_POST, $serviceid);
                            // mail('mail@simson.one','need proforma',print_r($ppp, true));
                            log_message('error', print_r($ppp, true));
                            if ($ppp['proformaid'] >0) {
                                $resmail = $this->sendProforma(array('invoiceid' => $ppp['proformaid'], 'invoicenum' => $ppp['invoicenum'], 'companyid' => $this->companyid));
                            }
                        } else {
                            $this->Admin_model->CreateFutureInvoiceOnActivation($serviceid, $client->vat_rate, 7);
                        }
                    }
                } else {
                    $result['result'] =false;
                    $result['message'] = "Service mobile was not created";
                }
            }

            unset($_SESSION['registration']);

            if ($this->data['setting']->mobile_platform == "TEUM") {
                if (!empty($_SESSION['order']['ContractDuration'])) {
                    $this->db->query("update a_services_addons set teum_autoRenew=? where serviceid=? and addonid=?", array($_SESSION['order']['ContractDuration']-1, $serviceid, $_SESSION['order']['addon']));
                    log_message('error', $this->db->last_query());
                }
            }
            unset($_SESSION['order']);
            echo json_encode($result);
        }
    }


    public function create_proforma($companyid, $client, $product, $serviceid)
    {
        $result['proformaid'] =0;
        $result['invoicenum'] = 0;
        $this->load->model('Admin_model');
        $service = $this->Admin_model->getService($serviceid);
        $killdate = new DateTime(date('Y-m-d'));
        $killdate->modify('+' . $client->payment_duedays . ' day');
        $setupfee = getSetupFee($service->packageid, $client->vat_rate, $client->id);
        log_message('error', print_r($setupfee, true));
        if ($this->data['setting']->country_base == "NL") {
            $inum = getNewInvoicenum($companyid);
            $this->load->library('magebo', array('companyid' => $companyid));
            $ogm = $this->magebo->Mod11($inum);
        } else {
            $ogm = ogm(getNewInvoicenum($companyid));
        }

        $proforma = array(
            'companyid' => $companyid,
            'invoicenum' => getNewInvoicenum($companyid),
            'userid' => $client->id,
            'subtotal' => '0.00',
            'tax' => $setupfee->tax,
            'total' => $setupfee->total,
            'date' => date('Y-m-d'),
            'duedate' => $killdate->format('Y-m-d'),
            'ogm' => $ogm,
            'notes' => str_replace(' ', '', $ogm),
            'paymentmethod' => 'banktransfer',
            'status' => 'Unpaid');

        $proformaid = $this->Admin_model->CreateProforma($proforma);
        if ($proformaid) {
            //$pin = "0" . substr($iPincode, 2, 1) . "-" . substr($iPincode, 3, 2) . " " . substr($iPincode, 5, 2) . " " . substr($iPincode, 7, 2) . " " . substr($iPincode, 9, 2);

            $this->Admin_model->CreateProformaItems(array(
                'companyid' => $companyid,
                'invoiceid' => $proformaid,
                'userid' => $client->id,
                'description' => 'Subscription Activation Fee (one time)',
                'price' => $setupfee->total,
                'qty' => 1,
                'amount' => $setupfee->total,
                'taxrate' => $client->vat_rate,
                'serviceid' => $serviceid,
                'invoicetype' => 'Service',
            ));
            $result['proformaid'] = $proformaid;
            $result['invoicenum'] = $proforma['invoicenum'];
        }

        return $result;
    }
    public function sendProforma($dd)
    {
        //  error_reporting(1);
        //echo "Sending Proforma\n";
        $this->data['setting'] = globofix($dd['companyid']);

        $this->load->model('Admin_model');
        $this->load->model('Proforma_model');

        //echo json_encode(array('result' => false, 'data' => $_POST)); dcoppieters@delta.nl
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/', 0755, true);
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/';
        $invoiceid = $dd['invoiceid'];
        // print_r($dd);
        $invoice = $this->Proforma_model->getInvoice($invoiceid);
        $client = $this->Admin_model->getClient($invoice['userid']);

        $this->load->library('magebo', array('companyid' => $client->companyid));
        if ($dd['invoicenum'] <= 0) {
            die('Invoice not found 11');
        }

        $invoice_ref = $this->magebo->Mod11($dd['invoicenum']);
        //$invoice_ref = "11111111111";
        // print_r($invoice_ref);
        $res = $this->download_id($invoiceid, $dd['companyid'], $this->data['setting']);

        if ($res) {
            if ($this->data['setting']->smtp_type == 'smtp') {
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => $this->data['setting']->smtp_host,
                    'smtp_port' => $this->data['setting']->smtp_port,
                    'smtp_user' => $this->data['setting']->smtp_user,
                    'smtp_pass' => $this->encryption->decrypt($this->data['setting']->smtp_pass),
                    'mailtype' => 'html',
                    'charset' => 'utf-8',
                    'starttls' => true,
                    'wordwrap' => true,
                );
            } else {
                $config['protocol'] = 'sendmail';
                $config['mailpath'] = '/usr/sbin/sendmail';
                $config['mailtype'] = 'html';
                $config['charset'] = 'utf-8';
                $config['wordwrap'] = true;
            }
            $this->email->clear(true);
            $this->email->initialize($config);
            $body = getMailContent('proforma_reseller', $client->language, $client->companyid);
            //$attachments = getAttachmentEmail('invoice', $client->language, $client->companyid);

            $amount = $invoice['total'];
            $body = str_replace('{$mInvoiceAmount}', number_format($amount, 2), $body);
            $body = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $body);
            $body = str_replace('{$invoiceid}', $invoice['id'], $body);
            $body = str_replace('{$clientid}', $client->mvno_id, $body);
            $body = str_replace('{$dInvoiceDate}', $invoice['date'], $body);
            $body = str_replace('{$dInvoiceDueDate}', $invoice['duedate'], $body);
            $body = str_replace('{$Companyname}', $this->data['setting']->companyname, $body);
            $body = str_replace('{$name}', format_name($client), $body);
            $body = str_replace('{$cInvoiceReference}', $invoice_ref, $body);

            $this->email->set_newline("\r\n");
            $this->email->from($this->data['setting']->smtp_sender, $this->data['setting']->smtp_name);
            $this->email->to($client->email);
            //$this->email->to(array('mail@simson.one', $_SESSION['email']));
            $subject = getSubject('proforma_reseller', $client->language, $client->companyid);
            $subject = str_replace('{$iInvoiceNbr}', $invoice['invoicenum'], $subject);
            $subject = str_replace('{$Companyname}', $this->data['setting']->companyname, $subject);
            $this->email->bcc($_SESSION['email']);
            $this->email->subject($subject);
            $this->email->attach($res);
            $this->email->message($body);
            if ($this->email->send()) {
                logEmailOut(array('userid' => $client->id, 'companyid' => $client->companyid, 'to' => $client->email, 'subject' => $subject, 'message' => $body));

                return array('result' => true);
            } else {
                return array('result' => false, 'error' => $this->email->print_debugger());
            }
        } else {
            return array('result' => false);
        }
    }

    public function download_id($id, $companyid, $setting)
    {
        $this->data['setting'] = $setting;
        set_time_limit(0);
        $this->load->model('Admin_model');

        if ($companyid == 54) {
            $brand = $this->Admin_model->getBrandPdfFooter(1);
            //print_r($brand);
            //exit;
            $this->lang->load('admin');
        }
        $this->config->set_item('language', 'english');
        $this->lang->load('admin');
        // $this->lang->load('admin');
        $invoice = $this->Proforma_model->getInvoice($id);

        $customerku = $this->Proforma_model->getClient($invoice['userid']);
        $this->load->library('magebo', array('companyid' => $companyid));
        $this->load->library('Spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        //$pdf->setData($invoice['notes']);
        //$pdf->setBank($this->data['setting']->bank_acc);
        //$pdf->settax($invoice['tax']);
        //$pdf->SetTitle($this->data['setting']->companyname . ' ' . $invoice['invoicenum']);
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        //$pdf->droidsansogo($this->data['setting']->proforma_footer);
        $pdf->SetAutoPageBreak(true, 30);
        $pdf->SetAuthor('Simson Parlindungan & Tim Claesen');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->setFoot($brand->brand_footer);
        $pdf->setFooterFont('droidsans');
        //$pdf->disableExtrabox(true);

        /*
        $this->getPageNumGroupAlias() to get current page number
        and
        $this->getPageGroupAlias() to get total page count
        */

        $tax = round($invoice['tax']) . '%';

        $pdf->AddPage('P', 'A4');

        $pdf->Image($brand->brand_logo, 10, 10, 60);
        $style = array('position' => '',
           'align' => 'R',
           'stretch' => false,
           'fitwidth' => true,
           'cellfitalign' => '',
           'border' => false,
           'hpadding' => 'auto',
           'vpadding' => 'auto',
           'fgcolor' => array(0, 0, 0),
           'bgcolor' => false,
           'text' => true,
           'font' => 'droidsans',
           'fontsize' => 8,
           'stretchtext' => 6,
        );

        $style1 = array('position' => '',
           'align' => 'R',
           'stretch' => false,
           'fitwidth' => true,
           'cellfitalign' => '',
           'border' => true,
           'hpadding' => 'auto',
           'vpadding' => 'auto',
           'fgcolor' => array(64, 64, 64),
           'bgcolor' => false,
           'color' => array(255, 255, 255),
           'text' => true,
           'font' => 'droidsans',
           'fontsize' => 8, 'stretchtext' => 6,
        );
        $pdf->setY(13);
        $pdf->setX(130);
        // $pdf->write1DBarcode(preg_replace("/[^0-9,.]/", "", $invoice['invoicenum']), 'I25', '', '', '', 18, 0.9, $style, 'R');

        $pdf->GetPage();

        $pdf->Image($brand->brand_logo, 10, 10, 90);
        $pdf->SetFont('droidsans', '', 10);
        if (!empty($customerku->companyname)) {
            $company = $customerku->companyname . "\n";
            $contact = '';
        } else {
            $company = '';
            $contact = $customerku->firstname . " " . $customerku->lastname . "\n";
        }
        //$address1 = str_replace("&#039;", "'", $customerku->address1) . "\n";
        $address1 = str_replace("&#039;", "'", $customerku->address1. ' '.$customerku->housenumber. ' '.$customerku->alphabet) . "\n";
        $pdf->setY(70);
        $city = $customerku->postcode . ", " . $customerku->city . "\n";
        $country = $customerku->country . "\n";
        $pdf->SetFont('droidsans', 'B', 12);
        $pdf->setCellPaddings(0, 0, 0, 0);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $a = '';
        $pdf->MultiCell(100, 10, $a, 0, 'J', 0, 0, '', '', true, 0, false, true, 30, 'T');
        $pdf->MultiCell(90, 30, $company . '' . $contact . '' . $address1 . '' . $city . '' . getCountryNameLang($customerku->country, $customerku->language), 0, 'L', 1, 1, '', '', true, 0, false, true, 30, 'M');

        if ($this->data['setting']->country_base == "BE") {
            $this->data['invoice_ref'] = ogm($this->uri->segment(4));
        } else {
            $this->data['invoice_ref'] = $this->magebo->Mod11($invoice['invoicenum']);
            // $invoice_ref = $this->magebo->Mod11($invoice->iInvoiceNbr);
        }

        $pdf->SetFont('droidsans', 'B', 14);
        //$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 153, 0)));
        $pdf->cell(70, 4, lang("Proforma Invoice") . ": " . $invoice['invoicenum'], 0, 1, 'L');
        $pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(64, 64, 64);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(35, 7, lang('Client Number'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(40, 7, lang('VAT'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(50, 7, lang('Reference'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Date'), 1, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(35, 7, lang('Duedate'), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->setCellPaddings(1, 1, 1, 1);
        $pdf->setCellMargins(1, 1, 1, 1);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('droidsans', '', 9);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(35, 5, trim($invoice['mvno_id']), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(40, 5, $customerku->vat, 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(50, 5, $this->data['invoice_ref'], 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['date'])), 1, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(35, 5, date("d-m-Y", strtotime($invoice['duedate'])), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->Ln(15);

        # Invoice Items
        $tblhtml = '<table width="550" bgcolor="#404040" cellspacing="1" cellpadding="1" border="0">
    <tr height="40" bgcolor="#404040" style="font-weight:bold;text-align:center;">
    <td align="left" width="7%"  color="#fff" >' . lang('Qty') . '</td>
        <td align="left" width="58%"  color="#fff">' . lang('Description') . '</td>
         <td align="right" width="18%" color="#fff">' . lang('Price/pcs') . '</td>
        <td align="right" width="18%" color="#fff">' . lang('Amount') . '</td>
    </tr>';
        foreach ($invoice['items'] as $item) {
            $tblhtml .= '
    <tr bgcolor="#fff" border="1">
         <td align="left">' . $item['qty'] . '<br /></td>
        <td align="left">' . nl2br($item['description']) . '<br /></td>
        <td align="right">' . '&euro;' . number_format($item['price'], 2) . '</td>
        <td align="right">' . '&euro;' . exvat($item['taxrate'], $item['amount']) . '</td>
    </tr>';
        }
        $tblhtml .= '

</table>';

        $pdf->writeHTML($tblhtml, true, false, false, false, '');

        $pdf->Ln(5);

        $styles = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64));

        //$pdf->Ln(10);
        $pdf->SetFont('droidsansb', 'B', 9);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->setCellPaddings(2, 1, 2, 1);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));

        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, 'Subtotal' . " :", 1, 'R', 1, 0, '', '', true, 0, false, true, 40, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['subtotal'], 2), 1, 'R', 1, 1, '', '', true, 0, false, true, 40, 'T');
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(64, 64, 64)));
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, "VAT " . floor($invoice['items'][0]['taxrate']) . '%:', 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['tax'], 2), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(135, 5, '', 0, 'L', 1, 0, '', '', true, 0, false, true, 10, 'T');

        $pdf->SetFillColor(64, 64, 64);
        $pdf->setCellMargins(0, 0, 0, 0);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->MultiCell(30, 5, "Total :", 1, 'R', 1, 0, '', '', true, 0, false, true, 10, 'T');
        $pdf->MultiCell(30, 5, $this->data['setting']->currency . number_format($invoice['total'], 2), 1, 'R', 1, 1, '', '', true, 0, false, true, 10, 'T');
        $taxrate = 1;
        $lebar = '38';
        $ypos = '100';
        $xpos = '161';
        /*
        Gelieve het totale bedrag te storten voor 13/03/2019
        op rekeningnummer NL10RABO0147382548
        met vermelding van de referentie 5310 0005 5200 0000
         */
        $pdf->setY(-120);
        $pdf->SetLeftMargin(-10);
        $pdf->SetRightMargin(0);
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('droidsansb', 'B', 7);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setImageScale(1.53);
        $pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
        $pdf->RoundedRect(5, 240, 200, 40, 3.50, '1111');
        $pdf->setXY(10, 242);
        $pdf->SetFont('droidsansb', '', 12);
        $pdf->Cell(140, 0, lang("Please pay amount of ") . " ".$this->data['setting']->currency . number_format($invoice['total'], 2) . " " . lang("to be transfered before ") . date("d-m-Y", strtotime($invoice['duedate'])), 0, 0, 'R', 0, '0', 4);
        $pdf->setXY(10, 248);
        $pdf->Cell(100, 0, lang("On account number") . " : NL10RABO0147382548", 0, 0, 'R', 0, '0', 4);
        if ($invoice['items'][0]['taxrate'] > 0) {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        } else {
            $pdf->ln(4);
            $pdf->setXY(10, 264);
            $pdf->Cell(30, 0, lang("Vat Exempt"), 0, 0, 'R', 0, '0', 4);
            $pdf->ln(4);
            $pdf->setXY(10, 270);
            $pdf->Cell(100, 0, lang("with payment reference ") . $this->data['invoice_ref'], 0, 0, 'R', 0, '0', 4);
        }
        //$this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/'

        $pdf->Output($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf', 'F');
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf')) {
            return $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/proformas/Proforma_' . $id . '.pdf';
        } else {
            return false;
        }
    }

    public function setting()
    {
        $this->load->model('Agent_model');
        if (isPost()) {
            $this->db = $this->load->database('default', true);
            $this->db->where('id', $_SESSION['reseller']['id']);
            $this->db->update('a_clients_agents', $_POST);
            if ($this->db->affected_rows()>0) {
                $this->session->set_flashdata('success', 'Account has been updated');
            } else {
                $this->session->set_flashdata('error', 'Account wasn\'t updated');
            }
            // redirect('reseller/setting');
        }
        $this->data['agent'] = $this->Agent_model->getAgent($_SESSION['reseller']['id']);
        $this->data['title'] = "Reseller Dasboard";
        $this->data['main_content'] = reseller_theme($this->companyid)."setting";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }

    public function orderdetail()
    {
        $this->data['title'] = "Reseller Dasboard";
        $this->data['main_content'] = reseller_theme($this->companyid)."dashboard";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }


    public function viewclient()
    {
        if (!isAllowed_reseller($this->session->reseller['id'], $this->uri->segment(4))) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        $this->data['client'] = $this->Admin_model->getClient($this->uri->segment(4));
        if ($this->data['client']->paymentmethod == "directdebit") {
            $this->data['financial'] = $this->Admin_model->getMandateId($this->data['client']->mageboid);
            if ($this->session->id == 1) {
                if (empty($this->data['financial']->IBAN)) {
                    $this->sepa_amend_cli(array('type' => 'NEW', 'userid' => $this->data['client']->id, 'iban' => $this->data['client']->iban));
                    //print_r($this->data['financial']);
                    $this->data['financial'] = $this->Admin_model->getMandateId($this->data['client']->mageboid);
                }
            }
        }
        $this->data['title'] = "Reseller Dasboard";
        $this->data['main_content'] = reseller_theme($this->companyid)."view_client";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }
    public function editclient()
    {
        if (!isAllowed_reseller($this->session->reseller['id'], $this->uri->segment(4))) {
            $this->session->set_flashdata('error', 'Access Denied');
            redirect('reseller/dashboard/client');
        }
        $this->data['client'] = (array)$this->Admin_model->getClient($this->uri->segment(4));
        $this->data['title'] = "Reseller Dasboard";
        $this->data['main_content'] = reseller_theme($this->companyid)."edit_client";
        $this->load->view(reseller_theme($this->companyid)."content", $this->data);
    }

    public function key_add()
    {
        $sso = "on";
        $_POST['level'] ="reseller";
        if ($this->Admin_model->getKeys($this->companyid, $_SESSION['reseller']['id']) <= 2) {
            $key = $this->Admin_model->generateNewKey($this->session->companyid, $sso, $this->session->reseller['agent'], 'reseller');
            $this->session->set_flashdata('success', 'new key has been generated :' . $key . ' ' . $this->Admin_model->getKeys($this->data['setting']->companyid));
        } else {
            $this->session->set_flashdata('error', lang('You have reach your maximum keys of 3'));
        }

        redirect('reseller/dashboard/setting');
    }

    public function ip_add()
    {
        if (isPost()) {
            if ($this->Admin_model->getIps() <= 7) {
                if ($this->Admin_model->checkIP(trim(strtolower($_POST['ip'])))) {
                    $this->session->set_flashdata('error', 'You already have this ip:' . $_POST['ip'] . ' on the list');
                } else {

                    $_POST['companyid'] = $this->companyid;
                    if ($this->Admin_model->insertIP($_POST)) {
                        $this->session->set_flashdata('success', lang('new IP has been inserted :') . trim($_POST['ip']));
                    } else {
                        $this->session->set_flashdata('error', lang('there was an error inserting your new IP'));
                    }
                }
            } else {
                $this->session->set_flashdata('error', lang('You have reach your maximum IP of 8'));
            }
        } else {
            $this->session->set_flashdata('error', lang('Wrong Method'));
        }

        redirect('reseller/dashboard/setting');
    }

    public function delete_key()
    {
        if(!$_SESSION['reseller']['id']){
            $this->session->set_flashdata('error', lang('Please login'));
            redirect('reseller/dashboard/setting');
        }
        $del = $this->Admin_model->delete_key($this->uri->segment(4), $_SESSION['reseller']['id']);

        if ($del) {
            $this->session->set_flashdata('success', lang('Token key has been deleted'));
        } else {
            $this->session->set_flashdata('error', lang('Token key can not be deleted'));
        }

        //$this->session->set_flashdata('success', lang('API key has been deleted'));
        redirect('reseller/dashboard/setting');
    }

    public function delete_ip()
    {
        $del = $this->Admin_model->delete_ip($this->uri->segment(4));
        if ($del) {
            $this->session->set_flashdata('success', lang('Ip address has been deleted'));
        } else {
            $this->session->set_flashdata('error', lang('Ip address can not be deleted'));
        }

        redirect('reseller/dashboard/setting');
    }

}
