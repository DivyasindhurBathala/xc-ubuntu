<?php
defined('BASEPATH') or exit('No direct script access allowed');
class InvoiceController extends CI_Controller
{
    protected $data = [];

    public function __construct()
    {
        // Ensure you run parent constructor
        parent::__construct();

        $this->companyid = get_companyidby_url(base_url());

        $this->data['setting'] = globofix($this->companyid);
        $this->config->set_item('language', $_SESSION['client']['language']);
        $this->lang->load('client');
    }
}
class Invoice extends InvoiceController
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Admin_model');
        $this->load->model('Client_model');
    }

    public function index()
    {
        if ($this->session->cid == 54) {
            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                redirect('client/invoice');
            }
        }

        //print_r($_SESSION['client']['state']);
        $this->data['title'] = lang("Invoice");
        $this->data['dtt'] = true;
        $this->data['main_content'] = 'themes/' . $this->data['setting']->default_theme . '/mobile/' . 'invoice';
        $this->load->view('themes/' . $this->data['setting']->default_theme . '/mobile/' . 'content', $this->data);
    }

    public function pay()
    {
        $live = "live_z8wbDeH4z4ANTpcmQKpy87RvMH9tea";
        $test = "test_lj3QAVk44vstI9VJTuukZAP0siDJVN";
        $invoice = $this->Invoice_model->getInvoice($this->uri->segment(4));
        //print_r($invoice);
        require APPPATH . "third_party/mollie/src/Mollie/API/Autoloader.php";

        $mollie = new Mollie_API_Client;
        $mollie->setApiKey("live_z8wbDeH4z4ANTpcmQKpy87RvMH9tea");

        $payment = $mollie->payments->create(array(
            "amount" => $invoice['total'],
            "description" => "Factuur #" . $invoice['invoicenum'],
            "redirectUrl" => base_url() . 'client/invoice',
            "webhookUrl" => base_url() . 'payment/mollie',
            "metadata" => array('invoiceid' => $invoice['id']),
        ));

        $this->Invoice_model->insertmollie(array('paymentid' => $payment->id, 'invoiceid' => $invoice['id'], 'userid' => $invoice['userid']));
        header('Location: ' . $payment->links->paymentUrl);
    }
    public function detail()
    {
        redirect('mobile/invoice/download/' . $this->uri->segment(4));
    }
    public function download()
    {
        $id = $this->uri->segment(4);

        $client = $this->Admin_model->getClient($this->session->client['id']);
        if (!file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/')) {
            mkdir($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/', 0755, true);
        }
        if (!empty($this->uri->segment(5))) {
            unlink($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf');
            unlink($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf');
        }
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf')) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $id . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/withcdr/' . $id . '.pdf');
            exit;
        }
        if (file_exists($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf')) {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $id . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
            @readfile($this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/' . $id . '.pdf');
            exit;
        }
        $invoice_path = $this->data['setting']->DOC_PATH . '/' . $this->companyid . '/invoices/';

        $id = $this->uri->segment(4);
        if ($this->data['setting']->companyid == 2) {
            $companyid = array(1, 2, 3, 4, 6, 8, 9, 25, 26, 28, 29, 31, 34, 35, 37, 41, 47, 50);
        } else {
            $companyid[] = $this->companyid;
        }

        if ($this->Client_model->IsAllowedInvoice($id, $client->mageboid)) {
            $invoice = $this->Admin_model->getInvoicePdf($id);

            //print_r($invoice->PDF);
            // exit;

            $file = $invoice_path . $id . '.pdf';

            file_put_contents($file, $invoice->PDF);
            if (file_exists($file)) {
                //if ($this->session->cid != "53") {
                $cdr = $this->downloadcdrpdf($id, $this->session->cid);
                if ($cdr) {
                    shell_exec('pdftk ' . $file . ' ' . $cdr . ' cat output ' . $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf');
                    unlink($file);
                    copy($this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf', $file);
                    $file = $this->data['setting']->DOC_PATH . $this->session->cid . '/invoices/withcdr/' . $id . '.pdf';
                }
                //}

                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $id . '.pdf"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                @readfile($file);
            } else {
                echo "file does not exists";
            }
        } else {
            $this->session->set_flashdata('error', lang('This acction has been denied'));
            redirect('errors/access_denied');
        }
    }
    public function downloadcdrpdf($id, $companyid)
    {
        error_reporting(1);
        $setting = globofix($companyid);
        //$mageboid = $this->uri->segment(5);
        $this->load->library('magebo', array('companyid', $companyid));
        $cdrs = $this->magebo->getInvoiceCdrFile($id);
        $gprs = $this->magebo->getDataCdr($id);
        // print_r($cdrs);
        //$cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);
        // exit;
        if (!$cdrs) {
            if ($gprs) {
                $gp = "";
            } else {
                return false;
            }
        } else {
            //print_r($cdrs);
            // exit;
            $table = '<h2>CDR Voice & SMS: ' . $pincode . '</h2><br />
            <table><thead>
            <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
            <th width="12%">' . lang('Date') . '</th>
            <th width="10%">' . lang('Number') . '</th>
            <th width="15%">' . lang('Destination') . '</th>
            <th width="42%">' . lang('Description') . '</th>
            <th width="10%">' . lang('Duration') . '</th>
            <th width="10%">' . lang('Cost') . ' (incl. VAT)</th>
            </tr>
            </thead>
            <tbody>
            ';
            // mail('mail@simson.one','cdr',print_r($cdrs, true));
            foreach ($cdrs as $array) {
                $table .= '
                <tr>
                <td width="12%">' . substr($array->dCallDate, 0, -3) . '</td>
                <td width="10%">' . $array->iPincode . '</td>
                <td width="15%">' . $array->cDialedNumber . '</td>
                <td width="42%">' . $array->cInvoiceGroupDescription.' - ' .$array->cCallTranslation. '</td>
                <td width="10%">' . gmdate("H:i:s", $array->iInvoiceDuration) . '</td>
                <td width="10%">'.$this->data['setting']->currency . str_replace('.', ',', includevat('21', $array->mInvoiceSale)) . '</td>
                </tr>
                ';
            }

            $table .= '</tbody></table>';
        }

        if ($gprs) {
            foreach ($gprs as $pincode => $cdr) {
                $gp .= '<h2>CDR DATA: ' . $pincode . '</h2><br />
                <table><thead>
                <tr bgcolor="#111" color="#fff" cellpadding="1" cellspacing="0">
                <th width="15%">' . lang('Date') . '</th>
                <th width="13%">' . lang('Number') . '</th>
                <th width="20%">' . lang('Type') . '</th>
                <th width="35%">' . lang('Zone') . ' </th>
                <th width="10%">' . lang('Bytes') . '</th>


                </tr>
                </thead>
                <tbody>
                ';
                foreach ($cdr as $array) {
                    $bytes[] = $array->Bytes;
                    $gp .= '
                    <tr>
                    <td width="15%">' . substr($array->SessionDate, 0, -3) . '</td>
                    <td width="13%">' . $array->iPincode . '</td>
                    <td width="20%">' . $array->Type . '</td>
                    <td width="35%">' . $array->Zone . ' '.$array->RoamingCountry . '</td>
                    <td width="10%">' . convertToReadableSize($array->Bytes) . '</td>

                    </tr>
                    ';
                }
                $gp .= '</tbody></table><br /><br /><strong>Total Data usage  ' . $pincode . ' : ' . convertToReadableSize(array_sum($bytes)) . '</strong>';
                unset($bytes);
            }
        }

        $this->load->library('spdf');
        $pdf = new Spdf('P', 'mm', 'A4', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Simson Asuni');
        $pdf->SetTitle('CDR');
        $pdf->setInvoicenumber($id);
        $pdf->SetSubject('CDR Invoice ' . $id);
        $pdf->SetKeywords('TCPDF, Invoice, Cdr, ' . $id);
        $pdf->AddPage('L', 'A4');
        //$pdf->SetFont('droidsans', '', '12');
        //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
        $pdf->ln(10);
        //$pdf->SetFont('droidsans', '', '9');
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);
        $pdf->SetPrintFooter(false);
        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(true, "10");

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once dirname(__FILE__) . '/lang/eng.php';
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // print TEXT
        $pdf->SetFont('helvetica', '', '9');
        //  $pdf->PrintChapter(1, lang('Call Detail Records'), $table, true);
        $pdf->writeHTML($table, true, false, true, false, 'J');
        if ($gprs) {
            $pdf->AddPage('L', 'A4');
            //$pdf->SetFont('droidsans', '', '12');
            //$pdf->Cell(180, 6, lang('Call Detail Records').' Invoice: '.$id, 0, 0, '', 0);
            $pdf->ln(10);
            $pdf->SetFont('helvetica', '', '9');
            // set default header data
            $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 010', PDF_HEADER_STRING);

            // set header and footer fonts
            $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(true, "10");

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once dirname(__FILE__) . '/lang/eng.php';
                $pdf->setLanguageArray($l);
            }
            $pdf->writeHTML($gp, true, false, true, false, 'J');
        }
        // print HTML
        //$pdf->PrintChapter(2, 'LOREM IPSUM [HTML]', FCPATH.'data/chapter_demo_2.txt', true);

        // ---------------------------------------------------------

        //Close and output PDF document
        if (!file_exists($setting->DOC_PATH . $companyid . '/invoices/cdr')) {
            mkdir($setting->DOC_PATH . $companyid . '/invoices/cdr');
        }
        $pdf->Output($setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf', 'F');
        return $setting->DOC_PATH . $companyid . '/invoices/cdr/' . $id . '.pdf';
    }
    public function downloadcdr($id)
    {
        //$id = $this->uri->segment(4);

        $client = $this->Admin_model->getClient($this->session->client['id']);
        $filename = $this->data['setting']->DOC_PATH . $client->companyid . '/cdr/' . $id . '.pdf';
        if (!file_exists($filename)) {
            $cdrs = $this->Admin_model->mageboGet('GetCdrs/' . $id);

            if (count($cdrs) > 0) {
                $html = '<table style="white-space:nowrap" width="100%">';
                $html .= "<thead><tr><th align='left'>" . lang('Date') . "</th><th align='left'>" . lang('Type') . "</th><th align='left'>" . lang('Destination') . "</th><th align='left'>" . lang('Duration') . "</th><th align='right'>" . lang('Cost') . "</th></tr></thead><tbody>";
                foreach ($cdrs as $array) {
                    $html .= "<tr><td align='left'>" . $array->datum . "</td><td align='left'>NA</td><td align='left'>" . $array->naar . "</td><td align='left'>" . gmdate("H:i:s", $array->duur) . "</td><td  align='right'>€" . str_replace('.', ',', includevat('21', $array->kost)) . "</td></tr>";
                    //$to[] = array($array->datum, 'NA', $array->naar, gmdate("H:i:s", $array->duur), includevat('21', $array->kost));
                }
                $html .= "</tbody></table>";
                /*      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                $this->xlswriter->writeSheet($to, 'Sheet1', $header);
                $this->xlswriter->setAuthor('Simson Parlindungan');
                $this->xlswriter->setTitle('Cdr Export');
                $this->xlswriter->setCompany('Delta');
                $filename = $this->data['setting']->DOC_PATH . $client->companyid . '/cdr/' . $id . '.xlsx';
                $this->xlswriter->writeToFile($filename);
                 */

                $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML($html);
                $mpdf->Output($filename, 'F');

                return $filename;
            } else {
                return false;
            }
        } else {
            return $filename;
        }
    }
}
