# Dockerfile

FROM php:7.3-apache
COPY conf/apache.conf /etc/apache2/sites-available/000-default.conf

RUN a2enmod rewrite

RUN docker-php-ext-install mysqli

COPY / /var/www/html/mvno

RUN chown -R www-data:www-data /var/www/html

RUN service apache2 restart

EXPOSE 80