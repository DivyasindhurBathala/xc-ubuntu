  $(document).ready(function() {
      $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
          $('#clients').DataTable({
              "autoWidth": false,
              "processing": true,
              "orderCellsTop": true,
              "ordering": true,
              "serverSide": true,
              "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getadmins',
              "aaSorting": [
                  [0, 'asc']
              ],
              "language": {
                  "url": window.location.protocol + '//' + window.location.host + "/assets/clear/js/datatables/lang/" + data.result + ".json"
              },

              "fnRowCallback": function(nRow, aData, iDisplayIndex) {
                  $('td:eq(0)', nRow).html('<div class="logged-user-avatar-info"><div class="avatar-w"><img height="40" class="rounded" src="' + window.location.protocol + '//' + window.location.host + '/assets/img/staf/' + aData[0] + '"></div></div>');
                  $('td:eq(5)', nRow).html('<a class="btn btn-sm btn-primary" href="' + window.location.protocol + '//' + window.location.host + '/admin/setting/staf_edit/' + aData[5] + '"><i class="fa fa-search"></i> Details</a> <a class="btn btn-sm btn-danger" href="' + window.location.protocol + '//' + window.location.host + '/admin/setting/staf_delete/' + aData[5] + '"><i class="fa fa-trash"></i> Delete</a>');

                  return nRow;


              },

          });

      });

  });