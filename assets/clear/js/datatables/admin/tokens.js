  $(document).ready(function()
{
$.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#tokens').DataTable({
    "autoWidth": false,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": true,
    "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/gettokens',
    "aaSorting": [[0, 'asc']],
        "language": {
      "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },

    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
      $('td:eq(2)', nRow).html(aData[2]+' (Level: '+aData[4]+')');
    $('td:eq(3)', nRow).html('<a href="#" class="btn btn-sm btn-danger close" onclick="confirmation_delete_key(\''+aData[3]+'\');"><i class="fa fa-trash"></i></a>');
              return nRow;
        },

  });
$('#ips').DataTable({
    "autoWidth": false,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": true,
    "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getips',
    "aaSorting": [[0, 'asc']],
        "language": {
      "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },

    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

   $('td:eq(3)', nRow).html('<a class="btn btn-sm btn-danger close" onclick="confirmation_delete_ip(\''+aData[3]+'\');"><i class="fa fa-trash"></i></a>');

              return nRow;


        },

  });

  $('#logs').DataTable({
    "autoWidth": true,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": true,
    "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getapi_logs',
    "aaSorting": [[1, 'desc']],
        "language": {
      "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },

    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
              return nRow;
        },

  });
  });




});