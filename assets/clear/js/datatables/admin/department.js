 $(document).ready(function()
{
    $.getJSON(window.location.protocol + '//' + window.location.host + '/admin/complete/get_lang', function(data) {
$('#dt_cn').DataTable({
    "autoWidth": false,
    "processing": true,
    "orderCellsTop": true,
    "ordering": true,
    "serverSide": true,
    "ajax": window.location.protocol + '//' + window.location.host + '/admin/table/getdepartment',
    "aaSorting": [[0, 'desc']],
     "language": {
           "url": window.location.protocol + '//' + window.location.host +"/assets/clear/js/datatables/lang/" + data.result + ".json"
        },
    "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
    $('td:eq(2)', nRow).html('<a class="btn btn-sm btn-primary" href="'+window.location.protocol + '//' + window.location.host + '/admin/setting/department_setting/' + aData[2] + '"><i class="fa fa-search"></i> View</a> <a class="btn btn-sm btn-primary" href="'+window.location.protocol + '//' + window.location.host + '/admin/setting/department_delete/' + aData[2] + '"><i class="fa fa-trash"></i> Delete</a>');
              return nRow;


        },
    
  });

});

});