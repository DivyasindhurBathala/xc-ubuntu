$( function() {

$(".zipcomplete").autocomplete({
     source: function(request, response) {
         // Fetch data
         $.ajax({
             url: window.location.protocol + '//' + window.location.host + '/admin/complete/zip',
             type: 'post',
             dataType: "json",
             data: {
                 keyword: request.term
             },
             success: function(data) {
                 response(data);
             }
         });
     },
     select: function(event, ui) {
         // Set selection
         $('#postcodenum').val(ui.item.value); // display the selected text

         $('#cityname').val(ui.item.city); // save selected id to input


         return false;
     }
 });
 $(".invoicecomplete").autocomplete({
    source: function(request, response) {
        // Fetch data
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/complete/invoices',
            type: 'post',
            dataType: "json",
            data: {
                keyword: request.term
            },
            success: function(data) {
                response(data);
            }
        });
    },
    select: function(event, ui) {
        // Set selection
        $('#invoicenum').val(ui.item.iInvoiceNbr); // display the selected text

        $('#amount').val(ui.item.mInvoiceAmount); // save selected id to input


        return false;
    }
});
 $(".invoicecreditnote").autocomplete({
    source: function(request, response) {
        // Fetch data
        $.ajax({
            url: window.location.protocol + '//' + window.location.host + '/admin/complete/getCreditnoteInvoicetobeProcess',
            type: 'post',
            dataType: "json",
            data: {
                keyword: request.term
            },
            success: function(data) {
                response(data);
            }
        });
    },
    select: function(event, ui) {
      $('#create_cn').prop('disabled', false);
   $('#hideme').show('slow');
   $('#invoicenum').val(ui.item.value); // display the selected text
   $('#cName').val(ui.item.cName);
   $('#mInvoiceAmount').val(ui.item.mInvoiceAmount); // save selected id to input
   $('#iAddressNbr').val(ui.item.iAddressNbr);


        return false;
    }
});


 $(".streetcomplete").autocomplete({
     source: function(request, response) {
         var zipcode = $('#postcodenum').val();
         $.ajax({
             url: window.location.protocol + '//' + window.location.host + '/admin/complete/street',
             type: 'post',
             dataType: "json",
             data: {
                 keyword: request.term,
                 zipcode: zipcode
             },
             success: function(data) {
                 response(data);
             }
         });
     },
     select: function(event, ui) {
         // Set selection
         $('#street').val(ui.item.value); // display the selected text
         return false;
     }
 });

 $( ".modalonly" ).autocomplete({
  appendTo: $("#modalonly").next(),
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/smart',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#userid').val(ui.item.value); // display the selected text
   $('#customerinfo').html('<div class="form-group"> <label for="companyname">Companyname</label> <input id="companyname" class="form-control" type="text" value="'+ui.item.companyname+'" readonly> </div> <div class="form-group"> <label for="customername">Customername</label> <input id="customername" class="form-control" type="text" readonly value="'+ui.item.fullname+'"> </div>');
   return false;
  }
 });

 // Initialize
 $( "#customerid" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $('#pelanggan').hide('slow');
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/complete/searchclient',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
     $('#step3').prop('disabled', false);
   // Set selection
   $('#customerid').val(ui.item.value); // display the selected tex
   $('#customername').val(ui.item.label);
   $('#namapelanggan').val(ui.item.label);
   $('#bos').prop('disabled', false);
   $('#pelanggan').show('slow');
   $('#namapelanggan').focus();

   return false;
  }
 });

 


 // Initialize
 $( "#customeridx" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $('#pelanggan').hide('slow');
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/complete/searchclientx',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
     $('#step3').prop('disabled', false);
   // Set selection
   $('#customerid').val(ui.item.value); // display the selected tex
   $('#customername').val(ui.item.label);
   $('#namapelanggan').val(ui.item.label);
   $('#bos').prop('disabled', false);
   $('#pelanggan').show('slow');
   $('#namapelanggan').focus();

   return false;
  }
 });

 



//"option", "appendTo", ".eventInsForm"
 $( ".pengkor" ).autocomplete({
  minLength: 4,
  source: function( request, response ) {
    var companyid = $('#cidx').val();
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/complete/getsimcard/'+companyid,
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
        $('#acceptx').prop('disabled', true);
     response( data );
    }
   });
  },
  select: function (event, ui) {
    console.log(ui.item.value);
    $('#simnumberx').val(ui.item.value);
    $('#msisdnxy').show('slow'); 
    $('#msisdnxx').html(ui.item.msisdn);
    $('#acceptx').prop('disabled', false);
   return false;
  }
 });



 // Initialize
 $( "#vat" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/btw',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#autocomplete').val(ui.item.label); // display the selected text
   $('#vat').val(ui.item.value); // display the selected text
   $('#city').val(ui.item.city); // save selected id to input
   $('#companyname').val(ui.item.companyname);
   $('#address1').val(ui.item.address1);
   $('#postcode').val(ui.item.postcode);
   return false;
  }
 });

  $( ".client" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/client',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#customer').val(ui.item.label); // display the selected text
   window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/invoice/create/'+ui.item.value);
   /*
   $('#city').val(ui.item.city); // save selected id to input
   $('#companyname').val(ui.item.companyname);
   $('#address1').val(ui.item.address1);
   $('#postcode').val(ui.item.postcode);
   */
   return false;
  }
 });

    $( ".invoices" ).autocomplete({
    source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/complete/invoices',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
        $('#create_cn').prop('disabled', true);
     response( data );
    }
   });
  },
  select: function (event, ui) {
   $('#create_cn').prop('disabled', false);
   $('#hideme').show('slow');
   $('#invoicenum').val(ui.item.value); // display the selected text
   $('#cName').val(ui.item.cName);
   $('#mInvoiceAmount').val(ui.item.mInvoiceAmount); // save selected id to input
   $('#iAddressNbr').val(ui.item.iAddressNbr);

   return false;
  }
 });

 $( ".invoiceassign" ).autocomplete({

   source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/complete/invoices',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
    $('#assign').prop('disabled', true);
    $('#complete').prop('disabled', true);
     response( data );
    }
   });
  },
  select: function (event, ui) {
   $('#complete').hide();
   $('#create_cn').prop('disabled', false);
   $('#hideme').show('slow');
   $('#invoicenum').val(ui.item.value); // display the selected text
   $('#cName').val(ui.item.cName);
   $('#mInvoiceAmount').val(ui.item.mInvoiceAmount); // save selected id to input
   $('#iAddressNbr').val(ui.item.iAddressNbr);
   $('#assign').prop('disabled', false);
   return false;
  }
 });


   $( ".clientcreditnote" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/client',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#customer').val(ui.item.label); // display the selected text
   window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/creditnote/create/'+ui.item.value);
   /*
   $('#city').val(ui.item.city); // save selected id to input
   $('#companyname').val(ui.item.companyname);
   $('#address1').val(ui.item.address1);
   $('#postcode').val(ui.item.postcode);
   */
   return false;
  }
 });

    $( ".clientquote" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/clientsearch',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#userid').val(ui.item.value); // display the selected text
    $('#fakeid').val(ui.item.value); // display the selected text
   $('#customername').val(ui.item.customername);
   $('#buzzme').show();
   $('#customername').prop('readonly', true);

   /*
   $('#city').val(ui.item.city); // save selected id to input
   $('#companyname').val(ui.item.companyname);
   $('#address1').val(ui.item.address1);
   $('#postcode').val(ui.item.postcode);
   */
   return false;
  }
 });



    $( ".addpayment" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/clientsearch',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#userid').val(ui.item.value); // display the selected text
   console.log(ui.item.value);
   $('#fakeid').val(ui.item.value); // display the selected text
   $('#customername').val(ui.item.customername);
   $('#buzzme').show();
   $('#customername').prop('readonly', true);
    $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/getInvoices/'+ui.item.value,
    type: 'POST',
    data: {
     keyword: ui.item.value
    },
    success: function( data ) {
     console.log(data);
     $("#invoices").show();
     $("#items").html(data);
    $('#scriptme').html('<script>$(document).ready(function(){ $(".pickme").click(function(){console.log(\'click me\');    });       $(".pickme").click(function(){      $("#invoice_id").val($(this).attr(\'id\'));$("#addmecall").addClass(\'has-success\');$("#invoice_id").addClass(\'is-valid\');});}); </script>');
    }
   });



   /*



     $.ajax({
        url: 'https://api.xmusix.eu/complete/clientinvoices/'+str[0],
        type: 'POST',
        data: {userid:str[0]},
        success:function(data){
                console.log(data);
        $('#invoices').show();
        $('#items').show();
        $("#items").html(data);
        }
         });



   $('#city').val(ui.item.city); // save selected id to input
   $('#companyname').val(ui.item.companyname);
   $('#address1').val(ui.item.address1);
   $('#postcode').val(ui.item.postcode);
   */
   return false;
  }
 });



  $( ".clientdetails" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/admin/complete/searchclient',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#customer').val(ui.item.label); // display the selected text
   if(ui.item.type == "client"){
      window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/client/detail/'+ui.item.value);
   }else if(ui.item.type == "service"){
      window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/subscription/detail/'+ui.item.value);
  }else if(ui.item.type == "invoice"){
    window.location.replace(window.location.protocol + '//' + window.location.host + '/admin/invoice/detail/'+ui.item.value+'/'+ui.item.iAddressNbr);
   }



   /*
   $('#city').val(ui.item.city); // save selected id to input
   $('#companyname').val(ui.item.companyname);
   $('#address1').val(ui.item.address1);
   $('#postcode').val(ui.item.postcode);
   */
   return false;
  }
 });
 $( ".clientinventory" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/smart',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#userid').val(ui.item.value); // display the selected text

   /*
   $('#city').val(ui.item.city); // save selected id to input
   $('#companyname').val(ui.item.companyname);
   $('#address1').val(ui.item.address1);
   $('#postcode').val(ui.item.postcode);
   */
   return false;
  }
 });

 $( ".products" ).autocomplete({

  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/products',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
  	var id = $(this).attr('id');
  	console.log(id);
 	var nomor = id.replace(/\D/g,'');
 	var tax = $('#tax').val();
 	var qty = $('#qty'+nomor).val();
   // Set selection
   $('#'+id).val(ui.item.product); // display the selected text
   $('#pcs'+nomor).val(ui.item.value); // save selected id to input
   var total = ui.item.value*qty;
   $('#total'+nomor).val(total);
   return false;
  }
 });
 $( "#companyname" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $('#loading_data_icon').html('<i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>');    // showing loading icon
   $.ajax({
    url: window.location.protocol + '//' + window.location.host + '/complete/company',
    type: 'post',
    dataType: "json",
    data: {
     keyword: request.term
    },
    success: function( data ) {
       $('#loading_data_icon').html('');    // showing loading icon
     response( data );
    }
   });
  },
  select: function (event, ui) {
   // Set selection
   $('#autocomplete').val(ui.item.label); // display the selected text
   $('#companyname').val(ui.item.value); // display the selected text
   $('#city').val(ui.item.city); // save selected id to input
   $('#vat').val(ui.item.btw);
   $('#address1').val(ui.item.address1);
   $('#postcode').val(ui.item.postcode);
   return false;
  }
 });


});
