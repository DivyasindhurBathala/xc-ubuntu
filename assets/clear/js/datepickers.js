$( function() {
$( "#pickdate" ).datepicker( { dateFormat: 'yy-mm-dd', changeMonth: true,
      changeYear: true,yearRange: "1902:2018"} );
$( "#pickdate1" ).datepicker( { dateFormat: 'yy-mm-dd'} );
$( "#pickdate2" ).datepicker( { dateFormat: 'yy-mm-dd' } );
$( "#pickdate3" ).datepicker( { dateFormat: 'yy-mm-dd' } );
$( "#magebo_fin_date" ).datepicker( { dateFormat: 'mm-dd-yy' } );
$( "#pickdate4" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
$( "#pickdate20" ).datepicker( { dateFormat: 'mm-dd-yy',minDate:0 } );
$( "#pickdate22" ).datepicker( { dateFormat: 'mm-dd-yy',minDate:1 });
$( "#pickdate24" ).datepicker( { dateFormat: 'mm-dd-yy',minDate:1 });
$( "#pickdate5" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
$( "#pickdate6" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0 } );
$( "#pickdate66" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:1 } );
$( "#firstdate").datepicker ({dateFormat: 'yy-mm-dd',minDate:0, beforeShowDay: function (date) {

        if (date.getDate() == 1) {
            return [true, ''];
        }
        return [false, ''];
       }});
$( "#pickdate7" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:2,beforeShowDay: disableSpecificDaysAndWeekends } );
$( "#pickdate8" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:6,beforeShowDay: disableSpecificDaysAndWeekends } );
$( "#pickdate9" ).datepicker( { dateFormat: 'yy-mm-dd',minDate:0,beforeShowDay: disableSpecificDaysAndWeekends } );
var disabledSpecificDays = ["01-01-2018", "3-30-2018", "4-01-2018", "2-04-2018", "4-05-2018", "05-05-2018", "10-05-2018", "13-05-2018", "20-05-2018", "21-05-2018", "17-06-2018", "4-11-2018", "25-12-2018", "26-12-2018"];
$( "#pickdate13x1" ).datepicker( { dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true,yearRange: "2015:2099" } );
$( "#pickdate13x2" ).datepicker( { dateFormat: 'yy-mm-dd',changeMonth: true,changeYear: true,yearRange: "2015:2099" } );
$( "#pickdate13" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099" } );
$( "#pickdate13a" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099" } );
$( "#pickdate13b" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099" } );
$( "#pickdate13c" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:30,changeMonth: true,changeYear: true,yearRange: "2019:2099" } );
$( "#pickdate14" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099" } );
$( "#pickdate15" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099",beforeShowDay: function(date)
{
    // getDate() returns the day [ 0 to 31 ]
    if (date.getDate() ==
        getLastDayOfYearAndMonth(date.getFullYear(), date.getMonth()))
    {
        return [true, ''];
    }

    return [false, ''];
}});

$( "#pickdate11" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099",beforeShowDay: function(date)
{
    // getDate() returns the day [ 0 to 31 ]
    if (date.getDate() ==
        getFirstDayOfYearAndMonth(date.getFullYear(), date.getMonth()))
    {
        return [true, ''];
    }

    return [false, ''];
}});


$( "#pickdate15a" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099",beforeShowDay: function(date)
{
    // getDate() returns the day [ 0 to 31 ]
    if (date.getDate() ==
        getLastDayOfYearAndMonth(date.getFullYear(), date.getMonth()))
    {
        return [true, ''];
    }

    return [false, ''];
}});
$( "#pickdate15b" ).datepicker( { dateFormat: 'yy-mm-dd\T00:00:00',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099",beforeShowDay: function(date)
{
    // getDate() returns the day [ 0 to 31 ]
    if (date.getDate() ==
        getLastDayOfYearAndMonth(date.getFullYear(), date.getMonth()))
    {
        return [true, ''];
    }

    return [false, ''];
}});
$( "#pickdate15d" ).datepicker( { dateFormat: 'yy-mm-dd\T23:59:59',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099",beforeShowDay: function(date)
{
    // getDate() returns the day [ 0 to 31 ]
    if (date.getDate() ==
        getLastDayOfYearAndMonth(date.getFullYear(), date.getMonth()))
    {
        return [true, ''];
    }

    return [false, ''];
}});
$( "#pickdate15e" ).datepicker( { dateFormat: 'yy-mm-dd\T23:59:59',minDate:0,changeMonth: true,changeYear: true,yearRange: "2019:2099",beforeShowDay: function(date)
{
    // getDate() returns the day [ 0 to 31 ]
    if (date.getDate() ==
        getLastDayOfYearAndMonth(date.getFullYear(), date.getMonth()))
    {
        return [true, ''];
    }

    return [false, ''];
}});


$( "#editorderdate" ).datepicker( {
	 onSelect: function(dateText, inst) {
	 	var serviceid = $('#simid').val();
        var date = $(this).val();
 $.ajax({
  url: window.location.protocol + '//' + window.location.host + '/admin/subscription/changeorder_startcontract',
  dataType: 'json',
  type: 'post',
  data: {
  	'serviceid': serviceid,
  'date': date
  },
  success: function(data) {

  },
  error: function(errorThrown) {}
  });

    },
	dateFormat: 'mm-dd-yy',
	minDate:0,
	beforeShowDay: disableSpecificDaysAndWeekends
} );
//var disabledSpecificDays = ["1, 1", "3, 30", "4, 1", "4, 2", "4, 5", "5, 5", "5, 10", "5, 13", "5, 20", "5, 21", "6, 17", "11, 4", "12, 25", "12, 26"];
/*
var enforceModalFocusFn = $.fn.modal.Constructor.prototype.enforceFocus;
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
$confModal.on('hidden', function() {
$.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
});
$confModal.modal({ backdrop : false });
*/

function getLastDayOfYearAndMonth(year, month)
{
    return(new Date((new Date(year, month + 1, 1)) - 1)).getDate();
}

function disableSpecificDaysAndWeekends(date) {
var m = date.getMonth();
var d = date.getDate();
var y = date.getFullYear();
for (var i = 0; i < disabledSpecificDays.length; i++) {
if ($.inArray((m + 1) + '-' + d + '-' + y, disabledSpecificDays) != -1 || new Date() > date) {
return [false];
}
}
var noWeekend = $.datepicker.noWeekends(date);
return !noWeekend[0] ? noWeekend : [true];
}
} );

