<?php
class DataTable {
  
  var $database = "GDC_ERP";
  var $host = "10.17.2.50";
  var $user = "Spongebobke";
  var $pass = "spongebobke";
  var $conn;
  var $numRows;
  var $baseTest = "tblInvoice";
  
    public function __construct() {
        $this->connect($this->host, $this->user, $this->pass, $this->database);
    }
    public function connect($_host, $_user, $_password, $_database) {
        $this->conn = mssql_connect($_host, $_user, $_password) or die(mssql_get_last_message());
        mssql_select_db($this->database, $this->conn) or die(mssql_get_last_message());
    }
    function query($sql) {
        mssql_query('SET ANSI_WARNINGS ON', $this->conn) or die(mssql_get_last_message());
        mssql_query('SET ANSI_NULLS ON', $this->conn) or die(mssql_get_last_message());
        $resultQuery = mssql_query($sql, $this->conn) or die(mssql_get_last_message());
        $results = array();
        while ($row = mssql_fetch_array($resultQuery))
            $results[] = $row;
        return $results;
    }
    
    function numRows($sql) {
        $resultQuery = mssql_query($sql, $this->conn) or die(mssql_get_last_message());
        $result = mssql_num_rows($resultQuery);
        return $result;
    }
  
  public function getData($params, $table, $columns) {
        $where = $sqlTot = $sqlRec = '';
        
        if( !empty($params['search']['value']) ) {
            $where = " WHERE";
            $i = 0;
            foreach($columns as $c) {
                if($i === 0) {
                    $where .= (count($columns) === 1) ? " (" . $c . " LIKE '%" . $params['search']['value'] . "%' " 
                                                      : " ( " . $c . " LIKE '%" . $params['search']['value'] . "%' ";
                } else {
                    $where .=" OR " . $c . " LIKE '%" . $params['search']['value'] . "%' ";
                }
                $i++;
            }
            $where .= " )";
        }
        
        $sql = "SELECT * FROM  [" . $table . "]";
    
        //Total de registros en la tabla
        $totalRecords = $this->numRows(utf8_decode($sql));
    
        $sqlTot .= $sql;
        
        $sqlRec = "SELECT TOP " . $params['length'] . " * FROM( SELECT *, ROW_NUMBER() over (ORDER BY "
                . $columns[$params['order'][0]['column']] . " " 
                . $params['order'][0]['dir'] . " ) as ct FROM "
                . "[" . $table . "]"
                . " " . $where . " ) sub WHERE ct > " . $params['start'];
        if(isset($where) && $where != '') {
            $sqlTot .= $where;
        }
        
        //Registros a mostrar en la tabla
        $dataRecords = $this->query(utf8_decode($sqlRec));
        
        //Total registros después del filtro
        $totalRecordsFiltered = $this->numRows(utf8_decode($sqlTot));
        $data = array(
            "draw"            => intval($params['draw']),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecordsFiltered),
            "data"            => $dataRecords
        );
        
        return $data;
    }
}