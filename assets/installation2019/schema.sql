-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: mvno
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `a_admin`
--

DROP TABLE IF EXISTS `a_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `companyid` int(11) NOT NULL DEFAULT '53',
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `address1` varchar(100) CHARACTER SET utf16 DEFAULT NULL,
  `gender` varchar(6) NOT NULL DEFAULT 'male',
  `postcode` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(2) NOT NULL DEFAULT 'BE',
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `resetcode` varchar(100) DEFAULT NULL,
  `master_code` varchar(20) DEFAULT NULL,
  `status` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  `datecreated` date NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'administratie',
  `picture` varchar(200) DEFAULT 'nopic.png',
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `phonenumber` varchar(12) DEFAULT NULL,
  `nationalnr` varchar(20) DEFAULT NULL,
  `theme` varchar(100) NOT NULL DEFAULT 'clear',
  `lastseen` int(11) DEFAULT NULL,
  `ip` varchar(30) DEFAULT NULL,
  `master` tinyint(4) NOT NULL DEFAULT '0',
  `email_notification` tinyint(4) NOT NULL DEFAULT '0',
  `order_notification` tinyint(4) NOT NULL DEFAULT '0',
  `ticket_notification` text,
  `isagent` tinyint(11) NOT NULL DEFAULT '0',
  `request_export_portinlist` tinyint(4) NOT NULL DEFAULT '0',
  `disable_dashboard_notification` tinyint(4) NOT NULL DEFAULT '0',
  `accept_tos` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=536 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_admin_group_roles`
--

DROP TABLE IF EXISTS `a_admin_group_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_admin_group_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `permissions` varchar(100) CHARACTER SET utf8 NOT NULL,
  `access` text NOT NULL,
  `date_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=409 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_admin_ip`
--

DROP TABLE IF EXISTS `a_admin_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_admin_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipaddr` varchar(20) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_admin_permissions`
--

DROP TABLE IF EXISTS `a_admin_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_admin_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folder` varchar(40) NOT NULL,
  `controller` varchar(100) NOT NULL,
  `method` varchar(100) NOT NULL,
  `descriptions` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`controller`,`method`)
) ENGINE=InnoDB AUTO_INCREMENT=789 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_admin_roles`
--

DROP TABLE IF EXISTS `a_admin_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_admin_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `rolename` varchar(100) NOT NULL,
  `perms` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61479 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_api_data`
--

DROP TABLE IF EXISTS `a_api_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_api_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `aabr` varchar(10) DEFAULT NULL,
  `m_payment_type` enum('PRE PAID','POST PAID','','') NOT NULL DEFAULT 'POST PAID',
  `m_username` varchar(100) NOT NULL,
  `m_password` varchar(100) NOT NULL,
  `teum_mvnoid` varchar(20) DEFAULT NULL,
  `m_provider` varchar(10) NOT NULL DEFAULT 'T-MOBILE',
  `platform` enum('TEUM','ARTA') NOT NULL,
  `arta_url` text NOT NULL,
  `arta_version` text NOT NULL,
  `arta_proxy_url` text,
  `arta_proxy_port` varchar(11) DEFAULT NULL,
  `unity_username` varchar(100) NOT NULL,
  `unity_password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=2117 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_apilog`
--

DROP TABLE IF EXISTS `a_apilog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_apilog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_env` varchar(10) DEFAULT NULL,
  `companyid` int(11) DEFAULT NULL,
  `api_call` varchar(50) DEFAULT NULL,
  `request` text NOT NULL,
  `result` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58615 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_chat`
--

DROP TABLE IF EXISTS `a_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) NOT NULL DEFAULT '',
  `to` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to` (`to`(191)),
  KEY `from` (`from`(191))
) ENGINE=InnoDB AUTO_INCREMENT=924 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_client_agents_payments`
--

DROP TABLE IF EXISTS `a_client_agents_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_client_agents_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `companyid` int(11) NOT NULL,
  `agentid` int(11) NOT NULL,
  `rawdata` text CHARACTER SET utf8 NOT NULL,
  `gateway` varchar(20) DEFAULT NULL,
  `payment_method` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `agentid` (`agentid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_client_login`
--

DROP TABLE IF EXISTS `a_client_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_client_login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `date_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3027 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients`
--

DROP TABLE IF EXISTS `a_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) DEFAULT NULL,
  `mvno_id` varchar(200) DEFAULT NULL,
  `companyid` int(10) NOT NULL,
  `mageboid` int(11) NOT NULL,
  `agentid` int(11) DEFAULT NULL,
  `ContactType2Id` int(11) DEFAULT '7',
  `teum_CustomerId` int(11) DEFAULT NULL,
  `teum_OrderCode` varchar(20) DEFAULT NULL,
  `vat` varchar(30) NOT NULL,
  `kvk` varchar(30) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(300) NOT NULL,
  `pwresetkey` varchar(20) DEFAULT NULL,
  `companyname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `initial` varchar(20) CHARACTER SET utf8 NOT NULL,
  `salutation` varchar(50) CHARACTER SET utf8 NOT NULL,
  `nationalnr` varchar(30) DEFAULT NULL,
  `firstname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `address1` varchar(100) CHARACTER SET utf8 NOT NULL,
  `housenumber` varchar(15) DEFAULT NULL,
  `alphabet` varchar(5) DEFAULT NULL,
  `postcode` varchar(20) NOT NULL,
  `city` varchar(200) NOT NULL,
  `country` varchar(2) NOT NULL,
  `language` varchar(15) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active',
  `phonenumber` varchar(20) NOT NULL,
  `paymentmethod` enum('directdebit','banktransfer','creditcard','online','paypal','mollie','sisow','others') NOT NULL DEFAULT 'directdebit',
  `payment_duedays` int(11) NOT NULL DEFAULT '14',
  `gender` enum('male','female') NOT NULL DEFAULT 'male',
  `iban` varchar(100) DEFAULT NULL,
  `iban_holder` varchar(200) DEFAULT NULL,
  `bic` varchar(100) DEFAULT NULL,
  `sso_id` varchar(20) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `date_created` date NOT NULL,
  `date_modified` datetime NOT NULL,
  `vat_exempt` tinyint(4) NOT NULL DEFAULT '0',
  `vat_rate` decimal(10,2) NOT NULL DEFAULT '21.00',
  `invoice_email` varchar(200) DEFAULT 'yes',
  `refferal` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `dunning_profile` tinyint(1) DEFAULT '0',
  `idcard_number` varchar(50) DEFAULT NULL,
  `id_type` enum('ID','Passport','DrivingLicence','DNI','NIF','NIE','CIF','Visa') NOT NULL DEFAULT 'Passport',
  `authdata` varchar(255) DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `lat` varchar(12) DEFAULT NULL,
  `lon` varchar(12) DEFAULT NULL,
  `google_id` varchar(60) DEFAULT NULL,
  `facebook_id` varchar(60) DEFAULT NULL,
  `google_pic` varchar(255) DEFAULT NULL,
  `facebook_pic` varchar(255) DEFAULT NULL,
  `notes` text,
  `lastseen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `mageboid` (`mageboid`),
  KEY `mvno_id` (`mvno_id`),
  KEY `email` (`email`),
  KEY `agentid` (`agentid`),
  KEY `lat` (`lat`),
  KEY `lon` (`lon`)
) ENGINE=InnoDB AUTO_INCREMENT=950437 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_agents`
--

DROP TABLE IF EXISTS `a_clients_agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ContactType2Id` int(11) DEFAULT NULL,
  `status` enum('Inactive','Active') NOT NULL DEFAULT 'Active',
  `companyid` int(11) NOT NULL,
  `agent` varchar(200) NOT NULL,
  `contact_name` varchar(50) DEFAULT NULL,
  `address1` varchar(70) CHARACTER SET utf8 NOT NULL,
  `postcode` varchar(10) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `city` varchar(40) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `resetcode` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phonenumber` varchar(17) DEFAULT NULL,
  `language` varchar(10) NOT NULL DEFAULT 'english',
  `country` varchar(2) DEFAULT NULL,
  `reseller_type` enum('Prepaid','Postpaid','Internal') NOT NULL DEFAULT 'Postpaid',
  `reseller_balance` decimal(15,2) NOT NULL DEFAULT '0.00',
  `comission_type` enum('Percentage','FixedAmount','None') NOT NULL,
  `comission_value` decimal(15,2) NOT NULL DEFAULT '0.00',
  `lastseen` int(11) DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `last_month_earning` decimal(15,2) NOT NULL DEFAULT '0.00',
  `isdefault` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5079 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_agents_earning`
--

DROP TABLE IF EXISTS `a_clients_agents_earning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_agents_earning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `agentid` int(11) NOT NULL,
  `month` varchar(20) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_agents_perms`
--

DROP TABLE IF EXISTS `a_clients_agents_perms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_agents_perms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `agentid` int(11) NOT NULL,
  `reload` tinyint(4) NOT NULL DEFAULT '1',
  `addbundle` tinyint(4) NOT NULL DEFAULT '1',
  `addorder` tinyint(4) NOT NULL DEFAULT '1',
  `editclient` tinyint(4) NOT NULL DEFAULT '1',
  `addclient` tinyint(4) NOT NULL DEFAULT '1',
  `service` tinyint(4) NOT NULL DEFAULT '1',
  `helpdesk` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `agentid` (`agentid`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_agents_topups`
--

DROP TABLE IF EXISTS `a_clients_agents_topups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_agents_topups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agentid` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(15,4) NOT NULL,
  `descriptions` text NOT NULL,
  `companyid` int(11) NOT NULL,
  `executor` varchar(50) NOT NULL,
  `channels` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_ciot`
--

DROP TABLE IF EXISTS `a_clients_ciot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_ciot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) DEFAULT NULL,
  `mvno_id` varchar(200) DEFAULT NULL,
  `companyid` int(10) NOT NULL,
  `mageboid` int(11) NOT NULL,
  `vat` varchar(30) NOT NULL,
  `kvk` varchar(30) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(300) NOT NULL,
  `pwresetkey` varchar(20) DEFAULT NULL,
  `companyname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `initial` varchar(20) CHARACTER SET utf8 NOT NULL,
  `salutation` varchar(50) CHARACTER SET utf8 NOT NULL,
  `nationalnr` varchar(30) DEFAULT NULL,
  `firstname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(100) CHARACTER SET utf8 NOT NULL,
  `address1` varchar(100) CHARACTER SET utf8 NOT NULL,
  `housenumber` varchar(5) DEFAULT NULL,
  `alphabet` varchar(20) DEFAULT NULL,
  `postcode` varchar(20) NOT NULL,
  `city` varchar(200) NOT NULL,
  `country` varchar(2) NOT NULL,
  `language` varchar(15) NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active',
  `phonenumber` varchar(20) NOT NULL,
  `paymentmethod` enum('directdebit','banktransfer','creditcard','online','paypal','mollie','sisow') NOT NULL DEFAULT 'directdebit',
  `payment_duedays` int(11) NOT NULL DEFAULT '14',
  `gender` enum('male','female') NOT NULL DEFAULT 'male',
  `iban` varchar(100) NOT NULL,
  `iban_holder` varchar(200) DEFAULT NULL,
  `bic` varchar(100) NOT NULL,
  `sso_id` varchar(20) NOT NULL,
  `date_birth` date NOT NULL,
  `date_created` date NOT NULL,
  `date_modified` datetime NOT NULL,
  `vat_exempt` tinyint(4) NOT NULL DEFAULT '0',
  `vat_rate` decimal(10,2) NOT NULL DEFAULT '21.00',
  `invoice_email` varchar(200) DEFAULT 'yes',
  `refferal` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `dunning_profile` tinyint(1) DEFAULT '0',
  `idcard_number` varchar(50) DEFAULT NULL,
  `agentid` int(11) DEFAULT NULL,
  `authdata` varchar(255) DEFAULT NULL,
  `ContactType2Id` int(11) NOT NULL DEFAULT '7',
  `location` varchar(20) DEFAULT NULL,
  `lat` varchar(12) DEFAULT NULL,
  `lon` varchar(12) DEFAULT NULL,
  `ciot` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `mageboid` (`mageboid`),
  KEY `mvno_id` (`mvno_id`),
  KEY `email` (`email`),
  KEY `agentid` (`agentid`),
  KEY `lat` (`lat`),
  KEY `lon` (`lon`)
) ENGINE=InnoDB AUTO_INCREMENT=945342 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_contacts`
--

DROP TABLE IF EXISTS `a_clients_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_contacts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `userid` int(10) NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `companyname` text NOT NULL,
  `email` text NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL,
  `city` text NOT NULL,
  `state` text NOT NULL,
  `postcode` text NOT NULL,
  `country` text NOT NULL,
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `phonenumber` text NOT NULL,
  `subaccount` int(1) NOT NULL DEFAULT '0',
  `password` text NOT NULL,
  `permissions` text NOT NULL,
  `domainemails` int(1) NOT NULL,
  `generalemails` int(1) NOT NULL,
  `invoiceemails` int(1) NOT NULL,
  `productemails` int(1) NOT NULL,
  `supportemails` int(1) NOT NULL,
  `affiliateemails` int(1) NOT NULL,
  `pwresetkey` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pwresetexpiry` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `gsm` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid_firstname_lastname` (`userid`,`firstname`(32),`lastname`(32)),
  KEY `email` (`email`(64)),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=2240 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_files`
--

DROP TABLE IF EXISTS `a_clients_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_files` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `userid` int(10) NOT NULL,
  `title` text NOT NULL,
  `filename` text NOT NULL,
  `adminonly` int(1) NOT NULL,
  `dateadded` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=514 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_mandates`
--

DROP TABLE IF EXISTS `a_clients_mandates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_mandates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `mandate_id` varchar(100) NOT NULL,
  `mandate_date` date NOT NULL,
  `mandate_status` enum('SEPA REOCCURRING','SEPA FIRST') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userid_3` (`userid`),
  KEY `companyid` (`companyid`),
  KEY `userid` (`userid`),
  KEY `userid_2` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=1812 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_notes`
--

DROP TABLE IF EXISTS `a_clients_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_notes` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL DEFAULT '53',
  `userid` int(10) NOT NULL,
  `admin` varchar(200) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `note` text NOT NULL,
  `sticky` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=1394 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_clients_sso`
--

DROP TABLE IF EXISTS `a_clients_sso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_clients_sso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_2` (`username`),
  KEY `username` (`username`),
  KEY `userid` (`userid`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=1147 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_config_master`
--

DROP TABLE IF EXISTS `a_config_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_config_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `val` varchar(100) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(100) NOT NULL DEFAULT 'System',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_configuration`
--

DROP TABLE IF EXISTS `a_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_configuration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `val` text,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=5303 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_countries`
--

DROP TABLE IF EXISTS `a_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(70) CHARACTER SET utf8mb4 NOT NULL,
  `dial_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=231 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_countries_full`
--

DROP TABLE IF EXISTS `a_countries_full`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_countries_full` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `dates` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `datee` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `DE` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `EN` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `FR` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `NL` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `DE_O` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `EN_O` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `FR_O` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `NL_O` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `aditional` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_danish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=294 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_currencies`
--

DROP TABLE IF EXISTS `a_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_currencies` (
  `code` varchar(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `symbol` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_debug_sql`
--

DROP TABLE IF EXISTS `a_debug_sql`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_debug_sql` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  `funct` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=151037 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_email_log`
--

DROP TABLE IF EXISTS `a_email_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_email_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `to` varchar(100) NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('sent','error','draft') NOT NULL DEFAULT 'sent',
  `error_message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50053 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_email_templates`
--

DROP TABLE IF EXISTS `a_email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_email_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `subject` varchar(300) NOT NULL,
  `variables` text,
  `description` varchar(100) NOT NULL,
  `language` varchar(100) NOT NULL,
  `body` text CHARACTER SET utf8mb4 NOT NULL,
  `default_body` text NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_modified` datetime DEFAULT NULL,
  `last_modifier` varchar(50) NOT NULL DEFAULT 'System',
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77778 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_event_log`
--

DROP TABLE IF EXISTS `a_event_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_event_log` (
  `JobId` int(11) NOT NULL AUTO_INCREMENT,
  `date_event` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SubscriptionId` varchar(60) DEFAULT NULL,
  `TaskName` varchar(30) DEFAULT NULL,
  `JobName` varchar(50) DEFAULT NULL,
  `Action` varchar(30) DEFAULT NULL,
  `FinalJob` varchar(10) DEFAULT NULL,
  `Success` varchar(10) DEFAULT NULL,
  `ResellerId` int(11) DEFAULT NULL,
  `Id` varchar(70) DEFAULT NULL,
  `companyid` int(11) DEFAULT NULL,
  `serviceid` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `accepted_nodate` tinyint(4) NOT NULL DEFAULT '1',
  `portalread` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`JobId`),
  KEY `companyid` (`companyid`),
  KEY `portalread` (`portalread`),
  KEY `JobName` (`JobName`),
  KEY `TaskName` (`TaskName`),
  KEY `date_event` (`date_event`)
) ENGINE=InnoDB AUTO_INCREMENT=42064 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_event_log_united`
--

DROP TABLE IF EXISTS `a_event_log_united`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_event_log_united` (
  `JobId` int(11) NOT NULL AUTO_INCREMENT,
  `date_event` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `SubscriptionId` varchar(60) DEFAULT NULL,
  `TaskName` varchar(30) DEFAULT NULL,
  `JobName` varchar(50) DEFAULT NULL,
  `Action` varchar(30) DEFAULT NULL,
  `FinalJob` varchar(10) DEFAULT NULL,
  `Success` varchar(10) DEFAULT NULL,
  `ResellerId` int(11) DEFAULT NULL,
  `Id` varchar(70) DEFAULT NULL,
  `companyid` int(11) DEFAULT NULL,
  `serviceid` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `accepted_nodate` tinyint(4) NOT NULL DEFAULT '1',
  `portalread` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`JobId`),
  KEY `companyid` (`companyid`),
  KEY `portalread` (`portalread`),
  KEY `JobName` (`JobName`),
  KEY `TaskName` (`TaskName`),
  KEY `date_event` (`date_event`)
) ENGINE=InnoDB AUTO_INCREMENT=39149 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_event_status`
--

DROP TABLE IF EXISTS `a_event_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_event_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `serviceid` int(11) NOT NULL,
  `sn` varchar(15) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `ack` tinyint(4) NOT NULL DEFAULT '0',
  `processed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1895 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_helpdesk_category`
--

DROP TABLE IF EXISTS `a_helpdesk_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_helpdesk_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `name` text NOT NULL,
  `isdefault` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=647 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_helpdesk_department`
--

DROP TABLE IF EXISTS `a_helpdesk_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_helpdesk_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `name` text NOT NULL,
  `importing` enum('No','Yes') NOT NULL DEFAULT 'No',
  `email` text NOT NULL,
  `pop3_host` text NOT NULL,
  `pop3_user` text NOT NULL,
  `pop3_password` text NOT NULL,
  `pop3_port` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_helpdesk_log`
--

DROP TABLE IF EXISTS `a_helpdesk_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_helpdesk_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `ticketid` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_helpdesk_ticket_replies`
--

DROP TABLE IF EXISTS `a_helpdesk_ticket_replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_helpdesk_ticket_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8,
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `ticketid` int(11) NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `attachments` text,
  `isadmin` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ticketid` (`ticketid`)
) ENGINE=InnoDB AUTO_INCREMENT=15714 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_helpdesk_tickets`
--

DROP TABLE IF EXISTS `a_helpdesk_tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_helpdesk_tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `tid` varchar(20) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `subject` text CHARACTER SET utf8 NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deptid` int(11) NOT NULL,
  `status` enum('Open','In-Progress','On-Hold','Answered','Awaiting-Reply','Resolved','Closed','Proefbilling') NOT NULL,
  `admin` varchar(200) NOT NULL DEFAULT 'System',
  `assigne` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '3',
  `categoryid` int(11) DEFAULT NULL,
  `adminonly` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `categoryid` (`categoryid`),
  KEY `userid` (`userid`),
  KEY `assigne` (`assigne`),
  KEY `deptid` (`deptid`)
) ENGINE=InnoDB AUTO_INCREMENT=17997 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_invoice_email`
--

DROP TABLE IF EXISTS `a_invoice_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_invoice_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_invoiceitems`
--

DROP TABLE IF EXISTS `a_invoiceitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_invoiceitems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `invoiceid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `amount` decimal(15,4) NOT NULL,
  `taxrate` decimal(15,4) NOT NULL,
  `serviceid` int(11) DEFAULT NULL,
  `invoicetype` varchar(20) NOT NULL DEFAULT 'Service',
  PRIMARY KEY (`id`),
  KEY `invoiceid` (`invoiceid`),
  KEY `companyid` (`companyid`),
  KEY `serviceid` (`serviceid`),
  KEY `invoiceid_2` (`invoiceid`)
) ENGINE=InnoDB AUTO_INCREMENT=2521 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_invoices`
--

DROP TABLE IF EXISTS `a_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_invoices` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `invoicenum` varchar(200) DEFAULT NULL,
  `iInvoiceNbr` varchar(12) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `subtotal` decimal(15,4) NOT NULL,
  `tax` decimal(15,4) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date` date NOT NULL,
  `duedate` date NOT NULL,
  `ogm` varchar(24) NOT NULL,
  `notes` varchar(200) DEFAULT NULL,
  `paymentmethod` varchar(200) DEFAULT 'banktransfer',
  `status` enum('Draft','Paid','Unpaid','Credited') NOT NULL DEFAULT 'Draft',
  `datepaid` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `invoicenum` (`invoicenum`),
  KEY `userid` (`userid`),
  KEY `status` (`status`),
  KEY `date` (`date`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=800000532 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_languages`
--

DROP TABLE IF EXISTS `a_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `iso2` varchar(2) NOT NULL,
  `iso3` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_logs`
--

DROP TABLE IF EXISTS `a_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_logs` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL DEFAULT '53',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text NOT NULL,
  `user` text NOT NULL,
  `userid` int(10) NOT NULL,
  `serviceid` int(11) DEFAULT NULL,
  `ip` text,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `serviceid` (`serviceid`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=445837 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_mobile_donor_list`
--

DROP TABLE IF EXISTS `a_mobile_donor_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_mobile_donor_list` (
  `operator_id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(2) NOT NULL DEFAULT 'NL',
  `operator_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `operator_desc` varchar(100) CHARACTER SET utf8 NOT NULL,
  `operator_code` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`operator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=533 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_mobile_stolen_number`
--

DROP TABLE IF EXISTS `a_mobile_stolen_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_mobile_stolen_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `msisdn` varchar(20) NOT NULL,
  `date_report` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reported_by` varchar(30) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_mvno`
--

DROP TABLE IF EXISTS `a_mvno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_mvno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `SimType` enum('PRE PAID','POST PAID','PSEUDO POST PAID') NOT NULL DEFAULT 'POST PAID',
  `companyid` int(11) NOT NULL,
  `RangeNumber` int(11) NOT NULL,
  `companyname` varchar(100) NOT NULL,
  `abbr` varchar(10) DEFAULT NULL,
  `portal_url` varchar(200) NOT NULL,
  `email_notification` varchar(200) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `apache` tinyint(4) NOT NULL DEFAULT '0',
  `ArtaResellerId` int(11) DEFAULT NULL,
  `Prorata` tinyint(4) NOT NULL DEFAULT '1',
  `email_on_order` int(11) NOT NULL DEFAULT '1',
  `email_portin_event` int(11) NOT NULL DEFAULT '1',
  `RID` int(11) NOT NULL,
  `show_incoming_cdr` tinyint(1) NOT NULL DEFAULT '0',
  `show_forwarding_cdr` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `portal_url_2` (`portal_url`),
  KEY `portal_url` (`portal_url`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_packageoptions`
--

DROP TABLE IF EXISTS `a_packageoptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_packageoptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mvno_id` int(11) NOT NULL,
  `cli` bigint(12) NOT NULL,
  `Voice_phone_calls_Originating` int(11) NOT NULL,
  `Data_Originating` int(11) NOT NULL,
  `Short_Messaging_Service_Originating` int(11) NOT NULL,
  `Voice_phone_calls_Terminating` int(11) NOT NULL,
  `Short_Messaging_Service_Terminating` int(11) NOT NULL,
  `Voice_phone_calls_Forward` int(11) NOT NULL,
  `Voice_phone_calls_Transit` int(11) NOT NULL,
  `Voice_phone_calls_Roaming_Originating` int(11) NOT NULL,
  `Data_Roaming_Originating` int(11) NOT NULL,
  `Short_Messaging_Service_Roaming_Originating` int(11) NOT NULL,
  `Voice_phone_calls_Roaming_Terminating` int(11) NOT NULL,
  `Short_Messaging_Service_Roaming_Terminating` int(11) NOT NULL,
  `Voice_phone_calls_Roaming_Forward` int(11) NOT NULL,
  `Premium_Numbers` int(11) NOT NULL,
  `Premium_Gaming` int(11) NOT NULL,
  `International_Numbers` int(11) NOT NULL,
  `Roaming` int(11) NOT NULL,
  `4G` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=638 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_payment_gateway`
--

DROP TABLE IF EXISTS `a_payment_gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_payment_gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `companyid` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `options` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_payments`
--

DROP TABLE IF EXISTS `a_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `invoiceid` bigint(13) DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `paymentmethod` varchar(50) DEFAULT 'banktransfer',
  `transid` varchar(200) DEFAULT NULL,
  `amount` decimal(15,4) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `adminid` int(11) DEFAULT NULL,
  `iban` varchar(45) DEFAULT NULL,
  `raw` text,
  `bar_status` tinyint(1) NOT NULL DEFAULT '1',
  `auto_unsuspend` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15086 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_paypal_payments`
--

DROP TABLE IF EXISTS `a_paypal_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_paypal_payments` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceid` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `txn_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=308 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_portin_management`
--

DROP TABLE IF EXISTS `a_portin_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_portin_management` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `OperationType` varchar(20) DEFAULT NULL,
  `CompanyName` varchar(35) DEFAULT NULL,
  `AuthorisedRequestrName` varchar(25) DEFAULT NULL,
  `VATNumber` varchar(25) DEFAULT NULL,
  `SN` varchar(15) DEFAULT NULL,
  `DonorOperator` varchar(35) DEFAULT NULL,
  `DonorSIM` varchar(35) DEFAULT NULL,
  `RequestType` int(11) DEFAULT NULL,
  `AccountNumber` varchar(20) DEFAULT NULL,
  `MSISDN` varchar(15) DEFAULT NULL,
  `ErrorID` int(11) DEFAULT NULL,
  `ErrorValue` text,
  `ErrorComment` text,
  `Status` varchar(40) DEFAULT NULL,
  `EnteredDate` varchar(20) DEFAULT NULL,
  `ActionDate_old` varchar(20) DEFAULT NULL,
  `ActionDate` varchar(20) DEFAULT NULL,
  `Country` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `SN` (`SN`),
  KEY `MSISDN` (`MSISDN`)
) ENGINE=InnoDB AUTO_INCREMENT=809837 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_portin_notes`
--

DROP TABLE IF EXISTS `a_portin_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_portin_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(14) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` text,
  `agent` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_porting_followup`
--

DROP TABLE IF EXISTS `a_porting_followup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_porting_followup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `agent_name` varchar(70) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_postcodes`
--

DROP TABLE IF EXISTS `a_postcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_postcodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(2) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `city` varchar(40) NOT NULL,
  `state` varchar(40) NOT NULL,
  `active` int(11) NOT NULL,
  `city2` varchar(30) NOT NULL,
  `idnum` varchar(10) NOT NULL,
  `extra1` varchar(20) NOT NULL,
  `extra2` varchar(20) NOT NULL,
  `lat` decimal(10,4) NOT NULL,
  `lon` decimal(10,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postcode` (`postcode`)
) ENGINE=InnoDB AUTO_INCREMENT=459585 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_pricing_specific`
--

DROP TABLE IF EXISTS `a_pricing_specific`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_pricing_specific` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `packageid` int(11) NOT NULL,
  `monthly` decimal(15,4) NOT NULL,
  `quarterly` decimal(15,4) NOT NULL,
  `semi_annually` decimal(15,4) NOT NULL,
  `annually` decimal(15,4) NOT NULL,
  `bienially` decimal(15,4) NOT NULL,
  `triennially` decimal(15,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `packageid` (`packageid`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products`
--

DROP TABLE IF EXISTS `a_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `api_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `proforma` tinyint(4) NOT NULL DEFAULT '0',
  `product_type` enum('mobile','voip','xdsl','others') NOT NULL DEFAULT 'mobile',
  `product_sub_type` enum('Prepaid','Postpaid') NOT NULL DEFAULT 'Postpaid',
  `voice` decimal(15,2) DEFAULT NULL,
  `sms` decimal(15,2) DEFAULT NULL,
  `data` decimal(15,2) DEFAULT NULL,
  `combi` tinyint(4) NOT NULL DEFAULT '0',
  `name` varchar(70) CHARACTER SET utf8mb4 DEFAULT NULL,
  `setup` decimal(15,4) DEFAULT NULL,
  `mobile_swapcost` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `mobile_changecost` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `recurring_total` decimal(15,4) DEFAULT NULL,
  `recurring_subtotal` decimal(15,4) DEFAULT NULL,
  `recurring_tax` decimal(15,4) DEFAULT NULL,
  `recurring_taxrate` decimal(15,4) NOT NULL DEFAULT '21.0000',
  `notes` varchar(50) DEFAULT NULL,
  `iInvoiceGroupNbr` varchar(10) DEFAULT NULL,
  `ArtaPackageId` int(11) DEFAULT NULL,
  `GPcRemark` varchar(100) DEFAULT NULL,
  `iInvoiceDescription` varchar(70) DEFAULT NULL,
  `PriceList` int(11) DEFAULT NULL,
  `PriceListSubType` int(11) DEFAULT NULL,
  `welcome_template` varchar(500) NOT NULL DEFAULT '/reports/UnitedReports/MobileWelcomeLetter/',
  `jasper_server` varchar(500) NOT NULL DEFAULT '/reports/UnitedReports/MobileWelcomeLetter/',
  `promotions` varchar(500) DEFAULT NULL,
  `show_incoming_cdr` tinyint(4) NOT NULL DEFAULT '0',
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `gid` (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products_factsheet`
--

DROP TABLE IF EXISTS `a_products_factsheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products_factsheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `factsheet_name` varchar(60) NOT NULL,
  `url` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products_group`
--

DROP TABLE IF EXISTS `a_products_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ContactId` int(11) NOT NULL DEFAULT '7',
  `brand_name` varchar(20) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `companyid` int(11) NOT NULL,
  `brand_logo` text,
  `brand_footer` text,
  `initial_reload_amount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products_language`
--

DROP TABLE IF EXISTS `a_products_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packageid` int(11) NOT NULL,
  `language` varchar(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products_mobile_bundles`
--

DROP TABLE IF EXISTS `a_products_mobile_bundles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products_mobile_bundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agid` int(11) DEFAULT NULL,
  `companyid` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `name_desc` varchar(40) NOT NULL,
  `bundle_type` enum('tarif','option','others') NOT NULL DEFAULT 'tarif',
  `subscriptionid` int(11) NOT NULL,
  `bundleid` int(11) NOT NULL,
  `recurring_taxrate` decimal(10,4) NOT NULL,
  `recurring_subtotal` decimal(10,4) NOT NULL,
  `recurring_tax` decimal(10,4) NOT NULL,
  `recurring_total` decimal(10,4) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '200',
  `GPInvoiceGroupNbr` int(11) DEFAULT NULL,
  `pricing_query` text,
  `pricing_query_extra` varchar(500) DEFAULT NULL,
  `GPcRemark` varchar(100) DEFAULT NULL,
  `bundle_duration_value` int(11) DEFAULT '0',
  `bundle_duration_type` enum('day','week','month','year') NOT NULL,
  `FCA` text,
  `FCA_extra` varchar(500) DEFAULT NULL,
  `FCL` text,
  `FCL_extra` varchar(500) DEFAULT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `show_customer` tinyint(1) NOT NULL DEFAULT '0',
  `base_bundle` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `bundle_type` (`bundle_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1256 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products_pdf`
--

DROP TABLE IF EXISTS `a_products_pdf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products_pdf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `vars` text,
  `name` varchar(50) NOT NULL,
  `package_gid` int(11) NOT NULL,
  `body` text CHARACTER SET utf8mb4 NOT NULL,
  `companyid` int(11) NOT NULL,
  `language` varchar(100) NOT NULL DEFAULT 'english',
  `font` varchar(30) NOT NULL DEFAULT 'droidsans',
  `font_size` int(11) NOT NULL DEFAULT '9',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4246 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products_voip_bundles`
--

DROP TABLE IF EXISTS `a_products_voip_bundles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products_voip_bundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `bundle_type` enum('tarif','option','others') NOT NULL DEFAULT 'tarif',
  `arta_bundleid` int(11) NOT NULL,
  `magebo_id` int(11) NOT NULL,
  `recurring_taxrate` decimal(10,4) NOT NULL,
  `recurring_subtotal` decimal(10,4) NOT NULL,
  `recurring_tax` decimal(10,4) NOT NULL,
  `recurring_total` decimal(10,4) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '200',
  `GPInvoiceGroupNbr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `bundle_type` (`bundle_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products_xdsl_bundles`
--

DROP TABLE IF EXISTS `a_products_xdsl_bundles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products_xdsl_bundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `bundle_type` enum('tarif','option','others') NOT NULL DEFAULT 'tarif',
  `arta_bundleid` int(11) NOT NULL,
  `magebo_id` int(11) NOT NULL,
  `recurring_taxrate` decimal(10,4) NOT NULL,
  `recurring_subtotal` decimal(10,4) NOT NULL,
  `recurring_tax` decimal(10,4) NOT NULL,
  `recurring_total` decimal(10,4) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '200',
  `GPInvoiceGroupNbr` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `bundle_type` (`bundle_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_products_xdsl_metadata`
--

DROP TABLE IF EXISTS `a_products_xdsl_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_products_xdsl_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `metadata` varchar(10) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_promotion`
--

DROP TABLE IF EXISTS `a_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `promo_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `promo_description` varchar(100) NOT NULL,
  `promo_value` varchar(20) NOT NULL,
  `promo_duration` int(11) NOT NULL,
  `promo_code` varchar(20) NOT NULL,
  `date_valid` date NOT NULL,
  `date_expired` date DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `configuration` text,
  `modified_by` varchar(40) DEFAULT 'System',
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `promo_code` (`promo_code`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_proximus_appointment_time_slot`
--

DROP TABLE IF EXISTS `a_proximus_appointment_time_slot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_proximus_appointment_time_slot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proximusid` varchar(40) NOT NULL,
  `dayOfWeek` varchar(15) DEFAULT NULL,
  `timeSlot` varchar(6) DEFAULT NULL,
  `startDateTime` varchar(40) DEFAULT NULL,
  `endDateTime` varchar(40) DEFAULT NULL,
  `date_received` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3038 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_proximus_offer`
--

DROP TABLE IF EXISTS `a_proximus_offer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_proximus_offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productid` varchar(10) NOT NULL,
  `productname` varchar(20) NOT NULL,
  `packageid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_proximus_order_status`
--

DROP TABLE IF EXISTS `a_proximus_order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_proximus_order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `proximusid` varchar(40) NOT NULL,
  `serviceid` int(11) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `circuitid` varchar(15) DEFAULT NULL,
  `feedback_description` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `remark` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1561 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_reminder`
--

DROP TABLE IF EXISTS `a_reminder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_reminder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `invoicenum` bigint(13) NOT NULL,
  `userid` bigint(13) NOT NULL,
  `type` enum('Reminder1','Reminder2','Reminder3','Reminder4') NOT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3666 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_reminder_exclude`
--

DROP TABLE IF EXISTS `a_reminder_exclude`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_reminder_exclude` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `iInvoiceNbr` varchar(20) NOT NULL,
  `flag` varchar(30) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `iInvoiceNbr` (`iInvoiceNbr`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_request_export`
--

DROP TABLE IF EXISTS `a_request_export`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_request_export` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `recipient` varchar(74) NOT NULL,
  `type_export` enum('client','subscription','others','all') DEFAULT 'client',
  `status` enum('created','exported','sent','failed','processing') NOT NULL DEFAULT 'created',
  `companyid` int(11) NOT NULL,
  `filename` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=1084 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_reseller_products`
--

DROP TABLE IF EXISTS `a_reseller_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_reseller_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agentid` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_reseller_request_changes`
--

DROP TABLE IF EXISTS `a_reseller_request_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_reseller_request_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `terms` int(11) NOT NULL DEFAULT '12',
  `packageid` int(11) NOT NULL,
  `date_request` date NOT NULL,
  `date_accepted` date DEFAULT NULL,
  `status` enum('Pending','Processing','Completed','Rejected') NOT NULL,
  `completed` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_reseller_simcard`
--

DROP TABLE IF EXISTS `a_reseller_simcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_reseller_simcard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `serviceid` int(11) DEFAULT NULL,
  `resellerid` int(11) DEFAULT NULL,
  `simcard` varchar(22) NOT NULL,
  `MSISDN` varchar(20) DEFAULT NULL,
  `IMSI` varchar(20) DEFAULT NULL,
  `PIN1` varchar(4) DEFAULT NULL,
  `PIN2` int(4) DEFAULT NULL,
  `PUK1` int(8) DEFAULT NULL,
  `PUK2` int(8) DEFAULT NULL,
  `AccountId` varchar(20) DEFAULT NULL,
  `CustomerOrderId` varchar(30) DEFAULT NULL,
  `SubscriptionId` varchar(30) DEFAULT NULL,
  `TeumServiceId` varchar(30) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `import_via` enum('scanner','csv','manual') NOT NULL DEFAULT 'manual',
  `created_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `simcard_2` (`simcard`),
  KEY `simcard` (`simcard`),
  KEY `companyid` (`companyid`),
  KEY `resellerid` (`resellerid`),
  KEY `serviceid` (`serviceid`),
  KEY `MSISDN` (`MSISDN`)
) ENGINE=InnoDB AUTO_INCREMENT=43237 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_role`
--

DROP TABLE IF EXISTS `a_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sepa_directdebit`
--

DROP TABLE IF EXISTS `a_sepa_directdebit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sepa_directdebit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` text,
  `uploaded` varchar(100) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `iBankIndex` int(11) NOT NULL,
  `companyid` int(11) NOT NULL,
  `status` enum('new','processing','completed','failed') NOT NULL DEFAULT 'new',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sepa_files`
--

DROP TABLE IF EXISTS `a_sepa_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sepa_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(200) NOT NULL,
  `date_received` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('New','Processing','Done') NOT NULL DEFAULT 'New',
  `companyid` int(11) NOT NULL,
  `uploader` varchar(100) NOT NULL DEFAULT 'System',
  PRIMARY KEY (`id`),
  UNIQUE KEY `filename` (`filename`),
  KEY `companyid` (`companyid`)
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sepa_items`
--

DROP TABLE IF EXISTS `a_sepa_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sepa_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileid` int(11) NOT NULL,
  `date_transaction` date NOT NULL,
  `debitcredit` varchar(20) DEFAULT NULL,
  `companyid` int(11) NOT NULL,
  `transactioncode` varchar(100) DEFAULT NULL,
  `transactionissuer` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `iban` varchar(100) DEFAULT NULL,
  `end2endid` varchar(100) DEFAULT NULL,
  `mandateid` varchar(50) DEFAULT NULL,
  `message` text,
  `name_owner` varchar(50) DEFAULT NULL,
  `return_code` varchar(100) DEFAULT NULL,
  `return_extra` varchar(50) DEFAULT 'not_sent',
  `status` enum('Pending','Completed') NOT NULL DEFAULT 'Pending',
  `BatchPaymentId` varchar(20) DEFAULT NULL,
  `pmt` varchar(10) DEFAULT NULL,
  `cd` varchar(10) DEFAULT NULL,
  `oxt` text,
  `invoicenumber` int(11) DEFAULT NULL,
  `iAddressNbr` int(11) DEFAULT NULL,
  `extra_status` tinyint(1) NOT NULL DEFAULT '0',
  `iPaymentNbr` int(11) DEFAULT NULL,
  `date_processing` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2780 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sepa_reject_codes`
--

DROP TABLE IF EXISTS `a_sepa_reject_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sepa_reject_codes` (
  `rejectcode` varchar(20) NOT NULL,
  `description` varchar(545) DEFAULT NULL,
  PRIMARY KEY (`rejectcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sepa_trx_code`
--

DROP TABLE IF EXISTS `a_sepa_trx_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sepa_trx_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debitcredit` varchar(500) NOT NULL,
  `transactioncode` varchar(500) NOT NULL,
  `isocode1` varchar(45) NOT NULL,
  `isocode2` varchar(45) NOT NULL,
  `isocode3` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_service_aqa`
--

DROP TABLE IF EXISTS `a_service_aqa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_service_aqa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serviceid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `msisdn` varchar(15) NOT NULL,
  `puk1` int(10) NOT NULL,
  `puk2` int(10) NOT NULL,
  `pin` int(4) NOT NULL,
  `msisdn_sim` varchar(25) NOT NULL,
  `msisdn_sn` varchar(15) NOT NULL,
  `status` enum('Pending','Active','Cancelled','Barred') NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `msisdn_type` enum('new','porting') NOT NULL,
  `remote_id` int(11) NOT NULL,
  `contactid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `serviceid` (`serviceid`),
  KEY `userid` (`userid`),
  KEY `msisdn` (`msisdn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services`
--

DROP TABLE IF EXISTS `a_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) DEFAULT NULL,
  `identity` varchar(15) DEFAULT NULL,
  `type` enum('mobile','xdsl','voip','others') NOT NULL,
  `status` enum('New','Pending','Processing','Active','Terminated','Cancelled','Suspended') NOT NULL DEFAULT 'Pending',
  `packageid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `date_created` date DEFAULT NULL,
  `date_terminate` date DEFAULT NULL,
  `termination_status` tinyint(4) DEFAULT '0',
  `billingcycle` enum('Monthly','Quarterly','Semi-Annually','Annually') NOT NULL DEFAULT 'Monthly',
  `date_start` date DEFAULT NULL,
  `date_contract` varchar(12) DEFAULT NULL,
  `date_contract_formated` date DEFAULT NULL,
  `date_contract_prolong` date DEFAULT NULL,
  `recurring` decimal(15,4) DEFAULT '0.0000',
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `notes` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `promocode` varchar(100) DEFAULT NULL,
  `iGeneralPricingIndex` varchar(200) DEFAULT NULL,
  `contract_terms` int(11) DEFAULT '1',
  `suspend_reason` text,
  `reject_reason` text,
  `future_activation` decimal(15,4) DEFAULT NULL,
  `xml_order` text,
  `autodelete` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `userid` (`userid`),
  KEY `status` (`status`),
  KEY `packageid` (`packageid`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=1818385 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services_addons`
--

DROP TABLE IF EXISTS `a_services_addons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services_addons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serviceid` int(11) DEFAULT NULL,
  `companyid` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `terms` int(11) DEFAULT NULL,
  `cycle` varchar(20) DEFAULT NULL,
  `addonid` int(11) NOT NULL,
  `recurring_total` decimal(10,4) NOT NULL,
  `addon_type` enum('option','others') NOT NULL DEFAULT 'option',
  `arta_bundleid` int(11) DEFAULT NULL,
  `iGeneralPricingIndex` int(11) DEFAULT NULL,
  `import` tinyint(4) NOT NULL DEFAULT '0',
  `proformaid` int(11) DEFAULT NULL,
  `date_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `teum_autoRenew` int(10) DEFAULT '0',
  `teum_DateStart` date DEFAULT NULL,
  `teum_DateEnd` date NOT NULL,
  `teum_NextRenewal` date DEFAULT NULL,
  `teum_CustomerOrderId` int(11) DEFAULT NULL,
  `teum_SubscriptionId` int(11) DEFAULT NULL,
  `teum_ProductId` int(11) DEFAULT NULL,
  `teum_ProductChargePurchaseId` varchar(20) DEFAULT NULL,
  `teum_SubscriptionProductAssnId` varchar(30) DEFAULT NULL,
  `teum_ServiceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `serviceid` (`serviceid`),
  KEY `addonid` (`addonid`),
  KEY `arta_bundleid` (`arta_bundleid`),
  KEY `teum_SubscriptionId` (`teum_SubscriptionId`)
) ENGINE=InnoDB AUTO_INCREMENT=2105 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services_mobile`
--

DROP TABLE IF EXISTS `a_services_mobile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services_mobile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL DEFAULT '53',
  `userid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `teum_accountid` varchar(30) DEFAULT NULL,
  `teum_customerid` varchar(30) DEFAULT NULL,
  `teum_subscriptionid` varchar(30) DEFAULT NULL,
  `msisdn_imsi` varchar(25) DEFAULT NULL,
  `msisdn_sn` bigint(13) DEFAULT NULL,
  `msisdn_sim` varchar(30) DEFAULT NULL,
  `msisdn_pin` varchar(10) DEFAULT NULL,
  `msisdn_puk1` varchar(10) DEFAULT NULL,
  `msisdn_puk2` varchar(10) DEFAULT NULL,
  `msisdn_status` enum('OrderWaiting','OrderActive','SimAssigned','ActivationRequested','ActivationError','Active','PortinAccepted','PortinRejected','PortinCancelled','PortinPending','SimBlocked','Suspended','Terminated','Cancelled','PortInValidateVerificationCodeAccepted','PortInValidateVerificationCodeRejected','PortOutRequest','PortinFailed','PortInWaitValidateVerificationCode','PortedOut') DEFAULT 'OrderWaiting',
  `msisdn_type` enum('porting','new') DEFAULT 'new',
  `msisdn_languageid` tinyint(4) DEFAULT '3',
  `msisdn_swap` tinyint(4) DEFAULT '0',
  `msisdn_contactid` int(11) DEFAULT '7',
  `arta_contactid_status` tinyint(4) NOT NULL DEFAULT '1',
  `donor_type` tinyint(4) DEFAULT '0',
  `donor_sim` varchar(30) DEFAULT NULL,
  `donor_msisdn` varchar(20) DEFAULT NULL,
  `donor_provider` varchar(30) DEFAULT NULL,
  `donor_customertype` tinyint(4) DEFAULT '0',
  `donor_accountnumber` varchar(30) DEFAULT NULL,
  `datacon_ftp` tinyint(4) DEFAULT '0',
  `datacon_date` date DEFAULT NULL,
  `date_ported` timestamp NULL DEFAULT NULL,
  `date_accepted` datetime DEFAULT NULL,
  `date_wish` date DEFAULT NULL,
  `ptype` enum('POST PAID','PRE PAID') DEFAULT 'POST PAID',
  `porting_sms` tinyint(4) DEFAULT '0',
  `porting_remark` text,
  `ptype_belgium` enum('PREPAY','POSTPAY_SIMPLE','POSTPAY_COMPLEX','NA') DEFAULT 'NA',
  `arta_packageid` int(11) DEFAULT NULL,
  `divert` tinyint(1) NOT NULL DEFAULT '0',
  `bar` tinyint(1) DEFAULT '0',
  `GPRS` tinyint(1) DEFAULT '1',
  `MMS` tinyint(1) DEFAULT '1',
  `LTE` tinyint(4) DEFAULT '1',
  `Roaming` tinyint(1) DEFAULT '1',
  `BarInternationalCalls` tinyint(1) DEFAULT '0',
  `BarPremium` tinyint(1) NOT NULL DEFAULT '1',
  `BarPremiumVoice` tinyint(1) DEFAULT '1',
  `bundleids` text,
  `initial_reload_amount` int(11) DEFAULT '0',
  `sim_delivery` varchar(40) DEFAULT 'MVNO',
  `pod_lastrequest` bigint(13) DEFAULT NULL,
  `pod_counter` int(11) DEFAULT '0',
  `packages_state` varchar(500) DEFAULT NULL,
  `portin_reminder` int(11) DEFAULT '0',
  `platform` enum('ARTA','TEUM') DEFAULT 'ARTA',
  `date_modified` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `serviceid` (`serviceid`),
  KEY `msisdn` (`msisdn`),
  KEY `companyid` (`companyid`),
  KEY `msisdn_sn` (`msisdn_sn`),
  KEY `datacon_ftp` (`datacon_ftp`),
  KEY `msisdn_status` (`msisdn_status`),
  KEY `msisdn_type` (`msisdn_type`),
  KEY `ptype` (`ptype`),
  KEY `teum_accountid` (`teum_accountid`),
  KEY `teum_customerid` (`teum_customerid`),
  KEY `teum_subscriptionid` (`teum_subscriptionid`)
) ENGINE=InnoDB AUTO_INCREMENT=14642 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services_mobile_error`
--

DROP TABLE IF EXISTS `a_services_mobile_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services_mobile_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servicename` varchar(30) DEFAULT NULL,
  `error_code` varchar(20) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services_pstn`
--

DROP TABLE IF EXISTS `a_services_pstn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services_pstn` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `carrier_selection_type` enum('IDA','CPS') NOT NULL,
  `carrier_connection_type` enum('STD','WLR') NOT NULL,
  `cli` varchar(20) NOT NULL,
  `tele_services` varchar(20) NOT NULL,
  `directory_listing` enum('Full','Partial','None') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services_voip`
--

DROP TABLE IF EXISTS `a_services_voip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services_voip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `cli` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `external_serviceid` int(11) NOT NULL,
  `mac_address` varchar(20) NOT NULL,
  `device_serialnumber` varchar(20) NOT NULL,
  `timezone` varchar(20) NOT NULL,
  `directory_listing` enum('Full','Partial','None','') NOT NULL,
  `contact_msisdn` varchar(20) NOT NULL,
  `device_item_name` varchar(50) DEFAULT NULL,
  `provisioning_username` varchar(50) DEFAULT NULL,
  `metranet_serviceid` varchar(50) DEFAULT NULL,
  `contract_information` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `serviceid` (`serviceid`),
  KEY `userid` (`userid`),
  KEY `cli` (`cli`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services_xdsl`
--

DROP TABLE IF EXISTS `a_services_xdsl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services_xdsl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requestid` varchar(40) DEFAULT NULL,
  `serviceid` int(11) NOT NULL,
  `companyid` int(11) NOT NULL DEFAULT '2',
  `proximus_orderid` varchar(40) DEFAULT NULL,
  `userid` int(11) NOT NULL,
  `circuitid` varchar(15) DEFAULT NULL,
  `status` enum('Pending','Provisioning','Done','Discarded','Others','On-Hold','Ordered','Validated','Workorder-Needed','Rejected','Appointment-Booked','Amend-Confirmed','Active','Trerminated','Ceased') NOT NULL DEFAULT 'Pending',
  `street` varchar(60) CHARACTER SET utf8mb4 NOT NULL,
  `number` int(11) NOT NULL,
  `alpha` varchar(10) DEFAULT NULL,
  `bus` varchar(10) DEFAULT NULL,
  `floor` varchar(10) DEFAULT NULL,
  `block` varchar(10) DEFAULT NULL,
  `city` varchar(50) CHARACTER SET utf8 NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country` varchar(3) DEFAULT 'BEL',
  `phonenumber` varchar(15) DEFAULT NULL,
  `dialnumber` varchar(15) DEFAULT NULL,
  `proximus_status` enum('Pending','Accepted','Installation','Active','Rejected') NOT NULL DEFAULT 'Pending',
  `realm` varchar(20) DEFAULT 'UNITED',
  `pppoe_username` varchar(30) DEFAULT NULL,
  `pppoe_password` varchar(20) DEFAULT NULL,
  `profile` varchar(10) DEFAULT NULL,
  `lex` varchar(10) DEFAULT NULL,
  `proformaid` bigint(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_updated` timestamp NULL DEFAULT NULL,
  `product_name` varchar(30) DEFAULT NULL,
  `product_id` varchar(10) DEFAULT NULL,
  `bussiness_pack` tinyint(1) NOT NULL DEFAULT '0',
  `fixed_ip` varchar(30) DEFAULT NULL,
  `address_verified` tinyint(4) NOT NULL DEFAULT '0',
  `introductionPairUsed` varchar(100) DEFAULT NULL,
  `feedback_description` text,
  `address_lang` varchar(2) DEFAULT NULL,
  `MDU_SDU` varchar(10) DEFAULT NULL,
  `appointment_date_requested` varchar(20) DEFAULT NULL,
  `appointment_date_confirmed` varchar(20) DEFAULT NULL,
  `date_activation` date DEFAULT NULL,
  `remarks` text,
  `remark_installation` text CHARACTER SET utf8,
  `workorderid` int(11) DEFAULT NULL,
  `type_install` enum('Unknown','Visit','Remote') NOT NULL DEFAULT 'Unknown',
  `type_order` enum('PROVIDE','CHANGE','CHANGE_OWNER') NOT NULL DEFAULT 'PROVIDE',
  `InvoiceGroupMaster` int(11) DEFAULT NULL,
  `ArticleIdVOIP` int(11) DEFAULT NULL,
  `ordercycle` enum('NEW','AMENDPRODUCT','CORRECTION') NOT NULL DEFAULT 'NEW',
  `channel` varchar(20) DEFAULT NULL,
  `SNA` tinyint(4) NOT NULL DEFAULT '0',
  `adsl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`serviceid`),
  KEY `companyid` (`companyid`),
  KEY `proximus_orderid` (`proximus_orderid`),
  KEY `proformaid` (`proformaid`),
  KEY `proximus_status` (`proximus_status`),
  KEY `postcode` (`postcode`),
  KEY `requestid` (`requestid`),
  KEY `circuitid` (`circuitid`)
) ENGINE=InnoDB AUTO_INCREMENT=1018 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services_xdsl_callback`
--

DROP TABLE IF EXISTS `a_services_xdsl_callback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services_xdsl_callback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(27) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rawdata` text NOT NULL,
  `action` varchar(100) NOT NULL,
  `env` enum('prod','dev') NOT NULL DEFAULT 'dev',
  `received` int(11) DEFAULT NULL,
  `answered` tinyint(11) DEFAULT NULL,
  `comments` text,
  `processed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `action` (`action`)
) ENGINE=InnoDB AUTO_INCREMENT=5409 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_services_xdsl_products`
--

DROP TABLE IF EXISTS `a_services_xdsl_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_services_xdsl_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(20) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `serviceid` int(11) DEFAULT NULL,
  `productid` varchar(10) DEFAULT NULL,
  `technology` varchar(20) DEFAULT NULL,
  `date_query` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `product_name` varchar(50) NOT NULL,
  `numberOfFreePairs` int(11) DEFAULT NULL,
  `averageLength` int(11) DEFAULT NULL,
  `unitOfMeasure` varchar(10) DEFAULT NULL,
  `minimumAttenuation` decimal(10,2) DEFAULT NULL,
  `maximumAttenuation` decimal(10,2) DEFAULT NULL,
  `networkType` varchar(20) DEFAULT NULL,
  `serviceAvailabilityStatus` varchar(10) DEFAULT NULL,
  `downstreamBandwidth` varchar(10) DEFAULT NULL,
  `upstreamBandwidth` varchar(10) DEFAULT NULL,
  `measuredMinimumDownloadSpeed` varchar(10) DEFAULT NULL,
  `measuredMaximumDownloadSpeed` varchar(10) DEFAULT NULL,
  `measuredMinimumUploadSpeed` varchar(10) DEFAULT NULL,
  `measuredMaximumUploadSpeed` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderid` (`orderid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=2077 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sessions`
--

DROP TABLE IF EXISTS `a_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_simcard_porting_not_ok`
--

DROP TABLE IF EXISTS `a_simcard_porting_not_ok`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_simcard_porting_not_ok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msisdn` varchar(12) NOT NULL,
  `donor_msisdn` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sms_log`
--

DROP TABLE IF EXISTS `a_sms_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batchid` int(11) DEFAULT NULL,
  `companyid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_sent` timestamp NULL DEFAULT NULL,
  `number` bigint(15) NOT NULL,
  `message` varchar(240) CHARACTER SET utf8 NOT NULL,
  `status` enum('Sent','Failed','Draft','Queued','Cancelled') NOT NULL DEFAULT 'Draft',
  `send_by` varchar(50) NOT NULL,
  `filter_raw` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sms_notifications`
--

DROP TABLE IF EXISTS `a_sms_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sms_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `msisdn` varchar(15) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3905 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sms_template`
--

DROP TABLE IF EXISTS `a_sms_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sms_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `companyid` int(11) NOT NULL,
  `language` varchar(20) NOT NULL DEFAULT 'dutch',
  `message` varchar(164) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_subscription_changes`
--

DROP TABLE IF EXISTS `a_subscription_changes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_subscription_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `old_pid` int(11) NOT NULL,
  `new_pid` int(11) NOT NULL,
  `date_commit` date NOT NULL,
  `requestor` varchar(30) NOT NULL,
  `date_request` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sn` varchar(12) NOT NULL,
  `msisdn` varchar(12) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `executor` varchar(30) DEFAULT NULL,
  `cron_status` tinyint(4) NOT NULL DEFAULT '0',
  `new_price` decimal(10,4) NOT NULL,
  `keep_start_contract_date` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `ContractDuration` tinyint(4) NOT NULL DEFAULT '12',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_subscriptions_manual`
--

DROP TABLE IF EXISTS `a_subscriptions_manual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_subscriptions_manual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin` varchar(70) NOT NULL,
  `userid` int(11) NOT NULL,
  `companyid` int(11) NOT NULL,
  `terms` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `description` varchar(200) NOT NULL,
  `vat_rate` decimal(10,2) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `end_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_sum_report`
--

DROP TABLE IF EXISTS `a_sum_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_sum_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL DEFAULT '54',
  `msisdn` varchar(14) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `target` decimal(10,2) NOT NULL,
  `used` decimal(10,2) NOT NULL,
  `extra` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `current_slack_percent` decimal(10,2) NOT NULL,
  `current_slack_amount` decimal(10,2) NOT NULL,
  `expectation_slack_percent` decimal(12,2) NOT NULL,
  `expectation_slack_amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=748 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_tblInvoice`
--

DROP TABLE IF EXISTS `a_tblInvoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_tblInvoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iCompanyNbr` int(11) NOT NULL,
  `iInvoiceNbr` int(10) DEFAULT NULL,
  `iAddressNbr` int(10) DEFAULT NULL,
  `iInvoiceType` int(4) DEFAULT NULL,
  `dInvoiceDate` date DEFAULT NULL,
  `dInvoiceDueDate` date DEFAULT NULL,
  `mInvoiceAmount` decimal(10,4) DEFAULT NULL,
  `tRemark` varchar(100) DEFAULT NULL,
  `iInvoiceStatus` int(4) DEFAULT NULL,
  `iPrintType` int(4) DEFAULT NULL,
  `bBookkeepingLock` int(4) DEFAULT NULL,
  `dLastUpdate` varchar(25) DEFAULT NULL,
  `iLastUserNbr` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iAddressNbr` (`iAddressNbr`),
  KEY `iInvoiceStatus` (`iInvoiceStatus`),
  KEY `dInvoiceDate` (`dInvoiceDate`),
  KEY `iLastUserNbr` (`iLastUserNbr`),
  KEY `iCompanyNbr` (`iCompanyNbr`),
  KEY `iInvoiceType` (`iInvoiceType`),
  KEY `iInvoiceNbr` (`iInvoiceNbr`)
) ENGINE=InnoDB AUTO_INCREMENT=2462246 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_tokens`
--

DROP TABLE IF EXISTS `a_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` enum('mvno','reseller','client') NOT NULL DEFAULT 'mvno',
  `companyid` int(11) NOT NULL,
  `agentid` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `token` text NOT NULL,
  `creator` varchar(100) NOT NULL,
  `sso` varchar(3) NOT NULL DEFAULT 'off',
  `host` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_topups`
--

DROP TABLE IF EXISTS `a_topups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_topups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `income_type` enum('bundle','topup','other') NOT NULL,
  `companyid` int(11) NOT NULL,
  `serviceid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `agentid` int(11) DEFAULT NULL,
  `amount` decimal(15,4) NOT NULL,
  `user` varchar(40) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bundle_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=373 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_topups_monthly`
--

DROP TABLE IF EXISTS `a_topups_monthly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_topups_monthly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `agentid` int(11) NOT NULL,
  `month` varchar(10) NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_translations`
--

DROP TABLE IF EXISTS `a_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `filename` varchar(20) NOT NULL DEFAULT 'admin',
  `lang` varchar(12) NOT NULL,
  `name` varchar(400) CHARACTER SET utf8 NOT NULL,
  `value` varchar(400) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companyid` (`companyid`),
  KEY `lang` (`lang`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=31554 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_united_resellers`
--

DROP TABLE IF EXISTS `a_united_resellers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_united_resellers` (
  `ResellerId` int(11) NOT NULL,
  `resellername` varchar(500) DEFAULT NULL,
  `resellerlogin` varchar(500) DEFAULT NULL,
  `resellerpass` varchar(500) DEFAULT NULL,
  `platformtype` varchar(45) DEFAULT NULL,
  `whmcsdb` varchar(500) DEFAULT NULL,
  `whmscurl` varchar(500) DEFAULT NULL,
  `whmcsadminurl` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`ResellerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `a_whitelist_ip`
--

DROP TABLE IF EXISTS `a_whitelist_ip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `a_whitelist_ip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `import_agent`
--

DROP TABLE IF EXISTS `import_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_agent` (
  `a` int(11) NOT NULL,
  `b` varchar(100) DEFAULT NULL,
  `c` varchar(100) DEFAULT NULL,
  `d` varchar(100) DEFAULT NULL,
  `e` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`a`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `import_trendcall_notes`
--

DROP TABLE IF EXISTS `import_trendcall_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_trendcall_notes` (
  `notesid` int(10) NOT NULL AUTO_INCREMENT,
  `customerid` int(10) DEFAULT NULL,
  `datenote` datetime DEFAULT NULL,
  `entered` varchar(100) DEFAULT NULL,
  `notes` text CHARACTER SET utf8mb4,
  `status` varchar(100) DEFAULT NULL,
  `priority` varchar(100) DEFAULT NULL,
  `assigned` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `closed` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`notesid`)
) ENGINE=InnoDB AUTO_INCREMENT=45603 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `magebo_invoices`
--

DROP TABLE IF EXISTS `magebo_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `magebo_invoices` (
  `iInvoiceNbr` int(11) NOT NULL AUTO_INCREMENT,
  `iAddressNbr` int(11) NOT NULL,
  `iInvoiceType` int(11) NOT NULL,
  `dInvoiceDate` date NOT NULL,
  `dInvoiceDueDate` date NOT NULL,
  `mInvoiceAmount` decimal(10,4) NOT NULL,
  `tRemark` varchar(100) NOT NULL,
  `iInvoiceStatus` int(11) NOT NULL,
  `iPrintType` int(11) NOT NULL,
  `bBookkeepingLock` int(11) NOT NULL,
  `dLastUpdate` datetime NOT NULL,
  `iLastUserNbr` int(11) NOT NULL,
  `iCompanyNbr` int(11) NOT NULL,
  PRIMARY KEY (`iInvoiceNbr`),
  KEY `iAddressNbr` (`iAddressNbr`),
  KEY `iInvoiceNbr` (`iInvoiceNbr`),
  KEY `iInvoiceStatus` (`iInvoiceStatus`),
  KEY `dInvoiceDate` (`dInvoiceDate`),
  KEY `iCompanyNbr` (`iCompanyNbr`)
) ENGINE=InnoDB AUTO_INCREMENT=300000002 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mytable`
--

DROP TABLE IF EXISTS `mytable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mytable` (
  `Code` varchar(7) NOT NULL,
  `Name` varchar(54) NOT NULL,
  `Symbol` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payments_payu`
--

DROP TABLE IF EXISTS `payments_payu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments_payu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` int(11) NOT NULL,
  `extOrderId` varchar(100) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `date` datetime NOT NULL,
  `payment_id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL,
  `orderId` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `extOrderId` (`extOrderId`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `public_street`
--

DROP TABLE IF EXISTS `public_street`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public_street` (
  `id_street` int(11) NOT NULL AUTO_INCREMENT,
  `zip` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `name_ascii` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `city` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `country` varchar(2) NOT NULL DEFAULT 'BE',
  PRIMARY KEY (`id_street`),
  KEY `street_zip_index` (`zip`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=150418 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `public_zip`
--

DROP TABLE IF EXISTS `public_zip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `public_zip` (
  `id_zip` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(4) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip_ascii` varchar(100) DEFAULT NULL,
  `country` varchar(2) NOT NULL DEFAULT 'BE',
  PRIMARY KEY (`id_zip`),
  KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5753 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tblorder_billi`
--

DROP TABLE IF EXISTS `tblorder_billi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblorder_billi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `circuit_id` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `userid` int(11) DEFAULT NULL,
  `lang` varchar(2) DEFAULT 'nl',
  `serviceid` int(11) DEFAULT NULL,
  `orderid` int(11) DEFAULT NULL,
  `wemanage_id` int(11) DEFAULT NULL,
  `activation_code` int(12) DEFAULT NULL,
  `proximus_profile` varchar(10) DEFAULT NULL,
  `street_install` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `number_install` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `bus_install` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `alpha_install` varchar(10) DEFAULT NULL,
  `city_install` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `postcode_install` varchar(6) CHARACTER SET latin1 DEFAULT NULL,
  `floor_install` varchar(2) CHARACTER SET latin1 DEFAULT NULL,
  `block_install` varchar(5) DEFAULT NULL,
  `install_date` date DEFAULT NULL,
  `serial` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `ppp_user` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `ppp_pass` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `sip_user` varchar(15) DEFAULT NULL,
  `sip_pass` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `sip_plan` varchar(10) DEFAULT NULL,
  `sip_status` varchar(10) DEFAULT NULL,
  `sip_enable` int(11) DEFAULT NULL,
  `lex_id` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `attn` varchar(10) DEFAULT NULL,
  `dialnumber` varchar(12) DEFAULT NULL,
  `gsm` varchar(15) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT '022900900',
  `appartement` varchar(10) DEFAULT NULL,
  `date_order` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order_status` varchar(20) DEFAULT NULL,
  `firstname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `lastname` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `vat` varchar(20) DEFAULT NULL,
  `company_name` varchar(25) DEFAULT NULL,
  `ipv6_switch` tinyint(1) NOT NULL DEFAULT '1',
  `wifi_switch` tinyint(1) NOT NULL DEFAULT '1',
  `wifi_ssid` varchar(30) DEFAULT NULL,
  `wifi_key` varchar(35) DEFAULT NULL,
  `webpass` varchar(20) DEFAULT NULL,
  `webuser` varchar(20) DEFAULT NULL,
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `ip` varchar(18) DEFAULT NULL,
  `snr` text,
  `xml` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idxserial` (`serial`),
  KEY `serviceid` (`serviceid`),
  KEY `wemanage_id` (`wemanage_id`),
  KEY `circuit_id` (`circuit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59407 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-11  8:23:34
